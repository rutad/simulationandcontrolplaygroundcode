thickness = 0.003;
clearance = 0.0002;
minX = -0.0143 - clearance;
maxX = 0.0143 + clearance;
minY = -0.0353 - clearance;
maxY = 0.0113 + clearance;
minZ = -0.017 - clearance;
maxZ = 0.017 + clearance;
SX = maxX - minX;
SY = maxY - minY;
SZ = maxZ - minZ;
height = 0.015;

translate([minX - 0.5 * thickness, minY - thickness + height * 0.5, 0]){
    cube(size = [thickness, height, SZ + 2 * thickness], center = true);
}

translate([maxX + 0.5 * thickness, minY - thickness + height * 0.5, 0]){
    cube(size = [thickness, height, SZ + 2 * thickness], center = true);
}

translate([0, minY - thickness + height * 0.5, minZ - 0.5 * thickness]){
    cube(size = [SX + 2 * thickness, height, thickness], center = true);
}

translate([0, minY - thickness + height * 0.5, maxZ + 0.5 * thickness]){
    cube(size = [SX + 2 * thickness, height, thickness], center = true);
}

translate([0, minY - 0.5 * thickness, 0]){
    cube(size = [SX + 2 * thickness, thickness, SZ + 2 * thickness], center = true);
}

