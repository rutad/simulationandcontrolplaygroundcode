# README #

This repository with many branches containes codes associated with various projects done during my PhD. Related papes and project info: www.cs.cmu.edu/~rutad/

1)Modular robot design (ICRA 2017) in branch ICRAModularRobots

2)Codesign of robot structure and function (CLAWAR 2018) in branch beichen (final) and beichen_video 

3)Assembly aware design of 3D printable electromechanical devices in branch Ruta, Yincheng and master (final)

4)Semantic design of Robot behaviors in branch Semantic Design

5)Experiements on Bayesian Optimization in branch BayesianOptForDesign

6)Collaborative project on automatic design of task-specific robotic arms in branch Airbus and Airbus_Rita (final)

Codebase built upon the code of Stelian Coros. Other collaborators on code: Beichen Li, Ye Yuan, Yincheng Zhao, Margarita Safonova.

Dependency: All libraries stored in libs repo (need to have this downloaded and path saved in PATH env variable of the system for any of the code to work), and Visual Studio

Questions: rutad@cmu.edu