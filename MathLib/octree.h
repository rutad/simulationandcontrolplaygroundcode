#pragma once

//TODO: octree should operate on points (or triangle centroids)...
//  An octree storing triangles. Can be intersected with a sphere or a line segment. 

#include <vector>
#include "boundingBox.h"
#include "Triangles.h"
#include "Sphere.h"

template<class TriangleClass> class Octree {
public:
  // make empty octree
  // maxDepth = maximum tree depth allowed
  // depth = how deep in the hierarchy is this octree (for recursion) (you should use depth=0 when using this class; non-zero values are only passed internally)
  Octree(int maxDepth=10, int depth=0);
  ~Octree() { deallocate(); }

  bool build(std::vector<TriangleClass> & triangleList, AxisAlignedBoundingBox & parentCube, int maxNumTriangles);

  // will construct a bounding box automatically:
  bool build(std::vector<TriangleClass> & triangleList, int maxNumTriangles);
  void setBuildPrintInfo(int info); // controls what gets printed out during construction: 0 = nothing, 1 = up to five warnings, 2 = all warnings
  void getBuildInfo(int * numMaxDepthExceededCases, int * numMaxTriInDepthExceededCases);

  // note: these two routines might return the same colliding triangle several times; call <TriangleClass>::makeUniqueList to make the list unique if needed
  void buildCollisionList(std::vector<TriangleClass*> & triangleList, const SimpleSphere & simpleSphere);
  void buildCollisionList(std::vector<TriangleClass*> & triangleList, P3D segmentStartPoint, P3D segmentEndPoint);

  void render(); // openGL rendering
  void render(int level); // only render leaf boxes at depth level 'level'
  void render(int level, int boxIndex); // only render leaf boxes number "boxIndex" at depth level 'level'
  void setRenderInfo(int info) { printRenderInfo = info; }

  int getDepth(); // compute tree depth
  AxisAlignedBoundingBox getBoundingBox() { return boundingBox; }

protected:

  void deallocate(); // free the memory
  void createChildCubes(AxisAlignedBoundingBox * childCubeBoxes);

  AxisAlignedBoundingBox boundingBox;
  Octree * childrenNodes[8];

  int maxDepth; // max depth allowed
  int depth; // how deep in the hierarchy is this octree

  void renderHelper(int level, int boxIndex); // only render leaf boxes number "boxIndex" at depth level 'level'
  static int renderCounter; // for rendering
  static int printRenderInfo;

  std::vector<TriangleClass> triangles;

  static int buildPrintInfo;
  static int numMaxDepthExceededCases;
  static int numMaxTriInDepthExceededCases;
};

template<class TriangleClass>
inline void Octree<TriangleClass>::getBuildInfo(int * numMaxDepthExceededCases, int * numMaxTriInDepthExceededCases)
{
  *numMaxDepthExceededCases = this->numMaxDepthExceededCases;
  *numMaxTriInDepthExceededCases = this->numMaxTriInDepthExceededCases;
}

template<class TriangleClass>
inline void Octree<TriangleClass>::setBuildPrintInfo(int info)
{
  buildPrintInfo = info;
  if (info == 1)
  {
    numMaxDepthExceededCases = 0;
    numMaxTriInDepthExceededCases = 0;
  }
}
