/*
* Copyright (c) 2007, Carnegie Mellon University
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*     * Redistributions of source code must retain the above copyright
*       notice, this list of conditions and the following disclaimer.
*     * Redistributions in binary form must reproduce the above copyright
*       notice, this list of conditions and the following disclaimer in the
*       documentation and/or other materials provided with the distribution.
*     * Neither the name of University of Southern California, nor the
*       names of its contributors may be used to endorse or promote products
*       derived from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY CARNEGIE MELLON UNIVERSITY
* ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
* WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
* IN NO EVENT SHALL CARNEGIE MELLON UNIVERSITY BE 
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
* (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
* LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
* ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
* (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
* SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

/*
* Copyright (c) 2015, University of Southern California
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*     * Redistributions of source code must retain the above copyright
*       notice, this list of conditions and the following disclaimer.
*     * Redistributions in binary form must reproduce the above copyright
*       notice, this list of conditions and the following disclaimer in the
*       documentation and/or other materials provided with the distribution.
*     * Neither the name of University of Southern California, nor the
*       names of its contributors may be used to endorse or promote products
*       derived from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY UNIVERSITY OF SOUTHERN CALIFORNIA 
* ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
* WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
* IN NO EVENT SHALL UNIVERSITY OF SOUTHERN CALIFORNIA BE 
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
* (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
* LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
* ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
* (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
* SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

/*
  Code author: Jernej Barbic

  Multithreaded computation of the distance field. Main computational routines.
*/

#ifdef COMPUTE_SIGNED_FIELD
  #ifdef COMPUTE_FLOOD_FIELD
    #define COMPUTEFIELDMT_CLASSNAME DistanceFieldMT
    #define COMPUTEFIELDMT_STRUCTNAME DistanceFieldMT_signed_flood_struct
    #define COMPUTEFIELDMT_WORKERTHREADNAME DistanceFieldMT_signed_flood_WorkerThread
    #define ZIGZAGROUTINE DistanceField::ZigZagFloodFillSigned
  #else
    #ifdef COMPUTE_CLOSEST_POINT
      #define COMPUTEFIELDMT_CLASSNAME ClosestPointFieldMT
      #define COMPUTEFIELDMT_STRUCTNAME ClosestPointFieldMT_signed_struct
      #define COMPUTEFIELDMT_WORKERTHREADNAME ClosestPointFieldMT_signed_WorkerThread
      #define ZIGZAGROUTINE ClosestPointField::ZigZagSigned
    #else
      #define COMPUTEFIELDMT_CLASSNAME DistanceFieldMT
      #define COMPUTEFIELDMT_STRUCTNAME DistanceFieldMT_signed_struct
      #define COMPUTEFIELDMT_WORKERTHREADNAME DistanceFieldMT_signed_WorkerThread
      #define ZIGZAGROUTINE DistanceField::ZigZagSigned
    #endif
  #endif
#else
  #ifdef COMPUTE_CLOSEST_POINT
    #define COMPUTEFIELDMT_CLASSNAME ClosestPointFieldMT
    #define COMPUTEFIELDMT_STRUCTNAME ClosestPointFieldMT_unsigned_struct
    #define COMPUTEFIELDMT_WORKERTHREADNAME ClosestPointFieldMT_unsigned_WorkerThread
    #define ZIGZAGROUTINE ClosestPointField::ZigZagUnsigned
  #else
    #define COMPUTEFIELDMT_CLASSNAME DistanceFieldMT
    #define COMPUTEFIELDMT_STRUCTNAME DistanceFieldMT_unsigned_struct
    #define COMPUTEFIELDMT_WORKERTHREADNAME DistanceFieldFieldMT_unsigned_WorkerThread
    #define ZIGZAGROUTINE DistanceField::ZigZagUnsigned
  #endif
#endif

#define USE_MULTICORE_LIBRARY
#ifdef USE_MULTICORE_LIBRARY
  #include "multicore.h"
#endif

struct COMPUTEFIELDMT_STRUCTNAME
{
  COMPUTEFIELDMT_CLASSNAME * object;
  void * objMeshOctree;
  void * meshGraph;
  int rank;
  int startSlice;
  int endSlice;
};

void * COMPUTEFIELDMT_WORKERTHREADNAME(void * arg)
{  
  struct COMPUTEFIELDMT_STRUCTNAME * threadArgp = (struct COMPUTEFIELDMT_STRUCTNAME*) arg;
  COMPUTEFIELDMT_CLASSNAME * object = threadArgp->object;
  void * objMeshOctree = threadArgp->objMeshOctree;
  void * meshGraph = threadArgp->meshGraph;
  int rank = threadArgp->rank;  
  //int startSlice = object->GetStartSlice(rank);
  //int endSlice = object->GetEndSlice(rank);
  int startSlice = threadArgp->startSlice;
  int endSlice = threadArgp->endSlice;
  //printf("rank: %d startSlice: %d endSlice: %d\n", rank, startSlice, endSlice);

  int asterisk=0;
  if (rank == 0)
    asterisk = 1;

  object->ZIGZAGROUTINE(objMeshOctree, meshGraph, startSlice, endSlice-1, asterisk);

  return NULL;
}

#ifdef COMPUTE_SIGNED_FIELD
  #ifdef COMPUTE_CLOSEST_POINT
    int ClosestPointFieldMT::ZigZagSigned(void * objMeshOctree_, void * meshGraph_)
  #else
    #ifdef COMPUTE_FLOOD_FIELD
      int DistanceFieldMT::ZigZagFloodFillSigned(void * objMeshOctree_, void * meshGraph_)
    #else
      int DistanceFieldMT::ZigZagSigned(void * objMeshOctree_, void * meshGraph_)
    #endif
  #endif
#else
  #ifdef COMPUTE_CLOSEST_POINT
    int ClosestPointFieldMT::ZigZagUnsigned(void * objMeshOctree_, void * meshGraph_)
  #else
    int DistanceFieldMT::ZigZagUnsigned(void * objMeshOctree_, void * meshGraph_)
  #endif
#endif
{
  Setup();

  struct COMPUTEFIELDMT_STRUCTNAME * threadArgv = (struct COMPUTEFIELDMT_STRUCTNAME*) malloc (sizeof(struct COMPUTEFIELDMT_STRUCTNAME) * numThreads);

  for(int i=0; i<numThreads; i++)
  {
    threadArgv[i].object = this;
    threadArgv[i].objMeshOctree = objMeshOctree_;
    threadArgv[i].meshGraph = meshGraph_;
    threadArgv[i].rank = i;
    threadArgv[i].startSlice = GetStartSlice(i);
    threadArgv[i].endSlice = GetEndSlice(i);
  }
   
  #ifdef USE_MULTICORE_LIBRARY 
    Multicore * multicore = new Multicore(numThreads);
    for(int i=0; i<numThreads; i++)
    {
      multicore->SetThreadTask(i, COMPUTEFIELDMT_WORKERTHREADNAME);    
      multicore->SetThreadTaskParameter(i, &threadArgv[i]);
    }

    //assigns equal z-range to every thread (weakness: can be non-even computational load)
    //multicore->LaunchThreads(0, numThreads);

    int zSlice = zMin;
    while (zSlice <= zMax)
    {
      int availableThread;
      multicore->WaitForAvailableThread(&availableThread);
      threadArgv[availableThread].startSlice = zSlice;
      threadArgv[availableThread].endSlice = zSlice + 1;
      multicore->LaunchThread(availableThread);
      zSlice++;
    }

    multicore->WaitForCompletionOfAllThreads();

    delete(multicore);
  #else
    printf("Launching %d threads...\n", numThreads);
    pthread_t * tid = (pthread_t*) malloc (sizeof(pthread_t) * numThreads);
    for(int i=0; i<numThreads; i++)
    {
      if (pthread_create(&tid[i], NULL, COMPUTEFIELDMT_WORKERTHREADNAME, &threadArgv[i]) != 0)
      {
        printf("Error: unable to launch thread %d.\n", i);
        exit(1);
      }
    }
  
    for(int i=0; i<numThreads; i++)
    {
      if (pthread_join(tid[i], NULL) != 0)
      {
        printf("Error: unable to join thread %d.\n", i);
        free(threadArgv);
        free(tid);
        free(startSlice);
        free(endSlice);
        return 1;
      }
    }
    free(tid);
  #endif

  free(threadArgv);
  free(startSlice);
  free(endSlice);

  return 0;
}

#undef ZIGZAGROUTINE
#undef COMPUTEFIELDMT_CLASSNAME
#undef COMPUTEFIELDMT_STRUCTNAME
#undef COMPUTEFIELDMT_WORKERTHREADNAME

