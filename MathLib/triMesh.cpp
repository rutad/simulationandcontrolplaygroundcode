#pragma warning(disable : 4996)
#pragma warning(disable : 4267)
#pragma warning(disable : 4244)

#include <float.h>
#include <math.h>
#include <string.h>
#include <vector>
#include <string>
#include <map>
#include <set>
#include <fstream>
#include <sstream>
#include <algorithm>
#include <assert.h>
#include "triMesh.h"
using namespace std;

TriMesh::TriMesh(int numVertices, double * vertices, int numTriangles, int * triangles){

	for (int i = 0; i<numVertices; i++)
		addVertex(P3D(vertices[3 * i + 0], vertices[3 * i + 1], vertices[3 * i + 2]));
	for (int i = 0; i<numTriangles; i++)
		faces.push_back(Triangle(triangles[3 * i + 0], triangles[3 * i + 1], triangles[3 * i + 2]));

	computeBoundingBox();
}


TriMesh::TriMesh(const TriMesh & triMesh_){
	this->faces = triMesh_.faces;
	this->vertexPositions = triMesh_.vertexPositions;
	computeBoundingBox();
}

void TriMesh::computeBoundingBox(){
	bmin = P3D(DBL_MAX, DBL_MAX, DBL_MAX);
	bmax = P3D(-DBL_MAX, -DBL_MAX, -DBL_MAX);

	for (unsigned int i = 0; i < vertexPositions.size(); i++) {
		P3D p = vertexPositions[i];

		if (p[0] < bmin[0]) bmin[0] = p[0];
		if (p[0] > bmax[0]) bmax[0] = p[0];

		if (p[1] < bmin[1]) bmin[1] = p[1];
		if (p[1] > bmax[1])	bmax[1] = p[1];

		if (p[2] < bmin[2])	bmin[2] = p[2];
		if (p[2] > bmax[2])	bmax[2] = p[2];
	}

	center = (bmin + bmax) * 0.5;
	cubeHalf = (bmax - bmin) * 0.5;
	diameter = V3D(bmax, bmin).length();
}

void TriMesh::getBoundingBox(double expansionRatio, P3D * bmin_, P3D * bmax_) const {
	*bmin_ = center - cubeHalf * expansionRatio;
	*bmax_ = center + cubeHalf * expansionRatio;
}

void TriMesh::getCubicBoundingBox(double expansionRatio, P3D * bmin_, P3D * bmax_) const {
	double maxHalf = cubeHalf[0];
	if (cubeHalf[1] > maxHalf) maxHalf = cubeHalf[1];
	if (cubeHalf[2] > maxHalf) maxHalf = cubeHalf[2];

	P3D cubeHalfCube = P3D(maxHalf, maxHalf, maxHalf);

	*bmin_ = center - cubeHalfCube * expansionRatio;
	*bmax_ = center + cubeHalfCube * expansionRatio;
}

void TriMesh::getMeshRadius(const P3D & centroid, double * radius) const {
	double radiusSquared = 0.0;
	for (auto iter = vertexPositions.begin(); iter != vertexPositions.end(); iter++) {
		double newSquared = V3D(*iter, centroid).length2();
		if (newSquared > radiusSquared)
			radiusSquared = newSquared;
	}
	*radius = sqrt(radiusSquared);
}

void TriMesh::getMeshGeometricParameters(P3D * centroid, double * radius) const{
	//find the centroid
	*centroid = P3D(0, 0, 0);
	for (auto iter = vertexPositions.begin(); iter != vertexPositions.end(); iter++)
		*centroid += *iter;
	*centroid /= vertexPositions.size();

	// find the radius
	getMeshRadius(*centroid, radius);
}

void TriMesh::scaleUniformly(const P3D & center, double factor){
	for (unsigned int i = 0; i < vertexPositions.size(); i++) // over all vertices
		vertexPositions[i] = center + (vertexPositions[i] - center) * factor;

	computeBoundingBox();
}

void TriMesh::transformRigidly(const P3D & translation, const Quaternion& rotation){
	for (unsigned int i = 0; i < vertexPositions.size(); i++) // over all vertices
		vertexPositions[i] = translation + rotation * V3D(vertexPositions[i]);

	computeBoundingBox();
}

double TriMesh::computeVolume() const{
	double volume = 0;

	// over all faces
	for (uint iFace = 0; iFace < faces.size(); iFace++){
		TriMesh::Triangle face = faces[iFace]; // get face whose number is iFace
		// base vertex
		P3D v0 = getPosition(face.v1Index);
		P3D v1 = getPosition(face.v2Index);
		P3D v2 = getPosition(face.v3Index);

		V3D normal = V3D(v0, v1).cross(V3D(v0, v2));
		V3D center = V3D((v0 + v1 + v2) / 3.0);
		volume += (normal.dot(center));
	}
	volume /= 6.0;
	return volume;
}

double TriMesh::computeSurfaceArea() const {
	double area = 0;
	// over all faces
	for (unsigned int iFace = 0; iFace < faces.size(); iFace++)
			area += computeFaceSurfaceArea(faces[iFace]);
	return area;
}

double TriMesh::computeFaceSurfaceArea(const TriMesh::Triangle& face) const {
	double faceSurfaceArea = 0;

	P3D pos0 = getPosition(face.v1Index);
	P3D pos1 = getPosition(face.v2Index);
	P3D pos2 = getPosition(face.v3Index);

	return computeTriangleSurfaceArea(pos0, pos1, pos2);
}

P3D TriMesh::computeFaceCentroid(const TriMesh::Triangle& face) const {
	return (getPosition(face.v1Index) + getPosition(face.v2Index) + getPosition(face.v3Index)) / 3.0;
}

// warning: normal is computed using the first three face vertices (assumes planar face)
V3D TriMesh::computeFaceNormal(const TriMesh::Triangle & face) const{
	// the three vertices
	P3D pos0 = getPosition(face.v1Index);
	P3D pos1 = getPosition(face.v2Index);
	P3D pos2 = getPosition(face.v3Index);
	V3D normal = V3D(pos0, pos1).cross(V3D(pos0, pos2));

	if (IS_ZERO(normal.length()) || isNaN(normal[0]) || isNaN(normal[1]) || isNaN(normal[2])){
		//degenerate geometry; return an arbitrary normal
		normal = P3D(1.0, 0.0, 0.0);
	}

	return normal.unit();
}

uint TriMesh::getClosestVertex(const P3D & queryPos, double * distance) const {
	double closestDist2 = DBL_MAX;
	double candidateDist2;
	uint indexClosest = 0;
	for (uint i = 0; i< getNumVertices(); i++) {
		V3D relPos = (queryPos, getPosition(i));
		if ((candidateDist2 = relPos.length2()) < closestDist2) {
			closestDist2 = candidateDist2;
			indexClosest = i;
		}
	}

	if (distance != NULL)
		*distance = sqrt(closestDist2);
	return indexClosest;
}

int TriMesh::removeZeroAreaFaces(){
	int numZeroAreaFaces = 0;

	// over all faces
	for (unsigned int iFace = 0; iFace < faces.size(); iFace++){
		TriMesh::Triangle face = faces[iFace]; // get face whose number is iFace
		P3D pos0 = getPosition(face.v1Index);
		P3D pos1 = getPosition(face.v2Index);
		P3D pos2 = getPosition(face.v3Index);
		V3D normal = V3D(pos0, pos1).cross(V3D(pos0, pos2));

		bool identicalVertex = (face.v1Index == face.v2Index || face.v1Index == face.v3Index || face.v2Index == face.v3Index);
		if (IS_ZERO(normal.length()) || isNaN(normal[0]) || isNaN(normal[1]) || isNaN(normal[2]) || identicalVertex){
			faces.erase(faces.begin() + iFace);
			iFace--;
			numZeroAreaFaces++;
		}
	}
	return numZeroAreaFaces;
}

/*
TriMesh * TriMesh::splitIntoConnectedComponents(int withinGroupsOnly, int verbose) const
{
// withinGroupsOnly:
// 0: off (global split; may fuse texture coordinates)
// 1: intersect global connected components with groups
// 2: break each group into connected components, regardless of the rest of the mesh

vector<DisjointSet*> dset;
if (withinGroupsOnly == 2)
{
for (unsigned int i = 0; i < groups.size(); i++) // over all groups
{
DisjointSet * groupDisjointSet = new DisjointSet(getNumVertices());
groupDisjointSet->MakeSet();
dset.push_back(groupDisjointSet);
}
}
else
{
DisjointSet * globalDisjointSet = new DisjointSet(getNumVertices());
globalDisjointSet->MakeSet();
for (unsigned int i = 0; i < groups.size(); i++) // over all groups
dset.push_back(globalDisjointSet);
}

// build vertex connections
for (unsigned int i = 0; i < groups.size(); i++) // over all groups
{
// if (verbose == 0)
// {
//   printf("%d ", i);
//   fflush(NULL);
// }

for (unsigned int j = 0; j < groups[i].getNumFaces(); j++) // over all faces
{
if (verbose)
{
if (j % 100 == 1)
printf("Processing group %d / %d, face %d / %d.\n", i, (int)groups.size(), j, (int)groups[i].getNumFaces());
}
const Face * face = groups[i].getFaceHandle(j);
int faceDegree = (int)face->getNumVertices();
for (int vtx = 0; vtx<faceDegree - 1; vtx++)
{
int vertex = face->getVertex(vtx).getPositionIndex();
int vertexNext = face->getVertex(vtx + 1).getPositionIndex();
dset[i]->UnionSet(vertex, vertexNext);
}
}
}
// if (verbose == 0)
//   printf("\n");

// determine group for every face
int numOutputGroups = 0;
vector<map<int, int> *> representatives;
if (withinGroupsOnly == 2)
{
for (unsigned int i = 0; i < groups.size(); i++) // over all groups
{
map<int, int> * groupMap = new map<int, int>();
representatives.push_back(groupMap);
}
}
else
{
map<int, int> * globalMap = new map<int, int>();
for (unsigned int i = 0; i < groups.size(); i++) // over all groups
representatives.push_back(globalMap);
}

vector<vector<int> > faceGroup;
for (unsigned int i = 0; i < groups.size(); i++) // over all groups
{
// if (verbose == 0)
// {
//   printf("%d ", i);
//   fflush(NULL);
// }

faceGroup.push_back(vector<int>());
for (unsigned int j = 0; j < groups[i].getNumFaces(); j++) // over all faces
{
if (verbose)
{
if (j % 100 == 1)
printf("Processing group %d / %d, face %d / %d.\n", i, (int)groups.size(), j, (int)groups[i].getNumFaces());
}
const Face * face = groups[i].getFaceHandle(j);
int rep = dset[i]->FindSet(face->getVertex(0).getPositionIndex());

map<int, int> ::iterator iter = representatives[i]->find(rep);
int groupID;
if (iter == representatives[i]->end())
{
groupID = numOutputGroups;
representatives[i]->insert(make_pair(rep, numOutputGroups));
numOutputGroups++;
}
else
groupID = iter->second;

faceGroup[i].push_back(groupID);
}
}

// if (verbose == 0)
//   printf("\n");

// build output mesh
TriMesh * output = new TriMesh();

// output vertices
for (unsigned int i = 0; i<getNumVertices(); i++)
output->addVertexPosition(getPosition(i));

// output normals
for (unsigned int i = 0; i<getNumNormals(); i++)
output->addVertexNormal(getNormal(i));

// output texture coordinates
for (unsigned int i = 0; i<getNumTextureCoordinates(); i++)
output->addTextureCoordinate(getTextureCoordinate(i));

// output materials
for (unsigned int i = 0; i<getNumMaterials(); i++)
output->addMaterial(getMaterial(i));

// create output groups, taking into account potential splitting in case: withinGroupsOnly == 1
int groupCount = 0;
map <pair<int, int>, int> outputGroup;
for (unsigned int i = 0; i < groups.size(); i++) // over all groups
{
for (unsigned int j = 0; j < groups[i].getNumFaces(); j++) // over all faces
{
int groupID = faceGroup[i][j];
if (withinGroupsOnly == 1)
{
if (outputGroup.find(make_pair(i, groupID)) == outputGroup.end())
{
outputGroup.insert(make_pair(make_pair(i, groupID), groupCount));
groupCount++;
}
}
else
{
outputGroup.insert(make_pair(make_pair(i, groupID), groupID));
}
}
}

if (withinGroupsOnly == 1)
numOutputGroups = groupCount;

// create groups
for (int i = 0; i<numOutputGroups; i++)
{
char s[96];
sprintf(s, "group%05d", i);
output->addGroup(string(s));
}

// add faces to groups
for (unsigned int i = 0; i < groups.size(); i++) // over all groups
{
for (unsigned int j = 0; j < groups[i].getNumFaces(); j++) // over all faces
{
const Face * face = groups[i].getFaceHandle(j);
int connectedGroupID = faceGroup[i][j];

map<pair<int, int>, int> ::iterator iter = outputGroup.find(make_pair(i, connectedGroupID));
if (iter == outputGroup.end())
{
printf("Error: encountered unhandled (input group, connected group) case.\n");
}
int groupID = iter->second;

output->addFaceToGroup(*face, groupID);

// set the material for this group
Group * outputGroupHandle = (Group*)output->getGroupHandle(groupID);
outputGroupHandle->setMaterialIndex(groups[i].getMaterialIndex());
}
}

output->computeBoundingBox();

// de-allocate
for (unsigned int i = 0; i < groups.size(); i++) // over all groups
{
delete(representatives[i]);
representatives[i] = NULL;
delete(dset[i]);
dset[i] = NULL;

if (withinGroupsOnly != 2)
break;
}

return output;
}


double TriMesh::computeMinEdgeLength() const
{
	double minLength = -1;

	// over all faces
	for (unsigned int i = 0; i < groups.size(); i++)
	{
		for (unsigned int iFace = 0; iFace < groups[i].getNumFaces(); iFace++)
		{
			TriMesh::Face face = groups[i].getFace(iFace); // get face whose number is iFace

			if (face.getNumVertices() < 3)
				cout << "Warning: encountered a face (group=" << i << ",face=" << iFace << ") with fewer than 3 vertices." << endl;

			for (unsigned k = 0; k<face.getNumVertices(); k++)
			{
				P3D pos0 = getPosition(face.getVertex(k));
				P3D pos1 = getPosition(face.getVertex((k + 1) % face.getNumVertices()));
				double length = len(pos1 - pos0);

				if (minLength < 0) // only the first time
					minLength = length;
				else if (length < minLength)
					minLength = length;
			}
		}
	}

	return minLength;

}

double TriMesh::computeAverageEdgeLength() const
{
	double totalLength = 0.0;
	int numEdges = 0;

	// over all faces
	for (unsigned int i = 0; i < groups.size(); i++)
	{
		for (unsigned int iFace = 0; iFace < groups[i].getNumFaces(); iFace++)
		{
			TriMesh::Face face = groups[i].getFace(iFace); // get face whose number is iFace

			if (face.getNumVertices() < 3)
				cout << "Warning: encountered a face (group=" << i << ",face=" << iFace << ") with fewer than 3 vertices." << endl;

			for (unsigned k = 0; k<face.getNumVertices(); k++)
			{
				P3D pos0 = getPosition(face.getVertex(k));
				P3D pos1 = getPosition(face.getVertex((k + 1) % face.getNumVertices()));
				double length = len(pos1 - pos0);
				totalLength += length;
				numEdges++;
			}
		}
	}

	return totalLength / numEdges;
}

double TriMesh::computeMedianEdgeLength() const
{
	vector<double> lengths;

	// over all faces
	for (unsigned int i = 0; i < groups.size(); i++)
	{
		for (unsigned int iFace = 0; iFace < groups[i].getNumFaces(); iFace++)
		{
			TriMesh::Face face = groups[i].getFace(iFace); // get face whose number is iFace

			if (face.getNumVertices() < 3)
				cout << "Warning: encountered a face (group=" << i << ",face=" << iFace << ") with fewer than 3 vertices." << endl;

			for (unsigned k = 0; k<face.getNumVertices(); k++)
			{
				P3D pos0 = getPosition(face.getVertex(k));
				P3D pos1 = getPosition(face.getVertex((k + 1) % face.getNumVertices()));
				double length = len(pos1 - pos0);
				lengths.push_back(length);
			}
		}
	}

	sort(lengths.begin(), lengths.end());

	return lengths[lengths.size() / 2];
}

double TriMesh::computeMaxEdgeLength() const
{
	double maxLength = 0;

	// over all faces
	for (unsigned int i = 0; i < groups.size(); i++)
	{
		for (unsigned int iFace = 0; iFace < groups[i].getNumFaces(); iFace++)
		{
			TriMesh::Face face = groups[i].getFace(iFace); // get face whose number is iFace

			if (face.getNumVertices() < 3)
				cout << "Warning: encountered a face (group=" << i << ",face=" << iFace << ") with fewer than 3 vertices." << endl;

			for (unsigned k = 0; k<face.getNumVertices(); k++)
			{
				P3D pos0 = getPosition(face.getVertex(k));
				P3D pos1 = getPosition(face.getVertex((k + 1) % face.getNumVertices()));
				double length = len(pos1 - pos0);
				if (length > maxLength)
					maxLength = length;
			}
		}
	}

	return maxLength;
}

double TriMesh::computeMinEdgeLength(int * vtxa, int * vtxb) const
{
	*vtxa = *vtxb = -1;

	double minLength = -1;

	// over all faces
	for (unsigned int i = 0; i < groups.size(); i++)
	{
		for (unsigned int iFace = 0; iFace < groups[i].getNumFaces(); iFace++)
		{
			TriMesh::Face face = groups[i].getFace(iFace); // get face whose number is iFace

			if (face.getNumVertices() < 3)
				cout << "Warning: encountered a face (group=" << i << ",face=" << iFace << ") with fewer than 3 vertices." << endl;

			for (unsigned k = 0; k<face.getNumVertices(); k++)
			{
				P3D pos0 = getPosition(face.getVertex(k));
				P3D pos1 = getPosition(face.getVertex((k + 1) % face.getNumVertices()));

				double length = len(pos1 - pos0);

				if (minLength < 0) // only the first time
					minLength = length;
				else if (length < minLength)
					minLength = length;

				*vtxa = face.getVertex(k).getPositionIndex();
				*vtxb = face.getVertex((k + 1) % face.getNumVertices()).getPositionIndex();
			}
		}
	}

	return minLength;
}

double TriMesh::computeMaxEdgeLength(int * vtxa, int * vtxb) const
{
	*vtxa = *vtxb = -1;

	double maxLength = 0;

	// over all faces
	for (unsigned int i = 0; i < groups.size(); i++)
	{
		for (unsigned int iFace = 0; iFace < groups[i].getNumFaces(); iFace++)
		{
			TriMesh::Face face = groups[i].getFace(iFace); // get face whose number is iFace

			if (face.getNumVertices() < 3)
				cout << "Warning: encountered a face (group=" << i << ",face=" << iFace << ") with fewer than 3 vertices." << endl;

			for (unsigned k = 0; k<face.getNumVertices(); k++)
			{
				P3D pos0 = getPosition(face.getVertex(k));
				P3D pos1 = getPosition(face.getVertex((k + 1) % face.getNumVertices()));
				double length = len(pos1 - pos0);

				if (length > maxLength)
					maxLength = length;

				*vtxa = face.getVertex(k).getPositionIndex();
				*vtxb = face.getVertex((k + 1) % face.getNumVertices()).getPositionIndex();
			}
		}
	}

	return maxLength;
}


int TriMesh::removeHangingFaces(){
	map< pair<unsigned int, unsigned int>, vector<pair<unsigned int, unsigned int> > > facesAdjacentToEdge;

	// build facesAdjacentToEdge
	// over all faces
	for (unsigned int iGroup = 0; iGroup < groups.size(); iGroup++)
	{
		for (unsigned int iFace = 0; iFace < groups[iGroup].getNumFaces(); iFace++)
		{
			TriMesh::Face face = groups[iGroup].getFace(iFace); // get face whose number is iFace
			for (unsigned int j = 0; j<face.getNumVertices(); j++) // over all vertices of this face
			{
				unsigned int vtxIndexA = face.getVertex(j).getPositionIndex();
				unsigned int vtxIndexB = face.getVertex((j + 1) % face.getNumVertices()).getPositionIndex();
				if (vtxIndexA > vtxIndexB)
					std::swap(vtxIndexA, vtxIndexB);

				std::pair<unsigned int, unsigned int> myPair(vtxIndexA, vtxIndexB);
				if (facesAdjacentToEdge.find(myPair) == facesAdjacentToEdge.end())
					facesAdjacentToEdge.insert(make_pair(myPair, vector<pair<unsigned int, unsigned int> >()));
				facesAdjacentToEdge[myPair].push_back(make_pair(iGroup, iFace));
			}
		}
	}

	set<pair<unsigned int, unsigned int> > eraseList;

	// check the map for edges with more than two neighbors
	for (map< pair<unsigned int, unsigned int>, vector<pair<unsigned int, unsigned int> > > ::iterator iter = facesAdjacentToEdge.begin(); iter != facesAdjacentToEdge.end(); iter++)
	{
		if ((iter->second).size() > 2)
		{
			// edge has more than two neighboring faces

			// check all adjacent faces, to see if any of them has an edge that has no other neighbor
			for (unsigned int i = 0; i<(iter->second).size(); i++)
			{
				unsigned int iGroup = (iter->second)[i].first;
				unsigned int iFace = (iter->second)[i].second;

				TriMesh::Face face = groups[iGroup].getFace(iFace); // get face whose number is iFace
				for (unsigned int j = 0; j<face.getNumVertices(); j++) // over all vertices
				{
					unsigned int vtxIndexA = face.getVertex(j).getPositionIndex();
					unsigned int vtxIndexB = face.getVertex((j + 1) % face.getNumVertices()).getPositionIndex();
					if (vtxIndexA > vtxIndexB)
						std::swap(vtxIndexA, vtxIndexB);

					std::pair<unsigned int, unsigned int> myPair(vtxIndexA, vtxIndexB);
					if (facesAdjacentToEdge[myPair].size() == 1)
					{
						// found an edge with only one neighboring face (this face)
						// erase the face
						eraseList.insert((iter->second)[i]);
						break;
					}
				}
			}
		}
	}

	// erase faces whose all three edges are not shared by any other face
	// over all faces
	for (unsigned int iGroup = 0; iGroup < groups.size(); iGroup++)
	{
		for (unsigned int iFace = 0; iFace < groups[iGroup].getNumFaces(); iFace++)
		{
			int eraseFace = 1;
			TriMesh::Face face = groups[iGroup].getFace(iFace); // get face whose number is iFace
			for (unsigned int j = 0; j<face.getNumVertices(); j++) // over all vertices of this face
			{
				unsigned int vtxIndexA = face.getVertex(j).getPositionIndex();
				unsigned int vtxIndexB = face.getVertex((j + 1) % face.getNumVertices()).getPositionIndex();
				if (vtxIndexA > vtxIndexB)
					std::swap(vtxIndexA, vtxIndexB);

				std::pair<unsigned int, unsigned int> myPair(vtxIndexA, vtxIndexB);
				if (facesAdjacentToEdge[myPair].size() > 1)
				{
					eraseFace = 0;
					break;
				}
			}

			if (eraseFace)
				eraseList.insert(make_pair(iGroup, iFace));
		}
	}

	//printf("Erase list size is: %d\n", (int)eraseList.size());

	// erase the scheduled faces
	// must iterate from the back to front, to have correct indexing
	for (set<pair<unsigned int, unsigned int> > ::reverse_iterator iter = eraseList.rbegin(); iter != eraseList.rend(); iter++)
	{
		unsigned int iGroup = iter->first;
		unsigned int iFace = iter->second;
		Group * groupHandle = (Group*)getGroupHandle(iGroup);
		groupHandle->removeFace(iFace);
	}

	return (int)eraseList.size();
}

int TriMesh::removeNonManifoldEdges()
{
	map< pair<unsigned int, unsigned int>, vector<pair<unsigned int, unsigned int> > > facesAdjacentToEdge;

	// build facesAdjacentToEdge
	// over all faces
	for (unsigned int iGroup = 0; iGroup < groups.size(); iGroup++)
	{
		for (unsigned int iFace = 0; iFace < groups[iGroup].getNumFaces(); iFace++)
		{
			TriMesh::Face face = groups[iGroup].getFace(iFace); // get face whose number is iFace
			for (unsigned int j = 0; j<face.getNumVertices(); j++) // over all vertices of this face
			{
				unsigned int vtxIndexA = face.getVertex(j).getPositionIndex();
				unsigned int vtxIndexB = face.getVertex((j + 1) % face.getNumVertices()).getPositionIndex();
				if (vtxIndexA > vtxIndexB)
					std::swap(vtxIndexA, vtxIndexB);

				std::pair<unsigned int, unsigned int> myPair(vtxIndexA, vtxIndexB);
				if (facesAdjacentToEdge.find(myPair) == facesAdjacentToEdge.end())
					facesAdjacentToEdge.insert(make_pair(myPair, vector<pair<unsigned int, unsigned int> >()));
				facesAdjacentToEdge[myPair].push_back(make_pair(iGroup, iFace));
			}
		}
	}

	vector<pair<unsigned int, unsigned int> > eraseList;

	// check the map for edges with more than two neighbors
	for (map<pair<unsigned int, unsigned int>, vector<pair<unsigned int, unsigned int> > > ::iterator iter = facesAdjacentToEdge.begin(); iter != facesAdjacentToEdge.end(); iter++)
	{
		if ((iter->second).size() > 2)
			eraseList.push_back(iter->first);
	}

	sort(eraseList.begin(), eraseList.end());
	//printf("Erase list size: %d\n", eraseList.size());

	int removedEdges = 0;

	for (unsigned int i = 0; i<eraseList.size(); i++)
	{
		if (eraseList[i].first == eraseList[i].second)
			continue;

		//printf("Removing edge: %d to %d.\n", eraseList[i].first, eraseList[i].second);

		int removeIsolatedVertices_ = 0;
		collapseEdge(eraseList[i].first, eraseList[i].second, removeIsolatedVertices_);
		removedEdges++;

		// renumber all future pairs
		for (unsigned int j = i + 1; j<eraseList.size(); j++)
		{
			if (eraseList[j].first == eraseList[i].second)
				eraseList[j].first = eraseList[i].first;
			if (eraseList[j].second == eraseList[i].second)
				eraseList[j].second = eraseList[i].first;

			if (eraseList[j].first > eraseList[j].second)
				std::swap(eraseList[j].first, eraseList[j].second);
		}
	}

	removeIsolatedVertices();

	return removedEdges;
}

void TriMesh::collapseEdge(unsigned int vertexA, unsigned int vertexB, int removeIsolatedVertices_)
{
	if (vertexA > vertexB)
		std::swap(vertexA, vertexB);

	// over all faces
	for (unsigned int iGroup = 0; iGroup < groups.size(); iGroup++)
	{
		Group * group = (Group*)getGroupHandle(iGroup);
		vector<unsigned int> eraseList;
		for (unsigned int iFace = 0; iFace < groups[iGroup].getNumFaces(); iFace++)
		{
			int eraseFace = 0;
			Face * face = (Face*)group->getFaceHandle(iFace); // get face whose number is iFace
			for (unsigned int j = 0; j<face->getNumVertices(); j++) // over all vertices of this face
			{
				unsigned int vtxIndex = face->getVertex(j).getPositionIndex();
				if (vtxIndex == vertexB)
				{
					Vertex * vertex = (Vertex*)face->getVertexHandle(j);
					vertex->setPositionIndex(vertexA);
				}
			}

			// remove consecutive vertices
			for (unsigned int j = 0; j<face->getNumVertices(); j++) // over all vertices of this face
			{
				unsigned int vtxIndexA = face->getVertex(j).getPositionIndex();
				unsigned int vtxIndexB = face->getVertex((j + 1) % face->getNumVertices()).getPositionIndex();
				if (vtxIndexA == vtxIndexB)
				{
					if (face->getNumVertices() <= 3)
						eraseFace = 1;
					else
						face->removeVertex(j);
					break;
				}
			}

			if (eraseFace)
				eraseList.push_back(iFace);
		}

		for (int i = (int)eraseList.size() - 1; i >= 0; i--)
		{
			//printf("Erasing face %d\n", eraseList[i]);
			group->removeFace(eraseList[i]);
		}
	}

	if (removeIsolatedVertices_)
		removeIsolatedVertices();
}


P3D TriMesh::getSurfaceSamplePosition(double sample) const
{
	unsigned int facePosition;
	for (facePosition = 0; facePosition< surfaceSamplingAreas.size() - 1; facePosition++)
	{
		if ((surfaceSamplingAreas[facePosition].first <= sample) &&
			(surfaceSamplingAreas[facePosition + 1].first > sample))
			break;
	}

	// facePosition now contains the index of the face to sample from
	const Face * face = surfaceSamplingAreas[facePosition].second;

	// sample at random on the face
	double alpha, beta;
	do
	{
		alpha = 1.0 * rand() / RAND_MAX;
		beta = 1.0 * rand() / RAND_MAX;
	} while (alpha + beta > 1);

	double gamma = 1 - alpha - beta;

	P3D v0 = getPosition(face->getVertex(0));
	P3D v1 = getPosition(face->getVertex(1));
	P3D v2 = getPosition(face->getVertex(2));

	P3D sampledPos = alpha * v0 + beta * v1 + gamma * v2;
	return sampledPos;
}

void TriMesh::buildVertexFaceNeighbors()
{
	vertexFaceNeighbors.clear();
	for (unsigned int i = 0; i<getNumVertices(); i++)
		vertexFaceNeighbors.push_back(std::list<VertexFaceNeighbor>());

	//go through each of the faces
	for (unsigned int i = 0; i < groups.size(); i++)
	{
		for (unsigned int iFace = 0; iFace < groups[i].getNumFaces(); iFace++)
		{
			const TriMesh::Face * faceHandle = groups[i].getFaceHandle(iFace); // get face whose number is iFace

			if (faceHandle->getNumVertices() < 3)
				cout << "Warning: encountered a face (group=" << i << ",face=" << iFace << ") with fewer than 3 vertices." << endl;

			for (unsigned int j = 0; j < faceHandle->getNumVertices(); j++)
			{
				const TriMesh::Vertex * vertexHandle = faceHandle->getVertexHandle(j);
				vertexFaceNeighbors[vertexHandle->getPositionIndex()].push_back(TriMesh::VertexFaceNeighbor(i, iFace, j));
			}
		}
	}
}

void TriMesh::clearVertexFaceNeighbors()
{
	vertexFaceNeighbors.clear();
}



int TriMesh::removeIsolatedVertices()
{
	vector<int> counter(getNumVertices(), 0);

	for (unsigned int i = 0; i < groups.size(); i++) // over all groups
		for (unsigned int j = 0; j < groups[i].getNumFaces(); j++) // over all faces
		{
			const Face * face = groups[i].getFaceHandle(j);
			for (unsigned int k = 0; k<face->getNumVertices(); k++)
				counter[face->getVertex(k).getPositionIndex()]++;
		}

	map<int, int> oldToNew;
	map<int, int> newToOld;

	int numConnectedVertices = 0;
	for (unsigned int i = 0; i<getNumVertices(); i++)
	{
		if (counter[i] != 0)
		{
			oldToNew.insert(make_pair(i, numConnectedVertices));
			newToOld.insert(make_pair(numConnectedVertices, i));
			numConnectedVertices++;
		}
	}

	int numOriginalVertices = getNumVertices();
	int numIsolatedVertices = numOriginalVertices - numConnectedVertices;

	// relink vertices, remove old vertices
	for (int i = 0; i<numConnectedVertices; i++)
		vertexPositions[i] = vertexPositions[newToOld[i]];

	for (int i = numConnectedVertices; i<numOriginalVertices; i++)
		vertexPositions.pop_back();

	// renumber vertices inside faces
	for (unsigned int i = 0; i < groups.size(); i++) // over all groups
		for (unsigned int j = 0; j < groups[i].getNumFaces(); j++) // over all faces
		{
			const Face * face = groups[i].getFaceHandle(j);
			for (unsigned int k = 0; k<face->getNumVertices(); k++)
			{
				Vertex * vertex = (Vertex*)face->getVertexHandle(k);
				int oldPositionIndex = vertex->getPositionIndex();
				vertex->setPositionIndex(oldToNew[oldPositionIndex]);
			}
		}

	return numIsolatedVertices;
}



void TriMesh::appendMesh(TriMesh * mesh)
{
	// add vertices
	int numVerticesCurrent = getNumVertices();
	for (unsigned int i = 0; i<mesh->getNumVertices(); i++)
		addVertexPosition(mesh->getPosition(i));

	// add normals
	int numNormalsCurrent = getNumNormals();
	for (unsigned int i = 0; i<mesh->getNumNormals(); i++)
		addVertexNormal(mesh->getNormal(i));

	// add texture coordinates
	int numTextureCoordinatesCurrent = getNumTextureCoordinates();
	for (unsigned int i = 0; i<mesh->getNumTextureCoordinates(); i++)
		addTextureCoordinate(mesh->getTextureCoordinate(i));

	// add materials
	int numMaterialsCurrent = getNumMaterials();
	for (unsigned int i = 0; i<mesh->getNumMaterials(); i++)
		addMaterial(mesh->getMaterial(i));

	for (unsigned int i = 0; i<mesh->getNumGroups(); i++)
	{
		const TriMesh::Group * group = mesh->getGroupHandle(i);
		addGroup(group->getName());
		unsigned int newGroupID = getNumGroups() - 1;
		TriMesh::Group * newGroup = (TriMesh::Group*) getGroupHandle(newGroupID);
		newGroup->setMaterialIndex(numMaterialsCurrent + group->getMaterialIndex());

		// over all faces in the group of the current obj file
		for (unsigned int j = 0; j<group->getNumFaces(); j++)
		{
			const TriMesh::Face * face = group->getFaceHandle(j);
			for (unsigned int k = 0; k<face->getNumVertices(); k++)
			{
				TriMesh::Vertex * vertex = (TriMesh::Vertex*) face->getVertexHandle(k);
				vertex->setPositionIndex(vertex->getPositionIndex() + numVerticesCurrent);
				if (vertex->hasNormalIndex())
					vertex->setNormalIndex(vertex->getNormalIndex() + numNormalsCurrent);
				if (vertex->hasTextureCoordinateIndex())
					vertex->setTextureCoordinateIndex(vertex->getTextureCoordinateIndex() + numTextureCoordinatesCurrent);
			}
			addFaceToGroup(*face, newGroupID);
		}
	}
}


*/
