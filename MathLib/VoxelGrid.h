#pragma once
#include <GUILib/GLMesh.h>
#include "boundingBox.h"
#include <vector>
#include <map>
using namespace std;

template<class T>
class VoxelGrid
{
public:
	vector<T> data;
	P3D startPoint;
	P3D endPoint;
	double voxelSize;
	int totalVoxelNum;
	int layerVoxelNum; // the number of voxels each x-y layer
	int voxelNum[3];  // the number of voxels along x, y, z

private:
	

public:
	VoxelGrid();
	VoxelGrid(AxisAlignedBoundingBox& bbox, double stepSize);

	~VoxelGrid();

	void setData(int x, int y, int z, const T& val);
	bool setDataSafe(int x, int y, int z, const T& val);
	void setData(int index, const T& val);
	T& getData(int x, int y, int z);
	T& getData(int index);
	P3D getVertex(int x, int y, int z);
	P3D getVoxelCenter(int x, int y, int z);

	bool isInside(int x, int y, int z);

	// Only voxels that have the value will be drawn.
	void glDraw(const T& val);
	// More intelligent glDraw, pass a test function and only passed voxels will be drawn
	void glDraw(bool (*test)(const T& A));

	void glDrawVoxel(int x, int y, int z);

	GLMesh* extractMeshWithData(const T& val);
	// More intelligent extracMesh, pass a test function and only passed voxels will be considered
	GLMesh* extractMeshWithData(bool(*test)(const T& A));

	void addFace(int x, int y, int z, int F, GLMesh* mesh, map<int, int>& vertexMap);
};


template<class T>
inline VoxelGrid<T>::VoxelGrid()
{
}

template<class T>
inline VoxelGrid<T>::VoxelGrid(AxisAlignedBoundingBox& bbox, double stepSize)
{
	voxelSize = stepSize;
	P3D center = bbox.center();

	for (int i = 0; i < 3; i++)
	{
		voxelNum[i] = (int)((bbox.bmax()[i] - bbox.bmin()[i]) / stepSize + 1);
		double ext = stepSize * voxelNum[i] * 0.5;
		startPoint[i] = center[i] - ext;
		endPoint[i] = center[i] + ext;
	}

	layerVoxelNum = voxelNum[0] * voxelNum[1];
	totalVoxelNum = layerVoxelNum * voxelNum[2];
	data.resize(totalVoxelNum);
}

template<class T>
inline VoxelGrid<T>::~VoxelGrid()
{
}

template<class T>
inline void VoxelGrid<T>::setData(int x, int y, int z, const T& val)
{
	int index = z * layerVoxelNum + (y * voxelNum[0] + x);
	data[index] = val;
}

template<class T>
inline bool VoxelGrid<T>::setDataSafe(int x, int y, int z, const T& val)
{
	if (isInside(x, y, z))
	{
		int index = z * layerVoxelNum + (y * voxelNum[0] + x);
		data[index] = val;
		return true;
	}

	return false;
}

template<class T>
inline void VoxelGrid<T>::setData(int index, const T& val)
{
	data[index] = val;
}

template<class T>
inline T& VoxelGrid<T>::getData(int x, int y, int z)
{
	int index = z * layerVoxelNum + (y * voxelNum[0] + x);
	return data[index];
}

template<class T>
inline T& VoxelGrid<T>::getData(int index)
{
	return data[index];
}

template<class T>
inline P3D VoxelGrid<T>::getVertex(int x, int y, int z)
{
	//if (x > voxelNum[0] || y > voxelNum[1] || z > voxelNum[2])
		//return P3D();

	P3D p;
	p[0] = startPoint[0] + x * voxelSize;
	p[1] = startPoint[1] + y * voxelSize;
	p[2] = startPoint[2] + z * voxelSize;

	return p;
}

template<class T>
inline P3D VoxelGrid<T>::getVoxelCenter(int x, int y, int z)
{
	//if (x >= voxelNum[0] || y >= voxelNum[1] || z >= voxelNum[2])
		//return P3D();

	P3D p;
	p[0] = startPoint[0] + (x + 0.5) * voxelSize;
	p[1] = startPoint[1] + (y + 0.5) * voxelSize;
	p[2] = startPoint[2] + (z + 0.5) * voxelSize;

	return p;
}

template<class T>
inline bool VoxelGrid<T>::isInside(int x, int y, int z)
{
	return x >= 0 && x < voxelNum[0]
		&& y >= 0 && y < voxelNum[1]
		&& z >= 0 && z < voxelNum[2];
}

template<class T>
inline void VoxelGrid<T>::glDraw(const T& val)
{
	for (int z = 0; z < voxelNum[2]; z++)
		for (int y = 0; y < voxelNum[1]; y++)
			for (int x = 0; x < voxelNum[0]; x++)
			{
				int index = z * layerVoxelNum + y * voxelNum[0] + x;
				if (data[index] == val)
					glDrawVoxel(x, y, z);
			}
}

template<class T>
inline void VoxelGrid<T>::glDraw(bool (*test)(const T& A))
{
	for (int z = 0; z < voxelNum[2]; z++)
		for (int y = 0; y < voxelNum[1]; y++)
			for (int x = 0; x < voxelNum[0]; x++)
			{
				int index = z * layerVoxelNum + y * voxelNum[0] + x;
				if (test(data[index]))
					glDrawVoxel(x, y, z);
			}
}

template<class T>
inline void VoxelGrid<T>::glDrawVoxel(int x, int y, int z)
{
	P3D vmin = getVertex(x, y, z);
	P3D vmax = getVertex(x + 1, y + 1, z + 1);

	drawBox(vmin, vmax);
}

template<class T>
inline GLMesh* VoxelGrid<T>::extractMeshWithData(const T& val)
{
	GLMesh* mesh = new GLMesh();
	map<int, int> vertexMap;

	for (int z = 0; z < voxelNum[2]; z++)
		for (int y = 0; y < voxelNum[1]; y++)
			for (int x = 0; x < voxelNum[0]; x++)
			{
				int index = z * layerVoxelNum + y * voxelNum[0] + x;
				if (data[index] != val) continue;

				for (int F = 0; F < 6; F++)
				{
					bool res = true;
					int nx = x;
					int ny = y;
					int nz = z;
					switch (F)
					{
					case 0: nx -= 1; res = nx >= 0;  break;
					case 1: nx += 1; res = nx < voxelNum[0]; break;
					case 2: ny -= 1; res = ny >= 0;  break;
					case 3: ny += 1; res = ny < voxelNum[1]; break;
					case 4: nz -= 1; res = nz >= 0;  break;
					case 5: nz += 1; res = nz < voxelNum[2]; break;
					default:
						break;
					}
					if (res && getData(nx, ny, nz) == val) continue;

					addFace(x, y, z, F, mesh, vertexMap);
				}
					
			}
	
	if (mesh->getVertexCount() == 0)
	{
		delete mesh;
		return NULL;
	}

	mesh->computeNormals();
	return mesh;
}

template<class T>
inline GLMesh* VoxelGrid<T>::extractMeshWithData(bool(*test)(const T& A))
{
	GLMesh* mesh = new GLMesh();
	map<int, int> vertexMap;

	for (int z = 0; z < voxelNum[2]; z++)
		for (int y = 0; y < voxelNum[1]; y++)
			for (int x = 0; x < voxelNum[0]; x++)
			{
				int index = z * layerVoxelNum + y * voxelNum[0] + x;
				if (!test(data[index])) continue;

				for (int F = 0; F < 6; F++)
				{
					bool res = true;
					int nx = x;
					int ny = y;
					int nz = z;
					switch (F)
					{
					case 0: nx -= 1; res = nx >= 0;  break;
					case 1: nx += 1; res = nx < voxelNum[0]; break;
					case 2: ny -= 1; res = ny >= 0;  break;
					case 3: ny += 1; res = ny < voxelNum[1]; break;
					case 4: nz -= 1; res = nz >= 0;  break;
					case 5: nz += 1; res = nz < voxelNum[2]; break;
					default:
						break;
					}
					if (res && test(getData(nx, ny, nz))) continue;

					addFace(x, y, z, F, mesh, vertexMap);
				}

			}

	mesh->computeNormals();
	return mesh;
}

template<class T>
inline void VoxelGrid<T>::addFace(int x, int y, int z, int F, GLMesh* mesh, map<int, int>& vertexMap)
{
	int vIndex[2][2];
	int nx, ny, nz;

	if (F >=0 && F < 2){
		nx = x + F % 2;
		for (ny = y; ny <= y + 1; ny++)
			for (nz = z; nz <= z + 1; nz++)
			{
				int hashVal = (nz << 16) + (ny << 8) + nx;
				auto itr = vertexMap.find(hashVal);
				if (itr == vertexMap.end()){
					vIndex[ny-y][nz-z] = vertexMap[hashVal] = mesh->getVertexCount();
					mesh->addVertex(getVertex(nx, ny, nz));
				}
				else {
					vIndex[ny-y][nz-z] = vertexMap[hashVal];
				}
			}

		if (F % 2 == 0) {
			GLIndexedTriangle triangle1(vIndex[0][0], vIndex[0][1], vIndex[1][1]);
			GLIndexedTriangle triangle2(vIndex[0][0], vIndex[1][1], vIndex[1][0]);
			mesh->addPoly(triangle1);
			mesh->addPoly(triangle2);
		}
		else {
			GLIndexedTriangle triangle1(vIndex[0][0], vIndex[1][1], vIndex[0][1]);
			GLIndexedTriangle triangle2(vIndex[0][0], vIndex[1][0], vIndex[1][1]);
			mesh->addPoly(triangle1);
			mesh->addPoly(triangle2);
		}
		
	}
	else if (F >= 2 && F < 4){
		ny = y + F % 2;
		for (nx = x; nx <= x + 1; nx++)
			for (nz = z; nz <= z + 1; nz++)
			{
				int hashVal = (nz << 16) + (ny << 8) + nx;
				auto itr = vertexMap.find(hashVal);
				if (itr == vertexMap.end()) {
					vIndex[nx - x][nz - z] = vertexMap[hashVal] = mesh->getVertexCount();
					mesh->addVertex(getVertex(nx, ny, nz));
				}
				else {
					vIndex[nx - x][nz - z] = vertexMap[hashVal];
				}
			}

		if (F % 2 == 0) {
			GLIndexedTriangle triangle1(vIndex[0][0], vIndex[1][1], vIndex[0][1]);
			GLIndexedTriangle triangle2(vIndex[0][0], vIndex[1][0], vIndex[1][1]);
			mesh->addPoly(triangle1);
			mesh->addPoly(triangle2);
		}
		else {
			GLIndexedTriangle triangle1(vIndex[0][0], vIndex[0][1], vIndex[1][1]);
			GLIndexedTriangle triangle2(vIndex[0][0], vIndex[1][1], vIndex[1][0]);
			mesh->addPoly(triangle1);
			mesh->addPoly(triangle2);
		}
	}
	else {
		nz = z + F % 2;
		for (nx = x; nx <= x + 1; nx++)
			for (ny = y; ny <= y + 1; ny++)
			{
				int hashVal = (nz << 16) + (ny << 8) + nx;
				auto itr = vertexMap.find(hashVal);
				if (itr == vertexMap.end()) {
					vIndex[nx - x][ny - y] = vertexMap[hashVal] = mesh->getVertexCount();
					mesh->addVertex(getVertex(nx, ny, nz));
				}
				else {
					vIndex[nx - x][ny - y] = vertexMap[hashVal];
				}
			}
		if (F % 2 == 0) {
			GLIndexedTriangle triangle1(vIndex[0][0], vIndex[0][1], vIndex[1][1]);
			GLIndexedTriangle triangle2(vIndex[0][0], vIndex[1][1], vIndex[1][0]);
			mesh->addPoly(triangle1);
			mesh->addPoly(triangle2);
		}
		else {
			GLIndexedTriangle triangle1(vIndex[0][0], vIndex[1][1], vIndex[0][1]);
			GLIndexedTriangle triangle2(vIndex[0][0], vIndex[1][0], vIndex[1][1]);
			mesh->addPoly(triangle1);
			mesh->addPoly(triangle2);
		}
	}
}