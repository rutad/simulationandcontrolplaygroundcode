//builds an octree on top of the geometry of a triangle mesh
//Can be intersected with a sphere or a line segment. 

#include "octree.h"
#include "triMesh.h"

template<class TriangleClass> class TriMeshOctree {
public:
  // builds an octree on top of the objMesh
  // maxNumTrianglesInLeafNode = max number of triangles in an octree leaf node
  // maxTreeDepth = max tree depth
  // (if tree depth were to exceed maxTreeDepth, a leaf node may have more than maxNumTrianglesInLeafNode triangles)
  TriMeshOctree( TriMesh * triMesh, int maxNumTrianglesInLeafNode, int maxTreeDepth, int printInfo = 1);
  ~TriMeshOctree() { delete(root); }

  // sphere-triangles query
  // retrieves all triangles intersected by the sphere 
  // (can potentially return the same triangle several times; call <TriangleClass>::makeUniqueList to make the list unique if needed)
  void rangeQuery(std::vector< TriangleClass* > & triangleList, const SimpleSphere & simpleSphere)
    { root->buildCollisionList(triangleList, simpleSphere); }

  // line segment-triangles query
  // retrieves all triangles intersected by the line segment
  // (can potentially return the same triangle several times; call <TriangleClass>::makeUniqueList to make the list unique if needed)
  void lineSegmentIntersection(std::vector< TriangleClass* > & triangleList, P3D segmentStart, P3D segmentEnd)
    { root->buildCollisionList(triangleList, segmentStart, segmentEnd); }

  void renderOctree() { root->render(); }
  void renderOctree(int level) { root->render(level); }
  void renderOctree(int level, int boxIndex) { root->render(level, boxIndex); }
  void setPrintOctreeInfo(int info) { root->setRenderInfo(info); }

  AxisAlignedBoundingBox boundingBox() { return (root->getBoundingBox()) ; }

protected:
  std::vector<TriangleClass> triangles;

  Octree<TriangleClass> * root;
  int maxNumTrianglesInLeafNode; // max number of triangles in an octree leaf node
  int maxTreeDepth; // max tree depth
  
  static const double bboxExpansionRatio;
};
