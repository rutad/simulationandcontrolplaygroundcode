#pragma once

#include <MathLib/MathLib.h>
#include <MathLib/Matrix.h>
#include <MathLib/V3D.h>

///<
class LinearSubspace
{
public:
	///< Not always a good idea to normalize the data...
	bool m_bNormalize = true;

	///< projection matrix:
	Eigen::MatrixXd		m_P;
	
	//	Average of samples:
	Eigen::MatrixXd		m_sampleMean;
	Eigen::MatrixXd		m_sampleStd;

	void				getMaxEigenVectors		(const Eigen::MatrixXd& _A, const int _numEigenModes);
	void				computeAndRemoveMean	(Eigen::MatrixXd& _A);

	int					numEigenVectors			()const;
	Eigen::MatrixXd		getEigenVector			(const int _i) const;	

	// Removes mean and projects.
	void				project					(const Eigen::MatrixXd& _x, Eigen::MatrixXd& _mReduced) const;

	// Un projects and adds mean
	void				unProject				(const Eigen::MatrixXd& _x, Eigen::MatrixXd& _mReduced) const;

	// Low-pass filterning by project and un-project.
	//	* Removes mean, projects, un-projects and then adds mean back. 
	void				projectAndUnproject		(const Eigen::MatrixXd& _mFullIn, Eigen::MatrixXd& _mFullOut) const;

	typedef Eigen::EigenSolver< Eigen::MatrixXd >::EigenvectorsType EigenVector;

};



// Take a point cloud, 
// -compute the average and
// -main vector offsets from the mean.
class PrincipalVector
{

public:

	typedef std::vector<V3D, Eigen::aligned_allocator<V3D>> PointsContainer;

	/// Constructor
	PrincipalVector(const PointsContainer& _points);
	~PrincipalVector();

	V3D getPrincipalVector	();
	V3D getMean				();


private:
	const PointsContainer&	m_dataPoints;
	LinearSubspace			m_pca;
};