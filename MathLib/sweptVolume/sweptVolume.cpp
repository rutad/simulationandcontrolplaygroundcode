#include "sweptVolume.h"
#include "../Quaternion.h"
#include <ctime>
#include <algorithm>

void sweptVolume::loadTrajectory(const Quaternion & q0, const Quaternion & q1, const P3D &p0, const P3D &p1)
{
	generator.loadTrajectory(q0, q1, p0, p1);
}

void sweptVolume::solve(GLMesh * ori, GLMesh *& des, double gridSize)
{
    //for (;;)
    {
        int time;

        FILE* fp = fopen("ori.obj", "w");
        ori->renderToObjFile(fp, 0, getRotationQuaternion(0, V3D(0, 0, 1)), P3D(0, 0, 0));
        std::fclose(fp);

        time = clock();
        GLMesh *tmp;
        tmp = generator.genRotateMesh(ori);
        printf("genRotateMesh Time : %d\n", clock() - time);

        fp = fopen("tmp.obj", "w");
        tmp->renderToObjFile(fp, 0, getRotationQuaternion(0, V3D(0, 0, 1)), P3D(0, 0, 0));
        std::fclose(fp);

        DistanceFields field(tmp);
        field.dx = gridSize;
        time = clock();
        field.calculate();
        printf("field calculate Time: %d\n", clock() - time);
        printf("field.calculate ... Done\n");
        vector<bool> outside;

        time = clock();
        findFront::solve(field.dis, outside, field.dx, field.xSum, field.ySum, field.zSum);
        printf("findFront::solve ... Done\n");
        printf("findFront Time: %d\n", clock() - time);
        time = clock();
        MarchingCubes cube(field.dis, outside, P3D(field.minX, field.minY, field.minZ), field.dx, field.xSum, field.ySum, field.zSum);
        des = cube.reconstruct();
        printf("cube.reconstruct ... Done\n");
        printf("reconstruct Time: %d\n", clock() - time);
        //if (detectConnected(des)) break;
        //gridSize /= 2.0;
        /*fp = fopen("ff.obj", "w");
        int fcnt = 0;
        for (int x = 0; x < field.xSum; x++) {
        	for (int y = 0; y < field.ySum; y++) {
        		for (int z = 0; z < field.zSum; z++) {
        			if (!outside[x * field.ySum * field.zSum + y * field.zSum + z]) {
        				double xx = field.minX + x * field.dx;
        				double yy = field.minY + y * field.dx;
        				double zz = field.minZ + z * field.dx;
        				fprintf(fp, "v %lf %lf %lf\n", xx - field.dx / 4, yy - field.dx / 4, zz - field.dx / 4);
        				fprintf(fp, "v %lf %lf %lf\n", xx - field.dx / 4, yy + field.dx / 4, zz - field.dx / 4);
        				fprintf(fp, "v %lf %lf %lf\n", xx + field.dx / 4, yy, zz - field.dx / 4);
        				fprintf(fp, "v %lf %lf %lf\n", xx, yy, zz + field.dx / 4);
        				fprintf(fp, "f %d %d %d\n", fcnt * 4 + 1, fcnt * 4 + 2, fcnt * 4 + 3);
        				fprintf(fp, "f %d %d %d\n", fcnt * 4 + 1, fcnt * 4 + 2, fcnt * 4 + 4);
        				fprintf(fp, "f %d %d %d\n", fcnt * 4 + 1, fcnt * 4 + 3, fcnt * 4 + 4);
        				fprintf(fp, "f %d %d %d\n", fcnt * 4 + 2, fcnt * 4 + 3, fcnt * 4 + 4);
        				fcnt++;
        			}
        		}
        	}
        }
        fclose(fp);*/
    }

	FILE* resfp = fopen("res.obj", "w");
	des->renderToObjFile(resfp, 0, getRotationQuaternion(0, V3D(0, 0, 1)), P3D(0, 0, 0));
    std::fclose(resfp);

        
}

bool sweptVolume::detectConnected(GLMesh* mesh)
{
    int N = mesh->getVertexCount();
    // use Disjoint Set to check the connectivity
    DisjointSet disjointSet(N);

    for (int i = 0;i < mesh->getPolyCount();++i)
    {
        GLIndexedTriangle poly = mesh->getTriangle(i);
        disjointSet.connect(poly.indexes[0], poly.indexes[1]);
        disjointSet.connect(poly.indexes[0], poly.indexes[2]);
        disjointSet.connect(poly.indexes[1], poly.indexes[2]);
    }

    int v = disjointSet.getFather(0);
    for (int i = 1;i < N;++i)
        if (disjointSet.getFather(i) != v)
        return false;

    return true;
}
