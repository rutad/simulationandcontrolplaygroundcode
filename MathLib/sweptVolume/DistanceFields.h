#pragma once

#include <GUILib/GLMesh.h>
#include "../Ray.h"
#include <vector>
#include <set>
using namespace std;

/**
	Computes a voxel-based distance field for a given 3d mesh.
*/

typedef vector<double> VD;
typedef vector<VD> VVD;
typedef vector<VVD> VVVD;
typedef vector<VVVD> VVVVD;
class DistanceFields
{
public:
	DistanceFields(GLMesh *mesh);
	~DistanceFields();
	void calculate();
	double getDistanceToPlane(const Ray& ray, const Plane& plane, P3D *closestPtOnRay);
	GLMesh *mesh;
	VD dis;
	VD findTri[6];
	double dx, minX, minY, minZ, maxX, maxY, maxZ;
	int xSum, ySum, zSum, xyzSum;
	void fun(double y, double ny1, double ny2, double nz1, double nz2, double &t1, double &t2);
	bool pointIsInTri(P3D res, P3D p1, P3D p2, P3D p3);
};

