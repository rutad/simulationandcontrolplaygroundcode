#pragma once
#include <vector>

class DisjointSet
{
    std::vector<int> father;

public:
    DisjointSet(int N);

    void connect(int x, int y);

    int getFather(int x);
};

