#include <iostream>
#include <cstdio>
#include <cstring>
#include <map>
#include <cmath>
#include <set>
#include "genMesh.h"
using namespace std;

namespace __genMesh
{
	map<P3D, int> num;
	map<pair<P3D, P3D>, P3D> vis;
	set<set<int> > faces;
	int cnt = 0;
	P3D getpoint_ret, last;
	const int density = 5;
	double maxlen;
	vector<P3D> pointlist;
};
using namespace __genMesh;

V3D genMesh::rotate(P3D a, double t)
{
	Quaternion tmp;
	tmp.setRotationFrom(angle * t, axis);
	V3D ans = V3D(orip + oriq.rotate(tmp.rotate((V3D)a)) + translation * t);
	//if (t > 0.975)
	//	cout << ans << endl << endl;
	return ans;
}

V3D genMesh::gradient(P3D a, double t)
{
	V3D u = V3D(a);
	double s2 = sin(angle*t), c2 = cos(angle*t);
	V3D nxu = axis.cross(u);
	V3D ans1 = (axis.cross(nxu) * s2 + nxu * (2*c2) - u * s2 + axis * (u.dot(axis) * s2)) * (angle / 2);

	double angle1;
	V3D axis1;
	Quaternion tmp;
	tmp.setRotationFrom(angle * t, axis);
	u = tmp.rotate((V3D)a);
	tmp = oriq;
	tmp.getAxisAngle(axis1, angle1);
	s2 = sin(angle1*t), c2 = cos(angle1*t);
	nxu = axis1.cross(u);
	V3D ans2 = (axis1.cross(nxu) * s2 + nxu * (2 * c2) - u * s2 + axis1 * (u.dot(axis1) * s2)) * (angle / 2);
	return V3D(ans1.x()*ans2.x(), ans1.y()*ans2.y(), ans1.z()*ans2.z()) + translation;
}

int find(P3D d, GLMesh *ans)
{
	if (num[d] == 0)
	{
		ans->addVertex(d);
		num[d] = ++cnt;
		pointlist.push_back(d);
	}
	return num[d]-1;
}

int genMesh::getpoint(P3D a, P3D b, P3D c, double u, double v, GLMesh *ans)
{
	V3D q = rotate(c, u), gradientq = gradient(c, u);
	V3D r1 = (rotate(b, u) - q), gradientr1 = (gradient(b, u) - gradientq);
	V3D r2 = (rotate(a, u) - q), gradientr2 = (gradient(a, u) - gradientq);
	V3D r1xr2 = r1.cross(r2);
	{
		double kr2 = gradientr2.dot(r1xr2), kq = gradientq.dot(r1xr2), kr1 = gradientr1.dot(r1xr2);
		double minv = -(kq + kr1) / kr2, maxv = -kq / kr2;
		if (minv > maxv) swap(minv, maxv);
		if (v<minv - 1e-8 || v>maxv + 1e-8) return -1;
		minv = kq / (kr1 - kr2), maxv = (kq + kr1) / (kr1 - kr2);
		if (minv > maxv) swap(minv, maxv);
		if (v<minv - 1e-8 || v>maxv + 1e-8) return -1;
	}
	double m = gradientr1.dot(r1xr2);
	if (fabs(m) > 1e-8)
	{
		P3D be = P3D(q - r1 * (gradientq.dot(r1xr2) / m));
		P3D d = P3D(r2 - r1 * (gradientr2.dot(r1xr2) / m));
		getpoint_ret = be + d * v;
		return find(getpoint_ret, ans);
	}
	return -1;
}

int genMesh::getpoint(P3D a, P3D b, double u, double v, GLMesh *ans)
{
	P3D p = P3D(rotate(a, u));
	P3D q = P3D(rotate(b, u));
	P3D d = p + (q-p)*v;
	return find(d, ans);
}

bool check(int a, int b, int c)
{
	if (a == -1 || b == -1 || c == -1)
		return false;
	if (a == b || b == c || a == c)
		return false;
	set<int> tmp;
	tmp.insert(a);tmp.insert(b);tmp.insert(c);
	if (faces.find(tmp) != faces.end())
		return false;
	faces.insert(tmp);
	return true;
}

bool checkr(double d1, double d2)
{
	return (d1 * d2 < -1e-8);
}

vector<double> doit(vector<double> u, int i, int j, int k)
{
	vector<double> ans;
	ans.push_back(u[i]);ans.push_back(u[j]);ans.push_back(u[k]);
	return ans;
}

void genMesh::subdivide(P3D a, P3D b, P3D c, GLMesh *ans, vector<double> u, vector<double> v, bool flag)
{
	int p[4];
	if (flag)
	{
		p[0] = getpoint(a, b, c, u[0], v[0], ans);
		p[1] = getpoint(a, b, c, u[1], v[1], ans);
		p[2] = getpoint(a, b, c, u[2], v[2], ans);
	}
	else
	{
		p[0] = getpoint(a, b, u[0], v[0], ans);
		p[1] = getpoint(a, b, u[1], v[1], ans);
		p[2] = getpoint(a, b, u[2], v[2], ans);
	}
	if (p[0] == -1 || p[1] == -1 || p[2] == -1)
	{
		if (check(p[0], p[1], p[2])) ans->addPoly(GLIndexedTriangle(p[0], p[1], p[2]));
		return;
	}
	u.push_back((u[0] + u[1]) / 2); v.push_back((v[0] + v[1]) / 2);
	u.push_back((u[1] + u[2]) / 2); v.push_back((v[1] + v[2]) / 2);
	u.push_back((u[2] + u[0]) / 2); v.push_back((v[2] + v[0]) / 2);
	bool b3, b4, b5;
	b3 = ((pointlist[p[0]] - pointlist[p[1]]).length() < maxlen);
	b4 = ((pointlist[p[1]] - pointlist[p[2]]).length() < maxlen);
	b5 = ((pointlist[p[2]] - pointlist[p[0]]).length() < maxlen);
	vector<double> u1, v1;
	switch (b3 * 4 + b4 * 2 + b5)
	{
	case 7: //nobreak
		if (check(p[0], p[1], p[2])) ans->addPoly(GLIndexedTriangle(p[0], p[1], p[2]));
		return;
	case 0: //allbreak
		u1 = doit(u, 3, 4, 5); v1 = doit(v, 3, 4, 5); subdivide(a, b, c, ans, u1, v1, flag);
		u1 = doit(u, 3, 4, 1); v1 = doit(v, 3, 4, 1); subdivide(a, b, c, ans, u1, v1, flag);
		u1 = doit(u, 3, 0, 5); v1 = doit(v, 3, 0, 5); subdivide(a, b, c, ans, u1, v1, flag);
		u1 = doit(u, 2, 4, 5); v1 = doit(v, 2, 4, 5); subdivide(a, b, c, ans, u1, v1, flag);
		return;
	case 1: //34break
		u1 = doit(u, 3, 4, 1); v1 = doit(v, 3, 4, 1); subdivide(a, b, c, ans, u1, v1, flag);
		u1 = doit(u, 3, 4, 0); v1 = doit(v, 3, 4, 0); subdivide(a, b, c, ans, u1, v1, flag);
		u1 = doit(u, 2, 4, 0); v1 = doit(v, 2, 4, 0); subdivide(a, b, c, ans, u1, v1, flag);
		return;
	case 2: //35break
		u1 = doit(u, 3, 5, 1); v1 = doit(v, 3, 5, 1); subdivide(a, b, c, ans, u1, v1, flag);
		u1 = doit(u, 3, 5, 0); v1 = doit(v, 3, 5, 0); subdivide(a, b, c, ans, u1, v1, flag);
		u1 = doit(u, 2, 5, 1); v1 = doit(v, 2, 5, 1); subdivide(a, b, c, ans, u1, v1, flag);
		return;
	case 4: //45break
		u1 = doit(u, 5, 4, 1); v1 = doit(v, 5, 4, 1); subdivide(a, b, c, ans, u1, v1, flag);
		u1 = doit(u, 5, 4, 2); v1 = doit(v, 5, 4, 2); subdivide(a, b, c, ans, u1, v1, flag);
		u1 = doit(u, 1, 5, 0); v1 = doit(v, 1, 5, 0); subdivide(a, b, c, ans, u1, v1, flag);
		return;
	case 3: //3break
		u1 = doit(u, 3, 2, 1); v1 = doit(v, 3, 2, 1); subdivide(a, b, c, ans, u1, v1, flag);
		u1 = doit(u, 3, 2, 0); v1 = doit(v, 3, 2, 0); subdivide(a, b, c, ans, u1, v1, flag);
		return;
	case 5: //4break
		u1 = doit(u, 4, 0, 1); v1 = doit(v, 4, 0, 1); subdivide(a, b, c, ans, u1, v1, flag);
		u1 = doit(u, 4, 0, 2); v1 = doit(v, 4, 0, 2); subdivide(a, b, c, ans, u1, v1, flag);
		return;
	case 6: //5break
		u1 = doit(u, 5, 1, 0); v1 = doit(v, 5, 1, 0); subdivide(a, b, c, ans, u1, v1, flag);
		u1 = doit(u, 5, 1, 2); v1 = doit(v, 5, 1, 2); subdivide(a, b, c, ans, u1, v1, flag);
		return;
	}
}

void genMesh::make_r(P3D a, P3D b, GLMesh *ans, P3D c)
{
	if (vis.find(make_pair(a, b)) == vis.end())
	{
		vis[make_pair(a, b)] = c;
		vis[make_pair(b, a)] = c;
		return;
	}
	//printf("hrere!!!\n");
	//if (0)
	{
		V3D r1 = (a - b), r2 = (vis[make_pair(a, b)] - a), r3 = (c - a);
		r1.normalize();r2.normalize();r3.normalize();
		V3D n1 = r1.cross(r2), n2 = -r1.cross(r3);
		double res = n2.cross(n1).dot(-r1);
		if (res < 1e-8)
			return;
	}
	vector<double> u, v;
	u.push_back(0);u.push_back(0);u.push_back(1);
	v.push_back(0);v.push_back(1);v.push_back(0);
	subdivide(a, b, c, ans, u, v, false);
	u[0] = 1, v[0] = 1;
	subdivide(a, b, c, ans, u, v, false);
}

void genMesh::make_d(P3D a, P3D b, P3D c, GLMesh *ans)
{
	make_r(a, b, ans, c);
	make_r(b, c, ans, a);
	make_r(c, a, ans, b);
	int p[4];
	vector<double> u, v;
	u.push_back(0);u.push_back(0);u.push_back(1);
	v.push_back(0);v.push_back(1);v.push_back(0);
	subdivide(a, b, c, ans, u, v, true);
	u[0] = 1, v[0] = 1;
	subdivide(a, b, c, ans, u, v, true);
	p[0] = find(P3D(rotate(a, 0)), ans), p[1] = find(P3D(rotate(b, 0)), ans), p[2] = find(P3D(rotate(c, 0)), ans);
	if (check(p[0], p[1], p[2]))
		ans->addPoly(GLIndexedTriangle(p[0], p[1], p[2]));
	p[0] = find(P3D(rotate(a, 1)), ans), p[1] = find(P3D(rotate(b, 1)), ans), p[2] = find(P3D(rotate(c, 1)), ans);
	if (check(p[0], p[1], p[2]))
		ans->addPoly(GLIndexedTriangle(p[0], p[1], p[2]));
}

GLMesh *genMesh::genRotateMesh(GLMesh *ori)
{
	GLMesh *ans = new GLMesh();
	maxlen = 0;
	for (int i = 0; i < ori->getVertexCount(); ++i)
	{
		P3D tmp = ori->getVertex(i);
		if (maxlen < fabs(tmp.x())) maxlen = fabs(tmp.x());
		if (maxlen < fabs(tmp.y())) maxlen = fabs(tmp.y());
		if (maxlen < fabs(tmp.z())) maxlen = fabs(tmp.z());
	}
	maxlen *= 0.1;
	for (int i = 0; i < ori->getPolyCount(); ++i)
	{
		GLIndexedTriangle &tri = ori->getTriangle(i);
		make_d(ori->getVertex(tri.indexes[0]), ori->getVertex(tri.indexes[1]), ori->getVertex(tri.indexes[2]), ans);
	}
	return ans;
}

void genMesh::loadTrajectory(const Quaternion &q0, const Quaternion &q1, const P3D &p0, const P3D &p1)
{
	rotation = q1*q0.getInverse();
	oriq = q0;
	translation = p1-p0;
	orip = p0;
	rotation.getAxisAngle(axis, angle);
}
