#pragma once

#include <OptimizationLib/ObjectiveFunction.h>

#include <OptimizationLib/GradientDescentFunctionMinimizer.h>
#include <OptimizationLib/NewtonFunctionMinimizer.h>
#include <OptimizationLib/BFGSFunctionMinimizer.h>
#include <OptimizationLib/CMAFunctionMinimizer.h>

class Calder;

class StaticEquilibriumConstraint : public ObjectiveFunction {
public:

	bool addMassParams = true;
	bool addLenParams = true;

	StaticEquilibriumConstraint(Calder* c) { this->calder = c; }
	virtual ~StaticEquilibriumConstraint(void) {
	}

	double setupConstraint();

	//regularizer looks like: r/2 * (p-p0)'*(p-p0). This function can update p0 if desired, given the current value of s.
	void updateRegularizingSolutionTo(const dVector &currentP){ m_p0 = currentP; }
	virtual double computeValue(const dVector& p);

	//this method gets called whenever a new best solution to the objective function is found
	virtual void setCurrentBestSolution(const dVector& p) {};

	// analytical gradient of static equilibrium constraint wrt mass and length params
	virtual void addGradientTo(dVector& grad, const dVector& p);

private:

	dVector m_p0;
	dVector tmpVec;
	double regularizer = 0.01;
	Calder* calder;
};