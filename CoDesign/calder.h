#pragma once

#include <GUILib/GLMesh.h>
#include <MathLib/mathLib.h>
#include <MathLib/P3D.h>
#include <MathLib/V3D.h>
#include <MathLib/Quaternion.h>
#include <MathLib/Ray.h>
#include <GUILib/TranslateWidget.h>
#include <GUILib/RotateWidget.h>
#include "CalderLeafBaseObject.h"

#include <OptimizationLib\ObjectiveFunction.h>
#include "StaticEquilibriumConstraint.h"

/*
Calder class represents a paramterized design of a calder
*/
class Calder {
public:
	P3D openLeafPos = P3D(0, 0, 0);
	DynamicArray<BranchingLeaf*> leaves;
	TerminalLeaf* lastLeaf = NULL;
	DynamicArray<double> paramList;

	StaticEquilibriumConstraint* SEConstraint = NULL;

public:
	// constructor
	Calder() {
		SEConstraint = new StaticEquilibriumConstraint(this);
	}

	// destructor
	~Calder() {
		if(lastLeaf)
			delete lastLeaf;
		while (!leaves.empty()) {
			delete leaves.back();
			leaves.pop_back();
		}
	};

	// writes obj's current parameters to 'params' so that optimizer can use and optimize them.
	void writeCurrentParametersToList(dVector& params, bool addMass, bool addLen) {
		paramList.clear();
		for (uint i = 0; i < leaves.size(); i++) {
			leaves[i]->setParameterStartIndex(paramList.size());
			leaves.at(i)->pushDesignParametersToList(paramList, addMass, addLen);
		}
		lastLeaf->setParameterStartIndex(paramList.size());
		lastLeaf->pushDesignParametersToList(paramList, addMass, addLen);

		resize(params, paramList.size());
		for (uint i = 0; i < paramList.size(); i++)
			params[i] = paramList[i];
	}

	// set obj's parameters from 'params'
	void setParametersFromList(const dVector& params, bool addMass, bool addLen) {
		paramList.clear();
		for (int i = 0; i < params.size(); i++)
			paramList.push_back(params[i]);

		for (uint i = 0; i < leaves.size(); i++) {
			int paramIndex = leaves[i]->getParameterStartIndex();
			leaves.at(i)->readDesignParametersFromList(paramList, paramIndex, addMass, addLen);
		}
		int paramInd = lastLeaf->getParameterStartIndex();
		lastLeaf->readDesignParametersFromList(paramList, paramInd, addMass, addLen);
	}

};
