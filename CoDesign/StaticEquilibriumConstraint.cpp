#include "StaticEquilibriumConstraint.h"
#include "CalderLeafBaseObject.h"
#include "calder.h"

double StaticEquilibriumConstraint::setupConstraint() {
	
	double netTorque = 0;

	for (size_t i = 0; i < calder->leaves.size(); i++)
	{
		netTorque += calder->leaves[i]->getTorque();
	}
	return netTorque;
}

double StaticEquilibriumConstraint::computeValue(const dVector& p) {

	calder->setParametersFromList(p, addMassParams, addLenParams);
	double totalEnergy = setupConstraint();
	Logger::consolePrint("net t:%lf\n", totalEnergy);
	totalEnergy = totalEnergy*totalEnergy;

	////add the regularizer contribution
	//if (regularizer > 0) {
	//	resize(tmpVec, p.size());
	//	if (m_p0.size() != p.size()) m_p0 = p;
	//	tmpVec = p - m_p0;
	//	totalEnergy += 0.5*regularizer*tmpVec.dot(tmpVec);
	//}

	return totalEnergy;
}

void StaticEquilibriumConstraint::addGradientTo(dVector& grad, const dVector& p) {

	calder->setParametersFromList(p, addMassParams, addLenParams);
	double netTorque = setupConstraint();

	grad = dVector(p.size());
	int count = 0;
	for (size_t i = 0; i < calder->leaves.size(); i++)
	{
		dVector partialGrad = calder->leaves[i]->getTorqueAnalyticalDerivative(addMassParams, addLenParams);
		for (auto j = 0; j < partialGrad.size(); j++)
		{
			grad[count] = 2 * (netTorque)* partialGrad[j];
			Logger::consolePrint("p(%d):%lf, p.grad(%d):%lf \n", count, p[count], j, partialGrad[j]);
			count = count + 1;
		}
	}

	// contribution from terminal leaf
	dVector partialGradTerminal = calder->lastLeaf->getTorqueAnalyticalDerivative(addMassParams, addLenParams);
	for (auto j = 0; j < partialGradTerminal.size(); j++)
	{
		grad[count] = 2 * (netTorque)* partialGradTerminal[j];
		Logger::consolePrint("p(%d):%lf, p.grad(%d):%lf \n", count, p[count], j, partialGradTerminal[j]);
		count = count + 1;
	}
}