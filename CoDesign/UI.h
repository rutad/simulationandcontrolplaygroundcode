#pragma once

#include <GUILib/GLApplication.h>
#include <GUILib/GLWindow3D.h>
#include <GUILib/GLWindowContainer.h>
#include <GUILib/GLMesh.h>
#include <GUILib/TranslateWidget.h>
#include <GUILib/RotateWidget.h>
#include "calder.h"
#include "CalderLeafBaseObject.h"

#include <OptimizationLib\ObjectiveFunction.h>


/**
* Test UI for Assembly-aware design project
*/
class UI : public GLApplication {
public:
	// constructor
	UI();
	// destructor
	virtual ~UI(void);
	// Run the App tasks
	virtual void process();
	// Draw the App scene - camera transformations, lighting, shadows, reflections, etc apply to everything drawn by this method
	virtual void drawScene();
	// This is the wild west of drawing - things that want to ignore depth buffer, camera transformations, etc. Not pretty, quite hacky, but flexible. Individual apps should be careful with implementing this method. It always gets called right at the end of the draw function
	virtual void drawAuxiliarySceneInfo();
	// Restart the application.
	virtual void restart();

	bool drawBulletCPs = false;
	bool drawClosestPointPairs = false;

	// plots for cost function
	//	bool drawPlots = false;
	//	PlotWindow* plotWindow = NULL;

	// Translate Widget
	TranslateWidget* tWidget;

	// assembly that is being built in the scene
	Calder mainCalder;
	CalderLeafBaseObject* selectedObject = NULL;
	BranchingLeaf* childSeekingBranch = NULL;

	// leaf property params
	int branchId = 1;
	double lx = 0.1;
	double ly = 0.05;

	//input callbacks...
	//all these methods should returns true if the event is processed, false otherwise...
	//any time a physical key is pressed, this event will trigger. Useful for reading off special keys...
	virtual bool onKeyEvent(int key, int action, int mods);
	//this one gets triggered on UNICODE characters only...
	virtual bool onCharacterPressedEvent(int key, int mods);
	//triggered when mouse buttons are pressed
	virtual bool onMouseButtonEvent(int button, int action, int mods, double xPos, double yPos);
	//triggered when mouse moves
	virtual bool onMouseMoveEvent(double xPos, double yPos);
	//triggered when using the mouse wheel
	virtual bool onMouseWheelScrollEvent(double xOffset, double yOffset);

	virtual bool processCommandLine(const std::string& cmdLine);

	// should be used for loading chassis?
	void loadFile(const char* fName);
	void saveFile(const char* fName);

};


void TW_CALL UIButtonEventAddLeaves(void* clientData);
void TW_CALL UIButtonEventAddTerminalLeaf(void* clientData);
void TW_CALL UIButtonEventDeleteTerminalLeaf(void* clientData);