#include "UI.h"
#include <GUILib/GLContentManager.h>
#include <GUILib/GLMesh.h>
#include <GUILib/GLMeshDisplayWindow.h>
#include <GUILib/GLWindowContainer.h>
#include <OptimizationLib/NewtonFunctionMinimizer.h>
#include <GUILib\GLTrackingCamera.h>
#include <GUILib/RotateWidgetV1.h>
#include <GUILib/RotateWidgetV2.h>
#include <GUILib/GLUtils.h>
#include <GUILib/PlotWindow.h>
#include <MathLib/Segment.h>
#include <GUILib/GLUtils.h>


//TODO:

UI::UI() {
	camera = new GLTrackingCamera();

	((GLTrackingCamera*)camera)->camDistance = -1;

	setWindowTitle("Co-Design Test");
	bgColor[0] = bgColor[1] = bgColor[2] = 1;

	showGroundPlane = false;
	showDesignEnvironmentBox = true;
	designEnvironmentScale = 1.0;

	TwAddSeparator(mainMenuBar, "sep3", "");

	TwAddVarRW(mainMenuBar, "Bullet CPs", TW_TYPE_BOOLCPP, &drawBulletCPs, " label='Draw Bullet Debug Info' group='Viz' key='b'");
	TwAddVarRW(mainMenuBar, "Bullet Closest Point Pairs", TW_TYPE_BOOLCPP, &drawClosestPointPairs, " label='Draw Closest Point Pairs Info' group='Viz' key='p'");

	TwAddButton(mainMenuBar, "Add leaf to calder", NULL, NULL, " ");
	TwAddButton(mainMenuBar, "Add branching leaf", UIButtonEventAddLeaves, "add", "group = 'leaves'");
	TwAddVarRW(mainMenuBar, "branch lx", TW_TYPE_DOUBLE, &lx, " min=0 max=0.9 step=0.01 group = 'leaves' ");
	TwAddVarRW(mainMenuBar, "branch ly", TW_TYPE_DOUBLE, &ly, " min=0 max=0.9 step=0.01 group = 'leaves' ");
	TwAddVarRW(mainMenuBar, "branch id", TW_TYPE_INT32, &branchId, " min=0 max=1 step=1 group = 'leaves' ");
	TwAddButton(mainMenuBar, "Add terminal leaf", UIButtonEventAddTerminalLeaf, "addTerminal", "group = 'leaves'");
	TwAddButton(mainMenuBar, "Delete terminal leaf", UIButtonEventDeleteTerminalLeaf, "deleteTerminal", "group = 'leaves'");

	//	TwAddVarRW(mainMenuBar, "Cost function plots", TW_TYPE_BOOLCPP, &drawPlots, " label='Draw cost plots' group='Viz'");

	tWidget = new TranslateWidget(AXIS_X | AXIS_Y | AXIS_Z);

	/*
	plotWindow = new PlotWindow(260 * 1.14, mainWindowHeight - 400, 500, 400);
	plotWindow->createData("optValues", 1, 0, 0);
	plotWindow->createData("bestValue", 0, 1, 0);
	plotWindow->createData("best10", 0, 0, 1);
	*/
}

UI::~UI(void) {
}

//triggered when mouse moves
bool UI::onMouseMoveEvent(double xPos, double yPos) {
	if (tWidget->onMouseMoveEvent(xPos, yPos) == true) return true;

	if (GLApplication::onMouseMoveEvent(xPos, yPos) == true)
		return true;

	return false;
}

//triggered when using the mouse wheel
bool UI::onMouseWheelScrollEvent(double xOffset, double yOffset) {
	if (GLApplication::onMouseWheelScrollEvent(xOffset, yOffset)) return true;

	return false;
}

void UI::loadFile(const char* fName) {

}

void UI::saveFile(const char* fName) {
	Logger::consolePrint("SAVE FILE: Do not know what to do with file \'%s\'\n", fName);
}

bool UI::onKeyEvent(int key, int action, int mods) {

	if (GLApplication::onKeyEvent(key, action, mods)) return true;

	return false;
}

bool UI::onCharacterPressedEvent(int key, int mods) {

	if (GLApplication::onCharacterPressedEvent(key, mods)) return true;

	return false;
}

//triggered when mouse buttons are pressed
bool UI::onMouseButtonEvent(int button, int action, int mods, double xPos, double yPos) {
	if (tWidget->isPicked()) return true;
	if (GLApplication::onMouseButtonEvent(button, action, mods, xPos, yPos)) return true;

	Ray clickedRay = getRayFromScreenCoords(xPos, yPos);
	bool foundSelectedObj = false;

	for (size_t i = 0; i < mainCalder.leaves.size(); i++)
	{
		foundSelectedObj = mainCalder.leaves[i]->pickWith(clickedRay, tWidget);
		if (foundSelectedObj) {
			selectedObject = mainCalder.leaves[i];
			break;
		}
	}

	if (!foundSelectedObj) {
		if (mainCalder.lastLeaf) {
			foundSelectedObj = mainCalder.lastLeaf->pickWith(clickedRay, tWidget);
			if (foundSelectedObj) {
				selectedObject = mainCalder.lastLeaf;
			}
		}
	}

	if (!foundSelectedObj)
		selectedObject = NULL;

	return false;
}


// Run the App tasks
void UI::process() {
	dVector p;
	dVector C;
	mainCalder.writeCurrentParametersToList(p, true, true);
	mainCalder.SEConstraint->computeValue(p);
	mainCalder.SEConstraint->addGradientTo(C, p);

	// checking analytical gradient with FD
	//dVector check;
	//mainCalder.SEConstraint->addEstimatedGradientTo(check, p);
	//for (size_t i = 0; i < C.size(); i++)
	//{
	//	Logger::consolePrint("c(%d):%lf, check(%d):%lf\n", i, C[i], i, check[i]);
	//}

	DynamicArray<MTriplet> delCDelM, delCDelL, delMdelL;
	dVector pMass, pLen;

	mainCalder.writeCurrentParametersToList(pMass, true, false);
	mainCalder.SEConstraint->addMassParams = true; mainCalder.SEConstraint->addLenParams = false;
	mainCalder.SEConstraint->addEstimatedHessianEntriesTo(delCDelM, pMass);

	mainCalder.writeCurrentParametersToList(pLen, false, true);
	mainCalder.SEConstraint->addMassParams = false; mainCalder.SEConstraint->addLenParams = true;
	mainCalder.SEConstraint->addEstimatedHessianEntriesTo(delCDelL, pLen);

	SparseMatrix CByM, CByL, jacobian;
	CByL.setFromTriplets(delCDelL.begin(), delCDelL.end());
	CByM.setFromTriplets(delCDelM.begin(), delCDelM.end());

	Eigen::SimplicialLDLT<SparseMatrix, Eigen::Lower> solver;
	//Eigen::SparseLU<SparseMatrix> solver;
	solver.compute(CByM);
	jacobian = solver.solve(CByL);

}

// Draw the App scene - camera transformations, lighting, shadows, reflections, etc apply to everything drawn by this method
void UI::drawScene() {

	if (selectedObject) {
		tWidget->visible = true;
		selectedObject->setObjectStateFromWidgets(tWidget);
	}
	else {
		tWidget->visible  = false;
	}

	if(childSeekingBranch)
		mainCalder.openLeafPos = childSeekingBranch->getChildLeafPosition();

	glEnable(GL_LIGHTING);

	for (size_t i = 0; i < mainCalder.leaves.size(); i++)
	{
		mainCalder.leaves[i]->setLeafPosition();
		mainCalder.leaves[i]->draw();
		mainCalder.leaves[i]->drawBranches();
	}

	// draw last leaf
	if(mainCalder.lastLeaf)
		mainCalder.lastLeaf->draw();

	// draw hanger hook
	drawCapsule(P3D(0, 0, 0), P3D(0, 0.1, 0), 0.005);
}

// This is the wild west of drawing - things that want to ignore depth buffer, camera transformations, etc. Not pretty, quite hacky, but flexible. Individual apps should be careful with implementing this method. It always gets called right at the end of the draw function
void UI::drawAuxiliarySceneInfo() {
	//clear the depth buffer so that the widgets show up on top of the object primitives
	glClear(GL_DEPTH_BUFFER_BIT);
	glDisable(GL_TEXTURE_2D);

	// draw the widgets
	tWidget->draw();

	// plots of cost function
	//	if (drawPlots)
	//		plotWindow->draw();

}

// Restart the application.
void UI::restart() {
	if (selectedObject) {
		selectedObject = NULL;
	}

	while (!mainCalder.leaves.empty()) {
		delete mainCalder.leaves.back();
		mainCalder.leaves.pop_back();
	}

	delete mainCalder.lastLeaf;
	mainCalder.lastLeaf = NULL;
	mainCalder.openLeafPos = P3D(0, 0, 0);
	childSeekingBranch = NULL;

	//mainAssembly.constraintsManager->constraintsCollection->softConstraints.clear();
}

bool UI::processCommandLine(const std::string& cmdLine) {

	if (strcmp(cmdLine.c_str(), "delete") == 0) {
		return true;
	}

	if (strcmp(cmdLine.c_str(), "save") == 0) {
		return true;
	}

	if (strcmp(cmdLine.c_str(), "load") == 0) {
		return true;
	}

	if (GLApplication::processCommandLine(cmdLine)) return true;

	return false;
}

void TW_CALL UIButtonEventAddLeaves(void* clientData) {
	if (strcmp((char*)clientData, "add") == 0) {
		UI* app = dynamic_cast<UI*>(GLApplication::getGLAppInstance());
		if (app) {
			BranchingLeaf* newLeaf = new BranchingLeaf(app->lx, app->ly, app->lx, app->ly, app->branchId, NULL);
			if (app->mainCalder.leaves.size() > 0) {
				app->mainCalder.leaves.back()->child = newLeaf;
				newLeaf->parentBranch = app->mainCalder.leaves.back();
			}

			app->mainCalder.leaves.push_back(newLeaf);
			app->mainCalder.leaves.back()->leafOrigin = app->mainCalder.openLeafPos;
			app->mainCalder.openLeafPos = app->mainCalder.leaves.back()->getChildLeafPosition();
			app->childSeekingBranch = app->mainCalder.leaves.back();
		}
	}
}

void TW_CALL UIButtonEventAddTerminalLeaf(void* clientData) {
	if (strcmp((char*)clientData, "addTerminal") == 0) {
		UI* app = dynamic_cast<UI*>(GLApplication::getGLAppInstance());
		if (app) {
			if (!app->mainCalder.lastLeaf) {
				app->mainCalder.lastLeaf = new TerminalLeaf(app->childSeekingBranch);
				if(app->childSeekingBranch)
					app->childSeekingBranch->child = app->mainCalder.lastLeaf;
			}
			else {
				Logger::consolePrint("calder can have only one terminal leaf!\n");
			}
		}
	}
}

void TW_CALL UIButtonEventDeleteTerminalLeaf(void* clientData) {
	if (strcmp((char*)clientData, "deleteTerminal") == 0) {
		UI* app = dynamic_cast<UI*>(GLApplication::getGLAppInstance());
		if (app) {
			if (app->mainCalder.lastLeaf) {
				if (app->selectedObject == app->mainCalder.lastLeaf) {
					app->selectedObject = NULL;
				}
				delete app->mainCalder.lastLeaf;
				app->mainCalder.lastLeaf = NULL;
			}
			else {
				Logger::consolePrint("no terminal leaf found!\n");
			}
		}
	}
}








