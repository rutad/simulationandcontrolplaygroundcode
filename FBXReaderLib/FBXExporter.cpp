
#include <FBXReaderLib/FBXExporter.h>

#include <FBXReaderLib\Common\Common.h>
#include <ControlLib/Robot.h>
#include <RBSimLib/Joint.h>
#include <RBSimLib/RigidBody.h>
#include <ControlLib/AnimatedRobot.h>


///< It imports all the necessary information for drawing the scene from a FBX file
FBXExporter::FBXExporter()//std::string fileName, double _mocapSamplingRate
{	

}

///<
FBXExporter::~FBXExporter()
{
	release();
}

///<
void FBXExporter::release()
{
	if (m_pFbxManager)
		m_pFbxManager->Destroy();

	m_pFbxManager = NULL;
	m_pScene = NULL;
	m_pRootNode = NULL;
}


FbxVector4 toFbxVec4(float* _pFloats)
{
	FbxVector4 f(_pFloats[0], _pFloats[1], _pFloats[2], _pFloats[3]);
	return f;
}

#include <FBXReaderLib\FBXImporter.h>

FbxNode* FBXExporter::findNode(const char* _csBoneName)
{
	FbxNode* pNode = NULL;

	FbxNode* pSearch = m_pRootNode;



	return NULL;
	
}

// Write the skeleton to fbx
void FBXExporter::skeletonToFbx(Skeleton& _skToExport, Skeleton* _pSrcSK, SkinnedMesh* _pMeshToExport, FBXImporter* _pImportedMesh, double animationTime, std::string fileName)
{
	m_sceneName = fileName;

	// 
	//void InitializeSdkObjects(FbxManager*& m_pFbxManager, FbxScene*& m_pScene);
	FBXCommon::InitializeSdkObjects(m_pFbxManager, m_pScene);
	
	// set scene info
	//FbxDocumentInfo* Create(FbxManager *pManager, const char *pName);
	FbxDocumentInfo* sceneInfo = FbxDocumentInfo::Create(m_pFbxManager, "SceneInfo");
	sceneInfo->mTitle = "title";
	sceneInfo->mSubject = "subject";
	sceneInfo->mAuthor = "author";
	sceneInfo->mRevision = "revision";
	sceneInfo->mKeywords = "keywords";
	sceneInfo->mComment = "comment";

	m_pScene->SetSceneInfo(sceneInfo);

	// build the node tree for the skeleton
	Skeleton::Joint* currJoint = _skToExport.m_pRoot;
	FbxNode* skeletonRootNode = jointToNode(m_pScene, *currJoint);
	addChildNodes(m_pScene, *currJoint, skeletonRootNode);

	// add the skeleton to the scene root node
	FbxNode* pRootNode = (FbxNode*)m_pScene->GetRootNode();
	pRootNode->AddChild(skeletonRootNode);

	// now that we have the skeleton, we can set the animation
	animateSkeleton(m_pScene, skeletonRootNode, _skToExport, animationTime);

	// do we also have to export meshes?
	if (_pMeshToExport)
	{
		

		//SkinnedMesh fmesh;
		const char* csMeshName = "SimMesh";
		FbxMesh* pDestMesh = FbxMesh::Create(m_pScene, csMeshName);

		// Create control points.
		int numVertices = _pMeshToExport->m_sourceVertices.size();
		pDestMesh->InitControlPoints(numVertices);

		FbxGeometryElementNormal* pNormals = pDestMesh->CreateElementNormal();
		pNormals->SetMappingMode(FbxGeometryElement::eByControlPoint);
		pNormals->SetReferenceMode(FbxGeometryElement::eDirect);

		//_pImportedMesh->
		
		FbxVector4* pVertices = pDestMesh->GetControlPoints();
		for (int i = 0; i < numVertices; ++i)
		{
			
			FbxVector4 v = toFbxVec4(_pMeshToExport->m_sourceVertices[i].m_x);
			FbxVector4 n = toFbxVec4(_pMeshToExport->m_sourceVertices[i].m_n);

			pVertices[i] = v;
			
			pNormals->GetDirectArray().Add(n);
		}

	
		int numVerticesPerFace = 3;
		int numPolygons = _pMeshToExport->m_indexArray.size()/ numVerticesPerFace;
		for (int i = 0; i < numPolygons; ++i)
		{
			pDestMesh->BeginPolygon();
			{
				for (int k = 0; k<numVerticesPerFace; ++k)
					pDestMesh->AddPolygon(_pMeshToExport->m_indexArray[i * 3 + k]);
			}	
			pDestMesh->EndPolygon();
		}


		//Still work to do with robot mesh rig...

		///< Copy skinning
		{
			printf("Exporting Bone weights...\n");
			
			FbxDeformer* pDeformer = FbxDeformer::Create(m_pScene, "Deformer");
			
			pDestMesh->AddDeformer(pDeformer);

			int numOfDeformers = pDestMesh->GetDeformerCount();

			//const char* csDeformerName="De"
			FbxSkin* pNewSkin = FbxSkin::Create(pDeformer, "Skin");

			
			//(FbxSkin*)pDestMesh->GetDeformer(0, FbxDeformer::eSkin);

			///< Essentially copy the original mapping to the new mesh...
			FbxSkin* pSrcDeformer = _pImportedMesh->getSkinDeformer();
			int iClusterCount = pSrcDeformer->GetClusterCount();
	
			for (int j = 0; j < iClusterCount; ++j)
			{
				std::stringstream ssNamme;

				ssNamme << "cluster" << j;
				FbxCluster* pDesCluster = FbxCluster::Create(pNewSkin, ssNamme.str().c_str());
				pNewSkin->AddCluster(pDesCluster);

				///< This code should be encapsulated:
				FbxCluster* pSrcCluster = pSrcDeformer->GetCluster(j);				
				const char* csBoneName	= pSrcCluster->GetLink()->GetName();
				Skeleton::Joint* pSrcJoint = _pSrcSK->FindJoint(csBoneName);
				int jointId = pSrcJoint->m_id;

				std::string mappedName = _skToExport.getJoint(jointId)->m_strName;				
				FbxNode* pBoneNode = m_pScene->FindNodeByName(mappedName.c_str());
				pDesCluster->SetLink(pBoneNode);				
				
				int numVerticesAffectedByBone_j = pSrcCluster->GetControlPointIndicesCount();
				pDesCluster->SetControlPointIWCount(numVerticesAffectedByBone_j);
				
				{
					int*	srcBoneVertexIndices = pSrcCluster->GetControlPointIndices();
					double* srcBoneVertexWeights = pSrcCluster->GetControlPointWeights();

					int*	desBoneVertexIndices = pDesCluster->GetControlPointIndices();
					double* desBoneVertexWeights = pDesCluster->GetControlPointWeights();

					// For a given bone, find the vertices affected by it
					for (int l = 0; l < numVerticesAffectedByBone_j; ++l)
					{
						desBoneVertexIndices[l] = srcBoneVertexIndices[l];
						
						desBoneVertexWeights[l] = srcBoneVertexWeights[l];
						
						//int vertIndex = boneVertexIndices[l];
					}
				}
				
			}

			

			
		}


		// Attach the mesh to a node
		FbxNode* pMeshNode = FbxNode::Create(m_pScene, csMeshName);
		pMeshNode->SetNodeAttribute(pDestMesh);

		// Set this node as a child of the root node
		m_pScene->GetRootNode()->AddChild(pMeshNode);	
	}


	// and then save the scene
	//bool SaveScene(FbxManager* pManager, FbxDocument* pScene, const char* pFilename, int pFileFormat = -1, bool pEmbedMedia = false);
	bool result = FBXCommon::SaveScene(m_pFbxManager, m_pScene, fileName.c_str());
	if (result)
		Logger::consolePrint("Succeeded at saving the character as an FBX. \n");
	else
		Logger::consolePrint("Failed at saving the character as an FBX. \n");
		
}

double FBXExporter::m_spaceScalingFactor = 100.0;

///<
void FBXExporter::rbsAnimationToFbx(Robot* robot, AnimatedRobot* _pAnimationToExport, Skeleton* _pSrcSK, SkinnedMesh* _pMesh, FBXImporter* _pImportedMesh, double animationTime, std::string fileName, int numKeyFrames)
{

	Skeleton skeleton;

	// first create the skeleton structure
	ReducedRobotState* initialState = _pAnimationToExport->getInterpolatedFrame(0.0);
	robot->setState(initialState);
	rbsToSkeleton(robot, initialState, skeleton);

	// then set up the animation
	double step = 1.0 / (double)(numKeyFrames - 1);
	for (int i = 0; i < numKeyFrames; ++i) {
		double time = i * step;
		ReducedRobotState* currentState = _pAnimationToExport->getInterpolatedFrame(time);

		// world position of root
		skeleton.m_X.push_back(currentState->getPosition() * m_spaceScalingFactor);

		// relative orientation of each joint
		skeleton.m_Q.push_back(std::vector<Quaternion>(robot->getJointCount() + 1));
		skeleton.m_Q[i][0] = currentState->getOrientation();
		for (int j = 0; j < robot->getJointCount(); ++j) {
			skeleton.m_Q[i][j + 1] = currentState->getJointRelativeOrientation(j);
		}
	}
	
	// then save the skeleton as fbx

	skeletonToFbx(skeleton, _pSrcSK, _pMesh, _pImportedMesh, animationTime, fileName);
}


void FBXExporter::rbsToSkeleton(Robot* robot, ReducedRobotState* rbs, Skeleton& sk) {
	// need root world position and world orientation
	P3D rootWorldPos = rbs->getPosition();
	Quaternion rootWorldOrientation = rbs->getOrientation();

	// set up root
	sk.m_pRoot = new Skeleton::Joint();
	sk.m_pRoot->m_strName = robot->root->name;
	sk.m_pRoot->m_q = rootWorldOrientation;
	sk.m_pRoot->m_offset = rootWorldPos;
	sk.m_pRoot->m_id = 0;
	sk.m_pRoot->m_pPrevious = NULL;

	RigidBody* rigidBodyRoot = robot->root;
	
	// build skeleton structure (also sets name and id)
	int numChildJoints = rigidBodyRoot->cJoints.size();
	sk.m_pRoot->m_childs.resize(numChildJoints);
	for (int i = 0; i < numChildJoints; ++i) {
		sk.m_pRoot->m_childs[i].m_pPrevious = sk.m_pRoot;
		buildSkeleton(&sk.m_pRoot->m_childs[i], rigidBodyRoot->cJoints[i]);
	}	

	// need joint relative positions and orientations
	int numJoints = rbs->getJointCount();
	for (int i = 0; i < numJoints; ++i) {
		Quaternion relQ = rbs->getJointRelativeOrientation(i);

		Skeleton::Joint* j = sk.getJoint(i + 1); // +1 because 0 is root
		j->m_q = relQ;
	}

	// for offset set all joint relative orientations to zero and then retrieve offsets in world space
	rbs->setJointsToZero();
	robot->setState(rbs);
	for (int i = 0; i < numJoints; ++i) {
		Joint* joint = robot->getJoint(i);
		RigidBody* parentBody = joint->parent;
		V3D dist;
		if (parentBody == rigidBodyRoot) {
			dist = joint->getWorldPosition() - parentBody->getCMPosition(); // root to joint
		} else {
			Joint* parentJoint = parentBody->pJoints[0];
			dist = joint->getWorldPosition() - parentJoint->getWorldPosition(); // parent joint to joint
		}
		dist *= m_spaceScalingFactor;

		Skeleton::Joint* j = sk.getJoint(i + 1); // +1 because 0 is root
		j->m_offset = dist;
	}
}

void FBXExporter::buildSkeleton(Skeleton::Joint* skJoint, Joint* rbsJoint) {
	// joint = rbsJoint
	skJoint->m_strName = rbsJoint->name;
	skJoint->m_id = rbsJoint->jIndex + 1; // +1 because root is 0 "joint"

	std::vector<Joint*>& childJoints = rbsJoint->child->cJoints;
	int numChildJoints = childJoints.size();
	skJoint->m_childs.resize(numChildJoints);
	for (int i = 0; i < numChildJoints; ++i) {
		skJoint->m_childs[i].m_pPrevious = skJoint;
		buildSkeleton(&skJoint->m_childs[i], childJoints[i]);
	}
}


FbxNode* FBXExporter::jointToNode(FbxScene* pScene, Skeleton::Joint& joint) {
	// retrieve attributes from the joint
	std::string name = joint.m_strName;
	Quaternion rotation = joint.m_q;
	Vector3d translation = joint.m_offset;
	int id = joint.m_id;

	// set name
	FbxString nodeName(name.c_str());

	FbxSkeleton* skeletonAttribute = FbxSkeleton::Create(pScene, name.c_str());
	if (joint.m_pPrevious == NULL) {
		skeletonAttribute->SetSkeletonType(FbxSkeleton::eRoot);
	} else if (joint.m_childs.empty()) {
		skeletonAttribute->SetSkeletonType(FbxSkeleton::eLimbNode); // need to set limb node as well or it will not generate a joint
	} else {
		skeletonAttribute->SetSkeletonType(FbxSkeleton::eLimbNode);
	}

	// create node
	FbxNode* skeletonNode = FbxNode::Create(pScene, nodeName.Buffer());
	skeletonNode->SetNodeAttribute(skeletonAttribute);

	// set translation and rotation (pre rotation is set to zero)

	// set the local translation in the FBX node
	FbxVector4 fbxLocalPos(translation[0], translation[1], translation[2]);
	skeletonNode->LclTranslation.Set(fbxLocalPos);

	//decompose the quaternion q as: q = R(c, gamma) * R(b, beta) * R(a, alpha). Unknowns are: alpha, beta, gamma
	// void computeEulerAnglesFromQuaternion(const Quaternion &q, const V3D &a, const V3D &b, const V3D &c, double& alpha, double& beta, double& gamma)
	// rotation in Skeleton is set as R(z) * R(y) * R(x) where R(axis) is a rotation around the axis
	// a = x, b = y, c = z
	double xAngle, yAngle, zAngle;
	computeEulerAnglesFromQuaternion(rotation, V3D(1, 0, 0), V3D(0, 1, 0), V3D(0, 0, 1), xAngle, yAngle, zAngle);
	// this should be the same "finalJointR" as when the fbx was loaded
	V3D finalJointR(xAngle, yAngle, zAngle);
	// now we set pre rotation to zero and put everything into R
	V3D rJoint = finalJointR * (180.0 / PI);

	// set the local rotation in the FBX node
	FbxVector4 fbxLocalRot(rJoint[0], rJoint[1], rJoint[2]);
	skeletonNode->LclRotation.Set(fbxLocalRot);

	// return the new node
	return skeletonNode;
}

void FBXExporter::addChildNodes(FbxScene* pScene, Skeleton::Joint& currJoint, FbxNode* pParentNode) {
	for (Skeleton::Joint& childJoint : currJoint.m_childs) {
		FbxNode* childNode = jointToNode(pScene, childJoint);
		pParentNode->AddChild(childNode);
		addChildNodes(pScene, childJoint, childNode);
	}
}

void FBXExporter::animateSkeleton(FbxScene* pScene, FbxNode* pSkeletonRoot, Skeleton& _sk, double animationTime) {

	FbxString animStackName = "Raptor motion";
	
	// create an animation stack
	FbxAnimStack* animStack = FbxAnimStack::Create(pScene, animStackName);

	// add AnimLayer to the AnimStack
	FbxAnimLayer* animLayer = FbxAnimLayer::Create(pScene, "Base Layer");
	animStack->AddMember(animLayer);
		
	int numJoints = _sk.numJoints();
	int numFrames = _sk.numKeyFrames();

	// translation is only for the root
	FbxAnimCurve* curveX = pSkeletonRoot->LclTranslation.GetCurve(animLayer, FBXSDK_CURVENODE_COMPONENT_X, true);
	FbxAnimCurve* curveY = pSkeletonRoot->LclTranslation.GetCurve(animLayer, FBXSDK_CURVENODE_COMPONENT_Y, true);
	FbxAnimCurve* curveZ = pSkeletonRoot->LclTranslation.GetCurve(animLayer, FBXSDK_CURVENODE_COMPONENT_Z, true);
	for (int timeIndex = 0; timeIndex < numFrames; ++timeIndex) {
		V3D T = _sk.m_X[timeIndex];
		FbxTime time;
		double frac = (double)timeIndex / (double)(numFrames - 1); // assume the keyframes are uniformly distributed
		time.SetSecondDouble(frac * animationTime);

		curveX->KeyModifyBegin();
		int keyIndexX = curveX->KeyAdd(time);
		curveX->KeySetValue(keyIndexX, T[0]);
		curveX->KeySetInterpolation(keyIndexX, FbxAnimCurveDef::eInterpolationCubic);
		curveX->KeyModifyEnd();

		curveY->KeyModifyBegin();
		int keyIndexY = curveY->KeyAdd(time);
		curveY->KeySetValue(keyIndexY, T[1]);
		curveY->KeySetInterpolation(keyIndexY, FbxAnimCurveDef::eInterpolationCubic);
		curveY->KeyModifyEnd();

		curveZ->KeyModifyBegin();
		int keyIndexZ = curveZ->KeyAdd(time);
		curveZ->KeySetValue(keyIndexZ, T[2]);
		curveZ->KeySetInterpolation(keyIndexZ, FbxAnimCurveDef::eInterpolationCubic);
		curveZ->KeyModifyEnd();
	}


	// animate rotation for all joint
	for (int i = 0; i < numJoints; ++i) {
		Skeleton::Joint* joint = _sk.getJoint(i);
		FbxNode* node = pScene->FindNodeByName(joint->m_strName.c_str());
		FbxAnimCurve* curveX = node->LclRotation.GetCurve(animLayer, FBXSDK_CURVENODE_COMPONENT_X, true);
		FbxAnimCurve* curveY = node->LclRotation.GetCurve(animLayer, FBXSDK_CURVENODE_COMPONENT_Y, true);
		FbxAnimCurve* curveZ = node->LclRotation.GetCurve(animLayer, FBXSDK_CURVENODE_COMPONENT_Z, true);

		for (int timeIndex = 0; timeIndex < numFrames; ++timeIndex) {
			// compute the rotations around the different axes from the quaternion
			Quaternion rotation = _sk.m_Q[timeIndex][joint->m_id];
			double xAngle, yAngle, zAngle;
			computeEulerAnglesFromQuaternion(rotation, V3D(1, 0, 0), V3D(0, 1, 0), V3D(0, 0, 1), xAngle, yAngle, zAngle);
			V3D finalJointR(xAngle, yAngle, zAngle);
			V3D Q = finalJointR * (180.0 / PI);

			FbxTime time;
			double frac = (double)timeIndex / (double)(numFrames - 1); // assume the keyframes are uniformly distributed
			time.SetSecondDouble(frac * animationTime);

			// set the keyframes for all 3 dimensions

			curveX->KeyModifyBegin();
			int keyIndexX = curveX->KeyAdd(time);
			curveX->KeySetValue(keyIndexX, Q[0]);
			curveX->KeySetInterpolation(keyIndexX, FbxAnimCurveDef::eInterpolationCubic);
			curveX->KeyModifyEnd();

			curveY->KeyModifyBegin();
			int keyIndexY = curveY->KeyAdd(time);
			curveY->KeySetValue(keyIndexX, Q[1]);
			curveY->KeySetInterpolation(keyIndexY, FbxAnimCurveDef::eInterpolationCubic);
			curveY->KeyModifyEnd();

			curveZ->KeyModifyBegin();
			int keyIndexZ = curveZ->KeyAdd(time);
			curveZ->KeySetValue(keyIndexX, Q[2]);
			curveZ->KeySetInterpolation(keyIndexZ, FbxAnimCurveDef::eInterpolationCubic);
			curveZ->KeyModifyEnd();
		}
	}
}

