
#include <FBXReaderLib\SkinnedMesh.h>
#include <fbxsdk.h>

SkinnedMesh::~SkinnedMesh() {

}


SkinnedMesh & SkinnedMesh::operator=(const SkinnedMesh & _fmesh)
{
	
	m_bDrawWireFrame = _fmesh.m_bDrawWireFrame;
	m_bDrawCPU = _fmesh.m_bDrawCPU;

	m_indexBufferID = _fmesh.m_indexBufferID;
	m_indexArray = _fmesh.m_indexArray;

	m_cpuShader=_fmesh.m_cpuShader;
	m_gpuShader=_fmesh.m_gpuShader;

	m_sourceVertices = _fmesh.m_sourceVertices;

	m_invBindMatrices = _fmesh.m_invBindMatrices;

	m_skinningMatrices = _fmesh.m_skinningMatrices;

	m_nodeTransform = _fmesh.m_nodeTransform;

	m_boneCountsPerVertex = _fmesh.m_boneCountsPerVertex;
	
	return *this;
}

///<
void SkinnedMesh::translateScaleRotateVertices(const V3D& _offset, const double _scale, const Quaternion& _q)
{
	// Globally rotate, scale and translate vertices, and
	// globally rotate normals.
	for (int i = 0; i < (int)m_sourceVertices.size(); ++i)
	{
		V3D vi = _q.rotate( V3D(m_sourceVertices[i].m_x[0], m_sourceVertices[i].m_x[1], m_sourceVertices[i].m_x[2]) );
		V3D ni = _q.rotate(V3D(m_sourceVertices[i].m_n[0], m_sourceVertices[i].m_n[1], m_sourceVertices[i].m_n[2]));
		
		for (int k = 0; k < 3; ++k)
		{
			m_sourceVertices[i].m_x[k] = vi[k] * _scale + _offset[k];
			m_sourceVertices[i].m_n[k] = ni[k];

		}
	}
}


void SkinnedMesh::CPUShader::create(const std::vector<FVertex>& _srcVertices)
{

	{
		if (m_vertexArray.size() != _srcVertices.size())
		{
			m_vertexArray.clear();
			m_vertexArray.resize(_srcVertices.size());

			for (int i = 0; i < (int)_srcVertices.size(); ++i)
			{
				memcpy(m_vertexArray[i].m_x, _srcVertices[i].m_x, sizeof(m_vertexArray[i].m_x));
				memcpy(m_vertexArray[i].m_n, _srcVertices[i].m_n, sizeof(m_vertexArray[i].m_n));
			}
			
		}
	}


	GLShader* pVert = new GLShader();
	//pVert->loadFromFile("../data/shaders/diffuse/skin_cpu.vert", GL_VERTEX_SHADER);
	pVert->loadFromFile("../data/shaders/diffuse/skin_cpu_blinn_phong.vert", GL_VERTEX_SHADER);
	GLShader* pFrag = new GLShader();
	//pFrag->loadFromFile("../data/shaders/diffuse/skin_cpu.frag", GL_FRAGMENT_SHADER);
	pFrag->loadFromFile("../data/shaders/diffuse/skin_cpu_blinn_phong.frag", GL_FRAGMENT_SHADER);
	m_program.load(pVert, pFrag);

	delete pVert;
	delete pFrag;

	glBindAttribLocation(m_program.getID(), 0, "in_position");
	glBindAttribLocation(m_program.getID(), 1, "in_normal");

	glGenVertexArrays(1, &m_vertexArrayID);
	glBindVertexArray(m_vertexArrayID);

	// Generate an ID for the vertex buffer.
	glGenBuffers(1, &m_vertexBufferID);
	glBindBuffer(GL_ARRAY_BUFFER, m_vertexBufferID);
	glBufferData(GL_ARRAY_BUFFER, m_vertexArray.size()*sizeof(Vertex), &m_vertexArray[0], GL_DYNAMIC_DRAW);


	for (int i = 0; i < 2; ++i)
	{
		glEnableVertexAttribArray(i);
		glBindBuffer(GL_ARRAY_BUFFER, m_vertexBufferID);
		glVertexAttribPointer(i, 4, GL_FLOAT, false, sizeof(Vertex), (unsigned char*)NULL + i*(4 * sizeof(GL_FLOAT)));
	}
}

///<
void SkinnedMesh::CPUShader::setVertexBuffer()
{

	glEnableClientState(GL_VERTEX_ARRAY);
	glBindVertexArray(m_vertexArrayID);
	
	///< Update data sent to GPU:	
	glBindBuffer(GL_ARRAY_BUFFER, m_vertexBufferID);
	glBufferSubData(GL_ARRAY_BUFFER, 0, m_vertexArray.size()*sizeof(Vertex), &m_vertexArray[0]);

}

void SkinnedMesh::GPUShader::setVertexBuffer()
{
	glEnableClientState(GL_VERTEX_ARRAY);

	// Bind the vertex array object that stored all the information about the vertex and index buffers.
	glBindVertexArray(m_vertexArrayID);

}

void SkinnedMesh::GPUShader::create(const std::vector<FVertex>& _srcVertices)
{
	///< First create the vertex buffer
	{
		if (m_vertexArray.size() != _srcVertices.size())
		{
			m_vertexArray.clear();
			m_vertexArray.resize(_srcVertices.size());
			assert(sizeof(FVertex) == sizeof(Vertex));
			
			m_vertexArray.clear();
			m_vertexArray.resize(_srcVertices.size());

			for (int i = 0; i < (int)_srcVertices.size(); ++i)
			{
				memcpy(m_vertexArray[i].m_x, _srcVertices[i].m_x, sizeof(m_vertexArray[i].m_x));
				memcpy(m_vertexArray[i].m_n, _srcVertices[i].m_n, sizeof(m_vertexArray[i].m_n));

				memcpy(m_vertexArray[i]._boneIds, _srcVertices[i]._boneIds, sizeof(m_vertexArray[i]._boneIds));
				memcpy(m_vertexArray[i]._boneWeights, _srcVertices[i]._boneWeights, sizeof(m_vertexArray[i]._boneWeights));
			}

			//memcpy(&m_vertexArray.begin(), &_srcVertices.begin(), sizeof(FVertex)*_srcVertices.size());		
		}
	}


	///< Now the real GPU shader and vertex buffers:
	GLShader* pVert = new GLShader();
	pVert->loadFromFile("../data/shaders/diffuse/diffuse_skin.vert", GL_VERTEX_SHADER);
	GLShader* pFrag = new GLShader();
	pFrag->loadFromFile("../data/shaders/diffuse/diffuse_skin.frag", GL_FRAGMENT_SHADER);
	m_program.load(pVert, pFrag);

	delete pVert;
	delete pFrag;

	glBindAttribLocation(m_program.getID(), 0, "in_position");
	glBindAttribLocation(m_program.getID(), 1, "in_normal");
	glBindAttribLocation(m_program.getID(), 2, "in_boneIds");
	glBindAttribLocation(m_program.getID(), 3, "in_boneWeights");

	glGenVertexArrays(1, &m_vertexArrayID);
	glBindVertexArray(m_vertexArrayID);

	// Generate an ID for the vertex buffer.
	glGenBuffers(1, &m_vertexBufferID);
	glBindBuffer(GL_ARRAY_BUFFER, m_vertexBufferID);
	glBufferData(GL_ARRAY_BUFFER, m_vertexArray.size() * sizeof(Vertex), &m_vertexArray[0], GL_STATIC_DRAW);


	for (int i = 0; i < 4; ++i)
		glEnableVertexAttribArray(i);

	// Specify the location and format of the position portion of the vertex buffer.
	for (int i = 0; i < 4; ++i)
	{
		glBindBuffer(GL_ARRAY_BUFFER, m_vertexBufferID);
		glVertexAttribPointer(i, 4, GL_FLOAT, false, sizeof(Vertex), (unsigned char*)NULL + i*(4 * sizeof(GL_FLOAT)));
	}
}

void SkinnedMesh::createGLBuffers() {

	///< Cpu rendering updates the vertices on the CPU and sends them to the GPU afterwards. Both versions share the index buffer.
	m_cpuShader.create(m_sourceVertices);

	m_gpuShader.create(m_sourceVertices);
		
	// Generate an ID for the index buffer.
	glGenBuffers(1, &m_indexBufferID);
	// Bind the index buffer and load the index data into it.
	
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_indexBufferID);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, m_indexArray.size() * sizeof(GLuint), &m_indexArray[0], GL_STATIC_DRAW);

}

///<
void SkinnedMesh::draw()
{
	if (m_bDrawCPU)
	{		
		m_cpuShader.m_program.bind();
		m_cpuShader.setVertexBuffer();

		// Render the vertex buffer using the index buffer.		
		if (m_bDrawWireFrame)
			glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

		glEnableClientState(GL_INDEX_ARRAY);
		glDrawElements(GL_TRIANGLES, m_indexArray.size(), GL_UNSIGNED_INT, &(m_indexArray[0]));

		if (m_bDrawWireFrame)
			glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

		m_cpuShader.m_program.unbind();
	}
	else
	{
		m_gpuShader.m_program.bind();

		m_gpuShader.setVertexBuffer();
		
		///< Send matrices
		GLint matLoc = glGetUniformLocation(m_gpuShader.m_program.getID(), "boneMatrices");
		const GLfloat* pMatrix = m_skinningMatrices[0].m_d;
		glUniformMatrix4fvARB(matLoc, m_skinningMatrices.size(), GL_FALSE, pMatrix);
		
		// Render the vertex buffer using the index buffer.		
		if (m_bDrawWireFrame)
			glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

		
		glDrawElements(GL_TRIANGLES, m_indexArray.size(), GL_UNSIGNED_INT, &m_indexArray[0]);
		//GL_QUADS
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

		m_gpuShader.m_program.unbind();
	}

	
};



///<
void  SkinnedMesh::updateSkinningMatrices(const TransformsContainer& _boneTransforms)
{
	for (int k = 0; k < (int)_boneTransforms.size(); ++k)
	{
		const Matrix4x4& invBind = m_invBindMatrices.at(k);
		const Matrix4x4& gTrans = _boneTransforms.at(k);

		Matrix4x4 skinningM =  (gTrans * invBind);

		for (int i = 0; i < 4; ++i)
			for (int j = 0; j < 4; ++j)
			{
				m_skinningMatrices[k].m_d[i * 4 + j] = static_cast<GLfloat> (skinningM(i, j));
			}
	}

	if (m_bDrawCPU)
		transformMeshVertices_linearBlendSkinning();
};

///<
void  SkinnedMesh::transformMeshVertices_linearBlendSkinning()
{
	Matrix4x4 transform;
	for (int vertIndex = 0; vertIndex < (int)m_cpuShader.m_vertexArray.size(); ++vertIndex)
	{
		const SkinnedMesh::FVertex& vert = m_sourceVertices[vertIndex];
		Vector4d pV = Vector4d(static_cast<GLdouble>(vert.m_x[0]), static_cast<GLdouble>(vert.m_x[1]), static_cast<GLdouble>(vert.m_x[2]), 1);
		Vector4d pN = Vector4d(static_cast<GLdouble>(vert.m_n[0]), static_cast<GLdouble>(vert.m_n[1]), static_cast<GLdouble>(vert.m_n[2]), 0);

		int vBoneWeightCount = m_boneCountsPerVertex[vertIndex];
		if (vBoneWeightCount != 0) 
		{
			double totalWeight = 0.0;				
			transform.fill(0);

			for (int j = 0; j < vBoneWeightCount; ++j) 
			{
				int boneIndex = (int)vert._boneIds[j];
				double boneWeight = vert._boneWeights[j];
				totalWeight += boneWeight;

				Matrix4x4 skinningM;
				for (int i = 0; i < 4; ++i)
					for (int j = 0; j < 4; ++j)
						skinningM(i, j) = static_cast<double>(m_skinningMatrices[boneIndex].m_d[i * 4 + j]);

				Matrix4x4 transform_j = skinningM; 
				transform += transform_j * boneWeight;
			}

			if (totalWeight>0.001)
				transform /= totalWeight;			

			pV = transform*pV;
			pN = transform*pN;
		}
		
		for (int k = 0; k < 4; ++k)
		{
			m_cpuShader.m_vertexArray[vertIndex].m_x[k] = static_cast<GLfloat> (pV[k]);
			m_cpuShader.m_vertexArray[vertIndex].m_n[k] = static_cast<GLfloat> (pN[k]);
		}
			
	}
};



void SkinnedMesh::writeVerticesToFile(const char* filename)
{
	FILE* fp = fopen(filename, "w");
	if (fp == NULL) {
		throwError("cannot open the file \'%s\'", filename);
	}
	
	int numBones = m_skinningMatrices.size();
	int numVertex = m_sourceVertices.size();
	int numIndex = m_indexArray.size();

	fprintf(fp, "numBones %d\n", numBones);
	fprintf(fp, "numVertex %d\n", numVertex);
	fprintf(fp, "numIndex %d\n", numIndex);

	// Vertex information
	for (int i = 0; i < (int)m_sourceVertices.size(); ++i)
	{
		fprintf(fp, "Vertex\n");

		fprintf(fp, "\tposition %f %f %f %f\n", m_sourceVertices[i].m_x[0], m_sourceVertices[i].m_x[1], m_sourceVertices[i].m_x[2], m_sourceVertices[i].m_x[3]);
		fprintf(fp, "\tnormal %f %f %f %f\n", m_sourceVertices[i].m_n[0], m_sourceVertices[i].m_n[1], m_sourceVertices[i].m_n[2], m_sourceVertices[i].m_n[3]);

		for (int j = 0; j < 4 && m_sourceVertices[i]._boneIds[j] != -1; ++j)
		{
			fprintf(fp, "\t\tboneId %f\n", m_sourceVertices[i]._boneIds[j]);
			fprintf(fp, "\t\tboneWeight %f\n", m_sourceVertices[i]._boneWeights[j]);
		}

		fprintf(fp, "/End\n\n");
	}

	// Index array
	printf("IndexArray\n");
	for (int i = 0; i < (int)m_indexArray.size(); ++i) {
		if ((i != 0) && (i % 20 == 0) || (i == m_indexArray.size() - 1)) {
			printf("%d\n", m_indexArray[i]);
		}
		else
			printf("%d, ", m_indexArray[i]);
	}
	printf("/End");

	fclose(fp);
}

void SkinnedMesh::readVerticesFromFile(const char* filename)
{
	FILE* fp = fopen(filename, "r");
	if (fp == NULL) {
		throwError("cannot open the file \'%s\'", filename);
	}

	m_sourceVertices.clear();
	m_boneCountsPerVertex.clear();
	m_indexArray.clear();

	// assuming they are written to the file as in order of sourceVertices
	int vertexId = -1;
	int boneIndex = 0;

	int numIndex = 0;

	char buffer[200];

	while (!feof(fp)) {

		readValidLine(buffer, 200, fp);

		char first[40];
		sscanf(buffer, "%s", &first);
		std::string type = first;		

		if (type.compare("numBones") == 0) 
		{
			int numBones = 0;
			sscanf(buffer, "%*s %d", &numBones);
			m_invBindMatrices.resize(numBones);
			m_skinningMatrices.resize(numBones);
		}

		
		if (type.compare("numVertex") == 0)
		{
			int numVertex = 0;
			sscanf(buffer, "%*s %d", &numVertex);
			m_sourceVertices.resize(numVertex);
		}
			
			
		if (type.compare("numIndex") == 0) 
			sscanf(buffer, "%*s %d", &numIndex);

		if (type.compare("Vertex") == 0) {
			vertexId++;
		}

		if (type.compare("/End") == 0) {
			m_boneCountsPerVertex.push_back(boneIndex);
			boneIndex = 0;
		}

		// read position vector
		if(type.compare("position") == 0)
			sscanf(buffer, "%*s %f %f %f %f", &m_sourceVertices[vertexId].m_x[0], &m_sourceVertices[vertexId].m_x[1], &m_sourceVertices[vertexId].m_x[2], &m_sourceVertices[vertexId].m_x[3]);
		// read normal vector
		if (type.compare("normal") == 0)
			sscanf(buffer, "%*s %f %f %f %f", &m_sourceVertices[vertexId].m_n[0], &m_sourceVertices[vertexId].m_n[1], &m_sourceVertices[vertexId].m_n[2], &m_sourceVertices[vertexId].m_n[3]);
		// read bone id
		if (type.compare("boneId") == 0)
			sscanf(buffer, "%*s %f", &m_sourceVertices[vertexId]._boneIds[boneIndex]);
		// read bone weight
		if (type.compare("boneWeight") == 0)
			sscanf(buffer, "%*s %f", &m_sourceVertices[vertexId]._boneWeights[boneIndex++]);
	
		if (type.compare("IndexArray") == 0)
		{
			assert(numIndex > 0);
			m_indexArray.resize(numIndex);

			char *pt;
			int indexId = 0;
			while (indexId < numIndex)
			{
				readValidLine(buffer, 200, fp);	
				pt = strtok(lTrim(buffer), ",");
				while (pt != NULL) 
				{
					m_indexArray[indexId] = atoi(pt);
					indexId++;
					pt = strtok(NULL, ",");
				}
			}
		}
	}

	fclose(fp);
}
