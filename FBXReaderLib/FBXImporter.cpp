
#include <FBXReaderLib\FBXImporter.h>
#include <FBXReaderLib\Common\Common.h>


#define PIVOT0 FbxNode::eSourcePivot
#define PIVOT1 FbxNode::eDestinationPivot

///<
void FBXImporter::loadSkeleton(const char* _csFileName, Skeleton& _sk)
{
	Logger::consolePrint("FBX FILE: '%s' \n", _csFileName);
	FBXImporter importer(_csFileName);
	importer.createSkeleton(_sk);
}

///<
void FBXImporter::loadMeshedSkeleton(const char* _csFileName, Skeleton& _sk, SkinnedMesh** _ppMesh)
{
	Logger::consolePrint("FBX FILE: '%s' \n", _csFileName);
	FBXImporter importer(_csFileName);
	importer.createSkeleton(_sk);

	importer.createMesh(_sk, _ppMesh);
}

///< It imports all the necessary information for drawing the scene from a FBX file
FBXImporter::FBXImporter(std::string fileName, double _mocapSamplingRate)
{
	m_sceneName = fileName;

	// Initialize an FBX SDK memory management object 
	m_pFbxManager = FbxManager::Create();

	// Create an importer that imports the comtents of an FBX file into the scene
	FbxImporter *fbxImporter = FbxImporter::Create(m_pFbxManager, "");

	if (!fbxImporter->Initialize(fileName.c_str()))
	{
		//Import Failed - LOG
		printf("Call to FbxImporter::Initialize failed\n");
		Logger::consolePrint("File: '%s' not found! \n", fileName);
	}

	// Create a new scene
	m_pScene = FbxScene::Create(m_pFbxManager, fileName.c_str());

	// Import the contents of the file into the scene
	fbxImporter->Import(m_pScene);

	//Importing Scene Success -LOG
	initImporterAtts(fbxImporter, _mocapSamplingRate);


	///< Test saving scene:
	{
		/*
		// this sets pManager and pScene
		//void InitializeSdkObjects(FbxManager*& pManager, FbxScene*& pScene);
		//FBXCommon::InitializeSdkObjects(m_pFbxManager, m_pScene);
		
		// we already have manager and scene -> just need FbxIOSettings
		FbxIOSettings* ios = FbxIOSettings::Create(m_pFbxManager, IOSROOT);
		m_pFbxManager->SetIOSettings(ios);

		// and then save the scene
		bool SaveScene(FbxManager* pManager, FbxDocument* pScene, const char* pFilename, int pFileFormat = -1, bool pEmbedMedia = false);
		FBXCommon::SaveScene(m_pFbxManager, m_pScene, "..\\data\\test.fbx");
		*/
	}
	

	// The file is imported, so clean up memory
	fbxImporter->Destroy();

	// Triangulate the mesh polygons
	FbxGeometryConverter geometryConverter(m_pFbxManager);
	geometryConverter.Triangulate(m_pScene, true);

}

///<
FBXImporter::~FBXImporter()
{
	///< Destroys everything...
	if(m_pFbxManager)
		m_pFbxManager->Destroy();

	m_pFbxManager = NULL;
	m_pScene = NULL;
	m_pRootNode = NULL;
}

///<
void FBXImporter::fillJoint(Skeleton::Joint& _jnt, const AnimationImport::Bone& _importedBone, int& _id)
{
	_id++;
	_jnt.m_id = _id;

	_jnt.m_strName = _importedBone.m_name;
	
	//_jnt.m_q = Quaternion::expMap(_importedBone.m_r);
	_jnt.m_q = _importedBone.m_q;
	_jnt.m_offset = _importedBone.m_t;

	_jnt.m_childs.resize(_importedBone.m_childs.size());

	for (int j = 0; j < (int)_importedBone.m_childs.size(); ++j)
	{
		_jnt.m_childs[j].m_pPrevious = &_jnt;
		fillJoint(_jnt.m_childs[j], _importedBone.m_childs[j], _id);
	}
}

FbxAnimLayer* FBXImporter::getFirstLayer()
{
	assert(m_pRootNode != NULL);

	int numAnimStacks = m_pScene->GetMemberCount<FbxAnimStack>();
	assert(numAnimStacks > 0);
	FbxAnimStack* pStack = m_pScene->GetMember<FbxAnimStack>(0);
	if (pStack)
	{
		int numLayers = pStack->GetMemberCount<FbxAnimLayer>();
		assert(numLayers > 0);

		FbxAnimLayer* pLayer = pStack->GetMember<FbxAnimLayer>(0); /// For now only one layer !
		assert(pLayer != NULL);

		return pLayer;

	}

	return NULL;
}

bool checkAttribute(FbxNode* _pNode, FbxNodeAttribute::EType _attribute)
{
	FbxNodeAttribute* pAttribute = _pNode->GetNodeAttribute();
	if (pAttribute)
	{
		FbxNodeAttribute::EType type = pAttribute->GetAttributeType();
		return type == _attribute;
	}

	return false;
}

//<
void FindFirstAnimatedNode(FbxNode* _pCurrentNode, FbxNode** _ppRNode)
{
	if (checkAttribute(_pCurrentNode, FbxNodeAttribute::eSkeleton))
	{
		*_ppRNode = _pCurrentNode;
		return;
	}
		
	///< Find first animated Node:
	for (int i = 0; i < _pCurrentNode->GetChildCount(); ++i)
	{
		FbxNode* pNodeChild = _pCurrentNode->GetChild(i);

		if (checkAttribute(pNodeChild, FbxNodeAttribute::eSkeleton))
		{
			*_ppRNode = pNodeChild;
		}
		else if(checkAttribute(pNodeChild, FbxNodeAttribute::eNurbsCurve))
		{
			FindFirstAnimatedNode(pNodeChild, _ppRNode);
		}
	}
}

///<
void FBXImporter::initImporterAtts(FbxImporter* _pFbxImporter, double _mocapSamplingRate)
{
	if (_pFbxImporter)
	{
		m_pRootNode = (FbxNode *)m_pScene->GetRootNode();

		bool m_bUseKeyFrames = true;
		if (m_bUseKeyFrames) //OrSampleInterpolatedMotionniformly
		{
			///< usually one..
			FbxAnimLayer* pLayer = getFirstLayer();
			if (pLayer)
			{				
				///: Checks only the first layer of children from the root...hopefully they are animated!
				FbxNode* pFirstAnimatedNode = NULL;
				FindFirstAnimatedNode(m_pRootNode, &pFirstAnimatedNode);

				if (pFirstAnimatedNode != NULL)
				{
					Logger::consolePrint("Loading key frames (not sampling between). \n");
					FbxAnimCurve* pAnimCurve = pFirstAnimatedNode->LclRotation.GetCurve(pLayer, FBXSDK_CURVENODE_COMPONENT_X);

					if (pAnimCurve)
						m_numOfPoses = pAnimCurve->KeyGetCount();
					else
						m_numOfPoses = 0;
				}
				else
				{
					assert(false);//, "not one of the childs"
				}			
			}
		}
		else
		{
			// Check if there ?s any take info, and assign 
			FbxTakeInfo* lCurrentTakeInfo = _pFbxImporter->GetTakeInfo(0);
			if (lCurrentTakeInfo)
			{
				m_fbxStartTime = lCurrentTakeInfo->mLocalTimeSpan.GetStart();
				m_fbxStopTime = lCurrentTakeInfo->mLocalTimeSpan.GetStop();
			}
			else
			{
				FbxTimeSpan timeLineTimeSpan;
				m_pScene->GetGlobalSettings().GetTimelineDefaultTimeSpan(timeLineTimeSpan);
				m_fbxStartTime = timeLineTimeSpan.GetStart();
				m_fbxStopTime = timeLineTimeSpan.GetStop();
			}

			///< NUMBER OF FRAMES TO LOAD:
			// Usually 30 frames per second, 
			// MOCAP comes at 120 fps.
			// Use eCustom to set manual framerate 
			if (_mocapSamplingRate>0.0)
			{
				m_fbxFrameTime.SetTime(0, 0, 0, 1, 0, FbxTime::eCustom);// FbxTime::eFrames120);
				m_fbxFrameTime.SetGlobalTimeMode(m_fbxFrameTime.GetGlobalTimeMode(), _mocapSamplingRate);
			}
			else
			{
				FbxTime t = m_pScene->GetGlobalSettings().GetTimeMode();
				printf("Mocap FrameRate : %f\n", t.GetFrameRate(FbxTime::eCustom));
				m_fbxFrameTime.SetTime(0, 0, 0, 1, 0, m_pScene->GetGlobalSettings().GetTimeMode());
			}
			
			m_numOfPoses = (int)((m_fbxStopTime.GetMilliSeconds() - m_fbxStartTime.GetMilliSeconds()) / m_fbxFrameTime.GetMilliSeconds()) + 1;

		}
	}
}

///<
class TransformGetter
{
public:

	V3D				m_x;
	V3D				m_s;
	Quaternion		m_q;

	TransformGetter() : m_s(1, 1, 1) {}
	
	
	static TransformGetter get(FbxTime& _time, FbxNode* _pNode)
	{
		return get(_time.Get(), _pNode);
	}

	static TransformGetter get(double _t, FbxNode* _pNode)
	{
		
		FbxAMatrix &lLocalTransform = _pNode->EvaluateLocalTransform(_t, PIVOT1);

		return getFromTransform(lLocalTransform, _pNode);

	}

	static TransformGetter getFromTransform(FbxAMatrix& _fbxTransform, FbxNode* _pNode)
	{
		TransformGetter rVal;

		FbxAMatrix T;
		T.SetT(_fbxTransform.GetT());
		FbxAMatrix R;
		R.SetR(_fbxTransform.GetR());
		FbxAMatrix S;
		S.SetS(_fbxTransform.GetS());

		//FbxDouble3 lRotation = _pNode->LclRotation.Get();
		
		//FbxDouble3 lPostRotation = _pNode->PostRotation.Get();

		///< Rotation aditions.
		/*
		FbxAMatrix Roff;
		Roff.SetT(_pNode->GetRotationOffset(PIVOT1));
		FbxAMatrix Rp;
		Rp.SetT(_pNode->GetRotationPivot(PIVOT1));
	
		FbxAMatrix Rpost;
		Rpost.SetR(_pNode->GetPostRotation(PIVOT1));
		*/
		//FbxAMatrix Soff;
		//Soff.SetT(_pNode->GetScalingOffset(PIVOT1));
		//FbxAMatrix Sp;
		//Sp.SetR(_pNode->GetScalingPivot(PIVOT1));

		/*
		FbxAMatrix Rpre;
		Rpre.SetR(_pNode->GetPreRotation(PIVOT1));
		FbxDouble3 lPreRotation = _pNode->PreRotation.Get();
		*/

		// See Maya and Fbx doc:
		//FbxAMatrix localTransform = T * Roff * Rp * Rpre * R * Rpost.Inverse() * Rp.Inverse(); // * Soff * Sp * S * Sp.Inverse();

		//FbxLimits& test = _pNode->GetRotationLimits();
		
		{
			if (strcmp(_pNode->GetName(), "JNT_l_elbow") == 0)
			{
				FbxDouble3 xPivot = _pNode->RotationPivot.Get();
				FbxDouble3 xPost = _pNode->PostRotation.Get();

				int z = 0;
				z++;
			}


			///< x:
			FbxVector4 fbxLocalPos = T.GetT();
			rVal.m_x = V3D(fbxLocalPos[0], fbxLocalPos[1], fbxLocalPos[2]);

			///< s:
			double s = S.Get(0, 0);
			rVal.m_s = V3D(s, s, s);

			///< q:
			FbxVector4 fbxLocalJointRotation = R.GetR();
			V3D jointRotationInAngles = (PI / 180.0)*V3D(fbxLocalJointRotation[0], fbxLocalJointRotation[1], fbxLocalJointRotation[2]);
			Quaternion qLocalJointRotation = getRotationQuaternion(jointRotationInAngles[2], V3D(0, 0, 1))*getRotationQuaternion(jointRotationInAngles[1], V3D(0, 1, 0))*getRotationQuaternion(jointRotationInAngles[0], V3D(1, 0, 0));

			FbxDouble3	preRotation = _pNode->PreRotation.Get();
			V3D	preRotationInAngles = (PI / 180.0)*V3D(preRotation[0], preRotation[1], preRotation[2]);
			Quaternion qPreRotation = getRotationQuaternion(preRotationInAngles[2], V3D(0, 0, 1))*getRotationQuaternion(preRotationInAngles[1], V3D(0, 1, 0))*getRotationQuaternion(preRotationInAngles[0], V3D(1, 0, 0));
				
			/*
			FbxDouble3	postRotation = _pNode->PostRotation.Get();
			V3D	postRotationInAngles = (PI / 180.0)*V3D(postRotation[0], postRotation[1], postRotation[2]);
			Quaternion qPostRotation = getRotationQuaternion(postRotationInAngles[2], V3D(0, 0, 1))*getRotationQuaternion(postRotationInAngles[1], V3D(0, 1, 0))*getRotationQuaternion(postRotationInAngles[0], V3D(1, 0, 0));
			*/
			

			rVal.m_q = qPreRotation*qLocalJointRotation;

			//m_q = getRotationQuaternion(finalJointR[0], V3D(1, 0, 0))*getRotationQuaternion(finalJointR[1], V3D(0, 1, 0))*getRotationQuaternion(finalJointR[2], V3D(0, 0, 1));
		}

		return rVal;
	}

	

};


/*
*	Problem is not every node (say joint) is key framed at each key frame...  I am using for now the root translation to querry the number of key frames and
*	assume all the other joints (nodes) have such a key frame.
*/
void FBXImporter::getKeyTimesFromRootTranslationKeyFrames(Skeleton& _sk, std::vector<FbxTime>& _kfTimes)
{
	FbxAnimLayer* pLayer = getFirstLayer();
	if (pLayer)
	{
		///<	
		Skeleton::Joint* pRootSkJoint = NULL;
		_sk.FindJoint(0, _sk.m_pRoot, &pRootSkJoint);

		FbxNode* pJointNode = m_pScene->FindNodeByName(pRootSkJoint->m_strName.c_str());		
		FbxAnimCurve* pJointAnimCurve = pJointNode->LclRotation.GetCurve(pLayer, FBXSDK_CURVENODE_COMPONENT_X);

		int keyCount = 0;
		if(pJointAnimCurve)
			keyCount = pJointAnimCurve->KeyGetCount();

		for (int i = 0; i < keyCount; ++i)
			_kfTimes.push_back(pJointAnimCurve->KeyGetTime(i));
	}
}

///<  Could load less poses here, but the fbx reader loads them before anyway...
void FBXImporter::loadMotion(Skeleton& _sk /*, int _numPoses*/)
{
	_sk.m_X.resize(this->m_numOfPoses);
	_sk.m_Q.resize(this->m_numOfPoses);

	int numJoints = _sk.size();
	for (int i = 0; i < (int)_sk.m_Q.size(); ++i)
		_sk.m_Q[i].resize(numJoints);

	FbxAnimLayer* pLayer = getFirstLayer();
	if(pLayer)
	{
		std::vector<FbxTime> keyTimes;
		getKeyTimesFromRootTranslationKeyFrames(_sk, keyTimes);

		///<
		for (int j = 0; j < numJoints; ++j)
		{
			Skeleton::Joint* pSkeletonJoint = NULL;
			_sk.FindJoint(j, _sk.m_pRoot, &pSkeletonJoint);
			FbxNode* pJointNode = m_pScene->FindNodeByName(pSkeletonJoint->m_strName.c_str());

			
			for (int poseNum = 0; poseNum < this->m_numOfPoses; ++poseNum)
			{
				TransformGetter mA;
				if (keyTimes.size() == this->m_numOfPoses)/// interpolate
				{
					mA = TransformGetter::get(keyTimes[poseNum], pJointNode);
				}
				else
				{
					mA = TransformGetter::get(this->m_fbxCurrentTime, pJointNode);
					this->m_fbxCurrentTime += this->m_fbxFrameTime;
				}					

				///< if root
				if (pSkeletonJoint->m_id == 0)
					_sk.m_X[poseNum] = mA.m_x;

				_sk.m_Q[poseNum][pSkeletonJoint->m_id] = mA.m_q;
				
			}
				
		}
	}
}

///<
void findFirstAttribute(FbxNode* _pNode, FbxNodeAttribute::EType _attribute, FbxNode** _ppFoundNode)
{
	if (_pNode != NULL)
	{
		if (_pNode->GetNodeAttribute() != NULL)
			if (_pNode->GetNodeAttribute()->GetAttributeType() == _attribute)
			{
				*_ppFoundNode = _pNode;
				return;
			}


		for (int i = 0; i < _pNode->GetChildCount(); i++)
		{
			FbxNode* pFbxChildNode = _pNode->GetChild(i);

			findFirstAttribute(pFbxChildNode, _attribute, _ppFoundNode);
		}
	}
}

///<
void FBXImporter::createMesh(Skeleton& _sk, SkinnedMesh** _ppMesh, V3D _translate, double _scale)
{
	createMesh(_sk, _ppMesh, m_pRootNode);

	if (*_ppMesh)
	{
		(*_ppMesh)->translateScaleRotateVertices(_translate, _scale, Quaternion());

		(*_ppMesh)->createGLBuffers();

		printf("Loading Mesh finished.\n");
	}
}


///<
void FBXImporter::createMesh(Skeleton& _sk, SkinnedMesh** _ppMesh)
{
	createMesh(_sk, _ppMesh, m_pRootNode);

	if (*_ppMesh)
	{
		(*_ppMesh)->createGLBuffers();
		printf("Loading Mesh finished.\n");
	}
	else
		printf("No mesh in fbx.\n");
}

///<
FbxMesh* FBXImporter::getFbxMeshNode()
{
	assert(m_pRootNode != NULL);
	if (m_pRootNode)
	{
		FbxNode* pNode = NULL;
		findFirstAttribute(m_pRootNode, FbxNodeAttribute::eMesh, &pNode);

		if (pNode != NULL)
			return (FbxMesh*)pNode->GetNodeAttribute();
	}
	return NULL;		
}

///<
int FBXImporter::numIndices()
{
	FbxMesh* pFbxMesh = getFbxMeshNode();
	if (pFbxMesh)
		return pFbxMesh->GetPolygonVertexCount();

	return 0;
}

///<
int* FBXImporter::getIndices()
{
	FbxMesh* pFbxMesh = getFbxMeshNode();
	if (pFbxMesh)
		return pFbxMesh->GetPolygonVertices();

	return NULL;
}

///<
int FBXImporter::numVertices()
{
	FbxMesh* pFbxMesh = getFbxMeshNode();
	if (pFbxMesh)
	{
		return  pFbxMesh->GetControlPointsCount();
	}
	return 0;
}

///<
FbxVector4* FBXImporter::getVertices()
{
	FbxMesh* pFbxMesh = getFbxMeshNode();
	if (pFbxMesh)
	{
		return pFbxMesh->GetControlPoints();
	}
	return NULL;
}

///<
void FBXImporter::createMesh(Skeleton& _sk, SkinnedMesh** _ppMesh, FbxNode *pRootNode)
{
	FbxMesh* pFbxMesh = getFbxMeshNode();
	if (pFbxMesh)
	{
		FbxNode* pMeshNode = (FbxNode*)pFbxMesh;
		//TransformGetter mA = TransformGetter::get(0,);

		FbxNode* pParent = pMeshNode->GetParent();
		TransformGetter aNodeTransform = TransformGetter::get(0, pParent);

		*_ppMesh = new SkinnedMesh();
		SkinnedMesh& _mesh = **_ppMesh;

		///< Create index array
		{
			_mesh.m_indexArray.resize(pFbxMesh->GetPolygonVertexCount());
			for (uint i = 0; i < _mesh.m_indexArray.size(); ++i)
				_mesh.m_indexArray[i] = pFbxMesh->GetPolygonVertices()[i];
		}

		// Create vertex array of the mesh
		int vertexCount = pFbxMesh->GetControlPointsCount();
		_mesh.m_sourceVertices.resize(vertexCount);



		FbxLayerElementArrayTemplate<FbxVector4>* pNormals = NULL;
		bool bNormalsSuccess = pFbxMesh->GetNormals(&pNormals);

		/// For now code assumes there are normals...
		assert(bNormalsSuccess);

		int nCount = pNormals->GetCount();
		if (vertexCount == nCount) ///< Mapping normals per vertex.
		{
			
			///<	
			for (int i = 0; i < (int)_mesh.m_sourceVertices.size(); i++)
			{
				FbxVector4 p = pFbxMesh->GetControlPointAt(i);
				FbxVector4 n = pNormals->GetAt(i);

				for (int k = 0; k < 3; ++k)
				{
					//float vk = _vertexScale*static_cast<GLfloat> (p[k]) + (GLfloat)_pVertexTranslate[k];
					float vk = p[k];
					_mesh.m_sourceVertices[i].m_x[k] = vk;
					_mesh.m_sourceVertices[i].m_n[k] = n[k];
				}

			}
		}
		else ///< Mapping the normals followig the index buffer...	
		{
			for (int i = 0; i < (int)_mesh.m_indexArray.size(); ++i)
			{
				int index = _mesh.m_indexArray[i];

				FbxVector4 p = pFbxMesh->GetControlPointAt(index);
				FbxVector4 n = pNormals->GetAt(i);

				for (int k = 0; k < 3; ++k)
				{
					_mesh.m_sourceVertices[index].m_x[k] = p[k];
					_mesh.m_sourceVertices[index].m_n[k] = n[k];
				}
			}
		}

		_mesh.m_boneCountsPerVertex.resize(_mesh.m_sourceVertices.size());
		std::fill(_mesh.m_boneCountsPerVertex.begin(), _mesh.m_boneCountsPerVertex.end(), 0);


		///< Perform the node's transforms to the vertices and normals.
		V3D _pVertexTranslate = aNodeTransform.m_x;
		Quaternion q = aNodeTransform.m_q;

		//double _vertexScale = aNodeTransform.m_s[0];

		_mesh.m_nodeTransform = NodeTransform(_pVertexTranslate, aNodeTransform.m_s, q);

		int z = 0;
		z++;

		loadBoneWeights(_sk, _mesh, pFbxMesh);


		//_mesh.translateScaleRotateVertices(_pVertexTranslate, _vertexScale, q);

	}
}

FbxSkin* FBXImporter::getSkinDeformer()
{
	FbxMesh* pFbxMesh = getFbxMeshNode();

	int numOfDeformers = pFbxMesh->GetDeformerCount();
	assert(numOfDeformers > 0);

	if (numOfDeformers>0)
		return (FbxSkin*)pFbxMesh->GetDeformer(0, FbxDeformer::eSkin);

	return NULL;
}

Matrix4x4 toMatrix4(FbxMatrix& _m)
{
	Matrix4x4 m;
	for (int i = 0; i < 4; ++i)
	{
		for (int j = 0; j < 4; ++j)
			m(i, j) = _m.Get(i, j);
	}

	return m;
}


// ------------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------


FbxNode* FBXImporter::findNodeByName(FbxNode* startNode, std::string name) {
    int numChild = startNode->GetChildCount();
    for (int i = 0; i < numChild; ++i) {
        FbxNode* child = startNode->GetChild(i);
        if (child->GetName() == name) {
            return child;
        } else {
            FbxNode* found = findNodeByName(child, name);
            if (found != NULL) {
                return found;
            }
        }
    }
    return NULL;
}




void FBXImporter::getAndSetMayaBindPose(Skeleton& _sk, SkinnedMesh& _mesh, FbxMesh* _pMesh)
{
	//<
	std::vector<FbxPose*> poseVec;
	const int lPoseCount = m_pScene->GetPoseCount();
	for (int i = 0; i < lPoseCount; ++i)
		poseVec.push_back(m_pScene->GetPose(i));

	if (poseVec.size() > 0)
	{
		if (poseVec[0]->IsBindPose())
		{
			int numNodes = poseVec[0]->GetCount();
			for (int i = 0; i < numNodes; ++i)
			{
				FbxNode* pNodeI = poseVec[0]->GetNode(i);

				const char* csJointName = pNodeI->GetName();
				Skeleton::Joint* pJoint = _sk.FindJoint(std::string(csJointName));

				if (pJoint)
				{
					FbxMatrix A = poseVec[0]->GetMatrix(i);
					Matrix4x4 aMayaBind = toMatrix4(A).transpose();
					_mesh.m_invBindMatrices[pJoint->m_id] = aMayaBind.inverse();
				}
			}
		}
	}
}


// ------------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------
#include <set>

void FBXImporter::setMissingJointsInBindPose(Skeleton& _sk, SkinnedMesh& _mesh, FbxMesh* _pMesh)
{

	///< Used to say if joint is in bind pose or not
	/// Only joints with skinning influence are in the bind pose.
	std::set<int> hasBindPose;

	// ------------------------------------------------------------------------------------------
	// ------------------------------------------------------------------------------------------
	// ------------------------------------------------------------------------------------------
	hasBindPose.clear();
	// ------------------------------------------------------------------------------------------
	// ------------------------------------------------------------------------------------------
	// ------------------------------------------------------------------------------------------

	//<
	std::vector<FbxPose*> poseVec;
	const int lPoseCount = m_pScene->GetPoseCount();
	for (int i = 0; i < lPoseCount; ++i)
		poseVec.push_back(m_pScene->GetPose(i));

	if (poseVec.size() > 0)
	{
		if (poseVec[0]->IsBindPose())
		{
			int numNodes = poseVec[0]->GetCount();
			for (int i = 0; i < numNodes; ++i)
			{
				FbxNode* pNodeI = poseVec[0]->GetNode(i);
				const char* csJointName = pNodeI->GetName();
				Skeleton::Joint* pJoint = _sk.FindJoint(std::string(csJointName));
				if (pJoint)
				{
					// for the joints that have an influence we find something in the bind pose
					// set the flag for those
					// for the other joints we have to recompute the bind pose transformation
					hasBindPose.insert(pJoint->m_id);

				}
			}
		}
	}



	// ------------------------------------------------------------------------------------------
	// ------------------------------------------------------------------------------------------
	// ------------------------------------------------------------------------------------------
	// joints that have an influence are also listed in the bind pose
	// joints without influence are nowhere listed
	// for these we have to recompute the transformation
	//
	// The following links discus exactly this problem:
	//      https://www.gamedev.net/topic/657796-how-to-calculate-bind-pose-for-bones-that-are-not-attached-to-the-skin-but-which-are-in-a-hierarchy-where-some-bones-are-attached-to-the-skin/
	//      https://forums.autodesk.com/t5/fbx-forum/skeleton-bind-pose-for-bones-not-linked-to-clusters/td-p/4173622?nobounce

	// we need the skeleton again to get the FbxNode
	FbxNode* pSkeletonNode = NULL;
	findFirstAttribute(m_pRootNode, FbxNodeAttribute::eSkeleton, &pSkeletonNode);

	int numJoints = _sk.numJoints();
	for (int i = 0; i < numJoints; ++i) { // maybe we cannot go by index but should follow hierarchy
										  // compute transformation if we did not retrieve a bind pose transformation for the joint
		if (hasBindPose.find(i) == hasBindPose.end()) {
			Skeleton::Joint* child = _sk.getJoint(i); // joint with no bind pose transform
			Skeleton::Joint* parent = NULL; // the parent of the joint wihtout transform (this should have transform)

											// find the parent joint
			for (int j = 0; j < numJoints; ++j) {
				Skeleton::Joint* joint = _sk.getJoint(j);
				for (Skeleton::Joint child : joint->m_childs) {
					if (child.m_id == i) {
						parent = joint;
						break;
					}
				}
				if (parent != NULL) {
					break;
				}
			}

			// find FbxNode for child
			FbxNode* childNode = findNodeByName(pSkeletonNode, child->m_strName);

			// find cluster for parent (this holds also the bind pose information)
			FbxCluster* parentCluster = NULL;
			FbxSkin* pSkin = getSkinDeformer();
			int numClusters = pSkin->GetClusterCount();
			for (int i = 0; i < numClusters; ++i) {
				FbxCluster* cluster = pSkin->GetCluster(i);
				std::string strBoneName = cluster->GetLink()->GetName();
				if (strBoneName == parent->m_strName) {
					parentCluster = cluster;
					break;
				}
			}

			// compute child local
			// FBXSDK_TIME_INFINITE:  returns the default value, without animation curves evaluation
			FbxAMatrix &ChildLocal = childNode->EvaluateLocalTransform(FBXSDK_TIME_INFINITE, PIVOT1);

			// compute parent global
			FbxAMatrix ParentGlobalAtBinding;
			parentCluster->GetTransformLinkMatrix(ParentGlobalAtBinding);

			// compute child global at binding
			//   ChildGlobalAtBinding = ParentGlobalAtBinding * ChildLocal
			FbxAMatrix ChildGlobalAtBinding = ParentGlobalAtBinding * ChildLocal;
			// convert from FbxAMatrix to FbxMatrix
			FbxMatrix globBind(ChildGlobalAtBinding);
			// set inverse of bind pose transformation
			Matrix4x4 aMayaBind = toMatrix4(globBind).transpose();
			_mesh.m_invBindMatrices[child->m_id] = aMayaBind.inverse();
		}
	}
}

///<
void FBXImporter::loadBoneWeights(Skeleton& _sk, SkinnedMesh& _mesh, FbxMesh* _pFBXMesh)
{
	printf("Loading Bone weights...\n"); 
	int numJoints = _sk.numJoints();
	{
		_mesh.m_skinningMatrices.resize(numJoints);
		_mesh.m_invBindMatrices.clear();
		_mesh.m_invBindMatrices.resize(numJoints);

		for (int i = 0; i < (int)_mesh.m_skinningMatrices.size(); i++)
			_mesh.m_invBindMatrices[i] = Matrix4x4::Identity();

		_sk.m_bUseMayaBindPose = false;
		if (_sk.m_bUseMayaBindPose)
		{
			getAndSetMayaBindPose(_sk, _mesh, _pFBXMesh);
			setMissingJointsInBindPose(_sk, _mesh, _pFBXMesh);
		}
		else
		{
			Matrix4x4 nodeInv = _mesh.m_nodeTransform.getMatrix();
			/// Use the local transforms of the first frame:
			for (int i = 0; i < (int)_mesh.m_skinningMatrices.size(); i++)
			{
				Skeleton::Joint* pJoint = _sk.getJoint(i);
				Matrix4x4 invBindM = _sk.getJointTransformationGlobal(pJoint).inverse();
				_mesh.m_invBindMatrices[i] = invBindM*nodeInv;
			}
		}			
	}

  
    // ------------------------------------------------------------------------------------------
    // Skinning weights		   --------------------------------------------------------------
    // ------------------------------------------------------------------------------------------

	FbxSkin* pSkin = getSkinDeformer();
	if (pSkin == NULL)
		return;

	int numClusters = pSkin->GetClusterCount();
	for(int i=0; i<numClusters; ++i)
	{		
		FbxCluster* cluster = pSkin->GetCluster(i);

		std::string strBoneName = cluster->GetLink()->GetName();

		Skeleton::Joint* pJoint = _sk.FindJoint(strBoneName);

		///< Skinning weights: vertices are mapped to different bones.  
		///< Here we go through bones and its associated vertices.
		int numVerticesAffectedByBone_i = cluster->GetControlPointIndicesCount();
		{
			int* boneVertexIndices = cluster->GetControlPointIndices();
			double* boneVertexWeights = cluster->GetControlPointWeights();

			// For a given bone, find the vertices affected by it
			for (int j = 0; j < numVerticesAffectedByBone_i; ++j)
			{
				int vertIndex = boneVertexIndices[j];

				SkinnedMesh::FVertex& fVert = _mesh.m_sourceVertices[vertIndex];

				int& boneCount = _mesh.m_boneCountsPerVertex[vertIndex];
				if (boneCount < 4)
				{
					fVert._boneIds[boneCount] = (GLfloat)pJoint->m_id;
					fVert._boneWeights[boneCount] = static_cast<GLfloat> (boneVertexWeights[j]);
					boneCount++;
				}
				else
				{
					Logger::consolePrint("bones have more than 4 influence !");
				}
			}
		}
	}

	printf("Loading Bone weights finished.\n");
}

void FBXImporter::createSkeletonAndMesh(Skeleton& _sk, SkinnedMesh** _ppMesh)
{
	createSkeleton(_sk, m_pRootNode);
	createMesh(_sk, _ppMesh);
	_sk.setPhaseTime(0.0);
}

///<
void FBXImporter::createSkeleton(Skeleton& _sk)
{
	createSkeleton(_sk, m_pRootNode);
	_sk.setPhaseTime(0.0);
}

///<
void FBXImporter::createSkeleton(Skeleton& _sk, FbxNode* pRootNode)
{
	FbxNode* pSkeletonNode = NULL;
	findFirstAttribute(pRootNode, FbxNodeAttribute::eSkeleton, &pSkeletonNode);

	if (pSkeletonNode != NULL)
	{
		const char* strNodeName = pSkeletonNode->GetName();

		AnimationImport temp;
		IttParseHierarchy(pSkeletonNode, temp, strNodeName);

		{
			///<
			_sk.m_pRoot = new Skeleton::Joint();
			_sk.m_pRoot->m_strName = temp.m_root.m_name;

			_sk.m_pRoot->m_q = temp.m_root.m_q.toUnit();
			_sk.m_pRoot->m_offset = temp.m_root.m_t;



			_sk.m_pRoot->m_id = 0;
			_sk.m_pRoot->m_pPrevious = NULL;

			_sk.m_pRoot->m_childs.resize(temp.m_root.m_childs.size());
			int id = 0;
			for (int j = 0; j < (int)temp.m_root.m_childs.size(); ++j)
			{
				_sk.m_pRoot->m_childs[j].m_pPrevious = _sk.m_pRoot;
				fillJoint(_sk.m_pRoot->m_childs[j], temp.m_root.m_childs[j], id);
			}
		}

		

		loadMotion(_sk);
	}
}

///<
void BuildHierarchy(FbxNode* _pFbxNode, AnimationImport::Bone* _pBone, const bool _bIsRoot)
{
	if (_pFbxNode)
	{
		if (_pFbxNode->GetChildCount()>0)
			_pBone->m_childs.resize(_pFbxNode->GetChildCount());


		TransformGetter mA = TransformGetter::get(0, _pFbxNode);
		_pBone->m_q = mA.m_q;
		_pBone->m_t = mA.m_x;

		{
			_pBone->m_l = 1.0;
		}

		for (int j = 0; j<_pFbxNode->GetChildCount(); ++j)
		{
			_pBone->m_childs[j].m_name = std::string(_pFbxNode->GetChild(j)->GetName());
			BuildHierarchy(_pFbxNode->GetChild(j), &_pBone->m_childs[j], false);
		}
	}
}

///<
void CountBones(AnimationImport::Bone* _pBone, int& _iCount)
{
	if (_pBone)
	{
		_iCount += _pBone->m_childs.size();
		for (int j = 0; j<(int)_pBone->m_childs.size(); ++j)
			CountBones(&_pBone->m_childs[j], _iCount);
	}
}

///<
void PopulateBones(AnimationImport::Bone* _pBone, std::vector<AnimationImport::Bone*>& _bones, int& _index)
{
	if (_pBone)
	{
		for (int i = 0; i<(int)_pBone->m_childs.size(); ++i)
		{
			_pBone->m_childs[i].m_id = _index;
			_bones[_pBone->m_childs[i].m_id] = &_pBone->m_childs[i];
			_index++;
			PopulateBones(&_pBone->m_childs[i], _bones, _index);
		}
	}
}

///<
void BuildArray(AnimationImport& _animData)
{
	int boneCount = 1;
	CountBones(&_animData.m_root, boneCount);
	if (boneCount>0)
	{
		_animData.m_boneArray.resize(boneCount);

		_animData.m_root.m_id = 0;

		_animData.m_boneArray[_animData.m_root.m_id] = &_animData.m_root;
		int index = 1;
		PopulateBones(_animData.m_boneArray[0], _animData.m_boneArray, index);
	}
}

///<
void FBXImporter::IttParseHierarchy(FbxNode* _pFbxNode, AnimationImport& _animImport, const char* _csRootName)
{
	if (_pFbxNode->GetNodeAttribute())
	{
		if (_pFbxNode->GetNodeAttribute()->GetAttributeType() == FbxNodeAttribute::eSkeleton)
		{
			///< Build Hierarchy !

			const char* strName = _pFbxNode->GetName();
			_animImport.m_root.m_name = std::string(strName);

			BuildHierarchy(_pFbxNode, &_animImport.m_root, true);
			BuildArray(_animImport);
		}
	}
}


// It gets the node Type of a Node, it is being used to determine whether or not the current Node is a Skeleton
std::string FBXImporter::nodeTypeName(FbxNodeAttribute::EType attrType)
{
	switch (attrType)
	{
	case FbxNodeAttribute::eUnknown:
		return "eUnknown";
		break;
	case FbxNodeAttribute::eNull:
		return "eNull";
		break;
	case FbxNodeAttribute::eMarker:
		return "eMarker";
		break;
	case FbxNodeAttribute::eSkeleton:
		return "eSkeleton";
		break;
	case FbxNodeAttribute::eMesh:
		return "eMesh";
		break;
	case FbxNodeAttribute::eNurbs:
		return "eNurbs";
		break;
	case FbxNodeAttribute::ePatch:
		return "ePatch";
		break;
	case FbxNodeAttribute::eCamera:
		return "eCamera";
		break;
	case FbxNodeAttribute::eCameraStereo:
		return "eCameraStereo";
		break;
	case FbxNodeAttribute::eCameraSwitcher:
		return "eCameraSwitcher";
		break;
	case FbxNodeAttribute::eLight:
		return "eLight";
		break;
	case FbxNodeAttribute::eOpticalReference:
		return "eOpticalReference";
		break;
	case FbxNodeAttribute::eOpticalMarker:
		return "eOpticalMarker";
		break;
	case FbxNodeAttribute::eNurbsCurve:
		return "eNurbsCurve";
		break;
	case FbxNodeAttribute::eTrimNurbsSurface:
		return "eTrimNurbsSurface";
		break;
	case FbxNodeAttribute::eBoundary:
		return "eBoundary";
		break;
	case FbxNodeAttribute::eNurbsSurface:
		return "eNurbsSurface";
		break;
	case FbxNodeAttribute::eShape:
		return "eShape";
		break;
	case FbxNodeAttribute::eLODGroup:
		return "eLODGroup";
		break;
	case FbxNodeAttribute::eSubDiv:
		return "eSubDiv";
		break;
	case FbxNodeAttribute::eCachedEffect:
		return "eCachedEffect";
		break;
	case FbxNodeAttribute::eLine:
		return "eLine";
		break;
	default:
		return "NULL";
		break;
	}
}
// It gets the Skeleton type (This property is not being taken into account)
std::string FBXImporter::skeletonTypeName(FbxSkeleton::EType skeletonType)
{
	switch (skeletonType)
	{
	case FbxSkeleton::EType::eEffector:
		return "eEffector";
		break;
	case FbxSkeleton::EType::eLimb:
		return "eLimb";
		break;
	case FbxSkeleton::EType::eLimbNode:
		return "eLimbNode";
		break;
	case FbxSkeleton::EType::eRoot:
		return "eRoot";
		break;
	default:
		return "NULL";
		break;
	}
}

// It gets the local tranformation data from the current pNode for the m_fbxCurrentTime
// The information is saved to the tChild, qChild and rChild variables
void FBXImporter::getNodeLocalTransform(FbxNode* pNode, FbxTime m_fbxCurrentTime, V3D &tChild, Quaternion &qChild, V3D &rChild)
{
	FbxAMatrix &lLocalTransform = pNode->EvaluateLocalTransform(m_fbxCurrentTime, PIVOT1);
	FbxAMatrix T;
	T.SetT(lLocalTransform.GetT());
	FbxAMatrix Roff;
	Roff.SetT(pNode->GetRotationOffset(PIVOT1));
	FbxAMatrix Rp;
	Rp.SetT(pNode->GetRotationPivot(PIVOT1));
	FbxAMatrix Rpre;
	Rpre.SetR(pNode->GetPreRotation(PIVOT1));
	FbxAMatrix R;
	R.SetR(lLocalTransform.GetR());
	FbxAMatrix Rpost;
	Rpost.SetR(pNode->GetPostRotation(PIVOT1));
	FbxAMatrix Soff;
	Soff.SetT(pNode->GetScalingOffset(PIVOT1));
	FbxAMatrix Sp;
	Sp.SetR(pNode->GetScalingPivot(PIVOT1));
	FbxAMatrix S;
	S.SetS(lLocalTransform.GetS());

	FbxVector4 lT, lR, lS;
	FbxAMatrix lGeometry;

	lT = pNode->GetGeometricTranslation(PIVOT1);
	lR = pNode->GetGeometricRotation(PIVOT1);
	lS = pNode->GetGeometricScaling(PIVOT1);

	lGeometry.SetT(lT);
	lGeometry.SetR(lR);
	lGeometry.SetS(lS);


	//this equation comes from the KFbxpNode doxygen
	//Maya and FBX
	// http://help.autodesk.com/view/FBX/2015/ENU/?guid=__files_GUID_10CDD63C_79C1_4F2D_BB28_AD2BE65A02ED_htm
	FbxAMatrix localTransform = T * Roff * Rp * Rpre * R * Rpost.Inverse() * Rp.Inverse()* Soff * Sp * S * Sp.Inverse();


	FbxVector4 fbxLocalPos = localTransform.GetT();
	FbxDouble  *pos = fbxLocalPos.Buffer();
	FbxVector4 fbxLocalRot = localTransform.GetR();
	FbxDouble  *rot = fbxLocalRot.Buffer();
	double   *quat = localTransform.GetQ();

	tChild = V3D(pos[0], pos[1], pos[2]);
	qChild = Quaternion(quat[3], quat[0], quat[1], quat[2]);
	rChild = V3D(rot[0], rot[1], rot[2]);
}


