

#include <FBXReaderLib/Skeleton.hpp>
#include <GUILib/GLUtils.h>
#include <Utils/Utils.h>


Skeleton::Joint::~Joint()
{
	delete m_pChildRigidBodyName;
}


Skeleton::Joint& Skeleton::Joint::operator=(const Skeleton::Joint& _jt)
{
	m_id = _jt.m_id;
	m_strName = _jt.m_strName;
	m_offset = _jt.m_offset;
	m_q = _jt.m_q;

	m_qPre = _jt.m_qPre;
	m_q0 = _jt.m_q0;

	m_pPrevious = NULL;

	if (_jt.m_pChildRigidBodyName)
	{
		m_pChildRigidBodyName = new std::string();
		*m_pChildRigidBodyName = *_jt.m_pChildRigidBodyName;
	}

	m_childs.clear();
	m_childs.resize(_jt.m_childs.size());
	for (int i = 0; i < (int)m_childs.size(); ++i)
	{
		m_childs[i] = _jt.m_childs[i];
		m_childs[i].m_pPrevious = this;
	}

	return *this;
}

///<
P3D Skeleton::ComputeCOM()
{
	P3D xCom;

	int numJoints = size();
	if (numJoints > 0)
	{
		for (int i = 0; i < numJoints; ++i)
		{
			Skeleton::Joint* pJoint = FindJoint(i);
			xCom += X(pJoint);
		}
		xCom = xCom*(1.0 / (double)numJoints);
	}

	return xCom;
}


void Skeleton::setDiscreteTimeIndex(int _i)
{
	assert(_i >= 0 && _i < numKeyFrames());

	m_currentFrameIndex = _i;

	setPoseToFrameIndex(m_currentFrameIndex);
}

void Skeleton::setPhaseTime(double _t)
{
	m_currentFrameIndex = (int)(_t*((double)numKeyFrames()-1));

	if (numKeyFrames() > 1)
	{
		SkeletonTrajectory skT(*this);
		this->copy(skT.getInterpolatedSkeleton(_t));
	}
}

double Skeleton::getPhaseTime()
{
	return (double)m_currentFrameIndex / (double)(numKeyFrames()-1);
}

///<
void Skeleton::updateAnimationTimeCycling(double _speedFactor)
{
	if (updateSkipFactor(_speedFactor))
	{
		if (numKeyFrames() > 0)
		{
			m_currentFrameIndex = (m_currentFrameIndex + 1) % numKeyFrames();

		}
		
		setPoseToFrameIndex(m_currentFrameIndex);
	}
}

///<
bool Skeleton::updateSkipFactor(double _speedFactor)
{
	int discreteNumSkipFrames = 0;
	if (_speedFactor>0.0)
		discreteNumSkipFrames = (int)(1.0 / _speedFactor); 

	if (m_skipFactor >= discreteNumSkipFrames - 1)
	{
		m_skipFactor = 0;
		return true;
	}		
	else
	{
		m_skipFactor++;
		return false;
	}
}

///<
void Skeleton::updateAnimationTilFinished_Continuous(const double _simTimeStep)
{

	m_continuousTimer.updateTilFinished(_simTimeStep);
	double t = m_continuousTimer.getPhase();
	setPhaseTime(t);
}

///<
void Skeleton::updateAnimationTilFinished(double _speedFactor)
{
	/*Let's do a continuous version*/
	if (updateSkipFactor(_speedFactor))
	{
		if (numKeyFrames() > 0)
		{
			if (m_currentFrameIndex < (int)numKeyFrames() - 1)
				m_currentFrameIndex += 1;

			setPoseToFrameIndex(m_currentFrameIndex);
		}
	}	
}

///< Not used so much:
void Skeleton::updateAnimationIndexFwdAndReverse()
{
	m_currentFrameIndex += m_ordering;

	if (m_currentFrameIndex <= -1)
	{
		m_currentFrameIndex = 0;
		m_ordering = 1;
	}
	else if (m_currentFrameIndex >= (int)m_X.size())
	{
		m_ordering = -1;
		m_currentFrameIndex = numKeyFrames() - 1;
	}

	setPoseToFrameIndex(m_currentFrameIndex);
}

int	Skeleton::numJoints() const { return size(); }
int	Skeleton::numKeyFrames() { return m_X.size(); }


///<
void Skeleton::copyWithoutRotations(Skeleton& _skeleton)
{
	copy(_skeleton);

	for (int i = 0; i < numJoints(); ++i)
	{
		Joint* pJoint = FindJoint(i);

		pJoint->m_q = Quaternion();
		pJoint->m_q0 = Quaternion();
		pJoint->m_qPre = Quaternion();
	}

	for (int i = 0; i < numJoints(); ++i)
	{
		V3D dx = _skeleton.X(i)- X(i);

		Joint* pJoint = FindJoint(i);

		pJoint->m_offset += dx;
	}
	
}

///<
void Skeleton::copy(const Skeleton& _sk)
{
	ASSERT(_sk.m_pRoot != NULL, "Skeleton not init");

	if (!m_pRoot)
		m_pRoot = new Joint();


	*m_pRoot = *_sk.m_pRoot;


	m_scaleDanger = _sk.m_scaleDanger;

}

///<
void Skeleton::setPoseToFrameIndex(int _i)
{
	if (_i >= 0 && _i < (int)m_X.size())
	{
		int iNext = _i + 1;
		if (iNext >= (int)m_X.size())
			iNext = (int)m_X.size() - 1;

		int iPrev = _i - 1;
		if (iPrev < 0)
			iPrev = 0;

		m_pRoot->m_offset = m_X[_i];
		std::vector<Quaternion>& Q = m_Q[_i];
		std::vector<Quaternion>& QNext = m_Q[iNext];
		std::vector<Quaternion>& QPrev = m_Q[iPrev];

		m_pRoot->m_q = Q[0];

		int numJoints = size();
		for (int i = 1; i < numJoints; ++i)
		{
			Joint* pJoint = NULL;
			FindJoint(i, m_pRoot, &pJoint);
			pJoint->m_q = Q[pJoint->m_id];
		}
	}
}

///<
std::vector<P3D> Skeleton::computeTrajectoryForJoint(const Joint* _pJoint)
{
	std::vector<P3D> traj;
	traj.resize(m_X.size());

	for (int i = 0; i < (int)traj.size(); ++i)
	{
		setPoseToFrameIndex(i);
		traj[i] = X(const_cast<Joint*>(_pJoint));
	}

	setPoseToFrameIndex(m_currentFrameIndex);

	return traj;
}

///<
void Skeleton::drawJointTrajectory(const Joint* _pJoint)
{
	if (!_pJoint)
		return;

	glDisable(GL_LIGHTING);
	glColor3d(1, 0, 0);
	glLineWidth(3.0);
	glBegin(GL_LINES);

	std::vector<P3D> x = computeTrajectoryForJoint(_pJoint);

	for (int i = 1; i <(int)x.size(); ++i)
	{
		P3D p = P3D() + x.at(i - 1);
		P3D p_n = P3D() + x.at(i);
		glVertex3d(p[0], p[1], p[2]);
		glVertex3d(p_n[0], p_n[1], p_n[2]);
	}

	glEnd();
	glLineWidth(1.0);
	glEnable(GL_LIGHTING);
}

void Skeleton::drawJointsSpheres()
{
	int numJoints = size();
	for (int i = 0; i < numJoints; ++i)
	{
		Skeleton::Joint* pJoint = FindJoint(i);
		drawSphere(X(pJoint), 0.03);
	}


}

///<
void Skeleton::drawLinks()
{
	/// For each bone: joint and child, draw bone.
	int numJoints = size();
	for (int i = 0; i < numJoints; ++i)
	{
		Skeleton::Joint* pJoint = FindJoint(i);
		for (int k = 0; k < (int)pJoint->m_childs.size(); ++k)
		{
			Skeleton::Joint* pChild = &pJoint->m_childs[k];
			drawCone(X(pJoint), X(pChild) - X(pJoint), 0.01);
		}
	}
}

///<
void Skeleton::drawLinksAndJointSpheres(double _linksScale, double _jointsScale)
{
	// Draw Skeleton
	int numJoints = size();
	for (int i = 0; i < numJoints; ++i)
	{
		Joint* pJoint = FindJoint(i);
		P3D jPos = X(pJoint);

		if (pJoint->m_pPrevious)
		{			
			P3D jPosPrev = X(pJoint->m_pPrevious);

			glColor3d(0.3, 0.6, 1);
			drawLink(jPos, jPosPrev, _linksScale*3.0*m_scaleDanger);
		}

		glColor3d(1.0, 0.6, 0.2);
		drawSphere(jPos, _jointsScale*0.03*m_scaleDanger);
	}
}

///<
void Skeleton::drawLink(P3D& _start, P3D& _end, float _thickness)
{
	glLineWidth(_thickness);
	glDisable(GL_LIGHTING);
	glBegin(GL_LINES);
	glVertex3d(_start.at(0), _start.at(1), _start.at(2));
	glVertex3d(_end.at(0), _end.at(1), _end.at(2));
	glEnd();
	glEnable(GL_LIGHTING);
}

///<
Skeleton::Joint* Skeleton::FindJoint(const std::string& _name)
{
	Skeleton::Joint* pJoint = NULL;
	FindJoint(_name, m_pRoot, &pJoint);
	return pJoint;
}

///<
Skeleton::Joint* Skeleton::FindJoint(int _index)
{
	Skeleton::Joint* pJoint = NULL;
	FindJoint(_index, m_pRoot, &pJoint);
	return pJoint;
}

///<
Skeleton::Joint* Skeleton::FindRigidBodyParentJoint(const std::string& _name)
{
	bool bLookForRigidBody = true;
	Skeleton::Joint* pJoint = NULL;
	FindJoint(_name, m_pRoot, &pJoint, bLookForRigidBody);
	return pJoint;
}

///<
void Skeleton::FindJoint(const std::string& _name, Joint* _pJoint, Joint** _ppFound, bool _bRB)
{
	if (_bRB && _pJoint->m_pChildRigidBodyName)
	{
		if (*_pJoint->m_pChildRigidBodyName == _name)
			*_ppFound = _pJoint;
	}
	else
	{
		if (_pJoint->m_strName == _name)
			*_ppFound = _pJoint;
	}

	for (int i = 0; i < (int)_pJoint->m_childs.size(); ++i)
	{
		FindJoint(_name, &_pJoint->m_childs[i], _ppFound, _bRB);
	}
}

///<
void Skeleton::FindJoint(int _index, Joint* _pJoint, Joint** _ppFound)
{
	if (_pJoint->m_id == _index)
		*_ppFound = _pJoint;

	for (int i = 0; i < (int)_pJoint->m_childs.size(); ++i)
	{
		FindJoint(_index, &_pJoint->m_childs[i], _ppFound);
	}
}

///<
P3D	 Skeleton::GetLimbPosition(Joint* _pJoint)
{
	Joint* pParent = _pJoint->m_pPrevious;

	V3D dx = X(_pJoint) - X(pParent);
	return X(pParent) + dx*0.5;
}

///<
P3D	 Skeleton::GetLimbPosition(const char* _csName)
{
	Joint* pJoint = FindJoint(_csName);
	assert(pJoint != NULL);
	return GetLimbPosition(pJoint);
}

///<
V3D Skeleton::getJointPos(Joint* _pJoint)
{
	V3D dx;
	if (_pJoint)
		while (_pJoint->m_pPrevious != NULL)
		{
			V3D offs = _pJoint->m_offset;
			dx = _pJoint->m_pPrevious->m_q.rotate(offs + dx);

			_pJoint = _pJoint->m_pPrevious;
		}

	dx += m_pRoot->m_offset;
	return dx;
}

///<
P3D Skeleton::X(const char* _csName)
{
	Joint* pJoint = NULL;
	FindJoint(std::string(_csName), m_pRoot, &pJoint);
	return X(pJoint);
}

///<
P3D Skeleton::X(int _jointIndex)
{
	Joint* pJoint = FindJoint(_jointIndex);
	return X(pJoint);
}

P3D Skeleton::X_BindPose(Joint* _pJoint)
{
	if(!m_bUseMayaBindPose)
		return X(_pJoint);

	Matrix4x4 bindI = m_bindMatrices[_pJoint->m_id].inverse();

	P3D x;
	for (int k = 0; k < 3; ++k)
		(double)x[k] = bindI(k, 3);
	

	return x;
}

///<
P3D	Skeleton::X(Joint* _pJoint)
{
	
	return P3D() + getJointPos(_pJoint) * m_scaleDanger;
}



///<
Quaternion Skeleton::Q(Joint* _pJoint)
{
	if (_pJoint == m_pRoot)
		return _pJoint->m_q;
	
	Quaternion Q_i_minus_one_to_Q_i;
	if (_pJoint)
	{
		Q_i_minus_one_to_Q_i = _pJoint->m_q;
		while (_pJoint->m_pPrevious != NULL)
		{
			_pJoint = _pJoint->m_pPrevious;

			///< q_0*q_1*q_2 (0 is root)			
			Quaternion qi_minus_one_or_parent = _pJoint->m_q;
			Q_i_minus_one_to_Q_i = qi_minus_one_or_parent*Q_i_minus_one_to_Q_i;
		}
	}
	
	return Q_i_minus_one_to_Q_i;
}

///<
Quaternion Skeleton::Q(int _jointIndex)
{
	Joint* pJoint = FindJoint(_jointIndex);
	return Q(pJoint);
}

Quaternion Skeleton::Q(const char* _csName)
{
	Joint* pJoint = FindJoint(_csName);
	return Q(pJoint);
}

Matrix4x4 Skeleton::getJointTransformationGlobal(Joint* _pJoint) 
{
	Quaternion _jQuad = Q(_pJoint->m_id);
	V3D _jpos = X(_pJoint->m_id);

	Matrix3x3 r = _jQuad.getRotationMatrix();

	Matrix4x4 m, mS;
	m.setIdentity();
	mS.setIdentity();

	for (int i = 0; i < 3; ++i)
		mS(i, i) = m_scaleDanger;

	for (int i = 0; i < 3; ++i)
		for (int j = 0; j < 3; ++j)
			m(i, j) = (double)r(i, j);

	m = mS*m;// scale is already included in position.

	for (int i = 0; i < 3; ++i)
		m(i, 3) = (double)_jpos[i];

	return m;
}

Matrix4x4 Skeleton::getJointTransformationGlobal(int _jointIndex)
{
	Joint* pJoint = FindJoint(_jointIndex);
	return getJointTransformationGlobal(pJoint);
}

Skeleton::TransformationContainer*	Skeleton::getCurrentGlobalTransforms()
{
	if (m_pCurrGlobalBoneTrans == NULL)
		m_pCurrGlobalBoneTrans = new TransformationContainer();

	updateGlobalBoneTransforms(*m_pCurrGlobalBoneTrans);

	return m_pCurrGlobalBoneTrans;
}

void Skeleton::updateGlobalBoneTransforms(TransformationContainer& _globalTransforms) 
{
	int boneCount = size();
	if (_globalTransforms.size() != boneCount)
		_globalTransforms.resize(boneCount);

	for (int i = 0; i < boneCount; i++)
		_globalTransforms[i] = getJointTransformationGlobal(i);

}

#include <MathLib/Trajectory.h>

SkeletonTrajectory::SkeletonTrajectory(Skeleton& _skMotion) : m_pSkeletonMotion(&_skMotion)
{
	m_pSmoothSkeleton = new Skeleton();

	///< Copy the skeleton:
	m_pSmoothSkeleton->copy(_skMotion);

	///< Create one key frame as buffer:
	m_pSmoothSkeleton->m_Q.resize(1);
	m_pSmoothSkeleton->m_X.resize(1);

	Skeleton::QVec& jointAngles = m_pSmoothSkeleton->m_Q[0];
	jointAngles.resize(_skMotion.numJoints());


}

SkeletonTrajectory::~SkeletonTrajectory()
{
	delete m_pSmoothSkeleton;
}

///<
Skeleton& SkeletonTrajectory::getInterpolatedSkeleton(const double _t)
{
	
	if (m_pSmoothSkeleton->m_Q.size() == 1)
		return *m_pSmoothSkeleton;

	///< Q
	Skeleton::QVec& jointAngles = m_pSmoothSkeleton->m_Q[0];
	{
		for (int i = 0; i<m_pSkeletonMotion->numJoints(); ++i)
		{
			Trajectory3D trajQ;

			for (int k = 0; k < m_pSkeletonMotion->numKeyFrames(); ++k)
			{
				double tk = (double)k / (double)(m_pSkeletonMotion->numKeyFrames() - 1);
				//#include <MathLib/Quaternion.h>
				trajQ.addKnot(tk, m_pSkeletonMotion->m_Q[k][i].toAxisAngle());
			}

			if(trajQ.getKnotCount() > 0)
				jointAngles[i] = Quaternion::ExpMap(trajQ.evaluate_linear(_t));//toUnit();

			//q_t.push_back(traj.evaluate_catmull_rom(t));
		}
	}
	
	///< X
	{
		Trajectory3D trajX;
		for (int k = 0; k < m_pSkeletonMotion->numKeyFrames(); ++k)
		{
			double tk = (double)k / (double)(m_pSkeletonMotion->numKeyFrames() - 1);
			trajX.addKnot(tk, m_pSkeletonMotion->m_X[k]);
		}

		if (trajX.getKnotCount() > 0)
			m_pSmoothSkeleton->m_X[0] = trajX.evaluate_linear(_t);

	}

	m_pSmoothSkeleton->setPoseToFrameIndex(0);
	return *m_pSmoothSkeleton;
}


///<
void Skeleton::IncrementSize(Joint* _pJoint, int& _size)
{
	if (_pJoint)
		for (int i = 0; i < (int)_pJoint->m_childs.size(); ++i)
		{
			IncrementSize(&_pJoint->m_childs[i], _size);
			_size++;
		}
}

int Skeleton::size() const
{
	int size = 1;
	if (m_pRoot)
		IncrementSize(m_pRoot, size);

	return size;
}

Skeleton::~Skeleton() {
	if (m_pRoot)
		delete m_pRoot;

	delete m_pCurrGlobalBoneTrans;
}



///<
void Skeleton::setPoseRaw(const dVector& _p)
{
	int numJoints = size();
	for (int i = 0; i < numJoints; ++i)
	{
		Quaternion& q = FindJoint(i)->m_q;
		for (int k = 0; k < 4; ++k)
			q[k] = _p[i * 4 + k];
	}
}

///<
void Skeleton::getPoseRaw(dVector& _p)
{
	int numJoints = size();
	for (int i = 0; i < numJoints; ++i)
	{
		Quaternion& q = FindJoint(i)->m_q;
		for (int k = 0; k < 4; ++k)
			_p[i * 4 + k] = q[k];
	}
}



/// Adjust root position based on lowest joint to be on ground.
void Skeleton::setToGround() 
{
	m_pRoot->m_offset[1] = 0;
	// find minimum position
	V3D minJointPosition = m_pRoot->m_offset;
	for (int i = 1; i < numJoints(); ++i)
	{
		V3D jointPos = X(getJoint(i));
		if (jointPos[1] < minJointPosition[1])
			minJointPosition = jointPos;
	}
	m_pRoot->m_offset[1] = -minJointPosition[1];
}

///<
void Skeleton::setScaleForRootHeightInWorldToBe(double _s)
{
	setScaleDanger(1.0);
	double sd = X(m_pRoot)[1];

	if (abs(sd) > 0.01) 
		setScaleDanger(_s / sd);

}