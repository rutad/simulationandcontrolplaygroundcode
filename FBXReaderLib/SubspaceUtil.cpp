#pragma once

#include <FBXReaderLib\SubspaceUtil.hpp>

struct MaxRealValueSmallerThan
{
	double m_maxValue;
	MaxRealValueSmallerThan(double _max) : m_maxValue(_max) {}
	bool operator()(const std::complex<double>& _c1, const std::complex<double>& _c2)
	{
		return (abs(_c1.real()) < abs(_c2.real()) && abs(_c1.real()) < m_maxValue);
	}
};

///<
void FindMaxIndex(const Eigen::EigenSolver< Eigen::MatrixXd >::EigenvalueType& _v, std::vector<int>& _indices)
{
	double max = 0;
	int maxId = 0;
	for (int i = 0; i < _v.size(); ++i)
	{
		if (abs(_v(i).real()) > max && std::find(_indices.begin(), _indices.end(), i) == _indices.end())
		{
			maxId = i;
			max = abs(_v(i).real());
		}
	}

	_indices.push_back(maxId);
}

///<

void SubspaceUtil::ComputeAndRemoveMean(Eigen::MatrixXd& _A)
{
	if (_A.cols() > 0)
	{
		Eigen::MatrixXd average = _A.col(0);
		average.fill(0);
		for (int j = 0; j < _A.cols(); ++j)
		{
			Eigen::MatrixXd cJ = _A.col(j);
			average += cJ;
		}
		average = average / (_A.cols());

		m_average = average;

		///< Colum-wise substraction
		for (int j = 0; j < _A.cols(); ++j)
		{
			for (int i = 0; i < 3; ++i)
			{
				_A(i, j) -= m_average(i);
			}
		}
	}
}

//<
void SubspaceUtil::GetMaxEigenVectors(const Eigen::MatrixXd& _A, const int _numEigenModes)
{
	assert(_numEigenModes <= _A.rows());

	///< 2 Eigen Vecs (a_0, a_1): [3 + 3xM, 2]
	Eigen::MatrixXd AA = _A*_A.transpose();
	Eigen::EigenSolver< Eigen::MatrixXd  > eSolve(AA);
	Eigen::EigenSolver< Eigen::MatrixXd >::EigenvalueType eV = eSolve.eigenvalues();
	EigenVector eVec = eSolve.eigenvectors();

	///<
	std::vector<int> maxIndices;
	for (int i = 0; i < _numEigenModes; ++i)
		FindMaxIndex(eV, maxIndices);

	Eigen::MatrixXd P(_A.rows(), _numEigenModes);
	P.fill(0);
	for (int i = 0; i < _numEigenModes; ++i)
	{
		int index = maxIndices[i];
		for (int j = 0; j < P.rows(); ++j)
			P(j, i) = eVec(j, index).real();
	}
	m_P = P;
}

///<
void SubspaceUtil::ComputeSkeletonSubspace(const Skeleton& _skAnim, SubspaceUtil& _x, SubspaceUtil& _q, int _numDimX, int _numDimQ)
{
	Eigen::MatrixXd mXFrames, mQFrames;

	int numPoses = _skAnim.m_X.size();
	int numJoints = _skAnim.m_Q[0].size();

	///< Root frames: 3xN
	mXFrames.resize(3, numPoses);
	mXFrames.fill(0);

	///< Angle frames: 3xM xN
	mQFrames.resize(4 * numJoints, numPoses);

	///< Fill the poses
	for (int i = 0; i < numPoses; ++i)
	{
		for (int k = 0; k < 3; ++k)
		{
			V3D x = _skAnim.m_X[i];
			mXFrames(k, i) = x[k];
		}		

		const Skeleton::QVec& Q = _skAnim.m_Q[i];
		for (int j = 0; j < numJoints; ++j)
		{
			Quaternion qP = Q[j];
			for (int k = 0; k < 4; ++k)
				mQFrames(j * 4 + k, i) = qP[k];
		}
	}

	///<
	_x.ComputeAndRemoveMean(mXFrames);
	_x.GetMaxEigenVectors(mXFrames, _numDimX);


	_q.ComputeAndRemoveMean(mQFrames);
	_q.GetMaxEigenVectors(mQFrames, _numDimQ);
}



int SubspaceUtil::NumEigenVectors()const
{
	return m_P.cols();
}

Eigen::MatrixXd SubspaceUtil::GetEigenVector(const int _i) const
{
	return m_P.col(_i);
}

//Removes mean and projects.
Eigen::MatrixXd SubspaceUtil::Project(const Eigen::MatrixXd& _xFull) const
{
	Eigen::MatrixXd temp = _xFull - m_average;
	Eigen::MatrixXd xSub = m_P.transpose()*temp;
	return xSub;
}

///< Un projects and adds mean
Eigen::MatrixXd SubspaceUtil::UnProject(const Eigen::MatrixXd& _xSub) const
{
	Eigen::MatrixXd xFull = m_P*_xSub + m_average;
	return xFull;
}

///< Removes mean and adds it back.
Eigen::MatrixXd SubspaceUtil::ProjectAndUnproject(const Eigen::MatrixXd& _xFull) const
{
	Eigen::MatrixXd xReduced = Project(_xFull);	
	return UnProject(xReduced);
}

