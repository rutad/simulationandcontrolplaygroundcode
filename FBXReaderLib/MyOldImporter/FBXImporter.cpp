
#include <FBXReaderLib\FBXImporter.h>
#include <FBXReaderLib\FBXCharacter.h>


#define PIVOT0 FbxNode::eSourcePivot
#define PIVOT1 FbxNode::eDestinationPivot


///<
void FBXImporter::loadSkeleton(const char* _csFileName, Skeleton& _sk)
{
	Logger::consolePrint("FBX FILE: '%s' \n", _csFileName);
	FBXImporter importer(_csFileName);
	importer.createSkeleton(_sk);
	_sk.setPoseToFrame(_sk.m_currentFrameIndex);// Assuming starts at t=0
}

//It imports all the necessary information for drawing the scene from a FBX file
FBXImporter::FBXImporter(std::string fileName, double _mocapSamplingRate)
{
	sceneName = fileName;

	// Initialize an FBX SDK memory management object 
	fbxManager = FbxManager::Create();

	// Create an importer that imports the comtents of an FBX file into the scene
	FbxImporter *fbxImporter = FbxImporter::Create(fbxManager, "");

	if (!fbxImporter->Initialize(fileName.c_str()))
	{
		//Import Failed - LOG
		printf("Call to FbxImporter::Initialize failed\n");
		Logger::consolePrint("File: '%s' not found! \n", fileName);
	}

	// Create a new scene
	scene = FbxScene::Create(fbxManager, fileName.c_str());

	// Import the contents of the file into the scene
	fbxImporter->Import(scene);

	//Importing Scene Success -LOG
	initImporterAtts(fbxImporter, _mocapSamplingRate);

	// The file is imported, so clean up memory
	fbxImporter->Destroy();

	// Triangulate the mesh polygons
	FbxGeometryConverter geometryConverter(fbxManager);
	geometryConverter.Triangulate(scene, true);
}

FBXImporter::~FBXImporter()
{
	this->fbxManager->Destroy();
	this->fbxManager = NULL;
	this->joints.clear();
}

void FBXImporter::initImporterAtts(FbxImporter *fbxImporter, double _mocapSamplingRate)
{
	if (fbxImporter)
	{
		int animStacks = fbxImporter->GetAnimStackCount();

		// Create one animation stack
		//FbxAnimStack* lAnimStack = FbxAnimStack::Create(pScene, "Stack001");

		// Check if there ?s any take info, and assign 
		FbxTakeInfo* lCurrentTakeInfo = fbxImporter->GetTakeInfo(0);
		if (lCurrentTakeInfo)
		{
			startTime = lCurrentTakeInfo->mLocalTimeSpan.GetStart();
			stopTime = lCurrentTakeInfo->mLocalTimeSpan.GetStop();
		}
		else
		{
			FbxTimeSpan timeLineTimeSpan;
			scene->GetGlobalSettings().GetTimelineDefaultTimeSpan(timeLineTimeSpan);
			startTime = timeLineTimeSpan.GetStart();
			stopTime = timeLineTimeSpan.GetStop();
		}

		///< NUMBER OF FRAMES TO LOAD:
		// Usually 30 frames per second, 
		// MOCAP comes at 120 fps.
		// Use eCustom to set manual framerate 
		if (_mocapSamplingRate>0.0)
		{
			frameTime.SetTime(0, 0, 0, 1, 0, FbxTime::eCustom);// FbxTime::eFrames120);
			frameTime.SetGlobalTimeMode(frameTime.GetGlobalTimeMode(), _mocapSamplingRate);
		}
		else
		{
			FbxTime t = scene->GetGlobalSettings().GetTimeMode();
			printf("Mocap FrameRate : %f\n", t.GetFrameRate(FbxTime::eCustom));
			frameTime.SetTime(0, 0, 0, 1, 0, scene->GetGlobalSettings().GetTimeMode());
		}

		root = (FbxNode *)scene->GetRootNode();
		maxNumOfParts = root->GetChildCount(true);

		numOfPoses = (int)((stopTime.GetMilliSeconds() - startTime.GetMilliSeconds()) / frameTime.GetMilliSeconds()) + 1;
		joints = std::vector<FBXJoint>(maxNumOfParts + 1);

	}

}

// It basically generates an FBXCharacter (&agent) from an already declared FBXCharacter and the Node origin from the Fbx File
// The pNode is being set to the root Node of the FBX file, which means that it starts analyzing from the first one of the file
// This function works recursively from the pNode going through all the other ones ignoring the ones which are not Skeleton type
// The variables level, parentID and childK are only used for keeping the function working well in the hierarchy of the file
int FBXImporter::createAgent(FBXCharacter &agent, FbxNode * pNode, int level, int parentID, int childK)
{
	int jointID = 0;
	int i = 0;

	FbxNodeAttribute *nodeAttr = pNode->GetNodeAttribute();
	FbxString		  nodeType = (nodeAttr == NULL) ? FbxString("IS_NULL") : nodeTypeName(nodeAttr->GetAttributeType()).c_str();
	FbxString		  skeletonStr = FbxString("eSkeleton");
	int				  numOfChildren = pNode->GetChildCount();
	//printf("ID: %3d/%3d Name: %10s Type: %10s\n",this->numOfJoints,this->maxNumOfParts,pNode->GetName(),nodeType.operator const char *());
	if (nodeType.operator==(skeletonStr))
	{
		jointID = this->numOfJoints;
		this->joints.at(jointID) = FBXJoint(numOfChildren);
		this->joints.at(jointID).jointID = jointID;
		this->joints.at(jointID).jointParentID = parentID;
		this->joints.at(parentID).childrenIDs.at(childK) = jointID;
		//Getting Positions
		getJointPositions(agent, this->joints.at(jointID), pNode);
		this->numOfJoints++;

		agent.numOfBodyParts = this->numOfBodyParts;
		agent.numOfPoses = this->numOfPoses;
		agent.numOfJoints = this->numOfJoints;
	}
	//Iterating the scene for getting all the nodes
	for (int k = 0; k < numOfChildren; k++)
	{
		//FbxNode *child = pNode->GetChild(k);
		//if (child->GetNodeAttribute()->GetAttributeType() == FbxNodeAttribute::eSkeleton) i++;
		i++;
		//createAgent(agent, child, level + 1, jointID, i - 1);
		createAgent(agent, pNode->GetChild(k), level + 1, jointID, i - 1);
	}

	if (nodeType.operator==(skeletonStr) && numOfChildren>0)
	{
		BodyPart bodyPart;
		this->joints.at(jointID).childrenIDs.resize(i);
		if (!joints.at(jointID).jointID)
			bodyPart = BodyPart(i, 0);
		else
			bodyPart = BodyPart(i, this->numOfBodyParts + 1);

		fillBodyPart(bodyPart, joints.at(jointID));
		if (!joints.at(jointID).jointID)
			agent.root = bodyPart;
		else
			agent.links.at(this->numOfBodyParts) = bodyPart;

		this->numOfBodyParts++;
	}
	return numOfJoints;
}

///<
void FBXImporter::FillJoint(Skeleton::Joint& _jnt, const AnimationImport::Bone& _importedBone, int& _id)
{
	_id++;
	_jnt.m_id = _id;

	_jnt.m_strName = _importedBone.m_name;


	//_jnt.m_q = Quaternion::ExpMap(_importedBone.m_r);
	_jnt.m_q = _importedBone.m_q;
	_jnt.m_offset = _importedBone.m_t;

	_jnt.m_childs.resize(_importedBone.m_childs.size());

	for (int j = 0; j < (int)_importedBone.m_childs.size(); ++j)
	{
		_jnt.m_childs[j].m_pPrevious = &_jnt;
		FillJoint(_jnt.m_childs[j], _importedBone.m_childs[j], _id);
	}
}

///<  Could load less poses here, but the fbx reader loads them before anyway...
void FBXImporter::loadMotion(Skeleton& _sk /*, int _numPoses*/)
{
	_sk.m_X.resize(this->numOfPoses);
	_sk.m_Q.resize(this->numOfPoses);

	int numJoints = _sk.size();
	for (int i = 0; i < (int)_sk.m_Q.size(); ++i)
		_sk.m_Q[i].resize(numJoints);


	for (int i = 0; i < numJoints; ++i)
	{
		Skeleton::Joint* pJoint = NULL;
		_sk.FindJoint(i, _sk.m_pRoot, &pJoint);

		FbxNode* pNode = scene->FindNodeByName(pJoint->m_strName.c_str());

		this->currentTime = this->startTime;
		for (int poseNum = 0; poseNum < this->numOfPoses; poseNum++)
		{
			//Getting Local Coordinates
			//getNodeLocalTransform(pNode, currentTime, tChild, qChild, rChild);


			FbxAMatrix &lLocalTransform = pNode->EvaluateLocalTransform(currentTime, PIVOT1);
			FbxAMatrix T;
			T.SetT(lLocalTransform.GetT());
			FbxAMatrix Roff;
			Roff.SetT(pNode->GetRotationOffset(PIVOT1));
			FbxAMatrix Rp;
			Rp.SetT(pNode->GetRotationPivot(PIVOT1));
			FbxAMatrix Rpre;
			Rpre.SetR(pNode->GetPreRotation(PIVOT1));
			FbxAMatrix R;
			R.SetR(lLocalTransform.GetR());
			FbxAMatrix Rpost;
			Rpost.SetR(pNode->GetPostRotation(PIVOT1));
			FbxAMatrix Soff;
			Soff.SetT(pNode->GetScalingOffset(PIVOT1));
			FbxAMatrix Sp;
			Sp.SetR(pNode->GetScalingPivot(PIVOT1));
			FbxAMatrix S;
			S.SetS(lLocalTransform.GetS());

			FbxVector4 lT, lR, lS;
			FbxAMatrix lGeometry;

			lT = pNode->GetGeometricTranslation(PIVOT1);
			lR = pNode->GetGeometricRotation(PIVOT1);
			lS = pNode->GetGeometricScaling(PIVOT1);

			lGeometry.SetT(lT);
			lGeometry.SetR(lR);
			lGeometry.SetS(lS);


			//this equation comes from the KFbxpNode doxygen
			//Maya and FBX
			// http://help.autodesk.com/view/FBX/2015/ENU/?guid=__files_GUID_10CDD63C_79C1_4F2D_BB28_AD2BE65A02ED_htm
			FbxAMatrix localTransform = T * Roff * Rp * Rpre * R * Rpost.Inverse() * Rp.Inverse() * Soff * Sp * S * Sp.Inverse();
			//FbxAMatrix localTransform = T * Rpre * R * Rpost.Inverse();


			FbxVector4 fbxLocalPos = localTransform.GetT();
			FbxDouble  *pos = fbxLocalPos.Buffer();
			FbxVector4 fbxLocalRot = localTransform.GetR();
			FbxDouble  *rot = fbxLocalRot.Buffer();

			double   *quat = localTransform.GetQ();


			if (pJoint->m_id == 0)///< root
				_sk.m_X[poseNum] = V3D(pos[0], pos[1], pos[2]);

			bool ismaya = true;

			if (ismaya)
				_sk.m_Q[poseNum][pJoint->m_id] = Quaternion(quat[3], quat[0], quat[1], quat[2]).toUnit();
			//_sk.mQ[poseNum][pJoint->m_id] = Quaternion::ExpMap(V3D(rot[0], rot[1], rot[2])*(PI / 180.0));

			//V3D rDebug = V3D(rot[0], rot[1], rot[2]);

			this->currentTime += this->frameTime;
		}
	}
}

void FindFirstAttribute(FbxNode* _pNode, FbxNodeAttribute::EType _attribute, FbxNode** _ppFoundNode)
{
	if (_pNode != NULL)
	{
		if (_pNode->GetNodeAttribute() != NULL)
			if (_pNode->GetNodeAttribute()->GetAttributeType() == _attribute)
			{
				*_ppFoundNode = _pNode;
				return;
			}


		for (int i = 0; i < _pNode->GetChildCount(); i++)
		{
			FbxNode* pFbxChildNode = _pNode->GetChild(i);

			FindFirstAttribute(pFbxChildNode, _attribute, _ppFoundNode);
		}
	}

}

///<
void FBXImporter::loadMesh(Skeleton& _sk, FMesh& _mesh) {
	printf("Loading Mesh...\n");
	loadMesh(_sk, _mesh, root);
	printf("Loading Mesh finished.\n");
}

///<
void FBXImporter::loadMesh(Skeleton& _sk, FMesh& _mesh, FbxNode *pRootNode)
{
	FbxNode* pNode = NULL;
	FindFirstAttribute(pRootNode, FbxNodeAttribute::eMesh, &pNode);

	if (pNode != NULL)
	{
		assert(pNode->GetNodeAttribute()->GetAttributeType() == FbxNodeAttribute::eMesh);

		FbxMesh* pMesh = (FbxMesh*)pNode->GetNodeAttribute();

		// Create vertex array of the mesh
		{
			_mesh.m_vertArray.resize(pMesh->GetControlPointsCount());
			_mesh.m_cpuRendering.m_vertexArray.resize(_mesh.m_vertArray.size() * 4);

			///<	
			for (int i = 0; i < (int)_mesh.m_vertArray.size(); i++)
			{
				FbxVector4 p = pMesh->GetControlPointAt(i);

				for (int k = 0; k < 3; ++k)
				{
					_mesh.m_cpuRendering.m_vertexArray[i * 4 + k] = static_cast<GLfloat> (p[k]);
					_mesh.m_vertArray[i].m_x0[k] = _mesh.m_cpuRendering.m_vertexArray[i * 4 + k];
				}
			}
		}

		///< Create index array
		{
			_mesh.m_indexArray.resize(pMesh->GetPolygonVertexCount());
			memcpy((GLuint*)&_mesh.m_indexArray[0], (GLuint*)pMesh->GetPolygonVertices(), sizeof(GLuint)*_mesh.m_indexArray.size());
		}

		_mesh.m_boneCountsPerVertex.resize(_mesh.m_vertArray.size());
		std::fill(_mesh.m_boneCountsPerVertex.begin(), _mesh.m_boneCountsPerVertex.end(), 0);

		loadBoneWeights(_sk, _mesh, pMesh);
		_mesh.createGLBuffers();
	}
}

///<
void FBXImporter::loadBoneWeights(Skeleton& _sk, FMesh& _mesh, FbxMesh* mesh)
{
	printf("Loading Bone weights...\n");
	int numOfDeformers = mesh->GetDeformerCount();
	FbxSkin* skin = (FbxSkin*)mesh->GetDeformer(0, FbxDeformer::eSkin);

	int totalAmountOfBones = skin->GetClusterCount();

	_mesh.m_skinningMatrices.resize(totalAmountOfBones);

	_mesh.m_invBindMatrices.clear();
	_mesh.m_invBindMatrices.resize(totalAmountOfBones);
	for (int i = 0; i < (int)_mesh.m_skinningMatrices.size(); i++)
	{
		FbxCluster* cluster = skin->GetCluster(i);
		std::string strBoneName = cluster->GetLink()->GetName();
		Skeleton::Joint* pJoint = _sk.FindJoint(strBoneName);

		///< initial (rest pose) transformation
		{
			Matrix4x4 invBindM = _sk.getJointTransformationGlobal(pJoint).inverse();
			_mesh.m_invBindMatrices[pJoint->m_id] = invBindM;
		}

		///< Skinning weights: vertices are mapped to different bones.  
		///< Here we go through bones and its associated vertices.
		int numVerticesAffectedByBone_i = cluster->GetControlPointIndicesCount();
		{
			int* boneVertexIndices = cluster->GetControlPointIndices();
			double* boneVertexWeights = cluster->GetControlPointWeights();

			// For a given bone, find the vertices affected by it
			for (int j = 0; j < numVerticesAffectedByBone_i; ++j)
			{
				int vertIndex = boneVertexIndices[j];

				FMesh::FVertex& fVert = _mesh.m_vertArray[vertIndex];

				int& boneCount = _mesh.m_boneCountsPerVertex[vertIndex];
				if (boneCount < 4)
				{
					fVert._boneIds[boneCount] = (GLfloat)pJoint->m_id;
					fVert._boneWeights[boneCount] = static_cast<GLfloat> (boneVertexWeights[j]);
					boneCount++;
				}
			}
		}
	}

	printf("Loading Bone weights finished.\n");
}

///<
void FBXImporter::createSkeleton(Skeleton& _sk)
{
	createSkeleton(_sk, root);
}

///<
void FBXImporter::createSkeleton(Skeleton& _sk, FbxNode* pRootNode)
{
	FbxNode* pSkeletonNode = NULL;
	FindFirstAttribute(pRootNode, FbxNodeAttribute::eSkeleton, &pSkeletonNode);

	if (pSkeletonNode != NULL)
	{
		const char* strNodeName = pSkeletonNode->GetName();

		AnimationImport temp;
		IttParseHierarchy(pSkeletonNode, temp, strNodeName);

		{
			///<
			_sk.m_pRoot = new Skeleton::Joint();
			_sk.m_pRoot->m_strName = temp.m_root.m_name;

			//_sk.m_pRoot->m_q = Quaternion::ExpMap(temp.m_root.m_r);
			_sk.m_pRoot->m_q = temp.m_root.m_q.toUnit();
			_sk.m_pRoot->m_offset = temp.m_root.m_t;
			_sk.m_pRoot->m_id = 0;
			_sk.m_pRoot->m_pPrevious = NULL;

			_sk.m_pRoot->m_childs.resize(temp.m_root.m_childs.size());
			int id = 0;
			for (int j = 0; j < (int)temp.m_root.m_childs.size(); ++j)
			{
				_sk.m_pRoot->m_childs[j].m_pPrevious = _sk.m_pRoot;
				FillJoint(_sk.m_pRoot->m_childs[j], temp.m_root.m_childs[j], id);
			}
		}

		loadMotion(_sk);
	}
}

///<
void BuildHierarchy(FbxNode* _pFbxNode, AnimationImport::Bone* _pBone, const bool _bIsRoot)
{
	if (_pFbxNode)
	{
		if (_pFbxNode->GetChildCount()>0)
		{
			_pBone->m_childs.resize(_pFbxNode->GetChildCount());
		}

		//FbxDouble3 s = _pFbxNode->LclScaling.Get();
		//FbxDouble3 t = _pFbxNode->LclTranslation.Get();
		//FbxDouble3 r = _pFbxNode->LclRotation.Get();

		FbxAMatrix &lLocalTransform = _pFbxNode->EvaluateLocalTransform(0, PIVOT1);
		//FbxAMatrix &lglobalTransform = _pFbxNode->EvaluateGlobalTransform(0, PIVOT1);

		FbxAMatrix T;
		T.SetT(lLocalTransform.GetT());
		FbxAMatrix Roff;
		Roff.SetT(_pFbxNode->GetRotationOffset(PIVOT1));
		FbxAMatrix Rp;
		Rp.SetT(_pFbxNode->GetRotationPivot(PIVOT1));
		FbxAMatrix Rpre;
		Rpre.SetR(_pFbxNode->GetPreRotation(PIVOT1));
		FbxAMatrix R;
		R.SetR(lLocalTransform.GetR());
		FbxAMatrix Rpost;
		Rpost.SetR(_pFbxNode->GetPostRotation(PIVOT1));
		FbxAMatrix Soff;
		Soff.SetT(_pFbxNode->GetScalingOffset(PIVOT1));
		FbxAMatrix Sp;
		Sp.SetR(_pFbxNode->GetScalingPivot(PIVOT1));
		FbxAMatrix S;
		S.SetS(lLocalTransform.GetS());

		//FbxAMatrix localTransform = T * R;
		FbxAMatrix localTransform = T * Roff * Rp * Rpre * R *Rpost.Inverse() * Rp.Inverse() * Soff * Sp * S * Sp.Inverse();

		FbxVector4 fbxLocalPos = localTransform.GetT();
		FbxDouble  *pos = fbxLocalPos.Buffer();
		FbxVector4 fbxLocalRot = localTransform.GetR();
		FbxDouble  *rot = fbxLocalRot.Buffer();

		double *quat = localTransform.GetQ();

		bool bIsMaya = true;
		if (bIsMaya)
		{

			FbxDouble3 rPost = _pFbxNode->PostRotation.Get();
			FbxDouble3 rPre = _pFbxNode->PreRotation.Get();
			Vector3d pre = Vector3d(rPre[0], rPre[1], rPre[2]);
			Vector3d post = Vector3d(rPost[0], rPost[1], rPost[2]);

			///< Be careful, Maya (i.e its users) use different rotation parameterizations (pre, post, etc).

			//Quaternion qPre = Quaternionf::GenRotation(pre.x(), M::xAxisd)*Quaternionf::GenRotation(pre.y(), M::yAxisd)*Quaternionf::GenRotation(pre.z(), M::zAxisd);
			//Quaternion qPost = Quaternionf::GenRotation(post.x(), M::xAxisd)*Quaternionf::GenRotation(post.y(), M::yAxisd)*Quaternionf::GenRotation(post.z(), M::zAxisd);
			//_pBone->m_r = Vector3d(r[0], r[1], r[2]);
			//_pBone->m_r = _pBone->m_r*(PI / 180.0);
			//_pBone->m_r = Quaternionf::LogMap(qPost*qPre);

			_pBone->m_q = Quaternion(quat[3], quat[0], quat[1], quat[2]).toUnit();
			_pBone->m_t = Vector3d(pos[0], pos[1], pos[2]);
		}

		{
			_pBone->m_l = 1.0;
		}

		for (int j = 0; j<_pFbxNode->GetChildCount(); ++j)
		{
			_pBone->m_childs[j].m_name = std::string(_pFbxNode->GetChild(j)->GetName());
			BuildHierarchy(_pFbxNode->GetChild(j), &_pBone->m_childs[j], false);
		}
	}
}

///<
void CountBones(AnimationImport::Bone* _pBone, int& _iCount)
{
	if (_pBone)
	{
		_iCount += _pBone->m_childs.size();
		for (int j = 0; j<(int)_pBone->m_childs.size(); ++j)
			CountBones(&_pBone->m_childs[j], _iCount);
	}
}

///<
void PopulateBones(AnimationImport::Bone* _pBone, std::vector<AnimationImport::Bone*>& _bones, int& _index)
{
	if (_pBone)
	{
		for (int i = 0; i<(int)_pBone->m_childs.size(); ++i)
		{
			_pBone->m_childs[i].m_id = _index;
			_bones[_pBone->m_childs[i].m_id] = &_pBone->m_childs[i];
			_index++;
			PopulateBones(&_pBone->m_childs[i], _bones, _index);
		}
	}
}

///<
void BuildArray(AnimationImport& _animData)
{
	int boneCount = 1;
	CountBones(&_animData.m_root, boneCount);
	if (boneCount>0)
	{
		_animData.m_boneArray.resize(boneCount);

		_animData.m_root.m_id = 0;

		_animData.m_boneArray[_animData.m_root.m_id] = &_animData.m_root;
		int index = 1;
		PopulateBones(_animData.m_boneArray[0], _animData.m_boneArray, index);
	}
}

///<
void FBXImporter::IttParseHierarchy(FbxNode* _pFbxNode, AnimationImport& _animImport, const char* _csRootName)
{
	if (_pFbxNode->GetNodeAttribute())
	{
		if (_pFbxNode->GetNodeAttribute()->GetAttributeType() == FbxNodeAttribute::eSkeleton)
		{
			///< Build Hierarchy !

			const char* strName = _pFbxNode->GetName();
			_animImport.m_root.m_name = std::string(strName);

			BuildHierarchy(_pFbxNode, &_animImport.m_root, true);
			BuildArray(_animImport);
		}
	}
}


// It gets the position information from the current pNode for all the Animation time
// It gets and saves two kind of informations:
// The Default Pose: in which is saved into the &joint.orientation and &joint.position
// The Animation Information: in which are the rotations and the offsets translations related to the default pose that allow the animation
// of the FBXCharacter over time (these information are saved directly into the FBXCharacter(translation and orientation vectors)
void FBXImporter::getJointPositions(FBXCharacter &agent, FBXJoint &joint, FbxNode* pNode)
{
	FbxAMatrix   lDummyGlobalPosition;
	Quaternion qChild = Quaternion();
	V3D        tChild = V3D(0, 0, 0);
	V3D		   rChild = V3D(0, 0, 0);
	int parentID = joint.jointParentID;
	int jointID = joint.jointID;
	//Getting Default Joint Orientation
	FbxVector4 rotFxbPre = pNode->PreRotation.Get();
	FbxVector4 rotFxbPos = pNode->PostRotation.Get();
	Quaternion qRefPre, qRefPos, qRef;
	qRefPre = agent.getQuaternionFromEuler(rotFxbPre[0], rotFxbPre[1], rotFxbPre[2]);
	qRefPos = agent.getQuaternionFromEuler(rotFxbPos[0], rotFxbPos[1], rotFxbPos[2]).getInverse();
	qRef = qRefPre*qRefPos;
	joint.orientation = qRef;

	///<
	this->currentTime = this->startTime;
	for (int poseNum = 0; poseNum < this->numOfPoses; poseNum++)
	{
		//Getting Local Coordinates
		getNodeLocalTransform(pNode, currentTime, tChild, qChild, rChild);

		//The animation data will be based on the default pose
		if (!poseNum)
			joint.position = tChild;
		if (!jointID)
			joint.position = V3D(0, 0, 0);

		V3D    trans = (tChild - joint.position);
		Quaternion quat = qChild;

		agent.translation.at(poseNum).at(jointID) = trans;
		agent.orientation.at(poseNum).at(jointID) = quat;

		this->currentTime += this->frameTime;
	}
}

// Once there is the this->joints information, it is possbile to create a BodyPart with the right orientation
// and positions for all the joints because it is necessary to make all of them displaced in way that their local position informations are related to
// the center of the box which represents the BodyPart
void FBXImporter::fillBodyPart(BodyPart &bodyPart, FBXJoint &joint)
{
	int partID = bodyPart.partID;
	int jointID = joint.jointID;
	int nChildrenJ = joint.childrenIDs.size();
	int childID = 0;
	FBXJoint child;
	P3D pointMax;
	P3D pointMin;
	P3D pointCenter;
	double bigX = 0;
	double bigY = 0;
	double bigZ = 0;
	double smallX = 0;
	double smallY = 0;
	double smallZ = 0;

	//Getting the edges

	for (int k = 0; k < nChildrenJ; k++)
	{
		childID = joint.childrenIDs.at(k);
		child = this->joints.at(childID);
		if (child.position.at(0) >= bigX) { bigX = child.position.at(0); }
		if (child.position.at(1) >= bigY) { bigY = child.position.at(1); }
		if (child.position.at(2) >= bigZ) { bigZ = child.position.at(2); }
		if (child.position.at(0) <= smallX) { smallX = child.position.at(0); }
		if (child.position.at(1) <= smallY) { smallY = child.position.at(1); }
		if (child.position.at(2) <= smallZ) { smallZ = child.position.at(2); }
	}
	pointMax = P3D(bigX, bigY, bigZ);
	pointMin = P3D(smallX, smallY, smallZ);
	pointCenter = (pointMax - (-pointMin)) / 2;
	//Getting the dimensional properties
	bodyPart.length = pointMax.at(0) - pointMin.at(0);
	bodyPart.width = pointMax.at(1) - pointMin.at(1);
	bodyPart.height = pointMax.at(2) - pointMin.at(2);
	//Organizing coordinate frame based on the pointCenter
	bodyPart.parentJoint = joint;
	bodyPart.parentJoint.position = -V3D(pointCenter);
	for (int k = 0; k < nChildrenJ; k++)
	{
		childID = joint.childrenIDs.at(k);
		child = this->joints.at(childID);
		bodyPart.childrenJoints.at(k) = child;
		bodyPart.childrenJoints.at(k).position = child.position - V3D(pointCenter);
	}
	int nJoints = bodyPart.childrenJoints.size();
	V3D vParent = bodyPart.parentJoint.position;
	if (nJoints == 1) //It is a skeleton with only a Parent and a Child
	{
		V3D vChild = V3D();
		V3D vertex[8];
		vChild = bodyPart.childrenJoints.at(0).position;
		V3D  direction = vChild - vParent;

		V3D  n, t, axis = direction.unit();
		axis.getOrthogonalVectors(n, t);
		double size = direction.length()*0.1;
		n = n.unit() * size;
		for (int i = 0; i < 4; i++)
		{
			vertex[i] = n.rotate(2 * i*PI / 4, axis) + vParent;
			vertex[i + 4] = n.rotate(2 * i*PI / 4, axis) + vChild;
		}
		double bigX = vertex[0].at(0);
		double bigY = vertex[0].at(1);
		double bigZ = vertex[0].at(2);
		double smallX = vertex[0].at(0);
		double smallY = vertex[0].at(1);
		double smallZ = vertex[0].at(2);
		for (int k = 1; k < 8; k++)
		{
			if (vertex[k].at(0) >= bigX) { bigX = vertex[k].at(0); }
			if (vertex[k].at(1) >= bigY) { bigY = vertex[k].at(1); }
			if (vertex[k].at(2) >= bigZ) { bigZ = vertex[k].at(2); }
			if (vertex[k].at(0) <= smallX) { smallX = vertex[k].at(0); }
			if (vertex[k].at(1) <= smallY) { smallY = vertex[k].at(1); }
			if (vertex[k].at(2) <= smallZ) { smallZ = vertex[k].at(2); }
		}
		pointMax = P3D(bigX, bigY, bigZ);
		pointMin = P3D(smallX, smallY, smallZ);
		bodyPart.length = pointMax.at(0) - pointMin.at(0);
		bodyPart.width = pointMax.at(1) - pointMin.at(1);
		bodyPart.height = pointMax.at(2) - pointMin.at(2);
	}
}


//Once there are all the BodyParts are filled with all joints informations, it is needed to create a properly hierarchy for them, assigning the correct IDs value to make them virtualy connected
void FBXImporter::creatingHierarchy(FBXCharacter &agent)
{
	for (int i = 0; i < this->numOfBodyParts; i++)
	{
		BodyPart &bodyPart = agent.getBodyPart(i);
		int nChildrenJ = bodyPart.childrenJoints.size();
		int childK = 0;
		for (int j = 0; j < nChildrenJ; j++)
		{
			bodyPart.childrenIDs.at(j) = -1;
			for (int k = 0; k < this->numOfBodyParts; k++)
			{
				BodyPart &bodyPartChild = agent.getBodyPart(k);
				if (bodyPart.childrenJoints.at(j).jointID == bodyPartChild.parentJoint.jointID)
				{
					bodyPart.childrenIDs.at(j) = bodyPartChild.partID;
					bodyPartChild.parentID = bodyPart.partID;
					childK++;
				}
			}
		}
		bodyPart.numOfChildren = childK;
		bodyPart.childrenIDs.resize(nChildrenJ);
	}
	agent.numOfBodyParts = this->numOfBodyParts;
	agent.links.resize(this->numOfBodyParts - 1);
}

// It prints all the hierarchy attributes for checking if they match with the expected hierarchy 
void FBXImporter::checking(FBXCharacter &agent)
{
	for (int i = 0; i < this->numOfBodyParts; i++)
	{
		BodyPart &bodyPart = agent.getBodyPart(i);
		int nChildrenJ = bodyPart.childrenJoints.size();
		//printf("Body ID    : %d   NumofChildren: %d\n", bodyPart.partID, bodyPart.numOfChildren);
		//printf("   Body Parent: %d\n", bodyPart.parentID);
		//printf("      JointParentID: %d\n", bodyPart.parentJoint.jointID);
		for (int j = 0; j < nChildrenJ; j++)	printf("       JointChildID: %3d   ----> ChildID: %3d\n", bodyPart.childrenJoints.at(j).jointID, bodyPart.childrenIDs.at(j));
	}
}

// It gets the node Type of a Node, it is being used to determine whether or not the current Node is a Skeleton
std::string FBXImporter::nodeTypeName(FbxNodeAttribute::EType attrType)
{
	switch (attrType)
	{
	case FbxNodeAttribute::eUnknown:
		return "eUnknown";
		break;
	case FbxNodeAttribute::eNull:
		return "eNull";
		break;
	case FbxNodeAttribute::eMarker:
		return "eMarker";
		break;
	case FbxNodeAttribute::eSkeleton:
		return "eSkeleton";
		break;
	case FbxNodeAttribute::eMesh:
		return "eMesh";
		break;
	case FbxNodeAttribute::eNurbs:
		return "eNurbs";
		break;
	case FbxNodeAttribute::ePatch:
		return "ePatch";
		break;
	case FbxNodeAttribute::eCamera:
		return "eCamera";
		break;
	case FbxNodeAttribute::eCameraStereo:
		return "eCameraStereo";
		break;
	case FbxNodeAttribute::eCameraSwitcher:
		return "eCameraSwitcher";
		break;
	case FbxNodeAttribute::eLight:
		return "eLight";
		break;
	case FbxNodeAttribute::eOpticalReference:
		return "eOpticalReference";
		break;
	case FbxNodeAttribute::eOpticalMarker:
		return "eOpticalMarker";
		break;
	case FbxNodeAttribute::eNurbsCurve:
		return "eNurbsCurve";
		break;
	case FbxNodeAttribute::eTrimNurbsSurface:
		return "eTrimNurbsSurface";
		break;
	case FbxNodeAttribute::eBoundary:
		return "eBoundary";
		break;
	case FbxNodeAttribute::eNurbsSurface:
		return "eNurbsSurface";
		break;
	case FbxNodeAttribute::eShape:
		return "eShape";
		break;
	case FbxNodeAttribute::eLODGroup:
		return "eLODGroup";
		break;
	case FbxNodeAttribute::eSubDiv:
		return "eSubDiv";
		break;
	case FbxNodeAttribute::eCachedEffect:
		return "eCachedEffect";
		break;
	case FbxNodeAttribute::eLine:
		return "eLine";
		break;
	default:
		return "NULL";
		break;
	}
}
// It gets the Skeleton type (This property is not being taken into account)
std::string FBXImporter::skeletonTypeName(FbxSkeleton::EType skeletonType)
{
	switch (skeletonType)
	{
	case FbxSkeleton::EType::eEffector:
		return "eEffector";
		break;
	case FbxSkeleton::EType::eLimb:
		return "eLimb";
		break;
	case FbxSkeleton::EType::eLimbNode:
		return "eLimbNode";
		break;
	case FbxSkeleton::EType::eRoot:
		return "eRoot";
		break;
	default:
		return "NULL";
		break;
	}
}

// It gets the local tranformation data from the current pNode for the currentTime
// The information is saved to the tChild, qChild and rChild variables
void FBXImporter::getNodeLocalTransform(FbxNode* pNode, FbxTime currentTime, V3D &tChild, Quaternion &qChild, V3D &rChild)
{
	FbxAMatrix &lLocalTransform = pNode->EvaluateLocalTransform(currentTime, PIVOT1);
	FbxAMatrix T;
	T.SetT(lLocalTransform.GetT());
	FbxAMatrix Roff;
	Roff.SetT(pNode->GetRotationOffset(PIVOT1));
	FbxAMatrix Rp;
	Rp.SetT(pNode->GetRotationPivot(PIVOT1));
	FbxAMatrix Rpre;
	Rpre.SetR(pNode->GetPreRotation(PIVOT1));
	FbxAMatrix R;
	R.SetR(lLocalTransform.GetR());
	FbxAMatrix Rpost;
	Rpost.SetR(pNode->GetPostRotation(PIVOT1));
	FbxAMatrix Soff;
	Soff.SetT(pNode->GetScalingOffset(PIVOT1));
	FbxAMatrix Sp;
	Sp.SetR(pNode->GetScalingPivot(PIVOT1));
	FbxAMatrix S;
	S.SetS(lLocalTransform.GetS());

	FbxVector4 lT, lR, lS;
	FbxAMatrix lGeometry;

	lT = pNode->GetGeometricTranslation(PIVOT1);
	lR = pNode->GetGeometricRotation(PIVOT1);
	lS = pNode->GetGeometricScaling(PIVOT1);

	lGeometry.SetT(lT);
	lGeometry.SetR(lR);
	lGeometry.SetS(lS);


	//this equation comes from the KFbxpNode doxygen
	//Maya and FBX
	// http://help.autodesk.com/view/FBX/2015/ENU/?guid=__files_GUID_10CDD63C_79C1_4F2D_BB28_AD2BE65A02ED_htm
	FbxAMatrix localTransform = T * Roff * Rp * Rpre * R * Rpost.Inverse() * Rp.Inverse()* Soff * Sp * S * Sp.Inverse();


	FbxVector4 fbxLocalPos = localTransform.GetT();
	FbxDouble  *pos = fbxLocalPos.Buffer();
	FbxVector4 fbxLocalRot = localTransform.GetR();
	FbxDouble  *rot = fbxLocalRot.Buffer();
	double   *quat = localTransform.GetQ();

	tChild = V3D(pos[0], pos[1], pos[2]);
	qChild = Quaternion(quat[3], quat[0], quat[1], quat[2]);
	rChild = V3D(rot[0], rot[1], rot[2]);
}






