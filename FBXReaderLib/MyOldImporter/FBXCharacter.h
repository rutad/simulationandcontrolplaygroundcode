#pragma once

#include <fbxsdk.h>

#include "FBXJoint.h"
#include <GUILib/GLUtils.h>
#include <GUILib/GLApplication.h>
#include <FBXReaderLib\Skeleton.hpp>

class AnimationUtils;
class FBXCharacter;
class FBXImporter;
struct BodyPart;
struct Options;

class Skeleton;


/**
* Body Part
- This Struct works as an abstraction of a rigid body in the space
- It is composed by a set of joints and also it can be added physical properties such as mass, density (which is not currently being considered)
- It is represented graphically by a box that fits all the joints, and it has the width, height and length properties
- It follows the same idea of the Joints hierarchy, containing a parent and a set of children
- The position attributes may be obtained from the Joints information
- It is important to notice that the BodyPart contains information about both of the Joint hierarchy and of the BodyPart Hierarchy
*/
struct BodyPart
{
public:

	//Joint attributes
	FBXJoint				parentJoint;
	std::vector<FBXJoint>  childrenJoints;

	//Dimension attributes
	double width;
	double height;
	double length;

	//Physics attributes
	double    density = 0;
	double    mass = 0;

	//Hierarchy attributes
	std::vector<int>  childrenIDs;
	int		       	  partID = 0;
	int			      parentID = 0;

	//Other attributes

	std::string	 partName;
	std::string	 partType;
	int			 numOfChildren = 0;

	/**
	Contructors and destructor
	*/
	BodyPart(int numOfChildren, int partID);
	BodyPart() {};
	~BodyPart();
};

/**
* FBXCharacter
- An FBXCharacter is a set of BodyParts and their animation information over time
- Its information can be obtained both by the FBXImporter class and also by the importing an SAH file
- This Class is used for representing all the information of an FBXCharacter such as all the BodyParts and their animation information through time
- Like the BodyPart and the Joint struct, the FBXCharacter contains a hierarchy, in which there is a root as the main BodyPart and all the links which are connected
according to their hierarchy attributes
*/
class FBXCharacter
{
private:
	// It is used for scalling the FBXCharacter for fitting in the screen since the original values are too high
	double scale = 0.005;



public:
	std::string agentName;
	//Hierarchy attributes
	int                   agentID = 0;
	BodyPart              root;
	std::vector<BodyPart> links;
	//Animation control attributes
	Timer	  time;
	int		  numOfBodyParts = 0;
	int		  numOfPoses = 0;
	int       numOfJoints = 0;
	int		  currentPose = 0;

	Quaternion  qGlobal = Quaternion();
	V3D         tGlobal = V3D(0, 0, 0);
	//Animation information
	std::vector<std::vector<V3D> >				translation;
	std::vector<std::vector<Quaternion> >		orientation;

	struct Pose
	{
		V3D							m_x;
		std::vector<Quaternion>		m_q;
	};

	Pose GetPose(int _poseId)
	{
		assert(_poseId < (int)orientation.size());

		Pose rPose;
		rPose.m_x = translation[_poseId].at(0);
		rPose.m_q = orientation[_poseId];
		return rPose;
	}

	//Drawing information
	std::vector<std::vector<P3D> >				drawingBoxes;
	std::vector<std::vector<P3D> >				drawingLinks;

	std::vector<std::vector<V3D> >				globalPositions;
	std::vector<std::vector<Quaternion> >       globalOrientations;
	//Physical attributes
	double totalMass = 0;
	/**
	Contructors and destructor
	*/
	FBXCharacter();
	FBXCharacter(FBXImporter &importer);
	~FBXCharacter();

	
	void FillChildJoint(const FBXJoint& _FBXJoint, Skeleton::Joint& _joint, int& _id) const;
	void CreateSkeletonHierarchy(Skeleton& _sk);

	void CreateHierarchy();
	/**
	Methods
	*/

	// It returns a reference to the right BodyPart having its unique ID as input
	inline BodyPart &			getBodyPart(int ID);
	inline const BodyPart &		getBodyPart(int ID) const;
	// It draws the FBXCharacter using the information from the 'drawingBoxes' and 'drawingLinks' vectors
	void drawCharacter();

	// It draws a BodyPart from the current 'drawingBoxes' information for the desired partID
	void drawBodyPart(int partID = 0);

	// It draws a link which may be just a line between two joints
	void drawLink(P3D &start, P3D &end);

	// It returns a Quaternion having the euler angles (in degrees), being the rotation order: q = qZ*qY*qX
	static Quaternion getQuaternionFromEuler(double teta_x, double teta_y, double teta_z);

	// It returns the local vertexes positions of the box that fits the &bodyPart for the desired poseNum
	void getBoxRelative2Center(P3D(&vertex)[8], BodyPart &bodyPart, int poseNum);

	// It calculates the global position for the entire FBXCharacter and updates the 'drawingBoxes' and 'drawingLinks' data
	// It is a recursive function which starts from the partID (a BodyPart) and goes over all of the children connected to this BodyPart
	// The &options parameter selects the desired setting for the animation
	// tParent and qParent are used for passing the Parent global positions and orientation for the next children (it doesn't need to be initialized)
	void computeAgentBoxesRecursively(Options &options, int poseNum = 0, int partID = 0, V3D tParent = V3D(), Quaternion qParent = Quaternion());

	// It computes the global positions for the 'drawingBoxes' data from a given &bodyPart and the current global position and orientation of its jointParent attribute
	void computeBox(BodyPart &bodyPart, V3D &tParent, Quaternion &qParent, int poseNum);

	// It computes the entire FBXCharacter
	// and also manages and updates the 'currentPose' attribute
	void UpdateCharacter(Options &options);

	//P3D getGlobalPosRecursively(int partID, int currPose, V3D &tParent, Quaternion &qParent);
	std::vector<P3D>	getTranslationTrajectoryOf(int partID);
	P3D					getCurrentGlobalPositionOf(int partID);
	Quaternion			getCurrentGlobalOrientationOf(int partID);
};