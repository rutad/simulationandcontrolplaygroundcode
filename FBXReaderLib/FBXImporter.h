#pragma once

#include <fbxsdk.h>

#include <MathLib/Trajectory.h>
#include <GUILib/GLUtils.h>
#include <../GUILib/GLApplication.h>

#include <Windows.h>

#include <FBXReaderLib/SkinnedMesh.h>
#include <FBXReaderLib/Import/ArmatureImport.hpp>

#include <FBXReaderLib/Skeleton.hpp>

class AnimationUtils;
class Joint;



/**
* FBXImporter
- Each FBX file is composed by a hierarchy of Nodes which can be: a Skeleton part, Mesh, Camera...
- This class is used for importing all the Skeletons and their animation information from an FBX file
- It has functions for filling up an FBXCharacter from the FBX data and also it handles all the hierarchy properly
*/
class FBXImporter
{
	FbxManager*					m_pFbxManager	= NULL;
	FbxScene*					m_pScene		= NULL;
	FbxNode*					m_pRootNode		= NULL;

	int							m_numOfPoses	= 0;

	std::string					m_sceneName;

	FbxTime						m_fbxFrameTime;
	FbxTime						m_fbxStartTime;
	FbxTime						m_fbxStopTime;
	FbxTime						m_fbxCurrentTime;

	void getKeyTimesFromRootTranslationKeyFrames(Skeleton& _sk, std::vector<FbxTime>& _kfTimes);

	FbxAnimLayer*		getFirstLayer();

	friend class FBXExporter;

	FbxMesh*	getFbxMeshNode	();
	int			numIndices		();
	int*		getIndices		();

	int			numVertices		();
	FbxVector4* getVertices		();

	FbxSkin*	getSkinDeformer();

	///<
	void			createMesh			(Skeleton& _sk, SkinnedMesh** _ppMesh, FbxNode* pNode);

	void			createSkeleton		(Skeleton& _sk, FbxNode* pNode);

public:

	/**
	Contructors and destructor
	*/
	FBXImporter(std::string fileName, double _mocapSamplingRate = -1);
	~FBXImporter();

	/**
		Methods
	*/
	static void		loadSkeleton				(const char* _csFileName, Skeleton& _sk);
	static void		loadMeshedSkeleton			(const char* _csFileName, Skeleton& _sk, SkinnedMesh** _ppMesh);

	///<
	void			fillJoint					(Skeleton::Joint& _jnt, const AnimationImport::Bone& _importedBone, int& _id);

	void			createSkeleton				(Skeleton& _sk);
	void			createSkeletonAndMesh		(Skeleton& _sk, SkinnedMesh** _ppMesh);
	
	void			createMesh					(Skeleton& _sk, SkinnedMesh** _ppMesh);
	void			createMesh					(Skeleton& _sk, SkinnedMesh** _ppMesh, V3D _translateVertices, double _scaleVertices);

	void			loadMotion					(Skeleton& _sk);	

	void			IttParseHierarchy			(FbxNode* _pFbxNode, AnimationImport& _animImport, const char* _csRootName);

	void			initImporterAtts			(FbxImporter* _pFbxImporter, double _mocapSamplingRate);

	// It gets the node Type of a Node, it is being used to determine whether or not the current Node is a Skeleton
	std::string		nodeTypeName				(FbxNodeAttribute::EType attrType);

	// It gets the Skeleton type (This property is not being taken into account)
	std::string		skeletonTypeName			(FbxSkeleton::EType skeletonType);


	// It gets the local tranformation data from the current pNode for the currentTime
	// The information is saved to the tChild, qChild and rChild variables
	void			getNodeLocalTransform		(FbxNode* _pNode, FbxTime currentTime, V3D &tChild, Quaternion &qChild, V3D &rChild);
	
private:

	static FbxNode* findNodeByName				(FbxNode* startNode, std::string name);
	///<
	void			loadBoneWeights				(Skeleton& _sk, SkinnedMesh& _mesh, FbxMesh* mesh);

	void			getAndSetMayaBindPose(Skeleton& _sk, SkinnedMesh& _mesh, FbxMesh* _pMesh);
	void			setMissingJointsInBindPose	(Skeleton& _sk, SkinnedMesh& _mesh, FbxMesh* _pMesh);

};
