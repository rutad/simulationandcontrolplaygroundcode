

#pragma once

#include <FBXReaderLib\Skeleton.hpp>
#include <FBXReaderLib\SubspaceUtil.hpp>

///<
class SkeletonSubspace
{
	//friend class DrawSkeleton;
public:
	///< Functions to reduced and fulfill spaces.
	SubspaceUtil m_xSpace, m_qSpace;

	//< These are the subspace coordinates of current pose.
	Eigen::MatrixXd m_currentSubX; //< Typicall size 2 for planar motion...can remove thise too...
	Eigen::MatrixXd m_currentSubQ; //< Typically, dim 8
	
	void ApplyToSekeleton(const Eigen::MatrixXd& _Q, Skeleton& _sk) const;
public:

	SkeletonSubspace() {}
	SkeletonSubspace(const Skeleton& _sk);
	void Create(const Skeleton& _sk);

	///< Turns sk into xSpace (dim 2) and qSpace (dim 8).
	void ProjectPose	(const Skeleton& _sk);
	void UnProjectPose	(Skeleton& _sk) const;

	//< serialize into opt-friendly vec.
	void SerializeParams(dVector& _p);
	void UnSerializeParams(const dVector& _p);

	///< draw all eigen poses, using _sk for temp sk rendering.
	void drawSubspace	(Skeleton& _sk) const;
};



