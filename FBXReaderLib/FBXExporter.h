#pragma once

#include <fbxsdk.h>

#include <MathLib/Trajectory.h>
#include <GUILib/GLUtils.h>
#include <../GUILib/GLApplication.h>

#include <Windows.h>

#include <FBXReaderLib/Import/ArmatureImport.hpp>
#include <FBXReaderLib\SkinnedMesh.h>

#include <FBXReaderLib\Skeleton.hpp>

class AnimationUtils;

class ReducedRobotState;
class Robot;
class Joint;
class AnimatedRobot;


/**
* FBXExporter
- Each FBX file is composed by a hierarchy of Nodes which can be: a Skeleton part, Mesh, Camera...
- This class is used for exporting skeleton and mesh.
*/
class FBXExporter
{
	FbxManager*					m_pFbxManager	= NULL;
	FbxScene*					m_pScene		= NULL;
	FbxNode*					m_pRootNode		= NULL;

	int							m_numOfPoses	= 0;		
	std::string					m_sceneName;

	FbxNode* FBXExporter::findNode(const char* _csBoneName);
public:


	FBXExporter();
	~FBXExporter();

	void rbsAnimationToFbx	(Robot* _pRobot, AnimatedRobot* _pAnimationToExport, Skeleton* _pSrcSK, SkinnedMesh* _pMesh, FBXImporter* _pImportedMesh, double animationTime, std::string fileName, int numKeyFrames = 100);
	void skeletonToFbx		(Skeleton& _sk, Skeleton* _pSrcSK, SkinnedMesh* _pMesh, FBXImporter* _pImportedMesh, double animationTime, std::string fileName);

	void release			();

private:

	static double m_spaceScalingFactor; // how much to scale spatial values (distances), otherwise not visible in Maya resp. everything too close together.

	static void			rbsToSkeleton		(Robot* robot, ReducedRobotState* rbs, Skeleton& sk);
	
	static void			buildSkeleton		(Skeleton::Joint* joint, Joint* rbsJoint);
	static FbxNode*		jointToNode			(FbxScene* pScene, Skeleton::Joint& joint);
	static void			addChildNodes		(FbxScene* pScene, Skeleton::Joint& currJoint, FbxNode* pParentNode);
	static void			animateSkeleton		(FbxScene* pScene, FbxNode* pSkeletonRoot, Skeleton& _sk, double animationTime);
};
