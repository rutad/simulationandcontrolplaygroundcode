


#pragma once

#include <MathLib/P3D.h>
#include <MathLib/V3D.h>
#include <MathLib/Quaternion.h>
#include <MathLib/MathLib.h>
#include <ControlLib\AnimationTime.h>

class SkeletonImport;


///<
class Skeleton
{
	friend class FBXImporter;
	friend class RBSMesh;

	///< can use the maya bind pose, or use the first frame of animation convention (which is better).
	/// Maya bind pose has some joint transforms missing, when they are not used in the skinning...
	bool			m_bUseMayaBindPose = false;

	/// This is used to scale the skeleton down to scale used in the simulations:
	double			m_scaleDanger		= 1.0;

	// This is for updating the animation:
	int				m_ordering			= 1;
	int				m_currentFrameIndex	= 0;
	int				m_skipFactor		= 0;

	///< Sets the skeleton pose to the one at frame _i
	void			setPoseToFrameIndex			(int _i);

	///< 
	bool			updateSkipFactor			(double _speedFactor);
	
public:

	
	
	V3D				m_rootVisDebugOffset;
	Quaternion		m_rootVisDebugRot;

	double		getScaleDanger						() { return m_scaleDanger; }
	void		setScaleDanger						(const double _s) { m_scaleDanger = _s; }

	///< translates the root to have the lowers end effector snapping to the ground.
	void setToGround						();
	///< Scales the whole skeleton such that the height of the root is at _s.
	void setScaleForRootHeightInWorldToBe	(double _s);

	struct Joint
	{
		int m_id;
		std::string			m_strName;

		std::string*		m_pChildRigidBodyName=NULL;

		Vector3d			m_offset;
		Quaternion			m_q;

		Quaternion			m_q0;
		Quaternion			m_qPre;

		Joint* m_pPrevious=NULL;
		std::vector<Joint> m_childs;

		Joint& operator=(const Joint& _jt);

		~Joint();
		Joint() : m_pPrevious(NULL), m_id(-1), m_pChildRigidBodyName(NULL) {}
		Joint(int id, std::string name) : m_pPrevious(NULL), m_id(id), m_strName(name), m_pChildRigidBodyName(NULL) {}
	};


	typedef std::vector<Quaternion>		QVec;

	///< The acutal skeleton data:
	Joint*						m_pRoot=NULL;
	
	std::vector< V3D >			m_X;
	std::vector< QVec >			m_Q;


//	void				setPositionTranslation					(P3D _dx)				{ m_dx = _dx; }
	void				setPosition								(P3D _rootPosition)		{	m_pRoot->m_offset = _rootPosition;	}
	void				copy									(const Skeleton& _sk);
	void				copyWithoutRotations					(Skeleton& _sk);

	///< Phase time is between 0 and 1.
	void				setPhaseTime							(double _t);
	double				getPhaseTime							();
	void				setDiscreteTimeIndex					(int _i);
	///< The animation has a number of frames:
	int					getCurrentTimeIndex						() { return m_currentFrameIndex; }
		
	///< update the time of animation:
	void				updateAnimationTimeCycling				(double _speedFactor=1.0);
private:
	void				updateAnimationIndexFwdAndReverse		();

	AnimationTimer m_continuousTimer;
public:
	void				updateAnimationTilFinished				(double _speedFactor = 1.0);
	void				updateAnimationTilFinished_Continuous	(const double _simTimeStep);


	///< Total number of joints
	int					size						() const;
	int					numJoints					() const;
	int					numKeyFrames				();

	Joint*				getJoint					(int _index)  { return FindJoint(_index); }
	Joint*				FindRigidBodyParentJoint	(const std::string& _name);
	Joint*				FindJoint					(int _index) ;
	Joint*				FindJoint					(const std::string& _name);
	void				FindJoint					(int _index, Joint* _pJoint, Joint** _ppFound);
	void				FindJoint					(const std::string& _name, Joint* _pJoint, Joint** _ppFound, bool _bRB = false);

	///< On a limb, this fct takes the child joint name, and returns the center-point between parent and child joint.
	P3D					GetLimbPosition				(const char* _csName);
	P3D					GetLimbPosition				(Joint* _pJoint);

	P3D					ComputeCOM					();

	V3D					getJointPos					(Joint* _pJoint);
	P3D					X							(const char* _csName);
	P3D					X							(int _jointIndex);
	P3D					X							(Joint* _pJoint);


	///////////////////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////////////////////
	///< This is only used for the RBS modeler

	typedef std::vector<Matrix4x4, Eigen::aligned_allocator<Matrix4x4> >		TransformsContainer;

	TransformsContainer m_bindMatrices;
	P3D					X_BindPose(Joint* _pJoint);

	//////////////////////////////////////////////////////////////////////////////////////////////////////


	Quaternion			Q							(Joint* _pJoint);
	Quaternion			Q							(int _jointIndex);
	Quaternion			Q							(const char* _csName);


	~Skeleton();
	Skeleton() {}
	
	Matrix4x4			getJointTransformationGlobal		(Joint* _pJoint) ;
	Matrix4x4			getJointTransformationGlobal		(int _jointIndex);

	typedef std::vector<Matrix4x4, Eigen::aligned_allocator<Matrix4x4> > TransformationContainer;

	void						updateGlobalBoneTransforms			(TransformationContainer& _globalTransforms);
	TransformationContainer*	getCurrentGlobalTransforms			();

	std::vector<P3D>	computeTrajectoryForJoint			(const Joint* _pJoint);

	void				drawJointTrajectory					(const Joint* _pJoint);
	void				drawLinksAndJointSpheres			(double _linksScale=1.0, double _jointsScale=1.0);
	void				drawJointsSpheres					();
	void				drawLinks							();
	void				drawLink							(P3D& _start, P3D& _end, float _thickness = 3.0);

	void				setPoseRaw							(const dVector& _p);
	void				getPoseRaw							(dVector& _p);

private:

	friend class SkeletonTrajectory;

	static void IncrementSize(Joint* _pJoint, int& _size);

	TransformationContainer* m_pCurrGlobalBoneTrans=NULL;
};


class SkeletonTrajectory
{

	Skeleton* m_pSkeletonMotion = NULL;

	///< Used as buffer for returning the interpolated sk.
	Skeleton* m_pSmoothSkeleton = NULL;
public:

	
	explicit SkeletonTrajectory(Skeleton& _skMotion);
	~SkeletonTrajectory();

	Skeleton& getInterpolatedSkeleton(const double _t);


};