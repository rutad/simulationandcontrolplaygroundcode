
#include <FBXReaderLib/SubspaceSkeleton.hpp>


///<
SkeletonSubspace::SkeletonSubspace(const Skeleton& _sk){Create(_sk);}

///<
void SkeletonSubspace::Create(const Skeleton& _sk)
{
	SubspaceUtil::ComputeSkeletonSubspace(_sk, m_xSpace, m_qSpace);
}

///<
void SkeletonSubspace::ApplyToSekeleton(const Eigen::MatrixXd& _Q, Skeleton& _sk) const
{
	int numJoints = _sk.size();
	for (int j = 0; j < numJoints; ++j)
	{
		Quaternion& qP = _sk.FindJoint(j)->m_q;
		for (int i = 0; i < 4; ++i)
			qP[i] = _Q(j * 4 + i, 0);

		qP.toUnit();
	}
}

///<
void SkeletonSubspace::UnProjectPose(Skeleton& _sk) const
{
	///< X
	{
		Eigen::MatrixXd xFull = m_xSpace.UnProject(m_currentSubX);
		for (int k = 0; k < 3; ++k)
			_sk.m_pRoot->m_offset(k) = xFull(k);
	}

	///< Q
	{
		Eigen::MatrixXd QFull = m_qSpace.UnProject(m_currentSubQ);
		ApplyToSekeleton(QFull, _sk);		
	}
}

void SkeletonSubspace::ProjectPose(const Skeleton& _sk)
{
	///< X
	m_currentSubX = m_xSpace.Project(_sk.m_pRoot->m_offset);

	///< Q
	{
		int numJoints = _sk.size();
		Eigen::MatrixXd qPose(4 * numJoints, 1);
		for (int j = 0; j < numJoints; ++j)
		{
			Quaternion qP = const_cast<Skeleton&>(_sk).FindJoint(j)->m_q;
			for (int i = 0; i < 4; ++i)
				qPose(j * 4 + i, 0) = qP[i];
		}
		m_currentSubQ = m_qSpace.Project(qPose);
	}	
}



///<
void SkeletonSubspace::SerializeParams(dVector& _p)
{
	int dim = m_qSpace.NumEigenVectors();
	_p.resize(dim);
	for (int i = 0; i < _p.size(); ++i)
		_p[i] = m_currentSubQ(i, 0);
}

///<
void SkeletonSubspace::UnSerializeParams(const dVector& _p)
{
	assert(_p.size() == m_qSpace.NumEigenVectors());

	for (int i = 0; i < _p.size(); ++i)
		m_currentSubQ(i, 0) = _p[i];
}


///< Buffer _tempSk
void SkeletonSubspace::drawSubspace(Skeleton& _tempSk) const
{
	for (int i = 0; i < m_qSpace.NumEigenVectors(); ++i)
	{
		Eigen::MatrixXd qEigenPoseI = m_qSpace.GetEigenVector(i);
		ApplyToSekeleton(qEigenPoseI, _tempSk);

		_tempSk.m_pRoot->m_offset[0] = (i + 1) * 100;
		_tempSk.drawLinksAndJointSpheres();
	}
}