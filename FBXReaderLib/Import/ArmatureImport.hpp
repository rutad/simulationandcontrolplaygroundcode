


#pragma once


///////////////////////////////////////////////////////////////////////////
//// Mesh import takes all the data possible, and application selects from the data buffer which is necessary(pos, uv, ...)
///////////////////////////////////////////////////////////////////////////

class AnimationImport
{
public:

	///< Import bone:
	struct Bone
	{
		std::string		m_name;

		int				m_id;
		float			m_l;

		std::vector<Bone>	m_childs;
		V3D					m_r;
		V3D					m_t;
		Quaternion          m_q;
	};

	///<
	int FindBoneIndex(const char* _csName)
	{
		for (int i=0; i<(int)m_boneArray.size();++i)
		{
			if (m_boneArray[i])
			{
				if (strcmp(m_boneArray[i]->m_name.c_str(),_csName)==0)
					return i;
			}
		}

		assert(false);// "Bone not found!"
		return -1;
	}

	Bone m_root;

	///< Built at final step; and used to reference others, don't need to delete them.
	std::vector<Bone*> m_boneArray;

	~AnimationImport(){
		
	}
};


