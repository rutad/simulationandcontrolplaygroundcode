#include <GUILib/GLUtils.h>
#include "TestAppLevelSets2D.h"
#include <GUILib/GLMesh.h>
#include <GUILib/GLContentManager.h>
#include <MathLib/MathLib.h>
#include <MathLib/Plane.h>
#include <FEMSimLib/CSTSimulationMesh2D.h>
#include <FEMSimLib/CSTSimulationMesh3D.h>
#include <FEMSimLib/MassSpringSimulationMesh3D.h>


TestAppLevelSets2D::TestAppLevelSets2D() {
	setWindowTitle("Test PDE Application...");

	TwAddSeparator(mainMenuBar, "sep2", "");

	showGroundPlane = false;

}

TestAppLevelSets2D::~TestAppLevelSets2D(void){
}

//triggered when mouse moves
bool TestAppLevelSets2D::onMouseMoveEvent(double xPos, double yPos) {

//	if (GLApplication::onMouseMoveEvent(xPos, yPos) == true) return true;
	return false;
}

//triggered when mouse buttons are pressed
bool TestAppLevelSets2D::onMouseButtonEvent(int button, int action, int mods, double xPos, double yPos) {

//	if (GLApplication::onMouseButtonEvent(button, action, mods, xPos, yPos)) return true;

	return false;
}

//triggered when using the mouse wheel
bool TestAppLevelSets2D::onMouseWheelScrollEvent(double xOffset, double yOffset) {
	if (GLApplication::onMouseWheelScrollEvent(xOffset, yOffset)) return true;

	return false;
}

bool TestAppLevelSets2D::onKeyEvent(int key, int action, int mods) {
	if (GLApplication::onKeyEvent(key, action, mods)) return true;

	return false;
}

bool TestAppLevelSets2D::onCharacterPressedEvent(int key, int mods) {
	if (GLApplication::onCharacterPressedEvent(key, mods)) return true;

	return false;
}


void TestAppLevelSets2D::loadFile(const char* fName) {
	Logger::consolePrint("Loading file \'%s\'...\n", fName);
	std::string fileName;
	fileName.assign(fName);

	std::string fNameExt = fileName.substr(fileName.find_last_of('.') + 1);
	Logger::consolePrint("...but how to do with that?");
}

void TestAppLevelSets2D::saveFile(const char* fName) {
	Logger::consolePrint("SAVE FILE: Do not know what to do with file \'%s\'\n", fName);
}


// Run the App tasks
void TestAppLevelSets2D::process() {
	//do the work here...

}


// Draw the App scene - camera transformations, lighting, shadows, reflections, etc apply to everything drawn by this method
void TestAppLevelSets2D::drawScene() {
	glDisable(GL_TEXTURE_2D);
	glDisable(GL_LIGHTING);
	glColor3d(1,1,1);

	levelSetFunction.draw();

}

// This is the wild west of drawing - things that want to ignore depth buffer, camera transformations, etc. Not pretty, quite hacky, but flexible. Individual apps should be careful with implementing this method. It always gets called right at the end of the draw function
void TestAppLevelSets2D::drawAuxiliarySceneInfo() {

}

// Restart the application.
void TestAppLevelSets2D::restart() {

}

bool TestAppLevelSets2D::processCommandLine(const std::string& cmdLine) {

	if (GLApplication::processCommandLine(cmdLine)) return true;

	return false;
}

