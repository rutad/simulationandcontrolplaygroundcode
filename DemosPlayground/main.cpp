#include <stdio.h>
#include <stdlib.h>
#include <../GUILib/GLApplication.h>

#include "TestAppFEMSim.h"
#include "TestAppFireworks.h"
#include "TestAppFEMSim3D.h"
#include "TestAppMCIntegration.h"
#include "TestAppPDEs.h"
#include "TestAppRBSim.h"
#include "TestAppIKRobot.h"
#include "TestAppLevelSets2D.h"
#include "TestAppRobotMotionOptimization.h"

int main(void){
	GLApplication* theApp;
	
//	theApp = new TestAppFEMSim();
//	theApp = new TestAppFEMSim3D();
//	theApp = new TestAppFireworks();
//	theApp = new TestAppMCIntegration();
//	theApp = new TestAppPDEs();
//	theApp = new TestAppRBSim();

//	theApp = new TestAppRobotMotionOptimization();

	theApp = new TestAppIKRobot();

//	theApp = new TestAppLevelSets2D();

	theApp->runMainLoop();
	delete theApp;
	return 0;
}


