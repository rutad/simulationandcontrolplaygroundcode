#pragma once
#include "GLWindow2D.h"
#include <MathLib/mathlib.h>
#include <MathLib/Trajectory.h>

class Box2d{
public:
	Box2d() {};
	Box2d(double posX_, double posY_, double sizeX_, double sizeY_, double originalSizeX_, double originalSizeY_, double scaleX_ = 1.0, double scaleY_ = 1.0);
	~Box2d();

	void reset(double posX_, double posY_, double sizeX_, double sizeY_, double originalSizeX_, double originalSizeY_, double scaleX_ = 1.0, double scaleY_ = 1.0);
	void computeFactor();

	void setPosition(double posX_, double posY_);
	void move(double x, double y);
	void resize(double sizeX_, double sizeY_);
	void scale(double x, double y);
	void reshape(double originalSizeX_, double originalSizeY_, double maxX, double maxY);

	// draws a vertex between 0 and 1
	void drawVertex(double x, double y, int pixelOffsetX = 0, int pixelOffsetY = 0);
	double getBoxPositionX(double x, int pixelOffsetX = 0);
	double getBoxPositionY(double y, int pixelOffsetY = 0);

	void outline();
	void fill();
	//private:
	double posX, posY;
	double originalSizeX, originalSizeY;
	double sizeX, sizeY;
	double scaleX, scaleY;
	double fx, fy;
};

class PlotData{
public:
	PlotData(const char* name);
	~PlotData();

	void add(double x, double y);
	void clearData();

	char* getName();
	void setColor(double r, double g, double b);
	void getColor(double& r, double& g, double& b);
	void setDrawLines(bool bDrawLines);
	void setDrawPoints(bool bDrawPoints);
	bool getDrawLines();
	bool getDrawPoints();
	double getLineWidth();
	void setLineWidth(double lineWidth);
	int getDataCount(){
		return data.getKnotCount();
	}

public:
	Trajectory1D data;

	double dataMin[2];
	double dataMax[2];

	char name[100];
	std::vector<double> color;
	double lineWidth;

	bool bDrawLines = true;
	bool bDrawPoints = false;
	bool bDrawShadow = false;
};

enum Widgetdouble {
	double_BOTTOM_LEFT,
	double_BOTTOM_RIGHT,
	double_TOP_LEFT,
	double_TOP_RIGHT
};

class PlotWindow : public GLWindow2D{
protected:
	// each window has two parts
	Box2d body, header;
	char title[100];

	int yAxTickWidth, xAxTickHeight;
	int titleHeight, legendWidth;
	
	std::vector<PlotData*> plotData;

	double xDataResolution;
	
	// returns index of data with name "name",
	// returns -1 if not existant
	int findDataByName(const char* name);

	void getExpForm(const double a, double &x, int &n);

	void glV2d(double x, double y);

public:
	PlotWindow(double posX, double posY, double sizeX, double sizeY);

	// look
	void setTitle(char* title);
	char* getTitle();
	void setLineLook(const char* name, bool bDrawLines, bool bDrawPoints, double lineWidth = 1.0);
	void setLineLook(int index, bool bDrawLines, bool bDrawPoints, double lineWidth = 1.0);
	
	// data
	virtual void createData(const char* name, double r, double g, double b);
	virtual void addDataPoint(const char* name, double x, double y);
	virtual void addDataPoint(int index, double x, double y);
	virtual void addDataPoint(double x, double y);
	virtual void addDataPoint(int index, double y);
	virtual void clearData(bool deleteContainer = false);


	void getYDataRange(double minX, double maxX, double &minY, double &maxY);
	void getXDataRange(double &minX, double &maxX);

	double getResolution();
	void setResolution(double res);

	virtual void draw();

	bool autoRangeY = true, autoRangeX = true, fixedXRange = false;
	double minYVal = -10, maxYVal = 10, minXVal = -10, maxXVal = 10, rangeX = 100;

	void setYRange(double minY, double maxY) {
		autoRangeY = false;
		this->minYVal = minY;
		this->maxYVal = maxY;
	}

	void setYToAutoRange() {
		autoRangeY = true;
	}

	//i.e. show only the last x data points
	void setToFixedXRange(double dim) {
		fixedXRange = true;
		autoRangeX = false;
		rangeX = dim;
	}

	void setXRange(double minX, double maxX) {
		autoRangeX = false;
		fixedXRange = false;
		this->minXVal = minX;
		this->maxXVal = maxX;
	}

	void setXToAutoRange() {
		autoRangeX = true;
		fixedXRange = false;
	}
};

