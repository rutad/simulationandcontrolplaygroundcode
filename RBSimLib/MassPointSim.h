#pragma once
#include <MathLib\P3D.h>
#include <MathLib\V3D.h>

using namespace std;

class MassPointSim
{
public:
    Eigen::SparseMatrix<double, Eigen::RowMajor> A, C, D;
    Eigen::DiagonalMatrix<double, Eigen::Dynamic> S, W;
    Eigen::VectorXd b, c, d, f, x;

    MassPointSim();
    ~MassPointSim();

    void simulate(vector<P3D> &startPositions, vector<V3D> &startVelocities, vector<P3D> &endPositions, vector<V3D> &forces, vector<V3D> &endVelocities, vector<P3D> &targetPositions, vector<double> masses, double timeStep);
};

