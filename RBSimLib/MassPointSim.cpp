#include "MassPointSim.h"
#include "OptimizationLib\QuadraticProblemFormulation.hpp"
#include "OptimizationLib\ooqpei_gtest_eigen.hpp"
#include <eigen3.2.5/Eigen/Core>

using namespace Eigen;
using namespace ooqpei;
using namespace ooqpei::eigen;
using namespace std;

MassPointSim::MassPointSim()
{
}

MassPointSim::~MassPointSim()
{
}

void MassPointSim::simulate(vector<P3D> &startPositions, vector<V3D> &startVelocities, vector<P3D> &endPositions, vector<V3D> &endVelocities, vector<V3D> &forces, vector<P3D> &targetPositions, vector<double> masses, double timeStep)
{
    if (startPositions.size() == 0 || startPositions.size() != targetPositions.size())
        return;

    int N = startPositions.size();

    A.resize(N * 3 * 2, N * 3 * 2);
    A.setZero();
    C.resize(N * 3, N * 3 * 2);
    C.setZero();

    Eigen::VectorXd s;
    S.resize(N * 3 * 2);
    S.setIdentity();
    W.resize(N * 3 * 2);
    W.setZero();

    b.resize(3 * N * 2);
    c.resize(3 * N);
    b.setZero();
    c.setZero();

    D.resize(N * 3, N * 3 * 2);
    D.setZero();
    d.resize(N * 3);
    f.resize(N * 3);
    // variables: positions, force
    // constrict: x(t+1)-h*h/m*force = x(t)+h*v(t)
    // energy: sum((x(t+1)-x0)'*(x(t+1)-x0))
    for (int i = 0;i < N;++i)
    {
        for (int j = 0;j < 3;++j)
        {
            // (x(t+1)-x0)'*(x(t+1)-x0)
            b[i * 3 + j] = targetPositions[i][j];
            A.insert(i * 3 + j, i * 3 + j) = 1.0;
            // 0.1*force'*force
            A.insert(i * 3 + j + N * 3, i * 3 + j + N * 3) = 0.0001;
            // equ constrict
            C.insert(i * 3 + j, i * 3 + j) = 1.0;
            C.insert(i * 3 + j, i * 3 + j + N * 3) = -timeStep * timeStep / masses[i];
            c[i * 3 + j] = startPositions[i][j] + timeStep * startVelocities[i][j];
            // inequ constrict
            D.insert(i * 3 + j, i * 3 + j + N * 3) = 0.0;//1.0;
            d[i * 3 + j] = -150.0;
            f[i * 3 + j] = 150.0;
        }
    }

    leastSquaresSolve(A, S, b, W, C, c, D, d, f, x);
    
    endPositions.clear();
    endVelocities.clear();
    forces.clear();
    for (int i = 0;i < N;++i)
    {
        P3D p = P3D(x[i * 3], x[i * 3 + 1], x[i * 3 + 2]);
        V3D v = (p - startPositions[i]) / timeStep;
        V3D force = (v - startVelocities[i]) / timeStep * masses[i];
        endPositions.push_back(p);
        endVelocities.push_back(v);;
        forces.push_back(force);
        if (force.length() > 0.01) {
            printf("p %.9lf %.9lf %.9lf\n", p[0], p[1], p[2]);

            printf("v %.9lf %.9lf %.9lf\n", v[0], v[1], v[2]);

            printf("f1 %.9lf %.9lf %.9lf\n", force[0], force[1], force[2]);
            printf("f2 %.9lf %.9lf %.9lf\n", x[i * 3 + 3 * N + 0], x[i * 3 + 3 * N + 1], x[i * 3 + 3 * N + 2]);
        }
    }
}
