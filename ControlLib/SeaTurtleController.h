#pragma once

//TODO:
// - how to compute Reynolds number - make drag coeff a function of it
// - apply torque due to hydrodynamic forces also (Cm)
// - catmul rom splines are not smooth enough. Should use the RBF interpolation method!
// - initialize the state of the sea turtle such that it already is going at some desired speed and the joint velocities match the trajectories...

#include <MathLib/Trajectory.h>
#include <Utils/Utils.h>
#include <RBSimLib/RigidBody.h>
#include <GUILib/GLUtils.h>
#include <RBSimLib/AbstractRBEngine.h>
#include "ControlParameter.h"

class DragPlate{
public:
	RigidBody* body;
	V3D normal;
	P3D midPoint;
	double area;

	DragPlate(RigidBody* prb, V3D n, P3D p, double a){
		body = prb;
		normal = n;
		midPoint = p;
		area = a;
	}

	void draw(){
		P3D worldPoint = body->getWorldCoordinates(midPoint);
		V3D worldNormal = body->getWorldCoordinates(normal);

//		drawDisk(sqrt(area/PI), worldPoint, worldNormal);
		glBegin(GL_LINES);
			glVertex3d(worldPoint[0], worldPoint[1], worldPoint[2]);
			glVertex3d(worldPoint[0]+worldNormal[0]/10.0, worldPoint[1]+worldNormal[1]/10.0, worldPoint[2]+worldNormal[2]/10.0);
		glEnd();
	}

	V3D computeDragForce(V3D fluidVelocity){
		V3D worldNormal = body->getWorldCoordinates(normal);
		V3D relVelocity = fluidVelocity - body->getAbsoluteVelocityForLocalPoint(midPoint);
		double relVNormal = worldNormal.dot(relVelocity);
		if (relVNormal > 0)
			return V3D();

//		double dragCoef = 2;
		double dragCoef = 1.28;
		double fluidDensity = 1000;
		double forceMagnitude = 0.5 * dragCoef * fluidDensity * relVNormal * relVNormal * area;

		return V3D(worldNormal * -forceMagnitude);
	}
};

class FinElement{
public:
	RigidBody* body;
	V3D normal;
	V3D chordAxis;
	P3D applicationPoint;
	double area;

public:
	FinElement(RigidBody* p, V3D n, V3D ca, P3D ap, double a);

	void computeDragAndLiftForces(V3D fluidVelocity, V3D* dragForce, V3D* liftForce);

	void glPoint(P3D p){
		glVertex3d(p[0], p[1], p[2]);
	}

	void draw(V3D fluidVelocity){
		V3D dragForce, liftForce;

		computeDragAndLiftForces(fluidVelocity, &dragForce, &liftForce);
		P3D appPoint = body->getWorldCoordinates(applicationPoint);

		glBegin(GL_LINES);
			glColor3d(1,0,0);
			glPoint(appPoint);
			glPoint(appPoint + dragForce * 0.1);
			glColor3d(0,1,0);
			glPoint(appPoint);
			glPoint(appPoint + liftForce * 0.1);
		glEnd();
	}
};

inline void addFinElements(DynamicArray<FinElement>* finElems, RigidBody* theFin, P3D chordStart, P3D transverseStart, P3D chordEnd, P3D transverseEnd, double nrElems){
	V3D normal = V3D(chordStart, chordEnd).cross(V3D(transverseStart, transverseEnd));
	double totalArea = normal.length();
	normal.toUnit();

	for (int i=0;i<nrElems;i++){
		finElems->push_back(FinElement(theFin, normal, V3D(chordStart, chordEnd).unit(), transverseStart + V3D(transverseStart, transverseEnd) * ((double)(i+0.5)/nrElems), totalArea/nrElems));
	}
}

inline void addDragPlates(DynamicArray<DragPlate>* dragPlates, RigidBody* arb, V3D normal, P3D p1, P3D p2, P3D p3, P3D p4, double factor = 1){
	V3D v1(p1, p2);
	V3D v2(p3, p4);
	double area = v1.cross(V3D(p2, p3)).length() * factor;
//	tprintf("area: %lf\n", area);
	P3D t1 = p1 + v1 * 0.15;
	P3D t2 = p3 + v2 * 0.15;
	P3D t = t1 + V3D(t1, t2) * 0.15;
	dragPlates->push_back(DragPlate(arb, normal, t, area/4.0));
	t = t1 + V3D(t1, t2) * 0.85;
	dragPlates->push_back(DragPlate(arb, normal, t, area/4.0));

	t1 = p1 + v1 * 0.85;
	t2 = p3 + v2 * 0.85;
	t = t1 + V3D(t1, t2) * 0.15;
	dragPlates->push_back(DragPlate(arb, normal, t, area/4.0));
	t = t1 + V3D(t1, t2) * 0.85;
	dragPlates->push_back(DragPlate(arb, normal, t, area/4.0));
}

typedef int TRAJ_COUNT[6];

class SeaTurtleController{
public:
	AbstractRBEngine* rbEngine;

	DynamicArray<DragPlate> dragPlates;
	DynamicArray<FinElement> finElems;

	double strideDuration;

	Trajectory1D leftShoulderTwist;
	Trajectory1D leftShoulderUpDown;
	Trajectory1D leftShoulderForwardsBackwards;
	Trajectory1D leftHipUpDown;

	Trajectory1D rightShoulderTwist;
	Trajectory1D rightShoulderUpDown;
	Trajectory1D rightShoulderForwardsBackwards;
	Trajectory1D rightHipUpDown;

	Trajectory1D thrusterForce;


	//define the list of parameters that parameterize the various gaits...
	DynamicArray<ControlParameter> parameterList;
	int P_STRIDE_DURATION;

	TRAJ_COUNT P_LEFT_SHOULDER_TWIST;
	TRAJ_COUNT P_LEFT_SHOULDER_UP_DOWN;
	TRAJ_COUNT P_LEFT_SHOULDER_FORWARDS_BACKWARDS;
	TRAJ_COUNT P_LEFT_HIP_UP_DOWN;

	TRAJ_COUNT P_RIGHT_SHOULDER_TWIST;
	TRAJ_COUNT P_RIGHT_SHOULDER_UP_DOWN;
	TRAJ_COUNT P_RIGHT_SHOULDER_FORWARDS_BACKWARDS;
	TRAJ_COUNT P_RIGHT_HIP_UP_DOWN;

	RigidBody* mainBody;
	Joint* leftShoulder;
	Joint* rightShoulder;
	Joint* leftHip;
	Joint* rightHip;

	//state information
	double stridePhase;

	void setupSeaTurtleController();
public:
	SeaTurtleController(AbstractRBEngine* world);
	~SeaTurtleController(void);

	void computeForcesAndControlInputs(V3D fluidVel = V3D(0,0,0));
	void step(double dt);

	Quaternion getDesiredBodyWorldOrientation();
	Quaternion getBodyOrientationError();

	Quaternion getLeftShoulderOrientation(double t);
	Quaternion getRightShoulderOrientation(double t);
	Quaternion getLeftHipOrientation(double t);
	Quaternion getRightHipOrientation(double t);

	void setParameters();
};


