#include <ControlLib/CharacterState.h>
#include <ControlLib/ArticulatedCharacter.h>
#include <RBSimLib/RigidBody.h>
#include <RBSimLib/RBState.h>
#include <RBSimLib/Joint.h>
#include <RBSimLib/HingeJoint.h>
#include <RBSimLib/UniversalJoint.h>
#include <fstream>
#include <iomanip>
#include <queue>

CharacterState::CharacterState(const ArticulatedCharacter* character) : m_character(character) {
	populateState(character);
}

CharacterState::~CharacterState() {
}

void CharacterState::populateState(const ArticulatedCharacter* _character) {
	// state of each rigid body
	int numBodies = (int)_character->getRigidBodyCount();
	m_rigidBodyStates.resize(numBodies);
	for (int i = 0; i < numBodies; ++i) {
		RigidBody* body = _character->getRigidBody(i);
		m_rigidBodyStates[i] = body->state;
	}
}

int CharacterState::getJointCount() {
	return m_character->getJointCount();
}


// ------------------------------------------------------------------------------------------------
// get state values

P3D CharacterState::getRootPosition() const {
	return m_rigidBodyStates[0].position;
}

Quaternion CharacterState::getRootOrientation() const {
	return m_rigidBodyStates[0].orientation;
}

V3D CharacterState::getRootVelocity() const {
	return m_rigidBodyStates[0].velocity;
}

V3D CharacterState::getRootAngularVelocity() const {
	return m_rigidBodyStates[0].angularVelocity;
}

P3D CharacterState::getRigidBodyPosition(int index) const {
	return m_rigidBodyStates[index].position;
}

Quaternion CharacterState::getRigidBodyOrientation(int index) const {
	return m_rigidBodyStates[index].orientation;
}

V3D CharacterState::getRigidBodyVelocity(int index) const {
	return m_rigidBodyStates[index].velocity;
}

V3D CharacterState::getRigidBodyAngularVelocity(int index) const {
	return m_rigidBodyStates[index].angularVelocity;
}

RBState CharacterState::getRigidBodyState(int index) const {
    return m_rigidBodyStates[index];
}


// ------------------------------------------------------------------------------------------------
// compute values depending on state

Quaternion CharacterState::getJointRelativeOrientation(int index) const {
    int parentIndex;
    int childIndex;
    m_character->getParentAndChildIndex(index, parentIndex, childIndex);

    Quaternion& parentQ = getRigidBodyOrientation(parentIndex);
    Quaternion& childQ = getRigidBodyOrientation(childIndex);

    Quaternion relQ = (parentQ.getComplexConjugate() * childQ).toUnit();

    return relQ;
}

V3D CharacterState::getJointRelativeAngVelocity(int index) const {
    int parentIndex;
    int childIndex;
    m_character->getParentAndChildIndex(index, parentIndex, childIndex);

    V3D parentW = getRigidBodyAngularVelocity(parentIndex);
    V3D childW = getRigidBodyAngularVelocity(childIndex);

    Quaternion parentQ = getRigidBodyOrientation(parentIndex);

    V3D relW_world = childW - parentW; // in world coordinates
    V3D relW = parentQ.getComplexConjugate().rotate(relW_world); // in parent coordinates

    return relW;
}

// ------------------------------------------------------------------------------------------------
// reading/writing from/to file

void CharacterState::writeToFile(const std::string& fileName, ArticulatedCharacter* character) {
	std::ofstream outStream(fileName.c_str());

	outStream << "# For each rigid body we have:" << std::endl;
	outStream << "#		Name of the rigid body (optional)" << std::endl;
	outStream << "#		Position (world coordinates)" << std::endl;
	outStream << "#		Orientation (world coordinates)" << std::endl;
	outStream << "#		Velocity (world coordinates)" << std::endl;
	outStream << "#		Angular velocity (world coordinates)" << std::endl;
	outStream << std::endl; // empty line

	
	for (int i = 0; i < (int)m_rigidBodyStates.size(); ++i) {
		RBState& state = m_rigidBodyStates[i];
		V3D position = state.position;
		Quaternion orientation = state.orientation;
		V3D velocity = state.velocity;
		V3D angVelocity = state.angularVelocity;

		if (character) {
			std::string name = character->getRigidBody(i)->name;
			outStream << "# " << name << std::endl;
		}

		outStream << std::setprecision(PRECISION) << position[0] << " " << position[1] << " " << position[2] << std::endl;
		outStream << std::setprecision(PRECISION) << orientation[0] << " " << orientation[1] << " " << orientation[2] << " " << orientation[3] << std::endl;
		outStream << std::setprecision(PRECISION) << velocity[0] << " " << velocity[1] << " " << velocity[2] << std::endl;
		outStream << std::setprecision(PRECISION) << angVelocity[0] << " " << angVelocity[1] << " " << angVelocity[2] << std::endl;
		outStream << std::endl; // empty line
	}
}

void CharacterState::readFromFile(const std::string fileName) {
	std::ifstream inStream(fileName.c_str());

	for (int i = 0; i < (int)m_rigidBodyStates.size(); ++i) {
		RBState& state = m_rigidBodyStates[i];
		P3D& position = state.position;
		Quaternion& orientation = state.orientation;
		V3D& velocity = state.velocity;
		V3D& angVelocity = state.angularVelocity;
		
		readValidLineAsStream(inStream) >> position[0] >> position[1] >> position[2];
		readValidLineAsStream(inStream) >> orientation[0] >> orientation[1] >> orientation[2] >> orientation[3];
		readValidLineAsStream(inStream) >> velocity[0] >> velocity[1] >> velocity[2];
		readValidLineAsStream(inStream) >> angVelocity[0] >> angVelocity[1] >> angVelocity[2];
	}
}
