#pragma once
#include <GUILib/GLApplication.h>

#include <MathLib/MathLib.h>
#include <MathLib/P3D.h>
#include <MathLib/V3D.h>

// to set the correct rotation axes
#include <RBSimLib/HingeJoint.h>
#include <RBSimLib/UniversalJoint.h>
#include <RBSimLib/BallAndSocketJoint.h>

#include <MathLib/Trajectory.h>
#include <ControlLib/AnimationTime.h>


// TODO: Clean up this clas!!!
// This class is just a copy of the AnimatedRobot adapted to ArticulatedCharacter etc. instead of robot.
// It has way to much stuff here that does not all belong to the same place.


class ArticulatedCharacter;
class CharacterPose;
class LinearSubspace;

///< This class implements key framed character poses.
class AnimatedCharacter
{

	ArticulatedCharacter*								m_pCharacter = NULL;
	
	///< Used for the get interpolated frame function (to return);
	CharacterPose*					m_pTempState		= NULL;

	/// Extract a linear subspace from the key frames.
	LinearSubspace*						m_pLinearSubspace	= NULL;

	enum RootUpdatePolicy
	{
		INTERPOLATE_ROOT=0,
		INTEGRATE_ROOT,
		N
	};

	RootUpdatePolicy m_rootUpdatePolicy = INTERPOLATE_ROOT;

	MatrixNxM		m_tempReducedM;
	MatrixNxM		m_tempFullSpace;

	dVector			vectorize					(CharacterPose* _pRBS);
	double getJointRelativeAngle(CharacterPose* _pRBS, int _jIndex);
	
protected:

	int									m_timer				= 0;
	int									m_maxTicks			= 200;

	int									m_framesPerSecond	= 120;

	int									m_frameIndex		= 0;
	int									m_frameIndexStep	= 1;



	std::string							m_pathName;

	void								getInterpolatedFrameImpl(const double _t, CharacterPose* _animatedFrame, bool _bLoopEndWithFirstFrame = true, bool _bInterpolateRoot=false);

	AnimationTimer						m_animationTimer;
//	DiscreteAnimationTimer				m_discreteTimer;

public:


	std::vector<CharacterPose*>		m_keyFrames;


	ArticulatedCharacter*				getCharacter() { return m_pCharacter; }

	AnimatedCharacter(ArticulatedCharacter* _pCharacter, TwBar* _pMenu = NULL);
	~AnimatedCharacter();

	/*
		Create a set of key frames.
	*/
	void				createKeyFrames				(const int _numKeyFrames);
	void				createSubsetOfKeyFrames		(int _minIndex, int _maxIndex);

	void				releaseKeyFrames			();

	void				setPathName					(const std::string& _pathName);
	void				saveKeyFrames				();
	void				loadKeyFrames				(bool _bCopyFirstAsLast = false);

	int					numKeyFrames				() { return m_keyFrames.size(); }
	
	//<
	void				updateCurrentFrameIndex		();
	int					getCurrentFrameIndex		() { return m_frameIndex; }

	//<
	void				translate					(const V3D& _x);
	void				fixDistanceToGround			();
private:
	///< Note used:
	void				alignAnimatedPoseWithGround	(CharacterPose* pAnimatedFrame);
public:


	/*
		* Maybe this should be a child class: a "TimedAnimatedCharacter"?
	*/
	AnimationTimer&		getTimer() { return m_animationTimer; }
	int					getCurrentTimeIndex();
	CharacterPose*	getCurrentFrame();
	CharacterPose*  getCurrentInterpolatedFrame();


	/*
		Access
	*/
	CharacterPose*	getFrameAtIndex(int _i);
	CharacterPose*	getFirstFrame				();

	CharacterPose*  getInterpolatedFrame					(const double _t);
	void				getInterpolatedFrame					(const double _t, CharacterPose* _pAnimatedFrame);

	void				getInterpolatedFrameIntegrateRoot		(const double _t, int _numCycles, CharacterPose* _pAnimatedFrame);

	V3D					getInterpolatedVelocity					(double _t0, double _t1);
	P3D					getInterpolatedRootPosition				(double _t);


	/*
	* Closed loop: Linearly interpolate frame at time t in [0, 1], assuming the last and first frames are looping.
	*/
	void				getInterpolatedFrameLoopEndWithFirst(const double _t, CharacterPose* _pAnimatedFrame);


	/*
		Draw the Key Frames
	*/
	void				drawKeyFrames						(int flags, V3D* _pOffset=NULL);
	static void			drawCharacterSkeleton				(ArticulatedCharacter* _pCharacter, int flags);
	static void			drawState							(ArticulatedCharacter* _pCharacter, CharacterPose* _pState, int _flags);

	/*
		* Linear subspace:
	*/
	void				createLinearSubspace				(int _dimOfSubSpace);
	dVector				reduceStateWithLinearProjection		(CharacterPose* _pRBS);
	dVector				reconstructSubLinearPose			(const dVector& _subSpaceX);
	int					getLinearSubSpaceDim				() const;




};


///<
class AnimatedCharacterTimelineDraw
{
public:

	AnimatedCharacterTimelineDraw(AnimatedCharacter* _pCharacterKeyFrames, Trajectory1D* _pTimeWarp, const double _t);

};

