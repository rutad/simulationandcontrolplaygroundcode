#include <ControlLib/ArticulatedCharacter.h>
#include <ControlLib/CharacterState.h>
#include <ControlLib/CharacterPose.h>

#include <Utils/Utils.h>

#include <RBSimLib/RigidBody.h>
#include <RBSimLib/Joint.h>
#include <RBSimLib/HingeJoint.h>
#include <RBSimLib/UniversalJoint.h>
#include <RBSimLib/BallAndSocketJoint.h>

#include <queue>

ArticulatedCharacter::ArticulatedCharacter(RigidBody* root) {
	if (root == NULL) {
		throwError("Can't build create a character without a root!\n");
	}

	retrieveRigidBodies(root);
	retrieveEndEffectorRigidBodies();
    setRigidBodyIndexMap();
	retrieveJoints();
    setJointIndices();

	computeTotalMass();
}

ArticulatedCharacter::~ArticulatedCharacter() {
    m_rigidBodies.clear();
	m_endEffectorBodies.clear();
}


void ArticulatedCharacter::retrieveRigidBodies(RigidBody* root) {
	// traverse down the structure and collect all rigid bodies (breadth first search)
	std::queue<RigidBody*> bodyQueue; // bodies for which we need to retrieve the joint
	bodyQueue.push(root);
	while (!bodyQueue.empty()) {
		RigidBody* currentBody = bodyQueue.front();

		if (currentBody->pJoints.size() > 1) {
			throwError("Possible kinematic loop detected in character definition. Not currently allowed...\n");
		}

		for (Joint* joint : currentBody->cJoints) {
			bodyQueue.push(joint->child);
		}
		bodyQueue.pop();

		m_rigidBodies.push_back(currentBody);
	}
}

void ArticulatedCharacter::retrieveEndEffectorRigidBodies() {
	for (RigidBody* body : m_rigidBodies) {
		if (body->rbProperties.getEndEffectorPointCount() > 0) {
			m_endEffectorBodies.push_back(body);
		}
	}
}

void ArticulatedCharacter::setRigidBodyIndexMap() {
    for (int i = 0; i < (int)m_rigidBodies.size(); ++i) {
        RigidBody* body = m_rigidBodies[i];
        m_bodyIdToIndexMap.insert({ body->id, i });
    }
}

void ArticulatedCharacter::retrieveJoints() {
	for (RigidBody* body : m_rigidBodies) {
		for (Joint* joint : body->cJoints) {
			m_joints.push_back(joint);
		}
	}
}

void ArticulatedCharacter::setJointIndices() {
    for (int i = 0; i < (int)m_joints.size(); i++) {
        m_joints[i]->jIndex = i;
    }
}

void ArticulatedCharacter::computeTotalMass() {
	for (RigidBody* body : m_rigidBodies) {
		m_mass += body->rbProperties.mass;
	}
}

// ------------------------------------------------------------------------------------------------
// rigid body getter

RigidBody* ArticulatedCharacter::getRoot() const {
	return m_rigidBodies[0];
}

RigidBody* ArticulatedCharacter::getRigidBody(int index) const {
	if (index >= 0 && index < (int)m_rigidBodies.size()) {
		return m_rigidBodies[index];
	} else {
		return NULL;
	}
}

RigidBody* ArticulatedCharacter::getRigidBody(std::string name) const {
	for (RigidBody* body : m_rigidBodies) {
		if (body->name == name) {
			return body;
		}
	}
	return NULL;
}

const std::vector<RigidBody*>& ArticulatedCharacter::getRigidBodies() const {
	return m_rigidBodies;
}

std::vector<RigidBody*>& ArticulatedCharacter::getRigidBodies() {
	return m_rigidBodies;
}

int ArticulatedCharacter::getRigidBodyCount() const {
	return (int)m_rigidBodies.size();
}

// ------------------------------------------------------------------------------------------------
// joint getter

Joint* ArticulatedCharacter::getJoint(int index) const {
	if (index >= 0 && index < (int)m_joints.size()) {
		return m_joints[index];
	} else {
		return NULL;
	}
}

Joint* ArticulatedCharacter::getJoint(std::string name) const {
	for (Joint* joint : m_joints) {
		if (joint->name == name) {
			return joint;
		}
	}
	return NULL;
}

int ArticulatedCharacter::getJointIndex(std::string name) const {
    return getJoint(name)->jIndex;
}

const std::vector<Joint*>& ArticulatedCharacter::getJoints() const {
	return m_joints;
}

std::vector<Joint*>& ArticulatedCharacter::getJoints() {
	return m_joints;
}

int ArticulatedCharacter::getJointCount() const {
	return (int)m_joints.size();
}

// ------------------------------------------------------------------------------------------------
// compute joint related values

// for a single joint of the pose
void ArticulatedCharacter::getJointAxisAngles(CharacterPose* _pPose, int jIndex, std::vector<double>& angles) const {
	Quaternion qRel = _pPose->getJointRelativeOrientation(jIndex);
	getJointAxisAngles(jIndex, qRel, angles);
}

// for all joints of the pose
void ArticulatedCharacter::getJointAxisAngles(CharacterPose* _pPose, std::vector<double>& angles) const {
	int numJoints = getJointCount();
	std::vector<Quaternion> jointOrientations(numJoints);
	for (int i = 0; i < numJoints; ++i) {
		jointOrientations[i] = _pPose->getJointRelativeOrientation(i);
	}
	getJointAxisAnglesFromOrientations(jointOrientations, angles);
}

// for a single joint of the state
void ArticulatedCharacter::getJointAxisAngles(CharacterState* _pState, int jIndex, std::vector<double>& angles) const {
	Quaternion qRel = _pState->getJointRelativeOrientation(jIndex);
	getJointAxisAngles(jIndex, qRel, angles);
}

// for all joints of the state
void ArticulatedCharacter::getJointAxisAngles(CharacterState* _pState, std::vector<double>& angles) const {
	int numJoints = getJointCount();
	std::vector<Quaternion> jointOrientations(numJoints);
	for (int i = 0; i < numJoints; ++i) {
		jointOrientations[i] = _pState->getJointRelativeOrientation(i);
	}
	getJointAxisAnglesFromOrientations(jointOrientations, angles);
}

// for all the given joint orientations
void ArticulatedCharacter::getJointAxisAnglesFromOrientations(const std::vector<Quaternion>& _jointOrientations, std::vector<double>& _jointAxisAngles) const {
	assert(_jointOrientations.size() == getJointCount());
	
	_jointAxisAngles.clear();

	int numJoints = getJointCount();
	for (int i = 0; i < numJoints; ++i) {
		std::vector<double> angles;
		getJointAxisAngles(i, _jointOrientations[i], angles);
		for (double angle : angles) {
			_jointAxisAngles.push_back(angle);
		}
	}
}

// computes the angles for the given joint orientation according to joint type
void ArticulatedCharacter::getJointAxisAngles(int jIndex, const Quaternion& qRel, std::vector<double>& angles) const {
	angles.clear();

	// retrieve the joint and then figure out what type it is in order to correctly retrieve the joint angle
	Joint* joint = getJoint(jIndex);

	// one rotation axis for the hinge joint
	HingeJoint* hingeJoint = dynamic_cast<HingeJoint*>(joint);
	if (hingeJoint) {
		V3D axis = hingeJoint->rotationAxis;
		double angle1;
		computeRotationAngleFromQuaternion(qRel, axis, angle1);
		angles.push_back(angle1);
	}

	// two rotation axes for the universal joint
	UniversalJoint* universalJoint = dynamic_cast<UniversalJoint*>(joint);
	if (universalJoint) {
		V3D axis1 = universalJoint->rotAxisChild;
		V3D axis2 = universalJoint->rotAxisParent;
		double angle1;
		double angle2;
		computeEulerAnglesFromQuaternion(qRel, axis2, axis1, angle2, angle1);
		angles.push_back(angle1);
		angles.push_back(angle2);
	}

	// three rotation axes for the ball and socket joint
	BallAndSocketJoint* ballAndSocketJoint = dynamic_cast<BallAndSocketJoint*>(joint);
	if (ballAndSocketJoint) {
		V3D axis1 = V3D(1, 0, 0);
		V3D axis2 = V3D(0, 1, 0);
		V3D axis3 = V3D(0, 0, 1);
		double angle1;
		double angle2;
		double angle3;
		computeEulerAnglesFromQuaternion(qRel, axis3, axis2, axis1, angle3, angle2, angle1);
		angles.push_back(angle1);
		angles.push_back(angle2);
		angles.push_back(angle3);
	}
}

// computes the orientations from the given angles according to joint type
void ArticulatedCharacter::getJointOrientationsFromAngles(const std::vector<double>& _jointAxisAngles, std::vector<Quaternion>& _jointOrientations) {
	assert(_jointAxisAngles.size() == getNumDof());

	int numJoints = getJointCount();
	_jointOrientations.resize(numJoints);

	int angleIndex = 0;
	Quaternion relQ;
	for (int i = 0; i < numJoints; ++i) {
		Joint* joint = m_joints[i];

		if (HingeJoint* hingeJoint = dynamic_cast<HingeJoint*> (joint)) {
			V3D axis = hingeJoint->rotationAxis; // in parent coordinates
			double angle = _jointAxisAngles[angleIndex++];

			relQ.setRotationFrom(angle, axis);
		}

		if (UniversalJoint* universalJoint = dynamic_cast<UniversalJoint*> (joint)) {
			V3D axis1 = universalJoint->rotAxisChild; // in child coordinates
			double angle1 = _jointAxisAngles[angleIndex++];
			Quaternion relQ1;
			relQ1.setRotationFrom(angle1, axis1);

			V3D axis2 = universalJoint->rotAxisParent; // in parent coordinates
			double angle2 = _jointAxisAngles[angleIndex++];
			Quaternion relQ2;
			relQ2.setRotationFrom(angle2, axis2);

			relQ = relQ1 * relQ2;
		}

		if (BallAndSocketJoint* ballAndSocketJoint = dynamic_cast<BallAndSocketJoint*> (joint)) {
			RigidBody* parent = ballAndSocketJoint->parent;
			RigidBody* child = ballAndSocketJoint->child;

			V3D axis1 = V3D(1, 0, 0); // in parent coordinates
			double angle1 = _jointAxisAngles[angleIndex++];
			Quaternion relQ1;
			relQ1.setRotationFrom(angle1, axis1);

			V3D axis2 = V3D(0, 1, 0); // in parent coordinates
			double angle2 = _jointAxisAngles[angleIndex++];
			Quaternion relQ2;
			relQ2.setRotationFrom(angle2, axis2);

			V3D axis3 = V3D(0, 0, 1); // in parent coordinates
			double angle3 = _jointAxisAngles[angleIndex++];
			Quaternion relQ3;
			relQ3.setRotationFrom(angle3, axis3);

			relQ = relQ1 * relQ2 * relQ3;
		}

		_jointOrientations[i] = relQ;
	}
}

// ------------------------------------------------------------------------------------------------
// end effector stuff

std::vector<RigidBody*>& ArticulatedCharacter::getEndEffectorRigidBodies() {
	return m_endEffectorBodies;
}

int ArticulatedCharacter::getEndEffectorCount() const {
	return (int)m_endEffectorBodies.size();
}

P3D ArticulatedCharacter::getEndEffectorWorldPosition(int _i) const {
	P3D eeLocal = m_endEffectorBodies[_i]->rbProperties.endEffectorPoints[0].coords;
	P3D eeWorld = m_endEffectorBodies[_i]->getWorldCoordinates(eeLocal);
	return eeWorld;
}

double ArticulatedCharacter::getEndEffectorRadius(int _i) const {
	double r = 0.0;

	SphereCDP* pSphere = (SphereCDP*)m_endEffectorBodies[_i]->cdps[0];
	if (pSphere) {
		r = pSphere->r;
	}

	return r;
	//return eeBodies[_i]->rbProperties.endEffectorPoints[0].featureSize;
}

// ------------------------------------------------------------------------------------------------
// other getter

double ArticulatedCharacter::getMass() const {
	return m_mass;
}

void ArticulatedCharacter::getParentAndChildIndex(int jointIndex, int& parentIndex, int& childIndex) const {
    Joint* joint = getJoint(jointIndex);
    RigidBody* parent = joint->parent;
    RigidBody* child = joint->child;
    
	m_bodyIdToIndexMap.at(parent->id);
    parentIndex = m_bodyIdToIndexMap.at(parent->id);
    childIndex = m_bodyIdToIndexMap.at(child->id);
}

int ArticulatedCharacter::getNumDof() const {
	int numDof = 0;
	int numJoints = getJointCount();
	for (Joint* joint : m_joints) {
		if (HingeJoint* hingeJoint = dynamic_cast<HingeJoint*> (joint)) {
			numDof += 1;
		}
		if (UniversalJoint* universalJoint = dynamic_cast<UniversalJoint*> (joint)) {
			numDof += 2;
		}
		if (BallAndSocketJoint* ballAndSocketJoint = dynamic_cast<BallAndSocketJoint*> (joint)) {
			numDof += 3;
		}
	}
	return numDof;
}

// ------------------------------------------------------------------------------------------------
// compute state depending values

P3D ArticulatedCharacter::computeCOM() const {
	P3D com(0.0);
	for (RigidBody* body : m_rigidBodies) {
		com += body->getCMPosition() * body->rbProperties.mass;
	}
	double totalMass = getMass();
	com /= totalMass; 

	return com;
}

V3D ArticulatedCharacter::computeCOMVelocity() const {
	V3D comVel(0.0);
	for (RigidBody* body : m_rigidBodies) {
		comVel += body->getCMVelocity() * body->rbProperties.mass;
	}
	double totalMass = getMass();
	comVel /= totalMass;

	return comVel;
}

Quaternion ArticulatedCharacter::getHeading() const {
    // computeHeading() is defined in Quaternion.h
    return computeHeading(getRoot()->getOrientation(), Globals::worldUp);
}

Quaternion ArticulatedCharacter::computeJointRelativeOrientation(int jointIndex) const {
	return m_joints[jointIndex]->computeRelativeOrientation();
}

V3D ArticulatedCharacter::computeJointRelativeAngularVelocity(int jointIndex) const {
	V3D childW = m_joints[jointIndex]->child->getAngularVelocity();
	V3D parentW = m_joints[jointIndex]->parent->getAngularVelocity();
	V3D relW_world = childW - parentW; // in world coordinates
	// we will store relW in the parent's coordinates, to get an orientation invariant expression for it
	V3D relW_parent = m_joints[jointIndex]->parent->getLocalCoordinates(relW_world);
	return relW_parent;
}

// ------------------------------------------------------------------------------------------------
// set/get load/save state

void ArticulatedCharacter::setState(const CharacterState& characterState) {
	// set all rigid body states
	for (int i = 0; i < (int)getRigidBodyCount(); ++i) {
		RigidBody* body = m_rigidBodies[i];
		body->state = characterState.getRigidBodyState(i);
	}
}

void ArticulatedCharacter::setState(const CharacterState* characterState) {
	setState(*characterState);
}

void ArticulatedCharacter::setPose(const CharacterPose& characterPose) {
	// set root body state
	RigidBody* root = getRoot();
	root->state.position = characterPose.getRootPosition();
	root->state.orientation= characterPose.getRootOrientation();
	root->state.velocity = characterPose.getRootLinearVelocity();
	root->state.angularVelocity = characterPose.getRootAngularVelocity();

	// set state for remaining bodies
	// traverse down the hierarchy (joint list already in hierarchical order)
	//     keep parent fixed
	//     set orientation of child
	//     set angular velocity of child
	//     fix joint constraints
	int numJoints = (int)m_joints.size();
	for (int i = 0; i < numJoints; ++i) {
		Quaternion relQ = characterPose.getJointRelativeOrientation(i); // parent to child, unit quaternion
		V3D relW = characterPose.getJointRelativeAngularVelocity(i); // in parent coordinates

		Joint* joint = m_joints[i];

		// child orientation in world coordinates
		joint->child->state.orientation = joint->parent->getOrientation() * relQ;

		// relative angular velocity in world coordinates
		V3D relW_world = joint->parent->getWorldCoordinates(relW);
		// total angular velocity of child in world coordinates
		joint->child->state.angularVelocity = joint->parent->state.angularVelocity + relW_world;

		// now we have to fix the joint constraints to get the position and linear velocity (and consider degrees of freedom)
		// parent needs to be already fixed -> guaranteed since m_joints is ordered according to hierarchy
		joint->fixJointConstraints(true, true, true, true);
	}
}

void ArticulatedCharacter::setPose(const CharacterPose* characterPose) {
	setPose(*characterPose);
}

void ArticulatedCharacter::saveRBSToFile(std::string fileName) {
	FILE* fp = fopen(fileName.c_str(), "w");

	// first need to write all the rigid bodies
	for (RigidBody* body : m_rigidBodies) {
		body->writeToFile(fp);
	}

	// then all the joints
	for (Joint* joint : m_joints) {
		joint->writeToFile(fp);
	}

	fclose(fp);
}
