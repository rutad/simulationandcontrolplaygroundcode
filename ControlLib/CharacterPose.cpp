#include <ControlLib/CharacterPose.h>
#include <ControlLib/ArticulatedCharacter.h>
#include <RBSimLib/RigidBody.h>
#include <RBSimLib/Joint.h>
#include <iomanip>
// FIXME: for now need robot to read rbs animations through ReducedRobotState because all animations are stored from ReducedRobotState
#include <ControlLib/Robot.h>

CharacterPose::CharacterPose(const ArticulatedCharacter* _character) {
	populatePose(_character);
}

CharacterPose::CharacterPose(const CharacterPose* _characterPose) {
	setPose(_characterPose);
}

CharacterPose::~CharacterPose() {
	m_relQ.clear();
	m_relW.clear();
}

void CharacterPose::populatePose(const ArticulatedCharacter* _character) {
	// root data
	RigidBody* root = _character->getRoot();
	m_rootPosition = root->getCMPosition();
	m_rootOrientation = root->getOrientation();
	m_rootLinVel = root->getCMVelocity();
	m_rootAngVel = root->getAngularVelocity();

	// joint data
	int numJoints = _character->getJointCount();
	m_relQ.resize(numJoints);
	m_relW.resize(numJoints);
	for (int i = 0; i < numJoints; ++i) {
		m_relQ[i] = _character->computeJointRelativeOrientation(i);
		m_relW[i] = _character->computeJointRelativeAngularVelocity(i);
	}
}

int CharacterPose::getJointCount() const {
	return (int)m_relQ.size();
}

void CharacterPose::setPose(const CharacterPose& pose) {
	// root data
	m_rootPosition = pose.getRootPosition();
	m_rootOrientation = pose.getRootOrientation();
	m_rootLinVel = pose.getRootLinearVelocity();
	m_rootAngVel = pose.getRootAngularVelocity();

	// joint data
	int numJoints = pose.getJointCount();
	m_relQ.resize(numJoints);
	m_relW.resize(numJoints);
	for (int i = 0; i < numJoints; ++i) {
		m_relQ[i] = pose.getJointRelativeOrientation(i);
		m_relW[i] = pose.getJointRelativeAngularVelocity(i);
	}
}

void CharacterPose::setPose(const CharacterPose* pose) {
	setPose(*pose);
}

// --------------------------------------------------------------------------------
// setter for root state

void CharacterPose::setRootPosition(P3D& p) {
	m_rootPosition = p;
}

void CharacterPose::setRootOrientation(Quaternion& q) {
	m_rootOrientation = q;
}

void CharacterPose::setRootLinearVelocity(V3D& v) {
	m_rootLinVel = v;
}

void CharacterPose::setRootAngularVelocity(V3D& w) {
	m_rootAngVel = w;
}

// --------------------------------------------------------------------------------
// getter for root state

P3D CharacterPose::getRootPosition() const {
	return m_rootPosition;
}

Quaternion CharacterPose::getRootOrientation() const {
	return m_rootOrientation;
}

V3D CharacterPose::getRootLinearVelocity() const {
	return m_rootLinVel;
}

V3D CharacterPose::getRootAngularVelocity() const {
	return m_rootAngVel;
}

// --------------------------------------------------------------------------------
// setter for joint state

void CharacterPose::setJointRelativeOrientation(Quaternion& q, int jIndex) {
	m_relQ[jIndex] = q;
}

void CharacterPose::setJointRelativeAngularVelocity(V3D& w, int jIndex) {
	m_relW[jIndex] = w;
}

void CharacterPose::setJointsToZero() {
	for (int i = 0; i < getJointCount(); ++i) {
		setJointRelativeOrientation(Quaternion(), i);
	}
}

void CharacterPose::setVelocitiesToZero() {
	m_rootLinVel = V3D();
	m_rootAngVel = V3D();
	for (int i = 0; i < getJointCount(); ++i) {
		setJointRelativeAngularVelocity(V3D(), i);
	}
}

// --------------------------------------------------------------------------------
// getter for joint state

Quaternion CharacterPose::getJointRelativeOrientation(int jIndex) const {
	return m_relQ[jIndex];
}

V3D CharacterPose::getJointRelativeAngularVelocity(int jIndex) const {
	return m_relW[jIndex];
}

// ------------------------------------------------------------------------------------------------
// reading/writing from/to file

void CharacterPose::writeToFile(const std::string& fileName, ArticulatedCharacter* character) {
	std::ofstream outStream(fileName.c_str());

	// check if file ending is '.rs'. if so use ReducedRobotState otherwise normal write
	std::string fNameExt = fileName.substr(fileName.find_last_of('.') + 1);
	if (fNameExt.compare("rs") == 0) {
		int dim = 13 + 7 * (int)m_relW.size();
		ReducedRobotState state(dim);
		state.setPosition(m_rootPosition);
		state.setOrientation(m_rootOrientation);
		state.setVelocity(m_rootLinVel);
		state.setAngularVelocity(m_rootAngVel);

		for (int i = 0; i < (int)m_relQ.size(); ++i) {
			state.setJointRelativeOrientation(m_relQ[i], i);
			state.setJointRelativeAngVelocity(m_relW[i], i);
		}

		state.writeToFile(fileName.c_str());

	} else {

		// ------------------------------------------------------------------------------------
		// first root stuff

		outStream << "# For the root we have:" << std::endl;
		outStream << "#		Name of the root (optional)" << std::endl;
		outStream << "#		Position (world coordinates)" << std::endl;
		outStream << "#		Orientation (world coordinates)" << std::endl;
		outStream << "#		Velocity (world coordinates)" << std::endl;
		outStream << "#		Angular velocity (world coordinates)" << std::endl;

		outStream << std::setprecision(PRECISION) << m_rootPosition[0] << " " << m_rootPosition[1] << " " << m_rootPosition[2] << std::endl;
		outStream << std::setprecision(PRECISION) << m_rootOrientation[0] << " " << m_rootOrientation[1] << " " << m_rootOrientation[2] << " " << m_rootOrientation[3] << std::endl;
		outStream << std::setprecision(PRECISION) << m_rootLinVel[0] << " " << m_rootLinVel[1] << " " << m_rootLinVel[2] << std::endl;
		outStream << std::setprecision(PRECISION) << m_rootAngVel[0] << " " << m_rootAngVel[1] << " " << m_rootAngVel[2] << std::endl;
		outStream << std::endl << std::endl; // 2 empty lines

		// ------------------------------------------------------------------------------------
		// then joint stuff

		outStream << "# For each joint body we have:" << std::endl;
		outStream << "#		Name of the joint (optional)" << std::endl;
		outStream << "#		Relative orientation (parent coordinates)" << std::endl;
		outStream << "#		Relative angular velocity (parent coordinates)" << std::endl;
		outStream << std::endl; // empty line

		for (int i = 0; i < (int)m_relQ.size(); ++i) {
			Quaternion& relQ = m_relQ[i];
			V3D& relW = m_relW[i];

			if (character) {
				std::string name = character->getJoint(i)->name;
				outStream << "# " << name << std::endl;
			}

			outStream << std::setprecision(PRECISION) << relQ[0] << " " << relQ[1] << " " << relQ[2] << " " << relQ[3] << std::endl;
			outStream << std::setprecision(PRECISION) << relW[0] << " " << relW[1] << " " << relW[2] << std::endl;
			outStream << std::endl; // empty line
		}
	}
}

void CharacterPose::readFromFile(const std::string fileName) {
	// FIXME: all animations we have are from ReducedRobotState -> load it as a ReducedRobotState and then convert

	// check if file ending is '.rs'. if so use ReducedRobotState otherwise do normal read
	std::string fNameExt = fileName.substr(fileName.find_last_of('.') + 1);
	if (fNameExt.compare("rs") == 0) {

		int dim = 13 + 7 * (int)m_relW.size();
		ReducedRobotState state(dim);
		state.readFromFile(fileName.c_str());

		m_rootPosition = state.getPosition();
		m_rootOrientation = state.getOrientation();
		m_rootLinVel = state.getVelocity();
		m_rootAngVel = state.getAngularVelocity();

		for (int i = 0; i < (int)m_relQ.size(); ++i) {
			m_relQ[i] = state.getJointRelativeOrientation(i);
			m_relW[i] = state.getJointRelativeAngVelocity(i);
		}

	} else {

		std::ifstream inStream(fileName.c_str());

		// ------------------------------------------------------------------------------------
		// first root stuff

		readValidLineAsStream(inStream) >> m_rootPosition[0] >> m_rootPosition[1] >> m_rootPosition[2];
		readValidLineAsStream(inStream) >> m_rootOrientation[0] >> m_rootOrientation[1] >> m_rootOrientation[2] >> m_rootOrientation[3];
		readValidLineAsStream(inStream) >> m_rootLinVel[0] >> m_rootLinVel[1] >> m_rootLinVel[2];
		readValidLineAsStream(inStream) >> m_rootAngVel[0] >> m_rootAngVel[1] >> m_rootAngVel[2];

		// ------------------------------------------------------------------------------------
		// then joint stuff

		for (size_t i = 0; i < m_relQ.size(); ++i) {
			Quaternion& relQ = m_relQ[i];
			V3D& relW = m_relW[i];

			readValidLineAsStream(inStream) >> relQ[0] >> relQ[1] >> relQ[2] >> relQ[3];
			readValidLineAsStream(inStream) >> relW[0] >> relW[1] >> relW[2];
		}
	}
}