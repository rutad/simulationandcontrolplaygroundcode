

#include <ControlLib\RobotMesh.h>
#include <ControlLib\Robot.h>

#include <FBXReaderLib\Skeleton.hpp>



/*
struct RBSkeletonMap
{

	Skeleton*	m_pSKRef		= NULL;
	Robot*		m_pRobotRef		= NULL;


	//< rigid body world transform
	Quaternion		m_qWorld;
	V3D				m_xWorld;

	///< First record
	Quaternion		m_q_SK_to_RB_World_0;
	V3D				m_x_SK_to_RB_World_0;


};
*/

V3D RBSMesh_BranchesAndChainsMap::getRigidBodyPosition(Robot* _pRobot, Skeleton* _pSkeleton, int _id)
{
	return _pRobot->getRigidBody(_id)->state.position*(1.0 / _pSkeleton->getScaleDanger());
}

///<
V3D RBSMesh_BranchesAndChainsMap::getRobotSkeletonToRobotOffset(Robot* _pRobot, Skeleton* _pSkeleton)
{
	int rbId = searchRigidBodiesForFBXId(_pSkeleton->m_pRoot->m_id);

	return getRigidBodyPosition(_pRobot, _pSkeleton, rbId) - _pSkeleton->m_pRoot->m_offset;
}

///<
void RBSMesh_BranchesAndChainsMap::initialise()
{
	m_initialSkeletonRelativeTransforms.resize(m_pSkeleton->numJoints());
	for (uint i = 0; i < m_initialSkeletonRelativeTransforms.size(); ++i)
		m_initialSkeletonRelativeTransforms[i] = m_pSkeleton->getJoint(i)->m_q;

	///< Get the skinning from the skeleton as it was...maybe just do nothing actually..
	m_pSkeleton->setScaleForRootHeightInWorldToBe(1.0);

	m_initialSkeletonToRobotOffset = getRobotSkeletonToRobotOffset(m_pRobot, m_pSkeleton);
}

///<
SkinnedMesh::TransformsContainer RBSMesh_BranchesAndChainsMap::getTransformationsFromRobotUsingSkeleton(Robot* _pRobot, Skeleton* _pSkeleton)
{
	for (int i = 0; i < _pSkeleton->numJoints(); ++i)
	{
		Skeleton::Joint* pSkJoint = _pSkeleton->FindJoint(i);
		Quaternion q0 = m_initialSkeletonRelativeTransforms[pSkJoint->m_id];

		if (pSkJoint->m_id == 0)
		{
			///< Root:
			int rbId = searchRigidBodiesForFBXId(pSkJoint->m_id);

			pSkJoint->m_q = m_pRobot->getRigidBody(rbId)->state.orientation*q0;
			pSkJoint->m_offset = getRigidBodyPosition(_pRobot, _pSkeleton, rbId) - m_initialSkeletonToRobotOffset;
		}
		else
		{
			int rbId = searchRigidBodiesForFBXId(pSkJoint->m_id);

			if (rbId != -1)
			{
				/// This is rigid body:
				/// get the relative transform
				if (_pRobot->getBodies()[rbId]->pJoints.size() > 0)
				{
					Quaternion qRel = _pRobot->getBodies()[rbId]->pJoints[0]->computeRelativeOrientation();
					pSkJoint->m_q = qRel*q0;
				}
			}
			else
			{
				///< Relative sk can be identity or whatever was there before...

			}

		}
	}
	

	return *_pSkeleton->getCurrentGlobalTransforms();

}

