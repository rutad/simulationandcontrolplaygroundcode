
#pragma once

#include <MathLib/V3D.h>

class AnimationTimer
{
public:
	double m_time = 0.0;
	double m_totalTime = 1.0;
	double m_defaultTimeStep = 1.0 / 100;

	AnimationTimer(double _totalTime = 1.0) : m_totalTime(_totalTime) {}

	void update()
	{
		update(m_defaultTimeStep);
	}

	void updateTilFinished(const double _dt)
	{
		if (m_time < m_totalTime)
			m_time += _dt;
	}

	void update(const double _dt)
	{
		m_time += _dt;
		if (m_time > m_totalTime)
			m_time -= m_totalTime;
	}

	double getPhase()
	{
		return m_time / m_totalTime;
	}
};


/*
*
*/
struct IndexRest
{
	int			m_index;
	double		m_rest;

	IndexRest(double _val)
	{
		m_index = (int)_val;
		m_rest = _val - ((double)m_index);
	}

	IndexRest(const double _t, const int _size)
	{
		double sV = _t*(double)_size;
		m_index = MIN((int)sV, _size);
		m_rest = sV - ((double)m_index);
	}
};

IndexRest GetIndexRest(const double _t, const int _size);
