#pragma once
#include <vector>
#include <MathLib/P3D.h>
#include <MathLib/V3D.h>
#include <MathLib/Quaternion.h>

class RigidBody;
class Joint;
class CharacterState;
class CharacterPose;

/**
	This class represents an articulated character (i.e. tree structure starting at a root).
*/
class ArticulatedCharacter {
private:	
	/**
		List of all rigid bodies.
		The first rigid body in the list is the root.
		Rigid bodies higher up in the character hierarchy are also coming earlier in this list.
			index(bodyA) < index(bodyB) 
			if bodyA and bodyB are on the same chain and bodyA comes higher up in the hierarchy than bodyB
	*/
	std::vector<RigidBody*> m_rigidBodies;
	
	/**
		List of all rigid bodies that have end effectors.
	*/
	std::vector<RigidBody*> m_endEffectorBodies;

    /**
        Maps the rigid body id to the index in m_rigidBodies.
    */
    std::map<int, int> m_bodyIdToIndexMap;

	/**
		List of all joints (for easy access).
        Ordered according to hierarchy.
			index(jointA) < index(jointB) 
			if jointA and jointB are on the same chain and jointA comes higher up in the hierarchy than jointB
	*/
	std::vector<Joint*> m_joints;
	
	/**
		The characters total mass.
	*/
	double m_mass = 0.0;

	// ------------------------------------------------------------------------------------------------
	// initialization

	/**
		Traverse through the structure and retrieve the rigid bodies.
	*/
	void retrieveRigidBodies(RigidBody* root);

	/**
		Store all rigid bodies that contain an endeffector in m_endEffectorBodies.
	*/
	void retrieveEndEffectorRigidBodies();

    /**
        For each rigid body insert a map from the body id to the index in m_rigidBodies.
    */
    void setRigidBodyIndexMap();

	/**
		For each rigid body retrieve the child joint.
	*/
	void retrieveJoints();
    
    /**
        For each joint set its index such that it matches to the index in the character.
    */
    void setJointIndices();

	/**
		Compute the total mass of the character (sum of mass of rigid bodies).
	*/
	void computeTotalMass();

	// ------------------------------------------------------------------------------------------------

	/**
		Retrieves the relative angles of the joint for the given orientation.
		For each degree of freedom an angle is added to the angles list.
	*/
	void getJointAxisAngles(int jIndex, const Quaternion& qRel, std::vector<double>& angles) const;

public:
	/**
		The constructor.
	*/
	ArticulatedCharacter(RigidBody* root);
	~ArticulatedCharacter();

    // ------------------------------------------------------------------------------------------------
    // public accessible variables
    // TODO: whats 'forward' and 'right' used for?
	// Only used in GeneralizedCoordinatesCharacterRepresentation

    //keep track of the forward and right directions of the character...
    V3D			forward = V3D(1, 0, 0);
    V3D			right = V3D(0, 0, 1);

	// ------------------------------------------------------------------------------------------------
	// rigid body getter

	/**
		Get the root.
	*/
	RigidBody* getRoot() const;

	/**
		Get a rigid body by index.
	*/
	RigidBody* getRigidBody(int index) const;
	
	/**
		Get a rigid body by name.
	*/
	RigidBody* getRigidBody(std::string name) const;

	/**
		Get the list of rigid bodies.
	*/
	const std::vector<RigidBody*>& getRigidBodies() const;
	std::vector<RigidBody*>& getRigidBodies();

	/**
		Get the number of rigid bodies.
	*/
	int getRigidBodyCount() const;

	// ------------------------------------------------------------------------------------------------
	// joint getter

	/**
		Get a joint by index.
	*/
	Joint* getJoint(int index) const;

	/**
		Get a joint by name.
	*/
	Joint* getJoint(std::string name) const;

    /**
        Get a joint index by name.
    */
    int getJointIndex(std::string name) const;

	/**
		Get the list of joints.
	*/
	const std::vector<Joint*>& getJoints() const;
	std::vector<Joint*>& getJoints();

	/**
		Get the number of joints.
	*/
	int getJointCount() const;

	// ------------------------------------------------------------------------------------------------
	// compute joint related values

	/**
		Retrieves the relative angles of the joint for the requested joint for the given pose.
		For each degree of freedom an angle is added to the angles list.
	*/
	void getJointAxisAngles(CharacterPose* _pPose, int jIndex, std::vector<double>& angles) const;
	
	/**
		Computes the relative angles for each degree of freedom for each joint for the given pose.
		For each degree of freedom an angle is added to the angles list.
	*/
	void getJointAxisAngles(CharacterPose* _pPose, std::vector<double>& angles) const;
	
	/**
		Retrieves the relative angles of the joint for the requested joint for the given state.
		For each degree of freedom an angle is added to the angles list.
	*/
	void getJointAxisAngles(CharacterState* _pState, int jIndex, std::vector<double>& angles) const;
	
	/**
		Computes the relative angles for each degree of freedom for each joint for the given state.
		For each degree of freedom an angle is added to the angles list.
	*/
	void getJointAxisAngles(CharacterState* _pState, std::vector<double>& angles) const;
	
	/**
		Computes the relative angles for each degree of freedom for each joint.
		For each degree of freedom an angle is added to the angles list.
	*/
	void getJointAxisAnglesFromOrientations(const std::vector<Quaternion>& _jointOrientations, std::vector<double>& _jointAxisAngles) const;

	/**
		Computes the relative joint orientations from the given angles.
		There should be an angle for each degree of freedom.
	*/
	void getJointOrientationsFromAngles(const std::vector<double>& _jointAxisAngles, std::vector<Quaternion>& _jointOrientations);

	// ------------------------------------------------------------------------------------------------
	// end-effector stuff
	
	/**
		Get the list of rigid bodies that have end-effectors.
	*/
	std::vector<RigidBody*>& getEndEffectorRigidBodies();
	
	/**
		Get the number of rigid bodies with end-effectors.
	*/
	int getEndEffectorCount() const;
	
	/**
		Get the world position of the requested end-effector.
	*/
	P3D getEndEffectorWorldPosition(int _i) const;
	
	/**
		Get the radius of the requested end-effector.
	*/
	double getEndEffectorRadius(int _i) const;

	// ------------------------------------------------------------------------------------------------
	// other getter

	/**
		Get the total mass of the character.
	*/
	double getMass() const;

    /**
        Returns the index of the parent and child rigid body of the joint with the given index;
    */
    void getParentAndChildIndex(int jointIndex, int& parentIndex, int& childIndex) const;

	/**
		Returns the total number of degrees of freedom (just for the joints, without the root).
	*/
	int getNumDof() const;

	// ------------------------------------------------------------------------------------------------
	// compute state depending values

	/**
		This method is used to compute the center of mass of the character.
	*/
	P3D computeCOM() const;

	/**
		This method is used to compute the velocity of the center of mass of the character.
	*/
	V3D computeCOMVelocity() const;
    
    /**
        This method is used to return the current heading (rotation around vertical axis) of the character.
    */
    Quaternion getHeading() const;

	/**
		This method is used to compute relative orientation between the parent and child body of the requested joint.
	*/
	Quaternion computeJointRelativeOrientation(int jointIndex) const;

	/**
		This method is used to compute relative angular velocity between the parent and child body of the requested joint.
	*/
	V3D computeJointRelativeAngularVelocity(int jointIndex) const;

	// ------------------------------------------------------------------------------------------------
	// set/get load/save state

	// set from a rigid body state
	void setState(const CharacterState& characterState);
	void setState(const CharacterState* characterState);

	// set from a set pose (from relative angles)
	void setPose(const CharacterPose& characterPose);
	void setPose(const CharacterPose* characterPose);

	void saveRBSToFile(std::string fileName);
};