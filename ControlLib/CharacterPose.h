#pragma once
#include <MathLib/Quaternion.h>
#include <MathLib/P3D.h>
#include <MathLib/V3D.h>
#include <vector>

class ArticulatedCharacter;

/**
	This class represents a character pose through root state and joint relative orientations/angular velocities.
	Important: Since here relative orientations and angular velocities are used, which need to be computed,
		setting the character state from the pose might slightly change the actual state (get state and then set again might not result in the same state).
		Use this class only if you need to modify the state, otherwise just to retrieve the state use CharacterState.
*/
class CharacterPose {
private:
	// root stuff
	P3D m_rootPosition;
	Quaternion m_rootOrientation;
	V3D m_rootLinVel;
	V3D m_rootAngVel;

	// joint relative orientations and angular velocities
	std::vector<Quaternion> m_relQ;
	std::vector<V3D> m_relW;

	/**
		The precision for writing the state to file.
	*/
	int PRECISION = 25;

public:
	CharacterPose(const ArticulatedCharacter* _character);
	CharacterPose(const CharacterPose* _characterPose);
	~CharacterPose();

	void populatePose(const ArticulatedCharacter* _character);
	int getJointCount() const;

	/**
		Set the pose of this to the values of the given pose.
	*/
	void setPose(const CharacterPose& pose);
	void setPose(const CharacterPose* pose);

	// --------------------------------------------------------------------------------
	// setter for root state

	/**
		Set the position of the root in world coordinates.
	*/
	void setRootPosition(P3D& p);

	/**
		Set the orientation of the root in world coordinates.
	*/
	void setRootOrientation(Quaternion& q);

	/**
		Set the velocity of the root in world coordinates.
	*/
	void setRootLinearVelocity(V3D& v);

	/**
		Set the angular velocity of the root in world coordinates.
	*/
	void setRootAngularVelocity(V3D& w);

	// --------------------------------------------------------------------------------
	// getter for root state
	P3D getRootPosition() const;
	Quaternion getRootOrientation() const;
	V3D getRootLinearVelocity() const;
	V3D getRootAngularVelocity() const;

	// --------------------------------------------------------------------------------
	// setter for joint state

	/**
		Sets the joint relative orientation of the requested joint.
		Relative orientation is orientation of child with respect to parent.
	*/
	void setJointRelativeOrientation(Quaternion& q, int jIndex);

	/**
		Sets the joint relative angular velocity of the request joints.
		Relative angular velocity is angular velocity of child with respect to parent.
	*/
	void setJointRelativeAngularVelocity(V3D& w, int jIndex);

	void setJointsToZero();
	void setVelocitiesToZero();

	// --------------------------------------------------------------------------------
	// getter for joint state
	Quaternion getJointRelativeOrientation(int jIndex) const;
	V3D getJointRelativeAngularVelocity(int jIndex) const;

	// ------------------------------------------------------------------------------------------------
	// reading/writing from/to file

	/**
		Write the character pose to file.
		Also writes the names of the joints if a character is provided.
	*/
	void writeToFile(const std::string& fileName, ArticulatedCharacter* character = NULL);

	/**
		Read the character state from file
	*/
	void readFromFile(const std::string fileName);

};