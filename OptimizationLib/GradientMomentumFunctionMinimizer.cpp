#include "GradientMomentumFunctionMinimizer.h"
#include "ObjectiveFunction.h"

dVector GradientMomentumFunctionMinimizer::dpLast;

void GradientMomentumFunctionMinimizer::computeSearchDirection(ObjectiveFunction *function, const dVector& p, dVector& dp) {
	dVector grad;
	function->addGradientTo(grad, p);
	if (dpLast.size() != p.size())
		resize(dpLast, p.size());
	dp = coefficient * dpLast + grad;
	dpLast = dp;
}
