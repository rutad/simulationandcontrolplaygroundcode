#pragma once

#include <OptimizationLib/ObjectiveFunction.h>

class QuadraticFunction : public ObjectiveFunction {
public:
	QuadraticFunction() {}
	~QuadraticFunction() {}

	double alpha = 0.05;

	double computeValue(const dVector& m) {
		//Test case 1: Rosenbrock function
		double c1 = 1 - m[0];
		double c2 = m[1] - 0.5 * m[0] * m[0];
		return alpha * (c1 * c1 + 10 * c2 * c2);

		//Test case 2: narrow ellipse
//		double c1 = 2 - m[0];
//		double c2 = m[1] - m[0];
//		return alpha * (c1 * c1 + 10 * c2 * c2);
	}

	/*void addGradientTo(dVector& grad, const dVector& m) {
		resize(grad, m.size());
		dVector dm(2);
		dm[0] = m[0], dm[1] = 0.5 * m[1];
		grad += dm * 2.0 * alpha;
	}

	void addHessianEntriesTo(DynamicArray<MTriplet>& hessianEntries, const dVector& m) {
		ADD_HES_ELEMENT(hessianEntries, 0, 0, 2.0, alpha);
		ADD_HES_ELEMENT(hessianEntries, 1, 1, 1.0, alpha);
	}*/
};
