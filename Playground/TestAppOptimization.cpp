#include <GUILib/GLUtils.h>
#include "TestAppOptimization.h"
#include <GUILib/GLMesh.h>
#include <GUILib/GLContentManager.h>
#include <GUILib/GLTexture.h>
#include <GUILib/GLWindow2D.h>
#include <MathLib/MathLib.h>
#include <OptimizationLib/QuadraticFunction.h>
#include <OptimizationLib/GradientDescentFunctionMinimizer.h>
#include <OptimizationLib/NewtonFunctionMinimizer.h>
#include <OptimizationLib/GradientMomentumFunctionMinimizer.h>
#include <Utils/Logger.h>
#include <algorithm>

//TODO: implement Heavy Ball technique for GD in GD optimizer...
//TODO: make one optimization test app that visualizes on a 2d quadratic function : GD, heavy ball, Newton's method, BFGS...


//#define V2

TestAppOptimization::TestAppOptimization(): param(), solver(param) {
	setWindowTitle("Test Application for Optimization Strategies");

	showGroundPlane = false;
	showDesignEnvironmentBox = false;

	TwAddSeparator(mainMenuBar, "sep2", "");

	//add menu entries for the objects/textures that will be loaded...
	TwAddVarRW(mainMenuBar, "Draw Contours", TW_TYPE_BOOLCPP, &enableContours, " group='Control Panel' ");
	TwAddVarRW(mainMenuBar, "Contour Colors", TW_TYPE_BOOLCPP, &enableContourColors, " group='Control Panel' ");
	TwAddVarRW(mainMenuBar, "Show Gradient Descent", TW_TYPE_BOOLCPP, &enableGradient, " group='Control Panel' ");
	TwAddVarRW(mainMenuBar, "Show Newton's Method", TW_TYPE_BOOLCPP, &enableNewton, " group='Control Panel' ");
	TwAddVarRW(mainMenuBar, "Show Gradient Momentum", TW_TYPE_BOOLCPP, &enableGradientMomentum, " group='Control Panel' ");
	TwAddVarRW(mainMenuBar, "Show LBFGS", TW_TYPE_BOOLCPP, &enableLBFGS, " group='Control Panel' ");

	TwAddSeparator(mainMenuBar, "sep3", "");

	//color palette
	TwType colorType = TwDefineEnumFromString("Colors", "Red,Green,Blue,Magenta,Cyan,Yellow,Black,Grey");
	TwAddVarRW(mainMenuBar, "Gradient Descent", colorType, &colorGradient, " group='Colors' ");
	TwAddVarRW(mainMenuBar, "Newton's Method", colorType, &colorNewton, " group='Colors' ");
	TwAddVarRW(mainMenuBar, "Gradient Momentum", colorType, &colorGradientMomentum, " group='Colors' ");
	TwAddVarRW(mainMenuBar, "LBFGS", colorType, &colorLBFGS, " group='Colors' ");

	//Beichen Li: initialize the objective function
	function = new QuadraticFunction();

	//Beichen Li: camera distance
	camera->setCameraTarget(P3D(0.0, 0.0, 8.0));

	//Beichen Li: allocate cache array
	cache = new int[resolution << 1 | 1];

	//Beichen Li: initialize isolevel points
	initContours();

	//Beichen Li: initialize optimization task
	initOptimization();
}

TestAppOptimization::~TestAppOptimization(void){
	delete[] cache;
	delete function;
}


//triggered when mouse moves
bool TestAppOptimization::onMouseMoveEvent(double xPos, double yPos) {
//	if (tWidget.onMouseMoveEvent(xPos, yPos) == true) return true;
	if (showMenus)
		if (TwEventMousePosGLFW((int)xPos, (int)yPos)) return true;
	if (showConsole)
		consoleWindow->onMouseMoveEvent(xPos, yPos);
	return false;
}

//triggered when mouse buttons are pressed
bool TestAppOptimization::onMouseButtonEvent(int button, int action, int mods, double xPos, double yPos) {
	if (button == GLFW_MOUSE_BUTTON_1 && action == GLFW_PRESS) {
		glEnd();

		Ray thisRay = getRayFromScreenCoords(xPos, yPos);
		P3D point = thisRay.origin;
		V3D direction = thisRay.direction;
		point -= direction * (point.z() / direction.z());

		if (fabs(point.x()) < planeWidth && fabs(point.y()) < planeWidth) {
			resetOptimization(P3D(point.x(), point.y()));
		}

		return true;
	}

//	if (tWidget.onMouseButtonEvent(button, action, mods, xPos, yPos) == true) return true;
	if (showMenus)
		if (TwEventMouseButtonGLFW(button, action)) return true;
	if (showConsole)
		consoleWindow->onMouseButtonEvent(button, action, mods, xPos, yPos);

	return false;
}

//triggered when using the mouse wheel
bool TestAppOptimization::onMouseWheelScrollEvent(double xOffset, double yOffset) {
	if (showMenus)
		if (TwEventMouseWheelGLFW((int)yOffset))
			return true;

	return false;
}

bool TestAppOptimization::onKeyEvent(int key, int action, int mods) {
	if (action == GLFW_PRESS) {
		switch (key) {
		case GLFW_KEY_1:
			glEnd(); enableGradient = !enableGradient; break;
		case GLFW_KEY_2:
			glEnd(); enableNewton = !enableNewton; break;
		case GLFW_KEY_3:
			glEnd(); enableGradientMomentum = !enableGradientMomentum; break;
		case GLFW_KEY_4:
			glEnd(); enableLBFGS = !enableLBFGS; break;
		case GLFW_KEY_9:
			glEnd(); enableContours = !enableContours; break;
		case GLFW_KEY_0:
			glEnd(); enableContourColors = !enableContourColors; break;
		case GLFW_KEY_R:
			glEnd(); restart(); break;
		case GLFW_KEY_BACKSPACE:
			glEnd(); resume(); break;
		}
	}
	
	if (GLApplication::onKeyEvent(key, action, mods)) return true;

	return false;
}

bool TestAppOptimization::onCharacterPressedEvent(int key, int mods) {
	if (GLApplication::onCharacterPressedEvent(key, mods)) return true;

	return false;
}

void TestAppOptimization::loadFile(const char* fName) {
	Logger::consolePrint("Loading file \'%s\'...\n", fName);
	std::string fileName;
	fileName.assign(fName);
	
	std::string fNameExt = fileName.substr(fileName.find_last_of('.') + 1);

	if (fNameExt.compare("obj") == 0) {
		loadedObjs.push_back(fileName);
		selectedObjFile = loadedObjs.size() - 1;
		generateMenuEnumFromFileList("MainMenuBar/LoadedObjs", loadedObjs);
		return;
	}

	if (fNameExt.compare("bmp") == 0) {
		materialTextures.push_back(fileName);
		selectedMaterialTexture = materialTextures.size() - 1;
		generateMenuEnumFromFileList("MainMenuBar/LoadedMaterials", materialTextures);
		return;
	}

}

void TestAppOptimization::saveFile(const char* fName) {
	Logger::consolePrint("SAVE FILE: Do not know what to do with file \'%s\'\n", fName);
}


// Run the App tasks
void TestAppOptimization::process() {
	//do the work here...
	dVector p;

	iter += numIterations;

	GradientDescentFunctionMinimizer minimizerG(numIterations);
	p = point2Vector(pathGradient.back());
	minimizerG.minimize(function, p, energyGradient);
	pathGradient.push_back(P3D(p[0], p[1]));

	NewtonFunctionMinimizer minimizerN(numIterations);
	p = point2Vector(pathNewton.back());
	minimizerN.minimize(function, p, energyNewton);
	pathNewton.push_back(P3D(p[0], p[1]));

	GradientMomentumFunctionMinimizer minimizerGM(numIterations);
	p = point2Vector(pathGradientMomentum.back());
	minimizerGM.minimize(function, p, energyGradientMomentum);
	pathGradientMomentum.push_back(P3D(p[0], p[1]));

	p = point2Vector(pathLBFGS.back());
	solver.DEMOMinimize(*this, p, energyLBFGS);
	pathLBFGS.push_back(P3D(p[0], p[1]));

	logStatus();
}

void TestAppOptimization::resume() {
	P3D current;
	if (iter > 0) {
		iter--;

		pathGradient.pop_back();
		pathNewton.pop_back();
		pathGradientMomentum.pop_back();
		pathLBFGS.pop_back();

		energyGradient = function->computeValue(point2Vector(pathGradient.back()));
		energyNewton = function->computeValue(point2Vector(pathNewton.back()));
		energyGradientMomentum = function->computeValue(point2Vector(pathGradientMomentum.back()));
		energyLBFGS = function->computeValue(point2Vector(pathLBFGS.back()));

		solver.resume();

		logStatus();
	}
}

// Draw the App scene - camera transformations, lighting, shadows, reflections, etc apply to everything drawn by this method
void TestAppOptimization::drawScene() {
	double x = planeWidth;
	
	//Beichen Li: draw a white square as background
	glColor3d(1.0, 1.0, 1.0);
	glBegin(GL_POLYGON);
	glVertex3d(x, x, 0.0);
	glVertex3d(-x, x, 0.0);
	glVertex3d(-x, -x, 0.0);
	glVertex3d(x, -x, 0.0);
	glEnd();

	//Beichen Li: draw contour lines (isolevel curves)
	if (enableContours)
		drawContours();

	//Beichen Li: draw optimization paths
	drawPaths();
}

void TestAppOptimization::drawContours() {
	int end = isolevelPoints.size();
	double color, x, y;
	double dt = planeWidth / resolution;
	double red = 0.0, blue = 0.0;

	for (int i = 0; i < end; i++) {
		color = (isolevelPoints[i].colorDeg - minDeg) * 1.0 / (maxDeg - minDeg);
		x = isolevelPoints[i].ix * dt;
		y = isolevelPoints[i].iy * dt;

		//Beichen Li: Isolevel point color is set by interpolation between red and blue
		if (enableContourColors) {
			red = color < 0.5 ? 1.0 : 2 * (1 - color);
			blue = color < 0.5 ? 2 * color : 1.0;
		}

		glColor3d(red, 0, blue);
		glBegin(GL_POLYGON);
		glVertex3d(P3D(x, y, 0));
		glVertex3d(P3D(x + dt, y, 0));
		glVertex3d(P3D(x + dt, y + dt, 0));
		glVertex3d(P3D(x, y + dt, 0));
		glEnd();
	}
}

void TestAppOptimization::drawPaths() {
	//Beichen Li: Gradient descent (color = red)
	if (enableGradient) {
		switchColor(colorGradient);
		drawOptimizationPath(pathGradient);
	}

	//Beichen Li: Newton's method (color = green)
	if (enableNewton) {
		switchColor(colorNewton);
		drawOptimizationPath(pathNewton);
	}

	//Beichen Li: Gradient descent with momentum (color = blue)
	if (enableGradientMomentum) {
		switchColor(colorGradientMomentum);
		drawOptimizationPath(pathGradientMomentum);
	}

	//Beichen Li: LBFGS (color = magenta)
	if (enableLBFGS) {
		switchColor(colorLBFGS);
		drawOptimizationPath(pathLBFGS);
	}
}

void TestAppOptimization::drawOptimizationPath(DynamicArray<P3D>& path) {
	int end = path.size();
	glPointSize(10.0);
	glBegin(GL_POINTS);
	for (int i = 0; i < end; i++)
		glVertex3d(path[i]);
	glEnd();
	
	glLineWidth(4.0);
	glBegin(GL_LINE_STRIP);
	for (int i = 0; i < end; i++)
		glVertex3d(path[i]);
	glEnd();
}

// This is the wild west of drawing - things that want to ignore depth buffer, camera transformations, etc. Not pretty, quite hacky, but flexible. Individual apps should be careful with implementing this method. It always gets called right at the end of the draw function
void TestAppOptimization::drawAuxiliarySceneInfo() {

}

// Restart the application.
void TestAppOptimization::restart() {
	resetOptimization(pathGradient[0]);
}

bool TestAppOptimization::processCommandLine(const std::string& cmdLine) {

//	if (GLApplication::processCommandLine(cmdLine)) return true;

	return false;
}

void TestAppOptimization::initContours() {
	int end = resolution;
	double dt = planeWidth / resolution;
	double x, y;
	int f1, f2, f3, f4;
	int colorDeg;

	for (int ix = -end; ix < end; ix++) {
		for (int iy = -end; iy < end; iy++) {
			x = ix * dt;
			y = iy * dt;

			dVector p(2);
			p[0] = x, p[1] = y;
			f1 = iy == -end ? (ix == -end ? int(sqrt(function->computeValue(p)) / isolevelInterval) : cache[iy + end]) : f2;
			p[1] = y + dt;
			f2 = ix == -end ? int(sqrt(function->computeValue(p)) / isolevelInterval) : cache[iy + end + 1];
			p[0] = x + dt; p[1] = y;
			f3 = cache[iy + end] = iy == -end ? int(sqrt(function->computeValue(p)) / isolevelInterval) : f4;
			p[1] = y + dt;
			f4 = int(sqrt(function->computeValue(p)) / isolevelInterval);

			//Beichen Li: create a isolevel point here
			if (f1 != f2 || f2 != f3 || f3 != f4) {
				colorDeg = f1 > f2 ? f1 : f2;
				colorDeg = f3 > colorDeg ? f3 : colorDeg;
				colorDeg = f4 > colorDeg ? f4 : colorDeg;
				isolevelPoints.push_back(IsolevelPoint(ix, iy, colorDeg));
			}
		}

		cache[end << 1] = f4;
	}

	end = isolevelPoints.size();
	minDeg = 0x7fffffff;
	maxDeg = 0x80000000;
	for (int i = 0; i < end; i++) {
		maxDeg = (std::max)(isolevelPoints[i].colorDeg, maxDeg);
		minDeg = (std::min)(isolevelPoints[i].colorDeg, minDeg);
	}
}

void TestAppOptimization::initOptimization() {
	P3D start(-3.0, -4.0);

	//Setting the starting point for each optimization method
	pathGradient.push_back(start);
	pathNewton.push_back(start);
	pathGradientMomentum.push_back(start);
	pathLBFGS.push_back(start);
}

void TestAppOptimization::resetOptimization(const P3D& origin) {
	pathGradient.clear();
	pathNewton.clear();
	pathGradientMomentum.clear();
	pathLBFGS.clear();

	pathGradient.push_back(origin);
	pathNewton.push_back(origin);
	pathGradientMomentum.push_back(origin);
	pathLBFGS.push_back(origin);

	solver.k = 0;
	solver.end = 0;
	iter = 0;
}
