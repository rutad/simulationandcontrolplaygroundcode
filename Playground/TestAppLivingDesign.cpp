

#include <GUILib/GLUtils.h>
#include "TestAppLivingDesign.h"
#include <GUILib/GLMesh.h>
#include <GUILib/GLContentManager.h>
#include <GUILib/GLTrackingCamera.h>
#include <MathLib/MathLib.h>
#include <iostream>
#include <MathLib/boundingBox.h>


using namespace std;

/*
pick an RMCRobot by clicking on it, translation and rotation widgets will show on its root.
And its picked RMC will be painted in orange. (Root is painted in green)

Once you select an RMCRobot:
key 'Q': clone the RMCRobot
key 'W': save the picked RMCRobot to ../out/tmpRMCRobot.mrb
key 'F': switch the parent joint of the picked RMC to the next relative transformation.
key 'D': delete the subtree from the selected RMC


key 'S': save the whole design to ../out/tmpModularDesign.dsn
key 'R': load design from ../out/tmpModularDesign.dsn

key 'T': create search tree and set selected RMC as root
key 'Y': set search tree target RMC
key 'G': exit search tree mode
key 'B': build selected search tree path solution

*/


void TW_CALL LoadRoboToSim(void* clientData) {
	((TestAppLivingDesign*)clientData)->loadToSim();
}

void TW_CALL RestartMOPTSim(void* clientData) {
	((TestAppLivingDesign*)clientData)->warmStartMOPT();
}


//void TW_CALL setRobotScale(const void *value, void *clientData) {
//	ParameterizedRobotDesign* prd = ((TestAppLivingDesign*)clientData)->prd;
//	if (!prd) {
//		*(double*)value = 0;
//		return;
//	}
//	DynamicArray<double> params;
//	prd->getCurrentSetOfParameters(params);
//	params[0] = *(double*)value;
//	prd->setParameters(params);
//}
//
//void TW_CALL getRobotScale(void *value, void *clientData) {
//	ParameterizedRobotDesign* prd = ((TestAppLivingDesign*)clientData)->prd;
//	if (!prd) {
//		*(double*)value = 0;
//		return;
//	}
//	DynamicArray<double> params;
//	prd->getCurrentSetOfParameters(params);
//	*(double*)value = params[0];
//}

TestAppLivingDesign::TestAppLivingDesign() {
	TwDefine(" MainMenuBar alpha=255 ");   // semi-transparent blue bar
	bgColor[0] = bgColor[1] = bgColor[2] = 0.75;

	setWindowTitle("Living Design Application...");

	camera->onMouseWheelScrollEvent(0, 10.0);

	tWidget = new TranslateWidget();
	rWidget = new RotateWidgetV2();
	tWidget->visible = false;
	rWidget->visible = false;

	showGroundPlane = false;

	TwAddSeparator(mainMenuBar, "sep2", "");

	int size[2];
	int w, h;
	TwGetParam(mainMenuBar, NULL, "size", TW_PARAM_INT32, 2, size);
	w = (GLApplication::getMainWindowWidth() - size[0]) / 2;
	h = GLApplication::getMainWindowHeight();
	designWindow = new ModularDesignWindow(size[0], 0, w, h, this, "../data/modularRobot/configXM-430-V3.cfg");

	setViewportParameters(size[0] + w, 0, w, h);
	consoleWindow->setViewportParameters(size[0] + w, 0, w, 280);

	waitForFrameRate = true;

	TwAddSeparator(mainMenuBar, "sep2", "");

	drawCDPs = false;
	drawSkeletonView = false;
	showGroundPlane = false;

	TwAddButton(mainMenuBar, "LoadRobotToSim", LoadRoboToSim, this, " label='Load To Simulation' group='Sim' key='l' ");
	TwAddButton(mainMenuBar, "WarmstartMOPT", RestartMOPTSim, this, " label='WarmStart MOPT' group='Sim' key='w' ");

	TwAddVarRW(mainMenuBar, "RunOptions", TwDefineEnumFromString("RunOptions", "..."), &runOption, "group='Sim'");
	DynamicArray<std::string> runOptionList;
	runOptionList.push_back("\\Motion Optimization");
	runOptionList.push_back("\\Motion Plan Animation");
	runOptionList.push_back("\\Simulation");
	runOptionList.push_back("\\Simulation-SmoothControl");
	generateMenuEnumFromFileList("MainMenuBar/RunOptions", runOptionList);

	TwAddVarRW(mainMenuBar, "drawMOPTWindow", TW_TYPE_BOOLCPP, &drawMOPTWindow, " label='drawMOPTWindow' group='Viz2'");
	TwAddVarRW(mainMenuBar, "drawMotionPlan", TW_TYPE_BOOLCPP, &drawMotionPlan, " label='drawMotionPlan' group='Viz2'");
	TwAddVarRW(mainMenuBar, "DrawMeshes", TW_TYPE_BOOLCPP, &drawMeshes, " label='drawMeshes' group='Viz2'");
	TwAddVarRW(mainMenuBar, "DrawMOIs", TW_TYPE_BOOLCPP, &drawMOIs, " label='drawMOIs' group='Viz2'");
	TwAddVarRW(mainMenuBar, "DrawCDPs", TW_TYPE_BOOLCPP, &drawCDPs, " label='drawCDPs' group='Viz2'");
	TwAddVarRW(mainMenuBar, "DrawSkeletonView", TW_TYPE_BOOLCPP, &drawSkeletonView, " label='drawSkeleton' group='Viz2'");
	TwAddVarRW(mainMenuBar, "DrawJoints", TW_TYPE_BOOLCPP, &drawJoints, " label='drawJoints' group='Viz2'");
	TwAddVarRW(mainMenuBar, "drawContactForces", TW_TYPE_BOOLCPP, &drawContactForces, " label='drawContactForces' group='Viz2'");
	TwAddVarRW(mainMenuBar, "drawOrientation", TW_TYPE_BOOLCPP, &drawOrientation, " label='drawOrientation' group='Viz2'");

	TwAddSeparator(mainMenuBar, "sep3", "");

	TwAddVarRW(mainMenuBar, "groundKPScale", TW_TYPE_DOUBLE, &groundKPScale, "min=-5 max=10 step=1");
	TwAddVarRW(mainMenuBar, "groundKdVal", TW_TYPE_DOUBLE, &groundKdVal, "min=0 max=10000 step=100");

	TwAddSeparator(mainMenuBar, "sep4", "");

	TwAddVarRW(mainMenuBar, "PlanMotionPhase", TW_TYPE_DOUBLE, &motionPlanTime, "min=0 max=1 step=0.01 group='MotionOptimizationOptions'");

	moptWindow = new MOPTWindow(size[0], 0, w, h, this);

	simTimeStep = 1 / 500.0;
	showGroundPlane = true;
}

void TestAppLivingDesign::adjustWindowSize(int width, int height) {
	Logger::consolePrint("resize");

	mainWindowWidth = width;
	mainWindowHeight = height;
	// Send the window size to AntTweakBar
	TwWindowSize(width, height);

	setViewportParameters(0, 0, mainWindowWidth, mainWindowHeight);

	int size[2];
	int w, h;
	TwGetParam(mainMenuBar, NULL, "size", TW_PARAM_INT32, 2, size);
	w = (GLApplication::getMainWindowWidth() - size[0]) / 2;
	h = GLApplication::getMainWindowHeight();
	designWindow->setViewportParameters(size[0], 0, w, h);
	consoleWindow->setViewportParameters(size[0] + w, 0, w, 280);
	setViewportParameters(size[0] + w, 0, w, h);
}

TestAppLivingDesign::~TestAppLivingDesign(void){

	delete designWindow;
	delete moptWindow;
	
	// Widgets
	delete rWidget;
	delete tWidget;
}

//triggered when mouse moves
bool TestAppLivingDesign::onMouseMoveEvent(double xPos, double yPos) {
	if (showMenus)
		if (TwEventMousePosGLFW((int)xPos, (int)yPos)) return true;

	if (designWindow->isActive() || designWindow->mouseIsWithinWindow(xPos, yPos))
		if (designWindow->onMouseMoveEvent(xPos, yPos)) return true;

	if (!drawMOPTWindow)
		if (designWindow->isActive() || designWindow->mouseIsWithinWindow(xPos, yPos))
			if (designWindow->onMouseMoveEvent(xPos, yPos)) return true;

	if (drawMOPTWindow)
		if (moptWindow->isActive() || moptWindow->mouseIsWithinWindow(xPos, yPos))
			if (moptWindow->onMouseMoveEvent(xPos, yPos)) return true;

	if (GLApplication::onMouseMoveEvent(xPos, yPos) == true) return true;

	return false;
}


//triggered when mouse buttons are pressed
bool TestAppLivingDesign::onMouseButtonEvent(int button, int action, int mods, double xPos, double yPos) {
	
	if(designWindow->isActive() || designWindow->mouseIsWithinWindow(xPos, yPos))
		if (designWindow->onMouseButtonEvent(button, action, mods, xPos, yPos)) return true;

	
	if (!drawMOPTWindow)
		if (designWindow->isActive() || designWindow->mouseIsWithinWindow(xPos, yPos))
			if (designWindow->onMouseButtonEvent(button, action, mods, xPos, yPos)) return true;

	if (drawMOPTWindow)
		if (moptWindow->isActive() || moptWindow->mouseIsWithinWindow(xPos, yPos))
			if (moptWindow->onMouseButtonEvent(button, action, mods, xPos, yPos)) return true;

	if (GLApplication::onMouseButtonEvent(button, action, mods, xPos, yPos)) return true;
	return false;
}

//triggered when using the mouse wheel
bool TestAppLivingDesign::onMouseWheelScrollEvent(double xOffset, double yOffset) {
	if(designWindow->isActive())
		if (designWindow->onMouseWheelScrollEvent(xOffset, yOffset)) return true;

	if (!drawMOPTWindow)
		if (designWindow->mouseIsWithinWindow(GlobalMouseState::lastMouseX, GlobalMouseState::lastMouseY))
			if (designWindow->onMouseWheelScrollEvent(xOffset, yOffset)) return true;

	if (drawMOPTWindow)
		if (moptWindow->mouseIsWithinWindow(GlobalMouseState::lastMouseX, GlobalMouseState::lastMouseY))
			if (moptWindow->onMouseWheelScrollEvent(xOffset, yOffset)) return true;

	if (GLApplication::onMouseWheelScrollEvent(xOffset, yOffset)) return true;

	return false;
}

bool TestAppLivingDesign::onKeyEvent(int key, int action, int mods) {
	if (locomotionManager && locomotionManager->motionPlan) {
		if (key == GLFW_KEY_UP && action == GLFW_PRESS)
			locomotionManager->motionPlan->desDistanceToTravel.z() += 0.1;
		if (key == GLFW_KEY_DOWN && action == GLFW_PRESS)
			locomotionManager->motionPlan->desDistanceToTravel.z() -= 0.1;
		if (key == GLFW_KEY_LEFT && action == GLFW_PRESS)
			locomotionManager->motionPlan->desDistanceToTravel.x() += 0.1;
		if (key == GLFW_KEY_RIGHT && action == GLFW_PRESS)
			locomotionManager->motionPlan->desDistanceToTravel.x() -= 0.1;
		if (key == GLFW_KEY_PERIOD && action == GLFW_PRESS)
			locomotionManager->motionPlan->desTurningAngle += 0.1;
		if (key == GLFW_KEY_SLASH && action == GLFW_PRESS)
			locomotionManager->motionPlan->desTurningAngle -= 0.1;

		boundToRange(&locomotionManager->motionPlan->desDistanceToTravel.z(), -0.5, 0.5);
		boundToRange(&locomotionManager->motionPlan->desDistanceToTravel.x(), -0.5, 0.5);
		boundToRange(&locomotionManager->motionPlan->desTurningAngle, -0.5, 0.5);
	}


	if (key == GLFW_KEY_O && action == GLFW_PRESS)
		locomotionManager->motionPlan->writeRobotMotionAnglesToFile("../out/tmpMPAngles.mpa");
	if (key == GLFW_KEY_P && action == GLFW_PRESS)
		locomotionManager->motionPlan->writeRobotMotionAnglesToFile("../out/angle.p");
	if (key == GLFW_KEY_1 && action == GLFW_PRESS)
		runOption = MOTION_PLAN_OPTIMIZATION;
	if (key == GLFW_KEY_2 && action == GLFW_PRESS)
		runOption = MOTION_PLAN_ANIMATION;
	if (key == GLFW_KEY_3 && action == GLFW_PRESS)
		runOption = PHYSICS_SIMULATION;
	if (key == GLFW_KEY_4 && action == GLFW_PRESS)
		runOption = PHYSICS_SIMULATION_SMOOTH;

	if (key == GLFW_KEY_G && action == GLFW_PRESS)
	{
		if (designEngine)
		{
			static int dIndex = 0;
			if (dIndex >= (int)designEngine->designDB.size()) dIndex = 0;
			designEngine->visualizeDesign(dIndex);
			dIndex++;
		}
		return true;
	}

	if (key == GLFW_KEY_R && action == GLFW_PRESS)
	{
		loadDesignFile("../out/tmpModularRobotDesign.dsn");
		return true;
	}

	//if (!drawMOPTWindow && designWindow->onKeyEvent(key, action, mods)) return true;

	if (designWindow->isActive())
		if (designWindow->onKeyEvent(key, action, mods)) return true;

	if (GLApplication::onKeyEvent(key, action, mods)) return true;
	return false;
}

bool TestAppLivingDesign::onCharacterPressedEvent(int key, int mods) {
	if (GLApplication::onCharacterPressedEvent(key, mods)) return true;

	return false;
}


void TestAppLivingDesign::loadFile(const char* fName) {
	std::string fileName;
	fileName.assign(fName);

	std::string fNameExt = fileName.substr(fileName.find_last_of('.') + 1);

	if (fNameExt.compare("rs") == 0) {
		Logger::consolePrint("Load robot state from '%s'\n", fName);
		designWindow->setStartStateFName(fName);
		return;
	}

	if (designWindow->type == ROBOT_DESIGN && fNameExt.compare("rbs") == 0) {
		delete rbEngine;
		rbEngine = new ODERBEngine();
		designWindow->loadFile(fName);
		designWindow->saveFile("../out/tmpRobotDesign.rbs");

		//loadToSim();

		return;
	}


	if (fNameExt == "dsn")
	{
		loadDesignFile(fName);
	}

	if (fNameExt.compare("ffp") == 0) {
		moptWindow->footFallPattern.loadFromFile(fName);
		moptWindow->footFallPattern.writeToFile("..\\out\\tmpFFP.ffp");
		return;
	}

	if (fNameExt.compare("p") == 0) {
		if (locomotionManager && locomotionManager->motionPlan)
			locomotionManager->motionPlan->readParamsFromFile(fName);
		return;
	}

}

void TestAppLivingDesign::saveFile(const char* fName) {

	Logger::consolePrint("SAVE FILE: Do not know what to do with file \'%s\'\n", fName);
}

void TestAppLivingDesign::loadToSim() {
	delete worldOracle;
	delete robot;
	delete rbEngine;
	delete prd;

	worldOracle = new WorldOracle(Globals::worldUp, Globals::groundPlane);
	rbEngine = new ODERBEngine();

	/* ------ load the robot and set up an initial motion plan for it ------*/
	designWindow->saveFile("../out/tmpRobot.rbs");
	rbEngine->loadRBsFromFile("../out/tmpRobot.rbs");

	worldOracle->writeRBSFile("../out/tmpEnvironment.rbs");
	rbEngine->loadRBsFromFile("../out/tmpEnvironment.rbs");

	robot = new Robot(rbEngine->rbs[0]);
	prd = new ParameterizedRobotDesign(robot);

	ReducedRobotState startingRobotState = designWindow->getStartState(robot);
	startingRobotState.writeToFile("../out/tmpRobot.rs");
	designWindow->prepareForSimulation(rbEngine);
	robot->setState(&startingRobotState);

	/* ---------- Set up the legs and the foot fall pattern ---------- */

	setupSimpleRobotStructure(robot);

	moptWindow->loadRobot(robot, &startingRobotState);


	int nLegs = robot->bFrame->limbs.size();
	int nPoints = 3 * nLegs;

	//now, start MOPT...
	warmStartMOPT();

	Logger::consolePrint("Warmstart successful...\n");
	Logger::consolePrint("The robot has %d legs, weighs %lfkgs and is %lfm tall...\n", robot->bFrame->limbs.size(), robot->getMass(), robot->root->getCMPosition().y());
}

void TestAppLivingDesign::warmStartMOPT() {

	delete controller;
	delete controllerIK;
	delete locomotionManager;

	//plotWindow->clearData();
	//plotWindow->createData("optValues", 1, 0, 0);

	locomotionManager = moptWindow->initializeNewMP();

	controller = new PositionBasedRobotController(robot, locomotionManager->motionPlan);
	controllerIK = new PositionBasedRobotController(robot, locomotionManager->motionPlan);
	animationCycle = 0;

}

void TestAppLivingDesign::runMOPTStep() {
	double energyVal = moptWindow->runMOPTStep();

	//plotWindow->addDataPoint(0, energyVal);
}


// Run the App tasks
void TestAppLivingDesign::process() {
	//do the work here...
	if (designWindow->process()) return;

	//we need to sync the state of the robot with the motion plan when we first start physics-based tracking...
	static int lastRunOptionSelected = runOption + 1;
	if (lastRunOptionSelected != runOption && (runOption == PHYSICS_SIMULATION || runOption == PHYSICS_SIMULATION_SMOOTH)) {
		// TODO: should also set velocities based on the motion plan...
		Logger::consolePrint("Syncronizing robot state\n");
		ReducedRobotState moptRobotState(robot);
		moptRobotState.clearVelocities();
		locomotionManager->motionPlan->robotStateTrajectory.getRobotPoseAt(motionPlanTime, moptRobotState);

		//		moptRobotState.setPosition(moptRobotState.getPosition() + V3D(0,1,0) * 0.01);
		//		moptRobotState.setOrientation(Quaternion(1,0,0,0.1).toUnit());
		//		moptRobotState.setAngularVelocity(V3D(0.5,.5,.5));

		robot->setState(&moptRobotState);
		controller->stridePhase = controllerIK->stridePhase = motionPlanTime;
		//		lastRunOptionSelected = runOption;
		//		return;
	}
	lastRunOptionSelected = runOption;


	if (runOption == PHYSICS_SIMULATION || runOption == PHYSICS_SIMULATION_SMOOTH) {
		double simulationTime = 0;
		double maxRunningTime = 1.0 / desiredFrameRate;

		while (simulationTime / maxRunningTime < animationSpeedupFactor) {
			simulationTime += simTimeStep;

			controller->advanceInTime(simTimeStep);
			controllerIK->advanceInTime(simTimeStep);
			if (runOption == PHYSICS_SIMULATION)
				controller->computeControlSignals(simTimeStep);
			else
				controllerIK->computeControlSignals(simTimeStep);

			//set up simulation parameters...
			ODERBEngine* odeRBEngine = dynamic_cast<ODERBEngine*>(rbEngine);
			if (odeRBEngine) {
				double kp = pow(10, groundKPScale);
				double kd = groundKdVal;
				odeRBEngine->contactStiffnessCoefficient = simTimeStep * kp / (simTimeStep * kp + kd);
				odeRBEngine->contactDampingCoefficient = 1 / (simTimeStep * kp + kd);
			}

			rbEngine->step(simTimeStep);
			//			break;
		}
	}
	else if (runOption == MOTION_PLAN_OPTIMIZATION) {
		animationCycle = 0;

		runMOPTStep();
	}
	else if (runOption == MOTION_PLAN_ANIMATION) {
		motionPlanTime += (1.0 / desiredFrameRate) / locomotionManager->motionPlan->motionPlanDuration;
		if (motionPlanTime > 1) {
			animationCycle++;
			motionPlanTime -= 1;
		}
		moptWindow->setAnimationParams(motionPlanTime, 0);
	}
	
}

void TestAppLivingDesign::setupLights() {

	GLfloat bright[] = { 0.8f, 0.8f, 0.8f, 1.0f };
	GLfloat mediumbright[] = { 0.3f, 0.3f, 0.3f, 1.0f };

	glLightfv(GL_LIGHT1, GL_DIFFUSE, bright);
	glLightfv(GL_LIGHT2, GL_DIFFUSE, mediumbright);
	glLightfv(GL_LIGHT3, GL_DIFFUSE, mediumbright);
	glLightfv(GL_LIGHT4, GL_DIFFUSE, mediumbright);


	GLfloat light0_position[] = { 0.0f, 10000.0f, 10000.0f, 0.0f };
	GLfloat light0_direction[] = { 0.0f, -10000.0f, -10000.0f, 0.0f };

	GLfloat light1_position[] = { 0.0f, 10000.0f, -10000.0f, 0.0f };
	GLfloat light1_direction[] = { 0.0f, -10000.0f, 10000.0f, 0.0f };

	GLfloat light2_position[] = { 0.0f, -10000.0f, 0.0f, 0.0f };
	GLfloat light2_direction[] = { 0.0f, 10000.0f, -0.0f, 0.0f };

	GLfloat light3_position[] = { 10000.0f, -10000.0f, 0.0f, 0.0f };
	GLfloat light3_direction[] = { -10000.0f, 10000.0f, -0.0f, 0.0f };

	GLfloat light4_position[] = { -10000.0f, -10000.0f, 0.0f, 0.0f };
	GLfloat light4_direction[] = { 10000.0f, 10000.0f, -0.0f, 0.0f };


	glLightfv(GL_LIGHT0, GL_POSITION, light0_position);
	glLightfv(GL_LIGHT1, GL_POSITION, light1_position);
	glLightfv(GL_LIGHT2, GL_POSITION, light2_position);
	glLightfv(GL_LIGHT3, GL_POSITION, light3_position);
	glLightfv(GL_LIGHT4, GL_POSITION, light4_position);


	glLightfv(GL_LIGHT0, GL_SPOT_DIRECTION, light0_direction);
	glLightfv(GL_LIGHT1, GL_SPOT_DIRECTION, light1_direction);
	glLightfv(GL_LIGHT2, GL_SPOT_DIRECTION, light2_direction);
	glLightfv(GL_LIGHT3, GL_SPOT_DIRECTION, light3_direction);
	glLightfv(GL_LIGHT4, GL_SPOT_DIRECTION, light4_direction);


	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHT1);
	glEnable(GL_LIGHT2);
	glEnable(GL_LIGHT3);
	glEnable(GL_LIGHT4);
}


// Draw the App scene - camera transformations, lighting, shadows, reflections, etc apply to everything drawn by this method
void TestAppLivingDesign::drawScene() {
	
	glDisable(GL_LIGHTING);

	//in case the user is manipulating any of parameters of the motion plan, update them.

	if (locomotionManager)
		locomotionManager->drawMotionPlan(motionPlanTime, animationCycle, (runOption == MOTION_PLAN_ANIMATION || runOption == MOTION_PLAN_OPTIMIZATION) && drawMeshes, false, drawMotionPlan, drawContactForces, drawOrientation);

	if (runOption == PHYSICS_SIMULATION || runOption == PHYSICS_SIMULATION_SMOOTH) {
		int flags = 0;
		if (drawMeshes) flags |= SHOW_MESH | SHOW_MATERIALS;
		if (drawSkeletonView) flags |= SHOW_BODY_FRAME | SHOW_ABSTRACT_VIEW;
		if (drawMOIs) flags |= SHOW_MOI_BOX;
		if (drawCDPs) flags |= SHOW_CD_PRIMITIVES;
		if (drawJoints) flags |= SHOW_JOINTS;

		glEnable(GL_LIGHTING);
		rbEngine->drawRBs(flags);
		glDisable(GL_LIGHTING);
	}

	if (runOption == PHYSICS_SIMULATION)
		controller->drawDebugInfo();

	if (runOption == PHYSICS_SIMULATION_SMOOTH)
		controllerIK->drawDebugInfo();
	
}

// This is the wild west of drawing - things that want to ignore depth buffer, camera transformations, etc. Not pretty, quite hacky, but flexible. Individual apps should be careful with implementing this method. It always gets called right at the end of the draw function
void TestAppLivingDesign::drawAuxiliarySceneInfo() {
	
	//clear the depth buffer so that the widgets show up on top of the object primitives
	glClear(GL_DEPTH_BUFFER_BIT);

	// draw the widgets
	tWidget->draw();
	rWidget->draw();

	glClear(GL_DEPTH_BUFFER_BIT);
	
	//designWindow->draw();
	//designWindow->drawAuxiliarySceneInfo();

	if (drawMOPTWindow && moptWindow) {
		moptWindow->draw();
		moptWindow->drawAuxiliarySceneInfo();
	}


	if (!drawMOPTWindow)
	{
		designWindow->draw();
		designWindow->drawAuxiliarySceneInfo();
	}
}

// Restart the application.
void TestAppLivingDesign::restart() {

}

bool TestAppLivingDesign::processCommandLine(const std::string& cmdLine) {

	int index = cmdLine.find_first_of(' ');
	string cmd = cmdLine.substr(0, index);

	if (GLApplication::processCommandLine(cmdLine)) return true;

	return false;
}

void TestAppLivingDesign::loadDesignFile(const char* fName)
{
	designWindow->loadFile(fName);

	delete designEngine;
	designEngine = new LivingDesignEngine(designWindow);
	designEngine->generateDesignDB();
}