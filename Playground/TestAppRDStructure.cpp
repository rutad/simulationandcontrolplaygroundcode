
#include "TestAppRDStructure.h"
#include <GUILib/GLMesh.h>
#include <GUILib/GLContentManager.h>
#include <MathLib/MathLib.h>



TestAppRDStructure::TestAppRDStructure() {
	setWindowTitle("Test Application for Robot Designer -- structure");

	showGroundPlane = false;

	TwAddSeparator(mainMenuBar, "sep2", "");

	TwAddVarRW(mainMenuBar, "DrawAttachmentPoints", TW_TYPE_BOOLCPP, &BodyPartStructure::showAttachmentPoints, " label='DrawAttachmentPoints' group='Viz2'");
	TwAddVarRW(mainMenuBar, "DrawConvexHull", TW_TYPE_BOOLCPP, &BodyPartStructure::showConvexHull, " label='DrawConvexHull' group='Viz2'");
	TwAddVarRW(mainMenuBar, "DrawWireFrameConvexHull", TW_TYPE_BOOLCPP, &BodyPartStructure::showWireFrameConvexHull, " label='DrawWireFrameConvexHull' group='Viz2'");

	TwAddVarRW(mainMenuBar, "motor2PosX", TW_TYPE_DOUBLE, &motor2PosX, " label='motor2PosX' group='Viz3' min=-0.3 max=0.3 step=0.01");
	TwAddVarRW(mainMenuBar, "motor2PosY", TW_TYPE_DOUBLE, &motor2PosY, " label='motor2PosY' group='Viz3' min=-0.3 max=0.3 step=0.01");
	TwAddVarRW(mainMenuBar, "motor2PosZ", TW_TYPE_DOUBLE, &motor2PosZ, " label='motor2PosZ' group='Viz3' min=-0.3 max=0.3 step=0.01");

	TwAddVarRW(mainMenuBar, "motor2AngleX", TW_TYPE_DOUBLE, &motor2AngleX, " label='motor2AngleX' group='Viz3'");
	TwAddVarRW(mainMenuBar, "motor2AngleY", TW_TYPE_DOUBLE, &motor2AngleY, " label='motor2AngleY' group='Viz3'");
	TwAddVarRW(mainMenuBar, "motor2EmbeddingAngle", TW_TYPE_DOUBLE, &motor2EmbeddingAngle, " label='motor2EmbeddingAngle' group='Viz3'");
	TwAddVarRW(mainMenuBar, "motor2ChildEmbeddingAngle", TW_TYPE_DOUBLE, &motor2ChildEmbeddingAngle, " label='motor2ChildEmbeddingAngle' group='Viz3'");
}

TestAppRDStructure::~TestAppRDStructure(void){
}

void TestAppRDStructure::updateMotorAndStructureConfiguration() {
	motor1.setPosition(P3D(motor1PosX, motor1PosY, motor1PosZ));
    motor1.orientation = getRotationQuaternion(RAD(motor1AngleX), V3D(1, 0, 0)) * getRotationQuaternion(RAD(motor1AngleY), V3D(0, 1, 0)) * getRotationQuaternion(RAD(0), V3D(0, 0, 1));

	motor1.parentEmbeddingAngle = motor1EmbeddingAngle;
	motor1.childEmbeddingAngle = motor1ChildEmbeddingAngle;

	//the position of motor2 is represented in the coordinate frame of motor 1
	motor2.setPosition(motor1.getPosition() + motor1.orientation * getRotationQuaternion(RAD(motor1.motorAngle), MOTOR_ROT_AXIS_LOCAL) * V3D(motor2PosX, motor2PosY, motor2PosZ));
	motor2.orientation = motor1.orientation * getRotationQuaternion(RAD(motor1.motorAngle), MOTOR_ROT_AXIS_LOCAL) * getRotationQuaternion(RAD(motor2AngleX), V3D(1, 0, 0)) * getRotationQuaternion(RAD(motor2AngleY), V3D(0, 1, 0)) * getRotationQuaternion(RAD(0), V3D(0, 0, 1));

	motor2.parentEmbeddingAngle = motor2EmbeddingAngle;
	motor2.childEmbeddingAngle = motor2ChildEmbeddingAngle;

	endEffector.setPosition(motor2.getPosition() + motor2.orientation * getRotationQuaternion(RAD(motor2.motorAngle), MOTOR_ROT_AXIS_LOCAL) * V3D(eePosX, eePosY, eePosZ));
	endEffector.orientation = motor2.orientation * getRotationQuaternion(RAD(motor2.motorAngle), MOTOR_ROT_AXIS_LOCAL);
}

//triggered when mouse moves
bool TestAppRDStructure::onMouseMoveEvent(double xPos, double yPos) {
	if (tWidget.onMouseMoveEvent(xPos, yPos) == true) return true;
	if (GLApplication::onMouseMoveEvent(xPos, yPos) == true) return true;

	return false;
}

//triggered when mouse buttons are pressed
bool TestAppRDStructure::onMouseButtonEvent(int button, int action, int mods, double xPos, double yPos) {
	if (tWidget.onMouseButtonEvent(button, action, mods, xPos, yPos) == true) return true;

	if (GLApplication::onMouseButtonEvent(button, action, mods, xPos, yPos)) return true;

//	if (mods & GLFW_MOD_SHIFT) {
//		return true;
//	}

	return false;
}

//triggered when using the mouse wheel
bool TestAppRDStructure::onMouseWheelScrollEvent(double xOffset, double yOffset) {
	if (GLApplication::onMouseWheelScrollEvent(xOffset, yOffset)) return true;

	return false;
}

bool TestAppRDStructure::onKeyEvent(int key, int action, int mods) {
	if (GLApplication::onKeyEvent(key, action, mods)) return true;

	return false;
}

bool TestAppRDStructure::onCharacterPressedEvent(int key, int mods) {
	if (GLApplication::onCharacterPressedEvent(key, mods)) return true;

	return false;
}

void TestAppRDStructure::loadFile(const char* fName) {
	Logger::consolePrint("Loading file \'%s\'...\n", fName);
	std::string fileName;
	fileName.assign(fName);

	std::string fNameExt = fileName.substr(fileName.find_last_of('.') + 1);

}

void TestAppRDStructure::saveFile(const char* fName) {
	Logger::consolePrint("SAVE FILE: Do not know what to do with file \'%s\'\n", fName);
}

// Run the App tasks
void TestAppRDStructure::process() {
	motor1.advanceMotorAngle();
	motor2.advanceMotorAngle();
}

// Draw the App scene - camera transformations, lighting, shadows, reflections, etc apply to everything drawn by this method
void TestAppRDStructure::drawScene() {

	glPushMatrix();
	glTranslated(0, 1, 0);

	updateMotorAndStructureConfiguration();

    motor1.draw();
    motor2.draw();
	
	endEffector.draw();

	s1.draw();
	s2.draw();
	glPopMatrix();
	tWidget.draw();
}

// This is the wild west of drawing - things that want to ignore depth buffer, camera transformations, etc. Not pretty, quite hacky, but flexible. Individual apps should be careful with implementing this method. It always gets called right at the end of the draw function
void TestAppRDStructure::drawAuxiliarySceneInfo() {

}

// Restart the application.
void TestAppRDStructure::restart() {

}

bool TestAppRDStructure::processCommandLine(const std::string& cmdLine) {

	if (GLApplication::processCommandLine(cmdLine)) return true;

	return false;
}

