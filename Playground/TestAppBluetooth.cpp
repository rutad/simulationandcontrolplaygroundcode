#include <GUILib/GLUtils.h>
#include "TestAppBluetooth.h"
#include <GUILib/GLMesh.h>
#include <GUILib/GLContentManager.h>
#include <MathLib/MathLib.h>

void TW_CALL ConnectToBluetooth(void* clientData) {
	((TestAppBluetooth*)clientData)->connectPort();
}

void TW_CALL DisconnectBluetooth(void* clientData) {
	((TestAppBluetooth*)clientData)->closePort();

}

TestAppBluetooth::TestAppBluetooth() {
	setWindowTitle("Bluetooth test");

	TwAddSeparator(mainMenuBar, "sep2", "");

	//TwAddVarRW(mainMenuBar, "Bluetooth Port", TW_TYPE_STDSTRING, &BluetoothCOMPortName, "");
	TwAddButton(mainMenuBar, "ConnectToBluetooth", ConnectToBluetooth, this, " label='connectToBluetooth' key='x' ");
	TwAddButton(mainMenuBar, "DisconnectBluetooth", DisconnectBluetooth, this, " label='disconnectBluetooth'");

	int size[2];
	int w, h;
	TwGetParam(mainMenuBar, NULL, "size", TW_PARAM_INT32, 2, size);
	w = (GLApplication::getMainWindowWidth() - size[0]);
	h = GLApplication::getMainWindowHeight();
	setViewportParameters(size[0], 0, w, h);
	consoleWindow->setViewportParameters(size[0], 0, w, 280);

	plotWindow = new PlotWindow(260 * 1.14, mainWindowHeight - 400, 500, 400);
}

TestAppBluetooth::~TestAppBluetooth(void) {

}


int TestAppBluetooth::connectPort() {

	LONG    lLastError = ERROR_SUCCESS;

	// Attempt to open the serial port (COM1)
	lLastError = serial.Open(_T("COM8"), 0, 0, false);
	if (lLastError != ERROR_SUCCESS)
		return showError(serial.GetLastError(), _T("Unable to open COM-port"));

	// Setup the serial port (9600,8N1, which is the default setting)
	lLastError = serial.Setup(CSerial::EBaud9600, CSerial::EData8, CSerial::EParNone, CSerial::EStop1);
	if (lLastError != ERROR_SUCCESS)
		return showError(serial.GetLastError(), _T("Unable to set COM-port setting"));

	// Register only for the receive event
	lLastError = serial.SetMask(CSerial::EEventBreak |
		CSerial::EEventCTS |
		CSerial::EEventDSR |
		CSerial::EEventError |
		CSerial::EEventRing |
		CSerial::EEventRLSD |
		CSerial::EEventRecv);
	if (lLastError != ERROR_SUCCESS)
		return showError(serial.GetLastError(), _T("Unable to set COM-port event mask"));

	// Use 'non-blocking' reads, because we don't know how many bytes
	// will be received. This is normally the most convenient mode
	// (and also the default mode for reading data).
	lLastError = serial.SetupReadTimeouts(CSerial::EReadTimeoutNonblocking);
	if (lLastError != ERROR_SUCCESS)
		return showError(serial.GetLastError(), _T("Unable to set COM-port read timeout."));

	Logger::consolePrint("Port open!\n");
	return 0;

}

void TestAppBluetooth::closePort() {
	// Close the port again
	serial.Close();
	Logger::consolePrint("\nClosed port\n");
}


bool TestAppBluetooth::onKeyEvent(int key, int action, int mods) {
	if (key == GLFW_KEY_ESCAPE) {
		closePort();
		//let the main app handle the rest of the shutting down process...
	}


	if (GLApplication::onKeyEvent(key, action, mods)) return true;

	return false;
}


// Run the App tasks
void TestAppBluetooth::process() {
	LONG    lLastError = ERROR_SUCCESS;

	int errorCode = 0;

	// Keep reading data, until an EOF (CTRL-Z) has been received
	// Wait for an event
	lLastError = serial.WaitEvent();
	if (lLastError != ERROR_SUCCESS) {
		errorCode = showError(serial.GetLastError(), _T("Unable to wait for a COM-port event."));
		Logger::consolePrint("Unable to wait for a COM-port event, errorcode:%d\n", errorCode);
		return;
	}

	// Save event
	const CSerial::EEvent eEvent = serial.GetEventType();

	// Handle break event
	if (eEvent & CSerial::EEventBreak)
	{
		Logger::consolePrint("\n### BREAK received ###\n");
	}

	// Handle CTS event
	if (eEvent & CSerial::EEventCTS)
	{
		Logger::consolePrint("\n### Clear to send %s ###\n", serial.GetCTS() ? "on" : "off");
	}

	// Handle DSR event
	if (eEvent & CSerial::EEventDSR)
	{
		Logger::consolePrint("\n### Data set ready %s ###\n", serial.GetDSR() ? "on" : "off");
	}

	// Handle error event
	if (eEvent & CSerial::EEventError)
	{
		Logger::consolePrint("\n### ERROR: ");
		switch (serial.GetError())
		{
		case CSerial::EErrorBreak:		Logger::consolePrint("Break condition");			break;
		case CSerial::EErrorFrame:		Logger::consolePrint("Framing error");			break;
		case CSerial::EErrorIOE:		Logger::consolePrint("IO device error");			break;
		case CSerial::EErrorMode:		Logger::consolePrint("Unsupported mode");			break;
		case CSerial::EErrorOverrun:	Logger::consolePrint("Buffer overrun");			break;
		case CSerial::EErrorRxOver:		Logger::consolePrint("Input buffer overflow");	break;
		case CSerial::EErrorParity:		Logger::consolePrint("Input parity error");		break;
		case CSerial::EErrorTxFull:		Logger::consolePrint("Output buffer full");		break;
		default:						Logger::consolePrint("Unknown");					break;
		}
		Logger::consolePrint(" ###\n");
	}

	// Handle ring event
	if (eEvent & CSerial::EEventRing)
	{
		Logger::consolePrint("\n### RING ###\n");
	}

	// Handle RLSD/CD event
	if (eEvent & CSerial::EEventRLSD)
	{
		Logger::consolePrint("\n### RLSD/CD %s ###\n", serial.GetRLSD() ? "on" : "off");
	}

	// Handle data receive event
	if (eEvent & CSerial::EEventRecv)
	{
		// Read data, until there is nothing left
		DWORD dwBytesRead = 0;
		char szBuffer[6];
		do
		{
			// Read data from the COM-port
			lLastError = serial.Read(szBuffer, sizeof(szBuffer) - 1, &dwBytesRead);
			if (lLastError != ERROR_SUCCESS) {
				errorCode = showError(serial.GetLastError(), _T("Unable to read from COM-port."));
				Logger::consolePrint("Unable to read from COM-port:%d\n", errorCode);
				return;
			}

			Logger::consolePrint("data byte size:%d, size of buffer:%d\n", sizeof(dwBytesRead), sizeof(szBuffer));
			if (dwBytesRead > 0)
			{
				// Finalize the data, so it is a valid string
				szBuffer[dwBytesRead] = '\0';

				// Display the data
				Logger::consolePrint("%s", szBuffer);

				// Check if EOF (CTRL+'[') has been specified
				if (strchr(szBuffer, exitCommand)) {
					//close port
					closePort();
					appIsRunning = false;
				}
			}
		} while (dwBytesRead == sizeof(szBuffer) - 1);
	}
}

// Restart the application.
void TestAppBluetooth::restart() {

}

bool TestAppBluetooth::processCommandLine(const std::string& cmdLine) {
	if (GLApplication::processCommandLine(cmdLine)) return true;

	return false;
}

void TestAppBluetooth::adjustWindowSize(int width, int height) {
	Logger::consolePrint("resize");

	mainWindowWidth = width;
	mainWindowHeight = height;
	// Send the window size to AntTweakBar
	TwWindowSize(width, height);

	setViewportParameters(0, 0, mainWindowWidth, mainWindowHeight);

	int size[2];
	int w, h;
	TwGetParam(mainMenuBar, NULL, "size", TW_PARAM_INT32, 2, size);
	w = (GLApplication::getMainWindowWidth() - size[0]);
	h = GLApplication::getMainWindowHeight();

	setViewportParameters(size[0], 0, w, h);
}

void TestAppBluetooth::setupLights() {
	GLfloat bright[] = { 0.8f, 0.8f, 0.8f, 1.0f };
	GLfloat mediumbright[] = { 0.3f, 0.3f, 0.3f, 1.0f };

	glLightfv(GL_LIGHT1, GL_DIFFUSE, bright);
	glLightfv(GL_LIGHT2, GL_DIFFUSE, mediumbright);
	glLightfv(GL_LIGHT3, GL_DIFFUSE, mediumbright);
	glLightfv(GL_LIGHT4, GL_DIFFUSE, mediumbright);


	GLfloat light0_position[] = { 0.0f, 10000.0f, 10000.0f, 0.0f };
	GLfloat light0_direction[] = { 0.0f, -10000.0f, -10000.0f, 0.0f };

	GLfloat light1_position[] = { 0.0f, 10000.0f, -10000.0f, 0.0f };
	GLfloat light1_direction[] = { 0.0f, -10000.0f, 10000.0f, 0.0f };

	GLfloat light2_position[] = { 0.0f, -10000.0f, 0.0f, 0.0f };
	GLfloat light2_direction[] = { 0.0f, 10000.0f, -0.0f, 0.0f };

	GLfloat light3_position[] = { 10000.0f, -10000.0f, 0.0f, 0.0f };
	GLfloat light3_direction[] = { -10000.0f, 10000.0f, -0.0f, 0.0f };

	GLfloat light4_position[] = { -10000.0f, -10000.0f, 0.0f, 0.0f };
	GLfloat light4_direction[] = { 10000.0f, 10000.0f, -0.0f, 0.0f };


	glLightfv(GL_LIGHT0, GL_POSITION, light0_position);
	glLightfv(GL_LIGHT1, GL_POSITION, light1_position);
	glLightfv(GL_LIGHT2, GL_POSITION, light2_position);
	glLightfv(GL_LIGHT3, GL_POSITION, light3_position);
	glLightfv(GL_LIGHT4, GL_POSITION, light4_position);


	glLightfv(GL_LIGHT0, GL_SPOT_DIRECTION, light0_direction);
	glLightfv(GL_LIGHT1, GL_SPOT_DIRECTION, light1_direction);
	glLightfv(GL_LIGHT2, GL_SPOT_DIRECTION, light2_direction);
	glLightfv(GL_LIGHT3, GL_SPOT_DIRECTION, light3_direction);
	glLightfv(GL_LIGHT4, GL_SPOT_DIRECTION, light4_direction);


	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHT1);
	glEnable(GL_LIGHT2);
	glEnable(GL_LIGHT3);
	glEnable(GL_LIGHT4);
}

int TestAppBluetooth::showError(LONG lError, LPCTSTR lptszMessage)
{
	// Generate a message text
	TCHAR tszMessage[256];
	wsprintf(tszMessage, _T("%s\n(error code %d)"), lptszMessage, lError);

	// Display message-box and return with an error-code
	::MessageBox(0, tszMessage, _T("Listener"), MB_ICONSTOP | MB_OK);
	return 1;
}