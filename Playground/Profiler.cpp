#include "Profiler.h"

DynamicArray<double> Profiler::times;
DynamicArray<int> Profiler::parents;
DynamicArray<std::string> Profiler::descriptions;
DynamicArray<Timer> Profiler::timers;

int Profiler::searchByDescription(const std::string& description) {
	int end = descriptions.size();
	for (int i = 0; i < end; i++)
		if (descriptions[i] == description)
			return i;
	return -1;
}

void Profiler::startTimer(const std::string& description, const std::string& parent) {
	int index = searchByDescription(description);
	if (index < 0) {
		index = descriptions.size();
		times.push_back(0.0);
		parents.push_back(searchByDescription(parent));
		descriptions.push_back(description);
		timers.push_back(Timer());
	}
	timers[index].restart();
}

void Profiler::collectTimer(const std::string& description) {
	int index = searchByDescription(description);
	if (index >= 0)
		times[index] += timers[index].timeEllapsed();
}

void Profiler::displayResults() {
	Logger::logPrint("Profiling Result:\n");
	int end = descriptions.size();
	
	DynamicArray<int> indents;
	int cIndent = 1;

	for (int i = 0; i < end; i++) {
		cIndent = parents[i] == -1 ? 1 : indents[parents[i]] + 1;
		
		for (int j = 0; j < cIndent; j++)
			Logger::logPrint("  ");
		Logger::logPrint("%s: %.3lfs\n", descriptions[i].c_str(), times[i]);

		indents.push_back(cIndent);
	}
}

void Profiler::reset() {
	int end = descriptions.size();
	for (int i = 0; i < end; i++)
		times[i] = 0.0;
}
