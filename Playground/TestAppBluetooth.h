#pragma once

#include <GUILib/GLApplication.h>
#include <RobotDesignerLib/RobotDesignWindow.h>
#include <string>
#include <map>
#include <GUILib/TranslateWidget.h>
#include <RBSimLib/WorldOracle.h>
#include <GUILib/GLWindow3D.h>
#include <GUILib/GLWindow3DWithMesh.h>
#include <GUILib/GLWindowContainer.h>
#include <GUILib/PlotWindow.h>

#define STRICT
#include <tchar.h>
#include <windows.h>
#include <stdio.h>
#include <Utils\Serial.h>

/**
* Robot Design and Simulation interface
*/
class TestAppBluetooth : public GLApplication {
public:

protected:

	bool drawPlots = false;
	PlotWindow* plotWindow = NULL;

	bool connectToBluetooth = false;

	std::string BluetoothCOMPortName = "\"COM9\"";
	char exitCommand = 'x';

	CSerial serial;
public:

	// constructor
	TestAppBluetooth();
	// destructor
	virtual ~TestAppBluetooth(void);
	// Run the App tasks
	virtual void process();
	// Restart the application.
	virtual void restart();

	//input callbacks...
	//all these methods should returns true if the event is processed, false otherwise...
	//any time a physical key is pressed, this event will trigger. Useful for reading off special keys...
	virtual bool onKeyEvent(int key, int action, int mods);

	virtual bool processCommandLine(const std::string& cmdLine);

	void adjustWindowSize(int width, int height);
	virtual void setupLights();

	int connectPort();

	void closePort();

	int showError(LONG lError, LPCTSTR lptszMessage);
};

