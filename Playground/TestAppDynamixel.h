#pragma once

#include <GUILib/GLApplication.h>
#include <RobotDesignerLib/RobotDesignWindow.h>
#include <string>
#include <map>
#include <GUILib/TranslateWidget.h>
#include <RBSimLib/AbstractRBEngine.h>
#include <RBSimLib/WorldOracle.h>
#include <GUILib/GLWindow3D.h>
#include <GUILib/GLWindow3DWithMesh.h>
#include <GUILib/GLWindowContainer.h>
#include <RobotDesignerLib/LocomotionEngineMotionPlan.h>
#include <RobotDesignerLib/LocomotionEngine.h>
#include <RobotDesignerLib/FootFallPatternViewer.h>
#include <RobotDesignerLib/LocomotionEngineManagerGRF.h>
#include <RobotDesignerLib/LocomotionEngineManagerIP.h>
#include <GUILib/PlotWindow.h>

#pragma warning( disable : 4251) 
#include "dynamixel_sdk.h" 

//TODO: might be nice to set it up such that it works fine with different types of motors...
//TODO: might want to go through all the dynamixels and see which IDs are available, and never try to access any of the ones that are not...
#define JOINT_RAD 0.025

#define BAUDRATE       57600
#define BROADCAST_ID	254

#define TORQUE_ENABLE	64

//NOTE 1: all these values are based on the control table of the XM-series of Dynamixel actuators
//NOTE 2: careful, some of these use 2 bytes, others 4...
#define ADDR_GOAL_CURRENT           102
#define ADDR_GOAL_VELOCITY          104
#define ADDR_GOAL_POSITION          116
#define ADDR_VELOCITY_PROFILE       112
#define ADDR_ACCELERATION_PROFILE   108
#define ADDR_VELOCITY_LIMIT         44
#define ADDR_ACCELERATION_LIMIT		40

#define ADDR_PRESENT_POSITION       132
#define ADDR_PRESENT_VELOCITY       128

#define ADDR_VEL_P_GAIN				78
#define ADDR_POS_D_GAIN				80
#define ADDR_POS_P_GAIN				84
#define ADDR_RETURN_DELAY_TIME		9

#define ADDR_OPERATING_MODE				11
#define DXL_TORQUE_CONTROL_MODE			0
#define DXL_VELOCITY_CONTROL_MODE		1
#define DXL_POSITION_CONTROL_MODE		3

/**
 * Robot Design and Simulation interface
 */
class TestAppDynamixel : public GLApplication {
public:
	Joint* selectedJoint = NULL;
	Robot* robot = NULL;
protected:
	AbstractRBEngine* rbEngine = NULL;
	bool drawPlots = true;
	PlotWindow* plotWindow = NULL;

	bool drawMeshes = true, drawSkeletonView = false, drawJoints = false;

	Joint* highlightedJoint = NULL;

	bool connectedToDynamixels = false;

	std::string DXLPortName = "COM9";
	double motionScaleFactor = 1.0;
	double motorVelocityScaleFactor = 1.0;

/* Dynamixel specific parameters */
	dynamixel::PortHandler *portHandler = NULL;
	dynamixel::PacketHandler *packetHandler = NULL;

	int dxl_comm_result = COMM_TX_FAIL;
	uint8_t dxl_error = 0;

	enum RUN_OPTIONS {
		SYNC_SIM_FROM_ROBOT_STATE = 0,
		ANIMATION_PLAYBACK,
		PHYSICS_SIMULATION,
		ROBOT_CONTROL,
	};

	int runMode = SYNC_SIM_FROM_ROBOT_STATE;
	bool motorsOn = true;

	//this is the control mode...
	enum CONTROL_MODE {
		CM_POSITION_ONLY = 0,
		CM_POSITION_AND_FF_VELOCITY,
		CM_POSITION_AND_FB_VELOCITY,
		CM_FF_VELOCITY_ONLY,
		CM_FB_VELOCITY_ONLY,
		CM_VELOCITY_AND_ACCELERATION
	};
	int controlMode = CM_POSITION_ONLY;
	int lastControlMode = CM_POSITION_ONLY;

	int velPGain = 50;
	int posPGain = 800;
	int posDGain = 0;
	int lastPosPGain = -1;
	int lastPosDGain = -1;
	int lastVelPGain = -1;
	int returnDelayTime = 250;
	int lastReturnDelayTime = -1;



public:

	// constructor
	TestAppDynamixel();
	// destructor
	virtual ~TestAppDynamixel(void);
	// Run the App tasks
	virtual void process();
	// Draw the App scene - camera transformations, lighting, shadows, reflections, etc apply to everything drawn by this method
	virtual void drawScene();
	// This is the wild west of drawing - things that want to ignore depth buffer, camera transformations, etc. Not pretty, quite hacky, but flexible. Individual apps should be careful with implementing this method. It always gets called right at the end of the draw function
	virtual void drawAuxiliarySceneInfo();
	// Restart the application.
	virtual void restart();

	//input callbacks...

	//all these methods should returns true if the event is processed, false otherwise...
	//any time a physical key is pressed, this event will trigger. Useful for reading off special keys...
	virtual bool onKeyEvent(int key, int action, int mods);
	//this one gets triggered on UNICODE characters only...
	virtual bool onCharacterPressedEvent(int key, int mods);
	//triggered when mouse buttons are pressed
	virtual bool onMouseButtonEvent(int button, int action, int mods, double xPos, double yPos);
	//triggered when mouse moves
	virtual bool onMouseMoveEvent(double xPos, double yPos);
	//triggered when using the mouse wheel
	virtual bool onMouseWheelScrollEvent(double xOffset, double yOffset);

	virtual bool processCommandLine(const std::string& cmdLine);
	
	virtual void saveFile(const char* fName);
	virtual void loadFile(const char* fName);

	void loadMenuParametersFor(Joint* joint);
	void unloadMenuParametersFor(Joint* joint);

	void setSimRobotStateFromCurrentDXLMotorValues();
	void setDXLTargetMotorValuesFromSimRobotState();


	//motors may be assembled in a different way than they are modeled, so account for that...
	void flipMotorTargetsIfNeeded();

	//set dynamixel goals from target values
	virtual void sendControlInputsToPhysicalRobot();
	//set current sim values from robot readings...
	void syncSimulationWithPhysicalRobot();
	void readPhysicalRobotMotorPositions();
	void readPhysicalRobotMotorVelocities();

	void reportTrackingErrors(bool updateCurrentMotorPositions, bool updateCurrentMotorVelocities);

	//based on the state of the robot, this method will compute desired motor positions and velocities...
	void computeDXLControlInputsBasedOnSimRobot(double dt);

	void adjustWindowSize(int width, int height);
	virtual void setupLights();

	void connectToDynamixels();

	void closePort();
	void toggleMotors();
	virtual void zeroMotorPositions();
};


inline int getDXLPositionFromRad(double angle) {
	//each DXL tick represents 0.088 deg. So how many ticks do we want for angle?
	int val = (int)(DEG(angle) / (0.0879120879120879) + 0.5);

	//0 is at 2048...
	val += 2048;

	if (val < 0) val = 0;
	if (val > 4095) val = 4095;
	return val;
}

inline double getAngleRadFromDXLPosition(int dxlPos) {
	return RAD((dxlPos - 2048) * 0.0879120879120879);
}

inline double getAngleDegFromDXLPosition(int dxlPos) {
	return (dxlPos - 2048) * 0.0879120879120879;
}

inline int getDXLPositionFromDeg(double angle) {
	return getDXLPositionFromRad(RAD(angle));
}

inline double getVelRadPerSecFromDXLVelocity(int dxlVel) {
	return dxlVel * (0.229 * 2 * PI / 60);
}

inline int getDXLAccelFromRadPerSecSquared(double acc) {
	//each DXL tick represents 0.0036 rev/min/min or 0.0036 * 2 * PI / 60 / 60 rad/s/s.
	int val = (int)(acc / (0.0036 * 2 * PI / 60 / 60) + 0.5);
	if (val < -1223) val = -1223;
	if (val > 1223) val = 1223;
	return val;
}

inline int getDXLVelocityFromRadPerSec(double vel) {
	//each DXL tick represents 0.229 RPM, or 0.229 * 2 * PI / 60 rad/s. So how many ticks do we want for vel?
	int val = (int)(vel / (0.229 * 2 * PI / 60) + 0.5);
	if (val < -1223) val = -1223;
	if (val > 1223) val = 1223;
	return val;
}

inline int getDXLVelocityFromRPM(double vel) {
	//each DXL tick represents 0.229 RPM. So how many ticks do we want for vel?
	int val = (int)(vel / 0.229 + 0.5);
	if (val < 0) val = 0;
	if (val > 1223) val = 1223;
	return val;
}

inline uint8_t* DXL_INT32_TO_UINT8_ARRAY(int val, uint8_t* uint8Array) {
	uint8Array[0] = DXL_LOBYTE(DXL_LOWORD(val));
	uint8Array[1] = DXL_HIBYTE(DXL_LOWORD(val));
	uint8Array[2] = DXL_LOBYTE(DXL_HIWORD(val));
	uint8Array[3] = DXL_HIBYTE(DXL_HIWORD(val));
	return uint8Array;
}

