

#include <GUILib/GLUtils.h>
#include "TestAppModularRobot.h"
#include <GUILib/GLMesh.h>
#include <GUILib/GLContentManager.h>
#include <GUILib/GLTrackingCamera.h>
#include <MathLib/MathLib.h>
#include <iostream>
#include <MathLib/boundingBox.h>


using namespace std;

/*
pick an RMCRobot by clicking on it, translation and rotation widgets will show on its root.
And its picked RMC will be painted in orange. (Root is painted in green)

Once you select an RMCRobot:
key 'Q': clone the RMCRobot
key 'W': save the picked RMCRobot to ../out/tmpRMCRobot.mrb
key 'F': switch the parent joint of the picked RMC to the next relative transformation.
key 'D': delete the subtree from the selected RMC


key 'S': save the whole design to ../out/tmpModularDesign.dsn
key 'R': load design from ../out/tmpModularDesign.dsn

key 'T': create search tree and set selected RMC as root
key 'Y': set search tree target RMC
key 'G': exit search tree mode
key 'B': build selected search tree path solution

*/

TestAppModularRobot::TestAppModularRobot() {
	bgColor[0] = bgColor[1] = bgColor[2] = 1;
	setWindowTitle("Empty Test Application...");

	camera->onMouseWheelScrollEvent(0, 10.0);

	tWidget = new TranslateWidget();
	//	rWidget = new RotateWidgetV1();
	rWidget = new RotateWidgetV2();
	rWidget->visible = false;

	showGroundPlane = false;

	TwAddSeparator(mainMenuBar, "sep2", "");


	int size[2];
	int w, h;
	TwGetParam(mainMenuBar, NULL, "size", TW_PARAM_INT32, 2, size);
	w = (GLApplication::getMainWindowWidth() - size[0]) / 2;
	h = GLApplication::getMainWindowHeight();
	designWindow = new ModularDesignWindow(size[0], 0, w, h, this, "../data/modularRobot/configXM-320.cfg");

	setViewportParameters(size[0] + w, 0, w, h);
	consoleWindow->setViewportParameters(size[0] + w, 0, w, 280);
}

void TestAppModularRobot::adjustWindowSize(int width, int height) {
	Logger::consolePrint("resize");

	mainWindowWidth = width;
	mainWindowHeight = height;
	// Send the window size to AntTweakBar
	TwWindowSize(width, height);

	setViewportParameters(0, 0, mainWindowWidth, mainWindowHeight);

	int size[2];
	int w, h;
	TwGetParam(mainMenuBar, NULL, "size", TW_PARAM_INT32, 2, size);
	w = (GLApplication::getMainWindowWidth() - size[0]) / 2;
	h = GLApplication::getMainWindowHeight();
	designWindow->setViewportParameters(size[0], 0, w, h);
	setViewportParameters(size[0] + w, 0, w, h);
}

TestAppModularRobot::~TestAppModularRobot(void){

	delete designWindow;
	
	// Widgets
	delete rWidget;
	delete tWidget;
}

//triggered when mouse moves
bool TestAppModularRobot::onMouseMoveEvent(double xPos, double yPos) {
	if (showMenus)
		if (TwEventMousePosGLFW((int)xPos, (int)yPos)) return true;

	if (designWindow->isActive() || designWindow->mouseIsWithinWindow(xPos, yPos))
		if (designWindow->onMouseMoveEvent(xPos, yPos)) return true;

	if (GLApplication::onMouseMoveEvent(xPos, yPos) == true) return true;

	return false;
}


//triggered when mouse buttons are pressed
bool TestAppModularRobot::onMouseButtonEvent(int button, int action, int mods, double xPos, double yPos) {
	
	if(designWindow->isActive() || designWindow->mouseIsWithinWindow(xPos, yPos))
		if (designWindow->onMouseButtonEvent(button, action, mods, xPos, yPos)) return true;

	
	if (GLApplication::onMouseButtonEvent(button, action, mods, xPos, yPos)) return true;
	return false;
}

//triggered when using the mouse wheel
bool TestAppModularRobot::onMouseWheelScrollEvent(double xOffset, double yOffset) {
	if(designWindow->isActive())
		if (designWindow->onMouseWheelScrollEvent(xOffset, yOffset)) return true;

	if (GLApplication::onMouseWheelScrollEvent(xOffset, yOffset)) return true;

	return false;
}

bool TestAppModularRobot::onKeyEvent(int key, int action, int mods) {
	if (designWindow->isActive())
		if (designWindow->onKeyEvent(key, action, mods)) return true;

	if (GLApplication::onKeyEvent(key, action, mods)) return true;
	return false;
}

bool TestAppModularRobot::onCharacterPressedEvent(int key, int mods) {
	if (GLApplication::onCharacterPressedEvent(key, mods)) return true;

	return false;
}


void TestAppModularRobot::loadFile(const char* fName) {
	std::string fileName;
	fileName.assign(fName);

	std::string fNameExt = fileName.substr(fileName.find_last_of('.') + 1);

	if (fNameExt == "dsn")
	{
		designWindow->loadFile(fName);
	}
}

void TestAppModularRobot::saveFile(const char* fName) {

	Logger::consolePrint("SAVE FILE: Do not know what to do with file \'%s\'\n", fName);
}

// Run the App tasks
void TestAppModularRobot::process() {
	//do the work here...
	if (designWindow->process()) return;
	
}

void TestAppModularRobot::setupLights() {

	GLfloat bright[] = { 0.8f, 0.8f, 0.8f, 1.0f };
	GLfloat mediumbright[] = { 0.3f, 0.3f, 0.3f, 1.0f };

	glLightfv(GL_LIGHT1, GL_DIFFUSE, bright);
	glLightfv(GL_LIGHT2, GL_DIFFUSE, mediumbright);
	glLightfv(GL_LIGHT3, GL_DIFFUSE, mediumbright);
	glLightfv(GL_LIGHT4, GL_DIFFUSE, mediumbright);


	GLfloat light0_position[] = { 0.0f, 10000.0f, 10000.0f, 0.0f };
	GLfloat light0_direction[] = { 0.0f, -10000.0f, -10000.0f, 0.0f };

	GLfloat light1_position[] = { 0.0f, 10000.0f, -10000.0f, 0.0f };
	GLfloat light1_direction[] = { 0.0f, -10000.0f, 10000.0f, 0.0f };

	GLfloat light2_position[] = { 0.0f, -10000.0f, 0.0f, 0.0f };
	GLfloat light2_direction[] = { 0.0f, 10000.0f, -0.0f, 0.0f };

	GLfloat light3_position[] = { 10000.0f, -10000.0f, 0.0f, 0.0f };
	GLfloat light3_direction[] = { -10000.0f, 10000.0f, -0.0f, 0.0f };

	GLfloat light4_position[] = { -10000.0f, -10000.0f, 0.0f, 0.0f };
	GLfloat light4_direction[] = { 10000.0f, 10000.0f, -0.0f, 0.0f };


	glLightfv(GL_LIGHT0, GL_POSITION, light0_position);
	glLightfv(GL_LIGHT1, GL_POSITION, light1_position);
	glLightfv(GL_LIGHT2, GL_POSITION, light2_position);
	glLightfv(GL_LIGHT3, GL_POSITION, light3_position);
	glLightfv(GL_LIGHT4, GL_POSITION, light4_position);


	glLightfv(GL_LIGHT0, GL_SPOT_DIRECTION, light0_direction);
	glLightfv(GL_LIGHT1, GL_SPOT_DIRECTION, light1_direction);
	glLightfv(GL_LIGHT2, GL_SPOT_DIRECTION, light2_direction);
	glLightfv(GL_LIGHT3, GL_SPOT_DIRECTION, light3_direction);
	glLightfv(GL_LIGHT4, GL_SPOT_DIRECTION, light4_direction);


	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHT1);
	glEnable(GL_LIGHT2);
	glEnable(GL_LIGHT3);
	glEnable(GL_LIGHT4);
}


// Draw the App scene - camera transformations, lighting, shadows, reflections, etc apply to everything drawn by this method
void TestAppModularRobot::drawScene() {
	

	
}

// This is the wild west of drawing - things that want to ignore depth buffer, camera transformations, etc. Not pretty, quite hacky, but flexible. Individual apps should be careful with implementing this method. It always gets called right at the end of the draw function
void TestAppModularRobot::drawAuxiliarySceneInfo() {
	
	//clear the depth buffer so that the widgets show up on top of the object primitives
	glClear(GL_DEPTH_BUFFER_BIT);

	// draw the widgets
	tWidget->draw();
	rWidget->draw();

	glClear(GL_DEPTH_BUFFER_BIT);
	
	designWindow->draw();
	designWindow->drawAuxiliarySceneInfo();
}

// Restart the application.
void TestAppModularRobot::restart() {

}

bool TestAppModularRobot::processCommandLine(const std::string& cmdLine) {

	int index = cmdLine.find_first_of(' ');
	string cmd = cmdLine.substr(0, index);

	if (GLApplication::processCommandLine(cmdLine)) return true;

	return false;
}
