#include <GUILib/GLUtils.h>

#include "TestAppRobotDesignOptimization.h"


/**
	- compute gradient using sensitivity analysis
	- analyize eigenspectrum of jacobian - which parameters should be synnergistically modified to account for user preferences or specific goals (need larger battery pack, longer tail, etc).
	- use jacobian with GD to make robots that can walk faster, jump higher, traverse different terrains, etc.
	- use same method for metaparameters (e.g. timing, timing of individual substeps in ffp, swing foot height, etc).
	- adjoint method
	- heavy ball gradient-based optimization with small-ish steps to avoid need for line search...
*/

#include <GUILib/GLMesh.h>
#include <GUILib/GLContentManager.h>
#include <MathLib/MathLib.h>
#include <RBSimLib/ODERBEngine.h>
#include <ControlLib/SimpleLimb.h>
#include <RobotDesignerLib/SpotMiniParamterizedDesign.h>
#include "Profiler.h"
#include <omp.h>

using namespace LBFGSpp;

#define THRESHOLD	1e8

#define AUTO(prd) (static_cast<AutoParameterizedRobotDesign *>(prd))

#define GET_PARAMETERS(prd, p) {\
	AUTO(prd)->getCurrentSetOfParameters(p);\
}

#define SET_PARAMETERS(prd, p, motionPlan) {\
	AUTO(prd)->setParameters(p);\
	(motionPlan)->updateRobotRepresentation();\
}

//#define RUN_MOPT(energy, converged) {\
//	do {\
//		energy = moptWindow->locomotionManager->runMOPTStep();\
//	} while (energy < THRESHOLD && !(converged));\
//}

#define RUN_MOPT(energy, converged) {\
	energy = moptWindow->locomotionManager->runMOPTStep();\
}

void TW_CALL LoadRobot(void* clientData) {
	((TestAppRobotDesignOptimization*)clientData)->loadToSim();
}

void TW_CALL OptimizeDesign(void* clientData) {
	((TestAppRobotDesignOptimization*)clientData)->optimizeDesign();
}

void TW_CALL RunDOPTStep(void *clientData) {
	((TestAppRobotDesignOptimization*)clientData)->runDOPTStep();
}

void TW_CALL TestJacobians(void* clientData) {
	((TestAppRobotDesignOptimization*)clientData)->testJacobians();
}

void TW_CALL TestApproximation(void *clientData) {
	((TestAppRobotDesignOptimization*)clientData)->testApproximation();
}

void TW_CALL TestParallelism(void *clientData) {
	((TestAppRobotDesignOptimization*)clientData)->parallelTest();
}

void TW_CALL TestGradient(void *clientData) {
	((TestAppRobotDesignOptimization*)clientData)->testGradient();
}

void TW_CALL WarmstartMOPT(void* clientData) {
	((TestAppRobotDesignOptimization*)clientData)->warmStartMOPT();
}

TestAppRobotDesignOptimization::TestAppRobotDesignOptimization(): param(), solver(param) {
	bgColor[0] = bgColor[1] = bgColor[2] = 1;
	setWindowTitle("RobotDesigner");

	waitForFrameRate = true;

	TwAddSeparator(mainMenuBar, "sep2", "");

	drawCDPs = false;
	drawSkeletonView = false;
	showGroundPlane = false;

	TwAddButton(mainMenuBar, "LoadRobotToSim", LoadRobot, this, " label='Load To Simulation' group='Sim' key='l' ");
	TwAddButton(mainMenuBar, "WarmstartMOPT", WarmstartMOPT, this, " label='WarmStart MOPT' group='Sim' key='w' ");

	TwAddVarRW(mainMenuBar, "RunOptions", TwDefineEnumFromString("RunOptions", "..."), &runOption, "group='Sim'");
	DynamicArray<std::string> runOptionList;
	runOptionList.push_back("\\Motion Optimization");
	runOptionList.push_back("\\Motion Plan Animation");
	runOptionList.push_back("\\Design Optimization");
	runOptionList.push_back("\\Simulation");
	runOptionList.push_back("\\Simulation-SmoothControl");
	generateMenuEnumFromFileList("MainMenuBar/RunOptions", runOptionList);

	//TwAddVarRW(mainMenuBar, "drawMotionPlan", TW_TYPE_BOOLCPP, &drawMotionPlan, " label='drawMotionPlan' group='Viz2'");
	//TwAddVarRW(mainMenuBar, "DrawMeshes", TW_TYPE_BOOLCPP, &drawMeshes, " label='drawMeshes' group='Viz2'");
	//TwAddVarRW(mainMenuBar, "DrawMOIs", TW_TYPE_BOOLCPP, &drawMOIs, " label='drawMOIs' group='Viz2'");
	//TwAddVarRW(mainMenuBar, "DrawCDPs", TW_TYPE_BOOLCPP, &drawCDPs, " label='drawCDPs' group='Viz2'");
	//TwAddVarRW(mainMenuBar, "DrawSkeletonView", TW_TYPE_BOOLCPP, &drawSkeletonView, " label='drawSkeleton' group='Viz2'");
	//TwAddVarRW(mainMenuBar, "DrawJoints", TW_TYPE_BOOLCPP, &drawJoints, " label='drawJoints' group='Viz2'");
	//TwAddVarRW(mainMenuBar, "drawContactForces", TW_TYPE_BOOLCPP, &drawContactForces, " label='drawContactForces' group='Viz2'");
	//TwAddVarRW(mainMenuBar, "drawOrientation", TW_TYPE_BOOLCPP, &drawOrientation, " label='drawOrientation' group='Viz2'");

	TwAddSeparator(mainMenuBar, "sep3", "");
	TwAddButton(mainMenuBar, "Optimize Robot Design", OptimizeDesign, this, " label='optimize design' key='o' ");
	TwAddButton(mainMenuBar, "Run DOPT Step", RunDOPTStep, this, " label='run DOPT step' key='s' ");
	//TwAddButton(mainMenuBar, "Test Jacobians", TestJacobians, this, " label='test Jacobians' key='t' ");
	//TwAddButton(mainMenuBar, "Test Approximation", TestApproximation, this, " label='test approximation' key='t' ");
	TwAddButton(mainMenuBar, "Test Gradient", TestGradient, this, " label='test gradient' key='t' ");
	//TwAddButton(mainMenuBar, "Test Parallelism", TestParallelism, this, " label='test parallelism' key='p' ");
	
	TwAddSeparator(mainMenuBar, "sep4", "");
	TwAddVarRW(mainMenuBar, "PlanMotionPhase", TW_TYPE_DOUBLE, &motionPlanTime, "min=0 max=1 step=0.01 group='MotionOptimizationOptions'");

	simTimeStep = 1 / 500.0;
//	Globals::g = 0;

	int size[2];
	int w, h;
	TwGetParam(mainMenuBar, NULL, "size", TW_PARAM_INT32, 2, size);
	w = (GLApplication::getMainWindowWidth()-size[0]) / 2;
	h = GLApplication::getMainWindowHeight();
	
	moptWindow = new MOPTWindow(size[0], 0, w, h, this);
	
	setViewportParameters(size[0] + w, 0, w, h);
	consoleWindow->setViewportParameters(size[0] + w, 0, w, 280);

//
//load little robot
//	loadFile("../data/motionplans/spotMini/robotMini.rbs");
//	loadFile("../data/motionplans/spotMini/robot.rs");
//	loadToSim(false);
//	loadFile("../data/motionplans/spotMini/testWalk2.p");
//

///*
//load big robot
	loadFile("../data/motionplans/spotMini/robot.rbs");
	loadFile("../data/motionplans/spotMini/robot.rs");
	loadToSim(false);
//	loadFile("../data/motionplans/spotMini/testWalk.p");
//	loadFile("../out/optimalPlan.p");
	loadFile("../out/optimalPlanBig.p");

	LocomotionEngineMotionPlan *motionPlan = moptWindow->locomotionManager->motionPlan;

	V3D v(motionPlan->COMTrajectory.getCOMPositionAtTimeIndex(0), motionPlan->COMTrajectory.getCOMPositionAtTimeIndex(motionPlan->nSamplePoints - 1));
	Logger::consolePrint("distance: %lf %lf %lf\n", v.x(), v.y(), v.z());

//	testJacobians();
//*/
	
/*
//load optimized robot
	loadFile("../data/motionplans/spotMini/spotMiniOptimized.rbs");
	loadFile("../data/motionplans/spotMini/robot.rs");
	loadToSim(false);
	loadFile("../data/motionplans/spotMini/testWalkOptimized.p");
*/

	showGroundPlane = true;

	TwDefine(" MainMenuBar alpha=255 ");   // semi-transparent blue bar

	bgColor[0] = bgColor[1] = bgColor[2] = 0.75;

	dynamic_cast<GLTrackingCamera*>(this->camera)->rotAboutRightAxis = 0.25;
	dynamic_cast<GLTrackingCamera*>(this->camera)->rotAboutUpAxis = 0.95;
	dynamic_cast<GLTrackingCamera*>(this->camera)->camDistance = -1.5;

	//Beichen Li: initialization for LBFGS parameters
	param.max_iterations = 20;
	param.max_linesearch = 12;
	param.epsilon = 1e-3;

	//Beichen Li: OpenMP initialization
//	Logger::consolePrint("Max threads: %d\n", omp_get_max_threads());
	omp_set_num_threads(numThreads);
}

TestAppRobotDesignOptimization::~TestAppRobotDesignOptimization(void){
}

void TestAppRobotDesignOptimization::addParameterizedDesignParametersToMenu() {
	prd->getCurrentSetOfParameters(designParameters);
	designParameterNames.resize(designParameters.size());
	for (uint i = 0; i < designParameters.size(); i++) {
		designParameterNames[i] = string("p") + to_string(i);
		TwAddVarRW(mainMenuBar, designParameterNames[i].c_str(), TW_TYPE_DOUBLE, &designParameters[i], "step=0.01 group ='DesignParams'");
	}
//	prd->setParameters(params);
}

void TestAppRobotDesignOptimization::removeParameterizedDesignParametersFromMenu() {

	for (uint i = 0; i < designParameterNames.size(); i++) {
		TwRemoveVar(mainMenuBar, designParameterNames[i].c_str());
	}
}


//triggered when mouse moves
bool TestAppRobotDesignOptimization::onMouseMoveEvent(double xPos, double yPos) {
	if (showMenus)
		if (TwEventMousePosGLFW((int)xPos, (int)yPos)) return true;

	if (moptWindow->isActive() || moptWindow->mouseIsWithinWindow(xPos, yPos))
		if (moptWindow->onMouseMoveEvent(xPos, yPos)) return true;

	if (GLApplication::onMouseMoveEvent(xPos, yPos)) return true;

	return false;
}

//triggered when mouse buttons are pressed
bool TestAppRobotDesignOptimization::onMouseButtonEvent(int button, int action, int mods, double xPos, double yPos) {
	if (showMenus)

	if (moptWindow->isActive() || moptWindow->mouseIsWithinWindow(xPos, yPos))
		if (moptWindow->onMouseButtonEvent(button, action, mods, xPos, yPos)) return true;

	if (GLApplication::onMouseButtonEvent(button, action, mods, xPos, yPos)) return true;

	return false;
}

//triggered when using the mouse wheel
bool TestAppRobotDesignOptimization::onMouseWheelScrollEvent(double xOffset, double yOffset) {

	if (moptWindow->mouseIsWithinWindow(GlobalMouseState::lastMouseX, GlobalMouseState::lastMouseY))
		if (moptWindow->onMouseWheelScrollEvent(xOffset, yOffset)) return true;

	if (GLApplication::onMouseWheelScrollEvent(xOffset, yOffset)) return true;

	return false;
}

bool TestAppRobotDesignOptimization::onKeyEvent(int key, int action, int mods) {
	if (locomotionManager && locomotionManager->motionPlan){
		if (key == GLFW_KEY_UP && action == GLFW_PRESS)
			locomotionManager->motionPlan->desDistanceToTravel.z() += 0.1;
		if (key == GLFW_KEY_DOWN && action == GLFW_PRESS)
			locomotionManager->motionPlan->desDistanceToTravel.z() -= 0.1;
		if (key == GLFW_KEY_LEFT && action == GLFW_PRESS)
			locomotionManager->motionPlan->desDistanceToTravel.x() += 0.1;
		if (key == GLFW_KEY_RIGHT && action == GLFW_PRESS)
			locomotionManager->motionPlan->desDistanceToTravel.x() -= 0.1;
		if (key == GLFW_KEY_PERIOD && action == GLFW_PRESS)
			locomotionManager->motionPlan->desTurningAngle += 0.1;
		if (key == GLFW_KEY_SLASH && action == GLFW_PRESS)
			locomotionManager->motionPlan->desTurningAngle -= 0.1;

		boundToRange(&locomotionManager->motionPlan->desDistanceToTravel.z(), -0.5, 0.5);
		boundToRange(&locomotionManager->motionPlan->desDistanceToTravel.x(), -0.5, 0.5);
		boundToRange(&locomotionManager->motionPlan->desTurningAngle, -0.5, 0.5);
	}

	if (key == GLFW_KEY_O && action == GLFW_PRESS)
		locomotionManager->motionPlan->writeRobotMotionAnglesToFile("../out/tmpMPAngles.mpa");

	if (key == GLFW_KEY_1 && action == GLFW_PRESS)
		runOption = MOTION_PLAN_OPTIMIZATION;
	if (key == GLFW_KEY_2 && action == GLFW_PRESS)
		runOption = MOTION_PLAN_ANIMATION;
	if (key == GLFW_KEY_3 && action == GLFW_PRESS)
		runOption = DESIGN_OPTIMIZATION;

	if (key == GLFW_KEY_M && action == GLFW_PRESS)
		locomotionManager->motionPlan->writeParamsToFile("../out/MPParams.p");
	
	if (GLApplication::onKeyEvent(key, action, mods)) return true;

	return false;
}

bool TestAppRobotDesignOptimization::onCharacterPressedEvent(int key, int mods) {
	if (GLApplication::onCharacterPressedEvent(key, mods)) return true;
	return false;
}

void TestAppRobotDesignOptimization::loadFile(const char* fName) {
	Logger::consolePrint("Loading file \'%s\'...\n", fName);
	std::string fileName;
	fileName.assign(fName);

	std::string fNameExt = fileName.substr(fileName.find_last_of('.') + 1);

	if (fNameExt.compare("rs") == 0) {
		Logger::consolePrint("Load robot state from '%s'\n", fName);
		if (robot) {
			robot->loadReducedStateFromFile(fName);
		}
		return;
	}

	if (fNameExt.compare("rbs") == 0 ){ 
		removeParameterizedDesignParametersFromMenu();

		delete rbEngine;
		delete robot;
		delete prd;

		rbEngine = new ODERBEngine();
		rbEngine->loadRBsFromFile(fName);
		robot = new Robot(rbEngine->rbs[0]);
//		prd = new ParameterizedRobotDesign(robot);
//		prd = new SpotMiniParameterizedDesign(robot);
//		prd = new TestParameterizedRobotDesign(robot);
		prd = new AutoParameterizedRobotDesign(robot);


		addParameterizedDesignParametersToMenu();
		return;
	}

	if (fNameExt.compare("ffp") == 0) {
		moptWindow->footFallPattern.loadFromFile(fName);
		moptWindow->footFallPattern.writeToFile("..\\out\\tmpFFP.ffp");
		return;
	}

	if (fNameExt.compare("p") == 0) {
		if (locomotionManager && locomotionManager->motionPlan) {
			locomotionManager->motionPlan->readParamsFromFile(fName);
			locomotionManager->motionPlan->updateRobotRepresentation();
			//locomotionManager->motionPlan->robotRepresentation->initializeWorldCoordinates();
		}
		return;
	}

}

void TestAppRobotDesignOptimization::loadToSim(bool useWarmStarting){
	ReducedRobotState startingRobotState(robot);
	setupSimpleRobotStructure(robot);
	moptWindow->loadRobot(robot, &startingRobotState);

	int nLegs = robot->bFrame->limbs.size();
	int nPoints = 3 * nLegs;

	//now, start MOPT...
	warmStartMOPT(useWarmStarting);

	Logger::consolePrint("Warmstart successful...\n");
	Logger::consolePrint("The robot has %d legs, weighs %lfkgs and is %lfm tall...\n", robot->bFrame->limbs.size(), robot->getMass(), robot->root->getCMPosition().y());
}

void TestAppRobotDesignOptimization::warmStartMOPT(bool useWarmStarting) {
	delete locomotionManager;
	locomotionManager = moptWindow->initializeNewMP(useWarmStarting);
	animationCycle = 0;
}

void TestAppRobotDesignOptimization::saveFile(const char* fName) {
	Logger::consolePrint("SAVE FILE: Do not know what to do with file \'%s\'\n", fName);
}

double TestAppRobotDesignOptimization::runMOPTStep() {
	double energy = moptWindow->locomotionManager->runMOPTStep();

	Logger::consolePrint("Current Energy: %.6lf\n", energy);

	return energy;

//	dVector params;
//	LocomotionEngineMotionPlan* motionPlan = moptWindow->locomotionManager->locomotionEngine->motionPlan;
//	motionPlan->writeMPParametersToList(params);
//	Logger::consolePrint("obj %s value: %lf\n", moptWindow->locomotionManager->locomotionEngine->energyFunction->objectives[11]->description.c_str(), moptWindow->locomotionManager->locomotionEngine->energyFunction->objectives[11]->computeValue(params));

//	V3D v(motionPlan->COMTrajectory.getCOMPositionAtTimeIndex(0), motionPlan->COMTrajectory.getCOMPositionAtTimeIndex(motionPlan->nSamplePoints - 1));
//	Logger::consolePrint("distance travelled: %lf %lf %lf\n", v.x(), v.y(), v.z());
}

// Run the App tasks
void TestAppRobotDesignOptimization::process() {
	if (runOption == MOTION_PLAN_OPTIMIZATION) {
		animationCycle = 0;
		runMOPTStep();
	}
	else if (runOption == MOTION_PLAN_ANIMATION) {
		motionPlanTime += (1.0 / desiredFrameRate) / locomotionManager->motionPlan->motionPlanDuration;
		if (motionPlanTime > 1) {
			animationCycle++;
			motionPlanTime -= 1;
		}
		moptWindow->setAnimationParams(motionPlanTime, 0);
	}
	else if (runOption == DESIGN_OPTIMIZATION) {
		animationCycle = 0;
		optimizeDesign();
	}
}

// Draw the App scene - camera transformations, lighting, shadows, reflections, etc apply to everything drawn by this method
void TestAppRobotDesignOptimization::drawScene() {
	prd->setParameters(designParameters);

	robot->saveRBSToFile("../out/robotDesign.rbs");

	glColor3d(1, 1, 1);
	glDisable(GL_LIGHTING);

	//in case the user is manipulating any of parameters of the motion plan, update them.
	
	if (locomotionManager)
		locomotionManager->drawMotionPlan(motionPlanTime, animationCycle, false, true, drawMotionPlan, drawContactForces, drawOrientation);

}

// This is the wild west of drawing - things that want to ignore depth buffer, camera transformations, etc. Not pretty, quite hacky, but flexible. Individual apps should be careful with implementing this method. It always gets called right at the end of the draw function
void TestAppRobotDesignOptimization::drawAuxiliarySceneInfo() {
	moptWindow->draw();
	moptWindow->drawAuxiliarySceneInfo();
}

// Restart the application.
void TestAppRobotDesignOptimization::restart() {

}

bool TestAppRobotDesignOptimization::processCommandLine(const std::string& cmdLine) {
	if (GLApplication::processCommandLine(cmdLine)) return true;

	return false;
}

void TestAppRobotDesignOptimization::adjustWindowSize(int width, int height) {
	//Logger::consolePrint("resize");

	mainWindowWidth = width;
	mainWindowHeight = height;
	// Send the window size to AntTweakBar
	TwWindowSize(width, height);

	setViewportParameters(0, 0, mainWindowWidth, mainWindowHeight);

	int size[2];
	int w, h;
	TwGetParam(mainMenuBar, NULL, "size", TW_PARAM_INT32, 2, size);
	w = (GLApplication::getMainWindowWidth() - size[0]) / 2;
	h = GLApplication::getMainWindowHeight();
	setViewportParameters(size[0] + w, 0, w, h);
}


void TestAppRobotDesignOptimization::setupLights() {
	GLfloat bright[] = { 0.8f, 0.8f, 0.8f, 1.0f };
	GLfloat mediumbright[] = { 0.3f, 0.3f, 0.3f, 1.0f };

	glLightfv(GL_LIGHT1, GL_DIFFUSE, bright);
	glLightfv(GL_LIGHT2, GL_DIFFUSE, mediumbright);
	glLightfv(GL_LIGHT3, GL_DIFFUSE, mediumbright);
	glLightfv(GL_LIGHT4, GL_DIFFUSE, mediumbright);


	GLfloat light0_position[] = { 0.0f, 10000.0f, 10000.0f, 0.0f };
	GLfloat light0_direction[] = { 0.0f, -10000.0f, -10000.0f, 0.0f };

	GLfloat light1_position[] = { 0.0f, 10000.0f, -10000.0f, 0.0f };
	GLfloat light1_direction[] = { 0.0f, -10000.0f, 10000.0f, 0.0f };

	GLfloat light2_position[] = { 0.0f, -10000.0f, 0.0f, 0.0f };
	GLfloat light2_direction[] = { 0.0f, 10000.0f, -0.0f, 0.0f };

	GLfloat light3_position[] = { 10000.0f, -10000.0f, 0.0f, 0.0f };
	GLfloat light3_direction[] = { -10000.0f, 10000.0f, -0.0f, 0.0f };

	GLfloat light4_position[] = { -10000.0f, -10000.0f, 0.0f, 0.0f };
	GLfloat light4_direction[] = { 10000.0f, 10000.0f, -0.0f, 0.0f };


	glLightfv(GL_LIGHT0, GL_POSITION, light0_position);
	glLightfv(GL_LIGHT1, GL_POSITION, light1_position);
	glLightfv(GL_LIGHT2, GL_POSITION, light2_position);
	glLightfv(GL_LIGHT3, GL_POSITION, light3_position);
	glLightfv(GL_LIGHT4, GL_POSITION, light4_position);


	glLightfv(GL_LIGHT0, GL_SPOT_DIRECTION, light0_direction);
	glLightfv(GL_LIGHT1, GL_SPOT_DIRECTION, light1_direction);
	glLightfv(GL_LIGHT2, GL_SPOT_DIRECTION, light2_direction);
	glLightfv(GL_LIGHT3, GL_SPOT_DIRECTION, light3_direction);
	glLightfv(GL_LIGHT4, GL_SPOT_DIRECTION, light4_direction);


	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHT1);
	glEnable(GL_LIGHT2);
	glEnable(GL_LIGHT3);
	glEnable(GL_LIGHT4);
}

#define TEST_DP		1e-4
#define TEST_DPP	1e-4

void TestAppRobotDesignOptimization::testJacobians() {
	//we will evaluate dm/dp at the current point in motion and design params...
	LocomotionEngine *engine = moptWindow->locomotionManager->locomotionEngine;
	LocomotionEngineMotionPlan *motionPlan = moptWindow->locomotionManager->motionPlan;
	LocomotionEngine_EnergyFunction *energyFunction = engine->energyFunction;
	
	dVector m;
	motionPlan->writeMPParametersToList(m);
	dVector p;
	GET_PARAMETERS(prd, p);

	SparseMatrix dgdm;
	MatrixNxM dmdp;
	dVector dgdpi, dmdpi;
	dVector g_m, g_p;

	resize(dgdm, m.size(), m.size());
	resize(dmdp, m.size(), p.size());
	resize(dgdpi, m.size());
	resize(g_m, m.size());
	resize(g_p, m.size());

	DynamicArray<MTriplet> triplets;
	energyFunction->addHessianEntriesTo(triplets, m);
	dgdm.setFromTriplets(triplets.begin(), triplets.end());

	Eigen::SimplicialLDLT<SparseMatrix, Eigen::Lower> solver;
	solver.compute(dgdm);

	double dp = TEST_DP;
	//now, for every design parameter, estimate change in gradient, and use that to compute the corresponding entry in dm/dp...
	for (int i = 0; i < p.size(); i++) {
		double pVal = p[i];
		p[i] = pVal + dp;
		SET_PARAMETERS(prd, p, motionPlan);
		energyFunction->addGradientTo(g_p, m);
		
		p[i] = pVal - dp;
		SET_PARAMETERS(prd, p, motionPlan);
		energyFunction->addGradientTo(g_m, m);
		
		p[i] = pVal;
		SET_PARAMETERS(prd, p, motionPlan);

		dgdpi = (g_p - g_m) / (2 * dp);

		dmdp.col(i) = solver.solve(dgdpi) * -1;
	}

	//ok, now we have dm/dp, the analytic version. 
	//Now estimate this jacobian with finite differences...
	MatrixNxM dmdp_FD;
	dVector m_initial, m_m, m_p;
	resize(dmdp_FD, m.size(), p.size());
	resize(m_m, m.size());
	resize(m_p, m.size());

	dp = TEST_DP;
	motionPlan->writeMPParametersToList(m_initial);
	double energy;
	//now, for every design parameter, estimate change in gradient, and use that to compute the corresponding entry in dm/dp...
	for (int i = 0; i < p.size(); i++) {
		double pVal = p[i];
		p[i] = pVal + dp;
		SET_PARAMETERS(prd, p, motionPlan);
		RUN_MOPT(energy, engine->converged);
		motionPlan->writeMPParametersToList(m_p);
		motionPlan->setMPParametersFromList(m_initial);

		p[i] = pVal - dp;
		SET_PARAMETERS(prd, p, motionPlan);
		RUN_MOPT(energy, engine->converged);
		motionPlan->writeMPParametersToList(m_m);
		motionPlan->setMPParametersFromList(m_initial);

		p[i] = pVal;
		SET_PARAMETERS(prd, p, motionPlan);

		dmdp_FD.col(i) = (m_p - m_m) / (2 * dp);
	}
	
	print("../out/dmdp.txt", dmdp);
	print("../out/dmdp_FD.txt", dmdp_FD);
}

void TestAppRobotDesignOptimization::testApproximation() {
	LocomotionEngineMotionPlan *motionPlan = moptWindow->locomotionManager->motionPlan;
	LocomotionEngine *engine = moptWindow->locomotionManager->locomotionEngine;

	dVector p, m;
	motionPlan->writeMPParametersToList(m);
	GET_PARAMETERS(prd, p);
	computeDmdp();

	dVector dp(p.size()), p1(p.size());
	dVector m1(m.size()), m_(m.size());
	for (int i = 0; i < p.size(); i++)
		dp[i] = 1e-3;

	double step = 1.0, energy;
	for (int T = 1; T < 5; T++) {
		p1 = p + dp * step;
		m1 = m + dmdp * dp;
		SET_PARAMETERS(prd, p1, motionPlan);
		RUN_MOPT(energy, engine->converged);
		motionPlan->writeMPParametersToList(m_);
		motionPlan->setMPParametersFromList(m);

		//Compare m1 and m_
		std::stringstream ss;
		ss << "../out/approx" << T << ".csv";
		FILE *fp = fopen(ss.str().c_str(), "w");

		fprintf(fp, "m1,m1real\n");
		fprintf(fp, "step,%lf\n", step);
		for (int i = 0; i < m.size(); i++)
			fprintf(fp, "%lf,%lf\n", m1[i], m_[i]);

		fclose(fp);

		step *= 2.0;
	}

	SET_PARAMETERS(prd, p, motionPlan);
}

void TestAppRobotDesignOptimization::runDOPTStep() {
	//must first compute dm/dp, where m are motion parameters, p are design parameters stored in the prd:
	//partial g / partial p + partial g / partial m * dm/dp = dg/dp = 0
	//so dm/dp = -partial g / partial m ^ -1 * partial g / partial p

	LocomotionEngine *engine = moptWindow->locomotionManager->locomotionEngine;
	LocomotionEngineMotionPlan *motionPlan = moptWindow->locomotionManager->motionPlan;

	double energy = DBL_MAX;

	//Beichen Li: run MOPT steps till convergence
	RUN_MOPT(energy, engine->converged);

	Logger::consolePrint("Energy: %lf\n", energy);

	dVector m;
	motionPlan->writeMPParametersToList(m);
	dVector p;
	GET_PARAMETERS(prd, p);

	dVector dfdp;
	dfdp.resize(p.size());
	
	addGradientOfDesignParametersTo(dfdp, p);

	double len = dfdp.norm();
	Logger::consolePrint("dfdp norm: %lf\n", len);

	//print("../out/dfdp.txt", dfdp);

	//Beichen Li: here we need to verify the result of dfdp somehow
	if (designCheck) {
		dVector dfdp_FD;
		resize(dfdp_FD, p.size());
		double e_p, e_m;

		double dp = 1e-4;
		for (int i = 0; i < p.size(); i++) {
			double pVal = p[i];
			p[i] = pVal + dp;
			SET_PARAMETERS(prd, p, motionPlan);
			RUN_MOPT(energy, engine->converged);
			motionPlan->setMPParametersFromList(m);

			p[i] = pVal - dp;
			SET_PARAMETERS(prd, p, motionPlan);
			RUN_MOPT(energy, engine->converged);
			motionPlan->setMPParametersFromList(m);

			p[i] = pVal;
			SET_PARAMETERS(prd, p, motionPlan);

			dfdp_FD[i] = (e_p - e_m) / (2 * dp);
		}

		dVector err = dfdp_FD - dfdp;

		if (err.norm() >= 1e-3) {
			Logger::consolePrint("Warning: dfdp validity check failed\n");
			print("../out/dfdp.txt", dfdp);
			print("../out/dfdp_FD.txt", dfdp_FD);
			return;
		}
	}

	if (len > 0.1)
	dfdp = dfdp / len * 0.1;

	for (int i = 0; i < p.size(); i++)
	p[i] -= dfdp[i];

	SET_PARAMETERS(prd, p, motionPlan);
	dVectorToParametersList(p, designParameters);

	logParameters("dfdp", dfdp);
	logParameters("p", p);

	//Beichen Li: restore the regularizer value
	engine->energyFunction->resetRegularizer();
}

double TestAppRobotDesignOptimization::operator () (const dVector& p, dVector& grad, bool saveFlag) {
	LocomotionEngine *engine = moptWindow->locomotionManager->locomotionEngine;
	LocomotionEngineMotionPlan *motionPlan = moptWindow->locomotionManager->motionPlan;
	LocomotionEngine_EnergyFunction *energyFunction = engine->energyFunction;

	dVector p0;
	GET_PARAMETERS(prd, p0);
	
	//Beichen Li: Save current parameters or load from saved copy
	if (saveFlag) {
		motionPlan->writeMPParametersToList(savedMotionParameters);
		updateRegularizingSolution(p0);
	}
	else
		motionPlan->setMPParametersFromList(savedMotionParameters);

	SET_PARAMETERS(prd, p, motionPlan);

	double energy = DBL_MAX;

	//Beichen Li: run MOPT steps till convergence
	RUN_MOPT(energy, engine->converged);

	if (energy < THRESHOLD)
		Logger::logPrint("Converged! Energy: %10.10lf\n", energy);
	else
		Logger::logPrint("Did not converge!\n");

	grad.resize(p.size());
	addGradientOfDesignParametersTo(grad, p);

	//If the energy diverges, we should set the original parameters back
	if (energy >= THRESHOLD) {
		motionPlan->setMPParametersFromList(savedMotionParameters);
		SET_PARAMETERS(prd, p0, motionPlan);
	}

	//Beichen Li: restore the regularizer value
	energyFunction->resetRegularizer();

	return energy + computeRegularizationTerm(p);
}

double TestAppRobotDesignOptimization::operator () (const dVector& p, const dVector& dp, dVector& grad, bool saveFlag, bool MOPTFlag) {
	//Warning: computeDmdp(p) should be called before this function call
	
	LocomotionEngine *engine = moptWindow->locomotionManager->locomotionEngine;
	LocomotionEngineMotionPlan *motionPlan = moptWindow->locomotionManager->motionPlan;
	LocomotionEngine_EnergyFunction *energyFunction = engine->energyFunction;

	if (saveFlag) {
		motionPlan->writeMPParametersToList(savedMotionParameters);
		updateRegularizingSolution(p);
	}
	else
		motionPlan->setMPParametersFromList(savedMotionParameters);

	dVector m, m_;
	motionPlan->writeMPParametersToList(m);
//	dVector p0
	dVector p1;
	p1 = p + dp;
//	GET_PARAMETERS(prd, p0);
	SET_PARAMETERS(prd, p1, motionPlan);

	m_ = m + dmdp * dp;
	motionPlan->setMPParametersFromList(m_);

	double energy = DBL_MAX;

	//Beichen Li: MOPTFlag decides whether to perform MOPT at current point
	//true - perform MOPT to get accurate value
	//false - use first order approximation
	//Profiler::startTimer("MOPT", "Evaluate");
	if (MOPTFlag) {
		for (int i = 0; i < 300; i++)
			RUN_MOPT(energy, engine->converged);
	}
	else
		energy = energyFunction->computeValue(m_);
	//Profiler::collectTimer("MOPT");

	double reg = computeRegularizationTerm(p1);

	if (energy < THRESHOLD)
		Logger::logPrint("Converged! Energy: %10.10lf, Reg: %lf\n", energy, reg);
	else
		Logger::logPrint("Did not converge!\n");

	//Profiler::startTimer("dfdp", "Evaluate");
	grad.resize(p.size());
	addGradientOfDesignParametersTo(grad, p1);
	//Profiler::collectTimer("dfdp");
	
	//Beichen Li: this was once a fail-safe for divergent situations but it can be controlled with saveFlag
//	if (energy >= THRESHOLD) {
//		motionPlan->setMPParametersFromList(m);
//		SET_PARAMETERS(prd, p0, motionPlan);
//	}

	energyFunction->resetRegularizer();

	return energy + reg;
}

void TestAppRobotDesignOptimization::addGradientOfDesignParametersTo(dVector& grad, const dVector& p_) {
	//Beichen Li: here f = objFunction, g = gradient of energyFunction
	LocomotionEngine_EnergyFunction *energyFunction = moptWindow->locomotionManager->locomotionEngine->energyFunction;
	LocomotionEngineMotionPlan *motionPlan = moptWindow->locomotionManager->motionPlan;
	
	dVector m;
	motionPlan->writeMPParametersToList(m);
	
	SparseMatrix dgdm;
	MatrixNxM dgdp;
	dVector g_m, g_p;
	dVector dfdm;
	dVector p = p_;

	resize(dgdm, m.size(), m.size());
	resize(dgdp, m.size(), p.size());
	resize(dfdm, m.size());

	energyFunction->addGradientTo(dfdm, m);

	DynamicArray<MTriplet> triplets;
	energyFunction->addHessianEntriesTo(triplets, m);
	dgdm.setFromTriplets(triplets.begin(), triplets.end());

	Eigen::SimplicialLDLT<SparseMatrix, Eigen::Lower> solver;
	solver.compute(dgdm);

	//Beichen Li: the adjoint method calculates lambda = -dgdm ^ -T * dfdm ^ T first
	dVector lambda;
	resize(lambda, m.size());
	lambda = solver.solve(-dfdm);

	double dp = 1e-4;
	//Beichen Li: the next step is to compute dgdp using finite differences
	for (int i = 0; i < p.size(); i++) {
		resize(g_m, m.size());
		resize(g_p, m.size());

		double pVal = p[i];
		p[i] = pVal + dp;
		SET_PARAMETERS(prd, p, motionPlan);
		energyFunction->addGradientTo(g_p, m);

		p[i] = pVal - dp;
		SET_PARAMETERS(prd, p, motionPlan);
		energyFunction->addGradientTo(g_m, m);

		p[i] = pVal;

		dgdp.col(i) = (g_p - g_m) / (2 * dp);
	}

	SET_PARAMETERS(prd, p, motionPlan);

	//Beichen Li: finally, compute dfdp with adjoint equation dfdp = lambda ^ T * dgdp + (partial) dfdp
	//f here is equal to original f (used in MOPT) plus a regularizing term expressed by 0.5 * (p - pLast) ^ 2
	dVector pardfdp;
	double f_m, f_p;
	resize(pardfdp, p.size());

	dp = 1e-4;
	for (int i = 0; i < p.size(); i++) {
		double pVal = p[i];
		p[i] = pVal + dp;
		SET_PARAMETERS(prd, p, motionPlan);
		f_p = energyFunction->computeValue(m) + computeRegularizationTerm(p);

		p[i] = pVal - dp;
		SET_PARAMETERS(prd, p, motionPlan);
		f_m = energyFunction->computeValue(m) + computeRegularizationTerm(p);

		p[i] = pVal;

		pardfdp[i] = (f_p - f_m) / (2 * dp);
	}

	SET_PARAMETERS(prd, p, motionPlan);

	grad = dgdp.transpose() * lambda + pardfdp;
}

void TestAppRobotDesignOptimization::computeDmdp() {
	LocomotionEngine *engine = moptWindow->locomotionManager->locomotionEngine;
	LocomotionEngineMotionPlan *motionPlan = moptWindow->locomotionManager->motionPlan;
	LocomotionEngine_EnergyFunction *energyFunction = engine->energyFunction;
	
	double energy;
	RUN_MOPT(energy, engine->converged);

	dVector m, p;
	motionPlan->writeMPParametersToList(m);
	GET_PARAMETERS(prd, p);

	SparseMatrix dgdm(m.size(), m.size());
	MatrixNxM dgdp(m.size(), p.size());
	resize(dmdp, m.size(), p.size());

	//Beichen Li: first compute dgdm aka Hessian of f
	DynamicArray<MTriplet> triplets;
	energyFunction->addHessianEntriesTo(triplets, m);
	dgdm.setFromTriplets(triplets.begin(), triplets.end());

	Eigen::SimplicialLDLT<SparseMatrix, Eigen::Lower> solver;
	solver.compute(dgdm);

	//Beichen Li: then compute dgdp using finite differences and solve for dmdp
	dVector g_p(m.size()), g_m(m.size()), dgdpi;

	double dp = 1e-4;
	for (int i = 0; i < p.size(); i++) {
		double pVal = p[i];
		p[i] = pVal + dp;
		SET_PARAMETERS(prd, p, motionPlan);
		energyFunction->addGradientTo(g_p, m);

		p[i] = pVal - dp;
		SET_PARAMETERS(prd, p, motionPlan);
		energyFunction->addGradientTo(g_m, m);

		p[i] = pVal;

		dgdpi = (g_p - g_m) / (2 * dp);

		dmdp.col(i) = -solver.solve(dgdpi);
	}

	SET_PARAMETERS(prd, p, motionPlan);

//	print("dmdp.txt", dmdp);
}

void TestAppRobotDesignOptimization::optimizeDesign() {
	LocomotionEngineMotionPlan *motionPlan = moptWindow->locomotionManager->motionPlan;

//	moptWindow->motionPlanTime = 0.0;

	Timer timer;
	timer.restart();
	
//	parallelTest();

//	Profiler::startTimer("DOPT", "");

	dVector pV;
	GET_PARAMETERS(prd, pV);

	//for (int i = 0; i < 20; i++)
	solver.minimize(*this, pV, currentEnergy);

//	Profiler::collectTimer("DOPT");

//	SET_PARAMETERS(prd, pV, moptWindow->locomotionManager->motionPlan);
	dVectorToParametersList(pV, designParameters);

	motionPlan->writeParamsToFile("../out/MPParams.p");

	Logger::consolePrint("Time Elapsed: %.3lfs\n", timer.timeEllapsed());
	Logger::consolePrint("Current Energy: %lf\n", currentEnergy);

	V3D v(motionPlan->COMTrajectory.getCOMPositionAtTimeIndex(0), motionPlan->COMTrajectory.getCOMPositionAtTimeIndex(motionPlan->nSamplePoints - 1));
	Logger::consolePrint("distance travelled: %lf %lf %lf\n", v.x(), v.y(), v.z());

//	Profiler::displayResults();
}

void TestAppRobotDesignOptimization::testGradient() {
	LocomotionEngine_EnergyFunction *energyFunction = moptWindow->locomotionManager->locomotionEngine->energyFunction;
	LocomotionEngineMotionPlan *motionPlan = moptWindow->locomotionManager->locomotionEngine->motionPlan;

	dVector m;
	motionPlan->writeMPParametersToList(m);
	energyFunction->setCurrentBestSolution(m);

	dVector grad;
	resize(grad, m.size());
	energyFunction->addGradientTo(grad, m);

	SparseMatrix hessian;
	resize(hessian, m.size(), m.size());

	DynamicArray<MTriplet> triplets;
	energyFunction->addHessianEntriesTo(triplets, m);
	hessian.setFromTriplets(triplets.begin(), triplets.end());

	Eigen::SimplicialLDLT<SparseMatrix, Eigen::Lower> solver;
	solver.compute(hessian);

	dVector dm;
	resize(dm, m.size());
	dm = solver.solve(-grad);

	logParameters("Gradient", grad);
	logParameters("dm", dm);
}
