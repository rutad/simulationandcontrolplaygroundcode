#pragma once

#include <GUILib/GLApplication.h>
#include <RobotDesignerLib/RobotDesignWindow.h>
#include <string>
#include <map>
#include <GUILib/TranslateWidget.h>
#include <RBSimLib/AbstractRBEngine.h>
#include <RBSimLib/WorldOracle.h>
#include <GUILib/GLWindow3D.h>
#include <GUILib/GLWindow3DWithMesh.h>
#include <GUILib/GLWindowContainer.h>
#include <RobotDesignerLib/LocomotionEngineMotionPlan.h>
#include <RobotDesignerLib/LocomotionEngine.h>
#include <RobotDesignerLib/FootFallPatternViewer.h>
#include <RobotDesignerLib/LocomotionEngineManagerGRF.h>
#include <RobotDesignerLib/LocomotionEngineManagerIP.h>
#include <GUILib/PlotWindow.h>
#include "TestAppDynamixel.h"

/**
* Robot Design and Simulation interface: Making dynamixel work with a wheel
*/
class TestAppRobotWheel : public TestAppDynamixel {
private:
	double amplitude1 = 0.4;
	double frequency = 1.0;

	double motionPhase = 0.0;

	DynamicArray<uint> dxID;

public:

	// constructor
	TestAppRobotWheel();
	// destructor
	virtual ~TestAppRobotWheel(void);
	// Run the App tasks
	virtual void process();
	// Restart the application.
	virtual void restart();
	//set dynamixel goals from target values
	virtual void sendControlInputsToPhysicalRobot();

	virtual void loadFile(const char* fName);
	virtual void zeroMotorPositions();
};
