#pragma once
#include <Utils/Utils.h>
#include <Utils/Timer.h>
#include <Utils/Logger.h>

//Beichen Li: profiler class used to monitor hotspot functions

class Profiler {
public:
	static DynamicArray<double> times;
	static DynamicArray<int> parents;
	static DynamicArray<std::string> descriptions;
	static DynamicArray<Timer> timers;

	static int searchByDescription(const std::string& description);
	static void startTimer(const std::string& description, const std::string& parent);
	static void collectTimer(const std::string& description);
	static void displayResults();
	static void reset();

private:
	//Beichen Li: no instance of this class should be created
	Profiler() {}
	~Profiler() {}
};
