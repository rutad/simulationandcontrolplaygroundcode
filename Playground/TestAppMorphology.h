#pragma once

#include <GUILib/GLApplication.h>
#include <RobotDesignerLib/RobotDesignWindow.h>
#include <string>
#include <map>
#include <GUILib/TranslateWidget.h>
#include <RBSimLib/AbstractRBEngine.h>
#include <RBSimLib/WorldOracle.h>
#include <GUILib/GLWindow3D.h>
#include <GUILib/GLWindow3DWithMesh.h>
#include <GUILib/GLWindowContainer.h>
#include <RobotDesignerLib/LocomotionEngineMotionPlan.h>
#include <RobotDesignerLib/LocomotionEngine.h>
#include <RobotDesignerLib/FootFallPatternViewer.h>
#include <RobotDesignerLib/LocomotionEngineManagerGRF.h>
#include <RobotDesignerLib/LocomotionEngineManagerIP.h>
#include <GUILib/PlotWindow.h>
#include <RobotDesignerLib/ModularDesignWindow.h>
#include <RobotDesignerLib/SpotMiniParamterizedDesign.h>
#include <RobotDesignerLib/MOPTWindow.h>
#include <ControlLib/IK_Solver.h>

/**
 * Robot Design and Simulation interface
 */
class TestAppMorphology : public GLApplication {
public:
	bool MORPH_OPT = false;
	bool MORPH_OPT_STEP = false;

public:
	MOPTWindow* moptWindow = NULL;
	AbstractRBEngine* rbEngine = NULL;
	Robot* robot = NULL;
	SpotMiniParameterizedDesign* prd = NULL;
	
	DynamicArray<double> designParameters;
	DynamicArray<string> designParameterNames;


	double groundKPScale = -5;
	double groundKdVal = 2500;

	bool drawMeshes = true, drawMOIs = false, drawCDPs = false, drawSkeletonView = false, drawJoints = false, drawContactForces = true, drawOrientation = true;
	double simTimeStep;
	bool drawMotionPlan = false;

	double motionPlanTime = 0;
	
	int animationCycle = 0;

	int mCount = 0;

	LocomotionEngineManager* locomotionManager = NULL;

	enum RUN_OPTIONS {
		MOTION_PLAN_OPTIMIZATION = 0,
		MOTION_PLAN_ANIMATION
	};
	int runOption = MOTION_PLAN_OPTIMIZATION;

public:
	// constructor
	TestAppMorphology();
	// destructor
	virtual ~TestAppMorphology(void);
	// Run the App tasks
	virtual void process();
	// Draw the App scene - camera transformations, lighting, shadows, reflections, etc apply to everything drawn by this method
	virtual void drawScene();
	// This is the wild west of drawing - things that want to ignore depth buffer, camera transformations, etc. Not pretty, quite hacky, but flexible. Individual apps should be careful with implementing this method. It always gets called right at the end of the draw function
	virtual void drawAuxiliarySceneInfo();
	// Restart the application.
	virtual void restart();

	void runMOPTStep();

	//input callbacks...

	//all these methods should returns true if the event is processed, false otherwise...
	//any time a physical key is pressed, this event will trigger. Useful for reading off special keys...
	virtual bool onKeyEvent(int key, int action, int mods);
	//this one gets triggered on UNICODE characters only...
	virtual bool onCharacterPressedEvent(int key, int mods);
	//triggered when mouse buttons are pressed
	virtual bool onMouseButtonEvent(int button, int action, int mods, double xPos, double yPos);
	//triggered when mouse moves
	virtual bool onMouseMoveEvent(double xPos, double yPos);
	//triggered when using the mouse wheel
	virtual bool onMouseWheelScrollEvent(double xOffset, double yOffset);

	virtual bool processCommandLine(const std::string& cmdLine);
	
	virtual void saveFile(const char* fName);
	virtual void loadFile(const char* fName);

	void warmStartMOPT();
	void loadToSim();

	void adjustWindowSize(int width, int height);
	virtual void setupLights();

	void addParameterizedDesignParametersToMenu();
	void removeParameterizedDesignParametersFromMenu();


	
};




