#include <GUILib/GLUtils.h>

/*
- finalize set of brackets for modular robot design, add maybe even a leg attachment...
- get velocity based control with acceleration profile working...
- finetune PD gains too, for position and velocity, and see how that affects things...
- do the motion/acceleration regularizer on the QP-based optimization
- motion plan draw should no longer be using animationCycle, but rather offset body movements since last time...
*/

#include "TestAppDynamixel.h"
#include <GUILib/GLMesh.h>
#include <GUILib/GLContentManager.h>
#include <MathLib/MathLib.h>
#include <RBSimLib/ODERBEngine.h>
#include <ControlLib/SimpleLimb.h>

////TODO: might be nice to set it up such that it works fine with different types of motors...
////TODO: might want to go through all the dynamixels and see which IDs are available, and never try to access any of the ones that are not...
//#define JOINT_RAD 0.025
//
//#define BAUDRATE       57600
//#define BROADCAST_ID	254
//
//#define TORQUE_ENABLE	64
//
////NOTE 1: all these values are based on the control table of the XM-series of Dynamixel actuators
////NOTE 2: careful, some of these use 2 bytes, others 4...
//#define ADDR_GOAL_CURRENT           102
//#define ADDR_GOAL_VELOCITY          104
//#define ADDR_GOAL_POSITION          116
//#define ADDR_VELOCITY_PROFILE       112
//#define ADDR_ACCELERATION_PROFILE   108
//#define ADDR_VELOCITY_LIMIT         44
//
//#define ADDR_PRESENT_POSITION       132
//#define ADDR_PRESENT_VELOCITY       128
//
//#define ADDR_VEL_P_GAIN				78
//#define ADDR_POS_D_GAIN				80
//#define ADDR_POS_P_GAIN				84
//#define ADDR_RETURN_DELAY_TIME		9
//
//#define ADDR_OPERATING_MODE				11
//#define DXL_TORQUE_CONTROL_MODE			0
//#define DXL_VELOCITY_CONTROL_MODE		1
//#define DXL_POSITION_CONTROL_MODE		3

void TW_CALL setJointAngle(const void *value, void *clientData) {
	if (!((TestAppDynamixel*)clientData)->selectedJoint)
		return;
	HingeJoint* hj = dynamic_cast<HingeJoint*>(((TestAppDynamixel*)clientData)->selectedJoint);
	if (!hj)
		return;
	double jointAngle = *(double*)value;
	ReducedRobotState rs(((TestAppDynamixel*)clientData)->robot);
	Quaternion q;
	q.setRotationFrom(jointAngle, (hj->rotationAxis));
	rs.setJointRelativeOrientation(q, hj->jIndex);
	((TestAppDynamixel*)clientData)->robot->setState(&rs);
}

void TW_CALL getJointAngle(void *value, void *clientData) {
	if (!((TestAppDynamixel*)clientData)->selectedJoint)
		return;
	HingeJoint* hj = dynamic_cast<HingeJoint*>(((TestAppDynamixel*)clientData)->selectedJoint);
	if (!hj)
		return;
	ReducedRobotState rs(((TestAppDynamixel*)clientData)->robot);
	Quaternion q = rs.getJointRelativeOrientation(hj->jIndex);
	*(double*)value = q.getRotationAngle(hj->rotationAxis);
}

void TW_CALL ConnectToDynamixels(void* clientData) {
	((TestAppDynamixel*)clientData)->connectToDynamixels();
}

void TW_CALL ToggleMotors(void* clientData) {
	((TestAppDynamixel*)clientData)->toggleMotors();
}

void TW_CALL GoToZero(void* clientData) {
	((TestAppDynamixel*)clientData)->zeroMotorPositions();
}

TestAppDynamixel::TestAppDynamixel(){
	setWindowTitle("DynamixelRobotControl");

	TwAddSeparator(mainMenuBar, "sep2", "");

	drawSkeletonView = false;
	showGroundPlane = false;

	TwAddVarRW(mainMenuBar, "DrawMeshes", TW_TYPE_BOOLCPP, &drawMeshes, " label='drawMeshes' group='Viz2'");
	TwAddVarRW(mainMenuBar, "DrawSkeletonView", TW_TYPE_BOOLCPP, &drawSkeletonView, " label='drawSkeleton' group='Viz2'");
	TwAddVarRW(mainMenuBar, "DrawJoints", TW_TYPE_BOOLCPP, &drawJoints, " label='drawJoints' group='Viz2'");

	TwAddSeparator(mainMenuBar, "sep3", "");

	TwAddVarRW(mainMenuBar, "DXL Port", TW_TYPE_STDSTRING, &DXLPortName, "");
	TwAddButton(mainMenuBar, "ConnectToDynamixels", ConnectToDynamixels, this, " label='ConnectToDynamixels' key='x' ");
	motorsOn = false;
	TwAddButton(mainMenuBar, "Motors:", ToggleMotors, this, " label='Motors: OFF' ");
	TwAddButton(mainMenuBar, "Go To Zero", GoToZero, this, " label='Go To Zero' key='z'");

	TwAddVarRW(mainMenuBar, "Vel P Gain", TW_TYPE_INT32, &velPGain, "min=0 max=32767 step=1");
	TwAddVarRW(mainMenuBar, "Pos P Gain", TW_TYPE_INT32, &posPGain, "min=0 max=32767 step=1");
	TwAddVarRW(mainMenuBar, "Pos D Gain", TW_TYPE_INT32, &posDGain, "min=0 max=32767 step=1");
	TwAddVarRW(mainMenuBar, "Return Delay Time", TW_TYPE_INT32, &returnDelayTime, "min=0 max=254 step=1");

	TwAddVarRW(mainMenuBar, "MotionScaleFactor", TW_TYPE_DOUBLE, &motionScaleFactor, "min=0 max=1 step=0.1");
	TwAddVarRW(mainMenuBar, "MotorVelocityScaleFactor", TW_TYPE_DOUBLE, &motorVelocityScaleFactor, "min=0 max=1.5 step=0.1");

	controlMode = CM_POSITION_ONLY;
	TwAddVarRW(mainMenuBar, "ControlMode", TwDefineEnumFromString("ControlMode", "..."), &controlMode, "");
	DynamicArray<std::string> controlOptionList;
	controlOptionList.push_back("\\Position Only");
	controlOptionList.push_back("\\Position and FF Velocity");
	controlOptionList.push_back("\\Position and FB Velocity");
	controlOptionList.push_back("\\FF Velocity Only");
	controlOptionList.push_back("\\FB Velocity Only");
	controlOptionList.push_back("\\Velocity and Acceleration");

	generateMenuEnumFromFileList("MainMenuBar/ControlMode", controlOptionList);

	runMode = ROBOT_CONTROL;
	TwAddVarRW(mainMenuBar, "RunMode", TwDefineEnumFromString("RunMode", "..."), &runMode, "");
	DynamicArray<std::string> runOptionList;
	runOptionList.push_back("\\SyncSimFromRobot");
	runOptionList.push_back("\\Animation");
	runOptionList.push_back("\\Simulation");
	runOptionList.push_back("\\Robot Control");
	generateMenuEnumFromFileList("MainMenuBar/RunMode", runOptionList);

	int size[2];
	int w, h;
	TwGetParam(mainMenuBar, NULL, "size", TW_PARAM_INT32, 2, size);
	w = (GLApplication::getMainWindowWidth()-size[0]);
	h = GLApplication::getMainWindowHeight();
	setViewportParameters(size[0], 0, w, h);
	consoleWindow->setViewportParameters(size[0], 0, w, 280);

//	loadFile("../data/rbs/starlETH.rbs");
//	loadFile("../data/rbs/tripod.rbs");
//	loadFile("../data/rbs/dinoV4.rbs");
//	loadFile("../data/rbs/cat.rbs");
//	loadFile("../data/rbs/gorilla.rbs");
//	loadFile("../data/rbs/babyRex.rbs");
//	loadFile("../data/rbs/quadruped.rbs");
//	loadFile("../data/rbs/scorpy.rbs");
//	loadFile("../data/rbs/dog.rbs");
//	loadFile("../data/rbs/tmp.rbs");

//	loadFile("../data/rbs/snakeMonster.rbs");
//	loadFile("../data/rbs/SehoonsPuppy.rbs");	

	loadFile("../data/rbs/robotArm1DOF.rbs");

	connectToDynamixels();

	plotWindow = new PlotWindow(260 * 1.14, mainWindowHeight - 400, 500, 400);
	plotWindow->createData("targetValues", 1, 0, 0);
	plotWindow->createData("motorValues", 0, 1, 0);
	plotWindow->setToFixedXRange(50);
}

TestAppDynamixel::~TestAppDynamixel(void){
}

void TestAppDynamixel::zeroMotorPositions() {
	if (!packetHandler || !portHandler)
		return;

	Logger::consolePrint("Zeroing actuator positions...\n");

	//it is assumed that motors are in position mode, and that they are on... make them go to zero... at some point we may want to slow them down...
	dxl_comm_result = packetHandler->write4ByteTxRx(portHandler, BROADCAST_ID, ADDR_VELOCITY_LIMIT, getDXLVelocityFromRPM(300), &dxl_error);
	dxl_comm_result = packetHandler->write4ByteTxRx(portHandler, BROADCAST_ID, ADDR_VELOCITY_PROFILE, 25, &dxl_error);
	dxl_comm_result = packetHandler->write4ByteTxRx(portHandler, BROADCAST_ID, ADDR_GOAL_POSITION, getDXLPositionFromRad(0), &dxl_error);

	//wait a while... 
//	Timer timer;
//	while (timer.timeEllapsed() < 0.5);
	dxl_comm_result = packetHandler->write4ByteTxRx(portHandler, BROADCAST_ID, ADDR_VELOCITY_PROFILE, 0, &dxl_error);
}

void TestAppDynamixel::toggleMotors() {
	motorsOn = !motorsOn;

	std::string newButtonName;

	if (motorsOn) {
		newButtonName = "MainMenuBar/Motors: label='Motors: ON'";
		if (packetHandler && portHandler)
			dxl_comm_result = packetHandler->write1ByteTxRx(portHandler, BROADCAST_ID, TORQUE_ENABLE, 1, &dxl_error);
	}
	else {
		newButtonName = "MainMenuBar/Motors: label='Motors: OFF'";

		if (packetHandler && portHandler)
			dxl_comm_result = packetHandler->write1ByteTxRx(portHandler, BROADCAST_ID, TORQUE_ENABLE, 0, &dxl_error);
	}
	TwDefine(newButtonName.c_str());

}

void TestAppDynamixel::connectToDynamixels() {
	if (portHandler != NULL)
		closePort();
	// Initialize PortHandler instance
	portHandler = dynamixel::PortHandler::getPortHandler(DXLPortName.c_str());

	// Open port
	if (portHandler->openPort()) {
		Logger::consolePrint("Succees in opening port: %s!\n", DXLPortName.c_str());
	}
	else {
		Logger::consolePrint("Failed to open port: %s!\n", DXLPortName.c_str());
		portHandler = NULL;
		return;
	}

	// Initialize PacketHandler instance
	packetHandler = dynamixel::PacketHandler::getPacketHandler(2.0F);

	if (portHandler->setBaudRate(BAUDRATE)) {
		Logger::consolePrint("Set baud rate to: %d!\n", BAUDRATE);
	}else {
		Logger::consolePrint("Could not set baud rate...\n");
		return;
	}

/*
	Logger::consolePrint("Discovering actuators...\n");
	std::vector<uint8_t> vec;

	dxl_comm_result = packetHandler->broadcastPing(portHandler, vec);

	timer.restart();
	while (timer.timeEllapsed() < 1);

	for (int i = 0; i < (int)vec.size(); i++){
		Logger::consolePrint("Detected dynamixel with id: %03d\n", vec.at(i));
//		dxl_comm_result = packetHandler->reboot(portHandler, vec.at(i), &dxl_error);
	}
*/

	//can only set operating mode when motors are off
	motorsOn = true;
	toggleMotors();
	dxl_comm_result = packetHandler->write1ByteTxRx(portHandler, BROADCAST_ID, ADDR_OPERATING_MODE, DXL_POSITION_CONTROL_MODE, &dxl_error);

	//now turn the motors on and zero their positions...
	toggleMotors();

	zeroMotorPositions();

//	closePort();
}

void TestAppDynamixel::closePort() {
	//turn off the motors...
	if (packetHandler && portHandler){
		motorsOn = true;
		toggleMotors();
	}

	// Close port
	if (portHandler)
		portHandler->closePort();
	portHandler = NULL;
	packetHandler = NULL;
}

//motors may be assembled in a different way than they are modeled, so account for that...
void TestAppDynamixel::flipMotorTargetsIfNeeded() {
	for (int i = 0; i < robot->getJointCount(); i++) {
		HingeJoint* hj = dynamic_cast<HingeJoint*>(robot->getJoint(i));
		if (!hj) continue;
		if (hj->dynamixelProperties.flipMotorAxis) {
			hj->dynamixelProperties.targetMotorAngle *= -1;
			hj->dynamixelProperties.targetMotorVelocity *= -1;
			hj->dynamixelProperties.targetMotorAcceleration *= -1;
		}
	}
}


void TestAppDynamixel::sendControlInputsToPhysicalRobot() {
	if (!packetHandler || !portHandler)
		return;

	if (lastControlMode != controlMode) {
		bool motorsToggled = false;
		if (motorsOn){
			toggleMotors();
			motorsToggled = true;
		}

		if (controlMode == CM_POSITION_ONLY || controlMode == CM_POSITION_AND_FB_VELOCITY || controlMode == CM_POSITION_AND_FF_VELOCITY)
			dxl_comm_result = packetHandler->write1ByteTxRx(portHandler, BROADCAST_ID, ADDR_OPERATING_MODE, DXL_POSITION_CONTROL_MODE, &dxl_error);
		else if (controlMode == CM_VELOCITY_AND_ACCELERATION || controlMode == CM_FF_VELOCITY_ONLY || controlMode == CM_FB_VELOCITY_ONLY)
			dxl_comm_result = packetHandler->write1ByteTxRx(portHandler, BROADCAST_ID, ADDR_OPERATING_MODE, DXL_VELOCITY_CONTROL_MODE, &dxl_error);
		
		if (motorsToggled)
			toggleMotors();

		lastControlMode = controlMode;
	}
	if (velPGain != lastVelPGain) {
		dxl_comm_result = packetHandler->write2ByteTxRx(portHandler, BROADCAST_ID, ADDR_VEL_P_GAIN, (uint16_t)velPGain, &dxl_error);
		lastVelPGain = velPGain;
	}
	if (posPGain != lastPosPGain) {
		dxl_comm_result = packetHandler->write2ByteTxRx(portHandler, BROADCAST_ID, ADDR_POS_P_GAIN, (uint16_t)posPGain, &dxl_error);
		lastPosPGain = posPGain;
	}
	if (posDGain != lastPosDGain) {
		dxl_comm_result = packetHandler->write2ByteTxRx(portHandler, BROADCAST_ID, ADDR_POS_D_GAIN, (uint16_t)posDGain, &dxl_error);
		lastPosDGain = posDGain;
	}

	if (returnDelayTime != lastReturnDelayTime) {
		dxl_comm_result = packetHandler->write1ByteTxRx(portHandler, BROADCAST_ID, ADDR_RETURN_DELAY_TIME, (uint8_t)returnDelayTime, &dxl_error);
		lastReturnDelayTime = returnDelayTime;
	}

	//TODO: change the delay time, change the pos KP/KD and the velocity P gains

	flipMotorTargetsIfNeeded();


	//read off the DXL values and write them to the motors...
	//for (int i = 0; i < robot->getJointCount(); i++) {
	//	HingeJoint* hj = dynamic_cast<HingeJoint*>(robot->getJoint(i));
	//	if (!hj) continue;
	//	int dxlGoalPos = getDXLPositionFromRad(hj->dynamixelProperties.targetMotorAngle);

	//	if (hj->dynamixelProperties.dxl_id >= 0) {
	//		Logger::consolePrint("Motor %d should go to %d\n", hj->dynamixelProperties.dxl_id, dxlGoalPos);
	////		dxl_comm_result = packetHandler->write4ByteTxRx(portHandler, hj->dynamixelProperties.dxl_id, ADDR_GOAL_POSITION, dxlGoalPos, &dxl_error);
	//	}
	//}

	uint8_t tmp_param[4];

	if (controlMode == CM_POSITION_ONLY || controlMode == CM_POSITION_AND_FF_VELOCITY || controlMode == CM_POSITION_AND_FB_VELOCITY) {
		//write out target positions...
		dynamixel::GroupSyncWrite posGroupSyncWriter = dynamixel::GroupSyncWrite(portHandler, packetHandler, ADDR_GOAL_POSITION, 4);
		//write out velocity limits, based on how much each joint needs to move - using a rectangular velocity profile...
		dynamixel::GroupSyncWrite velGroupSyncWriter = dynamixel::GroupSyncWrite(portHandler, packetHandler, ADDR_VELOCITY_PROFILE, 4);

		for (int i = 0; i < robot->getJointCount(); i++) {
			HingeJoint* hj = dynamic_cast<HingeJoint*>(robot->getJoint(i));
			if (!hj) continue;
			int dxlGoalPos = getDXLPositionFromRad(hj->dynamixelProperties.targetMotorAngle * motionScaleFactor);

			if (hj->dynamixelProperties.dxl_id >= 0)
				posGroupSyncWriter.addParam(hj->dynamixelProperties.dxl_id, DXL_INT32_TO_UINT8_ARRAY(dxlGoalPos, tmp_param));

			int dxlGoalVel = getDXLVelocityFromRadPerSec(fabs(hj->dynamixelProperties.targetMotorVelocity) * motionScaleFactor);
			if (dxlGoalVel == 0) dxlGoalVel = 1; // 0 means no limit, which we shouldn't do if velocities are controlled at all...
			if (controlMode == CM_POSITION_ONLY) dxlGoalVel = 0; // 0 means no speed limit, which is what we want in position control mode...

//			Logger::consolePrint("target velocity: %lf, goal dxl vel: %d\n", hj->dynamixelProperties.targetMotorVelocity, dxlGoalVel);
			if (hj->dynamixelProperties.dxl_id >= 0)
				velGroupSyncWriter.addParam(hj->dynamixelProperties.dxl_id, DXL_INT32_TO_UINT8_ARRAY(dxlGoalVel, tmp_param));
		}
		//sync write velocity limits...
//		dxl_comm_result = velGroupSyncWriter.txPacket();
		if (dxl_comm_result != COMM_SUCCESS) {
			packetHandler->printTxRxResult(dxl_comm_result);
			Logger::consolePrint("Could not send groupRead packet (velGroupSyncWriter). This could happen if one attempts to read from IDs that do not exist on the robot side...\n");
		}

		//sync write goal positions...
		dxl_comm_result = posGroupSyncWriter.txPacket();
		if (dxl_comm_result != COMM_SUCCESS) {
			packetHandler->printTxRxResult(dxl_comm_result);
			Logger::consolePrint("Could not send groupRead packet (posGroupSyncWriter). This could happen if one attempts to read from IDs that do not exist on the robot side...\n");
		}
	}

	if (controlMode == CM_VELOCITY_AND_ACCELERATION || controlMode == CM_FF_VELOCITY_ONLY || controlMode == CM_FB_VELOCITY_ONLY) {
		//write out target positions...
		dynamixel::GroupSyncWrite velGroupSyncWriter = dynamixel::GroupSyncWrite(portHandler, packetHandler, ADDR_GOAL_VELOCITY, 4);
		//write out velocity limits, based on how much each joint needs to move - using a rectangular velocity profile...
		dynamixel::GroupSyncWrite accGroupSyncWriter = dynamixel::GroupSyncWrite(portHandler, packetHandler, ADDR_ACCELERATION_PROFILE, 4);

		for (int i = 0; i < robot->getJointCount(); i++) {
			HingeJoint* hj = dynamic_cast<HingeJoint*>(robot->getJoint(i));
			if (!hj) continue;
			int dxlGoalVel = getDXLVelocityFromRadPerSec(hj->dynamixelProperties.targetMotorVelocity * motionScaleFactor);

			if (hj->dynamixelProperties.dxl_id >= 0)
				velGroupSyncWriter.addParam(hj->dynamixelProperties.dxl_id, DXL_INT32_TO_UINT8_ARRAY(dxlGoalVel, tmp_param));

			int dxlGoalAcc = getDXLAccelFromRadPerSecSquared(fabs(hj->dynamixelProperties.targetMotorAcceleration) * motionScaleFactor);
			if (dxlGoalAcc == 0) dxlGoalAcc = 1; // 0 means no limit, which we shouldn't do if accelerations are controlled at all...
			if (controlMode == CM_FF_VELOCITY_ONLY || controlMode == CM_FB_VELOCITY_ONLY) dxlGoalAcc = 0; // 0 means no speed limit, which is what we want in position control mode...
			//Logger::consolePrint("target velocity: %lf, goal dxl vel: %d\n", hj->dynamixelProperties.targetMotorVelocity, dxlGoalVel);
			if (hj->dynamixelProperties.dxl_id >= 0)
				accGroupSyncWriter.addParam(hj->dynamixelProperties.dxl_id, DXL_INT32_TO_UINT8_ARRAY(dxlGoalAcc, tmp_param));
		}
		//sync write velocity limits...
		dxl_comm_result = accGroupSyncWriter.txPacket();
		if (dxl_comm_result != COMM_SUCCESS) {
			packetHandler->printTxRxResult(dxl_comm_result);
			Logger::consolePrint("Could not send groupRead packet (accGroupSyncWriter). This could happen if one attempts to read from IDs that do not exist on the robot side...\n");
		}

		//sync write goal positions...
		dxl_comm_result = velGroupSyncWriter.txPacket();
		if (dxl_comm_result != COMM_SUCCESS) {
			packetHandler->printTxRxResult(dxl_comm_result);
			Logger::consolePrint("Could not send groupRead packet (velGroupSyncWriter). This could happen if one attempts to read from IDs that do not exist on the robot side...\n");
		}
	}

	flipMotorTargetsIfNeeded();
}

void TestAppDynamixel::readPhysicalRobotMotorVelocities() {
	if (!packetHandler || !portHandler)
		return;
	dynamixel::GroupSyncRead velGroupSyncReader = dynamixel::GroupSyncRead(portHandler, packetHandler, ADDR_PRESENT_VELOCITY, 4);

	for (int i = 0; i < robot->getJointCount(); i++) {
		HingeJoint* hj = dynamic_cast<HingeJoint*>(robot->getJoint(i));
		if (!hj) continue;

		if (hj->dynamixelProperties.dxl_id >= 0)
			velGroupSyncReader.addParam(hj->dynamixelProperties.dxl_id);
	}

	dxl_comm_result = velGroupSyncReader.txRxPacket();
	if (dxl_comm_result != COMM_SUCCESS) {
		packetHandler->printTxRxResult(dxl_comm_result);
		Logger::consolePrint("Could not send groupRead (vel) packet. This could happen if one attempts to read from IDs that do not exist on the robot side...\n");
	}

	for (int i = 0; i < robot->getJointCount(); i++) {
		HingeJoint* hj = dynamic_cast<HingeJoint*>(robot->getJoint(i));
		if (!hj) continue;

		if (hj->dynamixelProperties.dxl_id >= 0) {
			if (velGroupSyncReader.isAvailable(hj->dynamixelProperties.dxl_id, ADDR_PRESENT_VELOCITY, 4)) {
				int32_t present_vel = velGroupSyncReader.getData(hj->dynamixelProperties.dxl_id, ADDR_PRESENT_VELOCITY, 4);
				hj->dynamixelProperties.currentMotorVelocity = getVelRadPerSecFromDXLVelocity(present_vel);
				//Logger::consolePrint("DXL: %d has value: %d\n", hj->dynamixelProperties.dxl_id, present_position);
			}
			else {
				Logger::consolePrint("No data available for DXL %d\n", hj->dynamixelProperties.dxl_id);
			}
		}
	}
}

void TestAppDynamixel::readPhysicalRobotMotorPositions() {
	if (!packetHandler || !portHandler)
		return;
	//read off the motor values and write them up into the appropriate sim containers
/*
	for (int i = 0; i < robot->getJointCount(); i++) {
	HingeJoint* hj = dynamic_cast<HingeJoint*>(robot->getJoint(i));
	if (!hj) continue;
	uint32_t present_position = 0;

	dxl_comm_result = packetHandler->read4ByteTxRx(portHandler, hj->dynamixelProperties.dxl_id, ADDR_PRESENT_POSITION, &present_position, &dxl_error);
//	Logger::consolePrint("DXL: %d has value: %d\n", hj->dynamixelProperties.dxl_id, present_position);

	hj->dynamixelProperties.physicalMotorCurrentAngle = getAngleRadFromDXLPosition(present_position);
	}
*/

// Initialize Groupsyncread instance for Present Position
	dynamixel::GroupSyncRead posGroupSyncReader = dynamixel::GroupSyncRead(portHandler, packetHandler, ADDR_PRESENT_POSITION, 4);

	for (int i = 0; i < robot->getJointCount(); i++) {
		HingeJoint* hj = dynamic_cast<HingeJoint*>(robot->getJoint(i));
		if (!hj) continue;

		if (hj->dynamixelProperties.dxl_id >= 0)
			posGroupSyncReader.addParam(hj->dynamixelProperties.dxl_id);
	}

	dxl_comm_result = posGroupSyncReader.txRxPacket();
	if (dxl_comm_result != COMM_SUCCESS) {
		packetHandler->printTxRxResult(dxl_comm_result);
		Logger::consolePrint("Could not send groupRead packet. This could happen if one attempts to read from IDs that do not exist on the robot side...\n");
	}

	for (int i = 0; i < robot->getJointCount(); i++) {
		HingeJoint* hj = dynamic_cast<HingeJoint*>(robot->getJoint(i));
		if (!hj) continue;

		if (hj->dynamixelProperties.dxl_id >= 0) {
			if (posGroupSyncReader.isAvailable(hj->dynamixelProperties.dxl_id, ADDR_PRESENT_POSITION, 4)) {
				int32_t present_position = posGroupSyncReader.getData(hj->dynamixelProperties.dxl_id, ADDR_PRESENT_POSITION, 4);
				hj->dynamixelProperties.currentMotorAngle = getAngleRadFromDXLPosition(present_position);
//				Logger::consolePrint("DXL: %d has value: %d\n", hj->dynamixelProperties.dxl_id, present_position);
			}
			else {
				Logger::consolePrint("No data available for DXL %d\n", hj->dynamixelProperties.dxl_id);
			}
		}
	}
}

void TestAppDynamixel::syncSimulationWithPhysicalRobot() {
	readPhysicalRobotMotorPositions();
	setSimRobotStateFromCurrentDXLMotorValues();
}

void TestAppDynamixel::setSimRobotStateFromCurrentDXLMotorValues() {
	//given the values stored in the joint's dxl properties structure (which are updated either from the menu or by sync'ing with the dynamixels), update the state of the robot... 
	ReducedRobotState rs(robot);

	for (int i = 0; i < robot->getJointCount();i++){
		HingeJoint* hj = dynamic_cast<HingeJoint*>(robot->getJoint(i));
		if (!hj) continue;
		Quaternion q;
		q.setRotationFrom(hj->dynamixelProperties.currentMotorAngle, hj->rotationAxis);
		rs.setJointRelativeOrientation(q, i);
	}
	robot->setState(&rs);
}

void TestAppDynamixel::setDXLTargetMotorValuesFromSimRobotState() {
	//given the values stored in the joint's dxl properties structure (which are updated either from the menu or by sync'ing with the dynamixels), update the state of the robot... 
	ReducedRobotState rs(robot);

	for (int i = 0; i < robot->getJointCount(); i++) {
		HingeJoint* hj = dynamic_cast<HingeJoint*>(robot->getJoint(i));
		if (!hj) continue;
		Quaternion q = rs.getJointRelativeOrientation(i);
		V3D w = rs.getJointRelativeAngVelocity(i);
		hj->dynamixelProperties.targetMotorAngle = q.getRotationAngle(hj->rotationAxis);
		hj->dynamixelProperties.targetMotorVelocity = w.dot(hj->rotationAxis);
	}
}


void TestAppDynamixel::loadMenuParametersFor(Joint* joint) {
	std::string objName = std::string("") + joint->name;
	TwAddButton(mainMenuBar, objName.c_str(), NULL, NULL, " ");

	HingeJoint* hj = dynamic_cast<HingeJoint*>(joint);
	if (!hj) return;

	TwAddSeparator(mainMenuBar, "DXLSep","");
	TwAddVarRW(mainMenuBar, "dxl_ID", TW_TYPE_INT32, &hj->dynamixelProperties.dxl_id, "");
	TwAddVarRW(mainMenuBar, "flipMotorAxis", TW_TYPE_BOOLCPP, &hj->dynamixelProperties.flipMotorAxis, "");


	//	TwAddVarRW(mainMenuBar, "motorAngle", TW_TYPE_DOUBLE, &hj->dynamixelProperties.motorAngle, "step=0.01");
	TwAddVarCB(mainMenuBar, "motorAngle", TW_TYPE_DOUBLE, setJointAngle, getJointAngle, this, "step=0.05");
}

void TestAppDynamixel::unloadMenuParametersFor(Joint* joint) {
	std::string objName = std::string("") + joint->name;
	TwRemoveVar(mainMenuBar, objName.c_str());

	HingeJoint* hj = dynamic_cast<HingeJoint*>(joint);
	if (!hj) return;

	TwRemoveVar(mainMenuBar, "DXLSep");
	TwRemoveVar(mainMenuBar, "dxl_ID");
	TwRemoveVar(mainMenuBar, "flipMotorAxis");
	TwRemoveVar(mainMenuBar, "motorAngle");
}


//triggered when mouse moves
bool TestAppDynamixel::onMouseMoveEvent(double xPos, double yPos) {

	Ray clickedRay = getRayFromScreenCoords(xPos, yPos);
	bool processed = false;

	//check if we're hovering over anything important here...
	if (!processed && GlobalMouseState::lButtonPressed == false && GlobalMouseState::rButtonPressed == false && GlobalMouseState::mButtonPressed == false) {
		//we should only have one thing highlighted at one time - so un-highlight everything...
		double highlightedJointDist = DBL_MAX;
		highlightedJoint = NULL;

		for (uint i = 0; i < robot->jointList.size(); i++) {
			P3D closestPointOnRay;
			if (clickedRay.getDistanceToPoint(robot->getJoint(i)->getWorldPosition(), &closestPointOnRay) < JOINT_RAD) {
				double distAlongRay = clickedRay.getRayParameterFor(closestPointOnRay);
				if (distAlongRay < highlightedJointDist) {
					highlightedJoint = robot->getJoint(i);
					highlightedJointDist = distAlongRay;
					processed = true;
				}
			}
		}
	}

	if (processed) return true;

	if (GLApplication::onMouseMoveEvent(xPos, yPos)) return true;

	return false;
}

//triggered when mouse buttons are pressed
bool TestAppDynamixel::onMouseButtonEvent(int button, int action, int mods, double xPos, double yPos) {

	Joint* currentlySelectedJoint = selectedJoint;

	//if any joint is being hovered over and we click the mouse button, we should invert its selection
	if (GlobalMouseState::lButtonPressed == true) {
		//change the selection of the joint that is highlighted
		if (mods & GLFW_MOD_CONTROL) {
			selectedJoint = NULL;
		}
		if (highlightedJoint != NULL){
			if (highlightedJoint == selectedJoint) {
				selectedJoint = NULL;
			}
			else {
				selectedJoint = highlightedJoint;
			}
		}

		if (selectedJoint != currentlySelectedJoint) {
			if (currentlySelectedJoint)
				unloadMenuParametersFor(currentlySelectedJoint);
			if (selectedJoint)
				loadMenuParametersFor(selectedJoint);
		}	
		if (highlightedJoint)
			return true;
	}

	if (GLApplication::onMouseButtonEvent(button, action, mods, xPos, yPos)) return true;

	return false;
}

//triggered when using the mouse wheel
bool TestAppDynamixel::onMouseWheelScrollEvent(double xOffset, double yOffset) {

	if (GLApplication::onMouseWheelScrollEvent(xOffset, yOffset)) return true;

	return false;
}

bool TestAppDynamixel::onKeyEvent(int key, int action, int mods) {
	if (key == GLFW_KEY_ESCAPE) {
		closePort();
		//let the main app handle the rest of the shutting down process...
	}


	if (GLApplication::onKeyEvent(key, action, mods)) return true;

	return false;
}

bool TestAppDynamixel::onCharacterPressedEvent(int key, int mods) {
	if (showMenus)
		if (TwEventCharGLFW(TwConvertKeyGLFW3to2(key), GLFW_PRESS))
			return true;
	if (key >= '1' && key <= '1' + ROBOT_CONTROL) {
		runMode = key - '1';
		return true;
	}

	if (GLApplication::onCharacterPressedEvent(key, mods)) return true;
	return false;
}

void TestAppDynamixel::loadFile(const char* fName) {
	Logger::consolePrint("Loading file \'%s\'...\n", fName);
	std::string fileName;
	fileName.assign(fName);

	std::string fNameExt = fileName.substr(fileName.find_last_of('.') + 1);

	if (fNameExt.compare("rbs") == 0) {
		delete robot;
		delete rbEngine;

		rbEngine = new ODERBEngine();
		RobotDesign* robotDesign = new RobotDesign();
		robotDesign->readRobotFromFile(fName);
		robotDesign->saveRobotToFile("../out/tmpRobot.rbs");
		rbEngine->loadRBsFromFile("../out/tmpRobot.rbs");
		robotDesign->transferMeshes(rbEngine->rbs);
		delete robotDesign;
		robot = new Robot(rbEngine->rbs[0]);
	}

}

void TestAppDynamixel::saveFile(const char* fName) {
	Logger::consolePrint("SAVE FILE: Do not know what to do with file \'%s\'\n", fName);
}

// Run the App tasks
void TestAppDynamixel::process() {
	if (runMode == SYNC_SIM_FROM_ROBOT_STATE)
		syncSimulationWithPhysicalRobot();
	else if (runMode == ROBOT_CONTROL) {
		//how the target robot state is computed is left up to classes that extend this one...
		setDXLTargetMotorValuesFromSimRobotState();
		sendControlInputsToPhysicalRobot();
	}
}

/*
Control modes:
- position only
- position and velocity (FF)
- position and velocity (FB)
- velocity only (FF)
- velocity only (FB)

- which one can track best?
- which one is smoothest?

- need a better way of computing FF velocities: (e.g. pos(t) - pos(t+1)/dt))
- need to see what is up with the timing. I think the motors will have 1/10s to run before they get commands next, but do they really? How far off are we?

*/

void TestAppDynamixel::reportTrackingErrors(bool updateCurrentMotorPositions, bool updateCurrentMotorVelocities) {
	if (updateCurrentMotorPositions)
		readPhysicalRobotMotorPositions();
	if (updateCurrentMotorVelocities)
		readPhysicalRobotMotorVelocities();

	bool first = true;
	//check tracking errors - this method should be called once motor pos/vels have been read, but before new target values are computed. This way we will be able to see how far away we are from what we asked for...
	for (int i = 0; i < robot->getJointCount(); i++) {
		HingeJoint* hj = dynamic_cast<HingeJoint*>(robot->getJoint(i));
		if (!hj) continue;
		if (hj->dynamixelProperties.dxl_id < 0) continue;
		if (first) {
			plotWindow->addDataPoint(1, hj->dynamixelProperties.currentMotorAngle);
			plotWindow->addDataPoint(0, hj->dynamixelProperties.targetMotorAngle);
			first = false;
		}
		Logger::logPrint("%d:\t%lf\t%lf\t%lf\t%lf\t%lf\t", i, hj->dynamixelProperties.currentMotorAngle, hj->dynamixelProperties.targetMotorAngle, hj->dynamixelProperties.currentMotorAngle - hj->dynamixelProperties.targetMotorAngle, hj->dynamixelProperties.currentMotorVelocity, hj->dynamixelProperties.targetMotorVelocity);
	}
	Logger::logPrint("\n");
}

//based on the state of the robot, this method will compute desired motor positions and velocities...
void TestAppDynamixel::computeDXLControlInputsBasedOnSimRobot(double dt) {
	//we'll need to read off the current robot motor positions. However, if they are not available, we might as well just start from the last set of motor positions we set...
	for (int i = 0; i < robot->getJointCount(); i++) {
		HingeJoint* hj = dynamic_cast<HingeJoint*>(robot->getJoint(i));
		if (!hj) continue;
		hj->dynamixelProperties.currentMotorAngle = hj->dynamixelProperties.targetMotorAngle;
		hj->dynamixelProperties.currentMotorVelocity = hj->dynamixelProperties.targetMotorVelocity;
	}

	//read current motor velocities and positions...
//	readPhysicalRobotMotorPositions();
//	readPhysicalRobotMotorVelocities();
//	reportTrackingErrors(false, false);

	//now compute the new targets - these include the position of each motor, as well as feed-forward target velocities...
	setDXLTargetMotorValuesFromSimRobotState();

	if (controlMode == CM_POSITION_AND_FB_VELOCITY || controlMode == CM_FB_VELOCITY_ONLY) {
		//now, based on this info and the time allowed to reach the target, compute velocities as well. If the motors are able to go at this speed, then the targets will be met...
		for (int i = 0; i < robot->getJointCount(); i++) {
			HingeJoint* hj = dynamic_cast<HingeJoint*>(robot->getJoint(i));
			if (!hj) continue;
			hj->dynamixelProperties.targetMotorVelocity = (hj->dynamixelProperties.targetMotorAngle - hj->dynamixelProperties.currentMotorAngle) / dt * motorVelocityScaleFactor;
		}
	}

	if (controlMode == CM_VELOCITY_AND_ACCELERATION) {
		//now, based on this info and the time allowed to reach the target, compute velocities as well. If the motors are able to go at this speed, then the targets will be met...
		for (int i = 0; i < robot->getJointCount(); i++) {
			HingeJoint* hj = dynamic_cast<HingeJoint*>(robot->getJoint(i));
			if (!hj) continue;

			//we want the motor to reach its target position at the end of the time step, while going with constant velocity from where it's at
			//x(t) = x0 + v0 * t + 1/2 * a * t*t ==> a = 2 * (x(t) - x0 - v0 * t) / (t * t)
			//v(t) = v0 + a*t
			hj->dynamixelProperties.targetMotorAcceleration = 2 * (hj->dynamixelProperties.targetMotorAngle - hj->dynamixelProperties.currentMotorAngle - dt * hj->dynamixelProperties.currentMotorVelocity) / (dt * dt);
			hj->dynamixelProperties.targetMotorVelocity = hj->dynamixelProperties.currentMotorVelocity + hj->dynamixelProperties.targetMotorAcceleration * dt;
		}
	}
}


// Draw the App scene - camera transformations, lighting, shadows, reflections, etc apply to everything drawn by this method
void TestAppDynamixel::drawScene() {
	glColor3d(1, 1, 1);

	int flags = 0;
	if (drawMeshes) flags |= SHOW_MESH | SHOW_MATERIALS;
	if (drawSkeletonView) flags |= SHOW_BODY_FRAME | SHOW_ABSTRACT_VIEW;
	if (drawJoints) flags |= SHOW_JOINTS;

	glEnable(GL_LIGHTING);
	rbEngine->drawRBs(flags);

	if (selectedJoint) {
		glColor3d(0.0, 1.0, 0.0);
		drawSphere(selectedJoint->getWorldPosition(), JOINT_RAD, 12);
	}

	if (highlightedJoint) {
		glColor4d(1.0, 0.0, 0.0, 0.5);

		glEnable(GL_CULL_FACE);
		glCullFace(GL_FRONT);
		drawSphere(highlightedJoint->getWorldPosition(), JOINT_RAD * 1.1, 12);
		glCullFace(GL_BACK);
		drawSphere(highlightedJoint->getWorldPosition(), JOINT_RAD * 1.1, 12);
		glDisable(GL_CULL_FACE);
	
	}

	glDisable(GL_LIGHTING);

}

// This is the wild west of drawing - things that want to ignore depth buffer, camera transformations, etc. Not pretty, quite hacky, but flexible. Individual apps should be careful with implementing this method. It always gets called right at the end of the draw function
void TestAppDynamixel::drawAuxiliarySceneInfo() {
	// plots of cost function
	if (drawPlots)
		plotWindow->draw();

}

// Restart the application.
void TestAppDynamixel::restart() {

}

bool TestAppDynamixel::processCommandLine(const std::string& cmdLine) {
	if (GLApplication::processCommandLine(cmdLine)) return true;

	return false;
}

void TestAppDynamixel::adjustWindowSize(int width, int height) {
	Logger::consolePrint("resize");

	mainWindowWidth = width;
	mainWindowHeight = height;
	// Send the window size to AntTweakBar
	TwWindowSize(width, height);

	setViewportParameters(0, 0, mainWindowWidth, mainWindowHeight);

	int size[2];
	int w, h;
	TwGetParam(mainMenuBar, NULL, "size", TW_PARAM_INT32, 2, size);
	w = (GLApplication::getMainWindowWidth() - size[0]);
	h = GLApplication::getMainWindowHeight();

	setViewportParameters(size[0], 0, w, h);
}

void TestAppDynamixel::setupLights() {
	GLfloat bright[] = { 0.8f, 0.8f, 0.8f, 1.0f };
	GLfloat mediumbright[] = { 0.3f, 0.3f, 0.3f, 1.0f };

	glLightfv(GL_LIGHT1, GL_DIFFUSE, bright);
	glLightfv(GL_LIGHT2, GL_DIFFUSE, mediumbright);
	glLightfv(GL_LIGHT3, GL_DIFFUSE, mediumbright);
	glLightfv(GL_LIGHT4, GL_DIFFUSE, mediumbright);


	GLfloat light0_position[] = { 0.0f, 10000.0f, 10000.0f, 0.0f };
	GLfloat light0_direction[] = { 0.0f, -10000.0f, -10000.0f, 0.0f };

	GLfloat light1_position[] = { 0.0f, 10000.0f, -10000.0f, 0.0f };
	GLfloat light1_direction[] = { 0.0f, -10000.0f, 10000.0f, 0.0f };

	GLfloat light2_position[] = { 0.0f, -10000.0f, 0.0f, 0.0f };
	GLfloat light2_direction[] = { 0.0f, 10000.0f, -0.0f, 0.0f };

	GLfloat light3_position[] = { 10000.0f, -10000.0f, 0.0f, 0.0f };
	GLfloat light3_direction[] = { -10000.0f, 10000.0f, -0.0f, 0.0f };

	GLfloat light4_position[] = { -10000.0f, -10000.0f, 0.0f, 0.0f };
	GLfloat light4_direction[] = { 10000.0f, 10000.0f, -0.0f, 0.0f };


	glLightfv(GL_LIGHT0, GL_POSITION, light0_position);
	glLightfv(GL_LIGHT1, GL_POSITION, light1_position);
	glLightfv(GL_LIGHT2, GL_POSITION, light2_position);
	glLightfv(GL_LIGHT3, GL_POSITION, light3_position);
	glLightfv(GL_LIGHT4, GL_POSITION, light4_position);


	glLightfv(GL_LIGHT0, GL_SPOT_DIRECTION, light0_direction);
	glLightfv(GL_LIGHT1, GL_SPOT_DIRECTION, light1_direction);
	glLightfv(GL_LIGHT2, GL_SPOT_DIRECTION, light2_direction);
	glLightfv(GL_LIGHT3, GL_SPOT_DIRECTION, light3_direction);
	glLightfv(GL_LIGHT4, GL_SPOT_DIRECTION, light4_direction);


	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHT1);
	glEnable(GL_LIGHT2);
	glEnable(GL_LIGHT3);
	glEnable(GL_LIGHT4);
}
