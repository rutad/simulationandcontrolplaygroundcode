#pragma once

#include <string>
#include <map>
#include <GUILib/GLApplication.h>
#include <GUILib/TranslateWidget.h>
#include <GUILib/RotateWidgetV1.h>
#include <GUILib/RotateWidgetV2.h>
#include <MathLib/VoxelGrid.h>
#include <MathLib/VertexGrid.h>
#include <MathLib/LevelSet.h>
#include <Utils/Timer.h>

/**
 * Test App
 */
class TestAppMeshOps : public GLApplication {
private:
	Timer timer;

	GLMesh *outputMesh1 = NULL;
	GLMesh *outputMesh2 = NULL;
	GLMesh *outputMesh3 = NULL;
	GLMesh *inputMesh1 = NULL;
	GLMesh *inputMesh2 = NULL;
	GLMesh *inputMesh3 = NULL;

	TranslateWidget* tWidget = NULL;
	RotateWidget* rWidget = NULL;

	VoxelGrid<int>* voxelGrid = NULL;
	VertexGrid<double>* vertexGrid1 = NULL;
	VertexGrid<double>* vertexGrid2 = NULL;


	vector<LevelSet> levelSets;

	bool drawOutputMesh1 = true;
	bool drawOutputMesh2 = true;
	bool drawOutputMesh3 = true;
	bool drawInputMesh1 = true;
	bool drawInputMesh2 = true;
	bool drawInputMesh3 = true;

	double voxelSize = 0.05;
public:
	// constructor
	TestAppMeshOps();
	// destructor
	virtual ~TestAppMeshOps(void);
	// Run the App tasks
	virtual void process();
	// Draw the App scene - camera transformations, lighting, shadows, reflections, etc apply to everything drawn by this method
	virtual void drawScene();
	// This is the wild west of drawing - things that want to ignore depth buffer, camera transformations, etc. Not pretty, quite hacky, but flexible. Individual apps should be careful with implementing this method. It always gets called right at the end of the draw function
	virtual void drawAuxiliarySceneInfo();
	// Restart the application.
	virtual void restart();

	void testConvexHull();

	void testSignedDistance();
	void calSignedDistance(VertexGrid<double>* grid, GLMesh* mesh);
	GLMesh* marchingCubes(VertexGrid<double>* grid);
	void projectGrid(VertexGrid<double>* grid1, VertexGrid<double>* grid2);
	void transformSDF(VertexGrid<double>* SDFGrid, Quaternion q);

	// test level set CSG
	void testLevelSetCSG();
	void testLevelSetProjection();

	// test fast marching
	void testFastMarching();

	//input callbacks...

	//all these methods should returns true if the event is processed, false otherwise...
	//any time a physical key is pressed, this event will trigger. Useful for reading off special keys...
	virtual bool onKeyEvent(int key, int action, int mods);
	//this one gets triggered on UNICODE characters only...
	virtual bool onCharacterPressedEvent(int key, int mods);
	//triggered when mouse buttons are pressed
	virtual bool onMouseButtonEvent(int button, int action, int mods, double xPos, double yPos);
	//triggered when mouse moves
	virtual bool onMouseMoveEvent(double xPos, double yPos);
	//triggered when using the mouse wheel
	virtual bool onMouseWheelScrollEvent(double xOffset, double yOffset);

	virtual bool processCommandLine(const std::string& cmdLine);
	
	virtual void saveFile(const char* fName);
	virtual void loadFile(const char* fName);

	virtual void setupLights();

};



