#pragma once

#include <GUILib/GLApplication.h>

#include <string>
#include <map>


#include <RobotDesignerLib/RobotDesignWindow.h>

#include <GUILib/TranslateWidget.h>
#include <RBSimLib/AbstractRBEngine.h>
#include <RBSimLib/WorldOracle.h>
#include <GUILib/GLWindow3D.h>
#include <GUILib/GLWindow3DWithMesh.h>
#include <GUILib/GLWindowContainer.h>
#include <GUILib/PlotWindow.h>
#include <RobotDesignerLib/RobotController.h>
#include <RobotDesignerLib/MOPTWindow.h>
#include <RobotDesignerLib/SkeletonMorphWindow.h>
#include <RobotDesignerLib/MotionGraphViewer.h>
#include <RobotDesignerLib/PathFollower.h>
#include <ControlLib/IK_Solver.h>
#include <RobotDesignerLib/KinematicRobotController.h>


struct MotionClipConfig {

	string motionClipName;
	string footFallPatternPath;
	double speedX = 0.0;
	double speedZ = 0.0;
	double turningAngle = 0.0;
};

struct RBSTemplateConfig {

	string rbsPath;
	string rsPath;
};

struct AnimationConfig {

	string simRobotPath;
	string animRobotPath;
	RBSTemplateConfig templateConfig;
	vector<MotionClipConfig> motionClipConfigs;
};

class ThreeLinkIKObj : public ObjectiveFunction {
public:
	double r1, r2, r3; // the length of link1, link2 and link3
	double k; // the ratio between first joint angle and second joint angle
	double targetLen; // the target length from the root to the end point
	double regularizer = 1;

public:
	ThreeLinkIKObj(double r1, double r2, double r3, double k, double targetLen);

	double computeValue(const dVector& p);
	double computeValueWithoutRegularizer(const dVector& p);

};


/**
* Robot Design and Simulation interface
*/
class RobotAnimationApp : public GLApplication {
private:
	MOPTWindow* moptWindow;
	SkeletonMorphWindow* skeletonWindow;
	MotionGraphViewer* motionGraphViewer;

	AnimationConfig animConfig;

	KinematicRobotController* controller = NULL;
	WorldOracle* worldOracle = NULL;
	Robot* robot = NULL;
	IK_Solver* ikSolver = NULL;
	ReducedRobotState* startState = NULL;
	AbstractRBEngine* rbEngine = NULL;
	MotionGraph* motionGraph = NULL;
	PathFollower* pathFollower = NULL;
	RobotPath robotPath;

	Robot* animRobot = NULL;
	AbstractRBEngine* animRbEngine = NULL;
	ReducedRobotState* animStartState = NULL;

	Skeleton*	m_pSkeleton = NULL;
	SkinnedMesh*		m_pMesh = NULL;
	RBSMesh*	m_pRBSMesh = NULL;

	bool followPath = true;
	bool recordAnimation = false;
	bool showRobotSimp = false;
	int animationFrame = 0;

	double stridePhase = 0;
	double simTimeStep = 1 / 1200.0; // should be divisble by 12.
	double motionPlanTime = 0;
	double motionExportRate = 0.01;

	double IKOffsetScale = 1.0;
	double footSwingAngle = 30.0;
	double rearKneeAngle = 30.0;

	double frontLimbStanceLen = 1.0;
	double rearLimbStanceLen = 0.9;
	double frontLimbSwingLen = 0.9;
	double rearLimbSwingLen = 0.8;
	double frontLimbAngleRatio = 1;
	double rearLimbAngleRatio = 1;
	double frontFeetStanceAngle = 0.0;
	double rearFeetStanceAngle = 0.0;
	double frontFeetSwingAngle = 30.0;
	double rearFeetSwingAngle = 30.0;
	bool frontLimbFlip = false;
	bool rearLimbFlip = true;
	bool frontShoulderFlip = true;
	bool rearShoulderFlip = false;

	RotateWidgetV2 rotateWidget;

	enum RUN_OPTIONS {
		MOTION_PLAN_OPTIMIZATION = 0,
		MOTION_PLAN_ANIMATION,
		ROBOT_ANIMATION
	};
	int runOption = MOTION_PLAN_OPTIMIZATION;

	enum WINDOW_OPTIONS {
		SKELETON_WINDOW = 0,
		MOPT_WINDOW
	};
	int windowOption = SKELETON_WINDOW;

public:

	// constructor
	RobotAnimationApp();
	// destructor
	virtual ~RobotAnimationApp(void);
	// Run the App tasks
	virtual void process();
	// Restart the application.
	virtual void restart();

	// Draw the App scene - camera transformations, lighting, shadows, reflections, etc apply to everything drawn by this method
	virtual void drawScene();
	// This is the wild west of drawing - things that want to ignore depth buffer, camera transformations, etc. Not pretty, quite hacky, but flexible. Individual apps should be careful with implementing this method. It always gets called right at the end of the draw function
	virtual void drawAuxiliarySceneInfo();

	//all these methods should returns true if the event is processed, false otherwise...
	//any time a physical key is pressed, this event will trigger. Useful for reading off special keys...
	virtual bool onKeyEvent(int key, int action, int mods);
	//this one gets triggered on UNICODE characters only...
	virtual bool onCharacterPressedEvent(int key, int mods);
	//triggered when mouse buttons are pressed
	virtual bool onMouseButtonEvent(int button, int action, int mods, double xPos, double yPos);
	//triggered when mouse moves
	virtual bool onMouseMoveEvent(double xPos, double yPos);
	//triggered when using the mouse wheel
	virtual bool onMouseWheelScrollEvent(double xOffset, double yOffset);

	virtual bool processCommandLine(const std::string& cmdLine);

	void adjustWindowSize(int width, int height);
	virtual void setupLights();

	void loadRobot(const char* fName, const char* fNameRS = NULL);
	void loadAnimRobot(const char* fName);
	void loadRobotFromModeler();
	void loadFBXFile(const char* fName);

	virtual void loadFile(const char* fName);
	virtual void saveFile(const char* fName);

	void exportMotion(MotionGraphNode* node, const char* folderName);

	void drawRobotPath();

	void generateNewMP();
	void handleMotionGraphViewer(int mods);
	void deleteMTNodeOrEdge();

	void prepareForRobotAnimation();
	void handleTransition();

	void loadConfig(const char* fName);
	void saveContextToFile(const char* fName);
	void loadContextFromFile(const char* fName);

	void reset();

	void createNewPathPts(double xPos, double yPos);
	void handleMotionSwitch();

	void generateWalkTrotMPs();

	void syncAnimRobotWithRobot();
	void syncAnimRobotWithRobotV2();
	void syncAnimRobotWithRobotV3();
	void inferLegParamsFromZeroAnimState();
	void syncNonLimbJointRelOrientation(Joint* robotJoint, Joint* animJoint, ReducedRobotState& robotState, ReducedRobotState& animState, set<Joint*>& boundaryJoints);
	void adjustAnimRobotAngle(Joint* joint, double angle);
	void adjustAnimRobotJPos(Joint* parentJoint, P3D curPos, P3D newPos);
	double solveJointAnglesForLimb(double r1, double r2, double r3, double k, double targetLen, bool flip);
};
