#include <GUILib/GLUtils.h>

#include "TestAppRobotWheel.h"
#include <GUILib/GLMesh.h>
#include <GUILib/GLContentManager.h>
#include <MathLib/MathLib.h>
#include <RBSimLib/ODERBEngine.h>
#include <ControlLib/SimpleLimb.h>

TestAppRobotWheel::TestAppRobotWheel() {
	setWindowTitle("RobotWheelControl");
	//loadFile("../data/rbs/car.rbs");
	loadFile("../data/rbs/robotArm1DOF.rbs");

	TwAddSeparator(mainMenuBar, "sep4", "");
	TwAddVarRW(mainMenuBar, "Motor1Amplitude", TW_TYPE_DOUBLE, &amplitude1, " label='Motor1Amplitude'");
	//TwAddVarRW(mainMenuBar, "Motor2Amplitude", TW_TYPE_DOUBLE, &amplitude2, " label='Motor2Amplitude'");
	//TwAddVarRW(mainMenuBar, "Motor2Phase", TW_TYPE_DOUBLE, &phase2, " label='Motor2Phase'");
	TwAddVarRW(mainMenuBar, "frequency", TW_TYPE_DOUBLE, &frequency, " label='motion frequency'");

	frequency = 2.0;
	desiredFrameRate = 60;
	waitForFrameRate = false;

	dxID.push_back(1);
	/*dxID.push_back(7);
	dxID.push_back(8);
	dxID.push_back(9);
	dxID.push_back(10);*/


	//set dx IDS.
	for (int i = 0; i < robot->getJointCount(); i++) {
		HingeJoint* hj = dynamic_cast<HingeJoint*>(robot->getJoint(i));
		if (!hj) continue;
		hj->dynamixelProperties.dxl_id = dxID[i];
		Logger::consolePrint("DynaID set:%d\n", hj->dynamixelProperties.dxl_id);
	}

	// set control mode
	controlMode = CM_FF_VELOCITY_ONLY;
	Logger::consolePrint("set velocity control mode!\n");
	if (controlMode == CM_VELOCITY_AND_ACCELERATION || controlMode == CM_FF_VELOCITY_ONLY || controlMode == CM_FB_VELOCITY_ONLY) {
		bool motorsToggled = false;
		if (motorsOn) {
			toggleMotors();
			motorsToggled = true;
		}
		for (int i = 0; i < robot->getJointCount(); i++) {
			dxl_comm_result = packetHandler->write1ByteTxRx(portHandler, dxID[i], ADDR_OPERATING_MODE, DXL_VELOCITY_CONTROL_MODE, &dxl_error);
			dxl_comm_result = packetHandler->write2ByteTxRx(portHandler, dxID[i], ADDR_VEL_P_GAIN, (uint16_t)velPGain, &dxl_error);
			dxl_comm_result = packetHandler->write2ByteTxRx(portHandler, dxID[i], ADDR_POS_P_GAIN, (uint16_t)posPGain, &dxl_error);
			dxl_comm_result = packetHandler->write2ByteTxRx(portHandler, dxID[i], ADDR_POS_D_GAIN, (uint16_t)posDGain, &dxl_error);
			dxl_comm_result = packetHandler->write1ByteTxRx(portHandler, dxID[i], ADDR_RETURN_DELAY_TIME, (uint8_t)returnDelayTime, &dxl_error);

		}
		if (motorsToggled)
			toggleMotors();
	}
}

TestAppRobotWheel::~TestAppRobotWheel(void) {
}

// Restart the application.
void TestAppRobotWheel::restart() {

}

Timer appTimer;

// Run the App tasks
void TestAppRobotWheel::process() {
	if (runMode == SYNC_SIM_FROM_ROBOT_STATE)
		syncSimulationWithPhysicalRobot();
	else {

		double vel1 = amplitude1 * frequency;

		ReducedRobotState rs(robot);
		for (uint i = 0; i < dxID.size(); i++) {
			rs.setJointRelativeAngVelocity(((HingeJoint*)robot->getJoint(i))->rotationAxis * vel1, i);
			Logger::consolePrint("id:%d, rotAxis(%lf, %lf, %lf), vel:%lf\n", i, ((HingeJoint*)robot->getJoint(i))->rotationAxis[0],
				((HingeJoint*)robot->getJoint(i))->rotationAxis[1], ((HingeJoint*)robot->getJoint(i))->rotationAxis[2],
				vel1);
		}
		robot->setState(&rs);
		computeDXLControlInputsBasedOnSimRobot(1.0 / desiredFrameRate);

		sendControlInputsToPhysicalRobot();

		double timeEllapsed = appTimer.timeEllapsed();
		//this is assuming that the process function takes exactly the amount of time that is allocated to it...
		motionPhase += frequency * (1.0 / desiredFrameRate);
		appTimer.restart();
		//Logger::consolePrint("Timing error: %lfs (+ve means it has taken longer than expected, so the motor has had extra time to run...)\n", timeEllapsed - 1.0 / desiredFrameRate);
	}
}

//TODO: do try a mode where we command velocities and have a velocity profile to smooth things out...

void TestAppRobotWheel::sendControlInputsToPhysicalRobot() {

	if (!packetHandler || !portHandler)
		return;

	//if (velPGain != lastVelPGain) {
	//	dxl_comm_result = packetHandler->write2ByteTxRx(portHandler, ID, ADDR_VEL_P_GAIN, (uint16_t)velPGain, &dxl_error);
	//	lastVelPGain = velPGain;
	//}
	//if (posPGain != lastPosPGain) {
	//	dxl_comm_result = packetHandler->write2ByteTxRx(portHandler, ID, ADDR_POS_P_GAIN, (uint16_t)posPGain, &dxl_error);
	//	lastPosPGain = posPGain;
	//}
	//if (posDGain != lastPosDGain) {
	//	dxl_comm_result = packetHandler->write2ByteTxRx(portHandler, ID, ADDR_POS_D_GAIN, (uint16_t)posDGain, &dxl_error);
	//	lastPosDGain = posDGain;
	//}

	//if (returnDelayTime != lastReturnDelayTime) {
	//	dxl_comm_result = packetHandler->write1ByteTxRx(portHandler, ID, ADDR_RETURN_DELAY_TIME, (uint8_t)returnDelayTime, &dxl_error);
	//	lastReturnDelayTime = returnDelayTime;
	//}

	//TODO: change the delay time, change the pos KP/KD and the velocity P gains

	flipMotorTargetsIfNeeded();

	uint8_t tmp_param[4];

	if (controlMode == CM_VELOCITY_AND_ACCELERATION || controlMode == CM_FF_VELOCITY_ONLY || controlMode == CM_FB_VELOCITY_ONLY) {
		//write out target positions...
		dynamixel::GroupSyncWrite velGroupSyncWriter = dynamixel::GroupSyncWrite(portHandler, packetHandler, ADDR_GOAL_VELOCITY, 4);
		//write out velocity limits, based on how much each joint needs to move - using a rectangular velocity profile...
		dynamixel::GroupSyncWrite accGroupSyncWriter = dynamixel::GroupSyncWrite(portHandler, packetHandler, ADDR_ACCELERATION_PROFILE, 4);

		for (int i = 0; i < robot->getJointCount(); i++) {
			HingeJoint* hj = dynamic_cast<HingeJoint*>(robot->getJoint(i));
			if (!hj) continue;
			int dxlGoalVel = getDXLVelocityFromRadPerSec(hj->dynamixelProperties.targetMotorVelocity * motionScaleFactor);

			if (hj->dynamixelProperties.dxl_id >= 0)
				velGroupSyncWriter.addParam(hj->dynamixelProperties.dxl_id, DXL_INT32_TO_UINT8_ARRAY(dxlGoalVel, tmp_param));

			int dxlGoalAcc = getDXLAccelFromRadPerSecSquared(fabs(hj->dynamixelProperties.targetMotorAcceleration) * motionScaleFactor);
			if (dxlGoalAcc == 0) dxlGoalAcc = 1; // 0 means no limit, which we shouldn't do if accelerations are controlled at all...
			if (controlMode == CM_FF_VELOCITY_ONLY || controlMode == CM_FB_VELOCITY_ONLY) dxlGoalAcc = 0; // 0 means no speed limit, which is what we want in position control mode...
			Logger::consolePrint("id:%d, target velocity: %lf, goal dxl vel: %d\n", i, hj->dynamixelProperties.targetMotorVelocity, dxlGoalVel);
			if (hj->dynamixelProperties.dxl_id >= 0)
				accGroupSyncWriter.addParam(hj->dynamixelProperties.dxl_id, DXL_INT32_TO_UINT8_ARRAY(dxlGoalAcc, tmp_param));
		}
		//sync write velocity limits...
		dxl_comm_result = accGroupSyncWriter.txPacket();
		if (dxl_comm_result != COMM_SUCCESS) {
			packetHandler->printTxRxResult(dxl_comm_result);
			Logger::consolePrint("Could not send groupRead packet (accGroupSyncWriter). This could happen if one attempts to read from IDs that do not exist on the robot side...\n");
		}

		//sync write goal positions...
		dxl_comm_result = velGroupSyncWriter.txPacket();
		if (dxl_comm_result != COMM_SUCCESS) {
			packetHandler->printTxRxResult(dxl_comm_result);
			Logger::consolePrint("Could not send groupRead packet (velGroupSyncWriter). This could happen if one attempts to read from IDs that do not exist on the robot side...\n");
		}
	}

	flipMotorTargetsIfNeeded();
}

void TestAppRobotWheel::loadFile(const char* fName) {
	Logger::consolePrint("Loading file \'%s\'...\n", fName);
	std::string fileName;
	fileName.assign(fName);

	std::string fNameExt = fileName.substr(fileName.find_last_of('.') + 1);

	if (fNameExt.compare("rbs") == 0) {
		delete robot;
		delete rbEngine;

		rbEngine = new ODERBEngine();
		rbEngine->loadRBsFromFile(fName);
		robot = new Robot(rbEngine->rbs[0]);
	}

}

void TestAppRobotWheel::zeroMotorPositions() {
	bool motorWasOn = motorsOn;
	// setting operating mode needs motors off
	if (motorsOn) {
		toggleMotors();
	}
	// set position mode
	for (uint i = 0;i< dxID.size(); i++) {
		dxl_comm_result = packetHandler->write1ByteTxRx(portHandler, dxID[i], ADDR_OPERATING_MODE, DXL_POSITION_CONTROL_MODE, &dxl_error);
	}

	//now turn the motors on and zero their positions...
	toggleMotors();
	
	// zero motor positions
	//TestAppDynamixel::zeroMotorPositions();
	for (uint i = 0; i < dxID.size(); i++) {
		dxl_comm_result = packetHandler->write4ByteTxRx(portHandler, dxID[i], ADDR_VELOCITY_LIMIT, getDXLVelocityFromRPM(300), &dxl_error);
		dxl_comm_result = packetHandler->write4ByteTxRx(portHandler, dxID[i], ADDR_VELOCITY_PROFILE, 100, &dxl_error);
		dxl_comm_result = packetHandler->write4ByteTxRx(portHandler, dxID[i], ADDR_GOAL_POSITION, getDXLPositionFromRad(0), &dxl_error);
		//dxl_comm_result = packetHandler->write4ByteTxRx(portHandler, dxID[i], ADDR_VELOCITY_PROFILE, 0, &dxl_error);
		Timer timer;
		while (timer.timeEllapsed() < 0.5);
		dxl_comm_result = packetHandler->write4ByteTxRx(portHandler, BROADCAST_ID, ADDR_VELOCITY_PROFILE, 0, &dxl_error);
	}

	// set control mode back to velocity control..
	toggleMotors();
	for (int i = 0; i < robot->getJointCount(); i++) {
		dxl_comm_result = packetHandler->write1ByteTxRx(portHandler, dxID[i], ADDR_OPERATING_MODE, DXL_VELOCITY_CONTROL_MODE, &dxl_error);

	}
	if (motorWasOn)
		toggleMotors();
	Logger::consolePrint("I am in wheel app!\n");
}