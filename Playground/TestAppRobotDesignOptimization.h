#pragma once

#include <GUILib/GLApplication.h>
#include <RobotDesignerLib/RobotDesignWindow.h>
#include <string>
#include <map>
#include <GUILib/TranslateWidget.h>
#include <RBSimLib/AbstractRBEngine.h>
#include <RBSimLib/WorldOracle.h>
#include <GUILib/GLWindow3D.h>
#include <GUILib/GLWindow3DWithMesh.h>
#include <GUILib/GLWindowContainer.h>
#include <RobotDesignerLib/LocomotionEngineMotionPlan.h>
#include <RobotDesignerLib/LocomotionEngine.h>
#include <RobotDesignerLib/FootFallPatternViewer.h>
#include <RobotDesignerLib/LocomotionEngineManagerGRF.h>
#include <RobotDesignerLib/LocomotionEngineManagerIP.h>
#include <GUILib/PlotWindow.h>
#include <RobotDesignerLib/ModularDesignWindow.h>
#include <RobotDesignerLib/ParameterizedRobotDesign.h>
#include <RobotDesignerLib/MOPTWindow.h>
#include <ControlLib/IK_Solver.h>
#include <omp.h>

#include "OptimizationLib/LBFGS.h"

/**
 * Robot Design and Simulation interface
 */
class TestAppRobotDesignOptimization : public GLApplication {
public:
	MOPTWindow* moptWindow = NULL;
	AbstractRBEngine* rbEngine = NULL;
	Robot* robot = NULL;
	ParameterizedRobotDesign* prd = NULL;
	DynamicArray<double> designParameters;
	DynamicArray<string> designParameterNames;

	LBFGSpp::LBFGSParam<double> param;
	LBFGSpp::LBFGSSolver<double> solver;

	bool drawMeshes = true, drawMOIs = false, drawCDPs = false, drawSkeletonView = false, drawJoints = false, drawContactForces = true, drawOrientation = true;
	double simTimeStep;
	bool drawMotionPlan = false;

	double motionPlanTime = 0;
	
	int animationCycle = 0;

	LocomotionEngineManager* locomotionManager = NULL;

	enum RUN_OPTIONS {
		MOTION_PLAN_OPTIMIZATION = 0,
		MOTION_PLAN_ANIMATION = 1,
		DESIGN_OPTIMIZATION = 2
	};
	int runOption = DESIGN_OPTIMIZATION;

	//Beichen Li: motion parameters need to be saved, especially during line search
	dVector savedMotionParameters;

	//Beichen Li: optimization of design parameters;
	dVector pLast;
	double regCoefficient = 20.0;
	double regularizer = 1.0;
	double epsilon = 0.1;
	double dfLast = 0.0;
	double currentEnergy = 0.0;

	//Beichen Li: saved dmdp for line search optimization
	MatrixNxM dmdp;

	//Beichen Li: designCheck is the switch for dfdp validation
	bool designCheck = false;

	//Beichen Li: OpenMP threads
	int numThreads = 4;

public:
	// constructor
	TestAppRobotDesignOptimization();
	// destructor
	virtual ~TestAppRobotDesignOptimization(void);
	// Run the App tasks
	virtual void process();
	// Draw the App scene - camera transformations, lighting, shadows, reflections, etc apply to everything drawn by this method
	virtual void drawScene();
	// This is the wild west of drawing - things that want to ignore depth buffer, camera transformations, etc. Not pretty, quite hacky, but flexible. Individual apps should be careful with implementing this method. It always gets called right at the end of the draw function
	virtual void drawAuxiliarySceneInfo();
	// Restart the application.
	virtual void restart();

	double runMOPTStep();

	void runDOPTStep();

	//input callbacks...

	//all these methods should returns true if the event is processed, false otherwise...
	//any time a physical key is pressed, this event will trigger. Useful for reading off special keys...
	virtual bool onKeyEvent(int key, int action, int mods);
	//this one gets triggered on UNICODE characters only...
	virtual bool onCharacterPressedEvent(int key, int mods);
	//triggered when mouse buttons are pressed
	virtual bool onMouseButtonEvent(int button, int action, int mods, double xPos, double yPos);
	//triggered when mouse moves
	virtual bool onMouseMoveEvent(double xPos, double yPos);
	//triggered when using the mouse wheel
	virtual bool onMouseWheelScrollEvent(double xOffset, double yOffset);

	virtual bool processCommandLine(const std::string& cmdLine);
	
	virtual void saveFile(const char* fName);
	virtual void loadFile(const char* fName);

	void warmStartMOPT(bool useWarmStarting = true);
	void loadToSim(bool useWarmStarting = true);
	void optimizeDesign();

	void testJacobians();
	void testApproximation();
	void testGradient();

	void adjustWindowSize(int width, int height);
	virtual void setupLights();

	void addParameterizedDesignParametersToMenu();
	void removeParameterizedDesignParametersFromMenu();

	//Beichen Li: to apply LBFGS we must make this application a functor
	//the function calculates f and df/dp according to p, which contains two steps:
	//1. run MOPT till convergence
	//2. calculate f and df/dp using adjoint method
	double operator () (const dVector& p, dVector& grad, bool saveFlag = true);
	double operator () (const dVector& p, const dVector& dp, dVector& grad, bool saveFlag = true, bool MOPTFlag = true);

	//Beichen Li: this function computes dfdp given design parameter set p
	void addGradientOfDesignParametersTo(dVector& grad, const dVector& p);

	//Beichen Li: computes dmdp at design parameter set p
	void computeDmdp();

	//Beichen Li: converts dVector to DynamicArray<double>
	void dVectorToParametersList(const dVector& v, DynamicArray<double>& p) {
		p.resize(v.size());
		for (int i = 0; i < v.size(); i++)
			p[i] = v[i];
	}

	//Beichen Li: converts DynamicArray<double> to dVector
	void parametersListToDVector(const DynamicArray<double>& p, dVector& v) {
		v.resize(p.size());
		for (uint i = 0; i < p.size(); i++)
			v[i] = p[i];
	}

	//Beichen Li: for regularization of design parameters
	double computeRegularizationTerm(const dVector& p) {
		if (pLast.size() != p.size())
			pLast = p;
		return 0.5 * regularizer * (p - pLast).squaredNorm();
	}

	//Beichen Li: for regularization of design parameters
	void updateRegularizingSolution(const dVector& p) {
		pLast = p;
	}

	void updateRegularizer(double f, double df) {
	//	if (fabs(df) * regCoefficient < regularizer &&
	//		fabs(dfLast - df) < epsilon * fabs(df)) {
	//		regularizer *= 0.1;
	//		regCoefficient *= 2.0;
	//	}
	//	dfLast = df;
		double temp = regCoefficient * fabs(df);
		regularizer = temp < 5.0 ? temp : 5.0;
	}

	void logParameters(const std::string& str, const dVector& p) {
		Logger::logPrint("%s: ", str.c_str());
		int end = p.size();
		for (int i = 0; i < end - 1; i++)
			Logger::logPrint("%lf ", p[i]);
		Logger::logPrint("%lf\n", p[end - 1]);
	}

	void parallelTest() {
		Logger::consolePrint("Number of threads: %d\n", omp_get_num_threads());
#pragma omp parallel for
		for (int i = 0; i < numThreads; i++)
#pragma omp critical
		{
			Logger::consolePrint("Thread %d\n", omp_get_thread_num());
		}
	}
};




