#include <GUILib/GLUtils.h>

#include "MySpotMiniApp.h"
#include <GUILib/GLMesh.h>
#include <GUILib/GLContentManager.h>
#include <MathLib/MathLib.h>
#include <RBSimLib/ODERBEngine.h>
#include <ControlLib/SimpleLimb.h>
#include <RobotDesignerLib/ControlUtil.h>
#include <ControlLib/MOPTQPTrackingController.h>

/**
	Should also try to have MOPT for a simple model being tracked by a complex robot - the secret to natural motion controllers?
		- biped, dinosaur, a mech, like those from titanfall 2
		- quadrupeds with animal-like limbs...
		- does mean that we should have a different way of setting regularizers for legs...

	- still check how well we're tracking foot positions... if it's not well enough, then it is perhaps a sign that the velocity term is not computed properly yet...
	- should at some point worry about the coriollis term in the F=Ma constraint, and double check it once more...?

	- need to do something about the feet - they snap a lot just in the very beginning...

	- make a less wide version of starleth, call it spotMini, and have it do a bunch of gaits, including bounding. And make it not recompute mass properties and all that...
	do also show a version of a bipedal robot...
	- also a cheetah version.
	- do the same with an ostridge like robot, call it cassie.

	figure out why the legs sync into the ground when no sim
	think of a better control law... could it be LQR-type? Full-state feedback?
	am I giving the right target... right now I'm asking it (targets for feet, COM, etc) to be in the future
	where they should be right now... so perhaps the targets should anyway be read some time in the future...
	maybe we need a feedforward acceleration term too...
	feels like I should have one app, or part of this one, where I just get to see what happens to a particle in space trying to follow a periodic trajectory when using different control laws...

	need an app where there is a 2d or 3d trajectory that is being tracked
	with explicit PD, with implicit PD, with target now, target later, with feedforward acceleration, without feedforward acceleration, with LQR control with different time horizons, with perturbations, without perturbations...

*/


/**
	UI code:
*/
void TW_CALL ResetRobot(void* clientData) {
	((MySpotMiniApp*)clientData)->syncRobotToMotionPlan();
}

MySpotMiniApp::MySpotMiniApp(){
	setWindowTitle("MySpotMini App");

	TwAddButton(mainMenuBar, "resetRobot", ResetRobot, this, "key=r");

//	loadFile("../data/modularRobot/XM-320Designs/quadruped.rbs");
//	loadFile("../data/modularRobot/XM-320Designs/quadrupedDynamicWalk.p");
//	loadFile("../data/modularRobot/XM-320Designs/quadrupedFlyingTrot.p");
//	loadFile("../data/modularRobot/XM-320Designs/quadrupedTrot.p");
//	loadFile("../data/modularRobot/XM-320Designs/quadrupedWalk.p");
//	loadFile("../data/modularRobot/XM-320Designs/quadrupedStand.p");

//	loadFile("../data/modularRobot/XM-320Designs/quadruped2.rbs");
//	loadFile("../data/modularRobot/XM-320Designs/quadruped2InplaceWalk.p");

//	loadFile("../data/motionPlans/starlETH/robot.rbs");
//	loadFile("../data/motionPlans/starlETH/inPlaceWalk.p");
//	loadFile("../data/motionPlans/starlETH/walk.p");
//	loadFile("../data/motionPlans/starlETH/dynamicWalk.p");
//	loadFile("../data/motionPlans/starlETH/trot.p");
//	loadFile("../data/motionPlans/starlETH/flyingTrot.p");

	loadFile("../data/motionPlans/spotMini/robot.rbs");
//	loadFile("../data/motionPlans/spotMini/walk.p");
	loadFile("../data/motionPlans/spotMini/trot.p");
//	loadFile("../data/motionPlans/spotMini/pace.p");
//	loadFile("../data/motionPlans/spotMini/bound.p");
//	loadFile("../data/motionPlans/spotMini/flyingTrot.p");

//	loadFile("../data/rbs/babyRex2.rbs");
//	loadFile("../data/mopt/babyRex2.p");

	TwAddSeparator(mainMenuBar, "sep4", "");
	TwAddVarRW(mainMenuBar, "Run Physics", TW_TYPE_BOOLCPP, &runPhysics, "");
	TwAddVarRW(mainMenuBar, "ShowDebugInfo", TW_TYPE_BOOLCPP, &drawControllerDebug, "");
	TwAddVarRW(mainMenuBar, "CheckControlSolution", TW_TYPE_BOOLCPP, &checkControlSolution, "");





	showGroundPlane = true;

//	desiredFrameRate = 10;

	//assing some DXL ids for testing...
	for (int i = 0; i < robot->getJointCount(); i++) {
		HingeJoint* hj = dynamic_cast<HingeJoint*>(robot->getJoint(i));
		if (!hj) continue;
		hj->dynamixelProperties.dxl_id = i + 1;
	}

//	slowMo = true;
//	runPhysics = false;
	drawControllerDebug = true;
	followCameraTarget = true;


	motionPlan->syncFootFallPatternWithMotionPlan(footFallPattern);
	ffpViewer = new FootFallPatternViewer((int)(260 * 1.14 + 10), mainWindowHeight - 210, 600, 200);
	ffpViewer->ffp = &footFallPattern;
}

void MySpotMiniApp::loadRobot(const char* fName) {
	delete robot;
	delete rbEngine;
	delete controller;
	delete motionPlan;
	delete worldOracle;

	worldOracle = new WorldOracle(Globals::worldUp, Globals::groundPlane);

	rbEngine = new ODERBEngine();
	RobotDesign* robotDesign = new RobotDesign();
	robotDesign->readRobotFromFile(fName);
	robotDesign->saveRobotToFile("../out/tmpRobot.rbs");
	rbEngine->loadRBsFromFile("../out/tmpRobot.rbs");
	robotDesign->transferMeshes(rbEngine->rbs);
	delete robotDesign;
	robot = new Robot(rbEngine->rbs[0]);
	setupSimpleRobotStructure(robot);

	worldOracle->writeRBSFile("../out/tmpEnvironment.rbs");
	rbEngine->loadRBsFromFile("../out/tmpEnvironment.rbs");

	motionPlan = new LocomotionEngineMotionPlan(robot, 10);
	robot->bFrame->updateStateInformation();

	//load from file should read the number of sample points, as well as the motion plan duration...

	controller = new MOPTQPTrackingController(robot);

	for (uint i = 0; i < controller->qpEngine->energyFunction->objectives.size();i++)
		TwAddVarRW(mainMenuBar, controller->qpEngine->energyFunction->objectives[i]->description.c_str(), TW_TYPE_DOUBLE, &controller->qpEngine->energyFunction->objectives[i]->weight, "min=0 max =1000000");

}

void MySpotMiniApp::loadFile(const char* fName) {
	Logger::consolePrint("Loading file \'%s\'...\n", fName);
	std::string fileName;
	fileName.assign(fName);

	std::string fNameExt = fileName.substr(fileName.find_last_of('.') + 1);

	if (fNameExt.compare("rbs") == 0)
		loadRobot(fName);

	if (fNameExt.compare("p") == 0)
		if (motionPlan) {
			motionPlan->readParamsFromFile(fName);
			syncRobotToMotionPlan();
		}
}

MySpotMiniApp::~MySpotMiniApp(void){
}

// Restart the application.
void MySpotMiniApp::restart() {

}

void MySpotMiniApp::syncRobotToMotionPlan() {
/*
	stridePhase = 0.380952;
	robot->loadReducedStateFromFile("../out/debugState.rs");
	ReducedRobotState tmprs(robot);
	tmprs.setPosition(P3D(0.0, tmprs.getPosition().y(),0.0));
	robot->setState(&tmprs);
	return;
*/

	Logger::consolePrint("Syncronizing robot state\n");
	stridePhase = 0.0;
	ReducedRobotState rs = controller->getTargetRobotState(motionPlan, stridePhase);

//	rs.clearVelocities();

//	rs.setPosition(rs.getPosition() + P3D(1.0,0,1.0));
//	rs.setHeading(1.57);
	robot->setState(&rs);
	robot->bFrame->updateStateInformation();

	Logger::consolePrint("robot weighs: %lfkgs and is %lfm tall\n", robot->getMass(), robot->root->getCMPosition().y());

}

// Run the App tasks
void MySpotMiniApp::process() {
	double simulationTime = 0;
	double maxRunningTime = 1.0 / desiredFrameRate;
	controller->doDebug = checkControlSolution;

//	int ccc = 0;

	while (simulationTime / maxRunningTime < animationSpeedupFactor) {
//		ccc++;
		simulationTime += simTimeStep;

		if (!runPhysics) {
			for (uint i = 0; i<robot->bFrame->limbs.size(); i++)
				robot->bFrame->limbs[i]->inContact = true;
			robot->bFrame->updateStateInformation();
		}
		else {
			rbEngine->markRBContacts(0.1);
			robot->bFrame->updateLimbGroundContactInformation();
			robot->bFrame->updateStateInformation();
		}

		controller->computeControlSignals(motionPlan, stridePhase, simTimeStep);

		if (runPhysics) {
			for (int i = 0; i < nPhysicsStepsPerControlStep; i++) {
				GeneralizedCoordinatesRobotRepresentation gcrr(robot);
				gcrr.computeWorldCoordinateTorquesFromU(controller->qpPlan->u);

				rbEngine->applyForceTo(robot->root, perturbationForce * forceScale, P3D());

				rbEngine->step(simTimeStep / nPhysicsStepsPerControlStep);
				robot->bFrame->updateStateInformation();
			}
		}
		else {
			GeneralizedCoordinatesRobotRepresentation gcrr(robot);
			gcrr.integrateGenerlizedAccelerationsForwardInTime(controller->qpEngine->qpPlan->a, simTimeStep);
			gcrr.syncRobotStateWithGeneralizedCoordinates();
			robot->bFrame->updateStateInformation();
		}

		stridePhase += simTimeStep / this->motionPlan->motionPlanDuration;
		if (stridePhase > 1.0)
			stridePhase -= 1.0;

		if (slowMo)
			break;
	}

/*
	string fName = "../out/state_" + to_string(stridePhase) + ".rs";
	robot->saveReducedStateToFile(fName.c_str());
*/

//	Logger::consolePrint("did %d steps\n", ccc);
}

void MySpotMiniApp::setPerturbationForceFromMouseInput(double xPos, double yPos) {
	Ray ray = getRayFromScreenCoords(xPos, yPos);
	P3D pForce;
	//		ray.getDistanceToPoint(robot->root->getCMPosition(), &pForce);
	ray.getDistanceToPlane(Plane(robot->root->getCMPosition(), V3D(0, 1, 0)), &pForce);
	//now we know how to compute this perturbation force...
	perturbationForce = V3D(robot->root->getCMPosition(), pForce);
	perturbationForce.y() = 0;
	if (perturbationForce.length() > 2.0) perturbationForce = perturbationForce.unit() * 2.0;
	forceScale = robot->getMass() * 1.5;
}

//triggered when mouse moves
bool MySpotMiniApp::onMouseMoveEvent(double xPos, double yPos) {

	perturbationForce = V3D();
	int shiftDown = glfwGetKey(this->glfwWindow, GLFW_KEY_LEFT_SHIFT);
	if (GlobalMouseState::lButtonPressed && shiftDown == GLFW_PRESS) {
		setPerturbationForceFromMouseInput(xPos, yPos);
		return true;
	}

	if (showMenus)
		if (TwEventMousePosGLFW((int)xPos, (int)yPos)) return true;

	if (GLApplication::onMouseMoveEvent(xPos, yPos)) return true;

	return false;
}

//triggered when mouse buttons are pressed
bool MySpotMiniApp::onMouseButtonEvent(int button, int action, int mods, double xPos, double yPos) {
	perturbationForce = V3D();
	int shiftDown = glfwGetKey(this->glfwWindow, GLFW_KEY_LEFT_SHIFT);
	if (GlobalMouseState::lButtonPressed && shiftDown == GLFW_PRESS) {
		setPerturbationForceFromMouseInput(xPos, yPos);
		return true;
	}

	if (showMenus)
		if (TwEventMouseButtonGLFW(button, action)) {
			return true;
		}

	if (GLApplication::onMouseButtonEvent(button, action, mods, xPos, yPos)) return true;

	return false;
}

//triggered when using the mouse wheel
bool MySpotMiniApp::onMouseWheelScrollEvent(double xOffset, double yOffset) {
	if (GLApplication::onMouseWheelScrollEvent(xOffset, yOffset)) return true;

	return false;
}

bool MySpotMiniApp::onKeyEvent(int key, int action, int mods) {
	if (GLApplication::onKeyEvent(key, action, mods)) return true;

	return false;
}

bool MySpotMiniApp::onCharacterPressedEvent(int key, int mods) {
	if (GLApplication::onCharacterPressedEvent(key, mods)) return true;
	return false;
}

void MySpotMiniApp::saveFile(const char* fName) {
	Logger::consolePrint("SAVE FILE: Do not know what to do with file \'%s\'\n", fName);
}

// Draw the App scene - camera transformations, lighting, shadows, reflections, etc apply to everything drawn by this method
void MySpotMiniApp::drawScene() {
	glColor3d(1, 1, 1);
	glDisable(GL_LIGHTING);

	int flags = SHOW_ABSTRACT_VIEW | SHOW_CD_PRIMITIVES;// SHOW_MESH | SHOW_MATERIALS; SHOW_CD_PRIMITIVES

	glEnable(GL_LIGHTING);
	rbEngine->drawRBs(flags);
	rbEngine->drawContactForces();

	glColor3d(1.0, 0.7, 0.7);
	drawArrow(robot->root->getCMPosition(), robot->root->getCMPosition() + perturbationForce, 0.01, 12);

	glDisable(GL_LIGHTING);

	if (drawControllerDebug)
		controller->draw();

//	glTranslated(0.25, 0, 0);
//	motionPlan->drawMotionPlan(stridePhase, 0, true, true, true, false);
}

P3D MySpotMiniApp::getCameraTarget() { 
	return robot->root->getCMPosition(); 
}

// This is the wild west of drawing - things that want to ignore depth buffer, camera transformations, etc. Not pretty, quite hacky, but flexible. Individual apps should be careful with implementing this method. It always gets called right at the end of the draw function
void MySpotMiniApp::drawAuxiliarySceneInfo() {
	if (ffpViewer){
		ffpViewer->cursorPosition = stridePhase;
		ffpViewer->draw();
	}
}

bool MySpotMiniApp::processCommandLine(const std::string& cmdLine) {
	if (GLApplication::processCommandLine(cmdLine)) return true;

	return false;
}

void MySpotMiniApp::adjustWindowSize(int width, int height) {
	Logger::consolePrint("resize");

	mainWindowWidth = width;
	mainWindowHeight = height;
	// Send the window size to AntTweakBar
	TwWindowSize(width, height);

	setViewportParameters(0, 0, mainWindowWidth, mainWindowHeight);

	int size[2];
	int w, h;
	TwGetParam(mainMenuBar, NULL, "size", TW_PARAM_INT32, 2, size);
	w = (GLApplication::getMainWindowWidth() - size[0]);
	h = GLApplication::getMainWindowHeight();
	setViewportParameters(size[0], 0, w, h);
}

void MySpotMiniApp::setupLights() {
	GLfloat bright[] = { 0.8f, 0.8f, 0.8f, 1.0f };
	GLfloat mediumbright[] = { 0.3f, 0.3f, 0.3f, 1.0f };

	glLightfv(GL_LIGHT1, GL_DIFFUSE, bright);
	glLightfv(GL_LIGHT2, GL_DIFFUSE, mediumbright);
	glLightfv(GL_LIGHT3, GL_DIFFUSE, mediumbright);
	glLightfv(GL_LIGHT4, GL_DIFFUSE, mediumbright);


	GLfloat light0_position[] = { 0.0f, 10000.0f, 10000.0f, 0.0f };
	GLfloat light0_direction[] = { 0.0f, -10000.0f, -10000.0f, 0.0f };

	GLfloat light1_position[] = { 0.0f, 10000.0f, -10000.0f, 0.0f };
	GLfloat light1_direction[] = { 0.0f, -10000.0f, 10000.0f, 0.0f };

	GLfloat light2_position[] = { 0.0f, -10000.0f, 0.0f, 0.0f };
	GLfloat light2_direction[] = { 0.0f, 10000.0f, -0.0f, 0.0f };

	GLfloat light3_position[] = { 10000.0f, -10000.0f, 0.0f, 0.0f };
	GLfloat light3_direction[] = { -10000.0f, 10000.0f, -0.0f, 0.0f };

	GLfloat light4_position[] = { -10000.0f, -10000.0f, 0.0f, 0.0f };
	GLfloat light4_direction[] = { 10000.0f, 10000.0f, -0.0f, 0.0f };


	glLightfv(GL_LIGHT0, GL_POSITION, light0_position);
	glLightfv(GL_LIGHT1, GL_POSITION, light1_position);
	glLightfv(GL_LIGHT2, GL_POSITION, light2_position);
	glLightfv(GL_LIGHT3, GL_POSITION, light3_position);
	glLightfv(GL_LIGHT4, GL_POSITION, light4_position);


	glLightfv(GL_LIGHT0, GL_SPOT_DIRECTION, light0_direction);
	glLightfv(GL_LIGHT1, GL_SPOT_DIRECTION, light1_direction);
	glLightfv(GL_LIGHT2, GL_SPOT_DIRECTION, light2_direction);
	glLightfv(GL_LIGHT3, GL_SPOT_DIRECTION, light3_direction);
	glLightfv(GL_LIGHT4, GL_SPOT_DIRECTION, light4_direction);


	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHT1);
	glEnable(GL_LIGHT2);
	glEnable(GL_LIGHT3);
	glEnable(GL_LIGHT4);
}
