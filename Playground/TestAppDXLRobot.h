#pragma once

#include <GUILib/GLApplication.h>
#include <RobotDesignerLib/RobotDesignWindow.h>
#include <string>
#include <map>
#include <GUILib/TranslateWidget.h>
#include <RBSimLib/AbstractRBEngine.h>
#include <RBSimLib/WorldOracle.h>
#include <GUILib/GLWindow3D.h>
#include <GUILib/GLWindow3DWithMesh.h>
#include <GUILib/GLWindowContainer.h>
#include <RobotDesignerLib/LocomotionEngineMotionPlan.h>
#include <RobotDesignerLib/LocomotionEngine.h>
#include <RobotDesignerLib/FootFallPatternViewer.h>
#include <RobotDesignerLib/LocomotionEngineManagerGRF.h>
#include <RobotDesignerLib/LocomotionEngineManagerIP.h>
#include <RobotDesignerLib/PositionBasedRobotController.h>
#include <GUILib/PlotWindow.h>
#include "TestAppDynamixel.h"

/**
 * Robot Design and Simulation interface
 */
class TestAppDXLRobot : public TestAppDynamixel {
private:
	LocomotionEngineMotionPlan* motionPlan = NULL;
	PositionBasedRobotController* controller = NULL;
	WorldOracle* worldOracle = NULL;

	double stridePhase = 0;
	double simTimeStep = 1/500.0;

public:

	// constructor
	TestAppDXLRobot();
	// destructor
	virtual ~TestAppDXLRobot(void);
	// Run the App tasks
	virtual void process();
	// Restart the application.
	virtual void restart();

	void loadRobot(const char* fName);
	virtual void loadFile(const char* fName);
};
