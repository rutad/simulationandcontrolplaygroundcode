#include <GUILib/GLUtils.h>
#include "TestAppShaders.h"
#include <GUILib/GLMesh.h>
#include <GUILib/GLContentManager.h>
#include <MathLib/MathLib.h>


int nSphereDiscretizaiton = 4;

//#define V2

TestAppShaders::TestAppShaders() {
	setWindowTitle("Test Application for shaders and materials...");

	TwAddSeparator(mainMenuBar, "sep2", "");
	
	TwAddVarRW(mainMenuBar, "nSphereElems", TW_TYPE_INT32, &nSphereDiscretizaiton, "");

	//add menu entries for the objects/textures that will be loaded...
	TwAddVarRW(mainMenuBar, "LoadedObjs", TwDefineEnumFromString("ObjFiles", "No OBJs loaded"), &selectedObjFile, NULL);
	TwAddVarRW(mainMenuBar, "LoadedMaterials", TwDefineEnumFromString("Materials", "No materials loaded"), &selectedMaterialTexture, NULL);
    
	TwAddVarRW(mainMenuBar, "obj color", TW_TYPE_COLOR4F, &modelColor, " label='model color' ");
}

TestAppShaders::~TestAppShaders(void){
}


//triggered when mouse moves
bool TestAppShaders::onMouseMoveEvent(double xPos, double yPos) {
	if (tWidget.onMouseMoveEvent(xPos, yPos) == true) return true;
	if (GLApplication::onMouseMoveEvent(xPos, yPos) == true) return true;

	return false;
}

//triggered when mouse buttons are pressed
bool TestAppShaders::onMouseButtonEvent(int button, int action, int mods, double xPos, double yPos) {
	if (tWidget.onMouseButtonEvent(button, action, mods, xPos, yPos) == true) return true;

	if (GLApplication::onMouseButtonEvent(button, action, mods, xPos, yPos)) return true;

	if (mods & GLFW_MOD_SHIFT) {
		lastClickedRay = getRayFromScreenCoords(xPos, yPos);
		return true;
	}

	return false;
}

//triggered when using the mouse wheel
bool TestAppShaders::onMouseWheelScrollEvent(double xOffset, double yOffset) {
	if (GLApplication::onMouseWheelScrollEvent(xOffset, yOffset)) return true;

	return false;
}

bool TestAppShaders::onKeyEvent(int key, int action, int mods) {
	if (GLApplication::onKeyEvent(key, action, mods)) return true;

	return false;
}

bool TestAppShaders::onCharacterPressedEvent(int key, int mods) {
	if (GLApplication::onCharacterPressedEvent(key, mods)) return true;

	return false;
}

void TestAppShaders::loadFile(const char* fName) {
	Logger::consolePrint("Loading file \'%s\'...\n", fName);
	std::string fileName;
	fileName.assign(fName);

	std::string fNameExt = fileName.substr(fileName.find_last_of('.') + 1);

	if (fNameExt.compare("obj") == 0) {
		loadedObjs.push_back(fileName);
		selectedObjFile = loadedObjs.size() - 1;
		generateMenuEnumFromFileList("MainMenuBar/LoadedObjs", loadedObjs);
		return;
	}

	if (fNameExt.compare("bmp") == 0) {
		materialTextures.push_back(fileName);
		selectedMaterialTexture = materialTextures.size() - 1;
		generateMenuEnumFromFileList("MainMenuBar/LoadedMaterials", materialTextures);
		return;
	}

}

void TestAppShaders::saveFile(const char* fName) {
	Logger::consolePrint("SAVE FILE: Do not know what to do with file \'%s\'\n", fName);
}


// Run the App tasks
void TestAppShaders::process() {
	//do the work here...
}

// Draw the App scene - camera transformations, lighting, shadows, reflections, etc apply to everything drawn by this method
void TestAppShaders::drawScene() {
	glDisable(GL_LIGHTING);
	glDisable(GL_TEXTURE_2D);
	glColor3d(1,1,1);


#ifdef V2
	showGroundPlane = false;
	bgColor[0] = bgColor[1] = bgColor[2] = 1;

	std::map<std::string, std::string> shaderMap;

	if (loadedObjs.size() > 0 && materialTextures.size() > 0) {
		shaderMap[loadedObjs[selectedObjFile]] = materialTextures[selectedMaterialTexture];
	}

	if (loadedObjs.size() > 0) {
		glScaled(0.01, 0.01, 0.01);
		for (uint i = 0; i < loadedObjs.size(); i++) {
			GLMesh* theMesh = GLContentManager::getGLMesh(loadedObjs[i].c_str());
			
			auto it = shaderMap.find(loadedObjs[i]);
			GLShaderMaterial tmpMat;
			if (it != shaderMap.end()) {
				tmpMat.setShaderProgram(GLContentManager::getShaderProgram("matcap"));
				std::string textureFileName = it->second;
				tmpMat.setTextureParam(textureFileName.c_str(), GLContentManager::getTexture(textureFileName.c_str()));
				theMesh->setMaterial(tmpMat);
			}

			theMesh->drawMesh();
		}
//		std::string objFileName = loadedObjs[selectedObjFile];
//		if (objFileName.length() > (uint)0) {
//			GLMesh* theMesh = GLContentManager::getGLMesh(objFileName.c_str());
//			theMesh->setMaterial(tmpMat);
//			theMesh->drawMesh();
//		}
	}
#else

	lastClickedRay.draw();

	glEnable(GL_LIGHTING);

	P3D p;
	if (IS_ZERO(lastClickedRay.getDistanceToPlane(Globals::groundPlane, &p))) {
		drawSphere(p, 0.05, 7);
	}

	GLShaderMaterial tmpMat;
	tmpMat.r = modelColor[0]; tmpMat.g = modelColor[1]; tmpMat.b = modelColor[2]; tmpMat.a = modelColor[3];
	if (materialTextures.size() > 0) {
		tmpMat.setShaderProgram(GLContentManager::getShaderProgram("matcap"));
		std::string textureFileName = materialTextures[selectedMaterialTexture];
		tmpMat.setTextureParam(textureFileName.c_str(), GLContentManager::getTexture(textureFileName.c_str()));
	}
	else {
//		FILE* fp = fopen("../data/shaders/materials/horseTexNM.mat", "r");
//		glScaled(0.01, 0.01, 0.01);

		FILE* fp = fopen("../data/shaders/materials/toon.mat", "r");
		tmpMat.readFromFile(fp);
		fclose(fp);
	}

	if (loadedObjs.size() > 0) {
		std::string objFileName = loadedObjs[selectedObjFile];
		if (objFileName.length() > (uint)0) {
			GLMesh* theMesh = GLContentManager::getGLMesh(objFileName.c_str());
			theMesh->setMaterial(tmpMat);
			theMesh->drawMesh();
		}
	}
	else {
		tmpMat.apply();
		drawSphere(P3D(0,1,0), 0.5, nSphereDiscretizaiton);
//		drawCylinder(P3D(0, 1, 0), P3D(0,2,0), 0.5, nSphereDiscretizaiton);

		tmpMat.end();
	}

	tWidget.draw();
	
/*
	Quaternion q;

	glColor3d(1, 0, 0);
	drawCylinder(P3D(0.0, 0, 0), q.rotate(V3D(1, 0, 0)), 0.05);
	glColor3d(0, 1, 0);
	drawCylinder(P3D(0, 0.0, 0), q.rotate(V3D(0, 1, 0)), 0.05);
	glColor3d(0, 0, 1);
	drawCylinder(P3D(0, 0, 0.0), q.rotate(V3D(0, 0, 1)), 0.05);

	Quaternion qRot;
	qRot.setRotationFrom(V3D(0, 0, 1).cross(V3D(1, 0, 0)), V3D(0, 0, 1), V3D(1, 0, 0));

	static double interpVal = 0;
	static double increment = 0.005;

	interpVal += increment;

	if (interpVal >= 1 || interpVal <= 0) increment *= -1;

	Quaternion qInterp = q.sphericallyInterpolateWith(qRot, interpVal);

	glColor3d(1, 0, 0);
	drawCylinder(P3D(0.0, 0, 0), qInterp.rotate(V3D(1.1, 0, 0)), 0.03);
	glColor3d(0, 1, 0);
	drawCylinder(P3D(0, 0.0, 0), qInterp.rotate(V3D(0, 1.1, 0)), 0.03);
	glColor3d(0, 0, 1);
	drawCylinder(P3D(0, 0, 0.0), qInterp.rotate(V3D(0, 0, 1.1)), 0.03);
*/
#endif

}

// This is the wild west of drawing - things that want to ignore depth buffer, camera transformations, etc. Not pretty, quite hacky, but flexible. Individual apps should be careful with implementing this method. It always gets called right at the end of the draw function
void TestAppShaders::drawAuxiliarySceneInfo() {

}

// Restart the application.
void TestAppShaders::restart() {

}

bool TestAppShaders::processCommandLine(const std::string& cmdLine) {

	if (GLApplication::processCommandLine(cmdLine)) return true;

	return false;
}

