#pragma once
#include "TransformEngine.h"

class TransformEngine;

class SmoothingEngine
{
public:
	TransformEngine* transformEngine;

	Eigen::Vector3i vertexDim;
	map<GLMesh*, Eigen::VectorXi> vertexIndexMap;
	Eigen::VectorXi duplicateCount;
	MatrixNxM vertexPos;

public:
	SmoothingEngine(TransformEngine* _transformEngine) : transformEngine(_transformEngine) {

	}
	~SmoothingEngine();

	// smoothing
	void smoothVoxelGrid();
	// use mean curvature to smooth mesh.
	void smoothMesh(GLMesh* mesh, double delta, int iterNum = 1);
	void buildVertexIndexMap();
	Eigen::VectorXi getMeshVertexIndex(GLMesh* mesh);
	void smoothVoxelBoundary(int iterNum);
};

