#include <igl/signed_distance.h>
#include "FoldingOptEngine.h"



FoldingOptEngine::~FoldingOptEngine()
{
}

void FoldingOptEngine::prepareForFoldingOrderOpt()
{
	// update capsule radius for accelerating collision detection
	TransformUtils::updateCapsulesMapRadius(transformEngine->RBs, transformEngine->capsulesMap);

	// build SDF map for folding order optimization
	buildSDFMap();
}

void FoldingOptEngine::createInitialFoldingOrder()
{
	Robot* robot = transformEngine->robot;
	ReducedRobotState* unfoldedState = transformEngine->unfoldedState;
	ReducedRobotState* foldedState = transformEngine->foldedState;
	vector<int>& foldingOrder = transformEngine->foldingOrder;

	for (int i = 0; i < robot->getJointCount(); i++)
	{
		if (unfoldedState->getJointRelativeOrientation(i) != foldedState->getJointRelativeOrientation(i))
		{
			foldingOrder.push_back(i);
		}
	}

	if (foldingOrder.empty())
	{
		Logger::consolePrint("No folding detected!");
		return;
	}

	if (!transformEngine->SDFMap.empty())
	{
		transformEngine->minCost = computeOrderCost(foldingOrder);
		Logger::print("Order: ");
		for (uint i = 0; i < foldingOrder.size(); i++)
			Logger::print("%d ", foldingOrder[i]);
		Logger::print("\n");
		Logger::consolePrint("Order Cost: %lf\n", transformEngine->minCost);
	}
}

void FoldingOptEngine::optimizeFoldingOrderGreedy()
{
	if (transformEngine->SDFMap.empty())
	{
		prepareForFoldingOrderOpt();
	}

	double sampleStep = 0.05;

	Robot* robot = transformEngine->robot;
	ReducedRobotState* unfoldedState = transformEngine->unfoldedState;
	ReducedRobotState* foldedState = transformEngine->foldedState;
	vector<int> unfoldingOrder;
	vector<bool> unfoldedJoint(robot->getJointCount(), true);

	for (int i = 0; i < robot->getJointCount(); i++)
	{
		if (unfoldedState->getJointRelativeOrientation(i) != foldedState->getJointRelativeOrientation(i))
			unfoldedJoint[i] = false;
	}

	ReducedRobotState curState(*foldedState);
	while (1)
	{
		double minCost = DBL_MAX;
		int bestJoint = -1;
		ReducedRobotState bestNextState(curState);

		for (int i = 0; i < robot->getJointCount(); i++)
		{
			if (unfoldedJoint[i]) continue;
			double cost = 0;

			ReducedRobotState newState(curState);
			newState.setJointRelativeOrientation(unfoldedState->getJointRelativeOrientation(i), i);
			for (double t = 0; t < 1.0 + 0.5 * sampleStep; t += sampleStep)
			{
				ReducedRobotState tmpState(&curState, &newState, min(1.0, t));
				robot->setState(&tmpState);
				TransformUtils::updateCapsuleMap(transformEngine->capsulesMap);

				cost += computeStateCost();
			}

			if (cost < minCost)
			{
				minCost = cost;
				bestJoint = i;
				bestNextState = newState;
			}
		}

		if (bestJoint >= 0)
		{
			// Logger::print("best joint: %d\n", bestJoint);
			unfoldingOrder.push_back(bestJoint);
			unfoldedJoint[bestJoint] = true;
			curState = bestNextState;
		}
		else {
			break;
		}
	}
	
	// reverse the order
	vector<int> foldingOrder = unfoldingOrder;
	reverse(foldingOrder.begin(), foldingOrder.end());
	transformEngine->foldingOrder = foldingOrder;

	Logger::print("greedy folding order: ");
	for (auto& jIndex : foldingOrder)
		Logger::print("%d ", jIndex);
	Logger::print("\n");
}

void FoldingOptEngine::optimizeFoldingOrder()
{
	if (transformEngine->SDFMap.empty())
	{
		prepareForFoldingOrderOpt();
	}

	vector<int>& foldingOrder = transformEngine->foldingOrder;
	double& minCost = transformEngine->minCost;

	if (foldingOrder.empty()) {
		createInitialFoldingOrder();
		return;
	}
	else if (minCost == DBL_MAX) {
		computeOrderCost(foldingOrder);
	}

	vector<int> newOrder = foldingOrder;
	int i = (int)getRandomNumberInRange(0.0, (double)foldingOrder.size() - 1e-5);
	int j;
	while (1)
	{
		j = (int)getRandomNumberInRange(0.0, (double)foldingOrder.size() - 1e-5);
		if (j != i) break;
	}
	swap(newOrder[i], newOrder[j]);

	double cost = computeOrderCost(newOrder);
	if (cost < minCost)
	{
		foldingOrder = newOrder;
		minCost = cost;
		Logger::print("Order: ");
		for (uint i = 0; i < foldingOrder.size(); i++)
			Logger::print("%d ", foldingOrder[i]);
		Logger::print("\n");
		Logger::consolePrint("Order Cost: %lf\n", minCost);
	}

}

double FoldingOptEngine::computeOrderCost(vector<int>& order)
{
	Robot* robot = transformEngine->robot;
	ReducedRobotState* unfoldedState = transformEngine->unfoldedState;
	ReducedRobotState* foldedState = transformEngine->foldedState;

	double cost = 0;

	vector<ReducedRobotState> animationStates;
	TransformUtils::getAnimationStates(animationStates, order, unfoldedState, foldedState, transformEngine->transParams->sampleStep);

	for (uint i = 0; i < animationStates.size(); i++)
	{
		robot->setState(&animationStates[i]);
		TransformUtils::updateCapsuleMap(transformEngine->capsulesMap);

		cost += computeStateCost();
	}

	return cost;
}

double FoldingOptEngine::computeStateCost()
{
	vector<RigidBody*>& RBs = transformEngine->RBs;
	map<RigidBody*, RigidBody*>& parentMap = transformEngine->parentMap;

	double cost = 0;

	for (uint i = 0; i < RBs.size() - 1; i++)
		for (uint j = i + 1; j < RBs.size(); j++)
		{
			if (parentMap[RBs[i]] == RBs[j] || parentMap[RBs[j]] == RBs[i])
				continue;

			cost += computeSDFCollisionBetweenRB(RBs[i], RBs[j]);
		}

	return cost;
}

double FoldingOptEngine::computeStateCostCapsules()
{
	vector<RigidBody*>& RBs = transformEngine->RBs;
	map<RigidBody*, RigidBody*>& parentMap = transformEngine->parentMap;

	double cost = 0;

	for (uint i = 0; i < RBs.size() - 1; i++)
		for (uint j = i + 1; j < RBs.size(); j++)
		{
			if (parentMap[RBs[i]] == RBs[j] || parentMap[RBs[j]] == RBs[i])
				continue;

			cost += computeCapsuleCollisionBetweenRB(RBs[i], RBs[j]);
		}

	return cost;
}

double FoldingOptEngine::computeCapsuleCollisionBetweenRB(RigidBody* RB_i, RigidBody* RB_j)
{
	double cost = 0;

	vector<Capsule>& capsules_i = transformEngine->capsulesMap[RB_i];
	vector<Capsule>& capsules_j = transformEngine->capsulesMap[RB_j];

	for (uint i = 0; i < capsules_i.size(); i++)
		for (uint j = 0; j < capsules_j.size(); j++)
		{
			cost += capsules_i[i].collisionWithCapsule(capsules_j[j]);
		}

	return cost;
}


void FoldingOptEngine::buildSDFMap()
{
	auto& SDFMap = transformEngine->SDFMap;
	auto& robotMeshes = transformEngine->robotMeshes;
	auto& RBs = transformEngine->RBs;
	double LSStepSize = transformEngine->transParams->LSStepSize;

	SDFMap.clear();

	for (uint i = 0; i < RBs.size(); i++)
	{
		RigidBody* rb = RBs[i];
		if (robotMeshes.count(rb) == 0) continue;

		GLMesh* mesh = robotMeshes[rb];

		AxisAlignedBoundingBox bbox = mesh->calBoundingBox();
		bbox.setbmax(bbox.bmax() + V3D(1, 1, 1) * 5 * LSStepSize);
		bbox.setbmin(bbox.bmin() + V3D(1, 1, 1) * -5 * LSStepSize);

		SDFMap[rb] = LevelSet(bbox, LSStepSize);
		LevelSet* tmpGrid = &SDFMap[rb];
		Logger::print("SDF-VertexNum:%d %d %d\n", tmpGrid->vertexNum[0], tmpGrid->vertexNum[1], tmpGrid->vertexNum[2]);

		Eigen::MatrixXd V, N;
		Eigen::MatrixXi F;
		mesh->getMeshMatrices(V, F, N);

		Eigen::MatrixXd P;
		P.resize(tmpGrid->totalVertexNum, 3);
		int index = 0;
		for (int z = 0; z < tmpGrid->vertexNum[2]; z++)
			for (int y = 0; y < tmpGrid->vertexNum[1]; y++)
				for (int x = 0; x < tmpGrid->vertexNum[0]; x++) {
					P.row(index++) = tmpGrid->getVertex(x, y, z);
				}

		Eigen::VectorXd S;
		Eigen::VectorXi I;
		Eigen::MatrixXd C;
		igl::signed_distance(P, V, F, igl::SIGNED_DISTANCE_TYPE_WINDING_NUMBER, S, I, C, N);

		for (int i = 0; i < S.size(); i++)
		{
			tmpGrid->setData(i, S[i]);
		}
	}
}

double FoldingOptEngine::computeSDFCollisionBetweenRB(RigidBody* RB_i, RigidBody* RB_j)
{
	map<RigidBody*, LevelSet>& SDFMap = transformEngine->SDFMap;

	if (computeCapsuleCollisionBetweenRB(RB_i, RB_j) == 0) return 0;
	if (SDFMap.count(RB_i) == 0 || SDFMap.count(RB_j) == 0)	return 0;


	double cost = 0;
	LevelSet* SDF_i = &SDFMap[RB_i];
	LevelSet* SDF_j = &SDFMap[RB_j];
	if (SDF_i->totalVertexNum > SDF_j->totalVertexNum)
	{
		swap(SDF_i, SDF_j);
		swap(RB_i, RB_j);
	}

	for (int z = 0; z < SDF_i->vertexNum[2]; z++)
		for (int y = 0; y < SDF_i->vertexNum[1]; y++)
			for (int x = 0; x < SDF_i->vertexNum[0]; x++) {
				double data = SDF_i->getData(x, y, z);
				if (data > 0 || data < -SDF_i->stepSize) continue;

				// get voxel center position
				P3D c = SDF_i->getVertex(x, y, z);
				// tranform the center into RB_i's local coordiante.
				c = RB_j->getLocalCoordinates(RB_i->getWorldCoordinates(c));

				if (SDF_j->isInside(c[0], c[1], c[2]))
				{
					double val = SDF_j->interpolateData(c[0], c[1], c[2]);
					cost -= min(0.0, val);
				}
			}


	return cost;
}

void FoldingOptEngine::debugSDFCollisionBetweenRB(RigidBody* RB_i, RigidBody* RB_j)
{
	map<RigidBody*, LevelSet>& SDFMap = transformEngine->SDFMap;

	if (computeCapsuleCollisionBetweenRB(RB_i, RB_j) == 0) return;

	double cost = 0;
	LevelSet* SDF_i = &SDFMap[RB_i];
	LevelSet* SDF_j = &SDFMap[RB_j];

	P3D sp_j = SDF_j->startPoint;

	for (int z = 0; z < SDF_i->vertexNum[2]; z++)
		for (int y = 0; y < SDF_i->vertexNum[1]; y++)
			for (int x = 0; x < SDF_i->vertexNum[0]; x++) {
				double data = SDF_i->getData(x, y, z);
				if (data > 0 || data < -SDF_i->stepSize) continue;

				// get voxel center position
				P3D c = SDF_i->getVertex(x, y, z);
				// tranform the center into RB_i's local coordiante.
				c = RB_j->getLocalCoordinates(RB_i->getWorldCoordinates(c));

				if (SDF_j->isInside(c[0], c[1], c[2]))
				{
					double val = SDF_j->interpolateData(c[0], c[1], c[2]);
					if (val < 0)
					{
						SDF_i->setData(x, y, z, 100.0);
					}
				}
			}


	return;
}