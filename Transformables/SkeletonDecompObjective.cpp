#include "SkeletonDecompObjective.h"
#include "TransformEngine.h"

SkeletonDecompObjective::SkeletonDecompObjective(SkeletonFoldingPlan* mp, const std::string& objectiveDescription, double weight){
	plan = mp;
	this->description = objectiveDescription;
	this->weight = weight;
}

SkeletonDecompObjective::~SkeletonDecompObjective(void){
}

double SkeletonDecompObjective::computeValue(const dVector& p){

	double retVal = 0;
	GeneralizedCoordinatesRobotRepresentation* gcRobot = plan->gcRobotRepresentation;
	TransformEngine* transformEngine = (TransformEngine*)plan->transformEngine;

	auto& RBs = transformEngine->RBs;
	auto& capsulesMap = transformEngine->capsulesMap;
	VoxelGrid<int>* voxelGrid = transformEngine->voxelGrid;
	double voxelSize = transformEngine->transParams->voxelSize;
	double sktInsideThreshold = transformEngine->transParams->sktInsideThreshold;
	P3D sp = voxelGrid->startPoint;
	GLMesh* MCMesh = transformEngine->MCMesh;

	for (int i = 0; i < MCMesh->getVertexCount(); i++)
	{
		P3D v = MCMesh->getVertex(i);
		Eigen::Vector3i vIndex;
		for (int k = 0; k < 3; k++)
			vIndex[k] = (int)((v[k] - sp[k]) / voxelSize);

		int rbId = voxelGrid->getData(vIndex[0], vIndex[1], vIndex[2]);
		if (rbId < 0) {
			Logger::print("invalid rbId: %d\n", rbId);
			retVal = 1e4;
			break;
		}

		RigidBody* rb = RBs[rbId];
		vector<Capsule>& capsules = transformEngine->capsulesMap[rb];
		double dist = TransformUtils::getClosestDistToCapsules(v, capsules);
		/*double diff = dist - sktInsideThreshold;
		double alpha = diff < sktInsideThreshold ? 1e1 : 1;

		retVal += alpha * diff * diff;*/

		retVal += dist * dist;
	}

	return retVal * weight;
}