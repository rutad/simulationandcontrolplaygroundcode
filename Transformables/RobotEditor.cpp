#include "RobotEditor.h"



RobotEditor::RobotEditor(TransformEngine* _transformEngine, Robot* _robot)
{
	transformEngine = _transformEngine;
	robot = _robot;
	robotDesign = new MorphologicalRobotDesign(robot);
	pointEEPath = "../data/robotDesigner/motorMeshes/XL-320_PointFoot.obj";
}

RobotEditor::~RobotEditor()
{
	delete robotDesign;
}

void RobotEditor::getNearestJoints()
{
	for (int i = 0; i < robot->getRigidBodyCount(); i++)
	{
		RigidBody* rb = robot->getRigidBody(i);
		origMeshTrans[rb] = rb->meshTransformations;
		nearestJoints[rb] = vector<int>();

		for (uint j = 0; j < rb->meshTransformations.size(); j++)
		{
			P3D T(rb->meshTransformations[j].T);
			double minDist = DBL_MAX;
			int minJoint = 0;

			for (int k = 0; k < (int)rb->cJoints.size(); k++)
			{
				double dist = (rb->cJoints[k]->pJPos - T).norm();
				if (dist < minDist)
				{
					minDist = dist;
					minJoint = k + 1;
				}
			}

			for (int k = 0; k < (int)rb->pJoints.size(); k++)
			{
				double dist = (rb->pJoints[k]->cJPos - T).norm();
				if (dist < minDist)
				{
					minDist = dist;
					minJoint = -k - 1;
				}
			}
			nearestJoints[rb].push_back(minJoint);
		}
	}
}

void RobotEditor::scalePickedRobot(RigidBody* rb, double scale, bool changeBodyLength)
{
	auto& RBIndexMap = transformEngine->RBIndexMap;
	vector<double> params;
	robotDesign->getCurrentSetOfParameters(params);
	int id = RBIndexMap[rb];

	if (id > 0)
	{
		params[id + 1] *= scale;
	}
	else {
		int index = changeBodyLength ? 1 : 0;
		params[index] *= scale;		
	}

	robotDesign->setParameters(params);

	adjustMeshTransForRB(rb);

	transformEngine->robotObsolete = true;
}

void RobotEditor::scalePickedRobotSym(RigidBody* rb, double scale, bool changeBodyLength)
{
	auto& jointSymmetryMap = transformEngine->jointSymmetryMap;
	scalePickedRobot(rb, scale, changeBodyLength);

	// scale the symmetric bone
	if (rb != robot->getRoot())
	{
		RigidBody* symRB = NULL;
		auto itr = jointSymmetryMap.find(rb->pJoints[0]->jIndex);
		if (itr != jointSymmetryMap.end()) {
			symRB = robot->getJoint(itr->second)->child;
			scalePickedRobot(symRB, scale, changeBodyLength);
		}
	}
}

void RobotEditor::adjustMeshTransForRB(RigidBody* rb)
{
	auto& rbOrigMeshes = transformEngine->RBOrigMeshes[rb];
	auto& rbOrigMeshTrans = origMeshTrans[rb];
	auto& rbNearestJoints = nearestJoints[rb];
	auto& initialMorphology = robotDesign->initialMorphology;

	for (uint i = 0; i < rbOrigMeshTrans.size(); i++)
	{
		if (rbOrigMeshes[i]->path == pointEEPath) continue;

		int NJIndex = rbNearestJoints[i];
		P3D origJPos = NJIndex > 0 ? initialMorphology[rb->cJoints[NJIndex - 1]->jIndex].pJPos : initialMorphology[rb->pJoints[-NJIndex - 1]->jIndex].cJPos;
		P3D JPos = NJIndex > 0 ? rb->cJoints[NJIndex - 1]->pJPos : rb->pJoints[-NJIndex - 1]->cJPos;

		rb->meshTransformations[i].T = rbOrigMeshTrans[i].T + (JPos - origJPos);
	}
}

void RobotEditor::adjustMeshTrans()
{
	for (int k = 0; k < robot->getRigidBodyCount(); k++)
	{
		RigidBody* rb = robot->getRigidBody(k);
		adjustMeshTransForRB(rb);
	}
}

void RobotEditor::saveParamsToFile(const char* fName)
{
	vector<double> params;
	robotDesign->getCurrentSetOfParameters(params);

	FILE* fp = fopen(fName, "w+");
	fprintf(fp, "%d\n", params.size());
	for (uint i = 0; i < params.size(); i++)
	{
		fprintf(fp, "%lf ", params[i]);
	}
	fclose(fp);
}

void RobotEditor::loadParamsFromFile(const char* fName)
{
	robotDesign->loadParamsFromFile(fName);

	adjustMeshTrans();

	transformEngine->robotObsolete = true;
}
