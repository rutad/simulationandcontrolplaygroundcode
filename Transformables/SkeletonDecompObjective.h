#pragma once

#include <OptimizationLib/ObjectiveFunction.h>
#include <MathLib/Matrix.h>
#include "SkeletonFoldingPlan.h"

class SkeletonDecompObjective : public ObjectiveFunction {

public:
	SkeletonDecompObjective(SkeletonFoldingPlan* mp, const std::string& objectiveDescription, double weight);
	virtual ~SkeletonDecompObjective(void);

	virtual double computeValue(const dVector& p);

private:
	//the energy function operates on a motion plan...
	SkeletonFoldingPlan* plan;
};

