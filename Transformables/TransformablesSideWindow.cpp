#include "TransformablesSideWindow.h"
#include <GUILib/GLUtils.h>
#include <GUILib/GLMesh.h>
#include <GUILib/GLContentManager.h>
#include <MathLib/MathLib.h>
#include <MathLib/Matrix.h>
#include <GUILib/GLTrackingCamera.h>



TransformablesSideWindow::TransformablesSideWindow(int x, int y, int w, int h, GLApplication* glApp) : GLWindow3D(x, y, w, h){
	//setWindowTitle("Test AppRobotDesignerlication...");
	this->glApp = glApp;

	tWidget = new TranslateWidget();
	rWidget = new RotateWidgetV2();
	rWidget->visible = false;
	tWidget->visible = false;

	camera->onMouseWheelScrollEvent(0, 10.0);

}

TransformablesSideWindow::~TransformablesSideWindow(void) {

}


void TransformablesSideWindow::setupLights() {
	GLfloat bright[] = { 0.6f, 0.6f, 0.6f, 1.0f };
	GLfloat mediumbright[] = { 0.3f, 0.3f, 0.3f, 1.0f };

	glLightfv(GL_LIGHT0, GL_DIFFUSE, bright);
	glLightfv(GL_LIGHT1, GL_DIFFUSE, bright);
	glLightfv(GL_LIGHT2, GL_DIFFUSE, bright);
	glLightfv(GL_LIGHT3, GL_DIFFUSE, bright);
	glLightfv(GL_LIGHT4, GL_DIFFUSE, bright);
	glLightfv(GL_LIGHT5, GL_DIFFUSE, bright);


	GLfloat light0_position[] = { 0.0f, 10000.0f, 0.0f, 0.0f };
	GLfloat light0_direction[] = { 0.0f, -10000.0f, -0.0f, 0.0f };

	GLfloat light1_position[] = { 10000.0f, 0.0f, 0.0f, 0.0f };
	GLfloat light1_direction[] = { -10000.0f, 0.0f, 0.0f, 0.0f };

	GLfloat light2_position[] = { -10000.0f, 0.0f, 0.0f, 0.0f };
	GLfloat light2_direction[] = { 10000.0f, 0.0f, 0.0f, 0.0f };

	GLfloat light3_position[] = { 0.0f, 0.0f, -10000.0f,  0.0f };
	GLfloat light3_direction[] = { 0.0f, 0.0f, 10000.0f,  0.0f };

	GLfloat light4_position[] = { 0.0f, 0.0f, 10000.0f,  0.0f };
	GLfloat light4_direction[] = { 0.0f, 0.0f, -10000.0f,  0.0f };

	GLfloat light5_position[] = { 0.0f, -10000.0f, 0.0f, 0.0f };
	GLfloat light5_direction[] = { 0.0f, 10000.0f, -0.0f, 0.0f };


	glLightfv(GL_LIGHT0, GL_POSITION, light0_position);
	glLightfv(GL_LIGHT1, GL_POSITION, light1_position);
	glLightfv(GL_LIGHT2, GL_POSITION, light2_position);
	glLightfv(GL_LIGHT3, GL_POSITION, light3_position);
	glLightfv(GL_LIGHT4, GL_POSITION, light4_position);
	glLightfv(GL_LIGHT5, GL_POSITION, light5_position);


	glLightfv(GL_LIGHT0, GL_SPOT_DIRECTION, light0_direction);
	glLightfv(GL_LIGHT1, GL_SPOT_DIRECTION, light1_direction);
	glLightfv(GL_LIGHT2, GL_SPOT_DIRECTION, light2_direction);
	glLightfv(GL_LIGHT3, GL_SPOT_DIRECTION, light3_direction);
	glLightfv(GL_LIGHT4, GL_SPOT_DIRECTION, light4_direction);
	glLightfv(GL_LIGHT5, GL_SPOT_DIRECTION, light5_direction);


	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHT1);
	glEnable(GL_LIGHT2);
	glEnable(GL_LIGHT3);
	glEnable(GL_LIGHT4);
	glEnable(GL_LIGHT5);

}

bool TransformablesSideWindow::pickJoint(double xPos, double yPos)
{
	Ray mouseRay = camera->getRayFromScreenCoords(xPos, yPos);
	int tmpPick = NOJOINT_ID;
	double closestDistance = 0.01;

	P3D rootP = robot->getRoot()->state.position;
	double distance = mouseRay.getDistanceToPoint(rootP);
	if (distance < 0.02)
	{
		pickedJoint = ROOT_ID;
		return true;
	}

	for (int i = 0; i < robot->getJointCount(); i++)
	{
		P3D jPos = robot->getJoint(i)->getWorldPosition();
		distance = mouseRay.getDistanceToPoint(jPos, NULL);
		//Logger::print("Dist: %lf   ", distance);
		if (distance < closestDistance)
		{
			tmpPick = i;
			closestDistance = distance;
		}
	}
	//Logger::print("joint: %d", tmpPick);
	pickedJoint = tmpPick;
	if (tmpPick == NOJOINT_ID) {
		return false;
	}
	else {
		return true;
	}

	return false;
}

bool TransformablesSideWindow::pickRB(double xPos, double yPos)
{
	Ray mouseRay = camera->getRayFromScreenCoords(xPos, yPos);
	RigidBody* tmpPick = NULL;
	double closestDistance = 0.05;

	// root
	vector<Capsule> capsules;
	TransformUtils::getCapsulesForRigidBody(robot->root, capsules);
	double distance = TransformUtils::getClosestDistToCapsules(mouseRay, capsules);
	if (distance < closestDistance)
	{
		tmpPick = robot->root;
		closestDistance = distance;
	}

	for (int i = 0; i < robot->getJointCount(); i++)
	{
		RigidBody* rb = robot->getJoint(i)->child;
		TransformUtils::getCapsulesForRigidBody(rb, capsules);
		double distance = TransformUtils::getClosestDistToCapsules(mouseRay, capsules);
		if (distance < closestDistance)
		{
			tmpPick = rb;
			closestDistance = distance;
		}
	}


	pickedRB = tmpPick;
	if (tmpPick == NULL) {
		return false;
	}
	else {
		//Logger::consolePrint("%s's thickness: %lf", tmpPick->getName().c_str(), tmpPick->rbProperties.thickness);
		return true;
	}
}

//triggered when mouse moves
bool TransformablesSideWindow::onMouseMoveEvent(double xPos, double yPos) {
	preDraw();
	if (tWidget->onMouseMoveEvent(xPos, yPos)) {
		if (pickedJoint == ROOT_ID) {
			unfoldedState->setPosition(tWidget->pos);
			rWidget->pos = tWidget->pos;
			postDraw();
			return true;
		}
	}

	if (rWidget->onMouseMoveEvent(xPos, yPos)) {
		robot->setState(unfoldedState);
		if (pickedJoint >= 0) {
			robot->getJoint(pickedJoint)->child->state.orientation = rWidget->getOrientation();
			ReducedRobotState tmpState(robot);
			Quaternion qRel = tmpState.getJointRelativeOrientation(pickedJoint);
			if (controlParams->useHingeJoint)
			{
				V3D axis = qRel.v; axis.toUnit();
				V3D rotationAxis = static_cast<HingeJoint*>(robot->getJoint(pickedJoint))->rotationAxis;
				double rotAngle = qRel.getRotationAngle(axis);
				double ang = axis.dot(rotationAxis) * rotAngle;
				qRel = getRotationQuaternion(ang, rotationAxis);

				map<int, int>& jointSymmetryMap = *controlParams->jointSymmetryMap;
				auto itr = jointSymmetryMap.find(pickedJoint);
				if (itr != jointSymmetryMap.end()) {
					int symJoint = itr->second;
					V3D symRotationAxis = static_cast<HingeJoint*>(robot->getJoint(symJoint))->rotationAxis;
					unfoldedState->setJointRelativeOrientation(getRotationQuaternion(-ang, symRotationAxis), symJoint);
				}
			}
			unfoldedState->setJointRelativeOrientation(qRel, pickedJoint);
		}
		else if (pickedJoint == ROOT_ID) {
			robot->getRoot()->state.orientation = rWidget->getOrientation();
			ReducedRobotState tmpState(robot);
			unfoldedState->setOrientation(tmpState.getOrientation());
		}

		postDraw();
		return true;
	}

	postDraw();
	return GLWindow3D::onMouseMoveEvent(xPos, yPos);
}

//triggered when mouse buttons are pressed
bool TransformablesSideWindow::onMouseButtonEvent(int button, int action, int mods, double xPos, double yPos) {
	preDraw();
	if (button == 0) {
		if (action == 1) {
			if (playAnimation) {
				postDraw();
				return true;
			}

			bool res = rWidget->onMouseMoveEvent(xPos, yPos);
			if (res) {
				postDraw();
				return true;
			}

			if (robot)
			{
				if (curState)
					robot->setState(curState);
				else
					robot->setState(unfoldedState);

				res = tWidget->onMouseMoveEvent(xPos, yPos);
				if (res) {
					return true;
				}

				pickedJoint = NOJOINT_ID;
				pickedRB = NULL;
				tWidget->visible = rWidget->visible = false;

				res = pickJoint(xPos, yPos);

				if (res) {
					if (pickedJoint == ROOT_ID)
					{
						tWidget->visible = rWidget->visible = true;
						tWidget->pos = rWidget->pos = robot->getRoot()->state.position;
						rWidget->setOrientation(robot->getRoot()->state.orientation);
					}
					else {
						rWidget->visible = true;
						rWidget->pos = robot->getJoint(pickedJoint)->getWorldPosition();
						rWidget->setOrientation(robot->getJoint(pickedJoint)->child->getOrientation());
					}
					postDraw();
					return true;
				}

				res = pickRB(xPos, yPos);
				if (res) {
					postDraw();
					return true;
				}
			}

		}
		else {

		}
	}
	else {
		if (action == 1 && (mods & GLFW_MOD_CONTROL))
		{
			resetAnimation();
		}
	}

	postDraw();
	return  GLWindow3D::onMouseButtonEvent(button, action, mods, xPos, yPos);
}

void TransformablesSideWindow::resetAnimation()
{
	sIndex = 0;
	playAnimation = false;
	animationStates.clear();
	curState = NULL;
}

//triggered when using the mouse wheel
bool TransformablesSideWindow::onMouseWheelScrollEvent(double xOffset, double yOffset) {
	if (GLWindow3D::onMouseWheelScrollEvent(xOffset, yOffset)) return true;

	return true;
}

bool TransformablesSideWindow::onKeyEvent(int key, int actionI, int mods) {

	return (GLWindow3D::onKeyEvent(key, actionI, mods));
 
	return false;
}

bool TransformablesSideWindow::onCharacterPressedEvent(int key, int mods) {
	if (GLWindow3D::onCharacterPressedEvent(key, mods)) return true;

	return false;
}

// Draw the AppRobotDesigner scene - camera transformations, lighting, shadows, reflections, etc AppRobotDesignerly to everything drawn by this method
void TransformablesSideWindow::drawScene() {
	glColor3d(1, 1, 1);
	glDisable(GL_LIGHTING);
	glPushMatrix();
	glScaled(0.15, 0.15, 0.15);
	drawDesignEnvironmentBox();
	glPopMatrix();

	glEnable(GL_LIGHTING);

	if (robot) {

		drawInterface();

		if (animationStates.empty()){
			robot->setState(unfoldedState);
		}
		else {
			sIndex = min(sIndex, 1 - 1.0 / (double)animationStates.size());
			int index = (int)(sIndex * (double)animationStates.size());
			robot->setState(&animationStates[index]);
			curState = &animationStates[index];
		}

		glEnable(GL_LIGHTING);
		glEnable(GL_CULL_FACE);
		glCullFace(GL_BACK);

		//draw the robot configuration...
		if (controlParams->showRobotAbstractView)
		{
			P3D rootP = robot->getRoot()->state.position;
			glColor4d(1.0, 0.0, 0.0, 0.5);
			drawSphere(rootP, 0.02, 36);

			for (int i = 0; i < robot->getRigidBodyCount(); i++)
				robot->getRigidBody(i)->draw(SHOW_ABSTRACT_VIEW);
		}

		// drawAttachementSegs();
		drawRBOrigMeshes();
		drawSkeletonLSMeshes();

		if (controlParams->showAllThickness)
		{
			for (auto itr = controlParams->colorMap->begin(); itr != controlParams->colorMap->end(); itr++)
			{
				vector<Capsule> capsules;
				TransformUtils::getCapsulesForRigidBody(itr->first, capsules);
				V3D color = itr->second;
				glColor3d(color[0], color[1], color[2]);
				for (uint i = 0; i < capsules.size(); i++)
				{
					double thickness = itr->first->rbProperties.thickness;
					if (controlParams->showCapsuleRadius && !controlParams->transformEngine->capsulesMap.empty())
						thickness = controlParams->transformEngine->capsulesMap[itr->first][i].r;

					drawCylinder(capsules[i].seg.a, capsules[i].seg.b, thickness);
				}
			}
		}

		if (controlParams->showRobotMeshes) {
			auto& robotMeshes = controlParams->transformEngine->robotMeshes;
			if (pickedRB)
			{
				if (robotMeshes.count(pickedRB)) {
					GLMesh* rbMesh = robotMeshes[pickedRB];
					V3D color = (*controlParams->colorMap)[pickedRB];
					glPushMatrix();
					TransformUtils::applyGLTransformForRB(pickedRB);
					if (controlParams->useMaterial)
						rbMesh->setMaterial(controlParams->shellMaterial);
					else {
						GLShaderMaterial defaultMat;
						defaultMat.setColor(color[0], color[1], color[2], 0.6);
						rbMesh->setMaterial(defaultMat);
					}
					rbMesh->drawMesh();
					glPopMatrix();
				}				
			}
			else {
				for (auto itr = robotMeshes.begin(); itr != robotMeshes.end(); itr++)
				{
					RigidBody* rb = itr->first;
					GLMesh* rbMesh = itr->second;
					V3D color = (*controlParams->colorMap)[itr->first];
					glPushMatrix();
					TransformUtils::applyGLTransformForRB(rb);
					if (controlParams->useMaterial)
						rbMesh->setMaterial(controlParams->shellMaterial);
					else {
						GLShaderMaterial defaultMat;
						defaultMat.setColor(color[0], color[1], color[2], 0.6);
						rbMesh->setMaterial(defaultMat);
					}rbMesh->drawMesh();
					glPopMatrix();
				}
			}		
		}
		

		if (playAnimation)
			postAnimation();
	}

	glClear(GL_DEPTH_BUFFER_BIT);

	rWidget->draw();
	tWidget->draw();

	
}

void TransformablesSideWindow::prepareForAnimation(vector<int>& order, ReducedRobotState* _startState, ReducedRobotState* _endState, double _animationStep)
{
	// only keep those joints that have different positions or angles for startState and endState.
	foldOrder.clear();
	startState = _startState;
	endState = _endState;
	animationStep = _animationStep;
	curState = NULL;

	for (uint i = 0; i < order.size(); i++)
	{
		int curJoint = order[i];
		if (curJoint == ROOT_ID)
		{
			if (startState->getPosition() != endState->getPosition()
				|| startState->getOrientation() != endState->getOrientation())
			{
				foldOrder.push_back(curJoint);
			}
		}
		else if (startState->getJointRelativeOrientation(curJoint) != endState->getJointRelativeOrientation(curJoint))
		{
			foldOrder.push_back(curJoint);
		}
	}
	if (foldOrder.empty()) return;

	rWidget->visible = false;
	tWidget->visible = false;
	playAnimation = true;

	sIndex = 0;
	animationStates.clear();
	TransformUtils::getAnimationStates(animationStates, foldOrder, startState, endState, animationStep);
}

void TransformablesSideWindow::prepareForMotionPlanAnimation(vector<ReducedRobotState>& motionPlanStates)
{
	rWidget->visible = false;
	tWidget->visible = false;
	playAnimation = true;
	sIndex = 0;

	animationStates = motionPlanStates;
}

void TransformablesSideWindow::postAnimation()
{
	if (playAnimation) {
		sIndex += 1.0 / (double)animationStates.size();
		if (sIndex > 1 - 1.0 / (double)animationStates.size())
		{
			sIndex = 1 - 1.0 / (double)animationStates.size();
			playAnimation = false;
		}
	}
}


void TransformablesSideWindow::drawInterface()
{
	if (pickedInterface == -1)
		return;

	MeshInterface& meshIF = controlParams->transformEngine->meshInterfaces[pickedInterface];
	RigidBody* rb1 = meshIF.rb1;
	RigidBody* rb2 = meshIF.rb2;
	auto points1 = meshIF.points;
	auto points2 = meshIF.points;

	robot->setState(foldedState);
	for (uint i = 0; i < points1.size(); i++)
	{
		points1[i] = rb1->getLocalCoordinates(points1[i]);
		points2[i] = rb2->getLocalCoordinates(points2[i]);
	}

	robot->setState(unfoldedState);
	for (uint i = 0; i < points1.size(); i++)
	{
		points1[i] = rb1->getWorldCoordinates(points1[i]);
		points2[i] = rb2->getWorldCoordinates(points2[i]);
	}

	for (int i = 0; i < 2; i++)
	{
		vector<P3D> points = i == 0 ? points1 : points2;
		for (uint j = 0; j < points.size(); j++)
		{
			double radius = 0.002;
			glColor4d(1.0, 0.0, 0.0, 1.0);
			if (j == pickedInterfaceP) {
				radius = 0.004;
				glColor4d(0.0, 0.0, 1.0, 1.0);
			}
			drawSphere(points[j], radius, 10);
		}

		glColor4d(0.0, 1.0, 1.0, 1.0);
		for (uint j = 0; j < points.size() - 1; j++)
		{
			drawCylinder(points[j], points[j + 1], 0.001, 10);
		}
		if (meshIF.loop)
		{
			drawCylinder(points.back(), points.front(), 0.001, 10);
		}
	}

	
}

void TransformablesSideWindow::drawRBOrigMeshes()
{
	if (!controlParams->showSkeletonOrigMeshes)
		return;

	set<string> meshFilter = {
		"skeleton", "motor"
	};

	auto& skeletonMeshes = controlParams->transformEngine->skeletonMeshes;

	GLMesh* bodyMesh = controlParams->transformEngine->bodyMesh;
	if (bodyMesh)
	{
		glPushMatrix();
		TransformUtils::applyGLTransformForRB(robot->getRoot());
		bodyMesh->setMaterial(controlParams->skeletonMaterial);
		bodyMesh->drawMesh();
		glPopMatrix();
	}
	

	for (auto& itr : controlParams->transformEngine->RBOrigMeshes)
	{
		RigidBody* rb = itr.first;
		auto& meshVec = itr.second;
		auto& meshTrans = rb->meshTransformations;
		auto& carveMeshes = rb->carveMeshes;
		auto& meshDescriptions = rb->meshDescriptions;

		glPushMatrix();
		TransformUtils::applyGLTransformForRB(rb);
		for (uint i = 0; i < meshVec.size(); i++)
		{
			if (meshFilter.count(meshDescriptions[i]) == 0) {
				continue;
			}
			glPushMatrix();
			meshTrans[i].applyGLMatrixTransform();
			meshVec[i]->setMaterial(controlParams->skeletonMaterial);
			meshVec[i]->drawMesh();
			if (controlParams->showCarveMeshes && rb->carveMeshes[i])
			{
				carveMeshes[i]->getMaterial().setColor(1.0, 0.0, 0.0, 1.0);
				carveMeshes[i]->drawMesh();
			}
			glPopMatrix();
		}
		auto itr = skeletonMeshes.find(rb);
		if (itr != skeletonMeshes.end())
		{
			GLMesh* sMesh = itr->second;
			sMesh->setMaterial(controlParams->skeletonMaterial);
			sMesh->drawMesh();
		}
		glPopMatrix();
	}
}

void TransformablesSideWindow::drawSkeletonLSMeshes()
{
	if (!controlParams->showSkeletonLSMeshes) return;
	
	auto& skeletonLSMeshes = controlParams->transformEngine->fabricationEngine->skeletonLSMeshes;
	for (auto& itr : skeletonLSMeshes)
	{
		RigidBody* rb = itr.first;
		GLMesh* sMesh = itr.second;
		glPushMatrix();
		TransformUtils::applyGLTransformForRB(rb);
		sMesh->getMaterial().setColor(1.0, 0.0, 0.0, 1.0);
		sMesh->drawMesh();
		glPopMatrix();
	}
}

void TransformablesSideWindow::drawAttachementSegs()
{
	auto& attachmentSegs = controlParams->transformEngine->fabricationEngine->attachmentSegs;
	for (auto& itr : attachmentSegs)
	{
		RigidBody* rb = itr.first;
		Segment& seg = itr.second;

		glPushMatrix();
		TransformUtils::applyGLTransformForRB(rb);
		V3D dir = (seg.b - seg.a).normalized();
		glColor4d(0.0, 0.0, 1.0, 1.0);
		drawCylinder(seg.a, seg.a + dir * 0.1, 0.005, 12);
		glPopMatrix();
	}
}

// This is the wild west of drawing - things that want to ignore depth buffer, camera transformations, etc. Not pretty, quite hacky, but flexible. Individual apps should be careful with implementing this method. It always gets called right at the end of the draw function
void TransformablesSideWindow::drawAuxiliarySceneInfo() {
	
	
}
