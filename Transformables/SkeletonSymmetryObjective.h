#pragma once

#include <OptimizationLib/ObjectiveFunction.h>
#include <MathLib/Matrix.h>
#include "SkeletonFoldingPlan.h"

class SkeletonSymmetryObjective : public ObjectiveFunction {

public:
	SkeletonSymmetryObjective(SkeletonFoldingPlan* mp, const std::string& objectiveDescription, double weight);
	virtual ~SkeletonSymmetryObjective(void);

	virtual double computeValue(const dVector& p);
	//virtual void addHessianEntriesTo(DynamicArray<MTriplet>& hessianEntries, const dVector& p);
	virtual void addGradientTo(dVector& grad, const dVector& p);

private:
	//the energy function operates on a motion plan...
	SkeletonFoldingPlan* plan;
	map<int, int>* symmMap;
};

