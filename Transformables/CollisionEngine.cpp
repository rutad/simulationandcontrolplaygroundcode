#include "CollisionEngine.h"




CollisionEngine::~CollisionEngine()
{
}

void CollisionEngine::buildVoxelInfoMap()
{
	auto& RBIndexMap = transformEngine->RBIndexMap;
	auto voxelGrid = transformEngine->voxelGrid;
	auto& RBs = transformEngine->RBs;

	voxelInfoMap.clear();

	for (auto itr = RBIndexMap.begin(); itr != RBIndexMap.end(); itr++)
	{
		voxelInfoMap[itr->first] = RBVoxelInfo();
	}

	// first round initialize N
	int index = 0;
	for (int z = 0; z < voxelGrid->voxelNum[2]; z++)
		for (int y = 0; y < voxelGrid->voxelNum[1]; y++)
			for (int x = 0; x < voxelGrid->voxelNum[0]; x++) {
				int RBIndex = voxelGrid->getData(index);
				if (RBIndex < 0) {
					index++;
					continue;
				}

				P3D center = voxelGrid->getVoxelCenter(x, y, z);
				RBVoxelInfo& voxelInfo = voxelInfoMap[RBs[RBIndex]];
				voxelInfo.N++;
				index++;
			}

	// resize matrices
	for (auto itr = voxelInfoMap.begin(); itr != voxelInfoMap.end(); itr++)
	{
		itr->second.localVoxels.resize(itr->second.N, 3);
		itr->second.worldVoxels.resize(itr->second.N, 3);
		itr->second.voxelIndices.resize(itr->second.N);
		itr->second.voxelFlags.resize(itr->second.N);
		itr->second.voxelFlags.setConstant(1);
	}

	// second round initialize matrices
	index = 0;
	for (int z = 0; z < voxelGrid->voxelNum[2]; z++)
		for (int y = 0; y < voxelGrid->voxelNum[1]; y++)
			for (int x = 0; x < voxelGrid->voxelNum[0]; x++) {
				int RBIndex = voxelGrid->getData(index);
				if (RBIndex < 0) {
					index++;
					continue;
				}

				P3D center = voxelGrid->getVoxelCenter(x, y, z);
				RBVoxelInfo& voxelInfo = voxelInfoMap[RBs[RBIndex]];
				voxelInfo.localVoxels.row(voxelInfo.curIndex) = RBs[RBIndex]->getLocalCoordinates(center);
				voxelInfo.worldVoxels.row(voxelInfo.curIndex) = center;
				voxelInfo.voxelIndices[voxelInfo.curIndex] = index;

				voxelInfo.curIndex++;
				index++;
			}
}


void CollisionEngine::carveVoxelsForCurrentOrder()
{
	auto robot = transformEngine->robot;
	auto unfoldedState = transformEngine->unfoldedState;
	auto foldedState = transformEngine->foldedState;
	double sampleStep = transformEngine->transParams->sampleStep;
	auto& capsulesMap = transformEngine->capsulesMap;
	auto voxelGrid = transformEngine->voxelGrid;
	auto& foldOrder = transformEngine->foldingOrder;

	if (foldOrder.empty())
		return;

	vector<ReducedRobotState> animationStates;
	TransformUtils::getAnimationStates(animationStates, foldOrder, unfoldedState, foldedState, sampleStep);

	for (uint i = 0; i < animationStates.size(); i++)
	{
		robot->setState(&animationStates[i]);
		TransformUtils::updateCapsuleMap(capsulesMap);
		TransformUtils::updateVoxelInfoMap(voxelInfoMap);

		carveVoxelsForCurrentState();
	}

	for (auto itr = voxelInfoMap.begin(); itr != voxelInfoMap.end(); itr++)
	{
		RigidBody* rb = itr->first;
		RBVoxelInfo& voxelInfo = itr->second;
		Eigen::VectorXi& voxelIndices = voxelInfo.voxelIndices;
		Eigen::VectorXi& voxelFlags = voxelInfo.voxelFlags;

		for (int i = 0; i < voxelInfo.N; i++)
		{
			if (voxelFlags[i] == 0)
			{
				voxelGrid->setData(voxelIndices[i], -1);
			}
		}
	}
}

void CollisionEngine::carveVoxelsForCurrentState()
{
	auto& RBs = transformEngine->RBs;

	for (uint i = 0; i < RBs.size() - 1; i++)
		for (uint j = i + 1; j < RBs.size(); j++)
		{
			checkCollisionVoxelsBetweenRB(RBs[i], RBs[j]);
		}
}

bool CollisionEngine::checkCollisionVoxelsBetweenRB(RigidBody* RB_i, RigidBody* RB_j)
{
	double voxelSize = transformEngine->transParams->voxelSize;

	bool res = false;

	RBVoxelInfo& voxelInfo_i = voxelInfoMap[RB_i];
	RBVoxelInfo& voxelInfo_j = voxelInfoMap[RB_j];

	double cost = transformEngine->foldingOptEngine->computeCapsuleCollisionBetweenRB(RB_i, RB_j);

	if (cost == 0) {
		//Logger::print("%s  ", RB_j->getName().c_str());
		return false;
	}


	MatrixNxM& worldVoxels_i = voxelInfo_i.worldVoxels;
	MatrixNxM& worldVoxels_j = voxelInfo_j.worldVoxels;
	Eigen::VectorXi& voxelFlags_i = voxelInfo_i.voxelFlags;
	Eigen::VectorXi& voxelFlags_j = voxelInfo_j.voxelFlags;
	int N_i = voxelInfo_i.N;
	int N_j = voxelInfo_j.N;

	for (int i = 0; i < N_i; i++)
	{
		if (voxelFlags_i[i] == 0) continue;

		bool collision = false;
		for (int j = 0; j < N_j; j++)
		{
			if (voxelFlags_j[j] == 0) continue;

			double dist = (worldVoxels_i.row(i) - worldVoxels_j.row(j)).norm();
			if (dist < 0.5 * voxelSize) {
				collision = true;
				break;
			}
		}

		if (collision) {
			voxelFlags_i[i] = 0;
			res = true;
		}

	}

	return res;
}


void CollisionEngine::getJointCollisionMap(map<Joint*, double>& jointCollisionMap)
{
	buildVoxelInfoMap();

	auto robot = transformEngine->robot;
	auto unfoldedState = transformEngine->unfoldedState;
	auto foldedState = transformEngine->foldedState;
	double sampleStep = transformEngine->transParams->sampleStep;

	for (int i = 0; i < robot->getJointCount(); i++)
	{
		if (unfoldedState->getJointRelativeOrientation(i) == foldedState->getJointRelativeOrientation(i)) continue;
		Joint* joint = robot->getJoint(i);

		vector<ReducedRobotState> animationStates;
		TransformUtils::getAnimationStatesForJoint(animationStates, i, foldedState, unfoldedState, sampleStep);

		double maxCollision = 0;
		for (uint j = 0; j < animationStates.size(); j++)
		{
			robot->setState(&animationStates[j]);
			TransformUtils::updateVoxelInfoMap(voxelInfoMap);

			maxCollision = max(maxCollision, getCollisionForJoint(joint));
		}

		jointCollisionMap[joint] = maxCollision;
		/*if (maxCollision < 0.05)
		{
			jointCollisionMap[joint] = max(maxCollision * 0.5, 0.03);
		}
		else {
			jointCollisionMap[joint] = min(maxCollision, 0.06);
		}*/
		
	}
	
}

double CollisionEngine::getCollisionForJoint(Joint* joint)
{
	double maxCollision = 0;
	double voxelSize = transformEngine->transParams->voxelSize;

	RigidBody* parentRB = joint->parent;
	RigidBody* childRB = joint->child;

	RBVoxelInfo& voxelInfo_i = voxelInfoMap[childRB];
	RBVoxelInfo& voxelInfo_j = voxelInfoMap[parentRB];

	MatrixNxM& worldVoxels_i = voxelInfo_i.worldVoxels;
	MatrixNxM& worldVoxels_j = voxelInfo_j.worldVoxels;
	int N_i = voxelInfo_i.N;
	int N_j = voxelInfo_j.N;

	for (int i = 0; i < N_i; i++)
	{
		for (int j = 0; j < N_j; j++)
		{
			double dist = (worldVoxels_i.row(i) - worldVoxels_j.row(j)).norm();
			if (dist < 0.99 * voxelSize) {
				V3D axis = parentRB->getWorldCoordinates(((HingeJoint*)joint)->rotationAxis);
				P3D jointPos = parentRB->getWorldCoordinates(joint->pJPos);
				P3D center = P3D(V3D(worldVoxels_i.row(i)));

				double dist = TransformUtils::getPointDistToLine(center, jointPos, axis.normalized());
				maxCollision = max(dist, maxCollision);
				break;
			}
		}
	}

	return maxCollision;
}

double CollisionEngine::getCollisionBetweenLSImplicit(LevelSet& parentLS, LevelSet& childLS, RigidBody* parentRB, RigidBody* childRB, vector<ReducedRobotState>& sampleStates, double cylinderR, V3D axis, P3D center)
{
	double collision = 0;
	Robot* robot = transformEngine->robot;
	robot->setState(&sampleStates[0]);
	RBState childInitState = childRB->state;
	Transformation L2W = Transformation(childInitState.orientation.getRotationMatrix(), childInitState.position);

	for (uint i = 1; i < sampleStates.size(); i++)
	{
		robot->setState(&sampleStates[i]);
		RBState childCurState = childRB->state;
		Transformation W2L = Transformation(childCurState.orientation.getRotationMatrix(), childCurState.position).inverse();
		Transformation M = L2W * W2L;

		for (int z = 0; z < parentLS.vertexNum[2]; z++)
			for (int y = 0; y < parentLS.vertexNum[1]; y++)
				for (int x = 0; x < parentLS.vertexNum[0]; x++) {
					double data = parentLS.getData(x, y, z);
					P3D c = parentLS.getVertex(x, y, z);
					double pR = TransformUtils::getPointDistToLine(c, center, axis);
					double childData = childLS.getData(x, y, z);
					if (!(data < 0 || (pR < cylinderR && childData < 0))) continue;

					// get voxel center position

					// tranform the center into RB_i's local coordiante.
					c = M.transform(c);

					if (childLS.isInside(c[0], c[1], c[2]))
					{
						double cR = TransformUtils::getPointDistToLine(c, center, axis);
						double val = childLS.interpolateData(c[0], c[1], c[2]);
						if (val < 0 && cR > cylinderR)
							collision -= min(0.0, val);
					}
				}
	}

	return collision;
}

double CollisionEngine::getCollisionBetweenMeshLS(LevelSet& parentLS, LevelSet& childLS, RigidBody* parentRB, RigidBody* childRB, vector<ReducedRobotState>& sampleStates)
{
	double collision = 0;
	Robot* robot = transformEngine->robot;
	robot->setState(&sampleStates[0]);
	RBState childInitState = childRB->state;
	Transformation W2L = Transformation(childInitState.orientation.getRotationMatrix(), childInitState.position).inverse();
	GLMesh* childMesh = childLS.extractMesh(0);

	for (uint i = 1; i < sampleStates.size(); i++)
	{
		robot->setState(&sampleStates[i]);
		RBState childCurState = childRB->state;
		Transformation L2W(childCurState.orientation.getRotationMatrix(), childCurState.position);
		Transformation M = L2W * W2L;

		for (int i = 0; i < childMesh->getVertexCount(); i++)
		{
			P3D c = childMesh->getVertex(i);

			c = M.transform(c);

			if (parentLS.isInside(c[0], c[1], c[2]))
			{
				double val = parentLS.interpolateData(c[0], c[1], c[2]);
				collision -= min(0.0, val);
			}
		}
	}

	delete childMesh;
	return collision;
}

double CollisionEngine::getCollisionBetweenLS(LevelSet& parentLS, LevelSet& childLS, RigidBody* parentRB, RigidBody* childRB, vector<ReducedRobotState>& sampleStates)
{
	double collision = 0;
	Robot* robot = transformEngine->robot;
	robot->setState(&sampleStates[0]);
	RBState childInitState = childRB->state;
	Transformation W2L = Transformation(childInitState.orientation.getRotationMatrix(), childInitState.position).inverse();

	for (uint i = 1; i < sampleStates.size(); i++)
	{
		robot->setState(&sampleStates[i]);
		RBState childCurState = childRB->state;
		Transformation L2W(childCurState.orientation.getRotationMatrix(), childCurState.position);
		Transformation M = L2W * W2L;

		for (int z = 0; z < childLS.vertexNum[2]; z++)
			for (int y = 0; y < childLS.vertexNum[1]; y++)
				for (int x = 0; x < childLS.vertexNum[0]; x++) {
					double data = childLS.getData(x, y, z);
					if (data >= 0) continue;

					// get voxel center position
					P3D c = childLS.getVertex(x, y, z);
					
					// tranform the center into RB_i's local coordiante.
					c = M.transform(c);

					if (parentLS.isInside(c[0], c[1], c[2]))
					{
						double val = parentLS.interpolateData(c[0], c[1], c[2]);
						collision -= min(0.0, val);
					}
				}
	}

	return collision;
}

double CollisionEngine::getCollisionBetweenLS(LevelSet& parentLS, LevelSet& childLS, vector<RBState>& parentStates, vector<RBState>& childStates)
{
	double collision = 0;
	RBState childInitState = childStates[0];
	Transformation W2L = Transformation(childInitState.orientation.getRotationMatrix(), childInitState.position).inverse();

	for (uint i = 1; i < childStates.size(); i++)
	{
		RBState childCurState = childStates[i];
		Transformation L2W(childCurState.orientation.getRotationMatrix(), childCurState.position);
		Transformation M = L2W * W2L;

		for (int z = 0; z < childLS.vertexNum[2]; z++)
			for (int y = 0; y < childLS.vertexNum[1]; y++)
				for (int x = 0; x < childLS.vertexNum[0]; x++) {
					double data = childLS.getData(x, y, z);
					if (data >= 0) continue;

					// get voxel center position
					P3D c = childLS.getVertex(x, y, z);

					// tranform the center into RB_i's local coordiante.
					c = M.transform(c);

					if (parentLS.isInside(c[0], c[1], c[2]))
					{
						double val = parentLS.interpolateData(c[0], c[1], c[2]);
						collision -= min(0.0, val);
					}
				}
	}

	return collision;
}

double CollisionEngine::getMaxCollisionRadius(LevelSet& parentLS, LevelSet& childLS, RigidBody* parentRB, RigidBody* childRB, vector<ReducedRobotState>& sampleStates, V3D axis, P3D center)
{
	axis.normalize();
	double maxR = 0;
	Robot* robot = transformEngine->robot;
	robot->setState(&sampleStates[0]);
	RBState childInitState = childRB->state;
	Transformation W2L = Transformation(childInitState.orientation.getRotationMatrix(), childInitState.position).inverse();

	for (uint i = 1; i < sampleStates.size(); i++)
	{
		robot->setState(&sampleStates[i]);
		RBState childCurState = childRB->state;
		Transformation L2W(childCurState.orientation.getRotationMatrix(), childCurState.position);
		Transformation M = L2W * W2L;

		for (int z = 0; z < childLS.vertexNum[2]; z++)
			for (int y = 0; y < childLS.vertexNum[1]; y++)
				for (int x = 0; x < childLS.vertexNum[0]; x++) {
					double data = childLS.getData(x, y, z);
					if (data >= 0) continue;

					// get voxel center position
					P3D c = childLS.getVertex(x, y, z);
					// tranform the center into RB_i's local coordiante.
					c = M.transform(c);

					if (parentLS.isInside(c[0], c[1], c[2]) && parentLS.interpolateData(c[0], c[1], c[2]) < 0)
					{
						double R = TransformUtils::getPointDistToLine(c, center, axis);
						maxR = max(maxR, R);
					}
				}
	}

	return maxR;
}