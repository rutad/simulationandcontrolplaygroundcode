#include "SmoothingEngine.h"
#include <igl/cotmatrix.h>
#include <igl/massmatrix.h>



SmoothingEngine::~SmoothingEngine()
{
}

void SmoothingEngine::smoothVoxelGrid()
{
	auto voxelGrid = transformEngine->voxelGrid;
	auto RBIndexMap = transformEngine->RBIndexMap;

	for (int z = 0; z < voxelGrid->voxelNum[2]; z++)
		for (int y = 0; y < voxelGrid->voxelNum[1]; y++)
			for (int x = 0; x < voxelGrid->voxelNum[0]; x++) {
				int type = voxelGrid->getData(x, y, z);
				if (type < 0) continue;
				map<int, int> neighborType;
				for (int i = -1; i < (int)RBIndexMap.size(); i++) {
					neighborType[i] = 0;
				}
				//neighborType[type]++;
				if (x > 0) {
					neighborType[voxelGrid->getData(x - 1, y, z)]++;
				}
				if (x < voxelGrid->voxelNum[0] - 1) {
					neighborType[voxelGrid->getData(x + 1, y, z)]++;
				}
				if (y > 0) {
					neighborType[voxelGrid->getData(x, y - 1, z)]++;
				}
				if (y < voxelGrid->voxelNum[1] - 1) {
					neighborType[voxelGrid->getData(x, y + 1, z)]++;
				}
				if (z > 0) {
					neighborType[voxelGrid->getData(x, y, z - 1)]++;
				}
				if (z < voxelGrid->voxelNum[2] - 1) {
					neighborType[voxelGrid->getData(x, y, z + 1)]++;
				}
				int maxNum = -1;
				int maxType = type;
				for (auto itr = neighborType.begin(); itr != neighborType.end(); itr++)
				{
					if (itr->first >= 0 && itr->second > maxNum)
					{
						maxNum = itr->second;
						maxType = itr->first;
					}
				}
				if (maxNum > 0)
					voxelGrid->setData(x, y, z, maxType);
			}

}

void SmoothingEngine::smoothMesh(GLMesh* mesh, double delta, int iterNum)
{
	MatrixNxM V;
	Eigen::MatrixXi F;
	mesh->getVertexMatrix(V);
	mesh->getFaceMatrix(F);

	SparseMatrix L;
	igl::cotmatrix(V, F, L);

	for (int i = 0; i < iterNum; i++)
	{
		SparseMatrix M;
		igl::massmatrix(V, F, igl::MASSMATRIX_TYPE_BARYCENTRIC, M);

		// Solve (M-delta*L) U = M*U
		const auto & S = (M - delta*L);
		Eigen::SimplicialLDLT<SparseMatrix> solver(S);
		assert(solver.info() == Eigen::Success);
		V = solver.solve(M*V).eval();
	}

	Eigen::VectorXi& vertexIndex = vertexIndexMap[mesh];
	for (int i = 0; i < V.rows(); i++)
	{
		if (vertexIndex[i] == -1)
			continue;

		vertexPos.row(vertexIndex[i]) += V.row(i) / duplicateCount[vertexIndex[i]];
	}
}

Eigen::VectorXi SmoothingEngine::getMeshVertexIndex(GLMesh* mesh)
{
	auto voxelGrid = transformEngine->voxelGrid;
	double voxelSize = transformEngine->transParams->voxelSize;

	Eigen::VectorXi vertexIndex(mesh->getVertexCount());
	V3D sp = voxelGrid->startPoint;

	for (int i = 0; i < mesh->getVertexCount(); i++)
	{
		P3D v = mesh->getVertex(i);
		Eigen::Vector3i index;
		bool surf = false;
		for (int j = 0; j < 3; j++) {
			index[j] = (int)round((v[j] - sp[j]) / voxelSize);
			if (index[j] <= 0 || index[j] >= vertexDim[j] - 1) {
				surf = true;
				break;
			}
		}
		if (!surf)
		{
			for (int x = -1; x <= 0; x++)
				for (int y = -1; y <= 0; y++)
					for (int z = -1; z <= 0; z++)
						if (voxelGrid->getData(index[0] + x, index[1] + y, index[2] + z) == -2)
							surf = true;
		}
		if (surf)
		{
			vertexIndex[i] = -1;
			continue;
		}

		vertexIndex[i] = index[2] * vertexDim[1] * vertexDim[0] + (index[1] * vertexDim[0] + index[0]);
		duplicateCount[vertexIndex[i]] += 1;
	}

	return vertexIndex;
}

void SmoothingEngine::buildVertexIndexMap()
{
	auto& RBs = transformEngine->RBs;
	auto voxelGrid = transformEngine->voxelGrid;

	vertexIndexMap.clear();

	for (int i = 0; i < 3; i++)
		vertexDim[i] = voxelGrid->voxelNum[i] + 1;

	duplicateCount.resize(vertexDim.array().prod());
	duplicateCount.setZero();
	vertexPos.resize(vertexDim.array().prod(), 3);

	for (uint i = 0; i < RBs.size(); i++)
	{
		if (RBs[i]->meshes.empty()) continue;

		GLMesh* mesh = RBs[i]->meshes[0];
		vertexIndexMap[mesh] = getMeshVertexIndex(mesh);
	}
}

void SmoothingEngine::smoothVoxelBoundary(int iterNum)
{
	buildVertexIndexMap();

	for (int i = 0; i < iterNum; i++)
	{
		vertexPos.setZero();

		for (auto itr = vertexIndexMap.begin(); itr != vertexIndexMap.end(); itr++)
		{
			GLMesh* mesh = itr->first;
			smoothMesh(mesh, 0.00005, 1);
		}

		for (auto itr = vertexIndexMap.begin(); itr != vertexIndexMap.end(); itr++)
		{
			GLMesh* mesh = itr->first;
			Eigen::VectorXi& vertexIndex = itr->second;
			for (int j = 0; j < mesh->getVertexCount(); j++)
			{
				if (vertexIndex[j] == -1) continue;

				P3D p = P3D(V3D(vertexPos.row(vertexIndex[j])));
				mesh->setVertex(j, p);
			}
			mesh->computeNormals();
		}
	}
}