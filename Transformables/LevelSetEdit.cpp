#include "TransformEngine.h"


void TransformEngine::levelSetEditingNew(int interfaceIndex)
{
	initialize();

	double lamb = 1;

	MeshInterface* curInterface = &meshInterfaces[interfaceIndex];
	typedef Eigen::Triplet<double> T;
	map<int, map<int, double>> vMap;
	map<int, map<int, double>> wMap;
	map<int, vector<T>> tMap;
	double LSStepSize = transParams->LSStepSize;
	robot->setState(foldedState);

	int layerVertexNum = MlevelSets[0].layerVertexNum;
	int stripVertexNum = MlevelSets[0].vertexNum[0];

	vector<int> movedLSIndex;
	movedLSIndex.push_back(curInterface->index1);
	movedLSIndex.push_back(curInterface->index2);

	auto& points = curInterface->points;
	P3D sp = MlevelSets[0].startPoint;

	// Logger::print("Begin solving linear system.\n");

	for (int k = 0; k < (int)movedLSIndex.size(); k++)
	{
		int index = movedLSIndex[k];
		vMap[index] = map<int, double>();
		vector<T> W;
		vector<double> Y;
		map<int, int> IndexMap;
		int curIndex = 0;
		int pIndex = 0;

		auto& M = vMap[index];

		for (uint i = 0; i < points.size(); i++)
		{
			P3D p0 = points[i];
			double L = MlevelSets[index].interpolateData(p0[0], p0[1], p0[2]);
			V3D G = meshLevelSet.interpolateGradient(p0[0], p0[1], p0[2]).normalized();
			P3D p = p0;
			for (int j = 0; j < 8; j++)
			{
				if (j == 0)
					Y.push_back(L);
				else {
					double val = MlevelSets[index].interpolateData(p[0], p[1], p[2]);
					if (val > 1e4)
						break;
					else
						Y.push_back(val);
				}
					
				int x = (int)((p[0] - sp[0]) / LSStepSize);
				int y = (int)((p[1] - sp[1]) / LSStepSize);
				int z = (int)((p[2] - sp[2]) / LSStepSize);

				for (int dx = 0; dx <= 1; dx++)
					for (int dy = 0; dy <= 1; dy++)
						for (int dz = 0; dz <= 1; dz++) {
							int nx = x + dx;
							int ny = y + dy;
							int nz = z + dz;
							P3D v = MlevelSets[index].getVertex(nx, ny, nz);
							int vIndex = nz * layerVertexNum + (ny * stripVertexNum + nx);

							double wx = (p[0] - sp[0]) / LSStepSize - nx;
							double wy = (p[1] - sp[1]) / LSStepSize - ny;
							double wz = (p[2] - sp[2]) / LSStepSize - nz;
							if (wx >= 0)
								wx = 1 - wx;
							else
								wx = 1 + wx;
							if (wy >= 0)
								wy = 1 - wy;
							else
								wy = 1 + wy;
							if (wz >= 0)
								wz = 1 - wz;
							else
								wz = 1 + wz;
							double weight = wx * wy * wz;

							int mIndex = -1;
							if (IndexMap.count(vIndex))
							{
								mIndex = IndexMap[vIndex];
							}
							else {
								mIndex = curIndex;
								IndexMap[vIndex] = curIndex++;
							}

							//Logger::print("%d %d %lf\n", pIndex, mIndex, weight);
							W.push_back(T(pIndex, mIndex, weight));
						}

				pIndex++;
				if (j < 5)
				{
					p -= G * 0.8 * LSStepSize;
					V3D g = MlevelSets[index].interpolateGradient(p[0], p[1], p[2]);
					double val = MlevelSets[index].interpolateData(p[0], p[1], p[2]);
					if (g.norm() > 1e1 || val > 1e1) {
						j = 4;
						continue;
					}
					V3D dp = (L - val) / g.norm() * g.normalized();
					p += dp;
				}
				else
				{
					if (j == 5)
						p = p0;

					p += G * 0.8 * LSStepSize;
					V3D g = MlevelSets[index].interpolateGradient(p[0], p[1], p[2]);
					double val = MlevelSets[index].interpolateData(p[0], p[1], p[2]);
					if (g.norm() > 1e1 || val > 1e1) {
						break;
					}
					V3D dp = (L - val) / g.norm() * g.normalized();
					p += dp;
				}
			}
		}

		dVector vy(Y.size());
		for (uint i = 0; i < Y.size(); i++)
			vy[i] = Y[i];

		// Logger::print("size: %d %d\n", vy.size(), curIndex);
		dVector sol;
		Eigen::SimplicialLDLT<SparseMatrix> solver;
		SparseMatrix C(vy.size(), curIndex);
		C.setFromTriplets(W.begin(), W.end());
		SparseMatrix ID(curIndex, curIndex);
		ID.setIdentity();

		solver.compute(C.transpose() * C + lamb * ID);
		sol = solver.solve(C.transpose() * vy);

		for (auto& itr : IndexMap)
		{
			M[itr.first] = sol[itr.second];
		}
	}

	double threshold = 50;
	uint floodIterNum = 1;

	int* vertexNum = MlevelSets[0].vertexNum;

	// Logger::print("Start flooding.\n");

	for (uint k = 0; k < floodIterNum; k++)
	{
		for (uint i = 0; i < RBs.size(); i++)
		{
			LevelSet curLS = MlevelSets[i];
			double v = 1e-3;
			double alpha = 1e-3;
			map<int, double>* pMap = NULL;
			if (vMap.count(i))
			{
				pMap = &vMap[i];
			}
			else {
				continue;
			}
#pragma omp parallel for
			for (int z = 1; z < vertexNum[2] - 1; z++)
				for (int y = 1; y < vertexNum[1] - 1; y++)
					for (int x = 1; x < vertexNum[0] - 1; x++) {

						double val = curLS.getData(x, y, z);
						if (fabs(val) > LSStepSize) continue;

						double L = (curLS.getData(x + 1, y, z) + curLS.getData(x - 1, y, z) + curLS.getData(x, y + 1, z)
							+ curLS.getData(x, y - 1, z) + curLS.getData(x, y, z + 1) + curLS.getData(x, y, z - 1) - 6 * val) / (LSStepSize * LSStepSize);

						//Logger::print("L:%lf ", L);

						if (L > threshold)
							L -= threshold;
						else if (L < -threshold)
							L += threshold;
						else
							L = 0;

						double n = -L * alpha * LSStepSize + v;
						int vIndex = z * layerVertexNum + (y * stripVertexNum + x);
						if (pMap && (*pMap).count(vIndex))
						{
							double extra = (*pMap)[vIndex];
							n += extra;
						}
						MlevelSets[i].addData(x, y, z, -n);
					}
		}
	}

#pragma omp parallel for
	for (int i = 0; i < (int)movedLSIndex.size(); i++)
	{
		int index = movedLSIndex[i];
		MlevelSets[index].intersectLS(&largeMeshLevelSet);
		MlevelSets[index].fastMarchingInward(LSStepSize);
		MlevelSets[index].fastMarchingOutward(LSStepSize, 4 * LSStepSize);
	}
	projectMultiLevelSetsEx(MlevelSets, movedLSIndex[0], movedLSIndex[1]);

#pragma omp parallel for
	for (int k = 0; k < (int)movedLSIndex.size(); k++)
	{
		int index = movedLSIndex[k];
		extractMeshesFromLSForRB(index);
	}

}

P3D TransformEngine::projectPointToSurf(P3D p)
{
	V3D g = meshLSGrad.interpolateData(p[0], p[1], p[2]);
	V3D dp = -meshLevelSet.interpolateData(p[0], p[1], p[2]) / g.norm() * g.normalized();
	return p + dp;
}

void TransformEngine::buildMeshInterfaces()
{
	initialize();

	meshInterfaces.clear();
	robot->setState(foldedState);
	double LSStepSize = transParams->LSStepSize;

	MLevelSetsGrad.clear();
	for (uint i = 0; i < MlevelSets.size(); i++)
	{
		MlevelSets[i].fastMarchingOutward(LSStepSize);
		MlevelSets[i].fastMarchingInward(LSStepSize);
		MLevelSetsGrad.push_back(MlevelSets[i].getGradient());
	}

	set<pair<uint, uint>> interfaceFound;

	for (uint i = 0; i < RBs.size(); i++)
	{
		RigidBody* rb = RBs[i];
		if (rb->meshes.empty()) continue;
		GLMesh* tMesh = rb->meshes[0];

		map<int, P3D> seedMap;
		map<int, double> distMap;

		for (int k = 0; k < tMesh->getVertexCount(); k++)
		{
			double dist1;
			P3D p = rb->getWorldCoordinates(tMesh->getVertex(k));
			if (meshLevelSet.isInside(p[0], p[1], p[2]))
			{
				dist1 = fabs(meshLevelSet.interpolateData(p[0], p[1], p[2]));
				if (dist1 > 0.5 * LSStepSize)
					continue;
			}
			else continue;


			for (uint j = 0; j < RBs.size(); j++)
			{
				if (i == j || interfaceFound.count(make_pair(i, j))
					|| RBs[j]->meshes.empty()) continue;

				double dist2 = fabs(MlevelSets[j].interpolateData(p[0], p[1], p[2]));
				if (dist2 < 0.5 * LSStepSize) {
					double dist = dist2;
					if (seedMap.count(j))
					{
						if (dist < distMap[j]) {
							seedMap[j] = p;
							distMap[j] = dist;
						}
					}
					else {
						seedMap[j] = p;
						distMap[j] = dist;
					}
				}
			}
		}

		for (auto& itr : seedMap)
		{
			int index1 = i;
			int index2 = itr.first;
			if (index1 > index2) swap(index1, index2);

			interfaceFound.insert(make_pair(index1, index2));
			interfaceFound.insert(make_pair(index2, index1));
			meshInterfaces.push_back(MeshInterface(index1, index2, RBs[index1], RBs[index2]));
			calculateInterfaceBetween(meshInterfaces.back(), itr.second);
			if (meshInterfaces.back().points.size() < 3)
				meshInterfaces.pop_back();
			else
				checkAdditionalInterface(meshInterfaces.back());
			//Logger::print(" %s %s size:%d \n", RBs[index1]->name.c_str(), RBs[index2]->name.c_str(), meshInterfaces.back().points.size());
		}
	}

	generateIFKnots();
}

void TransformEngine::calculateInterfaceBetween(MeshInterface& meshIF, P3D p)
{
	int index1 = meshIF.index1;
	int index2 = meshIF.index2;
	auto& cPoints = meshIF.points;
	double IFPSpace = transParams->IFPSpace;
	double LSStepSize = MlevelSets[0].stepSize;
	bool flip = false;
	int count = 0;

	P3D np = p;
	while (true)
	{
		if (cPoints.size() > 0)
		{
			V3D v1 = MLevelSetsGrad[index1].interpolateData(np[0], np[1], np[2]).normalized();
			V3D v2 = meshLSGrad.interpolateData(np[0], np[1], np[2]).normalized();
			V3D dp = v1.cross(v2);
			if (flip)
				dp = -dp;
			np += dp * IFPSpace;
		}

		V3D g1 = meshLSGrad.interpolateData(np[0], np[1], np[2]);
		V3D rectify_dp1 = -meshLevelSet.interpolateData(np[0], np[1], np[2]) / g1.norm() * g1.normalized();
		np += rectify_dp1;
		V3D g2 = MLevelSetsGrad[index1].interpolateData(np[0], np[1], np[2]);
		V3D rectify_dp2 = - MlevelSets[index1].interpolateData(np[0], np[1], np[2]) / g2.norm() * g2.normalized();
		np += rectify_dp2;

		if (count > 5 && (np - cPoints[0]).norm() < IFPSpace)
		{
			meshIF.loop = true;
			break;
		}

		if (!(fabs(MlevelSets[index1].interpolateData(np[0], np[1], np[2])) < 0.5 * LSStepSize && fabs(MlevelSets[index2].interpolateData(np[0], np[1], np[2])) < 0.5 * LSStepSize))
		{
			if (!flip)
			{
				reverse(cPoints.begin(), cPoints.end());
				flip = true;
				np = p;
				count = 0;
				continue;
			}
			else
				break;
		}

		cPoints.push_back(np);
		if (cPoints.size() > 100)
			break;
		
		count++;
	}

}

void TransformEngine::recalculateInterface(int interfaceIndex)
{
	double IFPSpace = transParams->IFPSpace;
	double LSStepSize = transParams->LSStepSize;
	auto& curInterface = meshInterfaces[interfaceIndex];
	int index1 = curInterface.index1;
	int index2 = curInterface.index2;
	auto& points = curInterface.points;

	P3D startP, endP;
	if (curInterface.startKnotIndex != -1 && curInterface.endKnotIndex != -1)
	{
		startP = meshIFKnots[curInterface.startKnotIndex].position;
		endP = meshIFKnots[curInterface.endKnotIndex].position;
	}
	else if (curInterface.endKnotIndex != -1)
	{
		startP = meshIFKnots[curInterface.endKnotIndex].position;
	}
	else if(curInterface.startKnotIndex != -1){
		startP = meshIFKnots[curInterface.startKnotIndex].position;
	}
	else {
		return;
	}

	points.clear();
	points.push_back(startP);
	P3D np = startP;
	double flip = 1.0;
	int count = 0;

	{
		V3D v1 = MlevelSets[index1].interpolateGradient(np[0], np[1], np[2]).normalized();
		V3D v2 = meshLSGrad.interpolateData(np[0], np[1], np[2]).normalized();
		V3D dp = v1.cross(v2);

		if (!endP.isZero())
		{
			V3D v = endP - startP;
			if (dp.dot(v) < 0)
				flip = -1.0;
		}
		/*P3D tp = np + dp * 0.02;

		if (!(fabs(MlevelSets[index1].interpolateData(tp[0], tp[1], tp[2])) < 0.5 * LSStepSize
			&& fabs(MlevelSets[index2].interpolateData(tp[0], tp[1], tp[2])) < 0.5 * LSStepSize))
			flip = -1.0;*/
	}
	

	while (true)
	{
		V3D v1 = MlevelSets[index1].interpolateGradient(np[0], np[1], np[2]).normalized();
		V3D v2 = meshLSGrad.interpolateData(np[0], np[1], np[2]).normalized();
		V3D dp = v1.cross(v2) * flip;
		np += dp * IFPSpace;

		V3D g1 = meshLSGrad.interpolateData(np[0], np[1], np[2]);
		V3D rectify_dp1 = -meshLevelSet.interpolateData(np[0], np[1], np[2]) / g1.norm() * g1.normalized();
		np += rectify_dp1;
		V3D g2 = MlevelSets[index1].interpolateGradient(np[0], np[1], np[2]);
		V3D rectify_dp2 = -MlevelSets[index1].interpolateData(np[0], np[1], np[2]) / g2.norm() * g2.normalized();
		np += rectify_dp2;

		if (count > 5 && (np - points[0]).norm() < IFPSpace)
		{
			break;
		}


		if (!endP.isZero() && (np - endP).norm() < IFPSpace)
		{
			if ((np - endP).norm() > 0.01)
				points.push_back(np);
			points.push_back(endP);
			break;
		}

		points.push_back(np);
		count++;
	}
}

void TransformEngine::checkAdditionalInterface(MeshInterface& meshIF)
{
	double LSStepSize = MlevelSets[0].stepSize;

	AxisAlignedBoundingBox bbox;
	bbox.empty();
	for (uint i = 0; i < meshIF.points.size(); i++)
	{
		bbox.addPoint(meshIF.points[i]);
	}
	vector<AxisAlignedBoundingBox> AABBs;
	AABBs.push_back(bbox);

	for (uint c = 0; c < 10; c++)
	{
		RigidBody* rb = meshIF.rb1;
		GLMesh* tMesh = rb->meshes[0];
		double seedDist = DBL_MAX;
		P3D seed;

		for (int k = 0; k < tMesh->getVertexCount(); k++)
		{
			double dist1;
			P3D p = rb->getWorldCoordinates(tMesh->getVertex(k));
			// test if p is a valid seed.
			bool insideAABB = false;
			for (uint i = 0; i < AABBs.size(); i++)
			{
				if (AABBs[i].distanceToPoint(p) < 0.03)
				{
					insideAABB = true;
					break;
				}
			}
			if (insideAABB) continue;

			if (meshLevelSet.isInside(p[0], p[1], p[2]))
			{
				dist1 = fabs(meshLevelSet.interpolateData(p[0], p[1], p[2]));
				if (dist1 > 0.5 * LSStepSize)
					continue;
			}
			else continue;

			double dist2 = fabs(MlevelSets[meshIF.index2].interpolateData(p[0], p[1], p[2]));
			if (dist2 < 0.5 * LSStepSize) {
				double dist = dist1 + dist2;
				if (dist < seedDist)
				{
					seedDist = dist;
					seed = p;
				}
			}
		}

		MeshInterface newInterface = meshIF;
		newInterface.points.clear();

		if (seedDist < DBL_MAX)
		{
			calculateInterfaceBetween(newInterface, seed);

			if (newInterface.points.size() >= 3)
			{
				meshInterfaces.push_back(newInterface);
				// update AABB list.
				bbox.empty();
				for (uint i = 0; i < newInterface.points.size(); i++)
				{
					bbox.addPoint(newInterface.points[i]);
				}
				AABBs.push_back(bbox);
			}
			else
				break;
		}
		else {
			break;
		}
	}
	
	return;
}

void TransformEngine::generateIFKnots()
{
	double IFPSpace = transParams->IFPSpace;
	set<pair<int, int>> S;
	meshIFKnots.clear();

	for (int i = 0; i < (int)meshInterfaces.size() - 1; i++)
	{
		if (meshInterfaces[i].loop) continue;
		auto& points = meshInterfaces[i].points;
		double dist = (points.back() - points.front()).norm();
		if (dist < IFPSpace) continue;

		for (int a = 0; a < 2; a++)
		{
			if (S.count(make_pair(i, a))) continue;
			MeshInterfaceKnot knot;
			knot.interfaceFlags.push_back(a);
			knot.interfaceIndices.push_back(i);
			P3D pi = a == 0 ? meshInterfaces[i].points.front() : meshInterfaces[i].points.back();

			for (int j = i + 1; j < (int)meshInterfaces.size(); j++)
			{
				if (meshInterfaces[j].loop) continue;
				dist = (meshInterfaces[j].points.front() - meshInterfaces[j].points.back()).norm();
				if (dist < IFPSpace) continue;
				for (int b = 0; b < 2; b++)
				{
					if (S.count(make_pair(j, b))) continue;

					P3D pj = b == 0 ? meshInterfaces[j].points.front() : meshInterfaces[j].points.back();
					if ((pj - pi).norm() < 2e-2)
					{
						knot.interfaceFlags.push_back(b);
						knot.interfaceIndices.push_back(j);
						S.insert(make_pair(j, b));
					}
				}
			}

			meshIFKnots.push_back(knot);
		}
	}

	for (uint i = 0; i < meshIFKnots.size(); i++)
	{
		P3D& p = meshIFKnots[i].position;
		auto& indices = meshIFKnots[i].interfaceIndices;
		auto& flags = meshIFKnots[i].interfaceFlags;
		p.setZero();
		for (uint j = 0; j < indices.size(); j++)
		{
			p += flags[j] == 0 ? meshInterfaces[indices[j]].points.front() : meshInterfaces[indices[j]].points.back();
		}
		p *= 1.0 / indices.size();

		for (uint j = 0; j < indices.size(); j++)
		{
			auto& np = flags[j] == 0 ? meshInterfaces[indices[j]].points.front() : meshInterfaces[indices[j]].points.back();
			np = p;

			if (flags[j] == 0)
			{
				meshInterfaces[indices[j]].startKnotIndex = (int)i;
			}
			else {
				meshInterfaces[indices[j]].endKnotIndex = (int)i;
			}
		}
	}

}
