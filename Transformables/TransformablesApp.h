#pragma once

#include <GUILib/GLApplication.h>
#include <string>
#include <map>
#include <GUILib/TranslateWidget.h>
#include <GUILib/RotateWidgetV1.h>
#include <GUILib/RotateWidgetV2.h>
#include <Utils/Timer.h>
#include <Transformables/TransformEngine.h>
#include <Transformables/TransformablesSideWindow.h>
#include <RobotDesignerLib/LocomotionEngineMotionPlan.h>
#include <RobotDesignerLib/ModularDesignWindow.h>
#include <ControlLib/IK_Solver.h>

/**
 * Test App
 */

struct TransConfig 
{
	string meshName;
	string skeletonName;
	string foldName;
	string unfoldName;
	string motionPlanName;
	string levelSetsName;
	string RDParamsName;
	string meshFolderName;
	string initFileName;
	string designName;
	V3D scale3D;
	double scale;
	vector<int> foldingOrder;
	set<int> flipJointSet;

	// TransParams
	bool useVolume = false;
	bool useXM430 = false;
	bool useLivingBracket = false;
	double LSStepSize = 0;
};


class TransformablesApp : public GLApplication {
public:
	// Timer
	Timer timer;

	// Widgets
	RotateWidget* rWidget = NULL;
	TranslateWidget* tWidget = NULL;

	// Mesh, VoxelGrid
	GLMesh* mesh = NULL;
	// Original mesh with split vertices
	GLMesh* visMesh = NULL;
	
	// Joint meshes
	GLMesh* jointPlugMesh = NULL;
	GLMesh* jointPlugCarveMesh = NULL;
	GLMesh* jointSocketMesh = NULL;
	GLMesh* jointSocketCarveMesh = NULL;

	// Robot
	AbstractRBEngine* rbEngine = NULL;
	Robot* robot = NULL;
	ReducedRobotState* foldedState = NULL;
	ReducedRobotState* unfoldedState = NULL;
	ReducedRobotState* startState = NULL;
	vector<ReducedRobotState> motionPlanStates;

	// Transform engine
	TransformEngine* transformEngine = NULL;
	IK_Solver* IKSolver_v2 = NULL;
	P3D selectedPoint;	//in local coordinates of selected rigid body
	P3D targetPoint;	//in world coordinates

	map<RigidBody*, V3D> colorMap;
	map<int, int> jointSymmetryMap;

	// Modular design window
	ModularDesignWindow* modularDesign = NULL;
	// Side window
	TransformablesSideWindow* sideWindow = NULL;
	// Control parameters for side window
	TransformablesControlParams controlParams;

	// User interface
	bool dragging = false;
	RigidBody* pickedRB = NULL;
	int pickedJoint = -2;
	RigidBody* pickedLimb = NULL;
	int pickedInterface = -1;
	int pickedInterfaceP = -1;
	int pickedKnot = -1;
	int jointToSwap = -1;
	double meshScale = 1.0;
	P3D startP;
	double movedDist;
	// Material
	string matFile = "../data/textures/matcap/MatCap_0008.bmp";

	// Control variable
	bool showMesh = true;
	bool showVoxelGrid = true;
	bool showDistanceField = false;
	bool showPickedRB = true;
	bool showJointBoundaries = false;
	bool editingRobot = false;
	bool showTransferedMesh = false;

	TransformParams transParams;
	
	// Configurations
	map<char, TransConfig> configMap;
	TransConfig curConfig;

	// Run options
	enum RUN_OPTIONS {
		SKELETON_CMA_OPT = 0,
		SKELETON_OPT,
		SKELETON_CONT_OPT,
		FOLD_ORDER_OPT,
		LS_FLOOD,
		CARVE_FLOOD,
		IF_EVOLVE
	};
	int runOption = SKELETON_CMA_OPT;

public:
	// constructor
	TransformablesApp();
	// destructor
	virtual ~TransformablesApp(void);
	// Run the App tasks
	virtual void process();
	// Draw the App scene - camera transformations, lighting, shadows, reflections, etc apply to everything drawn by this method
	virtual void drawScene();
	// This is the wild west of drawing - things that want to ignore depth buffer, camera transformations, etc. Not pretty, quite hacky, but flexible. Individual apps should be careful with implementing this method. It always gets called right at the end of the draw function
	virtual void drawAuxiliarySceneInfo();
	// Restart the application.
	virtual void restart();

	//input callbacks...

	//all these methods should returns true if the event is processed, false otherwise...
	//any time a physical key is pressed, this event will trigger. Useful for reading off special keys...
	virtual bool onKeyEvent(int key, int action, int mods);
	//this one gets triggered on UNICODE characters only...
	virtual bool onCharacterPressedEvent(int key, int mods);
	//triggered when mouse buttons are pressed
	virtual bool onMouseButtonEvent(int button, int action, int mods, double xPos, double yPos);
	//triggered when mouse moves
	virtual bool onMouseMoveEvent(double xPos, double yPos);
	//triggered when using the mouse wheel
	virtual bool onMouseWheelScrollEvent(double xOffset, double yOffset);

	virtual bool processCommandLine(const std::string& cmdLine);
	
	virtual void saveFile(const char* fName);
	virtual void loadFile(const char* fName);

	virtual void setupLights();

	virtual void adjustWindowSize(int w, int h);

	void loadRobotFromFile(const char* fName, const char* mpName = NULL);
	void loadMeshFromFile(const char* fName, double scale);
	void loadJointMesh();

	void assignMesh();
	void optimizeRobotState();
	void optimizeFoldingOrder();
	void carveMesh();

	void foldAnimation();
	void unfoldAnimation();
	void voxelize();

	bool pickJoint(double xPos, double yPos);
	void pickJointPostProc(int mods);
	bool pickRB(double xPos, double yPos);
	bool pickLimb(double xPos, double yPos);
	bool pickInterfacePoint(double xPos, double yPos);
	bool pickKnot(double xPos, double yPos);
	void moveInterfacePoint(double xPos, double yPos);
	void moveKnot(double xPos, double yPos);

	void drawRobot();
	void drawRBOrigMeshes();
	void drawRBThickness();
	void drawRBDistanceField();
	void drawJointMesh();
	void drawMeshInterface();
	void drawLSInterface();
	void drawJointBoundaries();
	void drawJointSlitMeshes();

	void getRBColorMap();
	void buildJointSymmetryMap();

	void saveRobotState();
	void saveRobotToRBS();
	void saveVoxelGrid();
	void savePickedRBMesh();
	
	void saveRobotMeshes(string path, bool localCoord = true);
	void loadRobotMeshes();

	void handleFoldingOrderSwap();

	void createTransformEngine();

	void loadConfigMap(const char* fName);
	void loadConfig(TransConfig& transConfig);

	void saveConfig();

	void exportFoldingJointAngles(bool fold = true);

	void splitRobotMeshVertices();
	int getJointIndexForInterface(int interfaceIndex);
};



