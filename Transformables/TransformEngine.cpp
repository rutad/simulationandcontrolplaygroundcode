#include <igl/signed_distance.h>
#include <process.h> 
#include <GUILib/OBJReader.h>
#include <MathLib/MeshBoolean.h>
#include <MathLib/MeshSimplify.h>
#include "TransformEngine.h"



TransformEngine::TransformEngine(Robot* _robot, ReducedRobotState* _foldedState, ReducedRobotState* _unfoldedState, TransformParams* _transParams, map<int, int>& _jointSymmetryMap, vector<ReducedRobotState>& _motionPlanStates)
{
	robot = _robot;
	foldedState = _foldedState;
	unfoldedState = _unfoldedState;
	transParams = _transParams;
	jointSymmetryMap = _jointSymmetryMap;
	motionPlanStates = _motionPlanStates;
	buildRobotRigidBodyMaps();
	getMPRangeOfMotion();

	robotEditor = new RobotEditor(this, robot);
	skeletonOptEngine = new SkeletonOptEngine(this);
	foldingOptEngine = new FoldingOptEngine(this);
	smoothingEngine = new SmoothingEngine(this);
	carvingEngine = new CarvingEngine(this);
	collisionEngine = new CollisionEngine(this);
	fabricationEngine = new FabricationEngine(this);
	
}

TransformEngine::~TransformEngine()
{
	TransformUtils::clearMeshMap(robotMeshes);
	TransformUtils::clearMeshMap(skeletonMeshes);
	for (uint i = 0; i < RBs.size(); i++)
		RBs[i]->meshes.clear();
	clearVoxelGrid();
	
	delete bodyMesh;
	delete MCMesh;
	delete skeletonOptEngine;
	delete foldingOptEngine;
	delete smoothingEngine;
	delete carvingEngine;
	delete collisionEngine;
	delete fabricationEngine;
}

void TransformEngine::buildRobotRigidBodyMaps()
{
	int index = 0;
	capsulesMap[robot->getRoot()] = vector<Capsule>();
	TransformUtils::getCapsulesForRigidBody(robot->getRoot(), capsulesMap[robot->getRoot()]);

	RBIndexMap[robot->getRoot()] = index++;
	RBs.push_back(robot->getRoot());

	for (uint i = 0; i < robot->jointList.size(); i++) {
		Joint* joint = robot->jointList[i];
		RigidBody* rb = joint->child;
		capsulesMap[rb] = vector<Capsule>();
		TransformUtils::getCapsulesForRigidBody(rb, capsulesMap[rb]);

		RBIndexMap[rb] = index++;
		RBs.push_back(rb);

		parentMap[joint->child] = joint->parent;
	}

	fixedLS.resize(RBs.size(), false);
	/*for (auto itr = parentMap.begin(); itr != parentMap.end(); itr++)
	{
		Logger::print("%s -> %s\n", itr->first->getName().c_str(), itr->second->getName().c_str());
	}*/
}

void TransformEngine::loadRBOrigMeshes()
{
	set<string> meshFilter = {
		"../data/robotDesigner/motorMeshes/XL-320_HornBracket.obj",
		"../data/robotDesigner/motorMeshes/XL-320_BottomBracket.obj",
		"../data/robotDesigner/motorMeshes/XL-320_PointFoot.obj",
		"../data/robotDesigner/motorMeshes/XL-320.obj",
		"../data/robotDesigner/motorMeshes/XM-430.obj"
		//"../data/robotDesigner/motorMeshes/XM-430-hornBracket2.obj",
		//"../data/robotDesigner/motorMeshes/XM-430-3DP-connector2.obj",
		//"../data/robotDesigner/motorMeshes/XM-430-bottomBracket2.obj",
		//"../data/robotDesigner/motorMeshes/3DP-LongPointEE2.obj",
		//"../data/robotDesigner/motorMeshes/XM-430-sideBracket2.obj",
		//"../data/robotDesigner/motorMeshes/XM-430-3DP-hornBracket2.obj",
		//"../data/robotDesigner/motorMeshes/3DP-PointEE2.obj"
	};

	RBOrigMeshes.clear();

	if (robot->root->meshes.empty())
		transParams->shellOnly = false;

	if (!transParams->shellOnly) {
		// Volume
		for (int i = 0; i < robot->getRigidBodyCount(); i++)
		{
			RigidBody* rb = robot->getRigidBody(i);
			vector<GLMesh*> newOrigMeshes;
			rb->carveMeshes.clear();
			rb->meshTransformations.clear();
			rb->meshes.clear();
			rb->meshDescriptions.clear();

			for (uint j = 0; j < rb->cJoints.size(); j++)
			{
				Joint* joint = rb->cJoints[j];
				Transformation trans = TransformUtils::getSocketJointTransformation(joint);
				newOrigMeshes.push_back(jointSocketMesh);
				rb->meshTransformations.push_back(trans);
				rb->carveMeshes.push_back(jointSocketCarveMesh);
			}

			for (uint j = 0; j < rb->pJoints.size(); j++)
			{
				Joint* joint = rb->pJoints[j];
				Transformation trans = TransformUtils::getPlugJointTransformation(joint);
				newOrigMeshes.push_back(jointPlugMesh);
				rb->meshTransformations.push_back(trans);
				rb->carveMeshes.push_back(jointPlugCarveMesh);
			}

			rb->meshDescriptions.assign(newOrigMeshes.size(), "skeleton");
			RBOrigMeshes[rb] = newOrigMeshes;
		}
	}
	else {
		// Shell
		for (int i = 0; i < robot->getRigidBodyCount(); i++)
		{
			RigidBody* rb = robot->getRigidBody(i);
			vector<GLMesh*> newOrigMeshes;
			vector<GLMesh*> newCarveMeshes;
			vector<Transformation> newMeshTrans;

			for (uint j = 0; j < rb->meshes.size(); j++) {
				GLMesh* oMesh = rb->meshes[j];

				if (meshFilter.count(oMesh->path))
				{
					newOrigMeshes.push_back(oMesh);
					if (rb->carveMeshes.size() > j)
						newCarveMeshes.push_back(rb->carveMeshes[j]);
					else
						newCarveMeshes.push_back(NULL);
					newMeshTrans.push_back(rb->meshTransformations[j]);
				}
			}
			RBOrigMeshes[rb] = newOrigMeshes;
			rb->carveMeshes = newCarveMeshes;
			rb->meshTransformations = newMeshTrans;
			rb->meshDescriptions.assign(newOrigMeshes.size(), "skeleton");
			rb->meshes.clear();
		}
	}

	auto& rootMeshes = RBOrigMeshes[robot->getRoot()];
	for (auto& tMesh : rootMeshes)
	{
		tMesh->calBoundingBox();
	}

}

void TransformEngine::getMeshVerticesForRBs()
{
	rbVertices.clear();
	robot->setState(startState);

	modularDesign->getMeshVerticesForRBs(robot, rbVertices);
}

void TransformEngine::loadRBOrigMeshesFromDesign(bool mergeMeshes, bool initialLoad)
{
	RBOrigMeshes.clear();
	robot->setState(startState);

	// store current meshes to avoid being overwritten
	map<RigidBody*, vector<GLMesh*>> curMeshes;
	for (int i = 0; i < robot->getRigidBodyCount(); i++)
	{
		RigidBody* rb = robot->getRigidBody(i);
		curMeshes[rb] = rb->meshes;
	}

	// transfer meshes from modular design
	modularDesign->transferMeshes(robot, mergeMeshes);

	// restore original meshes
	for (int i = 0; i < robot->getRigidBodyCount(); i++)
	{
		RigidBody* rb = robot->getRigidBody(i);
		RBOrigMeshes[rb] = rb->meshes;
		if (initialLoad)
			rb->meshes.clear();
		else
			rb->meshes = curMeshes[rb];
	}
}

void TransformEngine::getMPRangeOfMotion()
{
	int jointNum = robot->getJointCount();
	minMPJointAngles = dVector::Constant(jointNum, DBL_MAX);
	maxMPJointAngles = dVector::Constant(jointNum, -DBL_MAX);

	for (uint i = 0; i < motionPlanStates.size(); i++)
	{
		dVector stateJointAngles = TransformUtils::getJointAnglesForCurrentState(robot, &motionPlanStates[i]);
		minMPJointAngles = stateJointAngles.array().min(minMPJointAngles.array());
		maxMPJointAngles = stateJointAngles.array().max(maxMPJointAngles.array());
	}

	/*for (int i = 0; i < jointNum; i++)
	{
		Logger::print("MP, max: %lf, min: %lf\n", maxMPJointAngles[i], minMPJointAngles[i]);
	}*/
}

void TransformEngine::reinitializeRobot()
{
	Logger::print("reinitialize robot\n");
	TransformUtils::updateCapsuleMap(capsulesMap, true);
	skeletonOptEngine->reinitialize();
	robotObsolete = false;
}

void TransformEngine::euclideanVoxelAssignment()
{
	for (int z = 0; z < voxelGrid->voxelNum[2]; z++)
		for (int y = 0; y < voxelGrid->voxelNum[1]; y++)
			for (int x = 0; x < voxelGrid->voxelNum[0]; x++) {
				if (voxelGrid->getData(x, y, z) == -2) continue;

				int capsuleIndex;
				double dist;
				P3D center = voxelGrid->getVoxelCenter(x, y, z);
				RigidBody* rb = TransformUtils::getRobotClosestRigidBody(center, capsulesMap, &dist, &capsuleIndex);
				voxelGrid->setData(x, y, z, RBIndexMap[rb]);

				Capsule& cap = capsulesMap[rb][capsuleIndex];
				cap.r = max(cap.r, dist);
			}
}

void TransformEngine::geodesicVoxelAssignment()
{
	double voxelSize = transParams->voxelSize;
	TransformUtils::clearVoxelAssignment(voxelGrid);

	P3D sp = voxelGrid->startPoint;
	vector<vector<Eigen::Vector3i>> voxelFront(RBs.size());

	// assign seeding voxels for each rigid body
	for (uint i = 0; i < RBs.size(); i++)
	{
		RigidBody* rb = RBs[i];
		vector<Capsule>& capsules = capsulesMap[rb];
		vector<Eigen::Vector3i>& front = voxelFront[i];

		for (uint j = 0; j < capsules.size(); j++)
		{
			P3D a = capsules[j].seg.a;
			P3D b = capsules[j].seg.b;
			V3D v = (b - a).normalized();
			double len = (b - a).length();
			
			for (double t = voxelSize; t <= len - voxelSize; t += voxelSize)
			{
				P3D p = a + v * t;
				Eigen::Vector3i vIndex;
				for (int k = 0; k < 3; k++)
					vIndex[k] = (int)((p[k] - sp[k]) / voxelSize);

				if (voxelGrid->isInside(vIndex[0], vIndex[1], vIndex[2]) && 
					voxelGrid->getData(vIndex[0], vIndex[1], vIndex[2]) == -1)
				{
					voxelGrid->setData(vIndex[0], vIndex[1], vIndex[2], i);
					front.push_back(vIndex);
				}
			}
		}
	}

	while (1)
	{
		bool terminate = true;

		for (uint i = 0; i < RBs.size(); i++)
		{
			RigidBody* rb = RBs[i];
			vector<Eigen::Vector3i>& front = voxelFront[i];
			vector<Eigen::Vector3i> newFront;

			for (uint j = 0; j < front.size(); j++)
			{
				Eigen::Vector3i vIndex = front[j];
				for (int k = 0; k < 3; k++)
					for (int p = -1; p < 2; p += 2)
					{
						Eigen::Vector3i tmpIndex = vIndex;
						tmpIndex[k] += p;
						if (voxelGrid->isInside(tmpIndex[0], tmpIndex[1], tmpIndex[2])
							&& voxelGrid->getData(tmpIndex[0], tmpIndex[1], tmpIndex[2]) == -1)
						{
							voxelGrid->setData(tmpIndex[0], tmpIndex[1], tmpIndex[2], i);
							newFront.push_back(tmpIndex);
						}		
					}
			}

			front = newFront;
			if (!front.empty())
				terminate = false;
		}

		if (terminate) break;
	}
}


void TransformEngine::transferMeshLS(int maxIterNum, bool parentToChild)
{

	for (int i = 0; i < robot->getJointCount(); i++)
		transferMeshLSForJoint(i, maxIterNum, parentToChild);

	for (int i = 0; i < 20; i++)
		levelSetFlooding();

	meshInterfaces.clear();
	meshIFKnots.clear();
	robot->setState(foldedState);
}

void TransformEngine::transferMeshLSForJoint(int jIndex, int maxIterNum, bool parentToChild)
{
	vector<RBState> parentStates;
	vector<RBState> childStates;
	TransformUtils::getParentAndChildStatesForJoint(parentStates, childStates, robot, jIndex, foldedState, unfoldedState, 0.1);
	double maxR = getTransferMaxRForJoint(jIndex, parentStates, childStates, maxIterNum, parentToChild);
	execTransferMeshForJoint(jIndex, maxR, parentToChild);
}

double TransformEngine::getTransferMaxRForJoint(int jIndex, vector<RBState>& parentStates, vector<RBState>& childStates, int maxIterNum /*= 100*/, bool parentToChild /*= false*/)
{
	double maxR;
	double LSStepSize = transParams->LSStepSize;

	robot->setState(foldedState);
	HingeJoint* joint = (HingeJoint*)robot->getJoint(jIndex);
	RigidBody* parentRB = joint->parent;
	RigidBody* childRB = joint->child;
	if (robotMeshes.count(parentRB) == 0 || robotMeshes.count(childRB) == 0) return 0;

	LevelSet& parentLS = MlevelSets[RBIndexMap[parentRB]];
	LevelSet& childLS = MlevelSets[RBIndexMap[childRB]];
	V3D axis = parentRB->getWorldCoordinates(joint->rotationAxis);
	P3D jointPos = parentRB->getWorldCoordinates(joint->pJPos);

	// ******************* binary search *******************
	double R_lb = transParams->minTransferR;
	double R_ub = 0.1;

	int index = 0;

	while (R_ub - R_lb > 0.01)
	{
		double curR = (R_lb + R_ub) * 0.5;

		LevelSet cylinderLS(LSBBox, LSStepSize);
		LevelSet tmpParentLS = parentLS;
		LevelSet tmpChildLS = childLS;
		if (transParams->shellOnly)
		{
			tmpParentLS.intersectLS(&shellLevelSet);
			tmpChildLS.intersectLS(&shellLevelSet);
		}
		cylinderLS.initCylinder(axis, jointPos, curR);

		if (parentToChild)
		{
			cylinderLS.intersectLS(&tmpParentLS);
			tmpChildLS.unionLS(&cylinderLS);
			tmpParentLS.minusLS(&cylinderLS);
		}
		else {
			cylinderLS.intersectLS(&tmpChildLS);
			tmpParentLS.unionLS(&cylinderLS);
			tmpChildLS.minusLS(&cylinderLS);
		}

		double collision = collisionEngine->getCollisionBetweenLS(tmpParentLS, tmpChildLS, parentStates, childStates);
		// Logger::print("%d, ub: %lf, lb: %lf, C: %lf\n", index++, R_ub, R_lb, collision);

		if (collision < 1e-2)
		{
			R_ub = curR;
		}
		else {
			R_lb = curR;
		}
	}

	maxR = (R_lb + R_ub) * 0.5;

	return maxR;
}

void TransformEngine::execTransferMeshForJoint(int jIndex, double maxR, bool parentToChild)
{
	if (maxR == 0) return;

	double LSStepSize = transParams->LSStepSize;

	robot->setState(foldedState);
	HingeJoint* joint = (HingeJoint*)robot->getJoint(jIndex);
	RigidBody* parentRB = joint->parent;
	RigidBody* childRB = joint->child;

	LevelSet& parentLS = MlevelSets[RBIndexMap[parentRB]];
	LevelSet& childLS = MlevelSets[RBIndexMap[childRB]];
	V3D axis = parentRB->getWorldCoordinates(joint->rotationAxis);
	P3D jointPos = parentRB->getWorldCoordinates(joint->pJPos);

	LevelSet cylinderLS(LSBBox, LSStepSize);
	cylinderLS.initCylinder(axis, jointPos, maxR);

	if (parentToChild)
	{
		cylinderLS.intersectLS(&parentLS);
		childLS.unionLS(&cylinderLS);
		parentLS.minusLS(&cylinderLS);
	}
	else {
		cylinderLS.intersectLS(&childLS);
		parentLS.unionLS(&cylinderLS);
		childLS.minusLS(&cylinderLS);
	}
}

void TransformEngine::clearRBLevelSet(RigidBody* rb)
{
	MlevelSets[RBIndexMap[rb]].setConstant(DBL_MAX);

	if (rb->pJoints.size() > 0)
	{
		auto itr = jointSymmetryMap.find(rb->pJoints[0]->jIndex);
		if (itr != jointSymmetryMap.end())
		{
			MlevelSets[RBIndexMap[robot->getJoint(itr->second)->child]].setConstant(DBL_MAX);
		}
	}
	
	meshInterfaces.clear();
	meshIFKnots.clear();
}

void TransformEngine::assignVoxelMeshToRobot()
{
	initialize();

	// getJointCollsionMap();
	robot->setState(foldedState);

	TransformUtils::clearMeshMap(robotMeshes);
	TransformUtils::updateCapsuleMap(capsulesMap, true);

	for (uint i = 0; i < RBs.size(); i++)
	{
		RBs[i]->meshes.clear();
	}

	voxelGrid = expandedVoxelGrid;

	// calculate voxel assignment
	// euclideanVoxelAssignment();

	geodesicVoxelAssignment();

	// tweakJointVoxelAssignment();

	// smooth voxel assignment
	//if (smooth)
		//smoothingEngine->smoothVoxelGrid();

	// extract voxel meshes
	for (uint i = 0; i < RBs.size(); i++)
	{
		RigidBody* rb = RBs[i];

		GLMesh* nMesh = voxelGrid->extractMeshWithData(i);
		if (!nMesh) continue;

		rb->meshes.push_back(nMesh);
	}

	// smooth voxel meshes
	if (transParams->smoothVoxels)
		smoothingEngine->smoothVoxelBoundary(20);

	// get level sets from voxel meshes
	for (uint i = 0; i < RBs.size(); i++)
	{
		RigidBody* rb = RBs[i];
		if (rb->meshes.empty())
			continue;

		Logger::print("getting level set for rb %d\n", i);
		MlevelSets.push_back(LevelSet(LSBBox, transParams->LSStepSize));
		MlevelSets.back().updateLevelSet(rb->meshes[0]);
	}

	transferMeshLS();

	generateTransMeshes();

}

void TransformEngine::generateTransMeshes()
{
	if (robotObsolete)
		reinitializeRobot();
	robot->setState(foldedState);

	// get meshes from level sets
	for (uint i = 0; i < RBs.size(); i++)
	{
		RigidBody* rb = RBs[i];
		if (rb->meshes.size() > 0)
			delete rb->meshes[0];

		Logger::print("extracting mesh for rb %d\n", i);
		GLMesh* tmpMesh = MlevelSets[i].extractMesh(0);
		if (tmpMesh)
			rb->meshes[0] = tmpMesh;
		else {
			rb->meshes.clear();
			robotMeshes.erase(RBs[i]);
		}
	}

	// intersect voxel meshes with original mesh
	for (uint i = 0; i < RBs.size(); i++)
	{
		RigidBody* rb = RBs[i];
		if (rb->meshes.empty())
			continue;
		GLMesh* nMesh = rb->meshes[0];

		GLMesh* hostMesh = transParams->shellOnly ? shellMesh : mesh;
		GLMesh* bMesh = meshBoolean(hostMesh, nMesh, "Intersect");
		//GLMesh* bMesh = nMesh;
		if (bMesh->getVertexCount() == 0) {
			delete bMesh;
			rb->meshes.clear();
			robotMeshes.erase(rb);
		}
		else {
			TransformUtils::transformMeshFromWorldToLocalCoords(bMesh, rb);
			robotMeshes[rb] = bMesh;
			rb->meshes[0] = bMesh;
		}
		delete nMesh;
	}
}

void TransformEngine::initialize()
{
	if (initialized) return;

	delete shellMesh;
	double LSStepSize = transParams->LSStepSize;
	LSBBox = mesh->calBoundingBox();
	LSBBox.setbmin(LSBBox.bmin() + V3D(1, 1, 1) * -6 * LSStepSize);
	LSBBox.setbmax(LSBBox.bmax() + V3D(1, 1, 1) * 6 * LSStepSize);

	meshLevelSet = LevelSet(LSBBox, LSStepSize);
	meshLevelSet.updateLevelSet(mesh);
	meshLSGrad = meshLevelSet.getGradient();
	meshLSHessian = getHessian(meshLSGrad);
	
	LevelSet tmpLS = meshLevelSet;
	tmpLS.addConstant(transParams->shellThickness);
	GLMesh* shrinkedMesh = tmpLS.extractMesh(0);
	shellMesh = mesh->clone();
	shellMesh->append(shrinkedMesh, true);
	delete shrinkedMesh;

	MCMesh = meshLevelSet.extractMesh(0);

	largeMeshLevelSet = meshLevelSet;
	largeMeshLevelSet.addConstant(-2 * LSStepSize);
	tmpLS = meshLevelSet;
	tmpLS.addConstant(2 * LSStepSize);
	shellLevelSet = meshLevelSet;
	shellLevelSet.minusLS(&tmpLS);
	largeShellLevelSet = largeMeshLevelSet;
	largeShellLevelSet.minusLS(&tmpLS);

	MlevelSets.clear();
	voxelize();

	saveInitialization();

	initialized = true;
}

void TransformEngine::saveInitialization()
{
	FILE* fp = fopen("../out/transInit.tin", "w+");

	meshLevelSet.saveSparseToFile(fp);
	fprintf(fp, "\n\n");

	TransformUtils::saveVoxelGridToFile(fp, origVoxelGrid);
	fprintf(fp, "\n\n");

	TransformUtils::saveVoxelGridToFile(fp, expandedVoxelGrid);
	fprintf(fp, "\n\n");

	fclose(fp);
}

void TransformEngine::loadInitialization(const char* fName)
{
	FILE* fp = fopen(fName, "r");

	delete shellMesh;
	double LSStepSize = transParams->LSStepSize;
	LSBBox = mesh->calBoundingBox();
	LSBBox.setbmin(LSBBox.bmin() + V3D(1, 1, 1) * -6 * LSStepSize);
	LSBBox.setbmax(LSBBox.bmax() + V3D(1, 1, 1) * 6 * LSStepSize);

	meshLevelSet.loadSparseFromFile(fp);
	meshLSGrad = meshLevelSet.getGradient();
	meshLSHessian = getHessian(meshLSGrad);

	LevelSet tmpLS = meshLevelSet;
	tmpLS.addConstant(transParams->shellThickness);
	GLMesh* shrinkedMesh = tmpLS.extractMesh(0);
	shellMesh = mesh->clone();
	shellMesh->append(shrinkedMesh, true);
	delete shrinkedMesh;

	MCMesh = meshLevelSet.extractMesh(0);

	largeMeshLevelSet = meshLevelSet;
	largeMeshLevelSet.addConstant(-2 * LSStepSize);
	tmpLS = meshLevelSet;
	tmpLS.addConstant(2 * LSStepSize);
	shellLevelSet = meshLevelSet;
	shellLevelSet.minusLS(&tmpLS);
	largeShellLevelSet = largeMeshLevelSet;
	largeShellLevelSet.minusLS(&tmpLS);

	MlevelSets.clear();

	origVoxelGrid = TransformUtils::loadVoxelGridFromFile(fp);
	expandedVoxelGrid = TransformUtils::loadVoxelGridFromFile(fp);
	boundaryVoxelGrid = TransformUtils::getBoundaryVoxelGrid(origVoxelGrid);

	initialized = true;

	fclose(fp);
}

void TransformEngine::assignLSMeshToRobot()
{
	initialize();

	prepareForFlooding();

	for (int i = 0; i < transParams->maxFloodIter; i++)
	{
		levelSetFlooding();
	}
	transferMeshLS();

	for (int i = 0; i < transParams->maxFloodIter; i++)
	{
		levelSetFlooding();
	}

	extractMeshesFromLS();
	generateTransMeshes();
}

void TransformEngine::prepareForFlooding()
{
	initialize();

	double LSStepSize = transParams->LSStepSize;

	robot->setState(foldedState);

	TransformUtils::clearMeshMap(robotMeshes);
	TransformUtils::updateCapsuleMap(capsulesMap, true);

	for (uint i = 0; i < RBs.size(); i++)
	{
		RBs[i]->meshes.clear();
		MlevelSets.push_back(LevelSet(LSBBox, LSStepSize));
	}

	double intialR = transParams->LSCylinderR;

	int* vertexNum = MlevelSets[0].vertexNum;
	int index = 0;
	for (int z = 0; z < vertexNum[2]; z++)
		for (int y = 0; y < vertexNum[1]; y++)
			for (int x = 0; x < vertexNum[0]; x++) {

				P3D center = MlevelSets[0].getVertex(x, y, z);

				for (uint i = 0; i < RBs.size(); i++)
				{
					double dist = TransformUtils::getClosestDistToCapsules(center, capsulesMap[RBs[i]]);
					double multiplier = i == 0 ? 1 : 1;
					double val = dist - intialR * multiplier;
					MlevelSets[i].setData(index, val);
				}

				index++;
			}

	for (uint i = 0; i < MlevelSets.size(); i++)
	{
		MlevelSets[i].intersectLS(&largeMeshLevelSet);
		MlevelSets[i].fastMarchingInward(LSStepSize);
		MlevelSets[i].fastMarchingOutward(LSStepSize, 2 * LSStepSize);
	}
	projectMultiLevelSets(MlevelSets);

}

void TransformEngine::levelSetFlooding()
{
	initialize();

	double LSStepSize = transParams->LSStepSize;
	double threshold = transParams->LSFlood_L;
	robot->setState(foldedState);

	int* vertexNum = MlevelSets[0].vertexNum;

	//timer.restart();
#pragma omp parallel for
	for (int i = 0; i < (int)RBs.size(); i++)
	{
		if (fixedLS[i]) continue;

		LevelSet curLS = MlevelSets[i];
		P3D jPos, EEPos;
		V3D normal;
		double EEVal, len;
		bool isEE = false;
		auto& endEffectors = RBs[i]->rbProperties.endEffectorPoints;
		if (transParams->sharpFeet && i > 0 && endEffectors.size() > 0)
		{
			isEE = true;
			jPos = RBs[i]->pJoints[0]->cJPos;
			for (auto& p : endEffectors)
				EEPos += p.coords;
			EEPos /= endEffectors.size();
			jPos = RBs[i]->getWorldCoordinates(jPos);
			EEPos = RBs[i]->getWorldCoordinates(EEPos);
			normal = (EEPos - jPos).normalized();
			EEVal = EEPos.dot(normal);
			len = (EEPos - jPos).norm();
		}

		for (int z = 1; z < vertexNum[2] - 1; z++)
			for (int y = 1; y < vertexNum[1] - 1; y++)
				for (int x = 1; x < vertexNum[0] - 1; x++) {

					double val = curLS.getData(x, y, z);
					if (fabs(val) > LSStepSize) continue;
					
					double L = (curLS.getData(x + 1, y, z) + curLS.getData(x - 1, y, z) + curLS.getData(x, y + 1, z)
						+ curLS.getData(x, y - 1, z) + curLS.getData(x, y, z + 1) + curLS.getData(x, y, z - 1) - 6 * val) / (LSStepSize * LSStepSize);

					if (L > threshold)
						L -= threshold;
					else if (L < -threshold)
						L += threshold;
					else
						L = 0;

					double v = transParams->LSFlood_v;
					double alpha = transParams->LSFlood_alpha;

					double n;
					if (isEE)
					{
						bool test = false;
						for (int j = 0; j < (int)MlevelSets.size(); j++)
						{
							if (j != i && MlevelSets[j].getData(x, y, z) < LSStepSize)
							{
								test = true;
								break;
							}
						}

						if (!test)
						{
							P3D p = curLS.getVertex(x, y, z);
							double s = EEVal - p.dot(normal) / len;
							v = (s*s + 0.2) * v;
							if (s > 1.0 || s < 0.0)
								n = 0;
							else
								n = -L * alpha * LSStepSize + v;
						}
						else {
							n = -L * alpha * LSStepSize + v;
						}						
					}
					else {
						n = -L * alpha * LSStepSize + v;
					}

					MlevelSets[i].addData(x, y, z, -n);
				}
	}
	//Logger::print("Flooding: %lf\n", timer.timeEllapsed());

	//timer.restart();
#pragma omp parallel for
	for (int i = 0; i < (int)MlevelSets.size(); i++)
	{
		MlevelSets[i].intersectLS(&largeMeshLevelSet);
		MlevelSets[i].fastMarchingInward(LSStepSize);
		MlevelSets[i].fastMarchingOutward(LSStepSize, 4 * LSStepSize);
	}
	//Logger::print("FM: %lf\n", timer.timeEllapsed());

	// timer.restart();
	int fixNum = 0;
	for (uint i = 0; i < fixedLS.size(); i++)
		if (fixedLS[i]) fixNum++;

	if (fixNum == 0)
		projectMultiLevelSets(MlevelSets);
	else
		projectMultiLevelSetsWithFixedLS(MlevelSets, fixedLS);
	// Logger::print("Projection: %lf\n", timer.timeEllapsed());

}

void TransformEngine::extractMeshesFromLS(double levelSetVal)
{
	robot->setState(foldedState);
#pragma omp parallel for
	for (int i = 0; i < (int)RBs.size(); i++)
	{
		extractMeshesFromLSForRB(i, levelSetVal);
	}
}

void TransformEngine::extractMeshesFromLSForRB(int index, double levelSetVal /*= 0*/)
{
	LevelSet tmpLS = MlevelSets[index];
	if (transParams->shellOnly)
		tmpLS.intersectLS(&shellLevelSet);
	else
		tmpLS.intersectLS(&meshLevelSet);
	GLMesh* MCMesh = tmpLS.extractMesh(levelSetVal);

	if (!MCMesh)
	{
		// Logger::print("MCMesh %d empty!\n", index);
		if (RBs[index]->meshes.size() > 0) {
			delete RBs[index]->meshes[0];
			RBs[index]->meshes.clear();
			robotMeshes.erase(RBs[index]);
		}
		return;
	}

	RigidBody* rb = RBs[index];
	TransformUtils::transformMeshFromWorldToLocalCoords(MCMesh, rb);
	if (rb->meshes.empty())
	{
		rb->meshes.push_back(MCMesh);
	}
	else {
		delete rb->meshes[0];
		rb->meshes[0] = MCMesh;
	}

	robotMeshes[rb] = MCMesh;
}

void TransformEngine::saveLSToFile(const char* fName, bool sparse)
{
	FILE* fp = fopen(fName, "w+");

	for (uint i = 0; i < MlevelSets.size(); i++)
	{
		if (sparse)
			MlevelSets[i].saveSparseToFile(fp);
		else
			MlevelSets[i].saveToFile(fp);
	}

	fclose(fp);
}

void TransformEngine::loadLSFromFile(const char* fName, bool sparse)
{
	initialize();

	robot->setState(foldedState);

	FILE* fp = fopen(fName, "r");

	MlevelSets.assign(RBs.size(), LevelSet());
	for (uint i = 0; i < MlevelSets.size(); i++)
	{
		if (sparse)
			MlevelSets[i].loadSparseFromFile(fp);
		else
			MlevelSets[i].loadFromFile(fp);
	}
	fclose(fp);

	extractMeshesFromLS();
}

void TransformEngine::voxelize()
{
	if (!mesh) return;

	delete origVoxelGrid;
	delete expandedVoxelGrid;
	double voxelSize = transParams->voxelSize;

	AxisAlignedBoundingBox bbox = mesh->getBoundingBox();
	bbox.setbmin(bbox.bmin() + V3D(1, 1, 1) * -voxelSize);
	bbox.setbmax(bbox.bmax() + V3D(1, 1, 1) * voxelSize);
	origVoxelGrid = TransformUtils::voxelize(mesh, bbox, voxelSize, voxelSize, false);
	Logger::print("VoxelGrid VoxelNum:%d %d %d\n", origVoxelGrid->voxelNum[0], origVoxelGrid->voxelNum[1], origVoxelGrid->voxelNum[2]);

	bbox = mesh->getBoundingBox();
	bbox.setbmin(bbox.bmin() + V3D(1, 1, 1) * -4 * voxelSize);
	bbox.setbmax(bbox.bmax() + V3D(1, 1, 1) * 4 * voxelSize);
	expandedVoxelGrid = TransformUtils::voxelize(mesh, bbox, voxelSize, 4 * voxelSize, false);
	Logger::print("Expanded VoxelGrid VoxelNum:%d %d %d\n", expandedVoxelGrid->voxelNum[0], expandedVoxelGrid->voxelNum[1], expandedVoxelGrid->voxelNum[2]);

	boundaryVoxelGrid = TransformUtils::getBoundaryVoxelGrid(origVoxelGrid);
}

void TransformEngine::clearVoxelGrid() {
	delete origVoxelGrid;
	delete expandedVoxelGrid;
	delete boundaryVoxelGrid;
	origVoxelGrid = NULL;
	expandedVoxelGrid = NULL;
	boundaryVoxelGrid = NULL;
}


double TransformEngine::optimizeRobotState()
{
	initialize();

	voxelGrid = origVoxelGrid;

	if (robotObsolete)
		reinitializeRobot();
	return skeletonOptEngine->optimizeRobotState(foldedState, transParams->d_pos, RAD(transParams->d_angle), &jointSymmetryMap);
}

double TransformEngine::optimizeRobotStateCMA()
{
	initialize();

	voxelGrid = origVoxelGrid;

	if (robotObsolete)
		reinitializeRobot();	

	// timer.restart();
	double objVal = skeletonOptEngine->optimizeRobotStateCMA(foldedState);
	// Logger::print("CMA time: %lf\n", timer.timeEllapsed());

	robotEditor->adjustMeshTrans();

	return objVal;
}

double TransformEngine::optimizeRobotStateContinuous()
{
	initialize();

	if (robotObsolete)
		reinitializeRobot();
	return skeletonOptEngine->optimizeRobotStateContinous(foldedState, &jointSymmetryMap);
}

void TransformEngine::optimizeFoldingOrder()
{
	initialize();
	foldingOptEngine->optimizeFoldingOrder();
}

void TransformEngine::carveMeshForCurrentOrder()
{
	initialize();
	carvingEngine->carveMesh();
}

void TransformEngine::attachJointStructures()
{
	initialize();

	fabricationEngine->jointScale = transParams->jointScale;
	fabricationEngine->jointSocketMesh = jointSocketMesh;
	fabricationEngine->jointPlugMesh = jointPlugMesh;
	fabricationEngine->jointSocketCarveMesh = jointSocketCarveMesh;
	fabricationEngine->jointPlugCarveMesh = jointPlugCarveMesh;

	fabricationEngine->prepareForFabrication();

	fabricationEngine->getTransferDirection();

	Logger::print("Attach Skeletons...\n");
	fabricationEngine->attachSkeletonMeshes();

	Logger::print("Generate Slits...\n");
	fabricationEngine->generateJointSlitMeshes();

	Logger::print("Handle Body...\n");
	fabricationEngine->handleMainBody();

	Logger::print("Attach Joints...\n");
	fabricationEngine->attachJointMeshes();

	Logger::print("Attaching Joints Done!\n");
}

void TransformEngine::attachJointStructuresForLivingBracket()
{
	initialize();

	fabricationEngine->prepareForFabrication();

	Logger::print("Generating Joint Slit Meshes...\n");
	fabricationEngine->generateJointSlitMeshesForLivingBracket();

	Logger::print("Carving for Joint Slit...\n");
	fabricationEngine->carveForJointSlits();

	Logger::print("Merge Skeleton and Skin Meshes...\n");
	fabricationEngine->mergeSkeletonAndSkinMeshes();
}
