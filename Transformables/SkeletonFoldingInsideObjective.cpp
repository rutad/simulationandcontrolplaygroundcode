#include "SkeletonFoldingInsideObjective.h"

SkeletonFoldingInsideObjective::SkeletonFoldingInsideObjective(SkeletonFoldingPlan* mp, const std::string& objectiveDescription, double weight){
	plan = mp;
	levelSet = plan->levelSet;
	LSGrad = plan->LSGrad;
	LSHessian = plan->LSHessian;
	this->description = objectiveDescription;
	this->weight = weight;

}

SkeletonFoldingInsideObjective::~SkeletonFoldingInsideObjective(void){
}

double SkeletonFoldingInsideObjective::computeValue(const dVector& p){

	double retVal = 0;
	GeneralizedCoordinatesRobotRepresentation* gcRobot = plan->gcRobotRepresentation;
	targetVal = -8 * levelSet->stepSize;

	vector<RBBone>& bones = plan->bones;
	for (uint i = 0; i < bones.size(); i++)
	{
		vector<P3D>& localPoints = bones[i].localPoints;
		RigidBody* rb = bones[i].rb;
		for (uint j = 0; j < localPoints.size(); j++)
		{
			P3D point = gcRobot->getWorldCoordinatesFor(localPoints[j], rb);
			double LSVal = 1;
			if (levelSet->isInside(point[0], point[1], point[2]))
			{			
				LSVal = levelSet->interpolateData(point[0], point[1], point[2]);
			}
			retVal += 0.5 * (LSVal - targetVal) * (LSVal - targetVal);
		}
	}

	return retVal * weight;
}

void SkeletonFoldingInsideObjective::addGradientTo(dVector& grad, const dVector& p) {

	MatrixNxM dpdq;
	GeneralizedCoordinatesRobotRepresentation* gcRobot = plan->gcRobotRepresentation;
	targetVal = -8 * levelSet->stepSize;

	vector<RBBone>& bones = plan->bones;
	for (uint i = 0; i < bones.size(); i++)
	{
		vector<P3D>& localPoints = bones[i].localPoints;
		RigidBody* rb = bones[i].rb;
		for (uint j = 0; j < localPoints.size(); j++)
		{
			P3D point = gcRobot->getWorldCoordinatesFor(localPoints[j], rb);
			gcRobot->compute_dpdq(localPoints[j], rb, dpdq);

			if (levelSet->isInside(point[0], point[1], point[2]))
			{
				double LSVal = levelSet->interpolateData(point[0], point[1], point[2]);
				V3D G = LSGrad->interpolateData(point[0], point[1], point[2]);
				grad += dpdq.transpose() * G * (LSVal - targetVal) * weight;
			}
		}
	}

}

void SkeletonFoldingInsideObjective::addHessianEntriesTo(DynamicArray<MTriplet>& hessianEntries, const dVector& p) {

	MatrixNxM dpdq, dpdq_di;
	GeneralizedCoordinatesRobotRepresentation* gcRobot = plan->gcRobotRepresentation;
	targetVal = -8 * levelSet->stepSize;

	vector<RBBone>& bones = plan->bones;
	for (uint i = 0; i < bones.size(); i++)
	{
		vector<P3D>& localPoints = bones[i].localPoints;
		RigidBody* rb = bones[i].rb;
		for (uint j = 0; j < localPoints.size(); j++)
		{
			P3D point = gcRobot->getWorldCoordinatesFor(localPoints[j], rb);
			gcRobot->compute_dpdq(localPoints[j], rb, dpdq);

			if (levelSet->isInside(point[0], point[1], point[2]))
			{
				double LSVal = levelSet->interpolateData(point[0], point[1], point[2]);
				Vector3d G = LSGrad->interpolateData(point[0], point[1], point[2]);
				Matrix3x3 H = LSHessian->interpolateData(point[0], point[1], point[2]);
				//grad += dpdq.transpose() * G * (LSVal - targetVal) * weight;
				MatrixNxM T = (LSVal - targetVal) * H + G * G.transpose();
				MatrixNxM H1 = dpdq.transpose() * T * dpdq;
				for (int i = 0; i < H1.rows(); i++)
					for (int j = i; j < H1.cols(); j++) {
						ADD_HES_ELEMENT(hessianEntries, i, j, H1(i, j), weight);
					}

				V3D v = G * (LSVal - targetVal);
				for (int k = 0; k < plan->gcRobotRepresentation->getDimensionCount(); k++) {
					plan->gcRobotRepresentation->compute_ddpdq_dqi(localPoints[j], rb, dpdq_di, k);
					for (int l = k; l < plan->gcRobotRepresentation->getDimensionCount(); l++) {
						double val = dpdq_di.col(l).transpose().dot(v);
						ADD_HES_ELEMENT(hessianEntries, k, l, val, weight);
					}
				}

			}
		}
	}
}



