#pragma once

#include <OptimizationLib/ObjectiveFunction.h>
#include <MathLib/Matrix.h>
#include "SkeletonFoldingPlan.h"

class SkeletonFoldingInsideObjective : public ObjectiveFunction {

public:
	SkeletonFoldingInsideObjective(SkeletonFoldingPlan* mp, const std::string& objectiveDescription, double weight);
	virtual ~SkeletonFoldingInsideObjective(void);

	virtual double computeValue(const dVector& p);
	virtual void addHessianEntriesTo(DynamicArray<MTriplet>& hessianEntries, const dVector& p);
	virtual void addGradientTo(dVector& grad, const dVector& p);

private:
	SkeletonFoldingPlan* plan;
	LevelSet* levelSet;
	VertexGrid<V3D> *LSGrad;
	VertexGrid<Matrix3x3> *LSHessian;

	double targetVal;
};

