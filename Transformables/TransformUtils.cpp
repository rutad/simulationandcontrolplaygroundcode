#include <igl/signed_distance.h>
#include "TransformUtils.h"
#include <GUILib/GLContentManager.h>
#include <RBSimLib/HingeJoint.h>
#include <MathLib/ConvexHull3D.h>
#include <MathLib/MeshBoolean.h>
#include <GUILib/GLApplication.h>



TransformUtils::TransformUtils()
{
}


TransformUtils::~TransformUtils()
{
}


void TransformUtils::getCapsulesForRigidBody(RigidBody* rb, vector<Capsule>& capsules)
{
	P3D firstFeature = (rb->rbProperties.bodyPointFeatures.size() > 0) ? rb->rbProperties.bodyPointFeatures.begin()->coords : P3D();
	P3D lastFeature = (rb->rbProperties.bodyPointFeatures.size() > 0) ? (rb->rbProperties.bodyPointFeatures.end() - 1)->coords : P3D();

	firstFeature = rb->getWorldCoordinates(firstFeature);
	lastFeature = rb->getWorldCoordinates(lastFeature);

	capsules.clear();

	for (uint i = 0; i < rb->pJoints.size(); i++)
		capsules.push_back(Capsule(rb->getWorldCoordinates(rb->pJoints[i]->cJPos), firstFeature));

	for (int i = 0; i < (int)rb->rbProperties.bodyPointFeatures.size() - 1; i++)
		capsules.push_back(Capsule(rb->getWorldCoordinates(rb->rbProperties.bodyPointFeatures[i].coords), rb->getWorldCoordinates(rb->rbProperties.bodyPointFeatures[i + 1].coords)));

	for (uint i = 0; i < rb->cJoints.size(); i++)
		capsules.push_back(Capsule(lastFeature, rb->getWorldCoordinates(rb->cJoints[i]->pJPos)));

	for (uint i = 0; i < rb->rbProperties.endEffectorPoints.size(); i++)
		capsules.push_back(Capsule(lastFeature, rb->getWorldCoordinates(rb->rbProperties.endEffectorPoints[i].coords)));
}

double TransformUtils::getClosestDistToCapsules(const P3D& point, vector<Capsule>& capsules, int* capsuleIndex)
{
	double minDist = numeric_limits<double>::infinity();

	for (uint i = 0; i < capsules.size(); i++)
	{
		double dist = (capsules[i].seg.getClosestPointTo(point) - point).norm();
		if (dist < minDist)
		{
			minDist = dist;
			if (capsuleIndex) *capsuleIndex = i;
		}
	}

	return minDist;
}

double TransformUtils::getClosestDistToCapsules(const Ray& ray, vector<Capsule>& capsules)
{
	double minDist = numeric_limits<double>::infinity();

	for (uint i = 0; i < capsules.size(); i++)
	{
		double dist = ray.getDistanceToSegment(capsules[i].seg.a, capsules[i].seg.b);
		minDist = min(minDist, dist);
	}

	return minDist;
}

RigidBody* TransformUtils::getRobotClosestRigidBody(const P3D& point, map<RigidBody*, vector<Capsule>>& capsulesMap, double* dist, int* capsuleIndex)
{
	double minDist = numeric_limits<double>::infinity();
	RigidBody* closestRB = NULL;

	for (auto itr = capsulesMap.begin(); itr != capsulesMap.end(); itr++)
	{
		int tmpIndex;
		double dist = getClosestDistToCapsules(point, itr->second, &tmpIndex);

		if (dist < minDist)
		{
			minDist = dist;
			closestRB = itr->first;
			if (capsuleIndex)
				*capsuleIndex = tmpIndex;
		}
	}

	if (dist)
		*dist = minDist;

	return closestRB;
}

void TransformUtils::updateCapsuleMap(map<RigidBody*, vector<Capsule>>& capsulesMap, bool clearRadius)
{
	for (auto itr = capsulesMap.begin(); itr != capsulesMap.end(); itr++)
	{
		RigidBody* rb = itr->first;
		vector<Capsule>& capsules = itr->second;

		if (clearRadius)
		{
			getCapsulesForRigidBody(rb, capsules);
			continue;
		}

		int cIndex = 0;

		P3D firstFeature = (rb->rbProperties.bodyPointFeatures.size() > 0) ? rb->rbProperties.bodyPointFeatures.begin()->coords : P3D();
		P3D lastFeature = (rb->rbProperties.bodyPointFeatures.size() > 0) ? (rb->rbProperties.bodyPointFeatures.end() - 1)->coords : P3D();

		firstFeature = rb->getWorldCoordinates(firstFeature);
		lastFeature = rb->getWorldCoordinates(lastFeature);

		for (uint i = 0; i < rb->pJoints.size(); i++) {
			capsules[cIndex++].seg = Segment(rb->getWorldCoordinates(rb->pJoints[i]->cJPos), firstFeature);
		}

		for (int i = 0; i < (int)rb->rbProperties.bodyPointFeatures.size() - 1; i++) {
			capsules[cIndex++].seg = Segment(rb->getWorldCoordinates(rb->rbProperties.bodyPointFeatures[i].coords), rb->getWorldCoordinates(rb->rbProperties.bodyPointFeatures[i + 1].coords));
		}

		for (uint i = 0; i < rb->cJoints.size(); i++)
			capsules[cIndex++].seg = Segment(lastFeature, rb->getWorldCoordinates(rb->cJoints[i]->pJPos));

		for (uint i = 0; i < rb->rbProperties.endEffectorPoints.size(); i++)
			capsules[cIndex++].seg = Segment(lastFeature, rb->getWorldCoordinates(rb->rbProperties.endEffectorPoints[i].coords));
	}
}

void TransformUtils::updateCapsuleMapGC(map<RigidBody*, vector<Capsule>>& capsulesMap, GeneralizedCoordinatesRobotRepresentation* gcRobot, double radius)
{
	for (auto itr = capsulesMap.begin(); itr != capsulesMap.end(); itr++)
	{
		RigidBody* rb = itr->first;
		vector<Capsule>& capsules = itr->second;

		int cIndex = 0;

		P3D firstFeature = (rb->rbProperties.bodyPointFeatures.size() > 0) ? rb->rbProperties.bodyPointFeatures.begin()->coords : P3D();
		P3D lastFeature = (rb->rbProperties.bodyPointFeatures.size() > 0) ? (rb->rbProperties.bodyPointFeatures.end() - 1)->coords : P3D();

		firstFeature = gcRobot->getWorldCoordinatesFor(firstFeature, rb);
		lastFeature = gcRobot->getWorldCoordinatesFor(lastFeature, rb);

		for (uint i = 0; i < rb->pJoints.size(); i++) {
			capsules[cIndex++] = Capsule(gcRobot->getWorldCoordinatesFor(rb->pJoints[i]->cJPos, rb), firstFeature, radius);
		}

		for (int i = 0; i < (int)rb->rbProperties.bodyPointFeatures.size() - 1; i++) {
			capsules[cIndex++] = Capsule(gcRobot->getWorldCoordinatesFor(rb->rbProperties.bodyPointFeatures[i].coords, rb), gcRobot->getWorldCoordinatesFor(rb->rbProperties.bodyPointFeatures[i + 1].coords, rb), radius);
		}

		for (uint i = 0; i < rb->cJoints.size(); i++)
			capsules[cIndex++] = Capsule(lastFeature, gcRobot->getWorldCoordinatesFor(rb->cJoints[i]->pJPos, rb), radius);

		for (uint i = 0; i < rb->rbProperties.endEffectorPoints.size(); i++)
			capsules[cIndex++] = Capsule(lastFeature, gcRobot->getWorldCoordinatesFor(rb->rbProperties.endEffectorPoints[i].coords, rb), radius);
	}
}

void TransformUtils::updateVoxelInfoMap(map<RigidBody*, RBVoxelInfo>& voxelInfoMap)
{
	for (auto itr = voxelInfoMap.begin(); itr != voxelInfoMap.end(); itr++)
	{
		RigidBody* rb = itr->first;
		RBVoxelInfo& voxelInfo = itr->second;
		MatrixNxM& worldVoxels = voxelInfo.worldVoxels;
		MatrixNxM& localVoxels = voxelInfo.localVoxels;
		for (int i = 0; i < voxelInfo.N; i++)
		{
			P3D localVoxel(localVoxels(i, 0), localVoxels(i, 1), localVoxels(i, 2));
			worldVoxels.row(i) = rb->getWorldCoordinates(localVoxel);
		}
	}
}


void TransformUtils::transformMeshFromWorldToLocalCoords(GLMesh* inputMesh, RigidBody* parent) {
	for (int k = 0; k < inputMesh->getVertexCount(); k++) {
		P3D vertex = inputMesh->getVertex(k);
		V3D normal = inputMesh->getNormal(k);
		inputMesh->setVertex(k, parent->getLocalCoordinates(vertex));
		inputMesh->setVertexNormal(k, parent->getLocalCoordinates(normal));
	}
	//inputMesh->computeNormals();
}


void TransformUtils::transformMeshFromLocalToWorldCoords(GLMesh* inputMesh, RigidBody* parent) {
	for (int k = 0; k < inputMesh->getVertexCount(); k++) {
		P3D vertex = inputMesh->getVertex(k);
		V3D normal = inputMesh->getNormal(k);
		inputMesh->setVertex(k, parent->getWorldCoordinates(vertex));
		inputMesh->setVertexNormal(k, parent->getWorldCoordinates(normal));
	}
	//inputMesh->computeNormals();
}

void TransformUtils::transformMeshBetweenStates(GLMesh* inputMesh, RBState& startState, RBState& endState) {
	for (int k = 0; k < inputMesh->getVertexCount(); k++) {
		P3D vertex = inputMesh->getVertex(k);
		V3D normal = inputMesh->getNormal(k);
		inputMesh->setVertex(k, endState.getWorldCoordinates(startState.getLocalCoordinates(vertex)));
		inputMesh->setVertexNormal(k, endState.getWorldCoordinates(startState.getLocalCoordinates(normal)));
	}
	//inputMesh->computeNormals();
}

void TransformUtils::clearMeshMap(map<RigidBody*, GLMesh*>& meshMap)
{
	for (auto itr = meshMap.begin(); itr != meshMap.end(); itr++)
	{
		delete itr->second;
	}
	meshMap.clear();
}

void TransformUtils::getAnimationStates(vector<ReducedRobotState>& animationStates, vector<int>& order, ReducedRobotState* startState, ReducedRobotState* endState, double animationStep)
{
	ReducedRobotState* curState = new ReducedRobotState(*startState);   // the start state of a single joint transformation
	ReducedRobotState* targetState = new ReducedRobotState(*curState); // the end state of a single joint transformation
	int foldIndex = 0;
	double animation_t = 0.0;
	int curJoint = order[foldIndex];
	targetState->setJointRelativeOrientation(endState->getJointRelativeOrientation(curJoint), curJoint);

	while (1)
	{
		ReducedRobotState tmpState(curState, targetState, min(1.0, animation_t));
		animationStates.push_back(tmpState);

		animation_t += animationStep;

		if (animation_t > 1.0 + 0.5 * animationStep)
		{
			animation_t = 0.0;
			*curState = *targetState;
			foldIndex++;
			if (foldIndex >= (int)order.size()) break;

			int curJoint = order[foldIndex];
			targetState->setJointRelativeOrientation(endState->getJointRelativeOrientation(curJoint), curJoint);
		}
	}


	delete curState;
	delete targetState;
}

void TransformUtils::getAnimationStatesForMotor(vector<ReducedRobotState>& animationStates, vector<int>& order, ReducedRobotState* startState, ReducedRobotState* endState, double animationStep)
{
	ReducedRobotState* curState = new ReducedRobotState(*startState);   // the start state of a single joint transformation
	ReducedRobotState* targetState = new ReducedRobotState(*curState); // the end state of a single joint transformation
	int foldIndex = 0;
	double animation_t = 0.0;
	int curJoint = order[foldIndex];
	targetState->setJointRelativeOrientation(endState->getJointRelativeOrientation(curJoint), curJoint);

	while (1)
	{
		ReducedRobotState tmpState(curState, targetState, min(1.0, animation_t));
		animationStates.push_back(tmpState);

		animation_t += animationStep;

		if (animation_t > 1.0 - 0.5 * animationStep)
		{
			animation_t = 0.0;
			*curState = *targetState;
			foldIndex++;
			if (foldIndex >= (int)order.size()) break;

			int curJoint = order[foldIndex];
			targetState->setJointRelativeOrientation(endState->getJointRelativeOrientation(curJoint), curJoint);
		}
	}

	animationStates.push_back(*endState);

	delete curState;
	delete targetState;
}

void TransformUtils::getAnimationStatesForJoint(vector<ReducedRobotState>& animationStates, int JointIndex, ReducedRobotState* startState, ReducedRobotState* endState, double animationStep)
{
	ReducedRobotState* curState = new ReducedRobotState(*startState);   // the start state of a single joint transformation
	ReducedRobotState* targetState = new ReducedRobotState(*curState); // the end state of a single joint transformation
	double animation_t = 0.0;
	
	targetState->setJointRelativeOrientation(endState->getJointRelativeOrientation(JointIndex), JointIndex);

	while (1)
	{
		ReducedRobotState tmpState(curState, targetState, min(1.0, animation_t));
		animationStates.push_back(tmpState);

		animation_t += animationStep;

		if (animation_t > 1.0 + 0.5 * animationStep)
		{
			break;
		}
	}

	delete curState;
	delete targetState;
}

void TransformUtils::getParentAndChildStatesForJoint(vector<RBState>& parentStates, vector<RBState>& childStates, Robot* robot, int JointIndex, ReducedRobotState* startState, ReducedRobotState* endState, double animationStep)
{
	vector<ReducedRobotState> sampleStates;
	TransformUtils::getAnimationStatesForJoint(sampleStates, JointIndex, startState, endState, animationStep);
	RigidBody* parentRB = robot->getJoint(JointIndex)->parent;
	RigidBody* childRB = robot->getJoint(JointIndex)->child;

	for (auto& state : sampleStates)
	{
		robot->setState(&state);
		parentStates.push_back(parentRB->state);
		childStates.push_back(childRB->state);
	}
}

vector<FoldingInfo> TransformUtils::getAnimationStatesAndFoldingSequence(vector<ReducedRobotState>& animationStates, Robot* robot, vector<int>& order, ReducedRobotState* startState, ReducedRobotState* endState, double animationStep)
{
	vector<FoldingInfo> foldingSequence;
	FoldingInfo fInfo;

	ReducedRobotState* curState = new ReducedRobotState(*startState);   // the start state of a single joint transformation
	ReducedRobotState* targetState = new ReducedRobotState(*curState); // the end state of a single joint transformation
	int foldIndex = 0;
	double animation_t = 0.0;
	int curJoint = order[foldIndex];
	targetState->setJointRelativeOrientation(endState->getJointRelativeOrientation(curJoint), curJoint);

	fInfo.startIndex = 0;
	fInfo.joint = robot->getJoint(curJoint);

	while (1)
	{
		ReducedRobotState tmpState(curState, targetState, min(1.0, animation_t));
		animationStates.push_back(tmpState);

		animation_t += animationStep;

		if (animation_t > 1.0 + 0.5 * animationStep)
		{
			fInfo.endIndex = animationStates.size();
			getMovingRB(fInfo.movingRBs, robot, curState, targetState);
			foldingSequence.push_back(fInfo);
			fInfo.startIndex = fInfo.endIndex;

			animation_t = 0.0;
			*curState = *targetState;
			foldIndex++;
			if (foldIndex >= (int)order.size()) break;

			int curJoint = order[foldIndex];
			fInfo.joint = robot->getJoint(curJoint);

			targetState->setJointRelativeOrientation(endState->getJointRelativeOrientation(curJoint), curJoint);
		}
	}


	delete curState;
	delete targetState;

	return foldingSequence;
}

void TransformUtils::getMovingRB(set<RigidBody*>& movingRBs, Robot* robot, ReducedRobotState* curState, ReducedRobotState* targetState)
{
	movingRBs.clear();

	vector<RBState> curRBStates;
	robot->setState(curState);

	for (int i = 0; i < robot->getJointCount(); i++)
		curRBStates.push_back(robot->getJoint(i)->child->state);

	robot->setState(targetState);

	for (int i = 0; i < robot->getJointCount(); i++) {
		RigidBody* rb = robot->getJoint(i)->child;
		RBState rbState = rb->state;
		if (rbState.orientation != curRBStates[i].orientation
			|| rbState.position != curRBStates[i].position)
		{
			movingRBs.insert(rb);
		}
	}
}

void TransformUtils::saveVoxelGridToFile(const char* fName, VoxelGrid<int>* voxelGrid)
{
	FILE* fp = fopen(fName, "w+");

	saveVoxelGridToFile(fp, voxelGrid);

	fclose(fp);
}

void TransformUtils::saveVoxelGridToFile(FILE* fp, VoxelGrid<int>* voxelGrid)
{
	fprintf(fp, "%lf %lf %lf\n", voxelGrid->startPoint[0], voxelGrid->startPoint[1], voxelGrid->startPoint[2]);
	fprintf(fp, "%lf %lf %lf\n", voxelGrid->endPoint[0], voxelGrid->endPoint[1], voxelGrid->endPoint[2]);
	fprintf(fp, "%lf %d %d %d %d %d\n", voxelGrid->voxelSize, voxelGrid->totalVoxelNum, voxelGrid->layerVoxelNum
		, voxelGrid->voxelNum[0], voxelGrid->voxelNum[1], voxelGrid->voxelNum[2]);

	for (int i = 0; i < voxelGrid->totalVoxelNum; i++)
	{
		fprintf(fp, "%d ", voxelGrid->getData(i));
	}
}

VoxelGrid<int>* TransformUtils::loadVoxelGridFromFile(const char* fName)
{
	FILE* fp = fopen(fName, "r");

	VoxelGrid<int>* voxelGrid = loadVoxelGridFromFile(fp);

	fclose(fp);
	return voxelGrid;
}

VoxelGrid<int>* TransformUtils::loadVoxelGridFromFile(FILE* fp)
{
	VoxelGrid<int>* voxelGrid = new VoxelGrid<int>();

	fscanf(fp, "%lf %lf %lf\n", &voxelGrid->startPoint[0], &voxelGrid->startPoint[1], &voxelGrid->startPoint[2]);
	fscanf(fp, "%lf %lf %lf\n", &voxelGrid->endPoint[0], &voxelGrid->endPoint[1], &voxelGrid->endPoint[2]);
	fscanf(fp, "%lf %d %d %d %d %d\n", &voxelGrid->voxelSize, &voxelGrid->totalVoxelNum, &voxelGrid->layerVoxelNum
		, &voxelGrid->voxelNum[0], &voxelGrid->voxelNum[1], &voxelGrid->voxelNum[2]);

	voxelGrid->data.resize(voxelGrid->totalVoxelNum);

	for (int i = 0; i < voxelGrid->totalVoxelNum; i++)
	{
		fscanf(fp, "%d ", &voxelGrid->getData(i));
	}

	return voxelGrid;
}

VoxelGrid<int>* TransformUtils::voxelize(GLMesh* mesh, AxisAlignedBoundingBox bbox, double voxelSize, double levelSetVal, bool shellOnly)
{
	if (!mesh) return NULL;

	VoxelGrid<int> * voxelGrid = new VoxelGrid<int>(bbox, voxelSize);

	Eigen::MatrixXd V, N;
	Eigen::MatrixXi F;
	mesh->getMeshMatrices(V, F, N);

	Eigen::MatrixXd P;
	P.resize(voxelGrid->totalVoxelNum, 3);
	int index = 0;
	for (int z = 0; z < voxelGrid->voxelNum[2]; z++)
		for (int y = 0; y < voxelGrid->voxelNum[1]; y++)
			for (int x = 0; x < voxelGrid->voxelNum[0]; x++) {
				P.row(index++) = voxelGrid->getVoxelCenter(x, y, z);
			}

	Eigen::VectorXd S;
	Eigen::VectorXi I;
	Eigen::MatrixXd C;
	igl::signed_distance(P, V, F, igl::SIGNED_DISTANCE_TYPE_WINDING_NUMBER, S, I, C, N);

	for (int i = 0; i < S.size(); i++)
	{
		voxelGrid->setData(i, S[i] <= levelSetVal ? -1 : -2);
		//Logger::print("%d: %lf\n", i, S[i]);
	}

	if (shellOnly)
	{
		// First Mark inner voxels
		for (int z = 1; z < voxelGrid->voxelNum[2] - 1; z++)
			for (int y = 1; y < voxelGrid->voxelNum[1] - 1; y++)
				for (int x = 1; x < voxelGrid->voxelNum[0] - 1; x++) {
					if (voxelGrid->getData(x, y, z) == -2) continue;

					if (voxelGrid->getData(x + 1, y, z) >= -1 && voxelGrid->getData(x - 1, y, z) >= -1 &&
						voxelGrid->getData(x, y + 1, z) >= -1 && voxelGrid->getData(x, y - 1, z) >= -1 &&
						voxelGrid->getData(x, y, z + 1) >= -1 && voxelGrid->getData(x, y, z - 1) >= -1)
					{
						voxelGrid->setData(x, y, z, 0);
					}
				}

		// Then set inner voxels to invalid
		for (int z = 1; z < voxelGrid->voxelNum[2] - 1; z++)
			for (int y = 1; y < voxelGrid->voxelNum[1] - 1; y++)
				for (int x = 1; x < voxelGrid->voxelNum[0] - 1; x++) {
					if (voxelGrid->getData(x, y, z) == 0)
						voxelGrid->setData(x, y, z, -2);
				}
	}

	return voxelGrid;
}

VoxelGrid<int>* TransformUtils::getBoundaryVoxelGrid(VoxelGrid<int>* origGrid)
{
	VoxelGrid<int>* newGrid = new VoxelGrid<int>(*origGrid);

	for (int z = 1; z < newGrid->voxelNum[2] - 1; z++)
		for (int y = 1; y < newGrid->voxelNum[1] - 1; y++)
			for (int x = 1; x < newGrid->voxelNum[0] - 1; x++) {
				if (origGrid->getData(x, y, z) == -2) {
					continue;
				}

				if (origGrid->getData(x + 1, y, z) >= -1 && origGrid->getData(x - 1, y, z) >= -1 &&
					origGrid->getData(x, y + 1, z) >= -1 && origGrid->getData(x, y - 1, z) >= -1 &&
					origGrid->getData(x, y, z + 1) >= -1 && origGrid->getData(x, y, z - 1) >= -1)
				{
					newGrid->setData(x, y, z, -2);
				}
			}

	for (int i = 0; i < newGrid->totalVoxelNum; i++)
	{
		if (newGrid->getData(i) == -2){
			newGrid->setData(i, 0);
		}
		else {
			newGrid->setData(i, 1);
		}
	}

	return newGrid;
}

bool TransformUtils::testMeshIntersectSDF(GLMesh* mesh, RigidBody* rb, LevelSet* SDF)
{
	for (int i = 0; i < mesh->getVertexCount(); i++)
	{
		// get voxel center position
		P3D c = mesh->getVertex(i);
		// tranform the center into RB_i's local coordiante.
		c = rb->getLocalCoordinates(c);

		if (SDF->isInside(c[0], c[1], c[2]))
		{
			double val = SDF->interpolateData(c[0], c[1], c[2]);
			if (val < 0.05)
			{
				return true;
			}
		}
	}

	return false;
}

void TransformUtils::clearVoxelAssignment(VoxelGrid<int>* voxelGrid)
{
	for (int i = 0; i < voxelGrid->totalVoxelNum; i++)
	{
		if (voxelGrid->getData(i) != -2)
		{
			voxelGrid->setData(i, -1);
		}
	}
}

void TransformUtils::updateCapsulesMapRadius(vector<RigidBody*>& RBs, map<RigidBody*, vector<Capsule>>& capsulesMap)
{
	for (uint i = 0; i < RBs.size(); i++) {
		RigidBody* rb = RBs[i];
		if (rb->meshes.empty()) continue;
		GLMesh* mesh = rb->meshes[0];

		for (int j = 0; j < mesh->getVertexCount(); j++)
		{
			P3D v = rb->getWorldCoordinates(mesh->getVertex(j));

			vector<Capsule>& capsules = capsulesMap[rb];
			int capsuleIndex;
			double dist = TransformUtils::getClosestDistToCapsules(v, capsules, &capsuleIndex);

			Capsule& cap = capsules[capsuleIndex];
			cap.r = max(cap.r, dist);
		}
	}
}

GLMesh* TransformUtils::getCylinderMesh(const P3D& start, const P3D& end, double radius)
{
	GLMesh* cylinderMesh = GLContentManager::getGLMesh("../data/3dModels/cylinder.obj")->clone();

	cylinderMesh->scale(V3D(radius, (end - start).norm() / 2, radius), P3D());

	Eigen::Quaterniond q;
	q.setFromTwoVectors(V3D(0, 1, 0), (end - start).normalized());
	Quaternion nq;
	nq.setRotationFrom(q.toRotationMatrix());
	cylinderMesh->rotate(nq, P3D());

	cylinderMesh->translate((end + start) / 2);

	return cylinderMesh;
}

Quaternion TransformUtils::getPlugJointQuaternion(Joint* joint)
{
	V3D cJVec = joint->cJPos;
	V3D jointAxis = joint->child->getLocalCoordinates(joint->parent->getWorldCoordinates(((HingeJoint*)joint)->rotationAxis));
	Eigen::Quaterniond q1;
	q1.setFromTwoVectors(V3D(1, 0, 0), jointAxis.normalized());
	V3D zAxis = q1._transformVector(V3D(0, 0, 1));
	zAxis = zAxis - jointAxis * zAxis.dot(jointAxis);
	cJVec = cJVec - jointAxis * cJVec.dot(jointAxis);
	Eigen::Quaterniond q2;
	q2.setFromTwoVectors(zAxis, cJVec);
	Quaternion q;
	q.setRotationFrom((q2 * q1).toRotationMatrix());

	return q;
}

Quaternion TransformUtils::getSocketJointQuaternion(Joint* joint)
{
	V3D pJVec = joint->pJPos;
	V3D jointAxis = ((HingeJoint*)joint)->rotationAxis;
	Eigen::Quaterniond q1;
	q1.setFromTwoVectors(V3D(1, 0, 0), jointAxis.normalized());
	V3D zAxis = q1._transformVector(V3D(0, 0, 1));
	zAxis = zAxis - jointAxis * zAxis.dot(jointAxis);
	pJVec = pJVec - jointAxis * pJVec.dot(jointAxis);
	Eigen::Quaterniond q2;
	q2.setFromTwoVectors(zAxis, pJVec);
	Quaternion q;
	q.setRotationFrom((q2 * q1).toRotationMatrix());

	return q;
}

Transformation TransformUtils::getPlugJointTransformation(Joint* joint)
{
	return Transformation(getPlugJointQuaternion(joint).getRotationMatrix(), joint->cJPos);
}

Transformation TransformUtils::getSocketJointTransformation(Joint* joint)
{
	return Transformation(getSocketJointQuaternion(joint).getRotationMatrix(), joint->pJPos);
}

GLMesh* TransformUtils::getTransformedJointMesh(GLMesh* jointMesh, Joint* joint, double scale, bool childJoint)
{
	P3D jPos = childJoint ? joint->pJPos : joint->cJPos;
	V3D jVec(jPos);
	V3D jointAxis = ((HingeJoint*)joint)->rotationAxis;

	Eigen::Quaterniond q1;
	q1.setFromTwoVectors(V3D(1, 0, 0), jointAxis.normalized());
	V3D zAxis = q1._transformVector(V3D(0, 0, 1));
	zAxis = zAxis - jointAxis * zAxis.dot(jointAxis);
	jVec = jVec - jointAxis * jVec.dot(jointAxis);
	Eigen::Quaterniond q2;
	q2.setFromTwoVectors(zAxis, jVec);
	Quaternion q;
	q.setRotationFrom((q2 * q1).toRotationMatrix());

	GLMesh* nMesh = jointMesh->clone();
	nMesh->scale(scale, P3D());
	nMesh->rotate(q, P3D());
	nMesh->translate(jPos);

	return nMesh;
}

double TransformUtils::getPointDistToLine(const P3D& point, const P3D& pLine, const V3D& normal)
{
	V3D e = point - pLine;
	e -= normal * e.dot(normal);

	return e.length();
}

GLMesh* TransformUtils::computeJointSlitMesh(P3D orig, P3D endA, P3D endB, double width, double radius)
{
	vector<P3D> points;
	
	if (endA == endB)
		endB[1] += 1e-5;

	V3D n = (endA - orig).cross(endB - orig).normalized();
	V3D eA = (endA - orig).cross(n).normalized();
	V3D eB = (endB - orig).cross(n).normalized();
	V3D eC = eA.cross(n).normalized();

	for (int i = 0; i < 360; i += 30)
	{
		double theta = RAD(i);
		V3D v = radius * (cos(theta) * eA + sin(theta) * eC);
		points.push_back(orig + n * width + v);
		points.push_back(orig + n * -width + v);
	}

	points.push_back(endA + n * width + eA * radius);
	points.push_back(endA + n * width + eA * -radius);
	points.push_back(endA + n * -width + eA * radius);
	points.push_back(endA + n * -width + eA * -radius);
	points.push_back(endB + n * width + eB * radius);
	points.push_back(endB + n * width + eB * -radius);
	points.push_back(endB + n * -width + eB * radius);
	points.push_back(endB + n * -width + eB * -radius);

	GLMesh* mesh = new GLMesh();
	ConvexHull3D::computeConvexHullFromSetOfPoints(points, mesh);
	mesh->computeNormals();

	return mesh;
}

GLMesh* TransformUtils::computeJointSlitMesh(P3D orig, vector<P3D>& endPoints, double width, double radius)
{
	GLMesh* resMesh = NULL;

	vector<P3D> points;

	P3D endA = endPoints.front();
	P3D endB = endPoints.back();
	if (endA == endB)
		endB[1] += 1e-5;

	V3D n = (endA - orig).cross(endB - orig).normalized();
	V3D eA = (endA - orig).cross(n).normalized();
	V3D eB = (endB - orig).cross(n).normalized();
	V3D eC = eA.cross(n).normalized();

	for (int i = 0; i < 360; i += 30)
	{
		double theta = RAD(i);
		V3D v = radius * (cos(theta) * eA + sin(theta) * eC);
		points.push_back(orig + n * width + v);
		points.push_back(orig + n * -width + v);
	}

	for (auto& p : endPoints)
	{
		V3D e = (p - orig).cross(n).normalized();
		points.push_back(p + n * width + e * radius);
		points.push_back(p + n * width + e * -radius);
		points.push_back(p + n * -width + e * radius);
		points.push_back(p + n * -width + e * -radius);
	}

	GLMesh* mesh = new GLMesh();
	ConvexHull3D::computeConvexHullFromSetOfPoints(points, mesh);
	mesh->computeNormals();

	return mesh;
}

void TransformUtils::applyGLTransformForRB(RigidBody* rb)
{
	RBState& state = rb->state;
	glTranslated(state.position[0], state.position[1], state.position[2]);
	//and rotation part
	V3D rotAxis; double rotAngle;
	state.orientation.getAxisAngle(rotAxis, rotAngle);
	glRotated(DEG(rotAngle), rotAxis[0], rotAxis[1], rotAxis[2]);
}

double TransformUtils::GaussianKernel(double d, double sigma2, double a)
{
	return a * exp(-0.5 * d * d / sigma2);
}

dVector TransformUtils::getJointAnglesForCurrentState(Robot* robot, ReducedRobotState* robotState)
{
	int jointNum = robot->getJointCount();
	dVector jointAngles(jointNum);

	for (int i = 0; i < jointNum; i++)
	{
		HingeJoint* joint = (HingeJoint*)robot->getJoint(i);
		Quaternion qRel = robotState->getJointRelativeOrientation(i);

		V3D axis = joint->rotationAxis; axis.toUnit();
		double rotAngle = qRel.getRotationAngle(axis);
		jointAngles[i] = rotAngle;
	}

	return jointAngles;
}

