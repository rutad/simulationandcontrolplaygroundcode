

#include <GUILib/GLUtils.h>
#include "TransformablesApp.h"
#include <GUILib/GLMesh.h>
#include <GUILib/GLContentManager.h>
#include <GUILib/GLTrackingCamera.h>
#include <MathLib/MathLib.h>
#include <iostream>
#include <MathLib/boundingBox.h>
#include <GUILib/OBJReader.h>
#include <windows.h>


using namespace std;

/*
- Use the config file '..data/transform/config/config1.txt' to specify configurations.
- Key {'Q','W','E'} to switch between run options {'Skeleton Optimization', 'Folding Order Optimization', 'Levelset Flooding'}
  Note: press Q twice will also switch between 'Random Sampling Method' and 'Continous Gradient Method'.
- Key 'up', 'down' to scale the target mesh.
- Once the skeleton is in place, use key 'A' to generate the initial level sets. Then just press key 'space' to run the level set flooding.
- Key 'H' will generate editing points. Click and drag editing points will edit the interface.
- Key 'S' will do the mesh transfer. (Note: After mesh transfer, it is better to do some steps of level set flooding to smoothout sharp features.)
- Key 'D' will do boolean operations between real mesh and bone mehses to get higher quality results.
- Key 'X' will play motion plans.
- Key 'Z' will delete the level set for the selected bone. To select a bone, just click on it, the program will only show its mesh when successful.
- Key 'B' will save all bone meshes to '../out/transformableMeshes/', need to have this directory first.
- Key 'J' will carve for skeleton throughout motion sequence.
- Key 'K' will carve for meshes throughout motion sequence.
- Key 'L' will generate socket and attachment structure to meshes and skeleton.
- Key 'O' will save skeleton meshes to '../out/transformableMeshes/'.

===================================================================================================================================================

- Button 'Fold' (key 'F') and 'Unfold' to see the folding or unfolding animation. animationStep
  can be changed.
- Button 'SaveLevelSets' will save current level sets to '../out/tmpLevelSets.ls'.
  To load the level sets, you can either drag the file in or specify it in the config file. 
- Button 'SaveRobotState' will save current foldedRobotState and unfoldedRobotState to
  '../out/FoldedState.txt' and '../out/UnFoldedState.txt'.
*/

static void TW_CALL action(void* clientData) {
	TransformablesApp* app = dynamic_cast<TransformablesApp*>(GLApplication::getGLAppInstance());
	if (app == NULL)
		return;

	if (strcmp((char*)clientData, "assignMesh") == 0)
	{
		app->assignMesh();
	}	

	if (strcmp((char*)clientData, "voxelize") == 0)
	{
		app->voxelize();
	}

	if (strcmp((char*)clientData, "saveRobotState") == 0)
	{
		app->saveRobotState();
	}
	
	if (strcmp((char*)clientData, "saveRobot") == 0)
	{
		app->saveRobotToRBS();
	}

	if (strcmp((char*)clientData, "fold") == 0)
	{
		app->foldAnimation();
	}

	if (strcmp((char*)clientData, "unfold") == 0)
	{
		app->unfoldAnimation();
	}

	if (strcmp((char*)clientData, "carveMesh") == 0)
	{
		app->carveMesh();
	}

	if (strcmp((char*)clientData, "saveVoxelGrid") == 0)
	{
		app->saveVoxelGrid();
	}

	if (strcmp((char*)clientData, "saveLevelSets") == 0)
	{
		app->transformEngine->saveLSToFile("../out/levelSets.lss");
	}

	if (strcmp((char*)clientData, "saveConfig") == 0)
	{
		app->saveConfig();
	}

	if (strcmp((char*)clientData, "saveRobotDesign") == 0)
	{
		app->transformEngine->robotEditor->saveParamsToFile("../out/robotDim.rdp");
	}
	
	if (strcmp((char*)clientData, "splitMeshVertices") == 0)
	{
		app->splitRobotMeshVertices();
	}
}

void TransformablesApp::optimizeRobotState()
{
	showMesh = true;
	controlParams.showRobotMeshes = false;
	if (editingRobot) return;

	double objVal;
	if (runOption == SKELETON_OPT) {
		objVal = transformEngine->optimizeRobotState();
	}
	else if (runOption == SKELETON_CONT_OPT) {
		objVal = transformEngine->optimizeRobotStateContinuous();
	}
	else {
		objVal = transformEngine->optimizeRobotStateCMA();
		if (curConfig.designName.empty())
			transformEngine->fabricationEngine->generateCylinderSkeletonMeshes();
	}

	robot->setState(foldedState);
	if (pickedJoint >= 0)
	{
		rWidget->pos = robot->getJoint(pickedJoint)->getWorldPosition();
	}
	else if (pickedJoint == ROOT_ID)
	{
		rWidget->pos = tWidget->pos = robot->root->state.position;
	}
}

void TransformablesApp::optimizeFoldingOrder()
{
	if (transformEngine->robotMeshes.empty())
		assignMesh();

	transformEngine->optimizeFoldingOrder();
}

void TransformablesApp::carveMesh()
{
	timer.restart();

	if (transformEngine->foldingOrder.empty())
		optimizeFoldingOrder();

	transformEngine->carveMeshForCurrentOrder();
	// transformEngine->fabricationEngine->carveSkeletonPreciselyForRB();

	Logger::print("Mesh carving time: %lfs\n", timer.timeEllapsed());
}

void TransformablesApp::assignMesh()
{
	timer.restart();

	//if (!transformEngine->origVoxelGrid)
		//voxelize();

	robot->setState(foldedState);
	transformEngine->assignLSMeshToRobot();
	showMesh = false;
	showVoxelGrid = false;
	runOption = FOLD_ORDER_OPT;

	Logger::print("Mesh assignment time: %lfs\n", timer.timeEllapsed());

	return;
}

void TransformablesApp::foldAnimation()
{
	if (transformEngine->foldingOrder.empty())
	{
		Logger::consolePrint("No folding order!\n");
		return;
	}

	sideWindow->prepareForAnimation(transformEngine->foldingOrder, unfoldedState, foldedState, transParams.animationStep);
}

void TransformablesApp::unfoldAnimation()
{
	if (transformEngine->foldingOrder.empty())
	{
		optimizeFoldingOrder();
	}
	vector<int> unfoldOrder = transformEngine->foldingOrder;
	reverse(unfoldOrder.begin(), unfoldOrder.end());

	sideWindow->prepareForAnimation(unfoldOrder, foldedState, unfoldedState, transParams.animationStep);
}

void TransformablesApp::voxelize()
{
	transformEngine->voxelize();
	showVoxelGrid = true;
}

void TransformablesApp::getRBColorMap()
{
	srand(1);
	colorMap.clear();
	colorMap[robot->getRoot()] = getRandomUnitVector();
	for (uint i = 0; i < robot->jointList.size(); i++) {
		colorMap[robot->getJoint(i)->child] = getRandomUnitVector();
	}
}

void TransformablesApp::saveRobotState()
{
	if (robot)
	{
		Logger::consolePrint("Robot state saved!");
		foldedState->writeToFile("../out/folded.rs");
		unfoldedState->writeToFile("../out/unfolded.rs");
		transformEngine->robotEditor->saveParamsToFile("../out/robotDim.rdp");
	}
}

void TransformablesApp::saveRobotToRBS()
{
	if (robot)
	{
		Logger::consolePrint("Robot saved!");
		robot->setState(startState);
		robot->saveRBSToFile("../out/tmpRobot.rbs");
	}
}

void TransformablesApp::saveVoxelGrid()
{
	if (transformEngine->origVoxelGrid)
		TransformUtils::saveVoxelGridToFile("../out/tmpVoxelGrid.txt", transformEngine->voxelGrid);
}

void TransformablesApp::savePickedRBMesh()
{
	if (pickedRB && transformEngine->robotMeshes.count(pickedRB))
	{
		FILE* fp = fopen("../out/tmpRBMesh.obj", "w+");
		transformEngine->robotMeshes[pickedRB]->renderToObjFile(fp, 0, Quaternion(), P3D());
		fclose(fp);
	}
	
}

TransformablesApp::TransformablesApp() {
	setWindowTitle("Empty Test Application...");

	camera->onMouseWheelScrollEvent(0, 10.0);

	tWidget = new TranslateWidget();
	rWidget = new RotateWidgetV2();
	tWidget->visible = false;
	rWidget->visible = false;

	controlParams.colorMap = &colorMap;
	controlParams.jointSymmetryMap = &jointSymmetryMap;

	transParams.voxelSize = 0.01;
	transParams.LSStepSize = 0.01;
	transParams.d_pos = 0.1;
	transParams.d_angle = 180;
	transParams.sampleStep = 0.10;   // sampling step size when doing collision detection.
	transParams.animationStep = 0.05;
	transParams.jointScale = 1;
	transParams.smoothVoxels = true;
	transParams.maxFloodIter = 100;
	transParams.shellOnly = false;
	transParams.sharpFeet = false;
	transParams.IFPSpace = 0.015;
	transParams.skeletonRadius = 0.010;
	transParams.LSCylinderR = 0.016;
	transParams.LSFlood_L = 80;
	transParams.LSFlood_alpha = 1e-3;
	transParams.LSFlood_v = 1e-3;
	transParams.shellThickness = 0.005;
	transParams.sktInsideThreshold = 0.02;
	transParams.minTransferR = 0.02;


	showGroundPlane = false;

	TwAddSeparator(mainMenuBar, "sep2", "");
	TwAddButton(mainMenuBar, "SaveRobotDesign", action, "saveRobotDesign", "");
	TwAddButton(mainMenuBar, "SaveConfig", action, "saveConfig", "");
	TwAddButton(mainMenuBar, "SaveLevelSets", action, "saveLevelSets", "");
	TwAddButton(mainMenuBar, "SaveRobotState", action, "saveRobotState", "");
	TwAddButton(mainMenuBar, "SaveRobot", action, "saveRobot", "");
	TwAddButton(mainMenuBar, "SaveVoxelGrid", action, "saveVoxelGrid", "");

	TwAddSeparator(mainMenuBar, "sep3", "");
	TwAddButton(mainMenuBar, "AssignMesh", action, "assignMesh", "");
	TwAddButton(mainMenuBar, "CarveMesh", action, "carveMesh", "");
	// TwAddButton(mainMenuBar, "Voxelize", action, "voxelize", "");
	TwAddButton(mainMenuBar, "Fold", action, "fold", "");
	TwAddButton(mainMenuBar, "Unfold", action, "unfold", "");
	TwAddButton(mainMenuBar, "SplitMeshVertices", action, "splitMeshVertices", "");
	
	
	TwAddVarRW(mainMenuBar, "RunOptions", TwDefineEnumFromString("RunOptions", "..."), &runOption, "");
	DynamicArray<std::string> runOptionList;
	runOptionList.push_back("\\Skeleton CMA Optimization");
	runOptionList.push_back("\\Skeleton Optimization");
	runOptionList.push_back("\\Skeleton Continous Optimization");
	runOptionList.push_back("\\Folding Order Optimization");
	runOptionList.push_back("\\Level Set Flooding");
	runOptionList.push_back("\\Carve and Flood");
	runOptionList.push_back("\\Evolve Interface");
	generateMenuEnumFromFileList("MainMenuBar/RunOptions", runOptionList);

	TwAddSeparator(mainMenuBar, "sep4", "");
	TwAddVarRW(mainMenuBar, "UseHingeJoint", TW_TYPE_BOOLCPP, &controlParams.useHingeJoint, "");
	TwAddVarRW(mainMenuBar, "UseMaterial", TW_TYPE_BOOLCPP, &controlParams.useMaterial, "");
	TwAddVarRW(mainMenuBar, "ShowTransferedMesh", TW_TYPE_BOOLCPP, &showTransferedMesh, "");

	TwAddSeparator(mainMenuBar, "sep5", "");
	TwAddVarRW(mainMenuBar, "ShowMesh", TW_TYPE_BOOLCPP, &showMesh, "");
	TwAddVarRW(mainMenuBar, "ShowRobotMeshes", TW_TYPE_BOOLCPP, &controlParams.showRobotMeshes, "");
	TwAddVarRW(mainMenuBar, "ShowSkeletonOrigMeshes", TW_TYPE_BOOLCPP, &controlParams.showSkeletonOrigMeshes, "");
	TwAddVarRW(mainMenuBar, "ShowRobotAbstractView", TW_TYPE_BOOLCPP, &controlParams.showRobotAbstractView, "");
	TwAddVarRW(mainMenuBar, "ShowSkeletonLSMeshes", TW_TYPE_BOOLCPP, &controlParams.showSkeletonLSMeshes, "");
	TwAddVarRW(mainMenuBar, "ShowSkeletonSamples", TW_TYPE_BOOLCPP, &controlParams.showSkeletonSamples, "");
	TwAddVarRW(mainMenuBar, "ShowJointBoundries", TW_TYPE_BOOLCPP, &showJointBoundaries, "");

	//TwAddVarRW(mainMenuBar, "ShowVoxelGrid", TW_TYPE_BOOLCPP, &showVoxelGrid, "");
	//TwAddVarRW(mainMenuBar, "ShowAllThickness", TW_TYPE_BOOLCPP, &controlParams.showAllThickness, "");
	//TwAddVarRW(mainMenuBar, "ShowSkeletonCarvingMeshes", TW_TYPE_BOOLCPP, &controlParams.showCarveMeshes, "");
	//TwAddVarRW(mainMenuBar, "ShowCapsuleRadius", TW_TYPE_BOOLCPP, &controlParams.showCapsuleRadius, "");
	//TwAddVarRW(mainMenuBar, "ShowDistanceField", TW_TYPE_BOOLCPP, &showDistanceField, "");
	//TwAddVarRW(mainMenuBar, "ShowPickedRB", TW_TYPE_BOOLCPP, &showPickedRB, "");

	TwAddSeparator(mainMenuBar, "sep6", "");
	TwAddVarRW(mainMenuBar, "VoxelSize", TW_TYPE_DOUBLE, &transParams.voxelSize, "min=0.0 max=1.0 step=0.001");
	TwAddVarRW(mainMenuBar, "LSStepSize", TW_TYPE_DOUBLE, &transParams.LSStepSize, "min=0.0 max=1.0 step=0.001");
	TwAddVarRW(mainMenuBar, "AnimationStep", TW_TYPE_DOUBLE, &transParams.animationStep, "min=0.0 max=1.0 step=0.01");
	TwAddVarRW(mainMenuBar, "SampleStep", TW_TYPE_DOUBLE, &transParams.sampleStep, "min=0.0 max=1.0 step=0.05");
	TwAddVarRW(mainMenuBar, "JointScale", TW_TYPE_DOUBLE, &transParams.jointScale, "min=0.0 max=0.01 step=0.0001");
	TwAddVarRW(mainMenuBar, "SmoothVoxels", TW_TYPE_BOOLCPP, &transParams.smoothVoxels, "");
	TwAddVarRW(mainMenuBar, "d_pos", TW_TYPE_DOUBLE, &transParams.d_pos, "min=0.0 max=0.1 step=0.001");
	TwAddVarRW(mainMenuBar, "d_angle", TW_TYPE_DOUBLE, &transParams.d_angle, "min=0.0 max=360.0 step=1.0");
	TwAddVarRW(mainMenuBar, "ShellOnly", TW_TYPE_BOOLCPP, &transParams.shellOnly, "");
	TwAddVarRW(mainMenuBar, "SharpFeet", TW_TYPE_BOOLCPP, &transParams.sharpFeet, "");
	TwAddVarRW(mainMenuBar, "IFPSpace", TW_TYPE_DOUBLE, &transParams.IFPSpace, "min=0.0 max=0.02 step=0.001");
	TwAddVarRW(mainMenuBar, "SkeletonRadius", TW_TYPE_DOUBLE, &transParams.skeletonRadius, "min=0.0 max=0.03 step=0.001");
	TwAddVarRW(mainMenuBar, "ShellThickness", TW_TYPE_DOUBLE, &transParams.shellThickness, "min=0.0 max=0.03 step=0.001");
	TwAddVarRW(mainMenuBar, "SktInsideThreshold", TW_TYPE_DOUBLE, &transParams.sktInsideThreshold, "min=0.0 max=0.2 step=0.01");
	TwAddVarRW(mainMenuBar, "MinTransferRadius", TW_TYPE_DOUBLE, &transParams.minTransferR, "min=0.0 max=0.5 step=0.01");

	TwAddVarRW(mainMenuBar, "MaxFloodIter", TW_TYPE_INT16, &transParams.maxFloodIter, "");
	TwAddVarRW(mainMenuBar, "LSCylinderR", TW_TYPE_DOUBLE, &transParams.LSCylinderR, "min=0.0 max=1.0 step=0.001");
	TwAddVarRW(mainMenuBar, "LSFlood_L", TW_TYPE_DOUBLE, &transParams.LSFlood_L, "min=0.0 max=200.0 step=1.0");
	TwAddVarRW(mainMenuBar, "LSFlood_alpha", TW_TYPE_DOUBLE, &transParams.LSFlood_alpha, "min=0.0 max=0.01 step=0.0001");
	TwAddVarRW(mainMenuBar, "LSFlood_v", TW_TYPE_DOUBLE, &transParams.LSFlood_v, "min=0.0 max=0.01 step=0.0001");

	int size[2];
	int w, h;
	TwGetParam(mainMenuBar, NULL, "size", TW_PARAM_INT32, 2, size);
	w = (GLApplication::getMainWindowWidth() - size[0]) / 2;
	h = GLApplication::getMainWindowHeight();
	sideWindow = new TransformablesSideWindow(size[0], 0, w, h, this);
	sideWindow->controlParams = &controlParams;
	sideWindow->transParams = &transParams;

	setViewportParameters(size[0] + w, 0, w, h);
	consoleWindow->setViewportParameters(size[0] + w, 0, w, 280);

	TwAddSeparator(mainMenuBar, "sep8", "");
	TwAddVarRW(mainMenuBar, "Animation_t", TW_TYPE_DOUBLE, &sideWindow->sIndex, "min=0.0 max=1.0 step=0.001");
	TwAddVarRW(mainMenuBar, "PlayAnimation", TW_TYPE_BOOLCPP, &sideWindow->playAnimation, "");

	modularDesign = new ModularDesignWindow(size[0], 0, w, h, this, "../data/modularRobot/configXM-430-V3.cfg");

	loadJointMesh();

	loadConfigMap("../data/transformables/config/config2.txt");

	loadConfig(configMap['0']);

	controlParams.shellMaterial.setShaderProgram(GLContentManager::getShaderProgram("matcap"));
	controlParams.shellMaterial.setTextureParam(matFile.c_str(), GLContentManager::getTexture(matFile.c_str()));

	string sktMatFile = "../data/textures/matcap/MatCap_0008.bmp";
	controlParams.skeletonMaterial.setShaderProgram(GLContentManager::getShaderProgram("matcap"));
	controlParams.skeletonMaterial.setTextureParam(sktMatFile.c_str(), GLContentManager::getTexture(sktMatFile.c_str()));
}

void TransformablesApp::adjustWindowSize(int width, int height) {
	Logger::consolePrint("resize");

	mainWindowWidth = width;
	mainWindowHeight = height;
	// Send the window size to AntTweakBar
	TwWindowSize(width, height);

	setViewportParameters(0, 0, mainWindowWidth, mainWindowHeight);

	int size[2];
	int w, h;
	TwGetParam(mainMenuBar, NULL, "size", TW_PARAM_INT32, 2, size);
	w = (GLApplication::getMainWindowWidth() - size[0]) / 2;
	h = GLApplication::getMainWindowHeight();
	sideWindow->setViewportParameters(size[0], 0, w, h);
	setViewportParameters(size[0] + w, 0, w, h);
}

TransformablesApp::~TransformablesApp(void){

	delete jointPlugMesh;
	delete jointPlugCarveMesh;
	delete jointSocketMesh;
	delete jointSocketCarveMesh;

	delete mesh;
	delete visMesh;

	// Transfrom engine
	delete transformEngine;

	// Robot
	delete IKSolver_v2;
	delete rbEngine;
	delete robot;
	delete foldedState;
	delete unfoldedState;
	delete startState;
	
	// Widgets
	delete rWidget;
	delete tWidget;

	delete sideWindow;
}

//triggered when mouse moves
bool TransformablesApp::onMouseMoveEvent(double xPos, double yPos) {
	if (showMenus)
		if (TwEventMousePosGLFW((int)xPos, (int)yPos)) return true;

	if (sideWindow->isActive() || sideWindow->mouseIsWithinWindow(xPos, yPos))
		if (sideWindow->onMouseMoveEvent(xPos, yPos)) return true;

	if (dragging && pickedRB)
	{
		robot->setState(foldedState);
		Ray ray = getRayFromScreenCoords(xPos, yPos);
		V3D viewPlaneNormal = V3D(camera->getCameraPosition(), camera->getCameraTarget()).unit();
		ray.getDistanceToPlane(Plane(pickedRB->getWorldCoordinates(selectedPoint), viewPlaneNormal), &targetPoint);
		// ray.getDistanceToPoint(pickedRB->getWorldCoordinates(selectedPoint), &targetPoint);
		IKSolver_v2->ikPlan->endEffectors.back().targetEEPos = targetPoint;
		IKSolver_v2->solve(20);
		*foldedState = ReducedRobotState(robot);
		IKSolver_v2->ikPlan->setTargetIKState(*foldedState);
		transformEngine->skeletonOptEngine->reintializeCMA = true;
		if (dragging) editingRobot = true;
		return true;
	}

	if (pickedInterfaceP >= 0)
	{
		moveInterfacePoint(xPos, yPos);
		if (movedDist > 1e-6)
		{
			transformEngine->levelSetEditingNew(pickedInterface);
			movedDist = 0;
		}
		
		return true;
	}

	if (pickedKnot >= 0)
	{
		moveKnot(xPos, yPos);
		if (movedDist > 1e-6)
		{
			auto& indices = transformEngine->meshIFKnots[pickedKnot].interfaceIndices;
			for (uint i = 0; i < indices.size(); i++)
			{
				transformEngine->levelSetEditingNew(indices[i]);
			}
			movedDist = 0;
		}

		return true;
	}

	robot->setState(foldedState);
	if (tWidget->onMouseMoveEvent(xPos, yPos)){
		if (pickedJoint == ROOT_ID){
			foldedState->setPosition(tWidget->pos);
			rWidget->pos = tWidget->pos;
			transformEngine->skeletonOptEngine->reintializeCMA = true;
			if (dragging) editingRobot = true;
			return true;
		}
		return true;
	}

	if (rWidget->onMouseMoveEvent(xPos, yPos) && rWidget->isPicked()) {
		if (pickedJoint >= 0){
			robot->getJoint(pickedJoint)->child->state.orientation = rWidget->getOrientation();
			ReducedRobotState tmpState(robot);
			Quaternion qRel = tmpState.getJointRelativeOrientation(pickedJoint);
			if (controlParams.useHingeJoint)
			{
				V3D axis = qRel.v; axis.toUnit();
				V3D rotationAxis = static_cast<HingeJoint*>(robot->getJoint(pickedJoint))->rotationAxis;
				double rotAngle = qRel.getRotationAngle(axis);
				double ang = axis.dot(rotationAxis) * rotAngle;
				qRel = getRotationQuaternion(ang, rotationAxis);

				auto itr = jointSymmetryMap.find(pickedJoint);
				if (itr != jointSymmetryMap.end()) {
					int symJoint = itr->second;
					V3D symRotationAxis = static_cast<HingeJoint*>(robot->getJoint(symJoint))->rotationAxis;
					foldedState->setJointRelativeOrientation(getRotationQuaternion(-ang, symRotationAxis), symJoint);
				}
			}	
			foldedState->setJointRelativeOrientation(qRel, pickedJoint);
		}
		else if(pickedJoint == ROOT_ID){
			Quaternion rootQ = rWidget->getOrientation();
			V3D axis = rootQ.v; axis.toUnit();
			double rotAngle = rootQ.getRotationAngle(axis);
			double ang = axis.dot(V3D(1, 0, 0)) * rotAngle;
			// foldedState->setOrientation(getRotationQuaternion(ang, V3D(1, 0, 0)));
			foldedState->setOrientation(rootQ);
		}
		transformEngine->skeletonOptEngine->reintializeCMA = true;
		if (dragging) editingRobot = true;

		return true;
	}
	if (GLApplication::onMouseMoveEvent(xPos, yPos) == true) return true;

	return false;
}

bool TransformablesApp::pickLimb(double xPos, double yPos)
{
	Ray mouseRay = camera->getRayFromScreenCoords(xPos, yPos);
	RigidBody* tmpPick = NULL;
	double closestDistance = 0.01;
	P3D closestP;

	int nLegs = robot->bFrame->limbs.size();

	for (int i = 0; i < nLegs; i++) {
		RigidBody* limb = robot->bFrame->limbs[i]->getLastLimbSegment();
		int nEEs = limb->rbProperties.getEndEffectorPointCount();

		for (int j = 0; j < nEEs; j++) {
			P3D pos = limb->getWorldCoordinates(limb->rbProperties.getEndEffectorPoint(j));
			double distance = mouseRay.getDistanceToPoint(pos, NULL);
			if (distance < closestDistance)
			{
				tmpPick = limb;
				closestDistance = distance;
				closestP = pos;
				break;
			}
		}
	}

	pickedLimb = tmpPick;
	if (!tmpPick) {
		return false;
	}
	else {
		Logger::print("limb: %s\n", tmpPick->getName().c_str());
		tWidget->visible = true;
		tWidget->pos = closestP;
		startP = closestP;
		return true;
	}

	return false;
}

bool TransformablesApp::pickInterfacePoint(double xPos, double yPos)
{
	Ray mouseRay = camera->getRayFromScreenCoords(xPos, yPos);
	double minDist = 0.005;

	pickedInterface = -1;
	pickedInterfaceP = -1;

	auto& meshInterfaces = transformEngine->meshInterfaces;
	for (uint i = 0; i < meshInterfaces.size(); i++)
	{
		auto& points = meshInterfaces[i].points;
		for (uint j = 0; j < points.size(); j++)
		{
			double dist = mouseRay.getDistanceToPoint(points[j]);
			if (dist < minDist)
			{
				minDist = dist;
				pickedInterface = i;
				pickedInterfaceP = j;
			}
		}
	}

	sideWindow->pickedInterface = pickedInterface;
	sideWindow->pickedInterfaceP = pickedInterfaceP;
	if (pickedInterfaceP >= 0)
	{	
		movedDist = 0;
		Logger::print("PickedInterface: %d %d\n", pickedInterface, pickedInterfaceP);
		return true;
	}

	return false;
}

bool TransformablesApp::pickKnot(double xPos, double yPos)
{
	Ray mouseRay = camera->getRayFromScreenCoords(xPos, yPos);
	double minDist = 0.005;

	pickedKnot = -1;

	auto& meshIFKnots = transformEngine->meshIFKnots;
	for (uint i = 0; i < meshIFKnots.size(); i++)
	{
		double dist = mouseRay.getDistanceToPoint(meshIFKnots[i].position);
		if (dist < minDist)
		{
			minDist = dist;
			pickedKnot = i;
		}
	}

	if (pickedKnot >= 0)
	{
		movedDist = 0;
		Logger::print("Picked Knot: %d\n", pickedKnot);
		return true;
	}

	return false;
}

void TransformablesApp::moveInterfacePoint(double xPos, double yPos)
{
	MeshInterface& meshIF = transformEngine->meshInterfaces[pickedInterface];
	auto& points = meshIF.points;
	LevelSet& ls = transformEngine->MlevelSets[meshIF.index1];
	Ray mouseRay = camera->getRayFromScreenCoords(xPos, yPos);
	P3D newP;
	startP = points[pickedInterfaceP];
	mouseRay.getDistanceToPoint(startP, &newP);
	newP = transformEngine->projectPointToSurf(newP);
	points[pickedInterfaceP] = newP;
	movedDist += (newP - startP).norm();
	
	double L = ls.interpolateData(newP[0], newP[1], newP[2]);

	for (int i = -5; i < 5; i++)
	{
		if (i == 0) continue;

		int index = pickedInterfaceP + i;
		if (meshIF.loop)
		{
			if (index < 0)
				index += (int)points.size();
			if (index >= (int)points.size())
				index -= (int)points.size();
		}
		else if (index <= 0 || index >= (int)points.size() - 1)
			continue;

		P3D& p = points[index];
		double weight = TransformUtils::GaussianKernel(fabs(i), 5, 1);
		V3D dp = ls.interpolateGradient(p[0], p[1], p[2]).normalized() * (L * weight - ls.interpolateData(p[0], p[1], p[2]));
		p += dp;
		p = transformEngine->projectPointToSurf(p);
	}
}

void TransformablesApp::moveKnot(double xPos, double yPos)
{
	auto& curKnot = transformEngine->meshIFKnots[pickedKnot];
	Ray mouseRay = camera->getRayFromScreenCoords(xPos, yPos);
	P3D newP;
	startP = curKnot.position;
	mouseRay.getDistanceToPoint(startP, &newP);
	newP = transformEngine->projectPointToSurf(newP);
	curKnot.position = newP;
	movedDist += (newP - startP).norm();

	for (uint k = 0; k < curKnot.interfaceIndices.size(); k++)
	{
		auto& curIF = transformEngine->meshInterfaces[curKnot.interfaceIndices[k]];
		int flag = curKnot.interfaceFlags[k];
		LevelSet& ls = transformEngine->MlevelSets[curIF.index1];
		auto& points = curIF.points;
		double L = ls.interpolateData(newP[0], newP[1], newP[2]);

		int index = flag == 0 ? 0 : (int)points.size() - 1;
		points[index] = newP;
		int count = 0;

		while (count < 5)
		{
			count++;
			index += flag == 0 ? 1 : -1;
			if (index <= 0 || index >= (int)points.size() - 1)
				continue;

			P3D& p = points[index];
			double weight = TransformUtils::GaussianKernel(fabs(count), 5, 0.5);
			V3D dp = (newP - startP) * weight;
			p += dp;
			p = transformEngine->projectPointToSurf(p);
		}
	}
}

bool TransformablesApp::pickJoint(double xPos, double yPos)
{
	Ray mouseRay = camera->getRayFromScreenCoords(xPos, yPos);
	int tmpPick = NOJOINT_ID;
	double closestDistance = 0.005;

	P3D rootP = robot->getRoot()->state.position;
	double distance = mouseRay.getDistanceToPoint(rootP);
	if (distance < 0.02)
	{
		pickedJoint = ROOT_ID;
		return true;
	}

	for (int i = 0; i < robot->getJointCount(); i++)
	{
		P3D jPos = robot->getJoint(i)->getWorldPosition();
		distance = mouseRay.getDistanceToPoint(jPos, NULL);
		if (distance < closestDistance)
		{
			tmpPick = i;
			closestDistance = distance;
		}
	}
	
	pickedJoint = tmpPick;
	if (tmpPick == NOJOINT_ID) {
		return false;
	}
	else {
		Logger::consolePrint("joint: %d\n", tmpPick);
		return true;
	}

	return false;
}

void TransformablesApp::pickJointPostProc(int mods)
{
	if (pickedJoint == ROOT_ID)
	{
		tWidget->visible = rWidget->visible = true;
		tWidget->pos = rWidget->pos = robot->getRoot()->state.position;
		rWidget->setOrientation(robot->getRoot()->state.orientation);
	}
	else {
		rWidget->visible = true;
		rWidget->pos = robot->getJoint(pickedJoint)->getWorldPosition();
		rWidget->setOrientation(robot->getJoint(pickedJoint)->child->getOrientation());
	}

	if (mods == GLFW_MOD_ALT)
	{
		handleFoldingOrderSwap();
	}
}

bool TransformablesApp::pickRB(double xPos, double yPos)
{
	Ray mouseRay = camera->getRayFromScreenCoords(xPos, yPos);
	RigidBody* tmpPick = NULL;
	P3D pLocal;
	double tMin = DBL_MAX;
	for (int i = 0; i < robot->getRigidBodyCount(); i++)
		if (robot->getRigidBody(i)->getRayIntersectionPointTo(mouseRay, &pLocal)) {
			double tVal = mouseRay.getRayParameterFor(robot->getRigidBody(i)->getWorldCoordinates(pLocal));
			if (tVal < tMin) {
				tMin = tVal;
				tmpPick = robot->getRigidBody(i);
			}
		}

	pickedRB = tmpPick;
	sideWindow->pickedRB = pickedRB;
	if (pickedRB == NULL) {
		return false;
	}
	else {
		pickedRB->getRayIntersectionPointTo(mouseRay, &selectedPoint);
		V3D viewPlaneNormal = V3D(camera->getCameraPosition(), camera->getCameraTarget()).unit();
		mouseRay.getDistanceToPlane(Plane(pickedRB->getWorldCoordinates(selectedPoint), viewPlaneNormal), &targetPoint);
		// mouseRay.getDistanceToPoint(pickedRB->getWorldCoordinates(selectedPoint), &targetPoint);
		auto& endEffectors = IKSolver_v2->ikPlan->endEffectors;
		endEffectors.clear();
		endEffectors.push_back(IK_EndEffector());
		endEffectors.back().endEffectorLocalCoords = selectedPoint;
		endEffectors.back().endEffectorRB = pickedRB;
		endEffectors.back().targetEEPos = targetPoint;
		IKSolver_v2->ikPlan->updateRobotRepresentation();
		IKSolver_v2->ikPlan->setTargetIKState(*foldedState);
		//IKSolver_v2->ikOptimizer->checkDerivatives = true;
		string locked = transformEngine->fixedLS[transformEngine->RBIndexMap[pickedRB]] ? "locked" : "unlocked";
		Logger::consolePrint("RB: %s, %s", pickedRB->getName().c_str(), locked.c_str());
		return true;
	}
}

//triggered when mouse buttons are pressed
bool TransformablesApp::onMouseButtonEvent(int button, int action, int mods, double xPos, double yPos) {

	if(sideWindow->isActive() || sideWindow->mouseIsWithinWindow(xPos, yPos))
		if (sideWindow->onMouseButtonEvent(button, action, mods, xPos, yPos)) {
			pickedRB = sideWindow->pickedRB;
			return true;
		}

	if (button == 0) {
		if (action == 1){

			dragging = true;

			if (robot)
				robot->setState(foldedState);

			/*if ((mods == GLFW_MOD_SHIFT || mods == GLFW_MOD_CONTROL) && transformEngine->robotMeshes.size() > 0) {
				createROIPoint(xPos, yPos, mods);
				return true;
			}*/

			bool res = rWidget->onMouseMoveEvent(xPos, yPos);
			if (res) return true;

			if (robot)
			{

				res = tWidget->onMouseMoveEvent(xPos, yPos);

				if (res) return true;

				pickedLimb = NULL;
				pickedJoint = NOJOINT_ID;
				pickedRB = NULL;
				tWidget->visible = rWidget->visible = false;

				bool res = pickKnot(xPos, yPos);
				if (res) return true;

				res = pickInterfacePoint(xPos, yPos);
				if (res) return true;

				res = pickJoint(xPos, yPos);
				
				if (res) {
					pickJointPostProc(mods);
					return true;
				}

				//res = pickLimb(xPos, yPos);
				//if (res) return true;			

				res = pickRB(xPos, yPos);
				if (res) return true;
			}
			
		}
		else {
			dragging = false;
			editingRobot = false;

			if (pickedKnot >= 0)
			{
				/*auto& indices = transformEngine->meshIFKnots[pickedKnot].interfaceIndices;
				for (uint i = 0; i < indices.size(); i++)
				{
					transformEngine->recalculateInterface(indices[i]);
				}*/
				transformEngine->buildMeshInterfaces();
				pickedKnot = -1;
			}

			if (pickedInterface >= 0)
			{
				int jIndex = getJointIndexForInterface(pickedInterface);
				if (jIndex >= 0) {
					for (int i = 0; i < 10; i++)
						transformEngine->levelSetFlooding();
					// get transfer direction
					transformEngine->fabricationEngine->getTransferDirection();
					int transDir = transformEngine->fabricationEngine->transDir[jIndex];
					transformEngine->transferMeshLSForJoint(jIndex, 100, transDir == 1);
					for (int i = 0; i < 30; i++)
						transformEngine->levelSetFlooding();
					MeshInterface& meshIF = transformEngine->meshInterfaces[pickedInterface];
					robot->setState(foldedState);
					transformEngine->extractMeshesFromLSForRB(meshIF.index1);
					transformEngine->extractMeshesFromLSForRB(meshIF.index2);
				}				

				transformEngine->buildMeshInterfaces();
				pickedInterface = -1;
				pickedInterfaceP = -1;
				sideWindow->pickedInterface = -1;
				sideWindow->pickedInterfaceP = -1;
			}
		}
	}
	
	
	if (GLApplication::onMouseButtonEvent(button, action, mods, xPos, yPos)) return true;
	return false;
}

//triggered when using the mouse wheel
bool TransformablesApp::onMouseWheelScrollEvent(double xOffset, double yOffset) {
	if(sideWindow->isActive())
		if (sideWindow->onMouseWheelScrollEvent(xOffset, yOffset)) return true;

	if ((GetAsyncKeyState(VK_LSHIFT) < 0 || GetAsyncKeyState(VK_LCONTROL) < 0) && pickedRB)
	{
		robot->setState(foldedState);
		bool changeBodyLength = GetAsyncKeyState(VK_LCONTROL) < 0;
		transformEngine->robotEditor->scalePickedRobotSym(pickedRB, (1 + yOffset / 20.0), changeBodyLength);			
		if (curConfig.designName.empty())
			transformEngine->fabricationEngine->generateCylinderSkeletonMeshes();
		transformEngine->skeletonOptEngine->reintializeCMA = true;
		return true;
	}


	if (GLApplication::onMouseWheelScrollEvent(xOffset, yOffset)) return true;

	return false;
}

bool TransformablesApp::onKeyEvent(int key, int action, int mods) {

	if (mesh && key == GLFW_KEY_UP && action == GLFW_PRESS) {
		mesh->scale(1.1, P3D());
		visMesh->scale(1.1, P3D());
		meshScale *= 1.1;
		Logger::consolePrint("mesh scale: %lf\n", meshScale);
		showMesh = true;
		createTransformEngine();
		transformEngine->foldingOrder = curConfig.foldingOrder;
	}
		
	if (mesh && key == GLFW_KEY_DOWN && action == GLFW_PRESS) {
		mesh->scale(1/1.1, P3D());
		visMesh->scale(1/1.1, P3D());
		meshScale /= 1.1;
		Logger::consolePrint("mesh scale: %lf\n", meshScale);
		showMesh = true;
		createTransformEngine();
		transformEngine->foldingOrder = curConfig.foldingOrder;
	}
		
	
	if (robot && key == GLFW_KEY_LEFT && action == GLFW_PRESS) {
		for (auto& p : transformEngine->LSIFs[0].points)
		{
			p[1] -= 0.001;
		}
	}
		
	if (robot && key == GLFW_KEY_RIGHT && action == GLFW_PRESS){
		for (auto& p : transformEngine->LSIFs[0].points)
		{
			p[1] += 0.001;
		}
	}

	if (key >= GLFW_KEY_0 && key <= GLFW_KEY_9 && action == GLFW_PRESS)
	{
		if (configMap.count(key))
		{
			TransConfig& tmpConfig = configMap[key];
			loadConfig(tmpConfig);
		}
	}

	if (key == GLFW_KEY_Q && action == GLFW_PRESS)
	{
		if (runOption == SKELETON_CMA_OPT) {
			runOption = SKELETON_OPT;
		}
		else if (runOption == SKELETON_OPT) {
			runOption = SKELETON_CONT_OPT;
		}
		else {
			runOption = SKELETON_CMA_OPT;
		}
	}

	if (key == GLFW_KEY_W && action == GLFW_PRESS)
	{
		runOption = FOLD_ORDER_OPT;
		transformEngine->foldingOptEngine->prepareForFoldingOrderOpt();
	}

	if (key == GLFW_KEY_E && action == GLFW_PRESS)
	{
		runOption = LS_FLOOD;
	}

	if (key == GLFW_KEY_R && action == GLFW_PRESS)
	{
		runOption = CARVE_FLOOD;
	}

	if (key == GLFW_KEY_A && action == GLFW_PRESS)
	{
		transformEngine->prepareForFlooding();
		transformEngine->extractMeshesFromLS();
		runOption = LS_FLOOD;
		showMesh = false;
		controlParams.showRobotMeshes = true;
	}

	if (key == GLFW_KEY_S && action == GLFW_PRESS)
	{
		if (pickedJoint >= 0){
			transformEngine->transferMeshLSForJoint(pickedJoint, 100, (GLFW_MOD_CONTROL & mods) > 0);
			if (jointSymmetryMap.count(pickedJoint))
				transformEngine->transferMeshLSForJoint(jointSymmetryMap[pickedJoint], 100, (GLFW_MOD_CONTROL & mods) > 0);
		}
		else {
			transformEngine->transferMeshLS(100, (GLFW_MOD_CONTROL & mods) > 0);
		}
		transformEngine->extractMeshesFromLS();
	}

	if (key == GLFW_KEY_D && action == GLFW_PRESS)
	{
		transformEngine->generateTransMeshes();
		splitRobotMeshVertices();
	}
	
	if (key == GLFW_KEY_F && action == GLFW_PRESS)
	{
		foldAnimation();
	}

	if (key == GLFW_KEY_G && action == GLFW_PRESS)
	{
		if (pickedRB) {
			transformEngine->fixedLS[transformEngine->RBIndexMap[pickedRB]]
				= !transformEngine->fixedLS[transformEngine->RBIndexMap[pickedRB]];
			string locked = transformEngine->fixedLS[transformEngine->RBIndexMap[pickedRB]] ? "locked" : "unlocked";
			Logger::consolePrint("RB: %s, %s", pickedRB->getName().c_str(), locked.c_str());
		}
	}

	if (key == GLFW_KEY_H && action == GLFW_PRESS)
	{
		transformEngine->buildMeshInterfaces();
	}

	if (key == GLFW_KEY_J && action == GLFW_PRESS)
	{
		transformEngine->fabricationEngine->carveLS(1);
		transformEngine->extractMeshesFromLS();
	}

	if (key == GLFW_KEY_K && action == GLFW_PRESS)
	{
		transformEngine->fabricationEngine->carveLS(2);
		transformEngine->extractMeshesFromLS();
	}

	if (key == GLFW_KEY_L && action == GLFW_PRESS)
	{
		if (transParams.useLivingBracket)
		{
			transformEngine->attachJointStructuresForLivingBracket();
		}
		else if (transParams.shellOnly) {
			transformEngine->fabricationEngine->generateSocketStructureForRB();
		}
		else {
			transformEngine->attachJointStructures();
		}
		
	}

	if (key == GLFW_KEY_P && action == GLFW_PRESS)
	{
		
	}

	if (key == GLFW_KEY_U && action == GLFW_PRESS)
	{
		// exportFoldingJointAngles((mods & GLFW_MOD_CONTROL) == 0);
		modularDesign->saveDesignToFile("../out/tmpTransDesign.dsn");
	}

	if (key == GLFW_KEY_I && action == GLFW_PRESS)
	{
		// carveMesh();
		if (curConfig.useLivingBracket)
			transformEngine->fabricationEngine->carveLSForMeshesAndSkeletonsForLivingBracket();
		else {
			// transformEngine->fabricationEngine->generateCylinderSkeletonMeshes();
			transformEngine->fabricationEngine->generateSkeletonLevelSets();
		}
			
	}

	if (key == GLFW_KEY_O && action == GLFW_PRESS)
	{
		string path = curConfig.meshFolderName.empty() ? "../out/transformableMeshes/" : curConfig.meshFolderName;
		if (transParams.shellOnly)
		{
			if ((GLFW_MOD_CONTROL & mods) == 0)
				transformEngine->fabricationEngine->mergeAndSaveSkeletonMeshForRB(path.c_str());
			saveRobotMeshes(path);
		}
		else {
			saveRobotMeshes(path, false);
		}
		
	}

	if (key == GLFW_KEY_B && action == GLFW_PRESS)
	{
		if (transParams.shellOnly)
		{
			transformEngine->fabricationEngine->mergeAndSaveSkeletonMeshForRB("../out/transformableMeshes/");
			saveRobotMeshes("../out/transformableMeshes/");
		}
		else {
			saveRobotMeshes("../out/transformableMeshes/", false);
		}
	}

	if (key == GLFW_KEY_N && action == GLFW_PRESS)
	{
		if (curConfig.foldName.empty())
			*foldedState = *startState;
		else
			foldedState->readFromFile(curConfig.foldName.c_str());
	}

	if (key == GLFW_KEY_Z && action == GLFW_PRESS)
	{
		if (pickedRB)
		{
			transformEngine->clearRBLevelSet(pickedRB);
			runOption = LS_FLOOD;
		}
	}

	if (key == GLFW_KEY_X && action == GLFW_PRESS)
	{
		//assignMesh();
		if (motionPlanStates.size() > 0)
			sideWindow->prepareForMotionPlanAnimation(motionPlanStates);
	}

	if (key == GLFW_KEY_T && action == GLFW_PRESS)
	{
		loadRobotMeshes();
	}

	if (key == GLFW_KEY_V && action == GLFW_PRESS)
	{
		saveConfig();
	}

	if (key == GLFW_KEY_F1 && action == GLFW_PRESS)
	{
		transformEngine->buildLSInterfaces();
		runOption = IF_EVOLVE;
	}

	if (key == GLFW_KEY_F2 && action == GLFW_PRESS)
	{
		transformEngine->buildLSInterfaces();
		runOption = IF_EVOLVE;
		transformEngine->calEvolveVelocityField();
		transformEngine->evolveSamplePoints();
	}

	if (key == GLFW_KEY_F3 && action == GLFW_PRESS)
	{
		transformEngine->foldingOptEngine->prepareForFoldingOrderOpt();
		transformEngine->foldingOptEngine->optimizeFoldingOrderGreedy();
	}

	if (key == GLFW_KEY_F5 && action == GLFW_PRESS)
	{
		transformEngine->meshInterfaces.clear();
		transformEngine->meshIFKnots.clear();
	}

	if (key == GLFW_KEY_F6 && action == GLFW_PRESS)
	{
		transformEngine->loadRBOrigMeshesFromDesign(true);
	}

	if (GLApplication::onKeyEvent(key, action, mods)) return true;
	return false;
}

bool TransformablesApp::onCharacterPressedEvent(int key, int mods) {
	if (GLApplication::onCharacterPressedEvent(key, mods)) return true;

	return false;
}

void TransformablesApp::loadJointMesh() {

	jointPlugMesh = GLContentManager::getGLMesh("../data/transformables/jointMeshes/HingeJointPlug.obj");
	jointPlugCarveMesh = GLContentManager::getGLMesh("../data/transformables/jointMeshes/HingeJointPlugCarve.obj");
	jointSocketMesh = GLContentManager::getGLMesh("../data/transformables/jointMeshes/HingeJointSocket.obj");
	jointSocketCarveMesh = GLContentManager::getGLMesh("../data/transformables/jointMeshes/HingeJointSocketCarve.obj");

	jointPlugMesh->scale(0.001, P3D());
	jointPlugCarveMesh->scale(0.001, P3D());
	jointSocketMesh->scale(0.001, P3D());
	jointSocketCarveMesh->scale(0.001, P3D());

	jointPlugMesh->computeNormals();
	jointPlugCarveMesh->computeNormals();
	jointSocketMesh->computeNormals();
	jointSocketCarveMesh->computeNormals();

	jointPlugMesh->getMaterial().setColor(0.0, 1.0, 1.0, 1.0);
	jointPlugCarveMesh->getMaterial().setColor(0.0, 1.0, 1.0, 1.0);
	jointSocketMesh->getMaterial().setColor(1.0, 1.0, 0.0, 1.0);
	jointSocketCarveMesh->getMaterial().setColor(1.0, 1.0, 0.0, 1.0);
}

void TransformablesApp::loadMeshFromFile(const char* fName, double scale) {

	delete mesh;
	delete visMesh;
	mesh = OBJReader::loadOBJFile(fName);
	AxisAlignedBoundingBox bbox = mesh->getBoundingBox();
	double initScale = 1.0 / bbox.diameter();
	double objScale = initScale * scale;
	mesh->translate(-(bbox.center()));
	mesh->scale(initScale, P3D());
	mesh->scale(scale, P3D());
	mesh->computeNormals();
	mesh->calBoundingBox();

	visMesh = mesh->getSplitVerticeMesh();
	meshScale = scale;
	Logger::consolePrint("mesh scale: %lf, obj total scale: %lf\n", meshScale, objScale);
}

void TransformablesApp::loadRobotFromFile(const char* fName, const char* mpName) {

	delete transformEngine;
	delete IKSolver_v2;
	delete rbEngine;
	delete robot;
	delete foldedState;
	delete unfoldedState;
	delete startState;

	transformEngine = NULL;
	motionPlanStates.clear();

	rbEngine = new ODERBEngine();
	rbEngine->loadRBsFromFile(fName);

	robot = new Robot(rbEngine->rbs[0]);
	setupSimpleRobotStructure(robot);

	foldedState = new ReducedRobotState(robot);
	unfoldedState = new ReducedRobotState(robot);
	startState = new ReducedRobotState(robot);

	if (mpName)
	{
		LocomotionEngineMotionPlan* motionPlan = new LocomotionEngineMotionPlan(robot, 10);
		motionPlan->readParamsFromFile(mpName);
		for (double f = 0; f <= 1.0; f += 0.01)
		{
			motionPlanStates.push_back(ReducedRobotState(robot));
			motionPlan->robotStateTrajectory.getRobotPoseAt(f, motionPlanStates.back());
		}
		delete motionPlan;

		*unfoldedState = motionPlanStates[0];
	}

	getRBColorMap();
	buildJointSymmetryMap();

	IKSolver_v2 = new IK_Solver(robot, &jointSymmetryMap);
	IKSolver_v2->ikEnergyFunction->regularizer = 100;

	createTransformEngine();

	tWidget->pos = robot->getRoot()->state.position;

	sideWindow->robot = robot;
	sideWindow->foldedState = foldedState;
	sideWindow->unfoldedState = unfoldedState;
	sideWindow->tWidget->pos = tWidget->pos;

	pickedJoint = NOJOINT_ID;
	pickedRB = NULL;
}

void TransformablesApp::createTransformEngine()
{
	Logger::print("create transform engine.\n");
	delete transformEngine;
	transformEngine = new TransformEngine(robot, foldedState, unfoldedState, &transParams, jointSymmetryMap, motionPlanStates);
	controlParams.transformEngine = transformEngine;

	transformEngine->startState = startState;
	transformEngine->jointPlugMesh = jointPlugMesh;
	transformEngine->jointPlugCarveMesh = jointPlugCarveMesh;
	transformEngine->jointSocketMesh = jointSocketMesh;
	transformEngine->jointSocketCarveMesh = jointSocketCarveMesh;
	transformEngine->mesh = mesh;
	transformEngine->motionPlanStates = motionPlanStates;
	transformEngine->modularDesign = modularDesign;

	// load meshes
	if (curConfig.useLivingBracket)
		transformEngine->loadRBOrigMeshesFromDesign(false, true);
	else
		transformEngine->loadRBOrigMeshes();

	// initialize robot editor to adjust joint meshes
	transformEngine->robotEditor->getNearestJoints();

	if (curConfig.initFileName != "")
		transformEngine->loadInitialization(curConfig.initFileName.c_str());
	if (curConfig.designName.empty())
		transformEngine->fabricationEngine->generateCylinderSkeletonMeshes();

	runOption = SKELETON_CMA_OPT;
}

void TransformablesApp::loadConfigMap(const char* fName)
{
	FILE* fp = fopen(fName, "r");

	char buffer[200];
	char keyword[50];
	TransConfig* curConfig = NULL;

	while (!feof(fp)) {
		//get a line from the file...
		readValidLine(buffer, fp, 200);
		if (strlen(buffer) > 195)
			throwError("The input file contains a line that is longer than ~200 characters - not allowed");
		char *line = lTrim(buffer);
		if (strlen(line) == 0) continue;
		sscanf(line, "%s", keyword);

		if (strcmp(keyword, "Key") == 0)
		{
			char key[10];
			int num = sscanf(line + strlen(keyword), "%s", key);
			configMap[key[0]] = TransConfig();
			curConfig = &(configMap[key[0]]);
		}
		else if (strcmp(keyword, "Volume") == 0)
		{
			curConfig->useVolume = true;
		}
		else if (strcmp(keyword, "UseXM430") == 0)
		{
			curConfig->useXM430 = true;
		}
		else if (strcmp(keyword, "UseLivingBracket") == 0)
		{
			curConfig->useLivingBracket = true;
		}
		else if (strcmp(keyword, "Mesh") == 0)
		{
			char content[200];
			int num = sscanf(line + strlen(keyword), "%s", content);
			curConfig->meshName = content;
		}
		else if (strcmp(keyword, "Skeleton") == 0)
		{
			char content[200];
			int num = sscanf(line + strlen(keyword), "%s", content);
			curConfig->skeletonName = content;
		}
		else if (strcmp(keyword, "Fold") == 0)
		{
			char content[200];
			int num = sscanf(line + strlen(keyword), "%s", content);
			curConfig->foldName = content;
		}
		else if (strcmp(keyword, "Unfold") == 0)
		{
			char content[200];
			int num = sscanf(line + strlen(keyword), "%s", content);
			curConfig->unfoldName = content;
		}
		else if (strcmp(keyword, "Motion") == 0)
		{
			char content[200];
			int num = sscanf(line + strlen(keyword), "%s", content);
			curConfig->motionPlanName = content;
		}
		else if (strcmp(keyword, "LevelSets") == 0)
		{
			char content[200];
			int num = sscanf(line + strlen(keyword), "%s", content);
			curConfig->levelSetsName = content;
		}
		else if (strcmp(keyword, "RDParams") == 0)
		{
			char content[200];
			int num = sscanf(line + strlen(keyword), "%s", content);
			curConfig->RDParamsName = content;
		}
		else if (strcmp(keyword, "MeshFolder") == 0)
		{
			char content[200];
			int num = sscanf(line + strlen(keyword), "%s", content);
			curConfig->meshFolderName = content;
		}
		else if (strcmp(keyword, "InitFileName") == 0)
		{
			char content[200];
			int num = sscanf(line + strlen(keyword), "%s", content);
			curConfig->initFileName = content;
		}
		else if (strcmp(keyword, "Design") == 0)
		{
			char content[200];
			int num = sscanf(line + strlen(keyword), "%s", content);
			curConfig->designName = content;
		}
		else if (strcmp(keyword, "Scale") == 0)
		{
			double scale;
			int num = sscanf(line + strlen(keyword), "%lf", &scale);
			curConfig->scale = scale;
		}
		else if (strcmp(keyword, "Scale3D") == 0)
		{
			V3D scale3D;
			int num = sscanf(line + strlen(keyword), "%lf %lf %lf", &scale3D[0], &scale3D[1], &scale3D[2]);
			curConfig->scale3D = scale3D;
		}
		else if (strcmp(keyword, "FoldingOrder") == 0)
		{
			string str = line + strlen(keyword);
			int index = str.find_first_not_of(' ');
			bool stop = false;

			while (!stop && index != string::npos)
			{
				int jointIndex;
				int sIndex = str.find_first_of(' ', index);
				if (sIndex == string::npos) {
					jointIndex = atoi(str.substr(index).c_str());
					stop = true;
				}
				else {
					jointIndex = atoi(str.substr(index, sIndex - index).c_str());
				}
				curConfig->foldingOrder.push_back(jointIndex);

				index = str.find_first_not_of(' ', sIndex);
			}
		}
		else if (strcmp(keyword, "FlipJoints") == 0)
		{
			string str = line + strlen(keyword);
			int index = str.find_first_not_of(' ');
			bool stop = false;

			while (!stop && index != string::npos)
			{
				int jointIndex;
				int sIndex = str.find_first_of(' ', index);
				if (sIndex == string::npos) {
					jointIndex = atoi(str.substr(index).c_str());
					stop = true;
				}
				else {
					jointIndex = atoi(str.substr(index, sIndex - index).c_str());
				}
				curConfig->flipJointSet.insert(jointIndex);

				index = str.find_first_not_of(' ', sIndex);
			}
		}
		else if (strcmp(keyword, "MatFile") == 0)
		{
			char content[200];
			int num = sscanf(line + strlen(keyword), "%s", content);
			matFile = content;
		}
		else if (strcmp(keyword, "LSStepSize") == 0)
		{
			double stepSize;
			int num = sscanf(line + strlen(keyword), "%lf", &stepSize);
			curConfig->LSStepSize = stepSize;
		}
	}

	fclose(fp);
}

void TransformablesApp::loadConfig(TransConfig& transConfig)
{
	showMesh = true;
	curConfig = transConfig;

	// TransParams
	transParams.shellOnly = !curConfig.useVolume;
	transParams.LSStepSize = curConfig.LSStepSize > 0 ? curConfig.LSStepSize : 0.01;
	transParams.useLivingBracket = curConfig.useLivingBracket;

	// Mesh
	loadMeshFromFile(transConfig.meshName.c_str(), transConfig.scale);
	if (!transConfig.scale3D.isZero()) {
		mesh->scale(transConfig.scale3D, P3D());
		visMesh->scale(transConfig.scale3D, P3D());
	}	

	// Robot
	if (transConfig.designName != "")
	{
		modularDesign->loadDesignFromFile(transConfig.designName.c_str());
		modularDesign->saveToRBSFile("../out/tmpRobot.rbs");
		loadRobotFromFile("../out/tmpRobot.rbs", transConfig.motionPlanName.empty() ? NULL : transConfig.motionPlanName.c_str());
	}
	else {
		loadRobotFromFile(transConfig.skeletonName.c_str(), transConfig.motionPlanName.empty() ? NULL : transConfig.motionPlanName.c_str());
	}	

	// Robot State
	if (transConfig.foldName != "")
		foldedState->readFromFile(transConfig.foldName.c_str());
	if (transConfig.unfoldName != "")
		unfoldedState->readFromFile(transConfig.unfoldName.c_str());

	// Extra
	if (transConfig.RDParamsName != "")
		loadFile(transConfig.RDParamsName.c_str());
	if (transConfig.levelSetsName != "")
		loadFile(transConfig.levelSetsName.c_str());

	transformEngine->foldingOrder = transConfig.foldingOrder;
	
	IKSolver_v2->ikPlan->setTargetIKState(*foldedState);
	rWidget->pos = tWidget->pos = foldedState->getPosition();
	sideWindow->rWidget->pos = sideWindow->tWidget->pos = unfoldedState->getPosition();
	sideWindow->resetAnimation();
}

void TransformablesApp::saveConfig()
{
	if (curConfig.foldName != "")
		foldedState->writeToFile(curConfig.foldName.c_str());
	if (curConfig.unfoldName != "")
		unfoldedState->writeToFile(curConfig.unfoldName.c_str());
	if (curConfig.levelSetsName != "")
		transformEngine->saveLSToFile(curConfig.levelSetsName.c_str());
	if (curConfig.RDParamsName != "")
		transformEngine->robotEditor->saveParamsToFile(curConfig.RDParamsName.c_str());
}

void TransformablesApp::exportFoldingJointAngles(bool fold)
{
	double sampleStep = 0.5;
	vector<ReducedRobotState> animationStates;
	vector<int> flipFlag(robot->getJointCount(), 1);
	string fName;

	for (auto jIndex : curConfig.flipJointSet)
		flipFlag[jIndex] = -1;

	if (fold) {
		TransformUtils::getAnimationStatesForMotor(animationStates, transformEngine->foldingOrder, unfoldedState, foldedState, sampleStep);
		fName = "../out/tmpFoldJointAngles.mpa";
	}	
	else {
		vector<int> unfoldOrder = transformEngine->foldingOrder;
		reverse(unfoldOrder.begin(), unfoldOrder.end());
		TransformUtils::getAnimationStatesForMotor(animationStates, unfoldOrder, foldedState, unfoldedState, sampleStep);
		fName = "../out/tmpUnfoldJointAngles.mpa";
	}

	FILE* fp = fopen(fName.c_str(), "w+");
	fprintf(fp, "StateNum: %d, MotorNum: %d\n", animationStates.size(), robot->getJointCount());
	fprintf(fp, "{\n");
	//every line in the file will correspond to all joint angles of one state
	for (int i = 0; i < (int)animationStates.size(); i++) {
		fprintf(fp, "{");
		dVector jointAngles = TransformUtils::getJointAnglesForCurrentState(robot, &animationStates[i]);
		for (int j = 0; j < jointAngles.size(); j++) {
			fprintf(fp, "%lf", DEG(jointAngles[j]) * flipFlag[j]);
			if (j < jointAngles.size() - 1)
				fprintf(fp, ", ");
		}
		fprintf(fp, "},\n");
	}
	fprintf(fp, "},\n");

	fclose(fp);
	
}

void TransformablesApp::splitRobotMeshVertices()
{
	for (auto& itr : transformEngine->robotMeshes)
	{
		GLMesh* oldMesh = itr.second;
		GLMesh* newMesh = oldMesh->getSplitVerticeMesh();
		itr.second = newMesh;
		itr.first->meshes[0] = newMesh;
		delete oldMesh;
	}
}

int TransformablesApp::getJointIndexForInterface(int interfaceIndex)
{
	MeshInterface& curIF = transformEngine->meshInterfaces[interfaceIndex];
	RigidBody* rb1 = transformEngine->RBs[curIF.index1];
	RigidBody* rb2 = transformEngine->RBs[curIF.index2];

	for (int i = 0; i < robot->getJointCount(); i++)
	{
		Joint* joint = robot->getJoint(i);
		if ((joint->parent == rb1 && joint->child == rb2) || (joint->parent == rb2 && joint->child == rb1))
		{
			return i;
		}
	}

	return -1;
}

void TransformablesApp::loadFile(const char* fName) {
	Logger::consolePrint("Loading file \'%s\'...\n", fName);
	
	std::string fileName;
	fileName.assign(fName);

	std::string fNameExt = fileName.substr(fileName.find_last_of('.') + 1);

	if (fNameExt.compare("rbs") == 0) {
		loadRobotFromFile(fName);
		return;
	}

	if (fNameExt.compare("obj") == 0) {
		loadMeshFromFile(fName, pow(0.9, 5));
		createTransformEngine();
		return;
	}

	if (fNameExt.compare("ls") == 0) {
		transformEngine->loadLSFromFile(fName, false);
		showMesh = false;
		runOption = LS_FLOOD;
		return;
	}

	if (fNameExt.compare("lss") == 0) {
		transformEngine->loadLSFromFile(fName, true);
		showMesh = false;
		runOption = LS_FLOOD;
		return;
	}
	
	if (fNameExt.compare("rdp") == 0) {
		transformEngine->robotEditor->loadParamsFromFile(fName);
		if (curConfig.useLivingBracket)
			transformEngine->loadRBOrigMeshesFromDesign(false);
		else
			transformEngine->fabricationEngine->generateCylinderSkeletonMeshes();
		return;
	}

	if (fNameExt.compare("rs") == 0) {
		foldedState->readFromFile(fName);
		return;
	}
}

void TransformablesApp::saveFile(const char* fName) {

	Logger::consolePrint("SAVE FILE: Do not know what to do with file \'%s\'\n", fName);
}

// Run the App tasks
void TransformablesApp::process() {
	//do the work here...
	if (runOption == SKELETON_OPT || runOption == SKELETON_CONT_OPT
		|| runOption == SKELETON_CMA_OPT) {
		optimizeRobotState();
	}
	else if (runOption == FOLD_ORDER_OPT) {
		optimizeFoldingOrder();
	}
	else if (runOption == LS_FLOOD) {
		transParams.LSFlood_L = 50;
		transformEngine->levelSetFlooding();
		
		if (showTransferedMesh)
		{
			vector<LevelSet> oldLevelSets = transformEngine->MlevelSets;
			double* maxRs = new double[robot->getJointCount()];
			vector<vector<RBState>> parentStates(robot->getJointCount());
			vector<vector<RBState>> childStates(robot->getJointCount());
			for (int i = 0; i < robot->getJointCount(); i++)
				TransformUtils::getParentAndChildStatesForJoint(parentStates[i], childStates[i], robot, i, foldedState, unfoldedState, 0.2);

#pragma omp parallel for
			for (int i = 0; i < robot->getJointCount(); i++)
				maxRs[i] = transformEngine->getTransferMaxRForJoint(i, parentStates[i], childStates[i], 100, true);

			for (int i = 0; i < robot->getJointCount(); i++)
				transformEngine->execTransferMeshForJoint(i, maxRs[i], true);
			transformEngine->extractMeshesFromLS();
			transformEngine->MlevelSets = oldLevelSets;
		}
		else {
			transformEngine->extractMeshesFromLS();
		}
		
	}
	else if (runOption == CARVE_FLOOD) {
		transParams.LSFlood_L = 80;
		transformEngine->fabricationEngine->carveLS(2);
		for (int i = 0; i < 20; i++)
			transformEngine->levelSetFlooding();
		transformEngine->extractMeshesFromLS();
	}
	else if (runOption == IF_EVOLVE) {
		transformEngine->buildLSInterfaces();
		transformEngine->calEvolveVelocityField();
		transformEngine->evolveSamplePoints();
		for (int i = 0; i < 5; i++)
		{
			transformEngine->evolveLSIFs();
		}
		transformEngine->levelSetFlooding();
		transformEngine->extractMeshesFromLS();
	}
	
}

void TransformablesApp::setupLights() {

	GLfloat bright[] = { 0.6f, 0.6f, 0.6f, 1.0f };
	GLfloat mediumbright[] = { 0.3f, 0.3f, 0.3f, 1.0f };

	glLightfv(GL_LIGHT0, GL_DIFFUSE, bright);
	glLightfv(GL_LIGHT1, GL_DIFFUSE, bright);
	glLightfv(GL_LIGHT2, GL_DIFFUSE, bright);
	glLightfv(GL_LIGHT3, GL_DIFFUSE, bright);
	glLightfv(GL_LIGHT4, GL_DIFFUSE, bright);
	glLightfv(GL_LIGHT5, GL_DIFFUSE, bright);


	GLfloat light0_position[] = { 0.0f, 10000.0f, 0.0f, 0.0f };
	GLfloat light0_direction[] = { 0.0f, -10000.0f, -0.0f, 0.0f };

	GLfloat light1_position[] = { 10000.0f, 0.0f, 0.0f, 0.0f };
	GLfloat light1_direction[] = { -10000.0f, 0.0f, 0.0f, 0.0f };

	GLfloat light2_position[] = { -10000.0f, 0.0f, 0.0f, 0.0f };
	GLfloat light2_direction[] = { 10000.0f, 0.0f, 0.0f, 0.0f };

	GLfloat light3_position[] = { 0.0f, 0.0f, -10000.0f,  0.0f };
	GLfloat light3_direction[] = { 0.0f, 0.0f, 10000.0f,  0.0f };

	GLfloat light4_position[] = { 0.0f, 0.0f, 10000.0f,  0.0f };
	GLfloat light4_direction[] = { 0.0f, 0.0f, -10000.0f,  0.0f };

	GLfloat light5_position[] = { 0.0f, -10000.0f, 0.0f, 0.0f };
	GLfloat light5_direction[] = { 0.0f, 10000.0f, -0.0f, 0.0f };


	glLightfv(GL_LIGHT0, GL_POSITION, light0_position);
	glLightfv(GL_LIGHT1, GL_POSITION, light1_position);
	glLightfv(GL_LIGHT2, GL_POSITION, light2_position);
	glLightfv(GL_LIGHT3, GL_POSITION, light3_position);
	glLightfv(GL_LIGHT4, GL_POSITION, light4_position);
	glLightfv(GL_LIGHT5, GL_POSITION, light5_position);


	glLightfv(GL_LIGHT0, GL_SPOT_DIRECTION, light0_direction);
	glLightfv(GL_LIGHT1, GL_SPOT_DIRECTION, light1_direction);
	glLightfv(GL_LIGHT2, GL_SPOT_DIRECTION, light2_direction);
	glLightfv(GL_LIGHT3, GL_SPOT_DIRECTION, light3_direction);
	glLightfv(GL_LIGHT4, GL_SPOT_DIRECTION, light4_direction);
	glLightfv(GL_LIGHT5, GL_SPOT_DIRECTION, light5_direction);


	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHT1);
	glEnable(GL_LIGHT2);
	glEnable(GL_LIGHT3);
	glEnable(GL_LIGHT4);
	glEnable(GL_LIGHT5);
}

static bool testFunc(const int& A) {
	return A >= 0;
}

// Draw the App scene - camera transformations, lighting, shadows, reflections, etc apply to everything drawn by this method
void TransformablesApp::drawScene() {

	robot->setState(foldedState);

	if (controlParams.showSkeletonSamples)
		transformEngine->skeletonOptEngine->skeletonFoldingPlan->draw(pickedRB);

	/*if (drawVoxelGrid) {
		glColor4d(1.0, 0.0, 0.0, 0.6);
		transformEngine->boundaryVoxelGrid->glDraw([](const int& A) { return A == 1; });
	}*/

	// drawJointSlitMeshes();
	drawJointBoundaries();
	drawMeshInterface();
	drawLSInterface();
	drawRobot();

	if (showMesh)
	{
		visMesh->getMaterial().setColor(0.8, 0.8, 1.0, 0.6);
		visMesh->drawMesh();
	}
	
}

// This is the wild west of drawing - things that want to ignore depth buffer, camera transformations, etc. Not pretty, quite hacky, but flexible. Individual apps should be careful with implementing this method. It always gets called right at the end of the draw function
void TransformablesApp::drawAuxiliarySceneInfo() {
	
	//clear the depth buffer so that the widgets show up on top of the object primitives
	glClear(GL_DEPTH_BUFFER_BIT);

	// draw the widgets
	tWidget->draw();
	rWidget->draw();

	glClear(GL_DEPTH_BUFFER_BIT);
	sideWindow->draw();
}

// Restart the application.
void TransformablesApp::restart() {

}

bool TransformablesApp::processCommandLine(const std::string& cmdLine) {

	int index = cmdLine.find_first_of(' ');
	string cmd = cmdLine.substr(0, index);

	if (cmd == "load")
	{
		string fName = cmdLine.substr(index + 1);
		loadFile(fName.c_str());

		return true;
	}

	if (GLApplication::processCommandLine(cmdLine)) return true;

	return false;
}

void TransformablesApp::drawRobot() {

	
	if (robot) {

		glEnable(GL_LIGHTING);
		glEnable(GL_CULL_FACE);
		glCullFace(GL_BACK);

		robot->getRoot()->selected = false;
		for (int i = 0; i < robot->getJointCount(); i++)
			robot->getJoint(i)->child->selected = false;
		if (pickedRB)
			pickedRB->selected = true;

		

		// draw the robot configuration...
		if (controlParams.showRobotAbstractView)
		{
			P3D rootP = robot->getRoot()->state.position;
			glColor4d(1.0, 0.0, 0.0, 0.5);
			drawSphere(rootP, 0.02, 36);

			for (int i = 0; i < robot->getRigidBodyCount(); i++)
				robot->getRigidBody(i)->draw(SHOW_ABSTRACT_VIEW);
		}
		

		TransformUtils::updateCapsuleMap(transformEngine->capsulesMap);
		// TransformUtils::updateVoxelInfoMap(transformEngine->voxelInfoMap);

		// drawJointMesh();
		drawRBOrigMeshes();

		drawRBThickness();
		
		drawRBDistanceField();

		if (controlParams.showRobotMeshes)
		{
			auto& robotMeshes = transformEngine->robotMeshes;
			if (pickedRB)
			{
				if (robotMeshes.count(pickedRB))
				{
					GLMesh* rbMesh = robotMeshes[pickedRB];
					V3D color = colorMap[pickedRB];
					glPushMatrix();
					TransformUtils::applyGLTransformForRB(pickedRB);
					if (controlParams.useMaterial)
						rbMesh->setMaterial(controlParams.shellMaterial);
					else {
						GLShaderMaterial defaultMat;
						defaultMat.setColor(color[0], color[1], color[2], 0.6);
						rbMesh->setMaterial(defaultMat);
					}
					rbMesh->drawMesh();
					glPopMatrix();
				}
			}
			else {
				for (auto itr = robotMeshes.begin(); itr != robotMeshes.end(); itr++)
				{
					RigidBody* rb = itr->first;
					GLMesh* rbMesh = itr->second;
					V3D color = colorMap[rb];
					glPushMatrix();
					TransformUtils::applyGLTransformForRB(rb);

					if (controlParams.useMaterial)
						rbMesh->setMaterial(controlParams.shellMaterial);
					else {
						GLShaderMaterial defaultMat;
						defaultMat.setColor(color[0], color[1], color[2], 0.6);
						rbMesh->setMaterial(defaultMat);
					}
					rbMesh->drawMesh();
					glPopMatrix();
				}
			}
		}
	}
}

void TransformablesApp::drawRBOrigMeshes()
{
	if (!controlParams.showSkeletonOrigMeshes)
		return;

	set<string> meshFilter = {
		"skeleton", "motor"
	};

	auto& skeletonMeshes = transformEngine->skeletonMeshes;

	GLMesh* bodyMesh = transformEngine->bodyMesh;
	if (bodyMesh)
	{
		glPushMatrix();
		TransformUtils::applyGLTransformForRB(robot->getRoot());
		bodyMesh->setMaterial(controlParams.skeletonMaterial);
		bodyMesh->drawMesh();
		glPopMatrix();
	}

	auto& RBOrigMeshes = transformEngine->RBOrigMeshes;
	for (auto& itr : RBOrigMeshes)
	{
		RigidBody* rb = itr.first;
		auto& meshVec = itr.second;
		auto& meshTrans = rb->meshTransformations;
		auto& carveMeshes = rb->carveMeshes;
		auto& meshDescriptions = rb->meshDescriptions;

		glPushMatrix();
		TransformUtils::applyGLTransformForRB(rb);
		for (uint i = 0; i < meshVec.size(); i++)
		{
			if (meshFilter.count(meshDescriptions[i]) == 0) {
				continue;
			}
				
			glPushMatrix();
			meshTrans[i].applyGLMatrixTransform();
			meshVec[i]->setMaterial(controlParams.skeletonMaterial);
			meshVec[i]->drawMesh();
			glPopMatrix();
		}
		auto itr = skeletonMeshes.find(rb);
		if (itr != skeletonMeshes.end())
		{
			GLMesh* sMesh = itr->second;
			sMesh->setMaterial(controlParams.skeletonMaterial);
			sMesh->drawMesh();
		}
		glPopMatrix();
	}
}

void TransformablesApp::drawJointMesh()
{

	if (pickedJoint >= 0)
	{
		Joint* joint = robot->getJoint(pickedJoint);

		{
			GLMesh* plugMesh = TransformUtils::getTransformedJointMesh(jointPlugMesh, joint, 1, false);
			glPushMatrix();
			TransformUtils::applyGLTransformForRB(joint->child);
			plugMesh->getMaterial().setColor(1.0, 0.0, 0.0, 1.0);
			plugMesh->drawMesh();
			glPopMatrix();
			delete plugMesh;
		}

		{
			GLMesh* socketMesh = TransformUtils::getTransformedJointMesh(jointSocketMesh, joint, 1, true);
			glPushMatrix();
			TransformUtils::applyGLTransformForRB(joint->parent);
			socketMesh->getMaterial().setColor(0.0, 0.0, 1.0, 1.0);
			socketMesh->drawMesh();
			glPopMatrix();
			delete socketMesh;
		}

	}
}


void TransformablesApp::drawMeshInterface()
{
	auto& meshInterfaces = transformEngine->meshInterfaces;
	auto& meshIFKnots = transformEngine->meshIFKnots;

	for (uint i = 0; i < meshInterfaces.size(); i++)
	{
		vector<P3D>& points = meshInterfaces[i].points;
		
		for (uint j = 0; j < points.size(); j++)
		{
			double radius = 0.002;
			glColor4d(1.0, 0.0, 0.0, 1.0);
			if (i == pickedInterface && j == pickedInterfaceP) {
				radius = 0.004;
				glColor4d(0.0, 0.0, 1.0, 1.0);
			}			
			drawSphere(points[j], radius, 10);
		}
		
		if (i == pickedInterface)
			glColor4d(0.0, 1.0, 1.0, 1.0);
		else
			glColor4d(1.0, 0.5, 0.0, 1.0);
		for (uint j = 0; j < points.size() - 1; j++)
		{			
			drawCylinder(points[j], points[j + 1], 0.001, 10);
		}
		if (meshInterfaces[i].loop)
		{
			drawCylinder(points.back(), points.front(), 0.001, 10);
		}
	}

	for (uint i = 0; i < meshIFKnots.size(); i++)
	{
		double radius = 0.004;
		if (i == pickedKnot)
			glColor4d(1.0, 0.0, 1.0, 1.0);
		else
			glColor4d(0.0, 0.0, 1.0, 1.0);

		drawSphere(meshIFKnots[i].position, radius, 10);
	}
	
}

void TransformablesApp::drawLSInterface()
{
	auto& LSIFs = transformEngine->LSIFs;

	for (uint i = 0; i < LSIFs.size(); i++)
	{
		vector<P3D>& points = LSIFs[i].points;
		vector<V3D>& normals1 = LSIFs[i].normals1;
		vector<V3D>& normals2 = LSIFs[i].normals2;

		for (uint j = 0; j < points.size(); j++)
		{
			double radius = 0.002;
			glColor4d(1.0, 0.0, 0.0, 1.0);
			drawSphere(points[j], radius, 10);

			drawArrow(points[j], points[j] + normals1[j] * LSIFs[i].v1[j], 0.002, 10);
			glColor4d(0.0, 1.0, 1.0, 1.0);
			drawArrow(points[j], points[j] + normals2[j] * LSIFs[i].v2[j], 0.002, 10);
		}
	}
}

void TransformablesApp::drawJointBoundaries()
{
	if (!showJointBoundaries) return;

	auto& jointBoundaries = transformEngine->skeletonOptEngine->skeletonFoldingPlan->jointBoundaries;

	for (uint k = 0; k < jointBoundaries.size(); k++)
	{
		RigidBody* rb = robot->getJoint(k)->parent;
		auto& points = jointBoundaries[k].points;
		auto& normals = jointBoundaries[k].normals;

		V3D color = colorMap[rb];
		glColor4d(color[0], color[1], color[2], 1.0);
		glPushMatrix();
		TransformUtils::applyGLTransformForRB(rb);
		for (uint i = 0; i < points.size(); i++)
		{
			drawSphere(points[i], 0.002, 24);
			drawArrow(points[i], points[i] + normals[i] * 0.01, 0.001, 24);
		}
		glPopMatrix();
	}
}

void TransformablesApp::drawJointSlitMeshes()
{
	auto& jointSlitMeshes = transformEngine->fabricationEngine->jointSlitMeshes;

	for (auto& itr : jointSlitMeshes)
	{
		RigidBody* rb = itr.first;
		auto& jMesh = itr.second;

		glPushMatrix();
		TransformUtils::applyGLTransformForRB(rb);
		jMesh->setMaterial(controlParams.skeletonMaterial);
		jMesh->drawMesh();
		glPopMatrix();
	}
}

void TransformablesApp::drawRBDistanceField()
{
	if (showDistanceField && transformEngine->SDFMap.count(pickedRB))
	{
		LevelSet origGrid = transformEngine->SDFMap[pickedRB];
		for (auto itr = colorMap.begin(); itr != colorMap.end(); itr++)
		{
			RigidBody* testRB = itr->first;
			if (testRB == pickedRB || transformEngine->parentMap[testRB] == pickedRB
				|| transformEngine->parentMap[pickedRB] == testRB) continue;

			transformEngine->foldingOptEngine->debugSDFCollisionBetweenRB(pickedRB, testRB);
		}

		glPushMatrix();
		glColor4d(1, 0, 0, 1);
		RBState& state = pickedRB->state;
		glTranslated(state.position[0], state.position[1], state.position[2]);
		//and rotation part
		V3D rotAxis; double rotAngle;
		state.orientation.getAxisAngle(rotAxis, rotAngle);
		glRotated(DEG(rotAngle), rotAxis[0], rotAxis[1], rotAxis[2]);
		transformEngine->SDFMap[pickedRB].glDraw([](const double& A) {return A <= 0; });
		
		glColor4d(0, 1, 0, 1);
		transformEngine->SDFMap[pickedRB].glDraw([](const double& A) {return A >= 10.0; });

		glPopMatrix();

		transformEngine->SDFMap[pickedRB] = origGrid;
	}
}

void TransformablesApp::drawRBThickness()
{
	if (showDistanceField || showPickedRB) return;

	if (controlParams.showAllThickness)
	{
		for (auto itr = colorMap.begin(); itr != colorMap.end(); itr++)
		{
			vector<Capsule> capsules;
			if (transformEngine->capsulesMap.empty()) {
				TransformUtils::getCapsulesForRigidBody(itr->first, capsules);
			}
			else {
				capsules = transformEngine->capsulesMap[itr->first];
			}

			V3D color = itr->second;
			glColor3d(color[0], color[1], color[2]);
			for (uint i = 0; i < capsules.size(); i++)
			{
				double thickness = itr->first->rbProperties.thickness;
				if (controlParams.showCapsuleRadius && !transformEngine->capsulesMap.empty())
					thickness = transformEngine->capsulesMap[itr->first][i].r;

				drawCylinder(capsules[i].seg.a, capsules[i].seg.b, thickness);
			}
		}
	}
	else if (pickedRB) {
		vector<Capsule> capsules;
		if (transformEngine->capsulesMap.empty()) {
			TransformUtils::getCapsulesForRigidBody(pickedRB, capsules);
		}
		else {
			capsules = transformEngine->capsulesMap[pickedRB];
		}

		V3D color = colorMap[pickedRB];
		glColor3d(color[0], color[1], color[2]);
		for (uint i = 0; i < capsules.size(); i++)
		{
			double thickness = pickedRB->rbProperties.thickness;
			if (controlParams.showCapsuleRadius && !transformEngine->capsulesMap.empty())
				thickness = transformEngine->capsulesMap[pickedRB][i].r;

			drawCylinder(capsules[i].seg.a, capsules[i].seg.b, thickness);
		}
	}
}

void TransformablesApp::buildJointSymmetryMap()
{
	if (!robot) return;

	jointSymmetryMap.clear();

	for (int i = 0; i < robot->getJointCount() - 1; i++)
		for (int j = i + 1; j < robot->getJointCount(); j++)
		{
			P3D jPos1 = robot->getJoint(i)->getWorldPosition();
			P3D jPos2 = robot->getJoint(j)->getWorldPosition();
			jPos1[0] *= -1;

			if ((jPos1 - jPos2).norm() < 1e-3)
			{
				jointSymmetryMap[i] = j;
				jointSymmetryMap[j] = i;
			}
		}

	for (auto itr = jointSymmetryMap.begin(); itr != jointSymmetryMap.end(); itr++)
	{
		if (itr->first < itr->second)
		{
			V3D& axis1 = static_cast<HingeJoint*>(robot->getJoint(itr->first))->rotationAxis;
			V3D& axis2 = static_cast<HingeJoint*>(robot->getJoint(itr->second))->rotationAxis;
			// Logger::print("pair: %d %d\n", itr->first, itr->second);
			// Logger::print("axis1: %lf %lf %lf\n", axis1[0], axis1[1], axis1[2]);
			// Logger::print("axis2: %lf %lf %lf\n", axis2[0], axis2[1], axis2[2]);
			axis2 = axis1;
			axis2[0] *= -1;
		}
	}
}

void TransformablesApp::saveRobotMeshes(string path, bool localCoord)
{
	robot->setState(foldedState);
	for (auto itr = transformEngine->robotMeshes.begin(); itr != transformEngine->robotMeshes.end(); itr++)
	{
		string fName = path + itr->first->getName() + ".obj";
		FILE* fp = fopen(fName.c_str(), "w+");
		if (localCoord)
			itr->second->renderToObjFile(fp, 0, Quaternion(), P3D());
		else
			itr->second->renderToObjFile(fp, 0, itr->first->state.orientation, itr->first->state.position);
		fclose(fp);
	}
}

void TransformablesApp::loadRobotMeshes()
{
	if (curConfig.meshFolderName.empty())
		return;
	robot->setState(foldedState);

	transformEngine->robotMeshes.clear();

	vector<RigidBody*>& RBs = transformEngine->RBs;

	for (uint i = 0; i < RBs.size(); i++)
	{
		RBs[i]->meshes.clear();
		string fileName = curConfig.meshFolderName + RBs[i]->name + ".obj";
		if (!TestFileExists(fileName)) continue;

		GLMesh* tmpMesh = OBJReader::loadOBJFile(fileName.c_str());
		if (!transParams.shellOnly) {
			TransformUtils::transformMeshFromWorldToLocalCoords(tmpMesh, RBs[i]);
		}
		
		RBs[i]->meshes.push_back(tmpMesh);
		transformEngine->robotMeshes[RBs[i]] = tmpMesh;
	}

	showMesh = false;
}

void TransformablesApp::handleFoldingOrderSwap()
{
	if (transformEngine->foldingOrder.empty()) return;

	if (jointToSwap == -1)
	{
		jointToSwap = pickedJoint;
		return;
	}

	vector<int>& foldOrder = transformEngine->foldingOrder;

	uint index1 = 0;
	uint index2 = 0;
	for (uint i = 0; i < foldOrder.size(); i++)
	{
		if (foldOrder[i] == jointToSwap)
			index1 = i;
		else if (foldOrder[i] == pickedJoint)
			index2 = i;
	}
	swap(foldOrder[index1], foldOrder[index2]);
	Logger::print("swap folding order of joint %d and %d\n", jointToSwap, pickedJoint);
}
