#pragma once

#include "SkeletonFoldingCMAEnergyFunction.h"
#include "SkeletonFoldingPlan.h"
#include "OptimizationLib/CMAIterativeFunctionMinimizer.h"


/**
	This is a locomotion engine that works for arbitrary robot types
*/
class SkeletonFoldingCMAOptimizer{
public:
	SkeletonFoldingCMAOptimizer(SkeletonFoldingPlan* plan, SkeletonFoldingCMAEnergyFunction* energyFunction);
	virtual ~SkeletonFoldingCMAOptimizer(void);

	double optimizePlan(int maxIterNum);

	void reinitialize();

public:
	CMAIterativeFunctionMinimizer* CMA_minimizer = NULL;
	SkeletonFoldingCMAEnergyFunction* energyFunction;
	SkeletonFoldingPlan* plan;
};



