#pragma once
#include "TransformUtils.h"
#include "TransformEngine.h"
#include "SkeletonFoldingOptimizer.h"
#include "SkeletonFoldingCMAOptimizer.h"

class TransformEngine;

class SkeletonOptEngine
{
public:
	TransformEngine* transformEngine;
	SkeletonFoldingPlan* skeletonFoldingPlan;
	SkeletonFoldingEnergyFunction* skeletonFoldingFunc;
	SkeletonFoldingOptimizer* skeletonFoldingOptimizer;
	SkeletonFoldingCMAOptimizer* skeletonFoldingCMAOptimizer;
	SkeletonFoldingCMAEnergyFunction* skeletonFoldingCMAFunc;

	GLMesh* approxMesh = NULL;

	bool reintializeCMA = true;

public:
	SkeletonOptEngine(TransformEngine* _transformEngine);
	~SkeletonOptEngine();

	// reinitalize();
	void reinitialize();

	// random sampling optimization
	void getRobotStateSamples(ReducedRobotState* robotState, vector<ReducedRobotState>& samples, double d_pos, double d_angle, map<int, int>* symmetryMap = NULL);
	double computeObjectiveEuclidean(ReducedRobotState* robotState);
	double computeObjectiveGeodesic(ReducedRobotState* robotState);

	double computeObjectiveBulkiness(ReducedRobotState* robotState);

	double computeCollisionObjective(ReducedRobotState* robotState, bool printInfo = false);
	double computeLSDiffObjective(ReducedRobotState* robotState, bool printInfo = false);
	double computeObjectiveContinous(ReducedRobotState* robotState, bool printInfo = false);
	double optimizeRobotState(ReducedRobotState* robotState, double d_pos, double d_angle, map<int, int>* symmetryMap = NULL);

	double optimizeRobotStateContinous(ReducedRobotState* robotState, map<int, int>* symmetryMap = NULL);

	double optimizeRobotStateCMA(ReducedRobotState* robotState);
};

