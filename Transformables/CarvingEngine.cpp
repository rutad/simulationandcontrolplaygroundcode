#include "CarvingEngine.h"
#include <MathLib/MeshBoolean.h>



CarvingEngine::~CarvingEngine()
{
	delete sweptVolumeMesh;
}

void CarvingEngine::initialSetup()
{
	Robot* robot = transformEngine->robot;
	ReducedRobotState* unfoldedState = transformEngine->unfoldedState;
	ReducedRobotState* foldedState = transformEngine->foldedState;

	RBs.clear();
	preRBStates.clear();
	TransformUtils::clearMeshMap(carvedMeshes);

	robot->setState(transformEngine->foldedState);

	for (uint i = 0; i < transformEngine->RBs.size(); i++)
	{
		RigidBody* rb = transformEngine->RBs[i];
		if (rb->meshes.empty()) continue;
		
		// process mesh
		RBs.push_back(rb);
		GLMesh* nMesh = rb->meshes[0]->clone();
		TransformUtils::transformMeshFromLocalToWorldCoords(nMesh, rb);
		nMesh->calBoundingBox();
		carvedMeshes[rb] = nMesh;

		preRBStates[rb] = rb->state;
	}

	// get animation states
	vector<int> unfoldOrder = transformEngine->foldingOrder;
	reverse(unfoldOrder.begin(), unfoldOrder.end());

	animationStates.clear();
	foldingSequence = TransformUtils::getAnimationStatesAndFoldingSequence(animationStates, robot, unfoldOrder, foldedState, unfoldedState, sampleStep);

	Logger::print("state size: %d\n\n", animationStates.size());

	for (uint i = 0; i < foldingSequence.size(); i++)
	{
		Logger::print("Folding sequence %d\n", i);
		Logger::print("startIndex: %d   endIndex: %d\n", foldingSequence[i].startIndex, foldingSequence[i].endIndex);

		set<RigidBody*>& movingRBs = foldingSequence[i].movingRBs;
		for (auto itr = movingRBs.begin(); itr != movingRBs.end(); itr++)
		{
			Logger::print("movingRB: %s\n", (*itr)->getName().c_str());
		}
	}

	// start carving from the second state.
	stateIndex = 0;
	sequenceIndex = 0;
}

void CarvingEngine::carveMesh()
{
	initialSetup();

	while (1) {
		if (carveJointParentForCurrentSequence())
			break;
	}

	transformCarvedMesh();
}

bool CarvingEngine::carveForCurrentSequence()
{
	double clearance = 1e-4;

	Robot* robot = transformEngine->robot;
	if (sequenceIndex >= (int)foldingSequence.size())
	{
		Logger::print("Carving Finishes!\n", stateIndex);
		return true;
	}

	int startIndex = foldingSequence[sequenceIndex].startIndex;
	int endIndex = foldingSequence[sequenceIndex].endIndex;
	Joint* joint = foldingSequence[sequenceIndex].joint;
	set<RigidBody*>& movingRBs = foldingSequence[sequenceIndex].movingRBs;

	Logger::print("state: %d\n", stateIndex);
	
	// initalize grid box
	gridBBox.empty();
	for (stateIndex = startIndex; stateIndex < endIndex; stateIndex++)
	{
		robot->setState(&animationStates[stateIndex]);
		
		for (auto itr = movingRBs.begin(); itr != movingRBs.end(); itr++)
		{
			RigidBody* rb = *itr;
			if (rb->meshes.empty()) continue;

			GLMesh* mesh = rb->meshes[0];
			for (int k = 0; k < mesh->getVertexCount(); k++)
			{
				P3D v = rb->getWorldCoordinates(mesh->getVertex(k));
				gridBBox.addPoint(v);
			}
		}	
	}
	gridBBox.setbmin(gridBBox.bmin() + V3D(1, 1, 1) * -2 * 0.01);
	gridBBox.setbmax(gridBBox.bmax() + V3D(1, 1, 1) * 2 * 0.01);

	LevelSet levelSet(gridBBox, transformEngine->transParams->voxelSize);
	//Logger::print("LS-VertexNum:%d %d %d\n", levelSet.vertexNum[0], levelSet.vertexNum[1], levelSet.vertexNum[2]);

	// calculate union level set
	vector<GLMesh*> tmpMeshes;

	for (stateIndex = startIndex; stateIndex < endIndex; stateIndex++)
	{
		robot->setState(&animationStates[stateIndex]);

		for (auto itr = movingRBs.begin(); itr != movingRBs.end(); itr++)
		{
			RigidBody* rb = *itr;
			if (rb->meshes.empty()) continue;
			
			GLMesh* mesh = carvedMeshes[rb];
			TransformUtils::transformMeshBetweenStates(mesh, preRBStates[rb], rb->state);
			mesh->calBoundingBox();
			if ((stateIndex == startIndex || stateIndex == endIndex - 1))
				tmpMeshes.push_back(mesh->clone());

			preRBStates[rb] = rb->state;

			levelSet.updateLevelSet(mesh, [](double origVal, double inputVal) {return min(origVal, inputVal); } , false);
		}
	}
	
	for (uint i = 0; i < RBs.size(); i++)
	{
		RigidBody* rb = RBs[i];
		if (movingRBs.count(rb)) continue;

		for (uint j = 0; j < tmpMeshes.size(); j++)
		{
			if ((rb == foldingSequence[sequenceIndex].joint->parent) || TransformUtils::testMeshIntersectSDF(tmpMeshes[j], rb, &transformEngine->SDFMap[rb]))
			{
				Logger::print("intersect %s\n", rb->getName().c_str());
				meshBooleanIntrusive(carvedMeshes[rb], tmpMeshes[j], "Minus");
			}
		}	
	}

	for (uint i = 0; i < tmpMeshes.size(); i++)
		delete tmpMeshes[i];

	/*delete sweptVolumeMesh;
	sweptVolumeMesh = levelSet.extractMesh(clearance);

	for (uint i = 0; i < tmpMeshes.size(); i++)
	{
		Logger::print("Union swept volume with original mesh %d\n", i);
		meshBooleanIntrusive(sweptVolumeMesh, tmpMeshes[i], "Union");
		delete tmpMeshes[i];
	}

	for (uint i = 0; i < RBs.size(); i++)
	{
		RigidBody* rb = RBs[i];
		if (movingRBs.count(rb)) continue;

		if ((rb == foldingSequence[sequenceIndex].joint->parent) || (i > 0 && TransformUtils::testMeshIntersectSDF(sweptVolumeMesh, rb, &transformEngine->SDFMap[rb])))
		{
			Logger::print("intersect %s\n", rb->getName().c_str());
			
			// do the carving
			GLMesh* mesh = carvedMeshes[rb];
			carvedMeshes[rb] = meshBoolean(mesh, sweptVolumeMesh, "Minus");
			delete mesh;
		}
	}*/

	sequenceIndex++;
	return false;
}

bool CarvingEngine::carveJointParentForCurrentSequence()
{
	Robot* robot = transformEngine->robot;
	if (sequenceIndex >= (int)foldingSequence.size())
	{
		Logger::print("Carving Finishes!\n", stateIndex);
		return true;
	}

	int startIndex = foldingSequence[sequenceIndex].startIndex;
	int endIndex = foldingSequence[sequenceIndex].endIndex;
	Joint* joint = foldingSequence[sequenceIndex].joint;
	set<RigidBody*>& movingRBs = foldingSequence[sequenceIndex].movingRBs;
	RigidBody* parentRB = joint->parent;
	RigidBody* childRB = joint->child;

	Logger::print("state: %d\n", stateIndex);

	if (childRB->meshes.empty()) {
		sequenceIndex++;
		return false;
	}

	vector<GLMesh*> tmpMeshes;

	for (stateIndex = startIndex; stateIndex < endIndex; stateIndex++)
	{
		robot->setState(&animationStates[stateIndex]);

		for (auto itr = movingRBs.begin(); itr != movingRBs.end(); itr++)
		{
			RigidBody* rb = *itr;
			if (rb->meshes.empty()) continue;

			GLMesh* mesh = carvedMeshes[rb];
			TransformUtils::transformMeshBetweenStates(mesh, preRBStates[rb], rb->state);
			mesh->calBoundingBox();
			if (rb == childRB && (stateIndex == startIndex || stateIndex == endIndex - 1))
				tmpMeshes.push_back(mesh->clone());

			preRBStates[rb] = rb->state;
		}
	}

	for (uint j = 0; j < tmpMeshes.size(); j++)
	{
		Logger::print("intersect %s\n", parentRB->getName().c_str());
		meshBooleanIntrusive(carvedMeshes[parentRB], tmpMeshes[j], "Minus");
	}

	for (uint i = 0; i < tmpMeshes.size(); i++)
		delete tmpMeshes[i];

	sequenceIndex++;
	return false;
}

void CarvingEngine::transformCarvedMesh()
{
	TransformUtils::clearMeshMap(transformEngine->robotMeshes);

	for (uint i = 0; i < RBs.size(); i++)
	{
		GLMesh* oldMesh = RBs[i]->meshes[0];
		GLMesh* newMesh = carvedMeshes[RBs[i]];
		TransformUtils::transformMeshFromWorldToLocalCoords(newMesh, RBs[i]);

		transformEngine->robotMeshes[RBs[i]] = newMesh;
		RBs[i]->meshes[0] = newMesh;
	}

	carvedMeshes.clear();
}