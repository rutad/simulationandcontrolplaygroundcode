#pragma once

#include <OptimizationLib/ObjectiveFunction.h>
#include <MathLib/Matrix.h>
#include "SkeletonFoldingPlan.h"

class SkeletonEmbedInsideObjective : public ObjectiveFunction {

public:
	SkeletonEmbedInsideObjective(SkeletonFoldingPlan* mp, const std::string& objectiveDescription, double weight);
	virtual ~SkeletonEmbedInsideObjective(void);

	virtual double computeValue(const dVector& p);

private:
	//the energy function operates on a motion plan...
	SkeletonFoldingPlan* plan;
	LevelSet* levelSet;
};

