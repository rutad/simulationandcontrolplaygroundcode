#pragma once
#include "TransformEngine.h"

#define SLIT_WIDTH (13e-3 * jointScale)
#define SLIT_RADIUS (12e-3 * jointScale)
#define BODY_SLIT_RADIUS (15e-3 * jointScale)
#define BODY_SLIT_CLEARANCE (20e-3 * jointScale)
#define SKELETON_MESH_CLEARANCE (10e-3 * jointScale)
#define SKELETON_MESH_RADIUS (12e-3 * jointScale)

#define MESHBOOLEAN_INTRUSIVE meshBooleanIntrusive

class TransformEngine;

class FabricationEngine
{
private:
	TransformEngine* transformEngine;
	// Joint meshes

public:
	double jointScale = 1.0;

	map<RigidBody*, GLMesh*> jointSlitMeshes;
	// level sets using to represent skeleton
	map<RigidBody*, LevelSet> skeletonLevelSets;
	// visualization use only
	map<RigidBody*, GLMesh*> skeletonLSMeshes;

	// attachment segments
	map<RigidBody*, Segment> attachmentSegs;

	// hardcoded feature points for generating skeleton meshes
	map<string, vector<P3D>> moduleFeaturePoint;

	vector<int> transDir;

	GLMesh* jointPlugMesh = NULL;
	GLMesh* jointPlugCarveMesh = NULL;
	GLMesh* jointSocketMesh = NULL;
	GLMesh* jointSocketCarveMesh = NULL;

public:
	FabricationEngine(TransformEngine* _transformEngine);
	~FabricationEngine();

	void getTransferDirection();
	
	// generate skeleton meshes, including joint structures. (not for root)
	void attachSkeletonMeshes();

	// generate slit meshes
	void generateJointSlitMeshes();

	// handle main body, do it seperately from other parts because there are too many boolean operations on main body,
	// which may leads to mesh with big file size.
	void handleMainBody();

	// attach joint meshes
	void attachJointMeshes();

	// New
	void prepareForLSCarving();
	void generateSkeletonLevelSets();
	void generateHardcodedFeaturePoints();
	void generateCylinderSkeletonMeshes(RigidBody* selectedRB = NULL);
	void generateBodyMesh();

	// carving
	void carveLS(int mode); // mode: 1 -- carve for skeleton; 2 -- carve for meshes; 3 -- carve for both.
	void carveSkeletonLSForStates(vector<ReducedRobotState>& stateSequence);
	void carveRBMeshForStates(vector<ReducedRobotState>& stateSequence);
	void carveSkeletonPreciselyForRB();

	// shell
	void generateAttachmentSegs();
	void generateSocketStructureForRB();
	void mergeAndSaveSkeletonMeshForRB(const char* path);
	void createAndSaveStructuresForBody(const char* path);

	// volume
	void attachSkeletonToVolume();

	// living brackets
	void prepareForFabrication();
	void carveLSForMeshesAndSkeletonsForLivingBracket();
	void generateJointSlitMeshesForLivingBracket();
	void carveForJointSlits();
	void mergeSkeletonAndSkinMeshes();
};

