#include "TransformEngine.h"


void TransformEngine::buildLSInterfaces()
{
	initialize();

	int totalVertexNum = MlevelSets[0].totalVertexNum;
	double LSStepSize = transParams->LSStepSize;

	LSIFs.clear();
	for (uint i = 0; i < MlevelSets.size(); i++)
	{
		MlevelSets[i].fastMarchingOutward(LSStepSize);
		MlevelSets[i].fastMarchingInward(LSStepSize);
	}

	// Logger::print("Start Build LSIF.\n");

	for (int i = 0; i < (int)RBs.size() - 1; i++)
	{
		for (int j = i + 1; j < (int)RBs.size(); j++)
		{
			RigidBody* rb_i = RBs[i];
			RigidBody* rb_j = RBs[j];
			LevelSet& LS_i = MlevelSets[i];
			LevelSet& LS_j = MlevelSets[j];
			auto& LSGrad_i = MLevelSetsGrad[i];
			auto& LSGrad_j = MLevelSetsGrad[j];
			LSInterface LSIF(i, j, rb_i, rb_j);

			for (int k = 0; k < totalVertexNum; k++)
			{

				P3D p = LS_i.getVertex(k);
				double val_i = LS_i.interpolateData(p[0], p[1], p[2]);
				double val_j = LS_j.interpolateData(p[0], p[1], p[2]);
				double val_m = meshLevelSet.interpolateData(p[0], p[1], p[2]);			

				if (val_j <= LSStepSize && val_i <= 0 && val_m < 0.5 * LSStepSize)
				{
					V3D g_i, g_j;
					for (int c = 0; c < 3; c++)
					{
						g_i = LS_i.interpolateGradient(p[0], p[1], p[2]);
						g_j = LS_j.interpolateGradient(p[0], p[1], p[2]);
						V3D dp_i = -LS_i.interpolateData(p[0], p[1], p[2]) / g_i.norm() * g_i.normalized();
						V3D dp_j = -LS_j.interpolateData(p[0], p[1], p[2]) / g_j.norm() * g_j.normalized();
						// p += dp_i;
						p += (dp_j + dp_i) * 0.5;
					}
					
					LSIF.points.push_back(p);
					LSIF.normals1.push_back(g_i.normalized());
					LSIF.normals2.push_back(g_j.normalized());
				}
			}

			if (LSIF.points.size() > 0)
			{
				LSIF.v1.resize(LSIF.points.size());
				LSIF.v2.resize(LSIF.points.size());
				LSIF.v1.setZero();
				LSIF.v2.setZero();
				LSIFs.push_back(LSIF);
			}
		}
	}

	// Logger::print("End Build LSIF.\n");
}

void TransformEngine::evolveLSIFs()
{
	for (uint i = 0; i < LSIFs.size(); i++)
	{
		evolveLSInterface(i);
	}
}


void TransformEngine::evolveLSInterface(int interfaceIndex)
{
	initialize();

	double lamb = 1;

	LSInterface* curInterface = &LSIFs[interfaceIndex];
	typedef Eigen::Triplet<double> T;
	map<int, map<int, double>> vMap;
	double LSStepSize = transParams->LSStepSize;
	robot->setState(foldedState);

	int layerVertexNum = MlevelSets[0].layerVertexNum;
	int stripVertexNum = MlevelSets[0].vertexNum[0];

	vector<int> movedLSIndex;
	movedLSIndex.push_back(curInterface->index1);
	movedLSIndex.push_back(curInterface->index2);

	auto& points = curInterface->points;
	P3D sp = MlevelSets[0].startPoint;

	// Logger::print("Begin solving linear system.\n");

	for (int k = 0; k < (int)movedLSIndex.size(); k++)
	{
		int index = movedLSIndex[k];
		vMap[index] = map<int, double>();
		vector<T> W;
		vector<double> Y;
		map<int, int> IndexMap;
		int curIndex = 0;
		int pIndex = 0;

		auto& M = vMap[index];

		for (uint i = 0; i < points.size(); i++)
		{
			P3D p0 = points[i];
			double L = MlevelSets[index].interpolateData(p0[0], p0[1], p0[2]);
			V3D G = meshLevelSet.interpolateGradient(p0[0], p0[1], p0[2]).normalized();
			P3D p = p0;

			Y.push_back(L);

			int x = (int)((p[0] - sp[0]) / LSStepSize);
			int y = (int)((p[1] - sp[1]) / LSStepSize);
			int z = (int)((p[2] - sp[2]) / LSStepSize);

			for (int dx = 0; dx <= 1; dx++)
				for (int dy = 0; dy <= 1; dy++)
					for (int dz = 0; dz <= 1; dz++) {
						int nx = x + dx;
						int ny = y + dy;
						int nz = z + dz;
						P3D v = MlevelSets[index].getVertex(nx, ny, nz);
						int vIndex = nz * layerVertexNum + (ny * stripVertexNum + nx);

						double wx = (p[0] - sp[0]) / LSStepSize - nx;
						double wy = (p[1] - sp[1]) / LSStepSize - ny;
						double wz = (p[2] - sp[2]) / LSStepSize - nz;
						if (wx >= 0)
							wx = 1 - wx;
						else
							wx = 1 + wx;
						if (wy >= 0)
							wy = 1 - wy;
						else
							wy = 1 + wy;
						if (wz >= 0)
							wz = 1 - wz;
						else
							wz = 1 + wz;
						double weight = wx * wy * wz;

						int mIndex = -1;
						if (IndexMap.count(vIndex))
						{
							mIndex = IndexMap[vIndex];
						}
						else {
							mIndex = curIndex;
							IndexMap[vIndex] = curIndex++;
						}

						//Logger::print("%d %d %lf\n", pIndex, mIndex, weight);
						W.push_back(T(pIndex, mIndex, weight));
					}

			pIndex++;
		}

		dVector vy(Y.size());
		for (uint i = 0; i < Y.size(); i++)
			vy[i] = Y[i];

		// Logger::print("size: %d %d\n", vy.size(), curIndex);
		dVector sol;
		Eigen::SimplicialLDLT<SparseMatrix> solver;
		SparseMatrix C(vy.size(), curIndex);
		C.setFromTriplets(W.begin(), W.end());
		SparseMatrix ID(curIndex, curIndex);
		ID.setIdentity();

		solver.compute(C.transpose() * C + lamb * ID);
		sol = solver.solve(C.transpose() * vy);

		for (auto& itr : IndexMap)
		{
			M[itr.first] = sol[itr.second];
		}
	}

	double threshold = 50;
	uint floodIterNum = 1;

	int* vertexNum = MlevelSets[0].vertexNum;

	// Logger::print("Start flooding.\n");

	for (uint k = 0; k < floodIterNum; k++)
	{
		for (uint i = 0; i < RBs.size(); i++)
		{
			LevelSet curLS = MlevelSets[i];
			double v = 1e-3;
			double alpha = 1e-3;
			map<int, double>* pMap = NULL;
			if (vMap.count(i))
			{
				pMap = &vMap[i];
			}
			else {
				continue;
			}
#pragma omp parallel for
			for (int z = 1; z < vertexNum[2] - 1; z++)
				for (int y = 1; y < vertexNum[1] - 1; y++)
					for (int x = 1; x < vertexNum[0] - 1; x++) {

						double val = curLS.getData(x, y, z);
						if (fabs(val) > LSStepSize) continue;

						double L = (curLS.getData(x + 1, y, z) + curLS.getData(x - 1, y, z) + curLS.getData(x, y + 1, z)
							+ curLS.getData(x, y - 1, z) + curLS.getData(x, y, z + 1) + curLS.getData(x, y, z - 1) - 6 * val) / (LSStepSize * LSStepSize);

						//Logger::print("L:%lf ", L);

						if (L > threshold)
							L -= threshold;
						else if (L < -threshold)
							L += threshold;
						else
							L = 0;

						double n = -L * alpha * LSStepSize + v;
						int vIndex = z * layerVertexNum + (y * stripVertexNum + x);
						if (pMap && (*pMap).count(vIndex))
						{
							double extra = (*pMap)[vIndex];
							n += extra;
						}
						MlevelSets[i].addData(x, y, z, -n);
					}
		}
	}

#pragma omp parallel for
	for (int i = 0; i < (int)movedLSIndex.size(); i++)
	{
		int index = movedLSIndex[i];
		MlevelSets[index].intersectLS(&largeMeshLevelSet);
		MlevelSets[index].fastMarchingInward(LSStepSize);
		MlevelSets[index].fastMarchingOutward(LSStepSize, 4 * LSStepSize);
	}
	projectMultiLevelSetsEx(MlevelSets, movedLSIndex[0], movedLSIndex[1]);

#pragma omp parallel for
	for (int k = 0; k < (int)movedLSIndex.size(); k++)
	{
		int index = movedLSIndex[k];
		extractMeshesFromLSForRB(index);
	}

	// Logger::print("End Evolving.\n");
}


void TransformEngine::calEvolveVelocityField()
{
	// projectPointsBackToIF();

	// Logger::print("Start Cal Velocity.\n");

	double sampleStep = 0.1;
	vector<ReducedRobotState> stateSequence;
	TransformUtils::getAnimationStates(stateSequence, foldingOrder, unfoldedState, foldedState, sampleStep);

	map<RigidBody*, Transformation> foldedStateL2W;
	map<RigidBody*, Transformation> foldedStateW2L;

	robot->setState(foldedState);
	for (int i = 0; i < robot->getRigidBodyCount(); i++)
	{
		RigidBody* rb = robot->getRigidBody(i);
		foldedStateL2W[rb] = Transformation(rb->state.orientation.getRotationMatrix(), rb->state.position);
		foldedStateW2L[rb] = foldedStateL2W[rb].inverse();
	}

	for (auto& LSIF : LSIFs) {
		LSIF.v1.setZero();
		LSIF.v2.setZero();
		LevelSet& LS_1 = MlevelSets[LSIF.index1];
		LevelSet& LS_2 = MlevelSets[LSIF.index2];

		for (int i = 0; i < (int)LSIF.points.size(); i++) {
			P3D p = LSIF.points[i];
			LSIF.normals1[i] = LS_1.interpolateGradient(p[0], p[1], p[2]).normalized();
			LSIF.normals2[i] = LS_2.interpolateGradient(p[0], p[1], p[2]).normalized();
		}
	}

	for (auto& robotState : stateSequence)
	{
		robot->setState(&robotState);

		for (auto& LSIF : LSIFs)
		{
			RigidBody* RB_1 = LSIF.rb1;
			RigidBody* RB_2 = LSIF.rb2;
			int RBIndex_1 = LSIF.index1;
			int RBIndex_2 = LSIF.index2;
			LevelSet& LS_1 = MlevelSets[RBIndex_1];
			LevelSet& LS_2 = MlevelSets[RBIndex_2];
			dVector& v1 = LSIF.v1;
			dVector& v2 = LSIF.v2;
			Transformation relTrans1to2, relTrans2to1;

			Transformation W2L_1 = foldedStateW2L[RB_1];
			Transformation L2W_1(RB_1->state.orientation.getRotationMatrix(), RB_1->state.position);
			Transformation W2L_2 = Transformation(RB_2->state.orientation.getRotationMatrix(), RB_2->state.position).inverse();
			Transformation L2W_2 = foldedStateL2W[RB_2];
			relTrans1to2 = L2W_2 * W2L_2 * L2W_1 * W2L_1;
			relTrans2to1 = relTrans1to2.inverse();

			for (int i = 0; i < (int)LSIF.points.size(); i++)
			{
				P3D p = LSIF.points[i];
				
				P3D np = relTrans1to2.transform(p);
				if (LS_2.isInside(np[0], np[1], np[2]))
				{
					double in1 = LS_2.interpolateData(np[0], np[1], np[2]);
					v1[i] = min(v1[i], in1);
				}

				np = relTrans2to1.transform(p);
				if (LS_1.isInside(np[0], np[1], np[2]))
				{
					double in2 = LS_1.interpolateData(np[0], np[1], np[2]);
					v2[i] = min(v2[i], in2);
				}
			}
		}
	}

	// Logger::print("End Cal Velocity.\n");
}


void TransformEngine::evolveSamplePoints()
{
	for (auto& LSIF : LSIFs)
	{
		auto& points = LSIF.points;
		auto& normals1 = LSIF.normals1;
		auto& normals2 = LSIF.normals2;
		auto& v1 = LSIF.v1;
		auto& v2 = LSIF.v2;

		for (int i = 0; i < (int)points.size(); i++)
		{
			points[i] += (normals1[i] * v1[i] + normals2[i] * v2[i]) * 0.2;
		}
	}
}


void TransformEngine::projectPointsBackToIF()
{
	for (auto& LSIF : LSIFs)
	{
		auto& points = LSIF.points;
		auto& LS_1 = MlevelSets[LSIF.index1];

		for (int i = 0; i < (int)points.size(); i++)
		{
			P3D& p = points[i];
			V3D g_i = LS_1.interpolateGradient(p[0], p[1], p[2]);
			V3D dp_i = -LS_1.interpolateData(p[0], p[1], p[2]) / g_i.norm() * g_i.normalized();
			p += dp_i;
		}
	}
}


