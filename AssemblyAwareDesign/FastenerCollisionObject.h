#pragma once
#include "BaseObject.h"

/*For fastener collisions*/

class FastenerCollisionObject : public BaseObject {
public:
	btCapsuleShape colShape = btCapsuleShape(1,1);
	btCollisionObject colObject;
	//these are the local coordinates of the attachment point relative to the emComponent -- the grandparent
	P3D grandParentLocalSupportPos;

	Fastener* parent = NULL;

	FastenerCollisionObject(Fastener* ft, P3D pos) {
		parent = ft;
		grandParentLocalSupportPos = pos;
		initGeometry();
		name = "FastenerCollisionObject";
	}
	virtual ~FastenerCollisionObject() {};

	void initGeometry();

	virtual void getSilhouetteScalingDimensions(double &dimX, double &dimY, double &dimZ) {
		dimZ = 0;
		dimX = 0;
		dimY = 0;
	}

	virtual bool shouldCollideWith(BaseObject* obj);

	virtual P3D getPosition();

	virtual Quaternion getOrientation();

	virtual void drawObjectGeometry(GLShaderMaterial* material = NULL) {};

	virtual void addBulletCollisionObjectsToList(DynamicArray<btCollisionObject*>& bulletCollisionObjs);
};