#pragma once

#include "MakeBlockComponents.h"

class MotorComponents : public MakeBlockComponents {
public:
	MotorComponents() {
	}
	virtual ~MotorComponents() {};
};

class DCMotor : public MotorComponents {
public:
	// constructor
	DCMotor(bool textureFlag = true) {
		loadMeshAndTexture = textureFlag;
		initGeometry();
	}
	virtual ~DCMotor() {};

	virtual void initGeometry() {
		name = "DCMotor";
		//componentMeshes.push_back(GLContentManager::getGLMesh("../data/3dModels/pololuMotorWBracket_forGUI.obj"));

		if (loadMeshAndTexture) {
			componentMeshes.push_back(GLContentManager::getGLMesh("../data/3dModels/makeblock/pololuMotorBracket.obj"));
			GLShaderMaterial tmpMat;
			//tmpMat.r = wireColor[0]; tmpMat.g = wireColor[1]; tmpMat.b = wireColor[2]; tmpMat.a = wireColor[3]; componentMeshes.back()->setMaterial(tmpMat);
			tmpMat.setShaderProgram(GLContentManager::getShaderProgram("matcap"));
			tmpMat.setTextureParam("../data/textures/matcap/cream.bmp", GLContentManager::getTexture("../data/textures/matcap/cream.bmp"));
			componentMeshes.back()->setMaterial(tmpMat);

			componentMeshes.push_back(GLContentManager::getGLMesh("../data/3dModels/makeblock/pololuMotor.obj"));
			tmpMat.setShaderProgram(GLContentManager::getShaderProgram("matcap"));
			tmpMat.setTextureParam("../data/textures/matcap/roughAlu.bmp", GLContentManager::getTexture("../data/textures/matcap/roughAlu.bmp"));
			componentMeshes.back()->setMaterial(tmpMat);
		}

		//bounding box
		xMax = 0.0060 * 2;
		yMax = 0.0050 * 2;
		zMax = 0.0176;

		colShapes.push_back(btBoxShape(btVector3((btScalar)(xMax / 2.0), (btScalar)(yMax / 2.0), (btScalar)(zMax / 2.0))));
		colShapeGeometryCenterLocalOffset.push_back(V3D(0,0,-zMax/2));
		colShapes.push_back(btBoxShape(btVector3((btScalar)(0.0133), (btScalar)(0.0059), (btScalar)(zMax / 2.0))));
		colShapeGeometryCenterLocalOffset.push_back(V3D(0,0,zMax/2));

		// add assembly path and convex hull points
		disassemblyPath = new TwoPhaseArbitraryDirectionDissassemblyPath(this);
		DynamicArray<P3D> colShapePoints;
		for (size_t i = 0; i < colShapes.size(); i++)
		{
			addBTColShapeVerticesToPointList(colShapes[i], colShapePoints, colShapeGeometryCenterLocalOffset[i]);
		}

		DynamicArray<ConvexHull_Face> faces;
		ConvexHull3D::computeConvexHullFromSetOfPoints(colShapePoints, objectCoordsConvexHullVertices, faces);

		// support, fasteners and wires
		supports.push_back(new RigidlyAttachedSupport(this, P3D(0, -0.0059- 0.0025, 0), AxisAlignedBoundingBox(P3D(-0.0133, -0.0025, -zMax), P3D(0.0133, 0.0025, zMax))));

		fasteners.push_back(new Fastener(this, P3D(-0.017 / 2, -0.0025, 0.015 - 0.0025), V3D(0, 1, 0), 0.0025 / 2));
		fasteners.push_back(new Fastener(this, P3D(0.017 / 2, -0.0025, 0.015 - 0.0025), V3D(0, 1, 0), 0.0025 / 2));
	}

	virtual EMComponent* clone(bool cloneTexture = true) {
		DCMotor* newObject = new DCMotor(cloneTexture);
		// copy other properties eg: support sizes..
		return newObject;
	}

	virtual GLMesh* getWireGeometryMesh() {
		P3D pos = this->getPosition();

		GLMesh* wireMesh = new GLMesh();
		wireMesh->addCylinder(pos,
			pos + this->getWorldCoordinates(V3D(0, 0, 1)) *zMax*2, 0.0060);

		return wireMesh;
	}

	virtual DynamicArray<std::pair<char*, double*>> getObjectParameters() {
		DynamicArray<std::pair<char*, double*>> result;
		result.push_back(std::pair<char*, double*>("disassembly dir-x", &dynamic_cast<TwoPhaseArbitraryDirectionDissassemblyPath*>(disassemblyPath)->disassemblyDefaultPath[0]));
		result.push_back(std::pair<char*, double*>("disassembly dir-y", &dynamic_cast<TwoPhaseArbitraryDirectionDissassemblyPath*>(disassemblyPath)->disassemblyDefaultPath[1]));
		result.push_back(std::pair<char*, double*>("disassembly dir-z", &dynamic_cast<TwoPhaseArbitraryDirectionDissassemblyPath*>(disassemblyPath)->disassemblyDefaultPath[2]));
		dynamic_cast<TwoPhaseArbitraryDirectionDissassemblyPath*>(disassemblyPath)->disassemblyDefaultPath.toUnit();
		//result.push_back(std::pair<char*, double*>("Cuboid-height", &h));
		return result;
	}
};

class ServoMotor : public MotorComponents {
public:

	// constructor
	ServoMotor(bool textureFlag = true) {
		loadMeshAndTexture = textureFlag;
		initGeometry();
	}
	virtual ~ServoMotor() {};

	virtual void initGeometry() {
		name = "ServoMotor";

		if (loadMeshAndTexture) {
			componentMeshes.push_back(GLContentManager::getGLMesh("../data/3dModels/ServoMG995.obj"));
			GLShaderMaterial tmpMat;
			//tmpMat.r = wireColor[0]; tmpMat.g = wireColor[1]; tmpMat.b = wireColor[2]; tmpMat.a = wireColor[3]; componentMeshes.back()->setMaterial(tmpMat);
			tmpMat.setShaderProgram(GLContentManager::getShaderProgram("matcap"));
			tmpMat.setTextureParam("../data/textures/matcap/MatCap_0008.bmp", GLContentManager::getTexture("../data/textures/matcap/MatCap_0008.bmp"));
			componentMeshes.back()->setMaterial(tmpMat);
		}

		//bounding box
		xMax = 0.0273 * 2;
		yMax = 0.0216 * 2;
		zMax = 0.0098*2;

		colShapes.push_back(btBoxShape(btVector3((btScalar)(0.0215), (btScalar)(0.018645), (btScalar)(zMax / 2.0))));
		colShapeGeometryCenterLocalOffset.push_back(V3D(0, -0.0020, 0));

		// add assembly path and convex hull points
		MakeBlockComponents::initGeometry();

		// support, fasteners and wires
		supports.push_back(new RigidlyAttachedSupport(this, P3D(-xMax/2 + 0.0035, yMax / 8 -0.003, 0), AxisAlignedBoundingBox(P3D(-0.0035, -0.0035, -(zMax/2 + 0.0025)), P3D(0.0035, 0.0035, (zMax/2 + 0.0025)))));
		supports.back()->nonResizeableDirections.push_back(V3D(1, 0, 0));
		supports.push_back(new RigidlyAttachedSupport(this, P3D(xMax / 2 - 0.0035, yMax / 8 - 0.003, 0), AxisAlignedBoundingBox(P3D(-0.0035, -0.0035, -(zMax / 2 + 0.0025)), P3D(0.0035, 0.0035, (zMax / 2 + 0.0025)))));
		supports.back()->nonResizeableDirections.push_back(V3D(-1, 0, 0));

		fasteners.push_back(new Fastener(this, P3D(xMax/2.0 - 0.0035, 0.01, -zMax / 4.0), V3D(0, 1, 0), 0.0021));
		fasteners.push_back(new Fastener(this, P3D(xMax / 2.0 - 0.0035, 0.01, zMax / 4.0), V3D(0, 1, 0), 0.0021));
		fasteners.push_back(new Fastener(this, P3D(-xMax / 2.0 + 0.0035, 0.01, -zMax / 4.0), V3D(0, 1, 0), 0.0021));
		fasteners.push_back(new Fastener(this, P3D(-xMax / 2.0 + 0.0035, 0.01, zMax / 4.0), V3D(0, 1, 0), 0.0021));
	}

	virtual EMComponent* clone(bool cloneTexture = true) {
		ServoMotor* newObject = new ServoMotor(cloneTexture);
		// copy other properties eg: support sizes..
		return newObject;
	}

	virtual GLMesh* getWireGeometryMesh() {
		P3D pos = this->getWorldCoordinates(P3D(xMax / 2.0 - 0.0035, -yMax/2.0, -zMax / 4.0));
		P3D pos1 = this->getWorldCoordinates(P3D(xMax / 2.0 - 0.0035, -yMax / 2.0, zMax / 4.0));
		P3D pos2 = this->getWorldCoordinates(P3D(-xMax / 2.0 + 0.0035, -yMax / 2.0, -zMax / 4.0));
		P3D pos3 = this->getWorldCoordinates(P3D(-xMax / 2.0 + 0.0035, -yMax / 2.0, zMax / 4.0));

		GLMesh* wireMesh = new GLMesh();
		GLMesh* tempMesh = new GLMesh();
		GLMesh* tempMesh1 = new GLMesh();

		tempMesh->addCylinder(pos,
			pos + this->getWorldCoordinates(V3D(0, 1, 0)) *yMax*0.42, 0.0040);
		tempMesh->addCylinder(pos1,
			pos1 + this->getWorldCoordinates(V3D(0, 1, 0)) *yMax*0.42, 0.0040);

		tempMesh1->addCylinder(pos2,
			pos2 + this->getWorldCoordinates(V3D(0, 1, 0)) *yMax*0.42, 0.0040);
		tempMesh1->addCylinder(pos3,
			pos3 + this->getWorldCoordinates(V3D(0, 1, 0)) *yMax*0.42, 0.0040);

		ConvexHull3D::computeConvexHullForMesh(tempMesh, wireMesh);
		ConvexHull3D::computeConvexHullForMesh(tempMesh1, wireMesh);

		delete tempMesh;
		delete tempMesh1;
		return wireMesh;
	}
};

class MicroServoMotor : public MotorComponents {
public:
	// constructor
	MicroServoMotor(bool textureFlag = true) {
		loadMeshAndTexture = textureFlag;
		initGeometry();
	}
	virtual ~MicroServoMotor() {};

	virtual void initGeometry() {
		name = "MicroServoMotor";

		if (loadMeshAndTexture) {
			componentMeshes.push_back(GLContentManager::getGLMesh("../data/3dModels/microServo.obj"));
			GLShaderMaterial tmpMat;
			//tmpMat.r = wireColor[0]; tmpMat.g = wireColor[1]; tmpMat.b = wireColor[2]; tmpMat.a = wireColor[3]; componentMeshes.back()->setMaterial(tmpMat);
			tmpMat.setShaderProgram(GLContentManager::getShaderProgram("matcap"));
			tmpMat.setTextureParam("../data/textures/matcap/blueled.bmp", GLContentManager::getTexture("../data/textures/matcap/blueled.bmp"));
			componentMeshes.back()->setMaterial(tmpMat);
		}

		//bounding box
		xMax = 0.01615 * 2;
		yMax = 0.016 * 2;
		zMax = 0.006 * 2;

		colShapes.push_back(btBoxShape(btVector3((btScalar)(xMax / 2.0), (btScalar)(0.0111), (btScalar)(zMax / 2.0))));
		colShapeGeometryCenterLocalOffset.push_back(V3D(0, -0.004, 0));

		// add assembly path and convex hull points
		MakeBlockComponents::initGeometry();

		// support, fasteners and wires
		supports.push_back(new RigidlyAttachedSupport(this, P3D(-xMax / 2 + 0.0025, -0.0010, 0), AxisAlignedBoundingBox(P3D(-0.0025, -0.0025, -(zMax / 2 + 0.002)), P3D(0.0025, 0.0025, (zMax / 2 + 0.002)))));
		supports.back()->nonResizeableDirections.push_back(V3D(1, 0, 0));
		supports.push_back(new RigidlyAttachedSupport(this, P3D(xMax / 2 - 0.0025, -0.0010, 0), AxisAlignedBoundingBox(P3D(-0.0025, -0.0025, -(zMax / 2 + 0.002)), P3D(0.0025, 0.0025, (zMax / 2 + 0.002)))));
		supports.back()->nonResizeableDirections.push_back(V3D(-1,0,0));

		fasteners.push_back(new Fastener(this, P3D(xMax / 2.0 - 0.0025, 0.0, 0.0), V3D(0, 1, 0), 0.0011));
		fasteners.push_back(new Fastener(this, P3D(-xMax / 2.0 + 0.0025, 0.0, 0.0), V3D(0, 1, 0), 0.0011));
	}

	virtual EMComponent* clone(bool cloneTexture = true) {
		MicroServoMotor* newObject = new MicroServoMotor(cloneTexture);
		// copy other properties eg: support sizes..
		return newObject;
	}

};