#pragma once

#include <GUILib/GLMesh.h>
#include <MathLib/mathLib.h>
#include <MathLib/P3D.h>
#include <MathLib/V3D.h>
#include <MathLib/Quaternion.h>
#include <MathLib/Ray.h>
#include <GUILib/TranslateWidget.h>
#include <GUILib/RotateWidget.h>
#include <GUILib/PlotWindow.h>

#include <OptimizationLib\ObjectiveFunction.h>
#include <OptimizationLib\SoftUnilateralConstraint.h>
#include "BaseObject.h"

#include <BulletCollision/btBulletCollisionCommon.h>

class CollisionObject : public ObjectiveFunction {
public:
	
	//range at which the energy/cost becomes zero
	double epsilon = 0;
	//stiffness of quadratic cost function
	double a = 0;

	// pairwise collisions between..
	BaseObject *obj1 = NULL, *obj2 = NULL;
	P3D p_WorldOnO1, p_WorldOnO2;
	V3D nWorld;
	double penetrationDepth = 0;

	CollisionObject();
	~CollisionObject() {}

	double computeValue(const dVector& p);
};
