#pragma once

#include <GUILib/GLApplication.h>
#include <BulletCollision/btBulletCollisionCommon.h>
#include "BulletCollisionObject.h"

class Assembly;
class CollisionObject;
class EMComponent;
class AssemblableComponent;

#define CM_EMCOMPONENTS 0x01
#define CM_SUPPORTS 0x02
#define CM_FASTNERS 0x04
#define CM_WALLS 0x08

#define COL_SWEPT_CVX_HULL 0x01
#define COL_EMC_GEOM 0x02
#define COL_SWEPT_FASTNERS 0x04
#define COL_SUPPORT_GEOM 0x08

class CollisionManager {

public:
	// input assembly
	Assembly* assembly;

	// bullet related
	btCollisionConfiguration* bt_collision_configuration = NULL;
	btCollisionDispatcher* bt_dispatcher = NULL;
	btBroadphaseInterface* bt_broadphase = NULL;
	btCollisionWorld* bt_collision_world = NULL;
	btIDebugDraw* bt_DebugDrawer = NULL;

	DynamicArray<btCollisionObject*> bulletCollisionPrimitives;

	BT_DECLARE_ALIGNED_ALLOCATOR();

	// constructor - destructor
	CollisionManager(Assembly* inputAssembly);
	~CollisionManager(void) {
		clearCollisionPrimitives();
		delete bt_collision_world;
		delete bt_broadphase;
		delete bt_dispatcher;
		delete bt_collision_configuration;
		delete bt_DebugDrawer;
	};

	// broadphase collision functions
	void initializeCollisionHandler();
	void setupCollisionPrimitives(double dissassemblyPhase = 0, 
		int flags = CM_EMCOMPONENTS | CM_SUPPORTS | CM_FASTNERS | CM_WALLS, int colFlags = 0x00);

	void clearCollisionPrimitives();
	void drawCollisionPrimitives();
	BaseObject* getFirstObjectHitByRay(const Ray& r);

	void processCollisions();
	void performCollisionDetection();

	void performCollisionDetectionForEntireAssemblyProcess();
	void highlightComponentsCollidingWith(AssemblableComponent* activeComponent);

	CollisionObject getContactPtsBetweenPair(btCollisionObject* obj1, btCollisionObject* obj2);
};

class EMComponentsCollisionObjects {
public:
	DynamicArray<btCollisionObject*> componentCollisionObjects;
	DynamicArray<btCollisionObject*> fastnersCollisionObjects;
	DynamicArray<btCollisionObject*> supportsCollisionObjects;
	DynamicArray<btCollisionObject*> sweptConvexHullCollisionObjects;

	void clear();

	void populate(EMComponent* emComponent);

	void addCollisionObjectsBetween(CollisionManager* currentManager, btCollisionObject* shape1, 
		btCollisionObject* shape2, DynamicArray<CollisionObject>& collisionObjects, int cmFlags = 0x00, bool checkBounds = false);

	void doCollisionsAgainst(CollisionManager* currentManager, btCollisionObject* shape, 
		DynamicArray<CollisionObject>& collisionObjects, int colFlags, int cmFlags = 0x00);
};

class GLDebugDrawer : public btIDebugDraw {
	int m_debugMode = DBG_DrawWireframe;

public:

	GLDebugDrawer() {}
	virtual ~GLDebugDrawer() {}

	virtual void drawLine(const btVector3& from, const btVector3& to, const btVector3&  fromColor, const btVector3& toColor) {
		glBegin(GL_LINES);
		glColor3f(fromColor.getX(), fromColor.getY(), fromColor.getZ());
		glVertex3d(from.getX(), from.getY(), from.getZ());
		glColor3f(toColor.getX(), toColor.getY(), toColor.getZ());
		glVertex3d(to.getX(), to.getY(), to.getZ());
		glEnd();
	}

	virtual void    drawLine(const btVector3& from, const btVector3& to, const btVector3& color) {
		glBegin(GL_LINES);
		glColor3f(color.getX(), color.getY(), color.getZ());
		glVertex3d(from.getX(), from.getY(), from.getZ());
		glVertex3d(to.getX(), to.getY(), to.getZ());
		glEnd();
	}

	virtual void drawContactPoint(const btVector3& PointOnB, const btVector3& normalOnB, btScalar distance, int lifeTime, const btVector3& color) {}

	virtual void reportErrorWarning(const char* warningString) {}

	virtual void draw3dText(const btVector3& location, const char* textString) {}

	virtual void    setDebugMode(int debugMode) { m_debugMode = debugMode; }

	virtual int     getDebugMode() const { return m_debugMode; }
};

