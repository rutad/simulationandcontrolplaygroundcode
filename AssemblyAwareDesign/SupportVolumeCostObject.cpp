#include "SupportVolumeCostObject.h"
#include "Support.h"

SupportVolumeCostObject::SupportVolumeCostObject(RigidlyAttachedSupport* supp) {
	this->support = supp;

	double sx, sy, sz;
	support->getSilhouetteScalingDimensions(sx, sy, sz);//size of support
	supportDefaultVol = abs(sx*sz*sy*8);

	weight = 10;//10;
}

double SupportVolumeCostObject::computeValue(const dVector& p) {

	double currentSupportVol = 0;
	double sx, sy, sz;
	support->getSilhouetteScalingDimensions(sx, sy, sz);//size of support
	currentSupportVol = abs(sx*sz*sy * 8);

	//Logger::consolePrint("supp size: %lf, %lf, %lf\n", sx, sy, sz);

	double errorVal = currentSupportVol - supportDefaultVol;

	if (errorVal < 0)
		errorVal = 0;
		//Logger::consolePrint("support vol is less than default! something is wrong with support of: %s%d, error val:%lf, supportVolD:%lf, currentSupportVal:%lf\n",
			//support->parent->name.c_str(), support->parent->disassemblyIndex, supportDefaultVol, currentSupportVol);

	description = "SupportVolumeObjective " + support->parent->name  + std::to_string(support->parent->disassemblyIndex);

	return errorVal;

}

