#include "ConstraintsManager.h"
#include "assembly.h"

ConstraintsManager::ConstraintsManager(Assembly* assembly) :
	minimizer(1)
{
	this->assembly = assembly;
	constraintsCollection = new ConstraintsCollection(assembly);
}

ConstraintsManager::~ConstraintsManager(void) {
	delete constraintsCollection;
}

//solves over all the parameters exposed by an assembly in order to satisfy the various geometric, collision, structural and assembalability constraints
double ConstraintsManager::fixConstraints(bool& converged, int maxIterations) {
	minimizer.maxIterations = maxIterations;
	minimizer.maxLineSearchIterations = 40; //40;
	minimizer.printOutput = constraintsCollection->printDebugInfo;
	minimizer.lineSearchStartValue = 0.0625; //0.015625;// 0.0625;
	assembly->setupConstraints();
	assembly->writeCurrentParametersToList(params);
	//Logger::printStatic("There are %d parameters\n", params.size());
	double val = 0;
	if (params.size() == 0)
		return 0;
	if(constraintsCollection->printDebugInfo)
		Logger::consolePrint("Problem size: %d\n", params.size());
	converged = minimizer.minimize(constraintsCollection, params, val);
	return val;
}

double ConstraintsCollection::computeValue(const dVector& p) {
	assembly->setParametersFromList(p);
	assembly->collisionManager->performCollisionDetectionForEntireAssemblyProcess();
	
	// added because we need to create constraints only after we have collision objects, 
	// and since we compute them at every step now, we need to set up constraints using them again..
	assembly->setupConstraints();

	double totalEnergy = 0;

	for (uint i = 0; i < softConstraints.size(); i++) {
		totalEnergy += softConstraints[i]->computeValue(p) * softConstraints[i]->weight;
	}

	//add the regularizer contribution
	if (regularizer > 0) {
		resize(tmpVec, p.size());
		if (m_p0.size() != p.size()) m_p0 = p;
		tmpVec = p - m_p0;
		totalEnergy += 0.5*regularizer*tmpVec.dot(tmpVec);
	}

	return totalEnergy;
}


//regularizer looks like: r/2 * (p-p0)'*(p-p0). This function can update p0 if desired, given the current value of s.
void ConstraintsCollection::updateRegularizingSolutionTo(const dVector &currentP) {
	m_p0 = currentP;
}

//this method gets called whenever a new best solution to the objective function is found
void ConstraintsCollection::setCurrentBestSolution(const dVector& p) {
	updateRegularizingSolutionTo(p);
	assembly->setParametersFromList(p);

	double totalVal = computeValue(p);

	if (printDebugInfo) {
		Logger::consolePrint("-------------------------------\n");

		for (uint i = 0; i < softConstraints.size(); i++) {
			double w = softConstraints[i]->weight;
			double v = softConstraints[i]->computeValue(p);
			if (v > 0)
				Logger::consolePrint("%s: %lf (weight: %lf, unweighted cost: %lf)\n", softConstraints[i]->description.c_str(), v * w, w, v);
		}
	}
	if(printProgress)
		Logger::consolePrint("=====> total cost: %lf\n", totalVal);
}

void ConstraintsManager::printNonZeroConstraints() {
	constraintsCollection->softConstraints.clear();
	dVector params;
	assembly->writeCurrentParametersToList(params);
	//assembly->setupConstraints();
	constraintsCollection->updateRegularizingSolutionTo(params);

	double totalVal = constraintsCollection->computeValue(params);

	Logger::consolePrint("\n");
	for (uint i = 0; i<constraintsCollection->softConstraints.size(); i++) {
		double w = constraintsCollection->softConstraints[i]->weight;
		double v = constraintsCollection->softConstraints[i]->computeValue(params);
		if (v>0)
			Logger::consolePrint("%s: %lf (weight: %lf, unweighted cost: %lf)\n", constraintsCollection->softConstraints[i]->description.c_str(), v * w, w, v);
	}

	Logger::consolePrint("=====> total cost: %lf\n", totalVal);
}
