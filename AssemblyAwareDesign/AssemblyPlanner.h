#pragma once

#include <random>
#include <MathLib/mathLib.h>
#include <MathLib/P3D.h>
#include <MathLib/V3D.h>
#include <MathLib/Quaternion.h>
#include "BaseObject.h"

#define SAMPLE_UNIFORM 0x01
#define SAMPLE_AUTOTUNEGAUSSIAN 0x02

// stores disassembly index and positions of each object in the assembly, along with constraint cost
class AssemblyState {
public:
	DynamicArray<std::tuple<int, P3D, Quaternion>> objStates;
	DynamicArray<std::tuple<double,double, V3D>> objAssemDir;
	DynamicArray<Plane> supportWallPlanes;
	DynamicArray<int> id;
	double constraintCost = -1;
	bool validAssembly = false; // stores whether assembly is collision free
	// to be used to convey special information about the state eg: local minima.
	bool indicator = false;

	AssemblyState(DynamicArray<std::tuple<int, P3D, Quaternion>> states, double cost, bool valid) {
		objStates = states;
		constraintCost = cost;
		validAssembly = valid;
	}

	AssemblyState() {};
	~AssemblyState() {};

	AssemblyState(Assembly* inputAssem);

	AssemblyState(const AssemblyState& other) {
		constraintCost = other.constraintCost;
		objStates = other.objStates;
		objAssemDir = other.objAssemDir;
		supportWallPlanes = other.supportWallPlanes;
		id = other.id;
		validAssembly = other.validAssembly;
	}

	void clear() {
		objStates.clear();
		objAssemDir.clear();
		supportWallPlanes.clear();
		id.clear();
		constraintCost = -1;
		validAssembly = false;
	}

	void appendComponentState(EMComponent* addComponent);
};

bool AssemblyStateCompare(const AssemblyState t1, const AssemblyState t2);

class AssemblyPlanner {
public:
	P3D globalWorldBounds = P3D(0, 0, 0);
	// input assembly 
	Assembly* inputAssembly = NULL;

	// plot window for debugging/experiments
	//PlotWindow* plotPlans = NULL;


	//constructor-destructor
	AssemblyPlanner(Assembly* assembly) {
		this->inputAssembly = assembly;
	}
	/*AssemblyPlanner(Assembly* assembly, PlotWindow* plot) {
		this->inputAssembly = assembly;
		this->plotPlans = plot;
	}*/
	virtual ~AssemblyPlanner() {};

	virtual void setUpPlanner(int seed = 0);

	// utility functions
	void setInitialRandomAssemblyOrder(Assembly* assem, int seed);
	void setInitialRandomAssemblyPlacement(Assembly* assem, int seed, DynamicArray<int> perturbComponentsID, bool perturbCompFlag = true, int sampleFlag = SAMPLE_UNIFORM);
	void setInitialRandomAssemblyDirections(Assembly* assem, int seed, DynamicArray<int> perturbComponentsID, bool perturbCompFlag = true, int sampleFlag = SAMPLE_UNIFORM);
	void setInitialRandomAssemblyOrientations(Assembly* assem, int seed, DynamicArray<int> perturbComponentsID, bool perturbCompFlag = true, int sampleFlag = SAMPLE_UNIFORM);
	double getAssemblyCost(Assembly* assembly);
	void copyAssemblyStateToInputAssembly(Assembly* inputAssem, const AssemblyState& storedAssemblyState);
	void copyAssemblyStateFromInputAssembly(Assembly* inputAssem, AssemblyState& copiedAssemState, double inputAssemCost, bool converged = false, bool valid = false);

	// resetting order based on current configuration
	void setTopDownAssemblyOrder(Assembly* assem);
	void setAssemblyOrderBasedOnConfiguration(Assembly* assem);
	void setAssemblyDirBasedOnConfiguration(Assembly* assem);

	// sampling functions
	void calculateGaussianPositionPerturbation(DynamicArray<std::normal_distribution<double>>& distributionForPositions,
		P3D meanPosition, double currentAcceptRate, double& bandwidth, int sampleType, bool updateFlag = false);
	void calculateGaussianAnglePerturbation(std::normal_distribution<double>& distributionForAngle,
		double meanAngle, double currentAcceptRate, double& bandwidth, int sampleType, bool updateFlag = false);
	void perturbAssemblyOrientation(int seed, Assembly* assemblyToPerturb, int sampleType, double currentAcceptRate, double& bandwidth, bool updateFlag = false);
	void perturbAssemblyPlacement(int seed, Assembly* assemblyToPerturb, int sampleType, double currentAcceptRate, double& bandwidth, bool updateFlag = false);
	void perturbAssemblyOrder(int seed, Assembly* assemblyToPerturb);
	void perturbAssemblyDirections(int seed, Assembly* assemblyToPerturb, int sampleFlag, double currentAcceptRate, double& bandwidth);
};