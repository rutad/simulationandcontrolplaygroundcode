#pragma once

#include <GUILib/GLMesh.h>
#include <MathLib/mathLib.h>
#include <MathLib/P3D.h>
#include <MathLib/V3D.h>
#include <MathLib/Quaternion.h>
#include <MathLib/Ray.h>
#include <GUILib/TranslateWidget.h>
#include <GUILib/RotateWidget.h>
#include "BaseObject.h"
#include "Chassis.h"

#include <OptimizationLib\ObjectiveFunction.h>
#include <BulletCollision/btBulletCollisionCommon.h>
#include "CollisionManager.h"
#include "CollisionObject.h"
#include "ConstraintsManager.h"
#include "Fastener.h"

class RigidlyAttachedSupport;

/*
	An assembly is a collection of objects and connections.
*/
class Assembly {
public:
	DynamicArray<EMComponent*> electroMechanicalComponents;
	Chassis* chassis = NULL;

	//int optimizationParametersFlag = OPTIMIZE_STATE_PARAMETERS | OPTIMIZE_MECHANICAL_PARAMETERS | OPTIMIZE_STRUCTURE_PARAMETERS;
	//int optimizationParametersFlag = OPTIMIZE_STATE_PARAMETERS | OPTIMIZE_MECHANICAL_PARAMETERS |OPTIMIZE_STRUCTURE_PARAMETERS | OPTIMIZE_ASSEMBLY_PARAMETERS;
	int optimizationParametersFlag = OPTIMIZE_STATE_PARAMETERS;// | OPTIMIZE_ASSEMBLY_PARAMETERS;

	CollisionManager* collisionManager = NULL;
	ConstraintsManager* constraintsManager = NULL;
	DynamicArray<double> paramList;
	DynamicArray<CollisionObject> collisionObjects;
	bool minSupportVolume = true;

public:
	// constructor
	Assembly() {
		constraintsManager = new ConstraintsManager(this);
		collisionManager = new CollisionManager(this);
		//chassis = new OwlChassis();
		//chassis = new WallEChassis();
		//chassis = new BirdieChassis();
		//chassis = new TrapezoidChassis();
		//chassis = new TrapezoidFlippedChassis();
		//chassis = new BalanceBotChassis();
		chassis = new VirtualChassis();
		//chassis = new HexagonChassis();
		//chassis = new BunnyChassis();
		//chassis = new TrainingChassis();
	}
	Assembly(Chassis* chas);

	// destructor
	~Assembly() {
		delete collisionManager;
		delete constraintsManager;
		delete chassis;

		while (!electroMechanicalComponents.empty()) {
			delete electroMechanicalComponents.back();
			electroMechanicalComponents.pop_back();
		}
	};

	// writes obj's current parameters to 'params' so that optimizer can use and optimize them.
	void writeCurrentParametersToList(dVector& params) {
		paramList.clear();
		for (uint i = 0; i < electroMechanicalComponents.size(); i++){
			electroMechanicalComponents[i]->setParameterStartIndex(paramList.size());
			electroMechanicalComponents.at(i)->pushParametersToList(paramList, optimizationParametersFlag);
		}
		resize(params, paramList.size());
		for (uint i = 0; i < paramList.size(); i++)
			params[i] = paramList[i];
	}

	// set obj's parameters from 'params'
	void setParametersFromList(const dVector& params) {
		paramList.clear();
		for (int i = 0; i < params.size(); i++)
			paramList.push_back(params[i]);

		for (uint i = 0; i < electroMechanicalComponents.size(); i++){
			int paramIndex = electroMechanicalComponents[i]->getParameterStartIndex();
			electroMechanicalComponents.at(i)->readParametersFromList(paramList, paramIndex, optimizationParametersFlag);
		}
	}

	void setupConstraints();

	DynamicArray<AssemblableComponent*> getSortedListOfAssemblableComponents() {
		DynamicArray<AssemblableComponent*> assemblableComponents;
		DynamicArray<Fastener*> fastenerObjs;

		//we assume that disassembly indices are correctly set...
		for (uint i = 0; i < electroMechanicalComponents.size(); i++) {
			for (uint j = 0; j < electroMechanicalComponents.size(); j++) {
				if (electroMechanicalComponents[j]->disassemblyIndex == i) {
					//now push fastners and other dependents...
					fastenerObjs.clear();
					electroMechanicalComponents[j]->addFastnersToList(fastenerObjs);
					for (size_t k = 0; k < fastenerObjs.size(); k++)
					{
						assemblableComponents.push_back(fastenerObjs[k]);
					}
					assemblableComponents.push_back(electroMechanicalComponents[j]);
				}
			}
		}
		return assemblableComponents;
	}

	DynamicArray<AssemblableComponent*> getSortedListReverse() {
		DynamicArray<AssemblableComponent*> assemblableComponents;
		DynamicArray<Fastener*> fastenerObjs;

		//we assume that disassembly indices are correctly set...
		for (uint i = electroMechanicalComponents.size(); i-- > 0;) {
			for (uint j = 0; j < electroMechanicalComponents.size(); j++) {
				if (electroMechanicalComponents[j]->disassemblyIndex == i) {
					//now push fastners and other dependents...
					assemblableComponents.push_back(electroMechanicalComponents[j]);
					fastenerObjs.clear();
					electroMechanicalComponents[j]->addFastnersToList(fastenerObjs);
					for (size_t k = 0; k < fastenerObjs.size(); k++)
					{
						assemblableComponents.push_back(fastenerObjs[k]);
					}
				}
			}
		}
		return assemblableComponents;
	}

	double calculateFillRatio();
	double calculateComponentFillRatio();
	double calculateSupportFillRatio();
	void randomizeAssemblyOrder();

	// support functions
	void updateSupportPosition(RigidlyAttachedSupport* support);
	void findClosestChassisWallToSupport(RigidlyAttachedSupport* support);
	void snapSupportsToWalls(bool updateNearestWall = true, bool updateSupportSize = true);
	Plane findClosestChassisWallToComponent(EMComponent* component);

	Assembly* clone();
	Assembly* clone(DynamicArray<EMComponent*> subAssemblyComponents);

	// loading and saving aseembly
	void saveAssembly();
	void saveAssembly(const char* fName);
	void loadAssembly(const char* fName);
	void saveCAD();

	bool checkCollisionFreeValidness(bool computeCollisionObjects = true);

	DynamicArray<EMComponent*> getSortedComponentsBasedOnVolume(DynamicArray<EMComponent*> inputArrayOfComponents);

	DynamicArray<EMComponent*> getListOfNonFixedComponents() {
		DynamicArray<EMComponent*>nonFixedComponents;
		for (size_t i = 0; i < electroMechanicalComponents.size(); i++)
		{
			if (!electroMechanicalComponents[i]->frozen)
				nonFixedComponents.push_back(electroMechanicalComponents[i]);
		}
		return nonFixedComponents;
	}

	DynamicArray<EMComponent*> getListOfFixedComponents() {
		DynamicArray<EMComponent*>fixedComponents;
		for (size_t i = 0; i < electroMechanicalComponents.size(); i++)
		{
			if (electroMechanicalComponents[i]->frozen)
				fixedComponents.push_back(electroMechanicalComponents[i]);
		}
		return fixedComponents;
	}

	//void copyAssemblyPropertiesFromSubAssembly(Assembly* subAssembly);
};
