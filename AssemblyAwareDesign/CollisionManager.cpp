#include "CollisionManager.h"
#include "BulletCollisionObject.h"
#include "assembly.h"
#include "CollisionObject.h"
#include "Support.h"
#include "Fastener.h"

struct btContactPairResult : public btCollisionWorld::ContactResultCallback
{
	CollisionObject col;

	virtual	btScalar	addSingleResult(btManifoldPoint& cp, const btCollisionObjectWrapper* colObj0Wrap, int partId0, int index0, const btCollisionObjectWrapper* colObj1Wrap, int partId1, int index1)
	{
		col.p_WorldOnO1 = getP3D(cp.getPositionWorldOnA());
		col.p_WorldOnO2 = getP3D(cp.getPositionWorldOnB());
		col.nWorld = getV3D(cp.m_normalWorldOnB);
		col.penetrationDepth = cp.getDistance();
		col.weight = 100;

		return 0;
	}
};

CollisionManager::CollisionManager(Assembly* inputAssembly) {
	this->assembly = inputAssembly;
}

void CollisionManager::initializeCollisionHandler() {
	delete bt_collision_world;
	delete bt_broadphase;
	delete bt_dispatcher;
	delete bt_collision_configuration;
	delete bt_DebugDrawer;

	double scene_size = 500;
	unsigned int max_objects = 16000;

	bt_collision_configuration = new btDefaultCollisionConfiguration();
	bt_dispatcher = new btCollisionDispatcher(bt_collision_configuration);

	btScalar sscene_size = (btScalar)scene_size;
	btVector3 worldAabbMin(-sscene_size, -sscene_size, -sscene_size);
	btVector3 worldAabbMax(sscene_size, sscene_size, sscene_size);
	//This is one type of broadphase, bullet has others that might be faster depending on the application
	//	bt_broadphase = new bt32BitAxisSweep3(worldAabbMin, worldAabbMax, max_objects, 0);  // true for disabling raycast accelerator
	bt_broadphase = new btDbvtBroadphase();
	bt_collision_world = new btCollisionWorld(bt_dispatcher, bt_broadphase, bt_collision_configuration);
	bt_DebugDrawer = new GLDebugDrawer();
	bt_collision_world->setDebugDrawer(bt_DebugDrawer);
}

void CollisionManager::clearCollisionPrimitives() {
	//empty up the list of collision objects
	if (bt_collision_world) {
		while (bt_collision_world->getCollisionObjectArray().size() > 0)
			bt_collision_world->removeCollisionObject(bt_collision_world->getCollisionObjectArray()[0]);

		//	for (uint i = 0; i < bulletCollisionPrimitives.size(); i++)
		//		delete bulletCollisionPrimitives[i];
		bulletCollisionPrimitives.clear();
	}
}

void CollisionManager::setupCollisionPrimitives(double disassemblyPhase, int flags, int colFlags) {
	boundToRange(&disassemblyPhase, 0, 1);
	if (bt_collision_world == NULL)
		initializeCollisionHandler();
	clearCollisionPrimitives();

	DynamicArray<AssemblableComponent*> assemblableComponents = assembly->getSortedListOfAssemblableComponents();
	double range = 1.0 / assemblableComponents.size();
	int activeObjIndex = (int)(disassemblyPhase / range);
	double activeObjPhase = (disassemblyPhase - activeObjIndex * range) / range;

	//create collision primitives for all the entities we care about...
	for (uint i = 0; i < assemblableComponents.size(); i++) {
		assemblableComponents[i]->collided = false;
		if (flags & CM_EMCOMPONENTS) {
			// add existing components, yet to be dis-assembled
			if ((int) i > activeObjIndex  && dynamic_cast<EMComponent*> (assemblableComponents[i]))
				assemblableComponents[i]->addBulletCollisionForObjectOnPath(bulletCollisionPrimitives, 0);
			else if ((int) i == activeObjIndex) {
				if (colFlags & COL_SWEPT_CVX_HULL && dynamic_cast<EMComponent*> (assemblableComponents[i]))
					assemblableComponents[i]->addSweptConvexHullObjectsToList(bulletCollisionPrimitives);
				else
					assemblableComponents[i]->addBulletCollisionForObjectOnPath(bulletCollisionPrimitives, activeObjPhase);
			}
//			else //may want to skip objects that are fully dissassembled...
//				assembly->electroMechanicalComponents[i]->addBulletCollisionForObjectOnPath(bulletCollisionPrimitives, 1);
		}
	}

	// supports
	DynamicArray<RigidlyAttachedSupport*> allSupports;
	for (size_t i = 0; i < assembly->electroMechanicalComponents.size(); i++) {
		assembly->electroMechanicalComponents[i]->addSupportsToList(allSupports);
	}
	for (size_t i = 0; i < allSupports.size(); i++)	{
		allSupports[i]->collided = false;
		allSupports[i]->addBulletCollisionObjectsToList(bulletCollisionPrimitives);
	}

	// add all collision objects to bullet world..
	for (uint i = 0; i < bulletCollisionPrimitives.size(); i++)
		bt_collision_world->addCollisionObject(bulletCollisionPrimitives[i]);
}

void CollisionManager::drawCollisionPrimitives() {
	if (bt_collision_world)
		bt_collision_world->debugDrawWorld();
}

BaseObject* CollisionManager::getFirstObjectHitByRay(const Ray& r) {
	btCollisionWorld::ClosestRayResultCallback rayCallback(bulletVector(r.origin), bulletVector(r.origin + r.direction * 1000));
	bt_collision_world->rayTest(bulletVector(r.origin), bulletVector(r.origin + r.direction * 1000), rayCallback);
	if (rayCallback.hasHit())
		return (BaseObject*)rayCallback.m_collisionObject->getUserPointer();
	
	return NULL;
}


/*
	Collect all the objects between whom we need to check collisions. Then run bullet on this collection of objects
*/
void CollisionManager::processCollisions() {
	assembly->collisionObjects.clear();

	//perform collision detection
	performCollisionDetection();
}

/*
This method performs broadphase collision detection in bullet world
*/
void CollisionManager::performCollisionDetection() {
	//Perform collision detection
	if (bt_collision_world) {
		Logger::consolePrint("in performing collisions... number of collision primitives: %d\n", bulletCollisionPrimitives.size());
		bt_collision_world->performDiscreteCollisionDetection();
		int totalNumberOfCollisions = 0;

		Logger::consolePrint("bullet done performing collisions\n");

		// go through the results and check for pairs of collisions...
		int numManifolds = bt_collision_world->getDispatcher()->getNumManifolds();
		//For each contact manifold
		for (int i = 0; i < numManifolds; i++) {
			btPersistentManifold* contactManifold = bt_collision_world->getDispatcher()->getManifoldByIndexInternal(i);
			btCollisionObject* obA = (btCollisionObject*)(contactManifold->getBody0());
			btCollisionObject* obB = (btCollisionObject*)(contactManifold->getBody1());
			contactManifold->refreshContactPoints(obA->getWorldTransform(), obB->getWorldTransform());
			int numContacts = contactManifold->getNumContacts();

			//For each contact point in that manifold
			for (int j = 0; j < numContacts; j++) {
				totalNumberOfCollisions++;
				btManifoldPoint& pt = contactManifold->getContactPoint(j);

				CollisionObject col;
				col.obj1 = (BaseObject*)obA->getUserPointer();
				col.obj2 = (BaseObject*)obB->getUserPointer();
				if (col.obj1 == col.obj2)
					break;
				if (!col.obj1->shouldCollideWith(col.obj2) || !col.obj2->shouldCollideWith(col.obj1)) 
					break;
				col.obj1->collided = true;
				col.obj2->collided = true;
				//the coordinate frame of the collision primitive may not be the same as the coordinate frame of the object - so go from the world coordinates of the point to the local coordinates in object space...
				col.p_WorldOnO1 = getP3D(pt.getPositionWorldOnA());
				col.p_WorldOnO2 = getP3D(pt.getPositionWorldOnB());
				col.nWorld = getV3D(pt.m_normalWorldOnB);
				col.penetrationDepth = pt.getDistance();
				col.weight = 100;

				assembly->collisionObjects.push_back(col);

				//Logger::consolePrint("we have a collision!!. Dist: %lf\n", col.penetrationDepth);
			}
		}
		//Logger::consolePrint("total number of collisions: %d\n", totalNumberOfCollisions);
		Logger::consolePrint("done performing collisions\n");
	}
}

CollisionObject gjkClosestPointsBetweenBulletCollisionObjs(btCollisionObject* obj1, btCollisionObject* obj2) {
	// gjk
	btVoronoiSimplexSolver sGjkSimplexSolver;
	btGjkEpaPenetrationDepthSolver epa;

	btConvexShape* obj1ShapePtr = dynamic_cast <btConvexShape*>(obj1->getCollisionShape());
	btTransform obj1Tr = obj1->getWorldTransform();
	btConvexShape* obj2ShapePtr = dynamic_cast <btConvexShape*>(obj2->getCollisionShape());
	btTransform obj2Tr = obj2->getWorldTransform();

	btGjkPairDetector convexConvex(obj1ShapePtr, obj2ShapePtr, &sGjkSimplexSolver, &epa);
	btGjkPairDetector::ClosestPointInput input;

	// find closest points and normal using bullet gjk algorithm and stores in gjkOutput.
	input.m_transformA = obj1Tr;
	input.m_transformB = obj2Tr;
	btPointCollector gjkOutput;

	convexConvex.getClosestPoints(input, gjkOutput, 0);

	CollisionObject col;
	col.obj1 = (BaseObject*)obj1->getUserPointer();
	col.obj2 = (BaseObject*)obj2->getUserPointer();

	col.p_WorldOnO1 = getP3D(gjkOutput.m_pointInWorld);
	col.p_WorldOnO2 = getP3D(gjkOutput.m_pointInWorld + gjkOutput.m_normalOnBInWorld * gjkOutput.m_distance);
	col.nWorld = getV3D(gjkOutput.m_normalOnBInWorld);
	col.penetrationDepth = gjkOutput.m_distance;
	col.weight = 100;
	if (gjkOutput.m_hasResult == false)
		col.weight = 0;
	return col;
}

CollisionObject CollisionManager::getContactPtsBetweenPair(btCollisionObject* obj1, btCollisionObject* obj2) {
	btContactPairResult contactCallback;
	bt_collision_world->contactPairTest(obj1, obj2, contactCallback);
	return contactCallback.col;
}

void CollisionManager::highlightComponentsCollidingWith(AssemblableComponent* activeComponent) {
	//first rest collided flags of all objects
	assembly->chassis->collided = false;
	for (size_t i = 0; i < assembly->electroMechanicalComponents.size(); i++)
	{
		assembly->electroMechanicalComponents[i]->collided = false;
	}

	//Logger::consolePrint("input active obj name: %s \n",activeComponent->name.c_str());

	BaseObject* checkComponent = activeComponent;
	if (dynamic_cast<Fastener*> (activeComponent)) {
		checkComponent = dynamic_cast<Fastener*> (activeComponent)->fastenerCollisionObj;
	}

	// now figure out which objects are colliding with object of interest..
	for (uint i = 0; i < assembly->collisionObjects.size(); i++) {
		if (assembly->collisionObjects[i].penetrationDepth < 0) {
			if (assembly->collisionObjects[i].obj1 == checkComponent) {
				if ((dynamic_cast<WallObject*>(assembly->collisionObjects[i].obj2))) {
					assembly->chassis->collided = true;
				}
				else {
					assembly->collisionObjects[i].obj2->collided = true;
					//Logger::consolePrint("active obj name: %s, colliding obj name:%s\n", checkComponent->name.c_str(), assembly->collisionObjects[i].obj2->name.c_str());
				}
			}
			else if (assembly->collisionObjects[i].obj2 == checkComponent) {
				if ((dynamic_cast<WallObject*>(assembly->collisionObjects[i].obj1)))
					assembly->chassis->collided = true;
				else {
					assembly->collisionObjects[i].obj1->collided = true;
					//Logger::consolePrint("active obj name: %s, colliding obj name:%s\n", checkComponent->name.c_str(),assembly->collisionObjects[i].obj1->name.c_str());
				}
			}
		}
	}

}

void CollisionManager::performCollisionDetectionForEntireAssemblyProcess() {
	if (bt_collision_world == NULL)
		initializeCollisionHandler();

	assembly->collisionObjects.clear();

	// update support sizes
	assembly->snapSupportsToWalls(false);

	DynamicArray<EMComponentsCollisionObjects> emcColObjects;
	DynamicArray<btCollisionObject*> wallObjs;

	//first of all, collect all the collision detection primitives for all EMComponents and walls...
	assembly->chassis->collided = false;
	DynamicArray<AssemblableComponent*> assemblableComponents = assembly->getSortedListReverse();
	for (size_t i = 0; i < assemblableComponents.size(); i++)
	{
		assemblableComponents[i]->collided = false;
		if (dynamic_cast<Fastener*> (assemblableComponents[i]))
			dynamic_cast<Fastener*> (assemblableComponents[i])->fastenerCollisionObj->collided = false;
	}

	for (uint i = 0; i < assembly->electroMechanicalComponents.size(); i++) {
		//assembly->electroMechanicalComponents[i]->collided = false;
		emcColObjects.push_back(EMComponentsCollisionObjects());
		emcColObjects[emcColObjects.size() - 1].populate(assembly->electroMechanicalComponents[i]);
	}
	for (size_t i = 0; i < assembly->chassis->walls.size(); i++){
		assembly->chassis->walls[i].addBulletCollisionObjectsToList(wallObjs);
	}

	//now, iterate through all the objects, and decide which col objects to check against each other...
	for (uint i = 0; i < assembly->electroMechanicalComponents.size(); i++) {
		for (uint j = 0; j < assembly->electroMechanicalComponents.size(); j++) {
			if (i == j) continue;
			//now, we have two cases to consider... object i is being disassembled, object j will either be disassembled later, or it has already been removed...
			bool objJStillInScene = assembly->electroMechanicalComponents[j]->disassemblyIndex > assembly->electroMechanicalComponents[i]->disassemblyIndex;

			//as we remove object i, we need to...
			
			//check its fastners, as they need to be removed first....
			for (uint k = 0; k < emcColObjects[i].fastnersCollisionObjects.size(); k++)
				emcColObjects[j].doCollisionsAgainst(this, emcColObjects[i].fastnersCollisionObjects[k], assembly->collisionObjects, objJStillInScene ? COL_SUPPORT_GEOM | COL_EMC_GEOM : COL_SUPPORT_GEOM);

			//now check swept object of i against EMComponents still in scene, and all supports...
			for (uint k = 0; k < emcColObjects[i].sweptConvexHullCollisionObjects.size();k++)
				emcColObjects[j].doCollisionsAgainst(this, emcColObjects[i].sweptConvexHullCollisionObjects[k], assembly->collisionObjects, objJStillInScene ? COL_SUPPORT_GEOM | COL_EMC_GEOM : COL_SUPPORT_GEOM);
		}
		//check the swept object against all the walls...
		for (uint j = 0; j < wallObjs.size(); j++) {
			if (assembly->chassis->isWallReal[j]) {
				emcColObjects[i].doCollisionsAgainst(this, wallObjs[j], assembly->collisionObjects, COL_SWEPT_CVX_HULL, CM_WALLS);
				emcColObjects[i].doCollisionsAgainst(this, wallObjs[j], assembly->collisionObjects, COL_SWEPT_FASTNERS, CM_WALLS);
			}
			//else
				emcColObjects[i].doCollisionsAgainst(this, wallObjs[j], assembly->collisionObjects, COL_EMC_GEOM, CM_WALLS);
		}
	}

	//now clear all the collision primitives...
	for (uint i = 0; i < assembly->electroMechanicalComponents.size(); i++) {
		emcColObjects[i].clear();
	}

//	for (uint i = 0; i <wallObjs.size(); i++)
//		delete wallObjs[i];

}

void EMComponentsCollisionObjects::clear() {
//	for (uint i = 0; i < componentCollisionObjects.size(); i++)
//		delete componentCollisionObjects[i];
	componentCollisionObjects.clear();
//	for (uint i = 0; i < fastnersCollisionObjects.size(); i++)
//		delete fastnersCollisionObjects[i];
	fastnersCollisionObjects.clear();
//	for (uint i = 0; i < supportsCollisionObjects.size(); i++)
//		delete supportsCollisionObjects[i];
	supportsCollisionObjects.clear();
//	for (uint i = 0; i < sweptConvexHullCollisionObjects.size(); i++)
//		delete sweptConvexHullCollisionObjects[i];
	sweptConvexHullCollisionObjects.clear();
}

void EMComponentsCollisionObjects::populate(EMComponent* emComponent) {
	emComponent->addBulletCollisionObjectsToList(componentCollisionObjects);

	// fasteners
	DynamicArray<Fastener*> componentFasteners;
	emComponent->addFastnersToList(componentFasteners);
	for (size_t i = 0; i < componentFasteners.size(); i++)
	{
		componentFasteners[i]->addBulletCollisionObjectsToList(fastnersCollisionObjects);
	}

	// supports
	DynamicArray<RigidlyAttachedSupport*> componentSupports;
	emComponent->addSupportsToList(componentSupports);
	for (size_t i = 0; i < componentSupports.size(); i++) {
		componentSupports[i]->collided = false;
		componentSupports[i]->addBulletCollisionObjectsToList(supportsCollisionObjects);
	}

	emComponent->addSweptConvexHullObjectsToList(sweptConvexHullCollisionObjects);
}

void EMComponentsCollisionObjects::addCollisionObjectsBetween(CollisionManager* currentManager, btCollisionObject* shape1, 
	btCollisionObject* shape2, DynamicArray<CollisionObject>& collisionObjects, int flags, bool checkBounds) {
	if (!shape1 || !shape2)
		return;

	if (flags & CM_WALLS) {
		CollisionObject newColObj = currentManager->getContactPtsBetweenPair(shape1, shape2);
		newColObj.obj1 = (BaseObject*)shape1->getUserPointer();
		newColObj.obj2 = (BaseObject*)shape2->getUserPointer();

		if (checkBounds) {
			// check if point is out of bounds for the wall..
			if (dynamic_cast<WallObject*>(newColObj.obj1)) {
				if (!dynamic_cast<WallObject*>(newColObj.obj1)->checkPointOutOfBounds(newColObj.p_WorldOnO1))
					collisionObjects.push_back(newColObj);
			}
			else {
				if (!dynamic_cast<WallObject*>(newColObj.obj2)->checkPointOutOfBounds(newColObj.p_WorldOnO2))
					collisionObjects.push_back(newColObj);
			}
		}
		else
			collisionObjects.push_back(newColObj);
	}
	else {
		collisionObjects.push_back(gjkClosestPointsBetweenBulletCollisionObjs(shape1, shape2));
	}
}

void EMComponentsCollisionObjects::doCollisionsAgainst(CollisionManager* currentManager, btCollisionObject* shape,
	DynamicArray<CollisionObject>& collisionObjects, int colFlags, int cmFlags) {
	if (colFlags & COL_EMC_GEOM)
		for (uint i = 0; i < componentCollisionObjects.size(); i++)
			addCollisionObjectsBetween(currentManager, shape, componentCollisionObjects[i], collisionObjects, cmFlags);
	if (colFlags & COL_SWEPT_FASTNERS)
		for (uint i = 0; i < fastnersCollisionObjects.size(); i++)
			addCollisionObjectsBetween(currentManager, shape, fastnersCollisionObjects[i], collisionObjects, cmFlags, true);
	if (colFlags & COL_SUPPORT_GEOM)
		for (uint i = 0; i < supportsCollisionObjects.size(); i++)
			addCollisionObjectsBetween(currentManager, shape, supportsCollisionObjects[i], collisionObjects, cmFlags);
	if (colFlags & COL_SWEPT_CVX_HULL) {
		uint i = 0;
		if (sweptConvexHullCollisionObjects.size() > 1)
			i = 1;
		for (i; i < sweptConvexHullCollisionObjects.size(); i++)
			addCollisionObjectsBetween(currentManager, shape, sweptConvexHullCollisionObjects[i], collisionObjects, cmFlags, true);
	}
}
