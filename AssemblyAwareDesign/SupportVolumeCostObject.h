#pragma once

#include <GUILib/GLMesh.h>
#include <MathLib/mathLib.h>
#include <MathLib/P3D.h>
#include <MathLib/V3D.h>
#include <MathLib/Quaternion.h>
#include <MathLib/Ray.h>

#include <OptimizationLib\ObjectiveFunction.h>
#include "BaseObject.h"


class RigidlyAttachedSupport;


class SupportVolumeCostObject : public ObjectiveFunction {
public:

	// support of interest whose volume we want to minimize
	RigidlyAttachedSupport *support = NULL;

	// default min volume of the support
	double supportDefaultVol = 0;

	SupportVolumeCostObject(RigidlyAttachedSupport* sup);
	~SupportVolumeCostObject() {}

	double computeValue(const dVector& p);
};