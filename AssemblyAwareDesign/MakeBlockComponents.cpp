#include "MakeBlockComponents.h"
#include "AssemblyUtils.h"
#include <GUILib/GLUtils.h>
#include <iostream>
#include <GUILib/GLContentManager.h>
#include "BulletCollisionObject.h"

/**
Default constructor
*/
MakeBlockComponents::MakeBlockComponents() : EMComponent() {
	name = "MakeBlockComponent";
}


void MakeBlockComponents::drawObjectGeometry(GLShaderMaterial* material) {
	GLShaderMaterial tmpMat;
	GLShaderMaterial *matToUse = (material) ? material : &tmpMat;

	if (material == NULL) {
		tmpMat.r = 0.25; tmpMat.g = 0.28; tmpMat.b = 0.67; tmpMat.a = 0.7;
	}

	for (uint i = 0; i < componentMeshes.size();i++){
		GLShaderMaterial originalMaterial = componentMeshes[i]->getMaterial();
		if (material)
			componentMeshes[i]->setMaterial(*material);
		componentMeshes[i]->drawMesh();
		if (material)
			componentMeshes[i]->setMaterial(originalMaterial);
	}
}

void MakeBlockComponents::initGeometry() {
	disassemblyPath = new ArbitraryDirectionDissassemblyPath(this);
	DynamicArray<P3D> colShapePoints;
	//this will work for any union of box collision shapes (or other shapes that we can access the vertices of)
	for (size_t i = 0; i < colShapes.size(); i++)
	{
		addBTColShapeVerticesToPointList(colShapes[i], colShapePoints, colShapeGeometryCenterLocalOffset[i]);
	}

	DynamicArray<ConvexHull_Face> faces;
	ConvexHull3D::computeConvexHullFromSetOfPoints(colShapePoints, objectCoordsConvexHullVertices, faces);
}

void MakeBlockComponents::writeToFile(FILE* fp) {

	char* str;

	fprintf(fp, "%s\n", getASString(OBJ_OBJ));

	EMComponent::writeBasePropertiesToFile(fp);

	str = getASString(OBJ_END_OBJ);
	fprintf(fp, "%s\n\n\n", str);
}

void MakeBlockComponents::loadFromFile(FILE* fp) {
	if (fp == NULL)
		throwError("Invalid file pointer.");

	// read base object properties and go to the file part where object specific properties start
	readBasePropertiesFromFile(fp);

	//have a temporary buffer used to read the file line by line...
	char buffer[200];

	//this is where it happens.
	while (!feof(fp)) {
		//get a line from the file...
		readValidLine(buffer, 200, fp);
		char *line = lTrim(buffer);
		int lineType = getASLineType(line);
		switch (lineType) {

		case AS_NOT_IMPORTANT:
			if (strlen(line) != 0 && line[0] != '#')
				Logger::consolePrint("Ignoring input line: \'%s\'\n", line);
			break;

		case OBJ_END_OBJ:
			return;//and... done
			break;
		default:
			throwError("Incorrect assembly input file: \'%s\' - unexpected line.", buffer);
		}
	}
	throwError("Incorrect assembly input file! No /End found");
}
