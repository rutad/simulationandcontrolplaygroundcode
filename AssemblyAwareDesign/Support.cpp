#include "Support.h"


void RigidlyAttachedSupport::draw() {
	checkOGLErrors();
	glClearStencil(0);
	glClear(GL_STENCIL_BUFFER_BIT);
	glEnable(GL_STENCIL_TEST);

	glStencilFunc(GL_ALWAYS, 1, -1);
	glStencilOp(GL_KEEP, GL_KEEP, GL_REPLACE);

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	if (globalCoordinatesWallExtensionMesh.getVertexCount() <= 0) {
		setupGLTransformationMatrices();
	}

	// render the model to indicate places where the silhouette should not appear
	drawObjectGeometry();

	glStencilFunc(GL_NOTEQUAL, 1, -1);
	glStencilOp(GL_KEEP, GL_KEEP, GL_REPLACE);

	if (collided)
		drawSilhouette(0.002, 1, 0, 0);

	glDisable(GL_STENCIL_TEST);

	// render the normal model
	drawObjectGeometry();
	glPopMatrix();
}

void RigidlyAttachedSupport::drawSilhouette(double scale, double r, double g, double b) {
	//could first draw the original object in stencil buffer, and then draw silhouete where the stencil wasn't... to get rid of internal lines...

	// render the silhouette of the object...
	//	glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	glEnable(GL_CULL_FACE); // enable culling
	glCullFace(GL_FRONT); // enable culling of front faces
	GLShaderMaterial tmpMat;
	tmpMat.setShaderProgram(GLContentManager::getShaderProgram("silhouette"));
	tmpMat.setFloatParam("u_offset1", 0.0f);
	tmpMat.setFloatParam("u_color1", (float)r, (float)g, (float)b);
	double bbX, bbY, bbZ;
	getSilhouetteScalingDimensions(bbX, bbY, bbZ);
	double maxDim = MAX(bbX, bbY); maxDim = MAX(maxDim, bbZ);
	glPushMatrix();

	glTranslated(geoMeshCenter[0], geoMeshCenter[1], geoMeshCenter[2]);
	glScaled((bbX / maxDim + scale / maxDim) / (bbX / maxDim), (bbY / maxDim + scale / maxDim) / (bbY / maxDim), (bbZ / maxDim + scale / maxDim) / (bbZ / maxDim));
	glTranslated(-geoMeshCenter[0], -geoMeshCenter[1], -geoMeshCenter[2]);
	drawObjectGeometry(&tmpMat);
	glPopMatrix();
	glDisable(GL_CULL_FACE);
}