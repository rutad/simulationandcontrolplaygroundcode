#include "AssemblyPlanner.h"
#include "assembly.h"
#include "Support.h"
#include "MotorComponents.h"
#include <MathLib/Segment.h>

AssemblyState::AssemblyState(Assembly* inputAssem) {

	DynamicArray<RigidlyAttachedSupport*> assemSupports;
	for (uint k = 0; k < inputAssem->electroMechanicalComponents.size(); k++) {
		objStates.push_back(std::tuple<int, P3D, Quaternion>(inputAssem->electroMechanicalComponents[k]->disassemblyIndex,
			inputAssem->electroMechanicalComponents[k]->getPosition(), inputAssem->electroMechanicalComponents[k]->getOrientation()));
		
		inputAssem->electroMechanicalComponents[k]->addSupportsToList(assemSupports);

		ArbitraryDirectionDissassemblyPath* path = dynamic_cast<ArbitraryDirectionDissassemblyPath*>(inputAssem->electroMechanicalComponents[k]->
			disassemblyPath);
		if (path)
			objAssemDir.push_back(std::tuple<double,double, V3D>(path->assemblyAngleTheta, path->assemblyAnglePhi, path->disassemblyDefaultPath));
		else
			objAssemDir.push_back(std::tuple<double, double, V3D>(0,0, V3D(0,0,0)));
	}

	// support information
	for (uint m = 0; m < assemSupports.size(); m++) {
		supportWallPlanes.push_back(assemSupports[m]->closestWallPlane);
		id.push_back(assemSupports[m]->closestWallId);
	}
}

void AssemblyState::appendComponentState(EMComponent* addComponent) {
	DynamicArray<RigidlyAttachedSupport*> assemSupports;

	objStates.push_back(std::tuple<int, P3D, Quaternion>(addComponent->disassemblyIndex,
		addComponent->getPosition(), addComponent->getOrientation()));
	addComponent->addSupportsToList(assemSupports);

	ArbitraryDirectionDissassemblyPath* path = dynamic_cast<ArbitraryDirectionDissassemblyPath*>(addComponent->disassemblyPath);
	if (path)
		objAssemDir.push_back(std::tuple<double, double, V3D>(path->assemblyAngleTheta, path->assemblyAnglePhi, path->disassemblyDefaultPath));
	else
		objAssemDir.push_back(std::tuple<double, double, V3D>(0, 0, V3D(0, 0, 0)));

	// support information
	for (uint m = 0; m < assemSupports.size(); m++) {
		supportWallPlanes.push_back(assemSupports[m]->closestWallPlane);
		id.push_back(assemSupports[m]->closestWallId);
	}

	this->constraintCost = -1;
	this->validAssembly = false;
}

bool AssemblyStateCompare(const AssemblyState t1, const AssemblyState t2)
{
	return t1.constraintCost < t2.constraintCost;
}

void AssemblyPlanner::setUpPlanner(int seed) {
	P3D localWorldBounds = P3D(0, 0, 0);
	if (inputAssembly->chassis) {
		inputAssembly->chassis->getSilhouetteScalingDimensions(localWorldBounds[0],
			localWorldBounds[1], localWorldBounds[2]);

		// world bounds for random obj position (0.005 is wall thickness). 
		// This decides the max and mean position sample values..
		globalWorldBounds = inputAssembly->chassis->getWorldCoordinates(localWorldBounds + P3D(-0.005, -0.005, -0.005));
	}
	else
		Logger::consolePrint("Planner needs a chassis or an outer bounding box for all components\n");
}

double AssemblyPlanner::getAssemblyCost(Assembly* assembly) {

	// find nearest wall for each support
	assembly->snapSupportsToWalls(true,false);

	//clear old constraints
	assembly->constraintsManager->constraintsCollection->softConstraints.clear();

	// calculate constraint costs
	dVector params;
	assembly->writeCurrentParametersToList(params);
	assembly->constraintsManager->constraintsCollection->updateRegularizingSolutionTo(params);
	//assembly->setParametersFromList(params);
	//assembly->collisionManager->performCollisionDetectionForEntireAssemblyProcess();
	//assembly->setupConstraints();

	// computeValue sets up constraints after calculating collision objects..
	//Logger::consolePrint(" ---- ---- IN COST CALCULATE-------\n");
	//Logger::logPrint(" ---- ---- IN COST CALCULATE-------\n");
	double cost = assembly->constraintsManager->constraintsCollection->computeValue(params);
	//Logger::consolePrint(" ---- IN PRINT CNSTRAINTS:%lf-------", cost);
	//Logger::logPrint(" ---- IN PRINT CNSTRAINTS:%lf-------\n", cost);
	//assembly->constraintsManager->printNonZeroConstraints();
	return cost;
}

void AssemblyPlanner::setInitialRandomAssemblyPlacement(Assembly* assem, int seed, DynamicArray<int> perturbComponentsID, bool perturbCompFlag, int sampleFlag) {

	std::default_random_engine re(seed);

	if (sampleFlag & SAMPLE_UNIFORM) {
		// generate an initial random assembly state
		std::uniform_real_distribution<double >  unifx(-globalWorldBounds[0], globalWorldBounds[0]);
		std::uniform_real_distribution<double >  unify(-globalWorldBounds[1], globalWorldBounds[1]);
		std::uniform_real_distribution<double >  unifz(-globalWorldBounds[2], globalWorldBounds[2]);

		for (uint j = 0; j < assem->electroMechanicalComponents.size(); j++) {

			bool perturbThisComponent = false;
			if (perturbComponentsID.size() > 0 && perturbCompFlag) {
				for (size_t i = 0; i < perturbComponentsID.size(); i++)
				{
					if(j == perturbComponentsID[i]){
						perturbThisComponent = true;
						break;
					}
				}
			}
			else {
				perturbThisComponent = true;
			}

			if (perturbThisComponent) {
				// random position
				P3D newPos = P3D(unifx(re), unify(re), unifz(re));
				bool truncated;
				do {
					truncated = assem->electroMechanicalComponents[j]->getValidRandomPos(newPos,
						globalWorldBounds);
				} while (truncated);
				assem->electroMechanicalComponents[j]->setPosition(newPos);
			}
		}
	}
	else {
		std::normal_distribution<double> normalDistributionX, normalDistributionY, normalDistributionZ;
		normalDistributionX = std::normal_distribution<double>(0, globalWorldBounds[0] / 3);
		normalDistributionY = std::normal_distribution<double>(0, globalWorldBounds[1] / 3);
		normalDistributionZ = std::normal_distribution<double>(0, globalWorldBounds[2] / 3);

		for (uint j = 0; j < assem->electroMechanicalComponents.size(); j++) {

			bool perturbThisComponent = false;
			if (perturbComponentsID.size() > 0) {
				for (size_t i = 0; i < perturbComponentsID.size(); i++)
				{
					if (j == perturbComponentsID[i]) {
						perturbThisComponent = true;
						break;
					}
				}
			}
			else {
				perturbThisComponent = true;
			}

			if (perturbThisComponent) {
				// random position
				P3D newPos = P3D(normalDistributionX(re), normalDistributionY(re), normalDistributionZ(re));
				bool truncated;
				do {
					truncated = assem->electroMechanicalComponents[j]->getValidRandomPos(newPos,
						globalWorldBounds);
				} while (truncated);

				assem->electroMechanicalComponents[j]->setPosition(newPos);
			}
		}
	}
}

void AssemblyPlanner::setInitialRandomAssemblyOrder(Assembly* assem, int seed) {

	std::srand(seed);
	assem->randomizeAssemblyOrder();
}

void AssemblyPlanner::setInitialRandomAssemblyOrientations(Assembly* assem, int seed, DynamicArray<int> perturbComponentsID, bool perturbCompFlag, int sampleFlag) {
	// EXTEND TO SAMPLING FROM GAUSSIAN DISTRIBUTION IF NEEDED..
	std::default_random_engine re(seed);

	if (sampleFlag & SAMPLE_UNIFORM) {
		/***UNIFORM PERTURBATION TO ANGLES***/
		// For each obj, create a random rot by sampling uniform angle distribution (step size: p1/2 or pi/4)
		std::uniform_int_distribution<int> distributionForAngle(-1, 1); //(-2,2); (-1,1);		
		//std::uniform_real_distribution<double> distributionForAngle(-1,1);
		for (uint j = 0; j < assem->electroMechanicalComponents.size(); j++) {

			bool perturbThisComponent = false;
			if (perturbComponentsID.size() > 0 && perturbCompFlag) {
				for (size_t i = 0; i < perturbComponentsID.size(); i++)
				{
					if (j == perturbComponentsID[i]) {
						perturbThisComponent = true;
						break;
					}
				}
			}
			else {
				perturbThisComponent = true;
			}

			if (perturbThisComponent) {
				//sample a random point from the valid distribution
				double randAngle = distributionForAngle(re)*PI / 2.0; //*PI/2; *PI/4;
				//see if its valid based on object state (frozen or not) and maximum allowable values. 
				assem->electroMechanicalComponents[j]->getValidRandomAngle(randAngle);
				// set the valid random angle
				assem->electroMechanicalComponents[j]->additionalRotAngle = randAngle;
			}
		}
	}
}

void AssemblyPlanner::setInitialRandomAssemblyDirections(Assembly* assem, int seed, DynamicArray<int> perturbComponentsID, bool perturbCompFlag, int sampleFlag) {
	// EXTEND TO SAMPLING FROM GAUSSIAN DISTRIBUTION IF NEEDED..
	std::default_random_engine re(seed);
	if (sampleFlag & SAMPLE_UNIFORM) {
		/***SAMPLING FROM DISCRETE ASSEMBLY DIRECTIONS***/
		DynamicArray<V3D> validAxis = assem->chassis->validAssemblyDirs;
		//DynamicArray<std::pair<double, double>> validAxis = assem->chassis->validAssemblyDirs;

		std::uniform_int_distribution<int> distributionForAxis(0, size(validAxis) - 1);

		for (uint j = 0; j < assem->electroMechanicalComponents.size(); j++) {

			bool perturbThisComponent = false;
			if (perturbComponentsID.size() > 0 && perturbCompFlag) {
				for (size_t i = 0; i < perturbComponentsID.size(); i++)
				{
					if (j == perturbComponentsID[i]) {
						perturbThisComponent = true;
						break;
					}
				}
			}
			else {
				perturbThisComponent = true;
			}

			if (perturbThisComponent) {
				int axisInd = distributionForAxis(re);
				if (dynamic_cast<ServoMotor*>(assem->electroMechanicalComponents[j])
					|| dynamic_cast<MicroServoMotor*>(assem->electroMechanicalComponents[j]))
					continue;
				else {
					if (dynamic_cast<ArbitraryDirectionDissassemblyPath*>(assem->electroMechanicalComponents[j]->disassemblyPath)) {
						dynamic_cast<ArbitraryDirectionDissassemblyPath*>(assem->electroMechanicalComponents[j]->disassemblyPath)->disassemblyDefaultPath = validAxis[axisInd];
						if (dynamic_cast<ArbitraryDirectionDissassemblyPath*>(assem->electroMechanicalComponents[j]->disassemblyPath)->disassemblyDefaultPath != V3D(0, 1, 0))
							dynamic_cast<ArbitraryDirectionDissassemblyPath*>(assem->electroMechanicalComponents[j]->disassemblyPath)->resetInternalAxis();
						//dynamic_cast<ArbitraryDirectionDissassemblyPath*>(assem->electroMechanicalComponents[j]->disassemblyPath)->assemblyAngleTheta = validAxis[axisInd].first;
						//dynamic_cast<ArbitraryDirectionDissassemblyPath*>(assem->electroMechanicalComponents[j]->disassemblyPath)->assemblyAnglePhi = validAxis[axisInd].second;
					}
				}
			}
		}
	}
}

void AssemblyPlanner::copyAssemblyStateToInputAssembly(Assembly* inputAssem, const AssemblyState& storedAssemblyState) {
	DynamicArray<RigidlyAttachedSupport*> assemSupports;
	for (uint i = 0; i < storedAssemblyState.objStates.size(); i++) {
		inputAssem->electroMechanicalComponents[i]->setPosition(std::get<1>(storedAssemblyState.objStates[i]));
		inputAssem->electroMechanicalComponents[i]->setOrientation(std::get<2>(storedAssemblyState.objStates[i]));
		inputAssem->electroMechanicalComponents[i]->disassemblyIndex = std::get<0>(storedAssemblyState.objStates[i]);

		ArbitraryDirectionDissassemblyPath* path = dynamic_cast<ArbitraryDirectionDissassemblyPath*>(inputAssem->electroMechanicalComponents[i]->
			disassemblyPath);
		if (path) {
			path->assemblyAngleTheta = std::get<0>(storedAssemblyState.objAssemDir[i]);
			path->assemblyAnglePhi = std::get<1>(storedAssemblyState.objAssemDir[i]);
			path->disassemblyDefaultPath = std::get<2>(storedAssemblyState.objAssemDir[i]);
			if(path->disassemblyDefaultPath !=V3D(0,1,0))
				path->resetInternalAxis();
		}
		inputAssem->electroMechanicalComponents[i]->addSupportsToList(assemSupports);
	}

	// support information
	for (uint m = 0; m < assemSupports.size(); m++) {
		assemSupports[m]->closestWallPlane = storedAssemblyState.supportWallPlanes[m];
		assemSupports[m]->closestWallId = storedAssemblyState.id[m];
		assemSupports[m]->updateSupportSize();
	}
}

void AssemblyPlanner::copyAssemblyStateFromInputAssembly(Assembly* inputAssem, AssemblyState& copiedAssemState, 
	double inputAssemCost, bool converged, bool valid) {
	copiedAssemState.clear();

	DynamicArray<RigidlyAttachedSupport*> assemSupports;

	// copy object positions, orientations and disassembly index
	for (uint k = 0; k < inputAssem->electroMechanicalComponents.size(); k++) {
		copiedAssemState.objStates.push_back(std::tuple<int, P3D, Quaternion>(inputAssem->electroMechanicalComponents[k]->disassemblyIndex,
			inputAssem->electroMechanicalComponents[k]->getPosition(), inputAssem->electroMechanicalComponents[k]->getOrientation()));

		ArbitraryDirectionDissassemblyPath* path = dynamic_cast<ArbitraryDirectionDissassemblyPath*>(inputAssem->electroMechanicalComponents[k]->
			disassemblyPath);
		if (path)
			copiedAssemState.objAssemDir.push_back(std::tuple<double, double, V3D>(path->assemblyAngleTheta, path->assemblyAnglePhi, path->disassemblyDefaultPath));
		else
			copiedAssemState.objAssemDir.push_back(std::tuple<double, double, V3D>(0, 0, V3D(0, 0, 0)));

		inputAssem->electroMechanicalComponents[k]->addSupportsToList(assemSupports);
	}


	// copy support information
	for (uint m = 0; m < assemSupports.size(); m++) {
		copiedAssemState.supportWallPlanes.push_back(assemSupports[m]->closestWallPlane);
		copiedAssemState.id.push_back(assemSupports[m]->closestWallId);
	}

	copiedAssemState.constraintCost = inputAssemCost;
	copiedAssemState.indicator = converged;
	copiedAssemState.validAssembly = valid;
}

void AssemblyPlanner::perturbAssemblyOrientation(int seed, Assembly* assemblyToPerturb, int sampleType,
	double currentAcceptRate, double& bandwidth, bool updateFlag) {

	std::default_random_engine re(seed);

	if (sampleType & SAMPLE_UNIFORM) {
		/***UNIFORM PERTURBATION TO ANGLES***/
		// For each obj, create a random rot by sampling uniform angle distribution (step size: p1/2 or pi/4)
		std::uniform_int_distribution<int> distributionForAngle(-1, 1); //(-2,2); (-1,1);																		
		for (uint j = 0; j < assemblyToPerturb->electroMechanicalComponents.size(); j++) {
			//sample a random point from the valid distribution
			double randAngle = distributionForAngle(re)*PI / 2; //*PI/2; *PI/4;
			//see if its valid based on object state (frozen or not) and maximum allowable values. 
			assemblyToPerturb->electroMechanicalComponents[j]->getValidRandomAngle(randAngle);
			// set the valid random angle
			assemblyToPerturb->electroMechanicalComponents[j]->additionalRotAngle = randAngle;
		}
	}
	else {
		///***GAUSSIAN PERTURBATION TO ANGLES***/
		std::normal_distribution<double> distributionForAngle;
		// For each obj, create a random obj orientation by adding Gaussian/normal perturbation
		// defined by distributionForAngle to the current obj orientation.
		for (uint j = 0; j < assemblyToPerturb->electroMechanicalComponents.size(); j++) {
			// get valid perturbation distribution (Gaussian) for random angles
			calculateGaussianAnglePerturbation(distributionForAngle, assemblyToPerturb->electroMechanicalComponents[j]->additionalRotAngle,
				currentAcceptRate, bandwidth, sampleType, updateFlag);
			bool truncatedRot;
			double randAngle;
			int count = 0;
			do {
				// sample a random point from the valid distribution
				randAngle = distributionForAngle(re);
				// see if its valid based on object state (frozen or not) and maximum allowable values. 
				truncatedRot = assemblyToPerturb->electroMechanicalComponents[j]->getValidRandomAngle(randAngle);
				count = count + 1;
				if (count > 10)
					break;
			} while (truncatedRot); // if the random angles are out of bound and truncated, re sample.

			// set the random orientation
			assemblyToPerturb->electroMechanicalComponents[j]->additionalRotAngle = randAngle;
		}
	}
}

void AssemblyPlanner::perturbAssemblyPlacement(int seed, Assembly* assemblyToPerturb, int sampleFlag,
	double currentAcceptRate, double& bandwidth, bool updateFlag) {

	std::default_random_engine re(seed);
	DynamicArray<std::normal_distribution<double>> distributionForPositions;

	/***GAUSSIAN PERTURBATION TO POSITIONS***/
	// For each obj, create a random obj state by adding Gaussian/normal perturbation
	// defined by distributionForPositions to the current obj state.
	for (uint j = 0; j < assemblyToPerturb->electroMechanicalComponents.size(); j++) {
		// get valid perturbation distribution (Gaussian) around the current position of object
		calculateGaussianPositionPerturbation(distributionForPositions, assemblyToPerturb->electroMechanicalComponents[j]->getPosition(),
				currentAcceptRate, bandwidth, sampleFlag, updateFlag);
		
		bool truncated;
		P3D randPos;
		int count = 0;
		do {
			// sample a random point from the valid distribution
			randPos = P3D((distributionForPositions[0])(re), (distributionForPositions[1])(re), (distributionForPositions[2])(re));
			// see if its valid based on object state (frozen or not) and maximum allowable values. 
			truncated = assemblyToPerturb->electroMechanicalComponents[j]->getValidRandomPos(randPos, globalWorldBounds);
			count = count + 1;
			if (count > 10) {
				//std::uniform_real_distribution<double >  unifx(-globalWorldBounds[0], globalWorldBounds[0]);
				//std::uniform_real_distribution<double >  unify(-globalWorldBounds[1], globalWorldBounds[1]);
				//std::uniform_real_distribution<double >  unifz(-globalWorldBounds[2], globalWorldBounds[2]);
				//randPos = P3D(unifx(re), unify(re), unifz(re));
				//Logger::consolePrint("exiting placement loop without proper sample\n");
				break;
			}
		} while (truncated); // if the random position was out of bound and truncated, re sample.

		// set the random position for evaluation
		assemblyToPerturb->electroMechanicalComponents[j]->setPosition(randPos);

		// clear the distribution array for next object and delete pointers to distribution for the current object, in preparation for the next
		distributionForPositions.erase(distributionForPositions.begin(), distributionForPositions.end());
	}

}

void AssemblyPlanner::calculateGaussianPositionPerturbation(DynamicArray<std::normal_distribution<double>>& distributionForPositions,
	P3D meanPosition, double currentAcceptRate, double& bandwidth, int sampleType, bool updateFlag) {

	double minBWScale = 0.01;
	double maxBWScale = 0.9;

	/*** Fixed-Gaussian ****/
	double bandwidthC = 0.250;
	if (sampleType & SAMPLE_AUTOTUNEGAUSSIAN) {
		/*** Autotuned-Gaussian ***/
		bandwidthC = bandwidth;

		if (updateFlag) {
			if (currentAcceptRate < 0.23) //<0.4
				bandwidthC = bandwidthC / 1.01; //2 1.5
			else if (currentAcceptRate>0.5) //0.6
				bandwidthC = bandwidthC*1.01; //2 1.5

			if (bandwidthC > maxBWScale)
				bandwidthC = maxBWScale;
			if (bandwidthC < minBWScale)
				bandwidthC = minBWScale;

			bandwidth = bandwidthC;
		}
	}

	double sigmaX = bandwidthC * (globalWorldBounds[0]) / 3.0;
	double sigmaY = bandwidthC * (globalWorldBounds[1]) / 3.0;
	double sigmaZ = bandwidthC * (globalWorldBounds[2]) / 3.0;

	// generate gaussian distributions with desired mean and std. dev.
	// sample from this gaussian till you get a valid sample 
	std::normal_distribution<double> normalDistributionX = std::normal_distribution<double>(meanPosition[0], sigmaX);
	std::normal_distribution<double> normalDistributionY = std::normal_distribution<double>(meanPosition[1], sigmaY);
	std::normal_distribution<double> normalDistributionZ = std::normal_distribution<double>(meanPosition[2], sigmaZ);
	distributionForPositions = { normalDistributionX, normalDistributionY, normalDistributionZ };
}

void AssemblyPlanner::calculateGaussianAnglePerturbation(std::normal_distribution<double>& distributionForAngle,
	double meanAngle, double currentAcceptRate, double& bandwidth, int sampleType, bool updateFlag) {

	double minBWScale = 0.01;
	double maxBWScale = 0.9;

	/*** Fixed-Gaussian ****/
	double bandwidthC = 0.250;
	if (sampleType & SAMPLE_AUTOTUNEGAUSSIAN) {
		/*** Autotuned-Gaussian ***/
		bandwidthC = bandwidth;

		if (updateFlag) {
			if (currentAcceptRate < 0.23) //<0.4
				bandwidthC = bandwidthC / 1.01; //2 1.5
			else if (currentAcceptRate>0.5) //0.6
				bandwidthC = bandwidthC*1.01; //2 1.5

			if (bandwidthC > maxBWScale)
				bandwidthC = maxBWScale;
			if (bandwidthC < minBWScale)
				bandwidthC = minBWScale;

			bandwidth = bandwidthC;
		}
	}

	double maxAllowedAngle = PI / 2; //PI / 3
	double sigmaA = bandwidthC * (maxAllowedAngle) / 3.0;

	// generate gaussian distributions with desired mean and std. dev.
	// sample from this gaussian till you get a valid sample 
	std::normal_distribution<double> normalDistributionA = std::normal_distribution<double>(meanAngle, sigmaA);
	distributionForAngle = normalDistributionA;
}

void AssemblyPlanner::perturbAssemblyOrder(int seed, Assembly* assemblyToPerturb) {
	std::default_random_engine re(seed);

	//collect current disassembly indices
	DynamicArray<int> disassemblyIndices;
	for (uint i = 0; i < assemblyToPerturb->electroMechanicalComponents.size(); i++) {
		disassemblyIndices.push_back(assemblyToPerturb->electroMechanicalComponents[i]->disassemblyIndex);
	}
	std::uniform_int_distribution<int> randomObjIndDistribution(0, disassemblyIndices.size() - 1);

	/**swap assembly orders of 2 randomly selected objects**/
	// randomly selected 2 objs -- their indices
	int indicesToSwap[2] = { randomObjIndDistribution(re), randomObjIndDistribution(re) };
	std::swap(disassemblyIndices[indicesToSwap[0]], disassemblyIndices[indicesToSwap[1]]);

	// set new assembly orders 
	int disassemblyInd = 0;
	for (uint i = 0; i < assemblyToPerturb->electroMechanicalComponents.size(); i++) {
		assemblyToPerturb->electroMechanicalComponents[i]->disassemblyIndex = disassemblyIndices[i];
	}
}

void AssemblyPlanner::perturbAssemblyDirections(int seed, Assembly* assemblyToPerturb, int sampleFlag,
	double currentAcceptRate, double& bandwidth) {

	std::default_random_engine re(seed);

	if (sampleFlag & SAMPLE_UNIFORM) {
		/***SAMPLING FROM DISCRETE ASSEMBLY DIRECTIONS***/
		DynamicArray<V3D> validAxis = assemblyToPerturb->chassis->validAssemblyDirs;
		//DynamicArray<std::pair<double, double>> validAxis = assemblyToPerturb->chassis->validAssemblyDirs;

		std::uniform_int_distribution<int> distributionForAxis(0, size(validAxis) - 1);

		for (uint j = 0; j < assemblyToPerturb->electroMechanicalComponents.size(); j++) {
			int axisInd = distributionForAxis(re);
			if (dynamic_cast<ServoMotor*>(assemblyToPerturb->electroMechanicalComponents[j])
				|| dynamic_cast<MicroServoMotor*>(assemblyToPerturb->electroMechanicalComponents[j]))
				continue;
			else {
				if (dynamic_cast<ArbitraryDirectionDissassemblyPath*>(assemblyToPerturb->electroMechanicalComponents[j]->disassemblyPath)) {
					dynamic_cast<ArbitraryDirectionDissassemblyPath*>(assemblyToPerturb->electroMechanicalComponents[j]->disassemblyPath)->disassemblyDefaultPath = validAxis[axisInd];
					if (dynamic_cast<ArbitraryDirectionDissassemblyPath*>(assemblyToPerturb->electroMechanicalComponents[j]->disassemblyPath)->disassemblyDefaultPath!= V3D(0, 1, 0))
						dynamic_cast<ArbitraryDirectionDissassemblyPath*>(assemblyToPerturb->electroMechanicalComponents[j]->disassemblyPath)->resetInternalAxis();
					//dynamic_cast<ArbitraryDirectionDissassemblyPath*>(assemblyToPerturb->electroMechanicalComponents[j]->disassemblyPath)->assemblyAngleTheta = validAxis[axisInd].first;
					//dynamic_cast<ArbitraryDirectionDissassemblyPath*>(assemblyToPerturb->electroMechanicalComponents[j]->disassemblyPath)->assemblyAnglePhi = validAxis[axisInd].second;
				}
				//else if (dynamic_cast<TwoPhaseArbitraryDirectionDissassemblyPath*>(assemblyToPerturb->electroMechanicalComponents[j]->disassemblyPath)) {
				//	dynamic_cast<TwoPhaseArbitraryDirectionDissassemblyPath*>(assemblyToPerturb->electroMechanicalComponents[j]->disassemblyPath)->disassemblyDefaultPath = validAxis[axisInd];
				//	if (dynamic_cast<TwoPhaseArbitraryDirectionDissassemblyPath*>(assemblyToPerturb->electroMechanicalComponents[j]->disassemblyPath)->disassemblyDefaultPath != V3D(0, 1, 0))
				//		dynamic_cast<TwoPhaseArbitraryDirectionDissassemblyPath*>(assemblyToPerturb->electroMechanicalComponents[j]->disassemblyPath)->resetInternalAxis();
				//}
			}
		}
	}
	else {
		///***GAUSSIAN PERTURBATION TO ANGLES***/
		for (uint j = 0; j < assemblyToPerturb->electroMechanicalComponents.size(); j++) {
			std::normal_distribution<double> distributionForAngle1, distributionForAngle2;
			ArbitraryDirectionDissassemblyPath* path = dynamic_cast<ArbitraryDirectionDissassemblyPath*>(assemblyToPerturb->electroMechanicalComponents[j]->disassemblyPath);
			if (path) {
				calculateGaussianAnglePerturbation(distributionForAngle1, path->assemblyAnglePhi,
					currentAcceptRate, bandwidth, sampleFlag);
				calculateGaussianAnglePerturbation(distributionForAngle2, path->assemblyAngleTheta,
					currentAcceptRate, bandwidth, sampleFlag);
				bool truncatedRot1, truncatedRot2;
				double randAngle1, randAngle2;

				do {
					// sample a random point from the valid distribution
					randAngle1 = distributionForAngle1(re);
					// see if its valid based on object state (frozen or not) and maximum allowable values. 
					truncatedRot1 = assemblyToPerturb->electroMechanicalComponents[j]->getValidRandomAngle(randAngle1, ASSEMBLY_ANGLE);
				} while (truncatedRot1); // if the random angles are out of bound and truncated, re sample
				do {
					// sample a random point from the valid distribution
					randAngle2 = distributionForAngle1(re);
					// see if its valid based on object state (frozen or not) and maximum allowable values. 
					truncatedRot2 = assemblyToPerturb->electroMechanicalComponents[j]->getValidRandomAngle(randAngle2, ASSEMBLY_ANGLE);
				} while (truncatedRot2); // if the random angles are out of bound and truncated, re sample
										 // set the random angle

				// set the random angle
				path->assemblyAnglePhi = randAngle1;
				path->assemblyAngleTheta = randAngle2;
			}
		}
	}
}

//a struct to store the object index and a parameter value(could be volume, or y position)
struct objectParamterLog {
	int objectIndex;
	double value;
};

//compare function for struct objectParamterLog
bool compareObjectUsingLoggedParam(objectParamterLog* a, objectParamterLog* b) {
	return a->value > b->value;
};

void AssemblyPlanner::setTopDownAssemblyOrder(Assembly* assem) {
	//generate the disassembly order
	DynamicArray<objectParamterLog*> current2;//an array to store the object index and their corresponding y value
	for (uint i = 0; i < assem->electroMechanicalComponents.size(); i++) {
		objectParamterLog* t = new objectParamterLog();
		t->objectIndex = i;
		t->value = (assem->electroMechanicalComponents[i]->getPosition())[1];
		current2.push_back(t);
	}
	sort(current2.begin(), current2.end(), compareObjectUsingLoggedParam);

	for (uint i = 0; i < assem->electroMechanicalComponents.size(); i++) {
		assem->electroMechanicalComponents[current2[i]->objectIndex]->disassemblyIndex = i;
	}

	//deleting the two arrays
	for (uint i = 0; i < current2.size(); i++) {
		delete current2[i];
	}
}

void AssemblyPlanner::setAssemblyDirBasedOnConfiguration(Assembly* assem) {
	for (size_t i = 0; i < assem->electroMechanicalComponents.size(); i++)
	{
		if (dynamic_cast<MotorComponents*>(assem->electroMechanicalComponents[i])) continue;
		else {
			Plane closestPlane = assem->findClosestChassisWallToComponent(assem->electroMechanicalComponents[i]);
			auto path = dynamic_cast<ArbitraryDirectionDissassemblyPath*>(assem->electroMechanicalComponents[i]->disassemblyPath);
			if (path && path->getWorldCoordinatesAssemblyPath() != -closestPlane.n) {
				path->disassemblyDefaultPath = -closestPlane.n;
				path->assemblyAnglePhi = 0;
				path->assemblyAngleTheta = 0;
			}
		}
	}
}

void AssemblyPlanner::setAssemblyOrderBasedOnConfiguration(Assembly* assembly) {
	// for each object that is to be dis-assembled, check if its current assembly direction is blocked
	for (uint j = 0; j < assembly->electroMechanicalComponents.size(); j++) {
		for (uint i = 0; i< assembly->electroMechanicalComponents.size(); i++) {
			if (i == j) continue;
			//now, we have two cases to consider... object j is being disassembled, object i will either be disassembled later, or it has already been removed...
			bool objIStillInScene = assembly->electroMechanicalComponents[i]->disassemblyIndex > assembly->electroMechanicalComponents[j]->disassemblyIndex;

			if (objIStillInScene) {
				auto path = assembly->electroMechanicalComponents[j]->disassemblyPath;
				// j's path dir
				V3D removalDir = path->getWorldCoordinatesAssemblyPath();
				Segment  assemblyObjSegment(assembly->electroMechanicalComponents[j]->getPosition(),
					assembly->electroMechanicalComponents[j]->getPosition() + removalDir*path->pathLength);
				// is i close to path?
				// rough estimate
				P3D closestPt = assemblyObjSegment.getClosestPointTo(assembly->electroMechanicalComponents[i]->getPosition());
				// improve estimate based on size
				if (closestPt != assemblyObjSegment.a && closestPt != assemblyObjSegment.b) {
					V3D distVec = V3D(closestPt, assembly->electroMechanicalComponents[i]->getPosition());
					double x, y, z, x1, y1, z1;
					assembly->electroMechanicalComponents[j]->getSilhouetteScalingDimensions(x, y, z);
					assembly->electroMechanicalComponents[i]->getSilhouetteScalingDimensions(x1, y1, z1);
					// size estimates of obj i in scene and the disassembling obj j perpendicular to dir
					V3D disassemblyObjSizeEstimate = V3D(x, y, z) - V3D(x, y, z)*V3D(x, y, z).dot(removalDir);
					V3D blockingObjSizeEstimate = V3D(x1, y1, z1) - V3D(x1, y1, z1)*V3D(x1, y1, z1).dot(removalDir);
					double maxDisassemblyObjSizeEstimate = MAX(disassemblyObjSizeEstimate[0],
						MAX(disassemblyObjSizeEstimate[1], disassemblyObjSizeEstimate[2]));
					double maxBlockingObjSizeEstimate = MAX(blockingObjSizeEstimate[0],
						MAX(blockingObjSizeEstimate[1], blockingObjSizeEstimate[2]));
					// net size estimate
					double objSizeEstimate = maxDisassemblyObjSizeEstimate + maxBlockingObjSizeEstimate;
					//Logger::consolePrint("MainDisId and changeObjDisId :%d, %d| dist:%lf | main objSizeEstimate:%lf\n",
					//	assembly->electroMechanicalComponents[j]->disassemblyIndex, 
					//	assembly->electroMechanicalComponents[i]->disassemblyIndex,
					//	distVec.length(), objSizeEstimate);
					// swap order now if needed
					if (distVec.length() < objSizeEstimate && assembly->electroMechanicalComponents[j]->disassemblyIndex<
						assembly->electroMechanicalComponents[i]->disassemblyIndex) {
						int storeInd = assembly->electroMechanicalComponents[j]->disassemblyIndex;
						assembly->electroMechanicalComponents[j]->disassemblyIndex = assembly->electroMechanicalComponents[i]->disassemblyIndex;
						assembly->electroMechanicalComponents[i]->disassemblyIndex = storeInd;
						//Logger::consolePrint("Swap done\n");
					}
				}
			}
		}
	}
}
