#pragma once

#include <GUILib/GLMesh.h>
#include <MathLib/mathLib.h>
#include <MathLib/P3D.h>
#include <MathLib/V3D.h>
#include <MathLib/Quaternion.h>
#include <GUILib/TranslateWidget.h>
#include "BaseObject.h"
#include "AssemblyUtils.h"
#include "BulletCollisionObject.h"
#include "Support.h"
#include "Fastener.h"

/*
Makeblock objects -- for real examples.
*/

static double boardColor[4] = { 0.2, 0.4, 0, 1.0 }; // circuit board like color:{ 0.2, 0.4, 0, 1.0 }; nice blue shade: { 0.0, 0.2, 0.4, 1.0 }
static double connectorColor[4] = { 0.2, 0.2, 0.2, 1.0 };
static double wireColor[4] = { 0.5, 0.5, 0.5, 1.0 };
static double goldColor[4] = {0.854, 0.647, 0.125, 1.0};
static double goldColor2[4] = { 0.8313, 0.68627, 0.2156, 1.0 };

// base class for make block objects
class MakeBlockComponents : public EMComponent {
protected:
	DynamicArray<btBoxShape> colShapes;
	DynamicArray<btCollisionObject> colObjects;
	DynamicArray<V3D> colShapeGeometryCenterLocalOffset;
	// may be define vector of supports and fasteners
	DynamicArray<RigidlyAttachedSupport*> supports;
	DynamicArray<Fastener*> fasteners;

public:

	// component mesh
	DynamicArray<GLMesh*> componentMeshes;
	bool loadMeshAndTexture;

	// bounding box dimension
	double xMax = 1.0f;
	double yMax = 1.0f;
	double zMax = 1.0f;

	// constructor
	MakeBlockComponents();
	// destructor
	virtual ~MakeBlockComponents() {
		while (!supports.empty()) {
			delete supports.back();
			supports.pop_back();
		}
		while (!fasteners.empty()) {
			delete fasteners.back();
			fasteners.pop_back();
		}
	}

	virtual void initGeometry() = 0;

	virtual void drawObjectGeometry(GLShaderMaterial* material = NULL);

	virtual void getSilhouetteScalingDimensions(double &dimX, double &dimY, double &dimZ) {
		dimX = xMax / 2; dimY = yMax / 2;  dimZ = zMax / 2;
	}

	virtual void addBulletCollisionObjectsToList(DynamicArray<btCollisionObject*>& bulletCollisionObjs) {
		if (colObjects.size() != colShapes.size())
			colObjects.resize(colShapes.size());
		for (size_t i = 0; i < colShapes.size(); i++){
			bulletCollisionObjs.push_back(getBulletCollisionObjectFromLocalCoordsShape(this, &colShapes[i], &colObjects[i],
				colShapeGeometryCenterLocalOffset[i]));
		}
	}

	virtual EMComponent* clone(bool cloneTexture = true) = 0;

	//virtual void addAttachmentPseudoObjectsToList(DynamicArray<FastenerPseudoObject*>& attachmentObjs);
	virtual void addSupportsToList(DynamicArray<RigidlyAttachedSupport*>& supportObjs) {
		for (size_t i = 0; i < supports.size(); i++)
		{
			supportObjs.push_back(supports[i]);
		}
	}
	virtual void addFastnersToList(DynamicArray<Fastener*>& attachmentObjs) {
		for (size_t i = 0; i < fasteners.size(); i++)
		{
			attachmentObjs.push_back(fasteners[i]);
		}
	}

	//virtual void setCADStringToDefault();
	virtual void loadFromFile(FILE* fp);
	virtual void writeToFile(FILE* fp);

	virtual GLMesh* getWireGeometryMesh() {
		return NULL;
	}

	virtual DynamicArray<std::pair<char*, double*>> getObjectParameters() {
		DynamicArray<std::pair<char*, double*>> result;
		result.push_back(std::pair<char*, double*>("disassembly dir-x", &dynamic_cast<ArbitraryDirectionDissassemblyPath*>(disassemblyPath)->disassemblyDefaultPath[0]));
		result.push_back(std::pair<char*, double*>("disassembly dir-y", &dynamic_cast<ArbitraryDirectionDissassemblyPath*>(disassemblyPath)->disassemblyDefaultPath[1]));
		result.push_back(std::pair<char*, double*>("disassembly dir-z", &dynamic_cast<ArbitraryDirectionDissassemblyPath*>(disassemblyPath)->disassemblyDefaultPath[2]));
		dynamic_cast<ArbitraryDirectionDissassemblyPath*>(disassemblyPath)->disassemblyDefaultPath.toUnit();
		//result.push_back(std::pair<char*, double*>("Cuboid-height", &h));
		return result;
	}
};


/************************* MOTOR DRIVER **************************************/
class MakeBlockMotorDriver : public MakeBlockComponents {
public:
	// constructor
	MakeBlockMotorDriver(bool textureFlag = true) {
		loadMeshAndTexture = textureFlag;
		initGeometry();
	}
	virtual ~MakeBlockMotorDriver() {};

	virtual void initGeometry() {
		name = "MakeBlockMotorDriver";

		if (loadMeshAndTexture) {
			GLShaderMaterial tmpMat;
			componentMeshes.push_back(GLContentManager::getGLMesh("../data/3dModels/makeblock/makeBlock_RJ25_2.obj"));
			tmpMat.r = wireColor[0]; tmpMat.g = wireColor[1]; tmpMat.b = wireColor[2]; tmpMat.a = wireColor[3]; componentMeshes.back()->setMaterial(tmpMat);

			componentMeshes.push_back(GLContentManager::getGLMesh("../data/3dModels/makeblock/makeBlock_RJ25_1.obj"));
			tmpMat.r = boardColor[0]; tmpMat.g = boardColor[1]; tmpMat.b = boardColor[2]; tmpMat.a = boardColor[3]; componentMeshes.back()->setMaterial(tmpMat);
			tmpMat.setShaderProgram(GLContentManager::getShaderProgram("matcap"));
			tmpMat.setTextureParam("../data/textures/matcap/5.bmp", GLContentManager::getTexture("../data/textures/matcap/5.bmp"));
			componentMeshes.back()->setMaterial(tmpMat);

			componentMeshes.push_back(GLContentManager::getGLMesh("../data/3dModels/makeblock/makeBlock_RJ25_3.obj"));
			tmpMat.setTextureParam("../data/textures/matcap/glossBlack.bmp", GLContentManager::getTexture("../data/textures/matcap/glossBlack.bmp"));
			componentMeshes.back()->setMaterial(tmpMat);
			tmpMat.r = connectorColor[0]; tmpMat.g = connectorColor[1]; tmpMat.b = connectorColor[2]; tmpMat.a = connectorColor[3]; componentMeshes.back()->setMaterial(tmpMat);
		}

		//bounding box
		xMax = 0.0120 * 2;
		yMax = 0.0090 * 2;
		zMax = 0.0256 * 2;

		colShapes.push_back(btBoxShape(btVector3((btScalar)(xMax / 2.0), (btScalar)(yMax / 8.0), (btScalar)(zMax / 2.0))));
		colShapeGeometryCenterLocalOffset.push_back(V3D(0, -((yMax / 2.0 - yMax / 4.0) + (yMax / 2.0 - yMax / 4.0) / 2.0), 0));
		colShapes.push_back(btBoxShape(btVector3((btScalar)(xMax / 3.5), (btScalar)(yMax / 2.0), (btScalar)(zMax / 8.0))));
		colShapeGeometryCenterLocalOffset.push_back(V3D(0, 0, ((zMax / 2.0 - zMax / 4.0) + (zMax / 2.0 - zMax / 4.0) / 2.0)));
		colShapes.push_back(btBoxShape(btVector3((btScalar)(xMax / 2.0), (btScalar)(yMax / 2.0), (btScalar)(zMax / 16.0))));
		colShapeGeometryCenterLocalOffset.push_back(V3D(0, 0, -((zMax / 2.0 - zMax / 4.0) + (zMax / 2.5) / 2.0)));
		
		// add assembly path and convex hull points
		MakeBlockComponents::initGeometry();

		// support, fasteners and wires
		//supports.push_back(new RigidlyAttachedSupport(this, P3D(0, -yMax / 2.0, 0.0065), AxisAlignedBoundingBox(P3D(-xMax / 2, -0.0025, -0.003), P3D(xMax / 2, 0.0025, 0.003))));
		//
		//fasteners.push_back(new Fastener(this, P3D(-0.008, 0, 0.0065), V3D(0, 1, 0), 0.00205));
		//fasteners.push_back(new Fastener(this, P3D(0.008, 0, 0.0065), V3D(0, 1, 0), 0.00205));

		supports.push_back(new RigidlyAttachedSupport(this, P3D(0, -yMax / 2.0, -0.0014325), AxisAlignedBoundingBox(P3D(-xMax / 2, -0.0025, -0.005), P3D(xMax / 2, 0.0025, 0.005))));

		fasteners.push_back(new Fastener(this, P3D(-0.008, 0, -0.0014325), V3D(0, 1, 0), 0.00205));
		fasteners.push_back(new Fastener(this, P3D(0.008, 0, -0.0014325), V3D(0, 1, 0), 0.00205));
	}

	virtual EMComponent* clone(bool cloneTexture = true) {
		MakeBlockMotorDriver* newObject = new MakeBlockMotorDriver(cloneTexture);
		// copy other properties eg: support sizes..
		return newObject;
	}

	virtual GLMesh* getWireGeometryMesh() {
		P3D pos = this->getWorldCoordinates(P3D() + V3D(0, 0, ((zMax / 2.0 - zMax / 4.0) + (zMax / 2.0 - zMax / 4.0) / 2.0)));
		double a, b, c;
		computeEulerAnglesFromQuaternion(this->getOrientation(), V3D(1, 0, 0), V3D(0, 1, 0), V3D(0, 0, 1), a, b, c);
		a = a * 180 / PI;
		b = b * 180 / PI;
		c = c * 180 / PI;
		double scale = 1.0 +abs(b) / 90.0;

		GLMesh* wireMesh = new GLMesh();
		wireMesh->addCylinder(pos,
			pos + this->getWorldCoordinates(V3D(0,0,1)) *0.027*scale, 0.008);

		return wireMesh;
	}
};

/************************* CONTROLLER BOARD **************************************/
class MakeBlockOrionBoard : public MakeBlockComponents {
public:
	// constructor
	MakeBlockOrionBoard(bool textureFlag = true) {
		loadMeshAndTexture = textureFlag;
		initGeometry();
	}
	virtual ~MakeBlockOrionBoard() {};

	virtual void initGeometry() {
		name = "MakeBlockOrionBoard";
		//componentMeshes.push_back(GLContentManager::getGLMesh("../data/3dModels/makeBlock_orionBoard.obj"));

		if (loadMeshAndTexture) {
			componentMeshes.push_back(GLContentManager::getGLMesh("../data/3dModels/makeblock/makeBlock_orionBoard_1.obj"));
			//tmpMat.r = boardColor[0]; tmpMat.g = boardColor[1]; tmpMat.b = boardColor[2]; tmpMat.a = boardColor[3]; componentMeshes.back()->setMaterial(tmpMat);
			GLShaderMaterial tmpMat;
			tmpMat.setShaderProgram(GLContentManager::getShaderProgram("matcap"));
			tmpMat.setTextureParam("../data/textures/matcap/5.bmp", GLContentManager::getTexture("../data/textures/matcap/5.bmp"));
			componentMeshes.back()->setMaterial(tmpMat);

			componentMeshes.push_back(GLContentManager::getGLMesh("../data/3dModels/makeblock/makeBlock_orionBoard_2.obj"));
			tmpMat.setTextureParam("../data/textures/matcap/JG_Gold.bmp", GLContentManager::getTexture("../data/textures/matcap/JG_Gold.bmp"));
			componentMeshes.back()->setMaterial(tmpMat);

			componentMeshes.push_back(GLContentManager::getGLMesh("../data/3dModels/makeblock/makeBlock_orionBoard_3.obj"));
			tmpMat.setTextureParam("../data/textures/matcap/glossBlack.bmp", GLContentManager::getTexture("../data/textures/matcap/glossBlack.bmp"));
			componentMeshes.back()->setMaterial(tmpMat);
			//tmpMat3.r = connectorColor[0]; tmpMat3.g = connectorColor[1]; tmpMat3.b = connectorColor[2]; tmpMat3.a = connectorColor[3]; componentMeshes.back()->setMaterial(tmpMat3);
		}

		//bounding box
		xMax = 0.03 * 2;
		yMax = 0.0094 * 2;
		zMax = 0.04 * 2;

		colShapes.push_back(btBoxShape(btVector3((btScalar)(xMax / 2), (btScalar)(yMax / 2), (btScalar)(zMax / 2))));
		colShapeGeometryCenterLocalOffset.push_back(V3D(0, 0, 0));

		// add assembly path and convex hull points
		MakeBlockComponents::initGeometry();

		// support, fasteners and wires
		supports.push_back(new RigidlyAttachedSupport(this, P3D(0, -yMax / 2, 0.026), AxisAlignedBoundingBox(P3D(-0.012, -0.0025, -0.003), P3D(0.012, 0.0025, 0.003))));
		supports.push_back(new RigidlyAttachedSupport(this, P3D(0, -yMax / 2, -0.022), AxisAlignedBoundingBox(P3D(-0.012, -0.0025, -0.003), P3D(0.012, 0.0025, 0.003))));

		fasteners.push_back(new Fastener(this, P3D(0.0087, 0, 0.026), V3D(0, 1, 0), 0.00205));
		fasteners.push_back(new Fastener(this, P3D(-0.0078, 0, 0.026), V3D(0, 1, 0), 0.00205));
		fasteners.push_back(new Fastener(this, P3D(0.0087, 0, -0.022), V3D(0, 1, 0), 0.00205));
		fasteners.push_back(new Fastener(this, P3D(-0.0078, 0, -0.022), V3D(0, 1, 0), 0.00205));
	}

	virtual EMComponent* clone(bool cloneTexture = true) {
		MakeBlockOrionBoard* newObject = new MakeBlockOrionBoard(cloneTexture);
		// copy other properties eg: support sizes..
		return newObject;
	}

	virtual GLMesh* getWireGeometryMesh() {
		P3D pos = this->getWorldCoordinates(P3D() + P3D(0, 0, zMax / 2));
		P3D pos1 = this->getWorldCoordinates(P3D() + P3D(0, 0, -zMax / 2));
		P3D pos2 = this->getWorldCoordinates(P3D() + P3D(xMax / 4, 0, -zMax / 2));
		P3D pos3 = this->getWorldCoordinates(P3D() + P3D(-xMax / 4, 0, -zMax / 2));

		double a, b, c;
		computeEulerAnglesFromQuaternion(this->getOrientation(), V3D(1, 0, 0), V3D(0, 1, 0), V3D(0, 0, 1), a, b, c);
		a = a * 180 / PI;
		b = b * 180 / PI;
		c = c * 180 / PI;
		double scale = 1.0 + abs(b) / 90.0;

		GLMesh* wireMesh = new GLMesh();
		GLMesh* tempMesh = new GLMesh();

		tempMesh->addCylinder(pos1,
			pos1 + this->getWorldCoordinates(V3D(0, 0, -1)) *0.029*scale, 0.008);
		tempMesh->addCylinder(pos2,
			pos2 + this->getWorldCoordinates(V3D(0, 0, -1)) *0.029*scale, 0.008);
		tempMesh->addCylinder(pos3,
			pos3 + this->getWorldCoordinates(V3D(0, 0, -1)) *0.029*scale, 0.008);

		ConvexHull3D::computeConvexHullForMesh(tempMesh, wireMesh);
		wireMesh->addCylinder(pos,
			pos + this->getWorldCoordinates(V3D(0, 0, 1)) *0.017*scale, 0.008);

		delete tempMesh;
		return wireMesh;
	}
};

/************************* ULTRASONIC SENSOR **************************************/
class MakeBlockUltrasonic : public MakeBlockComponents {
public:
	// constructor
	MakeBlockUltrasonic(bool textureFlag = true) {
		loadMeshAndTexture = textureFlag;
		initGeometry();
	}
	virtual ~MakeBlockUltrasonic() {};

	virtual void initGeometry() {
		name = "MakeBlockUltrasonic";
		//componentMeshes.push_back(GLContentManager::getGLMesh("../data/3dModels/makeBlock_ultrasonic.obj"));

		if (loadMeshAndTexture) {
			GLShaderMaterial tmpMat;
			componentMeshes.push_back(GLContentManager::getGLMesh("../data/3dModels/makeblock/makeBlock_ultrasonic_2.obj"));
			tmpMat.r = wireColor[0]; tmpMat.g = wireColor[1]; tmpMat.b = wireColor[2]; tmpMat.a = wireColor[3]; componentMeshes.back()->setMaterial(tmpMat);

			componentMeshes.push_back(GLContentManager::getGLMesh("../data/3dModels/makeblock/makeBlock_ultrasonic_1.obj"));
			//tmpMat.r = boardColor[0]; tmpMat.g = boardColor[1]; tmpMat.b = boardColor[2]; tmpMat.a = boardColor[3]; componentMeshes.back()->setMaterial(tmpMat);
			tmpMat.setShaderProgram(GLContentManager::getShaderProgram("matcap"));
			tmpMat.setTextureParam("../data/textures/matcap/5.bmp", GLContentManager::getTexture("../data/textures/matcap/5.bmp"));
			componentMeshes.back()->setMaterial(tmpMat);

			componentMeshes.push_back(GLContentManager::getGLMesh("../data/3dModels/makeblock/makeBlock_ultrasonic_3.obj"));
			//tmpMat.r = connectorColor[0]; tmpMat.g = connectorColor[1]; tmpMat.b = connectorColor[2]; tmpMat.a = connectorColor[3]; componentMeshes.back()->setMaterial(tmpMat);
			tmpMat.setTextureParam("../data/textures/matcap/glossBlack.bmp", GLContentManager::getTexture("../data/textures/matcap/glossBlack.bmp"));
			componentMeshes.back()->setMaterial(tmpMat);
		}

		//bounding box
		xMax = 0.0280 * 2;
		yMax = 0.0152 * 2;
		zMax = 0.0180 * 2;

		colShapes.push_back(btBoxShape(btVector3((btScalar)(xMax / 2.0), (btScalar)(yMax / 4.0), (btScalar)(zMax / 2.0))));
		colShapeGeometryCenterLocalOffset.push_back(V3D(0, (yMax / 4.0), 0));
		colShapes.push_back(btBoxShape(btVector3((btScalar)(xMax / 8.0), (btScalar)(yMax / 4.0), (btScalar)(zMax / 4.0))));
		colShapeGeometryCenterLocalOffset.push_back(V3D(0, -(yMax / 4.0), zMax / 8.0));

		// add assembly path and convex hull points
		MakeBlockComponents::initGeometry();

		// support, fasteners and wires
		supports.push_back(new RigidlyAttachedSupport(this, P3D(0, 0, -zMax / 2.0), AxisAlignedBoundingBox(P3D(-0.012, -0.0025, -0.008), P3D(0.012, 0.0025, 0.008))));
		supports.back()->nonResizeableDirections.push_back(V3D(0, 0, 1));

		fasteners.push_back(new Fastener(this, P3D(-0.008, 0.0065, -0.014), V3D(0, 1, 0), 0.00205));
		fasteners.push_back(new Fastener(this, P3D(0.008, 0.0065, -0.014), V3D(0, 1, 0), 0.00205));

		gamma = -PI / 2;
		alpha = -PI / 2;
	}

	virtual EMComponent* clone(bool cloneTexture = true) {
		MakeBlockUltrasonic* newObject = new MakeBlockUltrasonic(cloneTexture);
		// copy other properties eg: support sizes..
		return newObject;
	}

	virtual GLMesh* getWireGeometryMesh() {
		P3D pos = this->getWorldCoordinates(P3D() + V3D(0, -(yMax / 4.0), zMax / 8.0));
		double a, b, c;
		computeEulerAnglesFromQuaternion(this->getOrientation(), V3D(1, 0, 0), V3D(0, 1, 0), V3D(0, 0, 1), a, b, c);
		a = a * 180 / PI;
		b = b * 180 / PI;
		c = c * 180 / PI;
		double scale = 1.0 + abs(b) / 90.0;

		GLMesh* wireMesh = new GLMesh();
		wireMesh->addCylinder(pos,
			pos + this->getWorldCoordinates(V3D(0, -1, 0)) *0.017*scale, 0.008); //0.027*Scale

		return wireMesh;
	}
};

/************************* BLUETOOTH SENSOR **************************************/
class MakeBlockBluetooth : public MakeBlockComponents {
public:
	// constructor
	MakeBlockBluetooth(bool textureFlag = true) {
		loadMeshAndTexture = textureFlag;
		initGeometry();
	}
	virtual ~MakeBlockBluetooth() {};

	virtual void initGeometry() {
		name = "MakeBlockBluetooth";
		//componentMeshes.push_back(GLContentManager::getGLMesh("../data/3dModels/makeBlock_bluetooth.obj"));

		if (loadMeshAndTexture) {
			componentMeshes.push_back(GLContentManager::getGLMesh("../data/3dModels/makeblock/makeBlock_bluetooth_1.obj"));
			//tmpMat.r = boardColor[0]; tmpMat.g = boardColor[1]; tmpMat.b = boardColor[2]; tmpMat.a = boardColor[3]; componentMeshes.back()->setMaterial(tmpMat);
			GLShaderMaterial tmpMat;
			tmpMat.setShaderProgram(GLContentManager::getShaderProgram("matcap"));
			tmpMat.setTextureParam("../data/textures/matcap/5.bmp", GLContentManager::getTexture("../data/textures/matcap/5.bmp"));
			componentMeshes.back()->setMaterial(tmpMat);

			componentMeshes.push_back(GLContentManager::getGLMesh("../data/3dModels/makeblock/makeBlock_bluetooth_2.obj"));
			tmpMat.setTextureParam("../data/textures/matcap/JG_Gold.bmp", GLContentManager::getTexture("../data/textures/matcap/JG_Gold.bmp"));
			componentMeshes.back()->setMaterial(tmpMat);

			componentMeshes.push_back(GLContentManager::getGLMesh("../data/3dModels/makeblock/makeBlock_bluetooth_3.obj"));
			//tmpMat.r = connectorColor[0]; tmpMat.g = connectorColor[1]; tmpMat.b = connectorColor[2]; tmpMat.a = connectorColor[3]; componentMeshes.back()->setMaterial(tmpMat);
			tmpMat.setTextureParam("../data/textures/matcap/glossBlack.bmp", GLContentManager::getTexture("../data/textures/matcap/glossBlack.bmp"));
			componentMeshes.back()->setMaterial(tmpMat);
		}

		//bounding box
		xMax = 0.0120 * 2;
		yMax = 0.0090 * 2;
		zMax = 0.0256 * 2;

		colShapes.push_back(btBoxShape(btVector3((btScalar)(xMax / 2.0), (btScalar)(yMax / 8.0), (btScalar)(zMax / 2.0))));
		colShapeGeometryCenterLocalOffset.push_back(V3D(0, -((yMax / 2.0 - yMax / 4.0) + (yMax / 2.0 - yMax / 4.0) / 2.0), 0));
		colShapes.push_back(btBoxShape(btVector3((btScalar)(xMax / 3.5), (btScalar)(yMax / 2.0), (btScalar)(zMax / 8.0))));
		colShapeGeometryCenterLocalOffset.push_back(V3D(0.001, 0, ((zMax / 2.0 - zMax / 4.0) + (zMax / 2.0 - zMax / 4.0) / 2)));

		// add assembly path and convex hull points
		MakeBlockComponents::initGeometry();

		// support, fasteners and wires
		supports.push_back(new RigidlyAttachedSupport(this, P3D(0, -yMax / 2.0, 0.0065), AxisAlignedBoundingBox(P3D(-xMax / 2, -0.0025, -0.005), P3D(xMax / 2, 0.0025, 0.005))));

		fasteners.push_back(new Fastener(this, P3D(-0.008, 0, 0.0065), V3D(0, 1, 0), 0.00205));
		fasteners.push_back(new Fastener(this, P3D(0.008, 0, 0.0065), V3D(0, 1, 0), 0.00205));
	}

	virtual EMComponent* clone(bool cloneTexture = true) {
		MakeBlockBluetooth* newObject = new MakeBlockBluetooth(cloneTexture);
		// copy other properties eg: support sizes..
		return newObject;
	}

	virtual GLMesh* getWireGeometryMesh() {
		P3D pos = this->getWorldCoordinates(P3D() + V3D(0, 0, ((zMax / 2.0 - zMax / 4.0) + (zMax / 2.0 - zMax / 4.0) / 2.0)));
		double a, b, c;
		computeEulerAnglesFromQuaternion(this->getOrientation(), V3D(1, 0, 0), V3D(0, 1, 0), V3D(0, 0, 1), a, b, c);
		a = a * 180 / PI;
		b = b * 180 / PI;
		c = c * 180 / PI;
		double scale = 1.0 + abs(b) / 90.0;

		GLMesh* wireMesh = new GLMesh();
		wireMesh->addCylinder(pos,
			pos + this->getWorldCoordinates(V3D(0, 0, 1)) *0.012*scale, 0.008); //0.027*scale

		return wireMesh;
	}
};


/************************* ENCODER-MOTOR DRIVER **************************************/
class MakeBlockEncoderMotorDriver : public MakeBlockComponents {
public:
	// constructor
	MakeBlockEncoderMotorDriver(bool textureFlag = true) {
		loadMeshAndTexture = textureFlag;
		initGeometry();
	}
	virtual ~MakeBlockEncoderMotorDriver() {};

	virtual void initGeometry() {
		name = "MakeBlockEncoderMotorDriver";
		//componentMeshes.push_back(GLContentManager::getGLMesh("../data/3dModels/makeBlock_EncoderMotorDriver.obj"));

		if (loadMeshAndTexture) {
			GLShaderMaterial tmpMat;
			componentMeshes.push_back(GLContentManager::getGLMesh("../data/3dModels/makeblock/makeBlock_EncoderMotorDriver_4.obj"));
			tmpMat.r = wireColor[0]; tmpMat.g = wireColor[1]; tmpMat.b = wireColor[2]; tmpMat.a = wireColor[3]; componentMeshes.back()->setMaterial(tmpMat);

			componentMeshes.push_back(GLContentManager::getGLMesh("../data/3dModels/makeblock/makeBlock_EncoderMotorDriver_1.obj"));
			//tmpMat.r = boardColor[0]; tmpMat.g = boardColor[1]; tmpMat.b = boardColor[2]; tmpMat.a = boardColor[3]; componentMeshes.back()->setMaterial(tmpMat);
			tmpMat.setShaderProgram(GLContentManager::getShaderProgram("matcap"));
			tmpMat.setTextureParam("../data/textures/matcap/5.bmp", GLContentManager::getTexture("../data/textures/matcap/5.bmp"));
			componentMeshes.back()->setMaterial(tmpMat);

			componentMeshes.push_back(GLContentManager::getGLMesh("../data/3dModels/makeblock/makeBlock_EncoderMotorDriver_2.obj"));
			tmpMat.setTextureParam("../data/textures/matcap/JG_Gold.bmp", GLContentManager::getTexture("../data/textures/matcap/JG_Gold.bmp"));
			componentMeshes.back()->setMaterial(tmpMat);

			componentMeshes.push_back(GLContentManager::getGLMesh("../data/3dModels/makeblock/makeBlock_EncoderMotorDriver_3.obj"));
			//tmpMat.r = connectorColor[0]; tmpMat.g = connectorColor[1]; tmpMat.b = connectorColor[2]; tmpMat.a = connectorColor[3]; componentMeshes.back()->setMaterial(tmpMat);
			tmpMat.setTextureParam("../data/textures/matcap/glossBlack.bmp", GLContentManager::getTexture("../data/textures/matcap/glossBlack.bmp"));
			componentMeshes.back()->setMaterial(tmpMat);
		}

		//bounding box
		xMax = 0.032;
		yMax = 0.0090 * 2;
		zMax = 0.070;

		colShapes.push_back(btBoxShape(btVector3((btScalar)(xMax / 2.0), (btScalar)(yMax / 8.0), (btScalar)(zMax / 2.0))));
		colShapeGeometryCenterLocalOffset.push_back(V3D(0, -((yMax / 2.0 - yMax / 4.0) + (yMax / 2.0 - yMax / 4.0) / 2.0), 0));
		colShapes.push_back(btBoxShape(btVector3((btScalar)(xMax / 4.5), (btScalar)(yMax / 2.0), (btScalar)(zMax / 9.0))));
		colShapeGeometryCenterLocalOffset.push_back(V3D(0, 0, ((zMax / 2.0 - zMax / 4.0) + (zMax / 2.0 - zMax / 4.0) / 2.0)));
		colShapes.push_back(btBoxShape(btVector3((btScalar)(xMax / 2.0), (btScalar)(yMax / 4.0), (btScalar)(zMax / 32.0))));
		colShapeGeometryCenterLocalOffset.push_back(V3D(0, -(btScalar)(yMax / 4.0), -((zMax / 2.0 - zMax / 4.0) + (zMax / 2.5) / 2.0)));

		// add assembly path and convex hull points
		MakeBlockComponents::initGeometry();

		// support, fasteners and wires
		supports.push_back(new RigidlyAttachedSupport(this, P3D(0, -yMax / 2.0, -0.008), AxisAlignedBoundingBox(P3D(-xMax / 2, -0.0025, -0.008), P3D(xMax / 2, 0.0025, 0.003))));

		fasteners.push_back(new Fastener(this, P3D(-0.008, 0, -0.01), V3D(0, 1, 0), 0.00205));
		fasteners.push_back(new Fastener(this, P3D(0.008, 0, -0.01), V3D(0, 1, 0), 0.00205));
	}

	virtual EMComponent* clone(bool cloneTexture = true) {
		MakeBlockEncoderMotorDriver* newObject = new MakeBlockEncoderMotorDriver(cloneTexture);
		// copy other properties eg: support sizes..
		return newObject;
	}

	virtual GLMesh* getWireGeometryMesh() {
		P3D pos = this->getWorldCoordinates(P3D() + V3D(0, 0, ((zMax / 2.0 - zMax / 4.0) + (zMax / 2.0 - zMax / 4.0) / 2.0)));
		double a, b, c;
		computeEulerAnglesFromQuaternion(this->getOrientation(), V3D(1, 0, 0), V3D(0, 1, 0), V3D(0, 0, 1), a, b, c);
		a = a * 180 / PI;
		b = b * 180 / PI;
		c = c * 180 / PI;
		double scale = 1.0 + abs(b) / 90.0;

		GLMesh* wireMesh = new GLMesh();
		wireMesh->addCylinder(pos,
			pos + this->getWorldCoordinates(V3D(0, 0, 1)) *0.027*scale, 0.008);

		return wireMesh;
	}
};

/************************* ACCELEROMETER **************************************/
class MakeBlockAccelerometer : public MakeBlockComponents {
public:
	// constructor
	MakeBlockAccelerometer(bool textureFlag = true) {
		loadMeshAndTexture = textureFlag;
		initGeometry();
	}
	virtual ~MakeBlockAccelerometer() {};

	virtual void initGeometry() {
		name = "MakeBlockAccelerometer ";
		//componentMeshes.push_back(GLContentManager::getGLMesh("../data/3dModels/makeBlock_Accelerometer.obj"));

		if (loadMeshAndTexture) {
			componentMeshes.push_back(GLContentManager::getGLMesh("../data/3dModels/makeblock/makeBlock_Accelerometer_1.obj"));
			//tmpMat.r = boardColor[0]; tmpMat.g = boardColor[1]; tmpMat.b = boardColor[2]; tmpMat.a = boardColor[3]; componentMeshes.back()->setMaterial(tmpMat);
			GLShaderMaterial tmpMat;
			tmpMat.setShaderProgram(GLContentManager::getShaderProgram("matcap"));
			tmpMat.setTextureParam("../data/textures/matcap/5.bmp", GLContentManager::getTexture("../data/textures/matcap/5.bmp"));
			componentMeshes.back()->setMaterial(tmpMat);

			componentMeshes.push_back(GLContentManager::getGLMesh("../data/3dModels/makeblock/makeBlock_Accelerometer_2.obj"));
			//tmpMat.r = connectorColor[0]; tmpMat.g = connectorColor[1]; tmpMat.b = connectorColor[2]; tmpMat.a = connectorColor[3]; componentMeshes.back()->setMaterial(tmpMat);
			tmpMat.setTextureParam("../data/textures/matcap/glossBlack.bmp", GLContentManager::getTexture("../data/textures/matcap/glossBlack.bmp"));
			componentMeshes.back()->setMaterial(tmpMat);
		}

		//bounding box
		xMax = 0.0120 * 2;
		yMax = 0.0086 * 2;
		zMax = 0.0257 * 2;

		colShapes.push_back(btBoxShape(btVector3((btScalar)(xMax / 2.0), (btScalar)(yMax / 8.0), (btScalar)(zMax / 2.0))));
		colShapeGeometryCenterLocalOffset.push_back(V3D(0, -((yMax / 2.0 - yMax / 4.0) + (yMax / 2.0 - yMax / 4.0) / 2.0), 0));
		colShapes.push_back(btBoxShape(btVector3((btScalar)(xMax / 3.5), (btScalar)(yMax / 2.0), (btScalar)(zMax / 8.0))));
		colShapeGeometryCenterLocalOffset.push_back(V3D(0.0, 0, ((zMax / 2.0 - zMax / 4.0) + (zMax / 2.0 - zMax / 4.0) / 2)));

		// add assembly path and convex hull points
		MakeBlockComponents::initGeometry();

		// support, fasteners and wires
		//supports.push_back(new RigidlyAttachedSupport(this, P3D(0, -yMax / 2.0, -0.0014325), AxisAlignedBoundingBox(P3D(-xMax / 2, -0.0025, -0.0112), P3D(xMax / 2, 0.0025, 0.0112))));

		//fasteners.push_back(new Fastener(this, P3D(-0.008, 0, 0.008 - 0.001435), V3D(0, 1, 0), 0.00205));
		//fasteners.push_back(new Fastener(this, P3D(0.008, 0, 0.008 - 0.001435), V3D(0, 1, 0), 0.00205));
		//fasteners.push_back(new Fastener(this, P3D(-0.008, 0, -0.008 - 0.001435), V3D(0, 1, 0), 0.00205));
		//fasteners.push_back(new Fastener(this, P3D(0.008, 0, -0.008 - 0.001435), V3D(0, 1, 0), 0.00205));

		// support, fasteners and wires
		//supports.push_back(new RigidlyAttachedSupport(this, P3D(0, -yMax / 2.0, 0.0065), AxisAlignedBoundingBox(P3D(-xMax / 2, -0.0025, -0.003), P3D(xMax / 2, 0.0025, 0.003))));
		
		//fasteners.push_back(new Fastener(this, P3D(-0.008, 0, 0.0065), V3D(0, 1, 0), 0.00205));
		//fasteners.push_back(new Fastener(this, P3D(0.008, 0, 0.0065), V3D(0, 1, 0), 0.00205));

		supports.push_back(new RigidlyAttachedSupport(this, P3D(0, -yMax / 2.0, -0.0014325), AxisAlignedBoundingBox(P3D(-xMax / 2, -0.0025, -0.005), P3D(xMax / 2, 0.0025, 0.005))));

		fasteners.push_back(new Fastener(this, P3D(-0.008, 0, -0.0014325), V3D(0, 1, 0), 0.00205));
		fasteners.push_back(new Fastener(this, P3D(0.008, 0, -0.0014325), V3D(0, 1, 0), 0.00205));

		beta = PI / 2;
	}

	virtual EMComponent* clone(bool cloneTexture = true) {
		MakeBlockAccelerometer * newObject = new MakeBlockAccelerometer(cloneTexture);
		// copy other properties eg: support sizes..
		return newObject;
	}

	virtual GLMesh* getWireGeometryMesh() {
		P3D pos = this->getWorldCoordinates(P3D() + V3D(0, 0, ((zMax / 2.0 - zMax / 4.0) + (zMax / 2.0 - zMax / 4.0) / 2.0)));
		double a, b, c;
		computeEulerAnglesFromQuaternion(this->getOrientation(), V3D(1, 0, 0), V3D(0, 1, 0), V3D(0, 0, 1), a, b, c);
		a = a * 180 / PI;
		b = b * 180 / PI;
		c = c * 180 / PI;
		double scale = 1.0 + abs(b) / 90.0;

		GLMesh* wireMesh = new GLMesh();
		wireMesh->addCylinder(pos,
			pos + this->getWorldCoordinates(V3D(0, 0, 1)) *0.027*scale, 0.008);

		return wireMesh;
	}
};

/************************** SOUND SENOSOR ************************************** */
class MakeBlockSoundSensor : public MakeBlockComponents {
public:
	// constructor
	MakeBlockSoundSensor(bool textureFlag = true) {
		loadMeshAndTexture = textureFlag;
		initGeometry();
	}
	virtual ~MakeBlockSoundSensor() {};

	virtual void initGeometry() {
		name = "MakeBlockSoundSensor ";
		//componentMeshes.push_back(GLContentManager::getGLMesh("../data/3dModels/makeBlock_soundSensor.obj"));

		if (loadMeshAndTexture) {
			componentMeshes.push_back(GLContentManager::getGLMesh("../data/3dModels/makeblock/makeBlock_soundSensor_1.obj"));
			//tmpMat.r = boardColor[0]; tmpMat.g = boardColor[1]; tmpMat.b = boardColor[2]; tmpMat.a = boardColor[3]; componentMeshes.back()->setMaterial(tmpMat);
			GLShaderMaterial tmpMat;
			tmpMat.setShaderProgram(GLContentManager::getShaderProgram("matcap"));
			tmpMat.setTextureParam("../data/textures/matcap/5.bmp", GLContentManager::getTexture("../data/textures/matcap/5.bmp"));
			componentMeshes.back()->setMaterial(tmpMat);

			componentMeshes.push_back(GLContentManager::getGLMesh("../data/3dModels/makeblock/makeBlock_soundSensor_2.obj"));
			//tmpMat.r = wireColor[0]; tmpMat.g = wireColor[1]; tmpMat.b = wireColor[2]; tmpMat.a = wireColor[3]; componentMeshes.back()->setMaterial(tmpMat);
			tmpMat.setTextureParam("../data/textures/matcap/JG_Gold.bmp", GLContentManager::getTexture("../data/textures/matcap/JG_Gold.bmp"));
			componentMeshes.back()->setMaterial(tmpMat);

			componentMeshes.push_back(GLContentManager::getGLMesh("../data/3dModels/makeblock/makeBlock_soundSensor_3.obj"));
			//tmpMat.r = connectorColor[0]; tmpMat.g = connectorColor[1]; tmpMat.b = connectorColor[2]; tmpMat.a = connectorColor[3]; componentMeshes.back()->setMaterial(tmpMat);
			tmpMat.setTextureParam("../data/textures/matcap/glossBlack.bmp", GLContentManager::getTexture("../data/textures/matcap/glossBlack.bmp"));
			componentMeshes.back()->setMaterial(tmpMat);
		}

		//bounding box
		xMax = 0.0120 * 2;
		yMax = 0.0086 * 2;
		zMax = 0.0257 * 2;

		colShapes.push_back(btBoxShape(btVector3((btScalar)(xMax / 2.0), (btScalar)(yMax / 8.0), (btScalar)(zMax / 2.0))));
		colShapeGeometryCenterLocalOffset.push_back(V3D(0, -((yMax / 2.0 - yMax / 4.0) + (yMax / 2.0 - yMax / 4.0) / 2.0), 0));
		colShapes.push_back(btBoxShape(btVector3((btScalar)(xMax / 3.5), (btScalar)(yMax / 2.0), (btScalar)(zMax / 8.0))));
		colShapeGeometryCenterLocalOffset.push_back(V3D(0.0, 0, ((zMax / 2.0 - zMax / 4.0) + (zMax / 2.0 - zMax / 4.0) / 2)));
		colShapes.push_back(btBoxShape(btVector3((btScalar)(0.01 / 2), (btScalar)(yMax / 4), (btScalar)(0.01 / 2))));
		colShapeGeometryCenterLocalOffset.push_back(V3D(0, -(btScalar)(yMax / 4.0), -((zMax / 2.0 - zMax / 4.0) + yMax / 3)));

		// add assembly path and convex hull points
		MakeBlockComponents::initGeometry();

		// support, fasteners and wires
		//supports.push_back(new RigidlyAttachedSupport(this, P3D(0, -yMax / 2.0, 0.0065), AxisAlignedBoundingBox(P3D(-xMax / 2, -0.0025, -0.003), P3D(xMax / 2, 0.0025, 0.003))));

		//fasteners.push_back(new Fastener(this, P3D(-0.008, 0, 0.0065), V3D(0, 1, 0), 0.00205));
		//fasteners.push_back(new Fastener(this, P3D(0.008, 0, 0.0065), V3D(0, 1, 0), 0.00205));

		supports.push_back(new RigidlyAttachedSupport(this, P3D(0, -yMax / 2.0, -0.0014325), AxisAlignedBoundingBox(P3D(-xMax / 2, -0.0025, -0.005), P3D(xMax / 2, 0.0025, 0.005))));

		fasteners.push_back(new Fastener(this, P3D(-0.008, 0, -0.0014325), V3D(0, 1, 0), 0.00205));
		fasteners.push_back(new Fastener(this, P3D(0.008, 0, -0.0014325), V3D(0, 1, 0), 0.00205));
	}

	virtual EMComponent* clone(bool cloneTexture = true) {
		MakeBlockSoundSensor * newObject = new MakeBlockSoundSensor(cloneTexture);
		// copy other properties eg: support sizes..
		return newObject;
	}

	virtual GLMesh* getWireGeometryMesh() {
		P3D pos = this->getWorldCoordinates(P3D() + V3D(0, 0, ((zMax / 2.0 - zMax / 4.0) + (zMax / 2.0 - zMax / 4.0) / 2.0)));
		double a, b, c;
		computeEulerAnglesFromQuaternion(this->getOrientation(), V3D(1, 0, 0), V3D(0, 1, 0), V3D(0, 0, 1), a, b, c);
		a = a * 180 / PI;
		b = b * 180 / PI;
		c = c * 180 / PI;
		double scale = 1.0 + abs(b) / 90.0;

		GLMesh* wireMesh = new GLMesh();
		wireMesh->addCylinder(pos,
			pos + this->getWorldCoordinates(V3D(0, 0, 1)) *0.027*scale, 0.008);

		return wireMesh;
	}
};


/************************** LED ************************************** */
class MakeBlockLED : public MakeBlockComponents {
public:
	// constructor
	MakeBlockLED(bool textureFlag = true) {
		loadMeshAndTexture = textureFlag;
		initGeometry();
	}
	virtual ~MakeBlockLED() {};

	virtual void initGeometry() {
		name = "MakeBlockLED ";
		//componentMeshes.push_back(GLContentManager::getGLMesh("../data/3dModels/makeBlock_LED.obj"));

		if (loadMeshAndTexture) {
			componentMeshes.push_back(GLContentManager::getGLMesh("../data/3dModels/makeblock/makeBlock_LED_1.obj"));
			//tmpMat.r = boardColor[0]; tmpMat.g = boardColor[1]; tmpMat.b = boardColor[2]; tmpMat.a = boardColor[3]; componentMeshes.back()->setMaterial(tmpMat);
			GLShaderMaterial tmpMat;
			tmpMat.setShaderProgram(GLContentManager::getShaderProgram("matcap"));
			tmpMat.setTextureParam("../data/textures/matcap/5.bmp", GLContentManager::getTexture("../data/textures/matcap/5.bmp"));
			componentMeshes.back()->setMaterial(tmpMat);

			componentMeshes.push_back(GLContentManager::getGLMesh("../data/3dModels/makeblock/makeBlock_LED_2.obj"));
			//tmpMat.r = wireColor[0]; tmpMat.g = wireColor[1]; tmpMat.b = wireColor[2]; tmpMat.a = wireColor[3]; componentMeshes.back()->setMaterial(tmpMat);
			tmpMat.setTextureParam("../data/textures/matcap/JG_Gold.bmp", GLContentManager::getTexture("../data/textures/matcap/JG_Gold.bmp"));
			componentMeshes.back()->setMaterial(tmpMat);

			componentMeshes.push_back(GLContentManager::getGLMesh("../data/3dModels/makeblock/makeBlock_LED_3.obj"));
			//tmpMat.r = connectorColor[0]; tmpMat.g = connectorColor[1]; tmpMat.b = connectorColor[2]; tmpMat.a = connectorColor[3]; componentMeshes.back()->setMaterial(tmpMat);
			tmpMat.setTextureParam("../data/textures/matcap/glossBlack.bmp", GLContentManager::getTexture("../data/textures/matcap/glossBlack.bmp"));
			componentMeshes.back()->setMaterial(tmpMat);
		}

		//bounding box
		xMax = 0.0120 * 2;
		yMax = 0.0086 * 2;
		zMax = 0.0257 * 2;

		colShapes.push_back(btBoxShape(btVector3((btScalar)(xMax / 2.0), (btScalar)(yMax / 8.0), (btScalar)(zMax / 2.0))));
		colShapeGeometryCenterLocalOffset.push_back(V3D(0, ((yMax / 2.0 - yMax / 4.0) + (yMax / 2.0 - yMax / 4.0) / 2.0), 0));
		colShapes.push_back(btBoxShape(btVector3((btScalar)(xMax / 3.5), (btScalar)(yMax / 2.0), (btScalar)(zMax / 8.0))));
		colShapeGeometryCenterLocalOffset.push_back(V3D(0.0, 0, ((zMax / 2.0 - zMax / 4.0) + (zMax / 2.0 - zMax / 4.0) / 2)));

		// add assembly path and convex hull points
		MakeBlockComponents::initGeometry();

		// support, fasteners and wires
		supports.push_back(new RigidlyAttachedSupport(this, P3D(0, -yMax / 10, -0.00181), AxisAlignedBoundingBox(P3D(-0.012, -yMax / 2, -0.0025 * 2), P3D(0.012, yMax / 2, 0.0025 * 2))));
		supports.back()->nonResizeableDirections.push_back(V3D(0, 0, 1));

		fasteners.push_back(new Fastener(this, P3D(-0.008, 0.012, -0.00181), V3D(0, 1, 0), 0.00205));
		fasteners.push_back(new Fastener(this, P3D(0.008, 0.012, -0.00181), V3D(0, 1, 0), 0.00205));
	}

	virtual EMComponent* clone(bool cloneTexture = true) {
		MakeBlockLED * newObject = new MakeBlockLED(cloneTexture);
		// copy other properties eg: support sizes..
		return newObject;
	}

	virtual GLMesh* getWireGeometryMesh() {
		P3D pos = this->getWorldCoordinates(P3D() + V3D(0, 0, ((zMax / 2.0 - zMax / 4.0) + (zMax / 2.0 - zMax / 4.0) / 2.0)));
		double a, b, c;
		computeEulerAnglesFromQuaternion(this->getOrientation(), V3D(1, 0, 0), V3D(0, 1, 0), V3D(0, 0, 1), a, b, c);
		a = a * 180 / PI;
		b = b * 180 / PI;
		c = c * 180 / PI;
		double scale = 1.0 + abs(b) / 90.0;

		GLMesh* wireMesh = new GLMesh();
		wireMesh->addCylinder(pos,
			pos + this->getWorldCoordinates(V3D(0, 0, 1)) *0.027*scale, 0.008);

		return wireMesh;
	}
};

/************************** BUTTON ************************************** */
class MakeBlockButton : public MakeBlockComponents {
public:
	// constructor
	MakeBlockButton(bool textureFlag = true) {
		loadMeshAndTexture = textureFlag;
		initGeometry();
	}
	virtual ~MakeBlockButton() {};

	virtual void initGeometry() {
		name = "MakeBlockButton ";
		//componentMeshes.push_back(GLContentManager::getGLMesh("../data/3dModels/makeBlock_button.obj"));

		if (loadMeshAndTexture) {
			GLShaderMaterial tmpMat;
			componentMeshes.push_back(GLContentManager::getGLMesh("../data/3dModels/makeblock/makeBlock_button_2.obj"));
			tmpMat.r = wireColor[0]; tmpMat.g = wireColor[1]; tmpMat.b = wireColor[2]; tmpMat.a = wireColor[3]; componentMeshes.back()->setMaterial(tmpMat);

			componentMeshes.push_back(GLContentManager::getGLMesh("../data/3dModels/makeblock/makeBlock_button_1.obj"));
			//tmpMat.r = boardColor[0]; tmpMat.g = boardColor[1]; tmpMat.b = boardColor[2]; tmpMat.a = boardColor[3]; componentMeshes.back()->setMaterial(tmpMat);
			tmpMat.setShaderProgram(GLContentManager::getShaderProgram("matcap"));
			tmpMat.setTextureParam("../data/textures/matcap/5.bmp", GLContentManager::getTexture("../data/textures/matcap/5.bmp"));
			componentMeshes.back()->setMaterial(tmpMat);

			componentMeshes.push_back(GLContentManager::getGLMesh("../data/3dModels/makeblock/makeBlock_button_3.obj"));
			//tmpMat.r = connectorColor[0]; tmpMat.g = connectorColor[1]; tmpMat.b = connectorColor[2]; tmpMat.a = connectorColor[3]; componentMeshes.back()->setMaterial(tmpMat);
			tmpMat.setTextureParam("../data/textures/matcap/glossBlack.bmp", GLContentManager::getTexture("../data/textures/matcap/glossBlack.bmp"));
			componentMeshes.back()->setMaterial(tmpMat);
		}

		//bounding box
		xMax = 0.0120 * 2;
		yMax = 0.0086 * 2;
		zMax = 0.0257 * 2;

		colShapes.push_back(btBoxShape(btVector3((btScalar)(xMax / 2.0), (btScalar)(yMax / 8.0), (btScalar)(zMax / 2.0))));
		colShapeGeometryCenterLocalOffset.push_back(V3D(0, ((yMax / 2.0 - yMax / 4.0) + (yMax / 2.0 - yMax / 4.0) / 2.0), 0));
		colShapes.push_back(btBoxShape(btVector3((btScalar)(xMax / 3.5), (btScalar)(yMax / 2.0), (btScalar)(zMax / 8.0))));
		colShapeGeometryCenterLocalOffset.push_back(V3D(0.0, 0, ((zMax / 2.0 - zMax / 4.0) + (zMax / 2.0 - zMax / 4.0) / 2)));
		colShapes.push_back(btBoxShape(btVector3((btScalar)(xMax / 2.0), (btScalar)(yMax / 4.0), (btScalar)(zMax / 5.0))));
		colShapeGeometryCenterLocalOffset.push_back(V3D(0, 0.00458, -0.015));

		// add assembly path and convex hull points
		MakeBlockComponents::initGeometry();

		// support, fasteners and wires
		supports.push_back(new RigidlyAttachedSupport(this, P3D(0, -yMax / 10, -0.00181), AxisAlignedBoundingBox(P3D(-0.012, -yMax / 2, -0.0015 * 2), P3D(0.012, yMax / 2, 0.0015 * 2))));
		supports.back()->nonResizeableDirections.push_back(V3D(0, 0, 1));
		supports.back()->nonResizeableDirections.push_back(V3D(0, 0, -1));

		fasteners.push_back(new Fastener(this, P3D(-0.008, 0.012, -0.00181), V3D(0, 1, 0), 0.00205));
		fasteners.push_back(new Fastener(this, P3D(0.008, 0.012, -0.00181), V3D(0, 1, 0), 0.00205));
	}

	virtual EMComponent* clone(bool cloneTexture = true) {
		MakeBlockButton * newObject = new MakeBlockButton(cloneTexture);
		// copy other properties eg: support sizes..
		return newObject;
	}

	virtual GLMesh* getWireGeometryMesh() {
		P3D pos = this->getWorldCoordinates(P3D() + V3D(0, 0, ((zMax / 2.0 - zMax / 4.0) + (zMax / 2.0 - zMax / 4.0) / 2.0)));
		P3D pos1 = this->getWorldCoordinates(P3D() + V3D(0, 0.00458, -0.015));
		double a, b, c;
		computeEulerAnglesFromQuaternion(this->getOrientation(), V3D(1, 0, 0), V3D(0, 1, 0), V3D(0, 0, 1), a, b, c);
		a = a * 180 / PI;
		b = b * 180 / PI;
		c = c * 180 / PI;
		double scale = 1.0 + abs(b) / 90.0;

		GLMesh* wireMesh = new GLMesh();
		wireMesh->addCylinder(pos,
			pos + this->getWorldCoordinates(V3D(0, 0, 1)) *0.027*scale, 0.008);
		wireMesh->addCylinder(pos1,
			pos1 + this->getWorldCoordinates(V3D(0, -1, 0)) *0.027*scale, 0.0115);

		return wireMesh;
	}
};

/************************** MEGA PI CONTROLLER ************************************** */
class MakeBlockMegaPi : public MakeBlockComponents {
public:
	// constructor
	MakeBlockMegaPi(bool textureFlag = true) {
		loadMeshAndTexture = textureFlag;
		initGeometry();
	}
	virtual ~MakeBlockMegaPi() {};

	virtual void initGeometry() {
		name = "MakeBlockMegaPi ";
		//componentMeshes.push_back(GLContentManager::getGLMesh("../data/3dModels/makeBlock_MegaPi.obj"));

		if (loadMeshAndTexture) {
			GLShaderMaterial tmpMat;
			componentMeshes.push_back(GLContentManager::getGLMesh("../data/3dModels/makeblock/makeBlock_MegaPi.obj"));
			tmpMat.setShaderProgram(GLContentManager::getShaderProgram("matcap"));
			tmpMat.setTextureParam("../data/textures/matcap/5.bmp", GLContentManager::getTexture("../data/textures/matcap/5.bmp"));
			componentMeshes.back()->setMaterial(tmpMat);
		}

		//bounding box
		xMax = 0.0865;
		yMax = 0.04;
		zMax = 0.0752;

		colShapes.push_back(btBoxShape(btVector3((btScalar)(xMax / 2.0), (btScalar)(yMax / 4.0), (btScalar)(zMax / 2.0))));
		colShapeGeometryCenterLocalOffset.push_back(V3D(0, (-yMax / 4.0), 0));
		colShapes.push_back(btBoxShape(btVector3((btScalar)(xMax / 4), (btScalar)(yMax / 4), (btScalar)(zMax / 6.0))));
		colShapeGeometryCenterLocalOffset.push_back(V3D(xMax / 4.0, (yMax / 4.0), zMax / 3));
		colShapes.push_back(btBoxShape(btVector3((btScalar)(xMax / 8), (btScalar)(yMax / 4), (btScalar)(zMax / 6.0))));
		colShapeGeometryCenterLocalOffset.push_back(V3D(-xMax / 8.0, (yMax / 4.0), zMax / 3));

		// add assembly path and convex hull points
		MakeBlockComponents::initGeometry();

		// support, fasteners and wires
		supports.push_back(new RigidlyAttachedSupport(this, P3D(0.0395, -yMax / 2, 0), AxisAlignedBoundingBox(P3D(-0.005, -0.0025, -0.0376), P3D(0.005, 0.0025, 0.0376))));
		supports.push_back(new RigidlyAttachedSupport(this, P3D(-0.01825, -yMax / 2, 0), AxisAlignedBoundingBox(P3D(-0.005, -0.0025, -0.0376), P3D(0.005, 0.0025, 0.0376))));

		fasteners.push_back(new Fastener(this, P3D(0.0397, -0.02, 0.0341), V3D(0, 1, 0), 0.00205));
		fasteners.push_back(new Fastener(this, P3D(0.0397, -0.02, -0.015), V3D(0, 1, 0), 0.00205));
		fasteners.push_back(new Fastener(this, P3D(-0.01825, -0.02, 0.0341), V3D(0, 1, 0), 0.00205));
		beta = PI / 2.0;
	}

	virtual EMComponent* clone(bool cloneTexture = true) {
		MakeBlockMegaPi * newObject = new MakeBlockMegaPi(cloneTexture);
		// copy other properties eg: support sizes..
		return newObject;
	}

	virtual GLMesh* getWireGeometryMesh() {
		P3D pos = this->getWorldCoordinates(P3D() + V3D(xMax/2.0, (-yMax / 4.0), zMax/4.0 + zMax/8.0));
		P3D pos1 = this->getWorldCoordinates(P3D() + V3D(xMax / 4.0 + xMax/8.0, (-yMax / 4.0), -(zMax / 2.0)));

		P3D posConnector1 = this->getWorldCoordinates(P3D() + V3D(xMax / 4.0 + xMax / 8.0, (yMax / 4.0), zMax / 3));
		P3D posConnector2 = this->getWorldCoordinates(P3D() + V3D(-xMax / 8.0, (yMax / 4.0), zMax / 3));

		double a, b, c;
		computeEulerAnglesFromQuaternion(this->getOrientation(), V3D(1, 0, 0), V3D(0, 1, 0), V3D(0, 0, 1), a, b, c);
		a = a * 180 / PI;
		b = b * 180 / PI;
		c = c * 180 / PI;
		double scale = 1.0 + abs(b) / 90.0;

		GLMesh* wireMesh = new GLMesh();
		GLMesh* tempMesh = new GLMesh();

		tempMesh->addCylinder(posConnector1,
			posConnector1 + this->getWorldCoordinates(V3D(0, 0, 1)) *0.032, 0.008);
		tempMesh->addCylinder(posConnector2,
			posConnector2 + this->getWorldCoordinates(V3D(0, 0, 1)) *0.032, 0.008);

		ConvexHull3D::computeConvexHullForMesh(tempMesh, wireMesh);

		wireMesh->addCylinder(pos,
			pos + this->getWorldCoordinates(V3D(1, 0, 0)) *0.027*scale, 0.008);
		wireMesh->addCylinder(pos1,
			pos1 + this->getWorldCoordinates(V3D(0, 0, -1)) *0.027*scale, 0.008);

		delete tempMesh;
		return wireMesh;
	}
};