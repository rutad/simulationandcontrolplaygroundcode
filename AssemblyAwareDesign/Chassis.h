#pragma once
# include "BaseObject.h"

class Chassis : public BaseObject {
public:

	GLMesh* chassisMesh = NULL;
	DynamicArray<WallObject> walls;
	DynamicArray<int> isWallReal;
	DynamicArray<V3D> validAssemblyDirs;
	//DynamicArray<std::pair<double,double>> validAssemblyDirs;

	// chassis bounding box dimension
	double xMax = 1.0f;
	double yMax = 1.0f;
	double zMax = 1.0f;

	Chassis() {
		name = "Chassis";
	};
	virtual ~Chassis() {};

	virtual void drawObjectGeometry(GLShaderMaterial* material = NULL);

	virtual void getSilhouetteScalingDimensions(double &dimX, double &dimY, double &dimZ) {

		// more accurate that using bounding box for volume calculation..
		// this is assuming cuboid
		if (walls.size() == 6) {
			double dim[3] = { 0,0,0 };
			int count = 0;
			for (size_t i = 0; i < walls.size();)
			{
				dim[count] = (V3D(walls[i].wallCenter).length() + V3D(walls[i + 1].wallCenter).length()) / 2.0;
				count++;
				i = i + 2;
			}

			dimY = dim[0], dimZ = dim[1], dimX = dim[2];
		}
		else {
			dimZ = zMax / 2.0;
			dimX = xMax / 2.0;
			dimY = yMax / 2.0;
		}
	}

	virtual bool shouldCollideWith(BaseObject* obj) {
		return true;
	}

};

class TrapezoidChassis : public Chassis {
public:

	// constructor
	TrapezoidChassis() : Chassis() {
		init();
	}
	virtual ~TrapezoidChassis() {};

	void init() {
		name = "TrapezoidChassis";

		chassisMesh = GLContentManager::getGLMesh("../data/3dModels/trapezoid.obj");

		//bounding box
		xMax = 0.0603 * 2;
		yMax = 0.05 * 2;
		zMax = 0.1650 * 2;

		// top fictitious wall
		walls.push_back(WallObject(P3D(0, yMax / 2, 0), V3D(0, -1, 0), P3D(zMax / 2, yMax / 2, zMax / 2)));
		isWallReal.push_back(0);
		validAssemblyDirs.push_back(V3D(0, 1, 0));
		//validAssemblyDirs.push_back(std::pair<double,double>(0,0));

		// bottom fictitious wall
		walls.push_back(WallObject(P3D(0, -yMax / 2, 0), V3D(0, 1, 0), P3D(xMax / 2, yMax / 2, xMax / 2)));
		isWallReal.push_back(0);
		validAssemblyDirs.push_back(V3D(0, -1, 0));

		// side real walls
		walls.push_back(WallObject(P3D(0, 0, zMax / 2 - yMax/2), V3D(0, 0, -1).rotate(PI/4, V3D(1,0,0)), P3D(xMax / 2, yMax / 2, zMax / 2)));
		isWallReal.push_back(1);
		walls.push_back(WallObject(P3D(0, 0, -zMax / 2 + yMax / 2), V3D(0, 0, 1).rotate(PI / 4, V3D(-1, 0, 0)), P3D(xMax / 2, yMax / 2, zMax / 2)));
		isWallReal.push_back(1);

		// front and back real walls
		walls.push_back(WallObject(P3D(-yMax / 2 + zMax / 2, 0, 0), V3D(-1, 0, 0).rotate(-PI / 4, V3D(0, 0, 1)), P3D(xMax / 2, yMax / 2, zMax / 2)));
		isWallReal.push_back(1);
		//validAssemblyDirs.push_back(V3D(1, 0, 0));
		//validAssemblyDirs.push_back(std::pair<double, double>(0, -acos(0.0)));
		walls.push_back(WallObject(P3D(yMax / 2 - zMax / 2, 0, 0), V3D(1, 0, 0).rotate(-PI / 4, V3D(0, 0, -1)), P3D(xMax / 2, yMax / 2, zMax / 2)));
		isWallReal.push_back(1);
		//validAssemblyDirs.push_back(V3D(-1, 0, 0));
		//validAssemblyDirs.push_back(std::pair<double, double>(0, acos(0.0)));
	}
};

class TrapezoidFlippedChassis : public Chassis {
public:

	// constructor
	TrapezoidFlippedChassis() : Chassis() {
		init();
	}
	virtual ~TrapezoidFlippedChassis() {};

	void init() {
		name = "TrapezoidFlippedChassis";

		chassisMesh = GLContentManager::getGLMesh("../data/3dModels/trapezoidFlipped.obj");

		//bounding box
		xMax = 0.0603 * 2;
		yMax = 0.05 * 2;
		zMax = 0.1650 * 2;

		// top fictitious wall
		walls.push_back(WallObject(P3D(0, yMax / 2, 0), V3D(0, -1, 0), P3D(xMax / 2, yMax / 2, xMax / 2)));
		isWallReal.push_back(0);
		validAssemblyDirs.push_back(V3D(0, 1, 0));
		//validAssemblyDirs.push_back(std::pair<double,double>(0,0));

		// bottom fictitious wall
		walls.push_back(WallObject(P3D(0, -yMax / 2 , 0), V3D(0, 1, 0), P3D(zMax / 2, yMax / 2, zMax / 2)));
		isWallReal.push_back(0);
		validAssemblyDirs.push_back(V3D(0, -1, 0));

		// side real walls
		walls.push_back(WallObject(P3D(0, 0, zMax / 2 - yMax / 2), V3D(0, 0, -1).rotate(-PI / 4, V3D(1, 0, 0)), P3D(zMax / 2 - yMax / 2, yMax / 2, zMax / 2 - yMax / 2)));
		isWallReal.push_back(1);
		walls.push_back(WallObject(P3D(0, 0, -zMax / 2 + yMax / 2), V3D(0, 0, 1).rotate(-PI / 4, V3D(-1, 0, 0)), P3D(-zMax / 2 + yMax / 2, -yMax / 2, -zMax / 2 + yMax / 2)));
		isWallReal.push_back(1);

		// front and back real walls
		walls.push_back(WallObject(P3D(-yMax / 2 + zMax / 2, 0, 0), V3D(-1, 0, 0).rotate(PI / 4, V3D(0, 0, 1)), P3D(-yMax / 2 + zMax / 2, -yMax / 2, -yMax / 2 + zMax / 2)));
		isWallReal.push_back(1);
		////validAssemblyDirs.push_back(V3D(1, 0, 0));
		////validAssemblyDirs.push_back(std::pair<double, double>(0, -acos(0.0)));
		walls.push_back(WallObject(P3D(yMax / 2 - zMax / 2, 0, 0), V3D(1, 0, 0).rotate(PI / 4, V3D(0, 0, -1)), P3D(yMax / 2 - zMax / 2, yMax / 2, yMax / 2 - zMax / 2)));
		isWallReal.push_back(1);
		//validAssemblyDirs.push_back(V3D(-1, 0, 0));
		//validAssemblyDirs.push_back(std::pair<double, double>(0, acos(0.0)));
	}
};


class WallEChassis : public Chassis {
public:

	// constructor
	WallEChassis() : Chassis() {
		init();
	}
	virtual ~WallEChassis() {};

	void init() {
		name = "WallEChassis";

		//chassisMesh = GLContentManager::getGLMesh("../data/3dModels/wallEChassis_medium_v3.obj");
		chassisMesh = GLContentManager::getGLMesh("../data/3dModels/wallEChassis_medium_v3_wShaft.obj");

		//bounding box
		xMax = 0.0603 * 2;
		yMax = 0.0325 * 2;
		zMax = 0.0655 * 2;

		// top fictitious wall
		walls.push_back(WallObject(P3D(0, yMax / 2, 0), V3D(0, -1, 0), P3D(xMax / 2, yMax / 2, zMax / 2)));
		isWallReal.push_back(0);
		validAssemblyDirs.push_back(V3D(0, 1, 0));
		//validAssemblyDirs.push_back(std::pair<double,double>(0,0));

		// bottom real wall
		walls.push_back(WallObject(P3D(0, -yMax / 2 + 0.005, 0), V3D(0, 1, 0), P3D(xMax / 2, yMax / 2, zMax / 2)));
		isWallReal.push_back(1);

		// side real walls
		walls.push_back(WallObject(P3D(0, 0, zMax / 2 - 0.008), V3D(0, 0, -1), P3D(xMax / 2, yMax / 2, zMax / 2)));
		isWallReal.push_back(1);
		walls.push_back(WallObject(P3D(0, 0, -zMax / 2 + 0.008), V3D(0, 0, 1), P3D(xMax / 2, yMax / 2, zMax / 2)));
		isWallReal.push_back(1);

		// front and back fictitious walls
		walls.push_back(WallObject(P3D(-xMax / 2, 0, 0), V3D(1, 0, 0), P3D(xMax / 2, yMax / 2, zMax / 2)));
		isWallReal.push_back(0);
		validAssemblyDirs.push_back(V3D(1, 0, 0));
		//validAssemblyDirs.push_back(std::pair<double, double>(0, -acos(0.0)));
		walls.push_back(WallObject(P3D(xMax / 2, 0, 0), V3D(-1, 0, 0), P3D(xMax / 2, yMax / 2, zMax / 2)));
		isWallReal.push_back(0);
		validAssemblyDirs.push_back(V3D(-1, 0, 0));
		//validAssemblyDirs.push_back(std::pair<double, double>(0, acos(0.0)));
	}
};

class BirdieChassis : public Chassis {
public:

	// constructor
	BirdieChassis() : Chassis() {
		init();
	}
	virtual ~BirdieChassis() {};

	void init() {
		name = "BirdieChassis";

		chassisMesh = GLContentManager::getGLMesh("../data/3dModels/birdie.obj");

		//bounding box
		xMax = 0.05 * 2;
		yMax = 0.0397 * 2;
		zMax = 0.0655 * 2;

		// top fictitious wall
		walls.push_back(WallObject(P3D(0, yMax / 2, 0), V3D(0, -1, 0), P3D(xMax/2, yMax/2, zMax/2)));
		isWallReal.push_back(0);
		validAssemblyDirs.push_back(V3D(0, 1, 0));
		//validAssemblyDirs.push_back(std::pair<double,double>(0,0));

		// bottom real wall
		walls.push_back(WallObject(P3D(0, - 0.01, 0), V3D(0, 1, 0), P3D(xMax / 2, yMax / 2, zMax / 2)));
		isWallReal.push_back(1);

		// side real walls
		walls.push_back(WallObject(P3D(0, 0, zMax / 2 ), V3D(0, 0, -1), P3D(xMax / 2, yMax / 2, zMax / 2)));
		isWallReal.push_back(1);
		walls.push_back(WallObject(P3D(0, 0, -zMax / 2 + 0.02), V3D(0, 0, 1), P3D(xMax / 2, yMax / 2, zMax / 2)));
		isWallReal.push_back(1);

		// front and back real walls
		walls.push_back(WallObject(P3D(-xMax / 2 , 0, 0), V3D(1, 0, 0), P3D(xMax / 2, yMax / 2, zMax / 2)));
		isWallReal.push_back(1);
		//validAssemblyDirs.push_back(V3D(1, 0, 0));
		//validAssemblyDirs.push_back(std::pair<double, double>(0, -acos(0.0)));
		walls.push_back(WallObject(P3D(xMax / 2 - 0.01, 0, 0), V3D(-1, 0, 0), P3D(xMax / 2, yMax / 2, zMax / 2)));
		isWallReal.push_back(1);
		//validAssemblyDirs.push_back(V3D(-1, 0, 0));
		//validAssemblyDirs.push_back(std::pair<double, double>(0, acos(0.0)));
	}
};

class OwlChassis : public Chassis {
public:

	// constructor
	OwlChassis() : Chassis() {
		init();
	}
	virtual ~OwlChassis() {};

	void init() {
		name = "OwlChassis";

		chassisMesh = GLContentManager::getGLMesh("../data/3dModels/owlChassis_v2.obj");

		//bounding box
		xMax = 0.09795 * 2;
		yMax = 0.03150 * 2; // 0.03850 * 2;
		zMax = 0.08273 * 2;

		// cover fictitious wall
		walls.push_back(WallObject(P3D(0, yMax / 2 , 0), V3D(0, -1, 0), P3D(xMax / 2, yMax / 2, zMax / 2)));
		isWallReal.push_back(0);
		validAssemblyDirs.push_back(V3D(0, 1, 0));
		//validAssemblyDirs.push_back(std::pair<double, double>(0, 0));

		// bottom face side real wall
		walls.push_back(WallObject(P3D(0, -yMax / 2 + 0.005, 0), V3D(0, 1, 0), P3D(xMax / 2, yMax / 2, zMax / 2)));
		isWallReal.push_back(1);

		// side fictitious walls
		walls.push_back(WallObject(P3D(0, 0, zMax / 2 - 0.008), V3D(0, 0, -1), P3D(xMax / 2, yMax / 2, zMax / 2)));
		isWallReal.push_back(0);
		validAssemblyDirs.push_back(V3D(0, 0, -1));
		//validAssemblyDirs.push_back(std::pair<double, double>(-acos(0.0), 0));
		walls.push_back(WallObject(P3D(0, 0, -zMax / 2 + 0.008), V3D(0, 0, 1), P3D(xMax / 2, yMax / 2, zMax / 2)));
		isWallReal.push_back(0);
		validAssemblyDirs.push_back(V3D(0, 0, 1));
		//validAssemblyDirs.push_back(std::pair<double, double>(acos(0.0), 0));

		// top and bottom real walls
		walls.push_back(WallObject(P3D(-0.05886, 0, 0), V3D(1, 0, 0), P3D(xMax / 2, yMax / 2, zMax / 2)));
		isWallReal.push_back(1);
		walls.push_back(WallObject(P3D(0.06434, 0, 0), V3D(-1, 0, 0), P3D(xMax / 2, yMax / 2, zMax / 2)));
		isWallReal.push_back(1);
	}
};

class BalanceBotChassis : public Chassis {
public:

	// constructor
	BalanceBotChassis() : Chassis() {
		init();
	}
	virtual ~BalanceBotChassis() {};

	void init() {
		name = "BalanceBotChassis";

		chassisMesh = GLContentManager::getGLMesh("../data/3dModels/balanceBotChassis_v3.obj");

		//bounding box
		xMax = 0.05 * 2;
		yMax = 0.0425 * 2 + 0.01;//0.0425 * 2;
		zMax = 0.05 * 2;

		// top fictitious wall
		walls.push_back(WallObject(P3D(0, yMax / 2 + 0.005, 0), V3D(0, -1, 0), P3D(xMax / 2, yMax / 2, zMax / 2)));
		isWallReal.push_back(0);
		validAssemblyDirs.push_back(V3D(0, 1, 0));
		//validAssemblyDirs.push_back(std::pair<double,double>(0,0));

		// bottom real wall
		walls.push_back(WallObject(P3D(0, -yMax / 2 , 0), V3D(0, 1, 0), P3D(xMax / 2, yMax / 2, zMax / 2)));
		isWallReal.push_back(1);

		// front and back fictitious walls
		walls.push_back(WallObject(P3D(0, 0, -zMax / 2), V3D(0, 0, 1), P3D(xMax / 2, yMax / 2, zMax / 2)));
		isWallReal.push_back(0);
		validAssemblyDirs.push_back(V3D(0, 0, 1));
		//validAssemblyDirs.push_back(std::pair<double, double>(0, -acos(0.0)));
		walls.push_back(WallObject(P3D(0, 0, zMax / 2), V3D(0, 0, -1), P3D(xMax / 2, yMax / 2, zMax / 2)));
		isWallReal.push_back(0);
		validAssemblyDirs.push_back(V3D(0, 0, -1));
		//validAssemblyDirs.push_back(std::pair<double, double>(0, acos(0.0)));

		// side real walls
		walls.push_back(WallObject(P3D( xMax / 2 - 0.0075, 0, 0), V3D(-1, 0, 0), P3D(xMax / 2, yMax / 2, zMax / 2)));
		isWallReal.push_back(1);
		walls.push_back(WallObject(P3D( -xMax / 2 + 0.0075, 0, 0), V3D(1, 0, 0), P3D(xMax / 2, yMax / 2, zMax / 2)));
		isWallReal.push_back(1);
	}
};

class VirtualChassis : public Chassis {
public:

	double currentScaleFactor = 1.0;
	double lengthScale = 0.1;

	// constructor
	VirtualChassis() : Chassis() {

		//bounding box
		xMax = lengthScale*sqrt(2);
		yMax = xMax / 2;//0.1 / 2;
		zMax = lengthScale*sqrt(2);

		init();
	}
	virtual ~VirtualChassis() {};

	void init() {
		name = "VirtualChassis";

		chassisMesh = GLContentManager::getGLMesh("../data/3dModels/virtualbox.obj");

		// top fictitious wall
		walls.push_back(WallObject(P3D(0, yMax / 2, 0), V3D(0, -1, 0), P3D(xMax / 2, yMax / 2, zMax / 2)));
		isWallReal.push_back(0);
		validAssemblyDirs.push_back(V3D(0, 1, 0));
		//validAssemblyDirs.push_back(std::pair<double,double>(0,0));

		// bottom real wall
		walls.push_back(WallObject(P3D(0, -yMax / 2 , 0), V3D(0, 1, 0), P3D(xMax / 2, yMax / 2, zMax / 2)));
		isWallReal.push_back(1);

		// side real walls
		walls.push_back(WallObject(P3D(0, 0, zMax / 2 ), V3D(0, 0, -1), P3D(xMax / 2, yMax / 2, zMax / 2)));
		isWallReal.push_back(1);
		walls.push_back(WallObject(P3D(0, 0, -zMax / 2 ), V3D(0, 0, 1), P3D(xMax / 2, yMax / 2, zMax / 2)));
		isWallReal.push_back(1);

		// front and back fictitious walls
		walls.push_back(WallObject(P3D(-xMax / 2, 0, 0), V3D(1, 0, 0), P3D(xMax / 2, yMax / 2, zMax / 2)));
		isWallReal.push_back(0);
		validAssemblyDirs.push_back(V3D(1, 0, 0));
		//validAssemblyDirs.push_back(std::pair<double, double>(0, -acos(0.0)));
		walls.push_back(WallObject(P3D(xMax / 2, 0, 0), V3D(-1, 0, 0), P3D(xMax / 2, yMax / 2, zMax / 2)));
		isWallReal.push_back(0);
		validAssemblyDirs.push_back(V3D(-1, 0, 0));
		//validAssemblyDirs.push_back(std::pair<double, double>(0, acos(0.0)));
	}

	virtual void drawObjectGeometry(GLShaderMaterial* material) {
		GLShaderMaterial tmpMat;
		GLShaderMaterial *matToUse = (material) ? material : &tmpMat;

		if (material == NULL) {
			tmpMat.r = 1; tmpMat.g = 1; tmpMat.b = 1; tmpMat.a = 0.2;
			//tmpMat.setShaderProgram(GLContentManager::getShaderProgram("matcap"));
			//tmpMat.setTextureParam(".. / data / textures / matcap / MatCap_0005.bmp", GLContentManager::getTexture("../data/textures/matcap/MatCap_0005.bmp"));
		}

		//matToUse->apply();
		//drawBox(P3D(-xMax / 2, -yMax / 2, -zMax / 2), P3D(xMax / 2, yMax / 2, zMax / 2));
		//matToUse->end();

		if (material == NULL) {
			tmpMat.r = 1; tmpMat.g = 1; tmpMat.b = 1; tmpMat.a = 0.4;
			//tmpMat.setShaderProgram(GLContentManager::getShaderProgram("matcap"));
			//tmpMat.setTextureParam(".. / data / textures / matcap / MatCap_0005.bmp", GLContentManager::getTexture("../data/textures/matcap/MatCap_0005.bmp"));
		}

		chassisMesh->setMaterial(*matToUse);
		chassisMesh->drawMesh();
	}

	virtual void scaleChassis(double scaleFactor) {
		walls.clear();
		validAssemblyDirs.clear();
		isWallReal.clear();

		xMax = lengthScale*sqrt(2);
		yMax = xMax / 2;//0.1 / 2;
		zMax = lengthScale*sqrt(2);

		xMax = xMax / pow(scaleFactor, 1.0 / 3.0);
		yMax = yMax / pow(scaleFactor, 1.0 / 3.0);
		zMax = zMax / pow(scaleFactor, 1.0 / 3.0);

		currentScaleFactor = scaleFactor;
		init();
	}

	virtual void updateSizeChassis(double len) {
		walls.clear();
		validAssemblyDirs.clear();
		isWallReal.clear();

		this->lengthScale = len;
		xMax = lengthScale*sqrt(2);
		yMax = xMax / 2;//0.1 / 2;
		zMax = lengthScale*sqrt(2);

		init();
	}
};

class HexagonChassis : public Chassis {
public:

	// constructor
	HexagonChassis() : Chassis() {
		init();
	}
	virtual ~HexagonChassis() {};

	void init() {
		name = "HexagonChassis";

		chassisMesh = GLContentManager::getGLMesh("../data/3dModels/hexagon.obj");

		//bounding box
		xMax = 0.11 * 2;
		yMax = 0.0525 * 2;
		zMax = 0.0953 * 2;

		// top fictitious wall
		walls.push_back(WallObject(P3D(0, yMax / 2, 0), V3D(0, -1, 0), P3D(xMax / 2, yMax / 2, xMax / 2)));
		isWallReal.push_back(0);
		validAssemblyDirs.push_back(V3D(0, 1, 0));

		// bottom real wall
		walls.push_back(WallObject(P3D(0, -yMax / 2, 0), V3D(0, 1, 0), P3D(zMax / 2, yMax / 2, zMax / 2)));
		isWallReal.push_back(1);

		// side real walls
		walls.push_back(WallObject(P3D( 0.08250, 0, -0.04763), V3D(-1, 0, 0).rotate(PI / 6, V3D(0, 1, 0)), P3D(xMax/2, yMax/2, zMax/2))); 
		isWallReal.push_back(1);
		walls.push_back(WallObject(P3D(-0.08250, 0, -0.04763), V3D(1, 0, 0).rotate(-PI / 6, V3D(0, 1, 0)), P3D(xMax / 2, yMax / 2, zMax / 2)));
		isWallReal.push_back(1);

		// side real walls
		walls.push_back(WallObject(P3D(0.08250, 0, 0.04763), V3D(-1, 0, 0).rotate(-PI / 6, V3D(0, 1, 0)), P3D(xMax / 2, yMax / 2, zMax / 2)));
		isWallReal.push_back(1);
		walls.push_back(WallObject(P3D(-0.08250, 0, 0.04763), V3D(1, 0, 0).rotate(PI / 6, V3D(0, 1, 0)), P3D(xMax / 2, yMax / 2, zMax / 2)));
		isWallReal.push_back(1);

		walls.push_back(WallObject(P3D(0, 0, -0.08660), V3D(0, 0, 1), P3D(xMax / 2, yMax / 2, zMax / 2)));
		isWallReal.push_back(1);
		walls.push_back(WallObject(P3D(0, 0, 0.08660), V3D(0, 0, -1), P3D(xMax / 2, yMax / 2, zMax / 2)));
		isWallReal.push_back(0);
		validAssemblyDirs.push_back(V3D(0, 0, 1));

		// front and back real walls
		//walls.push_back(WallObject(P3D(-yMax / 2 + zMax / 2, 0, 0), V3D(-1, 0, 0).rotate(PI / 4, V3D(0, 0, 1)), P3D(-yMax / 2 + zMax / 2, -yMax / 2, -yMax / 2 + zMax / 2)));
		//isWallReal.push_back(1);
		////validAssemblyDirs.push_back(V3D(1, 0, 0));
		////validAssemblyDirs.push_back(std::pair<double, double>(0, -acos(0.0)));
		//walls.push_back(WallObject(P3D(yMax / 2 - zMax / 2, 0, 0), V3D(1, 0, 0).rotate(PI / 4, V3D(0, 0, -1)), P3D(yMax / 2 - zMax / 2, yMax / 2, yMax / 2 - zMax / 2)));
		//isWallReal.push_back(1);
		//validAssemblyDirs.push_back(V3D(-1, 0, 0));
		//validAssemblyDirs.push_back(std::pair<double, double>(0, acos(0.0)));
	}
};

class BunnyChassis : public Chassis {
public:

	// constructor
	BunnyChassis() : Chassis() {
		init();
	}
	virtual ~BunnyChassis() {};

	void init() {
		name = "BunnyChassis";

		chassisMesh = GLContentManager::getGLMesh("../data/3dModels/bunnyChassis.obj");

		//bounding box
		xMax = 0.041 * 2;
		yMax = 0.028 * 2;
		zMax = 0.027 * 2;

		// top fictitious wall
		walls.push_back(WallObject(P3D(0, yMax / 2, 0), V3D(0, -1, 0), P3D(xMax / 2, yMax / 2, zMax / 2)));
		isWallReal.push_back(0);
		validAssemblyDirs.push_back(V3D(0, 1, 0));

		// bottom real wall
		walls.push_back(WallObject(P3D(0, -yMax / 2 - 0.02, 0), V3D(0, 1, 0), P3D(xMax / 2, yMax / 2, zMax / 2)));
		isWallReal.push_back(1);

		// side real walls
		walls.push_back(WallObject(P3D(0, 0, zMax / 2), V3D(0, 0, -1), P3D(xMax / 2, yMax / 2, zMax / 2)));
		isWallReal.push_back(1);
		walls.push_back(WallObject(P3D(0, 0, -zMax / 2), V3D(0, 0, 1), P3D(xMax / 2, yMax / 2, zMax / 2)));
		isWallReal.push_back(1);

		// front and back real walls
		walls.push_back(WallObject(P3D(-xMax / 2, 0, 0), V3D(1, 0, 0), P3D(xMax / 2, yMax / 2, zMax / 2)));
		isWallReal.push_back(1);
		walls.push_back(WallObject(P3D(xMax / 2 + 0.01, 0, 0), V3D(-1, 0, 0), P3D(xMax / 2, yMax / 2, zMax / 2)));
		isWallReal.push_back(0);
		validAssemblyDirs.push_back(V3D(1, 0, 0));

	}
};

class CloudChassis : public Chassis {
public:

	// constructor
	CloudChassis() : Chassis() {
		init();
	}
	virtual ~CloudChassis() {};

	void init() {
		name = "CloudChassis";

		chassisMesh = GLContentManager::getGLMesh("../data/3dModels/cloudChassis.obj");

		//bounding box
		xMax = 0.041 * 2;
		yMax = 0.028 * 2;
		zMax = 0.027 * 2;

		// top fictitious wall
		walls.push_back(WallObject(P3D(0, yMax / 2, 0), V3D(0, -1, 0), P3D(xMax / 2, yMax / 2, zMax / 2)));
		isWallReal.push_back(0);
		validAssemblyDirs.push_back(V3D(0, 1, 0));

		// bottom real wall
		walls.push_back(WallObject(P3D(0, -yMax / 2 - 0.02, 0), V3D(0, 1, 0), P3D(xMax / 2, yMax / 2, zMax / 2)));
		isWallReal.push_back(1);

		// side real walls
		walls.push_back(WallObject(P3D(0, 0, zMax / 2), V3D(0, 0, -1), P3D(xMax / 2, yMax / 2, zMax / 2)));
		isWallReal.push_back(1);
		walls.push_back(WallObject(P3D(0, 0, -zMax / 2), V3D(0, 0, 1), P3D(xMax / 2, yMax / 2, zMax / 2)));
		isWallReal.push_back(1);

		// front and back real walls
		walls.push_back(WallObject(P3D(-xMax / 2, 0, 0), V3D(1, 0, 0), P3D(xMax / 2, yMax / 2, zMax / 2)));
		isWallReal.push_back(1);
		walls.push_back(WallObject(P3D(xMax / 2 + 0.01, 0, 0), V3D(-1, 0, 0), P3D(xMax / 2, yMax / 2, zMax / 2)));
		isWallReal.push_back(1);

	}
};

class TrainingChassis : public Chassis {
public:

	// constructor
	TrainingChassis() : Chassis() {
		init();
	}
	virtual ~TrainingChassis() {};

	void init() {
		name = "TrainingChassis";

		chassisMesh = GLContentManager::getGLMesh("../data/3dModels/testChassis_box.obj");

		//bounding box
		xMax = 0.0525 * 2;
		yMax = 0.05 * 2;
		zMax = 0.05 * 2;

		// top fictitious wall
		walls.push_back(WallObject(P3D(0, yMax / 2, 0), V3D(0, -1, 0), P3D(xMax / 2, yMax / 2, zMax / 2)));
		isWallReal.push_back(0);
		validAssemblyDirs.push_back(V3D(0, 1, 0));

		// bottom real wall
		walls.push_back(WallObject(P3D(0, -yMax / 2, 0), V3D(0, 1, 0), P3D(xMax / 2, yMax / 2, zMax / 2)));
		isWallReal.push_back(0);

		// side real walls
		walls.push_back(WallObject(P3D(0, 0, zMax / 2 - 0.005), V3D(0, 0, -1), P3D(xMax / 2, yMax / 2, zMax / 2)));
		isWallReal.push_back(1);
		walls.push_back(WallObject(P3D(0, 0, -zMax / 2 + 0.005), V3D(0, 0, 1), P3D(xMax / 2, yMax / 2, zMax / 2)));
		isWallReal.push_back(1);

		// front and back fictitious walls
		walls.push_back(WallObject(P3D(-xMax / 2 + 0.005, 0, 0), V3D(1, 0, 0), P3D(xMax / 2, yMax / 2, zMax / 2)));
		isWallReal.push_back(1);
		validAssemblyDirs.push_back(V3D(1, 0, 0));

		walls.push_back(WallObject(P3D(xMax / 2 - 0.005, 0, 0), V3D(-1, 0, 0), P3D(xMax / 2, yMax / 2, zMax / 2)));
		isWallReal.push_back(1);
		validAssemblyDirs.push_back(V3D(-1, 0, 0));

	}
};