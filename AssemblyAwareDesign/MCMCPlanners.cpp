# include "MCMCPlanners.h"
#include "assembly.h"
#include "Support.h"
#include "MotorComponents.h"

MCMCPlannerState::MCMCPlannerState(MCMCPlanners* inputPlanner) {
	this->planner = inputPlanner;
}

void MCMCPlannerState::initializePlannerState(int seed) {

	//start with geometric temperatures -- base temp derived using betaMin and numChains.
	double baseTemp = exp(log(1 / betaMin) / (numberOfChains - 1));
	planner->setGeometricBetasAndChainInitialStates(numberOfChains, seed, baseTemp, currentChainStates, currentChainBetas,
		mode, currentChainBestStates);

	currentChainBestStates = currentChainStates;
	planner->copyAssemblyStateToInputAssembly(planner->inputAssembly, currentChainBestStates[0]);

	for (int i = 0; i < numberOfChains; i++) {
		currentChainWiseAcceptanceRates.push_back(std::pair<int, double>(0, 0));
		currentChainProposalBandwidth.push_back(0.05);
		if (i>0)
			chainPairSwapAcceptanceRates.push_back(std::pair<int, int>(0, 0));
	}

	initialized = true;
	initializationCounter = initializationCounter + 1;
}

void MCMCPlannerState::resetPlannerState() {
	if (currentChainStates.size() > 0) {
		currentChainBetas.clear();
		currentChainStates.clear();
		currentChainProposalBandwidth.clear();
		currentChainWiseAcceptanceRates.clear();
		chainPairSwapAcceptanceRates.clear();
		currentChainBestStates.clear();
		iterations = 0;
		swapAcceptanceCount = 0;
		converged = false;
		indicator = false;
	}
}

MCMCPlanners::MCMCPlanners(Assembly* assembly) : AssemblyPlanner(assembly) {
	plannerState = new MCMCPlannerState(this);
}


MCMCPlanners::MCMCPlanners(Assembly* assembly, DynamicArray<AssemblyState> initChainStates) : AssemblyPlanner(assembly) {
	plannerState = new MCMCPlannerState(this);
	plannerState->prevChainStates = initChainStates;
}


//MCMCPlanners::MCMCPlanners(Assembly* assembly, PlotWindow* plot) : SamplingBasedAssemblyPlanner(assembly, plot) {
//}

void MCMCPlanners::setUpPlanner(int seed) {
	AssemblyPlanner::setUpPlanner(seed);
	plannerState->initializePlannerState(seed);
}

double MCMCPlanners::searchOrderAndPlacementWithParallelTemperedMH_adaptive(int seed, int& statesEvaluated,
	int numSamples, int numberOfChains, double betaMin, bool printDebug) {
	// Reference: Schug et. al. (2004)-- http://onlinelibrary.wiley.com/doi/10.1002/prot.20290/epdf

	/* MCMC diagnostic values stored for plotting in matlab*/
	FILE *fp;
	fopen_s(&fp, "mcmc.csv", "w");

	/* Gradually adapt temperatures that give 23% swap acceptance between chains.*/
	// temperatures 
	double betaMax = 1;
	if (betaMin == -1)
		betaMin = 0.01;

	if (numberOfChains == 0)
		numberOfChains = 10;

	DynamicArray<AssemblyState> currentChainStates;
	DynamicArray<double> currentChainBetas;
	DynamicArray<std::pair<int, double>>currentChainWiseAcceptanceRates;
	DynamicArray<std::pair<int, int>>chainPairSwapAcceptanceRates;
	DynamicArray<double> currentChainProposalBandwidth;

	//start with geometric temperatures -- base temp derived using betaMin and numChains.
	double baseTemp = exp(log(1 / betaMin) / (numberOfChains - 1));
	setGeometricBetasAndChainInitialStates(numberOfChains, seed, baseTemp, currentChainStates, currentChainBetas);

	/* Set up */
	// number of samples per chain
	if (numSamples == 0)
		numSamples = 500; //2500;
	statesEvaluated = 0;
	int swapAcceptanceCount = 0;
	double swapAcceptRate = 0;
	double avgSwapRate = 0;

	AssemblyState bestState = currentChainStates[0];

	// a uniform random generator to create swap acceptance probability
	std::uniform_real_distribution<double>  unifRand(0, 1);
	std::default_random_engine uniRe(seed);
	std::uniform_int_distribution<int> chainIndDistribution(0, numberOfChains - 2);

	for (int i = 0; i < numberOfChains; i++) {
		currentChainWiseAcceptanceRates.push_back(std::pair<int, double>(0, 0));
		currentChainProposalBandwidth.push_back(0.2);
		if(i>0)
			chainPairSwapAcceptanceRates.push_back(std::pair<int, int>(0, 0));
		if (printDebug)
			fprintf(fp, "%d,", i);
	}
	if (printDebug)
		fprintf(fp, "%d \n", numberOfChains - 1);

	for (int i = 0; i < numSamples; i++) {
# pragma omp parallel for
		for (int j = 0; j < numberOfChains; j++) {
			parallelTemperingStep_adaptive(j, i, currentChainStates[j], currentChainBetas[j], currentChainWiseAcceptanceRates[j],
				currentChainProposalBandwidth[j]);
			//parallelTemperingMALAStep_adaptive(j, i, currentChainStates[j], currentChainBetas[j]);
			statesEvaluated = statesEvaluated + 1;
		}

		/**swap states of randomly chosen chains**/
		//select chains to swap
		int chainId1 = chainIndDistribution(uniRe);
		int chainId2 = chainId1 + 1;
		// calculate swap probability
		double alphaSwap = calculateSwapProbabilityForAdaptiveParallelTempering(chainId1, chainId2, currentChainStates, currentChainBetas);

		// do the swap
		if (unifRand(uniRe) <= alphaSwap) {
			// accept 
			swap(currentChainStates[chainId1], currentChainStates[chainId2]);
			chainPairSwapAcceptanceRates[chainId1] = std::pair<int, int>(std::get<0>(chainPairSwapAcceptanceRates[chainId1]) + 1,
				std::get<1>(chainPairSwapAcceptanceRates[chainId1]) + 1);
			swapAcceptanceCount = swapAcceptanceCount + 1;
		}
		else {
			chainPairSwapAcceptanceRates[chainId1] = std::pair<int, int>(std::get<0>(chainPairSwapAcceptanceRates[chainId1]),
				std::get<1>(chainPairSwapAcceptanceRates[chainId1]) + 1);
		}

		swapAcceptRate = (double)swapAcceptanceCount / (double)(i + 1);
		avgSwapRate += swapAcceptRate;

		printChainsSwapRates(i, chainPairSwapAcceptanceRates);
		if (printDebug)
			Logger::logPrint("\nitr number:%d, swap acceptanceRate:%lf\n", i, swapAcceptRate);

		/**Check if we found a good enough state**/
		for (int k = 0; k < numberOfChains; k++) {
			//plotPlans->addDataPoint((int)k, currentChainStates[k].constraintCost);
			if (printDebug)
				fprintf(fp, "%lf", currentChainStates[k].constraintCost);
			if (currentChainStates[k].constraintCost < bestState.constraintCost) {
				bestState = AssemblyState(currentChainStates[k]);
				//Logger::consolePrint("chainId for bestState:%d\n", k);
			}
			if (printDebug && k < numberOfChains - 1)
				fprintf(fp, ",");
		}
		if (printDebug)
			fprintf(fp, "\n");
		if (bestState.constraintCost < 10e-3) {
			break;
		}

		/*Adapt the temps/betas*/
		//if (i>0 && i % 50 == 0) {
			for (size_t k = 0; k < chainPairSwapAcceptanceRates.size(); k++)
			{
				double currentAvgSwapRate = (double)std::get<0>(chainPairSwapAcceptanceRates[k]) / (double)std::get<1>(chainPairSwapAcceptanceRates[k]);
				double delTemp = 1.0 / currentChainBetas[k + 1] - 1.0 / currentChainBetas[k];

				if (currentAvgSwapRate > 0.23) {
					for (size_t z = k + 1; z < currentChainBetas.size(); z++)
					{
						double newTemp = (1.0 / currentChainBetas[z]) + (0.1*delTemp);
						currentChainBetas[z] = 1.0 / newTemp;
					}
				}
				else {
					for (size_t z = k + 1; z < currentChainBetas.size(); z++)
					{
						double newTemp = (1.0 / currentChainBetas[z]) - (0.1*delTemp);
						currentChainBetas[z] = 1.0 / newTemp;
					}
				}
			}
			// check if the highest temperature is still in valid range
			if (printDebug && currentChainBetas.back() < betaMin) {
				Logger::consolePrint("Itr:%d, adapted beta beyond threshold:%lf\n", i, currentChainBetas.back());
			}
		//}
	}

	if (printDebug) {
		Logger::consolePrint("Avg swap rate:%lf\n", avgSwapRate / (double)(numSamples));
		Logger::consolePrint("Search with adaptive PTMCMC over! Best assembly cost:%lf\n", bestState.constraintCost);
		Logger::consolePrint("Number of assembly states evaluated:%d \n", statesEvaluated);
	}

	// copy best found state into input assembly and return after crosschecking its cost..
	copyAssemblyStateToInputAssembly(inputAssembly, bestState);
	if (abs(bestState.constraintCost - getAssemblyCost(inputAssembly))>1e-5)
		Logger::consolePrint("Cost mis-match in assembly search");
	Logger::consolePrint("Cost after setting best state:%lf", getAssemblyCost(inputAssembly));

	fclose(fp);
	return bestState.constraintCost;

}

void MCMCPlanners::parallelTemperingStep_adaptive(int chainNumber, int itrNumber, AssemblyState& currentChainState, 
	double beta, std::pair<int, double>& currentAcceptCount, double& bandwidth) {

	double currentAcceptRate = std::get<1>(currentAcceptCount);

	// this flag decided whether auto-tune gaussian perturbation, will update its bandwidth.. we want to do it periodically.. 
	bool updateFlag = false;
	if (itrNumber > 0 && itrNumber % 5 == 0)
		updateFlag = true;

	// a uniform random generator to create acceptance probability
	std::uniform_real_distribution<double>  unifRand(0, 1);
	std::default_random_engine uniRe(chainNumber + itrNumber);
	std::uniform_int_distribution<int> randomMoveDistribution = std::uniform_int_distribution<int>(0, 8);

	//new assembly
	Assembly* searchAssembly = inputAssembly->clone();

	// copy current assembly state into working search assembly
	copyAssemblyStateToInputAssembly(searchAssembly, currentChainState);

	// cost before perturbation
	//double oldCost = currentChainState.constraintCost;
	/*if (beta == 1.00) {
		Logger::consolePrint("gradient for coldest chain\n");
		 oldCost = searchAssembly->constraintsManager->fixConstraints(1);
	}*/
	// BURN - IN
	//if (itrNumber >20) {
	//	oldCost = searchAssembly->constraintsManager->fixConstraints(1);
	//}

	bool converged = false;
	double oldCost = searchAssembly->constraintsManager->fixConstraints(converged, 1);

	//need to check and exit if local minima..
	if (converged) {
		bool oldAssemValid = searchAssembly->checkCollisionFreeValidness(false);
		copyAssemblyStateFromInputAssembly(searchAssembly, currentChainState, oldCost, converged, oldAssemValid);
		plannerState->indicator = true;
		Logger::consolePrint("local minima found after gradient!\n");
		delete searchAssembly;
		return;
	}

	//choose a local/global move
	int moveId = randomMoveDistribution(uniRe);
	perturbWithLocalAndGlobalMoves(chainNumber + itrNumber, moveId, searchAssembly, currentAcceptRate, bandwidth, updateFlag);

	//setAssemblyDirBasedOnConfiguration(searchAssembly);
	//setAssemblyOrderBasedOnConfiguration(searchAssembly);
	setTopDownAssemblyOrder(searchAssembly);

	// cost after perturbation
	double newCost = getAssemblyCost(searchAssembly);
	bool validAssembly = searchAssembly->checkCollisionFreeValidness(false);

	// get acceptance probability 
	double alpha = calculateAcceptanceProbability(oldCost, newCost, beta);
	//decide whether to accept the perturbed assembly state or not
	if (unifRand(uniRe) <= alpha) {
		// accept 
		copyAssemblyStateFromInputAssembly(searchAssembly, currentChainState, newCost,false, validAssembly);
		std::get<0>(currentAcceptCount) = std::get<0>(currentAcceptCount) + 1;
		std::get<1>(currentAcceptCount) = std::get<0>(currentAcceptCount) / (double)(itrNumber + 1);
	}
	else {
		std::get<1>(currentAcceptCount) = std::get<0>(currentAcceptCount) / (double)(itrNumber + 1);
	}

	delete searchAssembly;
}

double MCMCPlanners::gradientTest() {
	DynamicArray<bool> convergedSet;
# pragma omp parallel for
	for (int j = 0; j < plannerState->numberOfChains; j++) {
		bool converged = runGradientOpt(plannerState->currentChainStates[j], 1);
		convergedSet.push_back(converged);
	}

	for (size_t i = 0; i < convergedSet.size(); i++)
	{
		if (convergedSet[i] == true) {
			plannerState->converged = true;
			Logger::logPrint("planner converegd\n");
		}
	}

	/**Check if we found a good enough state**/
	for (int k = 0; k < plannerState->numberOfChains; k++) {
		if (plannerState->currentChainStates[k].constraintCost < plannerState->currentChainBestStates[k].constraintCost) {
			plannerState->currentChainBestStates[k] = plannerState->currentChainStates[k];
			//Logger::consolePrint("chainId for bestState:%d\n", k);
		}
	}
	auto bestState = std::min_element(plannerState->currentChainBestStates.begin(), plannerState->currentChainBestStates.end(), AssemblyStateCompare);
	copyAssemblyStateToInputAssembly(inputAssembly, *bestState);

	return getAssemblyCost(inputAssembly);
}

double MCMCPlanners::interactiveSearchPTMH_adaptive() {

	double currentCost = 0;
	if (!plannerState->converged) {
		// a uniform random generator to create swap acceptance probability
		std::uniform_real_distribution<double>  unifRand(0, 1);
		std::default_random_engine uniRe(plannerState->iterations);
		std::uniform_int_distribution<int> chainIndDistribution(0, plannerState->numberOfChains - 2);

# pragma omp parallel for
		for (int j = 0; j < plannerState->numberOfChains; j++) {
			parallelTemperingStep_adaptive(j, plannerState->iterations, plannerState->currentChainStates[j], plannerState->currentChainBetas[j],
				plannerState->currentChainWiseAcceptanceRates[j], plannerState->currentChainProposalBandwidth[j]);
			//runGradientOpt(plannerState->currentChainStates[j], 3);
		}

		/**swap states of randomly chosen chains**/
		//select chains to swap
		int chainId1 = chainIndDistribution(uniRe);
		int chainId2 = chainId1 + 1;
		// calculate swap probability
		double alphaSwap = calculateSwapProbabilityForAdaptiveParallelTempering(chainId1, chainId2,
			plannerState->currentChainStates, plannerState->currentChainBetas);

		// do the swap
		if (unifRand(uniRe) <= alphaSwap) {
			// accept 
			swap(plannerState->currentChainStates[chainId1], plannerState->currentChainStates[chainId2]);
			plannerState->chainPairSwapAcceptanceRates[chainId1] = std::pair<int, int>(std::get<0>(plannerState->chainPairSwapAcceptanceRates[chainId1]) + 1,
				std::get<1>(plannerState->chainPairSwapAcceptanceRates[chainId1]) + 1);
			plannerState->swapAcceptanceCount = plannerState->swapAcceptanceCount + 1;
		}
		else {
			plannerState->chainPairSwapAcceptanceRates[chainId1] = std::pair<int, int>(std::get<0>(plannerState->chainPairSwapAcceptanceRates[chainId1]),
				std::get<1>(plannerState->chainPairSwapAcceptanceRates[chainId1]) + 1);
		}

		if (plannerState->printDebug) {
			printChainsSwapRates(plannerState->iterations, plannerState->chainPairSwapAcceptanceRates);
			printChainsAcceptRates(plannerState->iterations, plannerState->currentChainWiseAcceptanceRates,
				plannerState->currentChainProposalBandwidth);
		}

		/**Check if we found a good enough state**/
		for (int k = 0; k < plannerState->numberOfChains; k++) {
			if (plannerState->currentChainStates[k].constraintCost < plannerState->currentChainBestStates[k].constraintCost) {
				plannerState->currentChainBestStates[k] = plannerState->currentChainStates[k];
				//Logger::consolePrint("chainId for bestState:%d\n", k);
			}
		}
		auto bestState = std::min_element(plannerState->currentChainBestStates.begin(), plannerState->currentChainBestStates.end(), AssemblyStateCompare);
		if (bestState->constraintCost < 1e-3 || bestState->validAssembly || plannerState->indicator){// || bestState->validAssembly) {
			plannerState->converged = true;
			Logger::consolePrint("Planner converged with assembly cost:%lf or validAssembly:%d!\n", bestState->constraintCost, bestState->validAssembly);
		}
		if (plannerState->indicator || bestState->indicator)
			Logger::consolePrint("local minima found!\n");

		/*Adapt the temps/betas*/
		if (plannerState->iterations > 0 && plannerState->iterations % 10 == 0) {
			for (size_t k = 0; k < plannerState->chainPairSwapAcceptanceRates.size(); k++)
			{
				double currentAvgSwapRate = (double)std::get<0>(plannerState->chainPairSwapAcceptanceRates[k]) /
					(double)std::get<1>(plannerState->chainPairSwapAcceptanceRates[k]);
				double delTemp = 1.0 / plannerState->currentChainBetas[k + 1] - 1.0 / plannerState->currentChainBetas[k];

				if (currentAvgSwapRate > 0.23) {
					for (size_t z = k + 1; z < plannerState->currentChainBetas.size(); z++)
					{
						double newTemp = (1.0 / plannerState->currentChainBetas[z]) + (0.1*delTemp);
						plannerState->currentChainBetas[z] = 1.0 / newTemp;
					}
				}
				else {
					for (size_t z = k + 1; z < plannerState->currentChainBetas.size(); z++)
					{
						double newTemp = (1.0 / plannerState->currentChainBetas[z]) - (0.1*delTemp);
						plannerState->currentChainBetas[z] = 1.0 / newTemp;
					}
				}
			}
			// check if the highest temperature is still in valid range
			if (plannerState->printDebug && plannerState->currentChainBetas.back() < plannerState->betaMin) {
				Logger::consolePrint("Itr:%d, adapted beta beyond threshold:%lf\n", plannerState->iterations, plannerState->currentChainBetas.back());
			}
		}

		if (plannerState->printDebug) {
			Logger::consolePrint("Avg swap rate:%lf\n", (double)plannerState->swapAcceptanceCount / (double)(plannerState->iterations + 1));
			Logger::consolePrint("Planner iterations:%d \n", plannerState->iterations);
		}


		// copy best found state into input assembly and return after crosschecking its cost..
		copyAssemblyStateToInputAssembly(inputAssembly, *bestState);
		//Logger::consolePrint("---- IN INPUT ASSEMBLY! ----------\n");
		//Logger::logPrint("---- IN INPUT ASSEMBLY! ----------\n");
		//if (abs(bestState->constraintCost - getAssemblyCost(inputAssembly)) > 0.0001 ) {
		//	Logger::consolePrint("Cost mis-match in assembly search diff: %10.10lf\n", abs(bestState->constraintCost - getAssemblyCost(inputAssembly)));
		//}
		//else
			//Logger::consolePrint("Assembly cost:%lf, cold chain cost:%lf\n", bestState->constraintCost,
				//plannerState->currentChainStates[0].constraintCost);
		//Logger::consolePrint("Assembly cost:%lf, MCMC iterations:%d \n",getAssemblyCost(inputAssembly), plannerState->iterations);

		//copyAssemblyStateToInputAssembly(inputAssembly, plannerState->currentChainStates[0]);
		//Logger::consolePrint("Assembly current cost:%lf, best cost:%lf\n", plannerState->currentChainStates[0].constraintCost,
		//	bestState->constraintCost);

		plannerState->iterations = plannerState->iterations + 1;

		currentCost = getAssemblyCost(inputAssembly);
	}

	return currentCost;
}


void MCMCPlanners::perturbWithLocalAndGlobalMoves(int seed, int moveId, Assembly* assemblyToPerturb,
	double currentAcceptRate, double& proposalBandwidth, bool updateFlag) {

	std::default_random_engine re(seed);
	switch (moveId) {
		case 0: {
			/***Perturbing positions***/
			perturbAssemblyPlacement(seed, assemblyToPerturb, SAMPLE_AUTOTUNEGAUSSIAN, currentAcceptRate, proposalBandwidth, updateFlag);
			break;
		}

		case 2: {
			/***SWAP POSITIONS OF 2 RANDOMLY SELECTED OBJS***/
			std::uniform_int_distribution<int> randomObjIndDistribution(0, assemblyToPerturb->electroMechanicalComponents.size()-1);
			// randomly selected 2 objs -- their indices and positions
			int indicesToSwap[2] = { randomObjIndDistribution(re), randomObjIndDistribution(re) };
			P3D posToSwap[2] = { assemblyToPerturb->electroMechanicalComponents[indicesToSwap[0]]->getPosition(), 
				assemblyToPerturb->electroMechanicalComponents[indicesToSwap[1]]->getPosition()};
			// see if the swapped position would be valid (since some objects might be frozen)
			assemblyToPerturb->electroMechanicalComponents[indicesToSwap[0]]->getValidRandomPos(posToSwap[1], globalWorldBounds);
			assemblyToPerturb->electroMechanicalComponents[indicesToSwap[1]]->getValidRandomPos(posToSwap[0], globalWorldBounds);
			// set the new swapped positions
			assemblyToPerturb->electroMechanicalComponents[indicesToSwap[0]]->setPosition(posToSwap[1]);
			assemblyToPerturb->electroMechanicalComponents[indicesToSwap[1]]->setPosition(posToSwap[0]);

			//double rotToSwap[2] = { assemblyToPerturb->electroMechanicalComponents[indicesToSwap[0]]->additionalRotAngle, assemblyToPerturb->electroMechanicalComponents[indicesToSwap[1]]->additionalRotAngle };
			//// see if the swapped rot would be valid (since some objects might be frozen)
			//assemblyToPerturb->electroMechanicalComponents[indicesToSwap[0]]->getValidRandomAngle(rotToSwap[1]);
			//assemblyToPerturb->electroMechanicalComponents[indicesToSwap[1]]->getValidRandomAngle(rotToSwap[0]);
			//// set the new swapped rot
			//assemblyToPerturb->electroMechanicalComponents[indicesToSwap[0]]->additionalRotAngle = rotToSwap[1];
			//assemblyToPerturb->electroMechanicalComponents[indicesToSwap[1]]->additionalRotAngle = rotToSwap[0];

			break;
		}

		case 1: {
			/***SWAP ASSEMBLY ORDER OF 2 RANDOMLY SELECTED OBJS***/
			perturbAssemblyOrder(seed, assemblyToPerturb);

			//// change pos of a random obj -- calling this position reset for docs
			//std::uniform_int_distribution<int> randomObjIndDistribution(0, assemblyToPerturb->electroMechanicalComponents.size() - 1);
			//int objId = randomObjIndDistribution(re);
			//bool truncated;
			//P3D randPos;
			//std::uniform_real_distribution<double >  unifx(-globalWorldBounds[0], globalWorldBounds[0]);
			//std::uniform_real_distribution<double >  unify(-globalWorldBounds[1], globalWorldBounds[1]);
			//std::uniform_real_distribution<double >  unifz(-globalWorldBounds[2], globalWorldBounds[2]);
			//do {
			//	randPos = P3D(unifx(re), unify(re), unifz(re));
			//	truncated = assemblyToPerturb->electroMechanicalComponents[objId]->getValidRandomPos(randPos, globalWorldBounds);

			//} while (truncated);
			break;
		}

		case 3: {
			perturbAssemblyOrientation(seed, assemblyToPerturb, SAMPLE_UNIFORM, currentAcceptRate, proposalBandwidth, updateFlag);
			break;
		}
		case 5: {
			int sampleFlag = SAMPLE_UNIFORM;
			//if (plannerState->mode = UPDATED_ASSEMBLY_MODE)
			//	sampleFlag = SAMPLE_AUTOTUNEGAUSSIAN;
			perturbAssemblyDirections(seed, assemblyToPerturb, sampleFlag, currentAcceptRate, proposalBandwidth);
			break;
		}
		case 4: {
			/***SWAP ROTATIONS OF 2 RANDOMLY SELECTED OBJS***/
			std::uniform_int_distribution<int> randomObjIndDistribution(0, assemblyToPerturb->electroMechanicalComponents.size() - 1);
			// randomly selected 2 objs -- their indices and positions
			int indicesToSwap[2] = { randomObjIndDistribution(re), randomObjIndDistribution(re) };

			double rotToSwap[2] = { assemblyToPerturb->electroMechanicalComponents[indicesToSwap[0]]->additionalRotAngle, assemblyToPerturb->electroMechanicalComponents[indicesToSwap[1]]->additionalRotAngle };
			// see if the swapped rot would be valid (since some objects might be frozen)
			assemblyToPerturb->electroMechanicalComponents[indicesToSwap[0]]->getValidRandomAngle(rotToSwap[1]);
			assemblyToPerturb->electroMechanicalComponents[indicesToSwap[1]]->getValidRandomAngle(rotToSwap[0]);
			// set the new swapped rot
			assemblyToPerturb->electroMechanicalComponents[indicesToSwap[0]]->additionalRotAngle = rotToSwap[1];
			assemblyToPerturb->electroMechanicalComponents[indicesToSwap[1]]->additionalRotAngle = rotToSwap[0];

			break;
		}

		case 8: {
			// change angle of a random obj
			std::uniform_int_distribution<int> randomObjIndDistribution(0, assemblyToPerturb->electroMechanicalComponents.size() - 1);

			std::uniform_int_distribution<int> distributionForAngle(-2, 2); //(-2,2); (-1,1);																		
			int objId = randomObjIndDistribution(re);
			//sample a random point from the valid distribution
			double randAngle = distributionForAngle(re)*PI / 2; //*PI/2; *PI/4;
			//see if its valid based on object state (frozen or not) and maximum allowable values. 
			assemblyToPerturb->electroMechanicalComponents[objId]->getValidRandomAngle(randAngle);
			// set the valid random angle
			assemblyToPerturb->electroMechanicalComponents[objId]->additionalRotAngle = randAngle;

			break;
		}
		case 7: {
			// change assembly dir of a random obj
			std::uniform_int_distribution<int> randomObjIndDistribution(0, assemblyToPerturb->electroMechanicalComponents.size() - 1);
			int objId = randomObjIndDistribution(re);

			Plane closestPlane = assemblyToPerturb->findClosestChassisWallToComponent(assemblyToPerturb->electroMechanicalComponents[objId]);
			auto path = dynamic_cast<ArbitraryDirectionDissassemblyPath*>(assemblyToPerturb->electroMechanicalComponents[objId]->disassemblyPath);
			if (path && path->getWorldCoordinatesAssemblyPath() != -closestPlane.n) {
				path->disassemblyDefaultPath = -closestPlane.n;
				if (dynamic_cast<ArbitraryDirectionDissassemblyPath*>(assemblyToPerturb->electroMechanicalComponents[objId]->disassemblyPath)->disassemblyDefaultPath != V3D(0, 1, 0))
					dynamic_cast<ArbitraryDirectionDissassemblyPath*>(assemblyToPerturb->electroMechanicalComponents[objId]->disassemblyPath)->resetInternalAxis();
			}

			/*DynamicArray<V3D> validAxis = assemblyToPerturb->chassis->validAssemblyDirs;
			std::uniform_int_distribution<int> distributionForAxis(0, size(validAxis) - 1);
			int axisInd = distributionForAxis(re);
			if (!dynamic_cast<ServoMotor*>(assemblyToPerturb->electroMechanicalComponents[objId])
				&& !dynamic_cast<MicroServoMotor*>(assemblyToPerturb->electroMechanicalComponents[objId])) {
				if (dynamic_cast<ArbitraryDirectionDissassemblyPath*>(assemblyToPerturb->electroMechanicalComponents[objId]->disassemblyPath)) {
					dynamic_cast<ArbitraryDirectionDissassemblyPath*>(assemblyToPerturb->electroMechanicalComponents[objId]->disassemblyPath)->disassemblyDefaultPath = validAxis[axisInd];
					if (dynamic_cast<ArbitraryDirectionDissassemblyPath*>(assemblyToPerturb->electroMechanicalComponents[objId]->disassemblyPath)->disassemblyDefaultPath != V3D(0, 1, 0))
						dynamic_cast<ArbitraryDirectionDissassemblyPath*>(assemblyToPerturb->electroMechanicalComponents[objId]->disassemblyPath)->resetInternalAxis();
				}
			}*/
			break;
		}

		case 6: {
			/***SWAP ASSEMBLY DIRECTIONS OF 2 RANDOMLY SELECTED OBJS***/
			std::uniform_int_distribution<int> randomObjIndDistribution(0, assemblyToPerturb->electroMechanicalComponents.size() - 1);
			// randomly selected 2 objs -- their indices and positions
			int indicesToSwap[2] = { randomObjIndDistribution(re), randomObjIndDistribution(re) };
			if (dynamic_cast<MotorComponents*>(assemblyToPerturb->electroMechanicalComponents[indicesToSwap[0]])
				|| dynamic_cast<MotorComponents*>(assemblyToPerturb->electroMechanicalComponents[indicesToSwap[1]]))
				break;
			else {
				V3D dirToSwap[2] = { dynamic_cast<ArbitraryDirectionDissassemblyPath*>(assemblyToPerturb->
					electroMechanicalComponents[indicesToSwap[0]]->disassemblyPath)->disassemblyDefaultPath,
					dynamic_cast<ArbitraryDirectionDissassemblyPath*>(assemblyToPerturb->electroMechanicalComponents
						[indicesToSwap[1]]->disassemblyPath)->disassemblyDefaultPath };
				// set the new swapped positions
				dynamic_cast<ArbitraryDirectionDissassemblyPath*>(assemblyToPerturb->
					electroMechanicalComponents[indicesToSwap[0]]->disassemblyPath)->disassemblyDefaultPath
					 = dirToSwap[1];
				dynamic_cast<ArbitraryDirectionDissassemblyPath*>(assemblyToPerturb->
					electroMechanicalComponents[indicesToSwap[1]]->disassemblyPath)->disassemblyDefaultPath
					= dirToSwap[0];
			}

			break;
		}
		case 9: {
			bool converged = false;
			assemblyToPerturb->constraintsManager->fixConstraints(converged, 1);
			break;
		}
	}
}

bool MCMCPlanners::runGradientOpt(AssemblyState& bestStateFromMCMC, int itr) {
	//new assembly
	Assembly* searchAssembly = inputAssembly->clone();

	// copy current assembly state into working search assembly
	copyAssemblyStateToInputAssembly(searchAssembly, bestStateFromMCMC);

	//searchAssembly->constraintsManager->minimizer.bfgsApproximator.reset();
	bool converged = false;
	double gradCost = searchAssembly->constraintsManager->fixConstraints(converged, itr);
	Logger::logPrint("run opt cost:%lf\n", gradCost);

	// update state to values after gradient
	copyAssemblyStateFromInputAssembly(searchAssembly, bestStateFromMCMC, gradCost);

	delete searchAssembly;
	return converged;
}

/************************************ UTILITY FUNCTIONS ****************************************************************/
double MCMCPlanners::calculateSwapProbabilityForAdaptiveParallelTempering(int chain1Id, int chain2Id,
	const DynamicArray<AssemblyState>& currentChainStates, DynamicArray<double> currentChainBetas) {

	double beta1 = currentChainBetas[chain1Id];
	double beta2 = currentChainBetas[chain2Id];

	double alpha = MIN(1, exp(-(beta1 - beta2)*(currentChainStates[chain2Id].constraintCost - currentChainStates[chain1Id].constraintCost)));
	return alpha;
}

double MCMCPlanners::calculateAcceptanceProbability(double oldCost, double newCost, double beta) {
	// smaller beta => more exploration => accepts more bad moves..

	// alpha = min(1,p(current)/p(old)) where p = boltzmann-like density function of cost. p = exp(-beta*cost)/Z where Z = normalization.
	double alpha = MIN(1, exp(beta*(oldCost - newCost)));

	return alpha;
}


void MCMCPlanners::setGeometricBetasAndChainInitialStates(int numChains, int seed, double temp, DynamicArray<AssemblyState>& currentChainStates,
	DynamicArray<double>& currentChainBetas, int mode, DynamicArray<AssemblyState>& currentChainBestStates) {

	seed = seed + plannerState->initializationCounter;

	if (mode & NEW_ASSEMBLY_MODE) {
		//Logger::consolePrint("restarting with new mode on, seed:%d \n", seed);
		plannerState->resetPlannerState();
		//set all chains' initial states
		for (int i = 0; i < numChains; i++) {
			//setInitialRandomAssemblyOrder(inputAssembly, seed + i);
			setInitialRandomAssemblyPlacement(inputAssembly, seed + i, plannerState->incrementalPertubComponentsID);
			setInitialRandomAssemblyDirections(inputAssembly, seed + i, plannerState->incrementalPertubComponentsID);
			setInitialRandomAssemblyOrientations(inputAssembly, seed + i, plannerState->incrementalPertubComponentsID);
			setTopDownAssemblyOrder(inputAssembly);
			//inputAssembly->completeAssembly();
			inputAssembly->snapSupportsToWalls();
			AssemblyState newChainState(inputAssembly);
			newChainState.constraintCost = getAssemblyCost(inputAssembly);
			Logger::consolePrint("id:%d, cost:%lf\n", i, newChainState.constraintCost);
			currentChainStates.push_back(newChainState);
		}

		//Logger::consolePrint("set up with new mode\n");
		plannerState->mode = UPDATED_ASSEMBLY_MODE;
	}
	else if (mode & UPDATE_ASSEMBLY_INCREXPLORE_5050) {
		Logger::consolePrint("INCREXPLORE_5050\n");
		plannerState->resetPlannerState();
		//set all chains' initial states
		for (int i = 0; i < numChains; i++) {
			// exploit around prev best design with 5 cooler chains and explore for the 5 hottest chains..
			bool incrMode = true;
			if (i >= 5)
				incrMode = false;
			setInitialRandomAssemblyOrder(inputAssembly, seed + i);
			setInitialRandomAssemblyPlacement(inputAssembly, seed + i, plannerState->incrementalPertubComponentsID, incrMode);
			setInitialRandomAssemblyDirections(inputAssembly, seed + i, plannerState->incrementalPertubComponentsID, incrMode);
			setInitialRandomAssemblyOrientations(inputAssembly, seed + i, plannerState->incrementalPertubComponentsID, incrMode);
			//inputAssembly->completeAssembly();
			inputAssembly->snapSupportsToWalls();
			AssemblyState newChainState(inputAssembly);
			newChainState.constraintCost = getAssemblyCost(inputAssembly);
			Logger::consolePrint("id:%d, cost:%lf\n", i, newChainState.constraintCost);
			currentChainStates.push_back(newChainState);
		}
		//Logger::consolePrint("set up with update mode\n");
	}

	else if (mode & UPDATE_ASSEMBLY_INCREXPLORE_5050_BEST) {
		Logger::consolePrint("INCREXPLORE_5050 Best\n");
		plannerState->resetPlannerState();
		//set all chains' initial states
		for (int i = 0; i < numChains; i++) {
			// exploit around prev best design with 5 cooler chains and explore for the 5 hottest chains..
			bool incrMode = true;
			if (i >= 5) {
				incrMode = false;
				setInitialRandomAssemblyOrder(inputAssembly, seed + i);
				setInitialRandomAssemblyPlacement(inputAssembly, seed + i, plannerState->incrementalPertubComponentsID, incrMode);
				setInitialRandomAssemblyDirections(inputAssembly, seed + i, plannerState->incrementalPertubComponentsID, incrMode);
				setInitialRandomAssemblyOrientations(inputAssembly, seed + i, plannerState->incrementalPertubComponentsID, incrMode);
				//inputAssembly->completeAssembly();
				inputAssembly->snapSupportsToWalls();
				AssemblyState newChainState(inputAssembly);
				newChainState.constraintCost = getAssemblyCost(inputAssembly);
				//Logger::consolePrint("id:%d, cost:%lf\n", i, newChainState.constraintCost);
				currentChainStates.push_back(newChainState);
			}
			else {
				DynamicArray<AssemblyState> randomStates;
				for (size_t m = 0; m < 50; m++)
				{
					setInitialRandomAssemblyPlacement(inputAssembly, seed + m ^ 2 + i^2 + m*i, plannerState->incrementalPertubComponentsID, incrMode);
					setInitialRandomAssemblyDirections(inputAssembly, seed + m ^ 2 + i^2 + m*i, plannerState->incrementalPertubComponentsID, incrMode);
					setInitialRandomAssemblyOrientations(inputAssembly, seed + m ^ 2 + i^2 +m*i, plannerState->incrementalPertubComponentsID, incrMode);
					setTopDownAssemblyOrder(inputAssembly);
					//inputAssembly->completeAssembly();
					inputAssembly->snapSupportsToWalls();
					AssemblyState newChainState(inputAssembly);
					newChainState.constraintCost = getAssemblyCost(inputAssembly);
					randomStates.push_back(newChainState);
				}
				auto bestState = std::min_element(randomStates.begin(), randomStates.end(), AssemblyStateCompare);
				currentChainStates.push_back(*bestState);
				//Logger::consolePrint("id:%d, cost:%lf\n", i, currentChainStates.back().constraintCost);
			}
		}
		//Logger::consolePrint("set up with update mode\n");
	}
	else if (mode & UPDATED_ASSEMBLY_MODE) {
		plannerState->resetPlannerState();
		//set all chains' initial states
		// first state same as current input assembly state
		AssemblyState newChainState(inputAssembly);
		newChainState.constraintCost = getAssemblyCost(inputAssembly);
		currentChainStates.push_back(newChainState);
		//Logger::consolePrint("id:%d, cost:%lf\n", 0, newChainState.constraintCost);

		for (int i = 1; i < numChains; i++) {
			setInitialRandomAssemblyOrder(inputAssembly, seed + i);
			setInitialRandomAssemblyPlacement(inputAssembly, seed + i, plannerState->incrementalPertubComponentsID);
			setInitialRandomAssemblyDirections(inputAssembly, seed + i, plannerState->incrementalPertubComponentsID);
			setInitialRandomAssemblyOrientations(inputAssembly, seed + i, plannerState->incrementalPertubComponentsID);
			//inputAssembly->completeAssembly();
			inputAssembly->snapSupportsToWalls();
			AssemblyState newChainState(inputAssembly);
			newChainState.constraintCost = getAssemblyCost(inputAssembly);
			//Logger::consolePrint("id:%d, cost:%lf\n", i, newChainState.constraintCost);
			currentChainStates.push_back(newChainState);
		}

		//Logger::consolePrint("set up with update mode\n");
	}
	else if (mode & UPDATE_ASSEMBLY_PREVSTATE) {
		plannerState->resetPlannerState();
		Logger::consolePrint("PREV CHAINS UPDATE\n");
		//set all chains' initial states
		for (int i = 0; i < numChains; i++) {
			if(plannerState->prevChainStates.size()>0)
				copyAssemblyStateToInputAssembly(inputAssembly, plannerState->prevChainStates[i]);
			setInitialRandomAssemblyOrder(inputAssembly, seed + i);
			setInitialRandomAssemblyPlacement(inputAssembly, seed + i, plannerState->incrementalPertubComponentsID);
			setInitialRandomAssemblyDirections(inputAssembly, seed + i, plannerState->incrementalPertubComponentsID);
			setInitialRandomAssemblyOrientations(inputAssembly, seed + i, plannerState->incrementalPertubComponentsID);
			setTopDownAssemblyOrder(inputAssembly);
			inputAssembly->snapSupportsToWalls();
			AssemblyState newChainState(inputAssembly);
			newChainState.constraintCost = getAssemblyCost(inputAssembly);
			Logger::consolePrint("id:%d, cost:%lf\n", i, newChainState.constraintCost);
			currentChainStates.push_back(newChainState);
		}
	}
	else if (mode & UPDATE_ASSEMBLY_PREVSTATE_BEST) {
		plannerState->resetPlannerState();
		Logger::consolePrint("PREV CHAINS UPDATE BEST\n");
		//set all chains' initial states
		for (int i = 0; i < numChains; i++) {
			if (plannerState->prevChainStates.size()>0)
				copyAssemblyStateToInputAssembly(inputAssembly, plannerState->prevChainStates[i]);

			DynamicArray<AssemblyState> randomStates;
			for (size_t m = 0; m < 50; m++)
			{
				setInitialRandomAssemblyPlacement(inputAssembly, seed + m ^ 2 + i ^ 2 + m*i, plannerState->incrementalPertubComponentsID);
				setInitialRandomAssemblyDirections(inputAssembly, seed + m ^ 2 + i ^ 2 + m*i, plannerState->incrementalPertubComponentsID);
				setInitialRandomAssemblyOrientations(inputAssembly, seed + m ^ 2 + i ^ 2 + m*i, plannerState->incrementalPertubComponentsID);
				setTopDownAssemblyOrder(inputAssembly);
				//inputAssembly->completeAssembly();
				inputAssembly->snapSupportsToWalls();
				AssemblyState newChainState(inputAssembly);
				newChainState.constraintCost = getAssemblyCost(inputAssembly);
				randomStates.push_back(newChainState);
			}
			auto bestState = std::min_element(randomStates.begin(), randomStates.end(), AssemblyStateCompare);
			currentChainStates.push_back(*bestState);
			Logger::consolePrint("id:%d, cost:%lf\n", i, currentChainStates.back().constraintCost);
		}
	}
	else if (mode & UPDATE_ASSEMBLY_RANDOM) {
		Logger::consolePrint("updated_random\n");
		plannerState->resetPlannerState();
		//set all chains' initial states
		for (int i = 0; i < numChains; i++) {
			// exploit around prev best design with 1 cool chains with random placement of new component around old design
			// and explore for the 9 hottest chains..
			bool incrMode = true;
			if (i >= 1)
				incrMode = false;
			//setInitialRandomAssemblyOrder(inputAssembly, seed + i);
			setInitialRandomAssemblyPlacement(inputAssembly, seed + i, plannerState->incrementalPertubComponentsID, incrMode);
			setInitialRandomAssemblyDirections(inputAssembly, seed + i, plannerState->incrementalPertubComponentsID, incrMode);
			setInitialRandomAssemblyOrientations(inputAssembly, seed + i, plannerState->incrementalPertubComponentsID, incrMode);
			setTopDownAssemblyOrder(inputAssembly);
			//inputAssembly->completeAssembly();
			inputAssembly->snapSupportsToWalls();
			AssemblyState newChainState(inputAssembly);
			newChainState.constraintCost = getAssemblyCost(inputAssembly);
			Logger::consolePrint("id:%d, cost:%lf\n", i, newChainState.constraintCost);
			currentChainStates.push_back(newChainState);
		}
		//Logger::consolePrint("set up with update mode\n");
	}

	else if (mode & UPDATE_ASSEMBLY_RANDOMBEST) {
		plannerState->resetPlannerState();
		Logger::consolePrint("randombest\n");
		//set all chains' initial states
		for (int i = 0; i < numChains; i++) {
			// exploit around prev best design with 1 cool chains with random placement of new component around old design
			// and explore for the 9 hottest chains..
			bool incrMode = true;
			if (i >= 1)
				incrMode = false;

			if (i == 0) {
				DynamicArray<AssemblyState> randomStates;
				for (size_t m = 0; m < 50; m++)
				{			
					setInitialRandomAssemblyPlacement(inputAssembly, seed + m^2 + m, plannerState->incrementalPertubComponentsID, incrMode);
					setInitialRandomAssemblyDirections(inputAssembly, seed + m^2 + m, plannerState->incrementalPertubComponentsID, incrMode);
					setInitialRandomAssemblyOrientations(inputAssembly, seed + m^2 + m, plannerState->incrementalPertubComponentsID, incrMode);
					setTopDownAssemblyOrder(inputAssembly);
					//inputAssembly->completeAssembly();
					inputAssembly->snapSupportsToWalls();
					AssemblyState newChainState(inputAssembly);
					newChainState.constraintCost = getAssemblyCost(inputAssembly);
					randomStates.push_back(newChainState);
				}
				auto bestState = std::min_element(randomStates.begin(), randomStates.end(), AssemblyStateCompare);
				currentChainStates.push_back(*bestState);
			}
			else {
				//setInitialRandomAssemblyOrder(inputAssembly, seed + i);
				setInitialRandomAssemblyPlacement(inputAssembly, seed + i, plannerState->incrementalPertubComponentsID, incrMode);
				setInitialRandomAssemblyDirections(inputAssembly, seed + i, plannerState->incrementalPertubComponentsID, incrMode);
				setInitialRandomAssemblyOrientations(inputAssembly, seed + i, plannerState->incrementalPertubComponentsID, incrMode);
				setTopDownAssemblyOrder(inputAssembly);
				//inputAssembly->completeAssembly();
				inputAssembly->snapSupportsToWalls();
				AssemblyState newChainState(inputAssembly);
				newChainState.constraintCost = getAssemblyCost(inputAssembly);
				//Logger::consolePrint("id:%d, cost:%lf\n", i, newChainState.constraintCost);
				currentChainStates.push_back(newChainState);
			}
		}
		//Logger::consolePrint("set up with update mode\n");
	}

	//	Logger::consolePrint("update mode on, seed:%d \n", seed);
	//	//auto oldBestStates = currentChainBestStates;
	//	plannerState->resetPlannerState();

	//	//sort(oldBestStates.begin(), oldBestStates.end(), AssemblyStateCompare);
	//	//currentChainStates.insert(currentChainStates.begin(), oldBestStates.begin()+1,
	//	//	oldBestStates.begin() + 6);

	//	//copyAssemblyStateFromInputAssembly(inputAssembly, currentChainStates[0], getAssemblyCost(inputAssembly));

	//	AssemblyState newChainState(inputAssembly);
	//	newChainState.constraintCost = getAssemblyCost(inputAssembly);
	//	currentChainStates.push_back(newChainState);

	//	for (uint i = currentChainStates.size(); i < numChains; i++) {
	//		//setInitialRandomAssemblyPlacement(inputAssembly, seed + i, 0x00);
	//		// might want to use this when we have different assembly directions
	//		//setInitialRandomAssemblyOrder(inputAssembly, seed + i);
	//		//setAssemblyOrderBasedOnConfiguration(inputAssembly);
	//		//inputAssembly->completeAssembly();
	//		//AssemblyState newChainState(inputAssembly);
	//		//newChainState.constraintCost = getAssemblyCost(inputAssembly);
	//		//Logger::consolePrint("id:%d, cost:%lf\n", i, newChainState.constraintCost);
	//		//currentChainStates.push_back(newChainState);
	//		currentChainStates.push_back(currentChainStates[0]);
	//	}

	//}

	for (int i = 0; i < numChains; i++) {
		currentChainBetas.push_back(1 / pow(temp, i));
	}
}

void MCMCPlanners::printChainsStatus(int itr, const DynamicArray<AssemblyState>& chainStates) {
	Logger::logPrint("\nItr:%d ", itr);
	for (size_t i = 0; i < chainStates.size(); i++)
	{
		Logger::logPrint("Chain%d: %lf | ", i, chainStates[i].constraintCost);
	}
}

void MCMCPlanners::printChainsSwapRates(int itr, const DynamicArray<std::pair<int, int>>& chainSwapRates) {
	Logger::logPrint("\nInter chain swap rates:: Itr:%d ", itr);
	for (size_t i = 0; i < chainSwapRates.size(); i++)
	{
		Logger::logPrint("Chain%d: %lf | ", i, (double)std::get<0>(chainSwapRates[i]) / (double)std::get<1>(chainSwapRates[i]));
	}
}

void MCMCPlanners::printChainsAcceptRates(int itr, const DynamicArray<std::pair<int, double>>& chainAcceptRates,
	const DynamicArray<double>& bandwidth) {
	Logger::logPrint("\nIndividual accept rates:: Itr:%d ", itr);
	for (size_t i = 0; i < chainAcceptRates.size(); i++)
	{
		Logger::logPrint("Chain%d: %lf | ", i, std::get<1>(chainAcceptRates[i]));
	}
	Logger::logPrint("\nIndividual bandwidths:: Itr:%d ", itr);
	for (size_t i = 0; i < bandwidth.size(); i++)
	{
		Logger::logPrint("Chain%d: %lf | ", i, bandwidth[i]);
	}
}
