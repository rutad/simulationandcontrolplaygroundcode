#pragma once

#include <GUILib/GLMesh.h>
#include <MathLib/mathLib.h>
#include <MathLib/P3D.h>
#include <MathLib/V3D.h>
#include <MathLib/Quaternion.h>
#include <MathLib/Ray.h>
#include <GUILib/TranslateWidget.h>
#include <GUILib/RotateWidget.h>
#include <OptimizationLib\ObjectiveFunction.h>
#include <BulletCollision/btBulletCollisionCommon.h>
#include "AssemblyUtils.h"

class BaseObject;

/*
	Used to ensure the state of an object can be explicitly controlled, for example when it is dragged with the mouse by a user
*/
class TargetStateObjective : public ObjectiveFunction {
private:
	//target position
	P3D targetPosition;
	//target orientation - but store the inverse
	Quaternion invTargetOrientation;
public:
	//the object it applies to
	BaseObject* object;

	double weightPos = 1, weightOrientation = 1;

	void setTargets(const P3D& targetPos, const Quaternion& targetRot);

	TargetStateObjective(BaseObject* object) { this->object = object; 	description = "TargetStateObjective ";}
	~TargetStateObjective() {}

	virtual double computeValue(const dVector& p);
};

