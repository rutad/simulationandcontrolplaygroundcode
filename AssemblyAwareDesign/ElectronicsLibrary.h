#pragma once
#include "MakeBlockComponents.h"

/************************* PCB **************************************/
class PCB : public MakeBlockComponents {
public:
	// constructor
	PCB(bool textureFlag = true) {
		loadMeshAndTexture = textureFlag;
		//optimizeAlpha = true; optimizeBeta = true; optimizeGamma = true;
		initGeometry();
	}
	virtual ~PCB() {};

	virtual void initGeometry() {
		name = "PCB";

		if (loadMeshAndTexture) {
			GLShaderMaterial tmpMat;
			componentMeshes.push_back(GLContentManager::getGLMesh("../data/3dModels/electronicsLib/PCB2.obj"));
			tmpMat.r = boardColor[0]; tmpMat.g = boardColor[1]; tmpMat.b = boardColor[2]; tmpMat.a = boardColor[3]; componentMeshes.back()->setMaterial(tmpMat);
			tmpMat.setShaderProgram(GLContentManager::getShaderProgram("matcap"));
			tmpMat.setTextureParam("../data/textures/matcap/5.bmp", GLContentManager::getTexture("../data/textures/matcap/5.bmp"));
			componentMeshes.back()->setMaterial(tmpMat);

			componentMeshes.push_back(GLContentManager::getGLMesh("../data/3dModels//electronicsLib/PCB1.obj"));
			tmpMat.setTextureParam("../data/textures/matcap/JG_Gold.bmp", GLContentManager::getTexture("../data/textures/matcap/JG_Gold.bmp"));
			componentMeshes.back()->setMaterial(tmpMat);
			tmpMat.r = connectorColor[0]; tmpMat.g = connectorColor[1]; tmpMat.b = connectorColor[2]; tmpMat.a = connectorColor[3]; componentMeshes.back()->setMaterial(tmpMat);
		}

		//bounding box
		xMax = 0.0070 * 2*2;
		yMax = 0.00345 * 2*2;
		zMax = 0.0095 * 2*2;

		colShapes.push_back(btBoxShape(btVector3((btScalar)(xMax / 2.0), (btScalar)(yMax / 2.0), (btScalar)(zMax / 2.0))));
		colShapeGeometryCenterLocalOffset.push_back(V3D(0, 0.0030, 0));


		// add assembly path and convex hull points
		MakeBlockComponents::initGeometry();

		// support, fasteners and wires
		supports.push_back(new RigidlyAttachedSupport(this, P3D(0, -yMax / 2.0, 0), AxisAlignedBoundingBox(P3D(-xMax / 2, -0.0025, -zMax/2), P3D(xMax / 2, 0.0025, zMax/2))));

	}

	virtual EMComponent* clone(bool cloneTexture = true) {
		PCB* newObject = new PCB(cloneTexture);
		// copy other properties eg: support sizes..
		return newObject;
	}

	virtual GLMesh* getWireGeometryMesh() {
		return NULL;
	}
};

/************************* Board **************************************/
class Board : public MakeBlockComponents {
public:
	// constructor
	Board(bool textureFlag = true) {
		loadMeshAndTexture = textureFlag;
		//optimizeAlpha = true; optimizeBeta = true; optimizeGamma = true;
		initGeometry();
	}
	virtual ~Board() {};

	virtual void initGeometry() {
		name = "Board";

		if (loadMeshAndTexture) {
			GLShaderMaterial tmpMat;
			componentMeshes.push_back(GLContentManager::getGLMesh("../data/3dModels/electronicsLib/board2.obj"));
			tmpMat.r = boardColor[0]; tmpMat.g = boardColor[1]; tmpMat.b = boardColor[2]; tmpMat.a = boardColor[3]; componentMeshes.back()->setMaterial(tmpMat);
			tmpMat.setShaderProgram(GLContentManager::getShaderProgram("matcap"));
			tmpMat.setTextureParam("../data/textures/matcap/5.bmp", GLContentManager::getTexture("../data/textures/matcap/5.bmp"));
			componentMeshes.back()->setMaterial(tmpMat);

			componentMeshes.push_back(GLContentManager::getGLMesh("../data/3dModels//electronicsLib/board1.obj"));
			tmpMat.setTextureParam("../data/textures/matcap/glossBlack.bmp", GLContentManager::getTexture("../data/textures/matcap/glossBlack.bmp"));
			componentMeshes.back()->setMaterial(tmpMat);
			tmpMat.r = connectorColor[0]; tmpMat.g = connectorColor[1]; tmpMat.b = connectorColor[2]; tmpMat.a = connectorColor[3]; componentMeshes.back()->setMaterial(tmpMat);
		}

		//bounding box
		xMax = 0.0168 * 2 ;
		yMax = 0.0025 * 2 ;
		zMax = 0.0130 * 2 ;

		colShapes.push_back(btBoxShape(btVector3((btScalar)(xMax / 2.0), (btScalar)(yMax / 2.0), (btScalar)(zMax / 2.0))));
		colShapeGeometryCenterLocalOffset.push_back(V3D(0, 0.0017, 0));


		// add assembly path and convex hull points
		MakeBlockComponents::initGeometry();

		// support, fasteners and wires
		supports.push_back(new RigidlyAttachedSupport(this, P3D(0, -yMax / 2.0, 0), AxisAlignedBoundingBox(P3D(-xMax / 2, -0.0015, -zMax / 2), P3D(xMax / 2, 0.0015, zMax / 2))));

	}

	virtual EMComponent* clone(bool cloneTexture = true) {
		Board* newObject = new Board(cloneTexture);
		// copy other properties eg: support sizes..
		return newObject;
	}

	virtual GLMesh* getWireGeometryMesh() {
		return NULL;
	}
};

/************************* Sensor **************************************/
class Sensor : public MakeBlockComponents {
public:
	// constructor
	Sensor(bool textureFlag = true) {
		//optimizeAlpha = true; optimizeBeta = true; optimizeGamma = true;
		loadMeshAndTexture = textureFlag;
		initGeometry();
	}
	virtual ~Sensor() {};

	virtual void initGeometry() {
		name = "Sensor";

		if (loadMeshAndTexture) {
			GLShaderMaterial tmpMat;
			componentMeshes.push_back(GLContentManager::getGLMesh("../data/3dModels/electronicsLib/sensor1.obj"));
			tmpMat.r = boardColor[0]; tmpMat.g = boardColor[1]; tmpMat.b = boardColor[2]; tmpMat.a = boardColor[3]; componentMeshes.back()->setMaterial(tmpMat);
			tmpMat.setShaderProgram(GLContentManager::getShaderProgram("matcap"));
			tmpMat.setTextureParam("../data/textures/matcap/5.bmp", GLContentManager::getTexture("../data/textures/matcap/5.bmp"));
			componentMeshes.back()->setMaterial(tmpMat);

			componentMeshes.push_back(GLContentManager::getGLMesh("../data/3dModels//electronicsLib/sensor2.obj"));
			tmpMat.setTextureParam("../data/textures/matcap/glossBlack.bmp", GLContentManager::getTexture("../data/textures/matcap/glossBlack.bmp"));
			componentMeshes.back()->setMaterial(tmpMat);
			tmpMat.r = connectorColor[0]; tmpMat.g = connectorColor[1]; tmpMat.b = connectorColor[2]; tmpMat.a = connectorColor[3]; componentMeshes.back()->setMaterial(tmpMat);
		}

		//bounding box
		xMax = 0.0045 * 2;
		yMax = 0.0054 * 2;
		zMax = 0.0105 * 2;

		colShapes.push_back(btBoxShape(btVector3((btScalar)(xMax / 2.0), (btScalar)(yMax / 2.0), (btScalar)(zMax / 2.0))));
		colShapeGeometryCenterLocalOffset.push_back(V3D(0, 0.0045, 0));


		// add assembly path and convex hull points
		MakeBlockComponents::initGeometry();

		// support, fasteners and wires
		supports.push_back(new RigidlyAttachedSupport(this, P3D(0, -yMax / 4.0, 0), AxisAlignedBoundingBox(P3D(-xMax / 2, -0.0015, -zMax / 2), P3D(xMax / 2, 0.0015, zMax / 2))));

	}

	virtual EMComponent* clone(bool cloneTexture = true) {
		Sensor* newObject = new Sensor(cloneTexture);
		// copy other properties eg: support sizes..
		return newObject;
	}

	virtual GLMesh* getWireGeometryMesh() {
		return NULL;
	}
};


/************************* Adafruit **************************************/
class Adafruit : public MakeBlockComponents {
public:
	// constructor
	Adafruit(bool textureFlag = true) {
		//optimizeAlpha = true; optimizeBeta = true; optimizeGamma = true;
		loadMeshAndTexture = textureFlag;
		initGeometry();
	}
	virtual ~Adafruit() {};

	virtual void initGeometry() {
		name = "Adafruit";

		if (loadMeshAndTexture) {
			GLShaderMaterial tmpMat;
			componentMeshes.push_back(GLContentManager::getGLMesh("../data/3dModels/electronicsLib/adafruit0.obj"));
			tmpMat.r = boardColor[0]; tmpMat.g = boardColor[1]; tmpMat.b = boardColor[2]; tmpMat.a = boardColor[3]; componentMeshes.back()->setMaterial(tmpMat);
			tmpMat.setShaderProgram(GLContentManager::getShaderProgram("matcap"));
			tmpMat.setTextureParam("../data/textures/matcap/5.bmp", GLContentManager::getTexture("../data/textures/matcap/5.bmp"));
			componentMeshes.back()->setMaterial(tmpMat);

			componentMeshes.push_back(GLContentManager::getGLMesh("../data/3dModels//electronicsLib/adafruit1.obj"));
			tmpMat.setTextureParam("../data/textures/matcap/JG_Gold.bmp", GLContentManager::getTexture("../data/textures/matcap/JG_Gold.bmp"));
			componentMeshes.back()->setMaterial(tmpMat);
			tmpMat.r = connectorColor[0]; tmpMat.g = connectorColor[1]; tmpMat.b = connectorColor[2]; tmpMat.a = connectorColor[3]; componentMeshes.back()->setMaterial(tmpMat);
		}

		//bounding box
		xMax = 0.0063 * 2*2;
		yMax = 0.0014 * 2*2;
		zMax = 0.0095 * 2*2;

		colShapes.push_back(btBoxShape(btVector3((btScalar)(xMax / 2.0), (btScalar)(yMax / 2.0), (btScalar)(zMax / 2.0))));
		colShapeGeometryCenterLocalOffset.push_back(V3D(0,0, 0));


		// add assembly path and convex hull points
		MakeBlockComponents::initGeometry();

		// support, fasteners and wires
		supports.push_back(new RigidlyAttachedSupport(this, P3D(0, -yMax / 2.0 - 0.0015, 0), AxisAlignedBoundingBox(P3D(-xMax / 2, -0.0015, -zMax / 2), P3D(xMax / 2, 0.0015, zMax / 2))));

	}

	virtual EMComponent* clone(bool cloneTexture = true) {
		Adafruit* newObject = new Adafruit(cloneTexture);
		// copy other properties eg: support sizes..
		return newObject;
	}

	virtual GLMesh* getWireGeometryMesh() {
		return NULL;
	}
};

/************************* Breakout **************************************/
class Breakout : public MakeBlockComponents {
public:
	// constructor
	Breakout(bool textureFlag = true) {
		//optimizeAlpha = true; optimizeBeta = true; optimizeGamma = true;
		loadMeshAndTexture = textureFlag;
		initGeometry();
	}
	virtual ~Breakout() {};

	virtual void initGeometry() {
		name = "Breakout";

		if (loadMeshAndTexture) {
			GLShaderMaterial tmpMat;
			componentMeshes.push_back(GLContentManager::getGLMesh("../data/3dModels/electronicsLib/breakout0.obj"));
			tmpMat.r = boardColor[0]; tmpMat.g = boardColor[1]; tmpMat.b = boardColor[2]; tmpMat.a = boardColor[3]; componentMeshes.back()->setMaterial(tmpMat);
			tmpMat.setShaderProgram(GLContentManager::getShaderProgram("matcap"));
			tmpMat.setTextureParam("../data/textures/matcap/5.bmp", GLContentManager::getTexture("../data/textures/matcap/5.bmp"));
			componentMeshes.back()->setMaterial(tmpMat);

			componentMeshes.push_back(GLContentManager::getGLMesh("../data/3dModels//electronicsLib/breakout1.obj"));
			tmpMat.setTextureParam("../data/textures/matcap/JG_Gold.bmp", GLContentManager::getTexture("../data/textures/matcap/JG_Gold.bmp"));
			componentMeshes.back()->setMaterial(tmpMat);
			tmpMat.r = connectorColor[0]; tmpMat.g = connectorColor[1]; tmpMat.b = connectorColor[2]; tmpMat.a = connectorColor[3]; componentMeshes.back()->setMaterial(tmpMat);
		}

		//bounding box
		xMax = 0.063/2 ;
		yMax = 0.0182/2;
		zMax = 0.0550/2 ;

		colShapes.push_back(btBoxShape(btVector3((btScalar)(xMax / 2.0), (btScalar)(yMax / 2.0), (btScalar)(zMax / 2.0))));
		colShapeGeometryCenterLocalOffset.push_back(V3D(0, 0, 0));


		// add assembly path and convex hull points
		MakeBlockComponents::initGeometry();

		// support, fasteners and wires
		supports.push_back(new RigidlyAttachedSupport(this, P3D(0, -yMax / 2.0 - 0.0015, 0), AxisAlignedBoundingBox(P3D(-xMax / 2, -0.0015, -zMax / 2), P3D(xMax / 2, 0.0015, zMax / 2))));

	}

	virtual EMComponent* clone(bool cloneTexture = true) {
		Breakout* newObject = new Breakout(cloneTexture);
		// copy other properties eg: support sizes..
		return newObject;
	}

	virtual GLMesh* getWireGeometryMesh() {
		return NULL;
	}
};

/************************* MakeBlockGeneric **************************************/
class MakeBlockGeneric : public MakeBlockComponents {
public:
	// constructor
	MakeBlockGeneric(bool textureFlag = true) {
		//optimizeAlpha = true; optimizeBeta = true; optimizeGamma = true;
		loadMeshAndTexture = textureFlag;
		initGeometry();
	}
	virtual ~MakeBlockGeneric() {};

	virtual void initGeometry() {
		name = "MakeBlockGeneric";

		if (loadMeshAndTexture) {
			componentMeshes.push_back(GLContentManager::getGLMesh("../data/3dModels/makeblock/makeBlock_bluetooth_1.obj"));
			//tmpMat.r = boardColor[0]; tmpMat.g = boardColor[1]; tmpMat.b = boardColor[2]; tmpMat.a = boardColor[3]; componentMeshes.back()->setMaterial(tmpMat);
			GLShaderMaterial tmpMat;
			tmpMat.setShaderProgram(GLContentManager::getShaderProgram("matcap"));
			tmpMat.setTextureParam("../data/textures/matcap/5.bmp", GLContentManager::getTexture("../data/textures/matcap/5.bmp"));
			componentMeshes.back()->setMaterial(tmpMat);

			componentMeshes.push_back(GLContentManager::getGLMesh("../data/3dModels/makeblock/makeBlock_bluetooth_2.obj"));
			tmpMat.setTextureParam("../data/textures/matcap/JG_Gold.bmp", GLContentManager::getTexture("../data/textures/matcap/JG_Gold.bmp"));
			componentMeshes.back()->setMaterial(tmpMat);

			componentMeshes.push_back(GLContentManager::getGLMesh("../data/3dModels/makeblock/makeBlock_bluetooth_3.obj"));
			//tmpMat.r = connectorColor[0]; tmpMat.g = connectorColor[1]; tmpMat.b = connectorColor[2]; tmpMat.a = connectorColor[3]; componentMeshes.back()->setMaterial(tmpMat);
			tmpMat.setTextureParam("../data/textures/matcap/glossBlack.bmp", GLContentManager::getTexture("../data/textures/matcap/glossBlack.bmp"));
			componentMeshes.back()->setMaterial(tmpMat);
		}

		//bounding box
		xMax = 0.0120 * 2;
		yMax = 0.0090 * 2;
		zMax = 0.0256 * 2;

		colShapes.push_back(btBoxShape(btVector3((btScalar)(xMax / 2.0), (btScalar)(yMax / 8.0), (btScalar)(zMax / 2.0))));
		colShapeGeometryCenterLocalOffset.push_back(V3D(0, -((yMax / 2.0 - yMax / 4.0) + (yMax / 2.0 - yMax / 4.0) / 2.0), 0));
		colShapes.push_back(btBoxShape(btVector3((btScalar)(xMax / 3.5), (btScalar)(yMax / 2.0), (btScalar)(zMax / 8.0))));
		colShapeGeometryCenterLocalOffset.push_back(V3D(0.001, 0, ((zMax / 2.0 - zMax / 4.0) + (zMax / 2.0 - zMax / 4.0) / 2)));

		// add assembly path and convex hull points
		MakeBlockComponents::initGeometry();

		// support, fasteners and wires
		supports.push_back(new RigidlyAttachedSupport(this, P3D(0, -yMax / 2.0, 0.0065), AxisAlignedBoundingBox(P3D(-xMax / 2, -0.0025, -0.005), P3D(xMax / 2, 0.0025, 0.005))));

	}

	virtual EMComponent* clone(bool cloneTexture = true) {
		MakeBlockGeneric* newObject = new MakeBlockGeneric(cloneTexture);
		// copy other properties eg: support sizes..
		return newObject;
	}

	virtual GLMesh* getWireGeometryMesh() {
		return NULL;
	}
};