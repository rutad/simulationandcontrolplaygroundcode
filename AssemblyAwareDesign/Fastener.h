#pragma once
#include "BaseObject.h"
#include "FastenerCollisionObject.h"

/*For visual fastener */

class Fastener : public AssemblableComponent {

public:
	// support's parent
	EMComponent* parent = NULL;
	//these are the local coordinates of the attachment point relative to the parent
	P3D parentLocalSupportPos;
	//this is the direction along which the attachment will be inserted into the support structure, also in the coordinate frame of the parent
	V3D pathAxis;
	//and this is the radius
	double r = 0.005;
	// default fastenerLength
	double fastenerLength = 0.01;

	FastenerCollisionObject* fastenerCollisionObj = NULL;

public:
	Fastener(EMComponent* parent, const P3D& parentLocalSupportPos, const V3D& axis, double r) {
		this->parent = parent;
		this->parentLocalSupportPos = parentLocalSupportPos;
		this->pathAxis = axis;
		this->r = r;
		name = "FastenerVisualObject";
		disassemblyPath = new FixedDirectionDissassemblyPath(this, pathAxis);
		fastenerCollisionObj = new FastenerCollisionObject(this, parentLocalSupportPos);
	}
	// destructor
	virtual ~Fastener() {
		if (fastenerCollisionObj)
			delete fastenerCollisionObj;
	};

	virtual P3D getStartPositionInParentLocalCoordinates() {
		return parentLocalSupportPos;
	}

	virtual P3D getPosition() {
		BaseObject::setPosition(parent->getWorldCoordinates(getStartPositionInParentLocalCoordinates()));
		return BaseObject::getPosition();
	}

	virtual void setPosition(const P3D& p) {
		parentLocalSupportPos = parent->getLocalCoordinates(p);
	}

	virtual Quaternion getOrientation() {
		V3D aligningAxis = V3D(0, 1, 0)*SGN(pathAxis.dot(V3D(0, 1, 0)));

		if (abs(pathAxis.dot(V3D(0, 0, 1))) > 0) {
			aligningAxis = V3D(0, 0, 1)*SGN(pathAxis.dot(V3D(0, 0, 1)));
		}
		else if (abs(pathAxis.dot(V3D(1, 0, 0))) > 0) {
			aligningAxis = V3D(1, 0, 0)*SGN(pathAxis.dot(V3D(1, 0, 0)));
		}

		setOrientation(getRotationAxisThatAlignsVectors(aligningAxis, parent->getWorldCoordinates(pathAxis.unit())));
		return BaseObject::getOrientation();
	}

	void getSilhouetteScalingDimensions(double &dimX, double &dimY, double &dimZ) {
		dimY = fastenerLength / 2;
		dimX = dimZ = r;
	}

	virtual void drawObjectGeometry(GLShaderMaterial* material = NULL) {
		GLShaderMaterial tmpMat;
		GLShaderMaterial *matToUse = (material) ? material : &tmpMat;

		if (material == NULL) {
			tmpMat.r = 0.8; tmpMat.g = 0.8; tmpMat.b = 1.0; tmpMat.a = 1.0;
		}
		matToUse->apply();
		drawCapsule(P3D() + pathAxis.unit() * -fastenerLength / 2,
			P3D() + pathAxis.unit() * fastenerLength / 2, r * 0.9);
		matToUse->end();
	}

	virtual void addBulletCollisionObjectsToList(DynamicArray<btCollisionObject*>& bulletCollisionObjs) {
		fastenerCollisionObj->addBulletCollisionObjectsToList(bulletCollisionObjs);
	}

	virtual bool shouldCollideWith(BaseObject* obj) {
		return false;
	}

	virtual void drawSweptConvexHull() {
		// this is in world co-ordinates..
		GLShaderMaterial tmpMat;
		tmpMat.r = 0.25; tmpMat.g = 0.28; tmpMat.b = 0.67; tmpMat.a = 0.1;
		tmpMat.apply();
		drawCapsule(fastenerCollisionObj->getPosition(), fastenerCollisionObj->getPosition() + fastenerCollisionObj->getOrientation()*pathAxis.unit()* disassemblyPath->pathLength, r);
		tmpMat.end();
	}

	GLMesh* getFastenerMesh() {
		GLMesh* fastenerMesh = new GLMesh();
		fastenerMesh->addCylinder(fastenerCollisionObj->getPosition() + fastenerCollisionObj->getOrientation()*pathAxis.unit()* -0.02,
			fastenerCollisionObj->getPosition() + fastenerCollisionObj->getOrientation()*pathAxis.unit()* 0.01, r);

		return fastenerMesh;
	}
};