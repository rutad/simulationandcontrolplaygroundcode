#pragma once
# include "AssemblyPlanner.h"

/* A class created to test various versions of MCMC algorithms for assembly planning */

// this mode is used to figure out how to re-initialize MCMC planner
#define NEW_ASSEMBLY_MODE 0x01 // mode at start, any change in components of current assembly (addition/deletion) re-activates this mode
#define UPDATED_ASSEMBLY_MODE 0x02 // chnages in assembly due to gradient optimization etc is marked with this mode
#define UPDATE_ASSEMBLY_INCREXPLORE_5050 0x04
#define UPDATE_ASSEMBLY_RANDOM 8
#define UPDATE_ASSEMBLY_RANDOMBEST 16
#define UPDATE_ASSEMBLY_PREVSTATE 32
#define UPDATE_ASSEMBLY_INCREXPLORE_5050_BEST 64
#define UPDATE_ASSEMBLY_PREVSTATE_BEST 128


class MCMCPlanners;

class MCMCPlannerState {
public:
	bool converged = false;
	bool printDebug = false;
	bool initialized = false;
	int numberOfChains = 10;
	double betaMin = 0.001;
	int iterations = 0;
	int swapAcceptanceCount = 0;
	int mode = NEW_ASSEMBLY_MODE;
	int initializationCounter = 0;
	MCMCPlanners* planner = NULL;

	DynamicArray<AssemblyState> currentChainStates;
	DynamicArray<double> currentChainBetas;
	DynamicArray<std::pair<int, double>>currentChainWiseAcceptanceRates;
	DynamicArray<std::pair<int, int>>chainPairSwapAcceptanceRates;
	DynamicArray<double> currentChainProposalBandwidth;
	DynamicArray<AssemblyState> currentChainBestStates;

	MCMCPlannerState(MCMCPlanners* inputPlanner);
	~MCMCPlannerState() {};

	void initializePlannerState(int seed);
	void resetPlannerState();
	bool indicator = false; // to find out if gradient converged within planner..

	// for incremental design mode -- to detect which components in assembly to perturb during initialization..
	DynamicArray<int> incrementalPertubComponentsID;

	// for incremental design mode -- trying to using alternate designs found in previous step via initialization..
	DynamicArray<AssemblyState> prevChainStates;
};

class MCMCPlanners : public AssemblyPlanner {

public:

	MCMCPlannerState* plannerState = NULL;

	//constructor-destructor
	MCMCPlanners(Assembly* assembly);
	MCMCPlanners(Assembly* assembly, DynamicArray<AssemblyState> initChainStates);
	//MCMCPlanners(Assembly* assembly, PlotWindow* plot);
	virtual ~MCMCPlanners() {
		delete plannerState;
	};

	double interactiveSearchPTMH_adaptive();
	double gradientTest();
	double searchOrderAndPlacementWithParallelTemperedMH_adaptive(int seed, int& statesEvaluated,
		int numSamples = 0, int numberOfChains = 10, double betaMin = -1, bool printDebug = true);
	void parallelTemperingStep_adaptive(int chainNumber, int itrNumber, AssemblyState& currentChainState, double beta,
		std::pair<int, double>& currentAcceptCount, double& bandwidth);


	// proposal functions
	void perturbWithLocalAndGlobalMoves(int seed, int moveId, Assembly* assemblyToPerturb,
		double currentAcceptRate, double& proposalBandwidth, bool updateFlag = false);

	// utility functions
	double calculateSwapProbabilityForAdaptiveParallelTempering(int chain1Id, int chain2Id,
		const DynamicArray<AssemblyState>& currentChainStates, DynamicArray<double> currentChainBetas);
	double calculateAcceptanceProbability(double oldCost, double newCost, double beta);
	void setGeometricBetasAndChainInitialStates(int numChains, int seed, double temp, DynamicArray<AssemblyState>& currentChainStates,
		DynamicArray<double>& currentChainBetas, int mode = NEW_ASSEMBLY_MODE, DynamicArray<AssemblyState>& currentChainBestStates = DynamicArray<AssemblyState>());
	bool runGradientOpt(AssemblyState& bestStateFromMCMC, int itr);

	virtual void setUpPlanner(int seed = 38);

	// debugging functions
	void printChainsAcceptRates(int itr, const DynamicArray<std::pair<int, double>>& chainAcceptRates, 
		const DynamicArray<double>& bandwidth);
	void printChainsSwapRates(int itr, const DynamicArray<std::pair<int, int>>& chainSwapRates);
	void printChainsStatus(int itr, const DynamicArray<AssemblyState>& chainStates);
};