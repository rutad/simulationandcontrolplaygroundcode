#include "UIUserStudy.h"
#include <GUILib/GLContentManager.h>
#include <GUILib/GLMesh.h>
#include <GUILib/GLMeshDisplayWindow.h>
#include <GUILib/GLWindowContainer.h>
#include <iostream>
#include <queue>
#include "ObjectDisplayWindow.h"
#include <OptimizationLib/NewtonFunctionMinimizer.h>
#include <GUILib\GLTrackingCamera.h>
#include <GUILib/RotateWidgetV1.h>
#include <GUILib/RotateWidgetV2.h>
#include <GUILib/GLUtils.h>
#include <GUILib/PlotWindow.h>
#include <MathLib/Segment.h>
#include "BoxComponent.h"
#include "SphereComponent.h"
#include "MakeBlockComponents.h"
#include "MotorComponents.h"
#include "BatteryComponents.h"
#include <GUILib/GLUtils.h>
#include "Support.h"
#include "ElectronicsLibrary.h"

UIUserStudy::UIUserStudy() {
	camera = new GLTrackingCamera();

	((GLTrackingCamera*)camera)->camDistance = -1;

	setWindowTitle("Training: Assembly Aware Design");
	bgColor[0] = bgColor[1] = bgColor[2] = 1;

	showGroundPlane = false;
	showDesignEnvironmentBox = true;
	designEnvironmentScale = 1.0;

	windowArray = new GLWindowContainer(4, 4, (int)(getGLAppInstance()->getMainWindowWidth() * 0.75), (getGLAppInstance()->getMainWindowHeight() - 20 - (getGLAppInstance()->getMainWindowHeight() / 2)),
		(int)(getGLAppInstance()->getMainWindowWidth() * 0.25), getGLAppInstance()->getMainWindowHeight() / 2);

	TwDefine(" MainMenuBar color='80 200 200' text=dark ");
	//TwAddSeparator(mainMenuBar, "sep2", "");
	TwAddSeparator(mainMenuBar, "sep3", "");
	TwAddButton(mainMenuBar, "Save and load assembly", NULL, NULL, " ");
	TwAddButton(mainMenuBar, "Save to file", UIButtonEventSave2, "save", "");
	TwAddButton(mainMenuBar, "Load from file", UIButtonEventLoad2, "load", "");
	//TwAddButton(mainMenuBar, "Export .obj", UIButtonEventExport, "export", "");
	//TwAddSeparator(mainMenuBar, "sep4", "");
	TwAddSeparator(mainMenuBar, "sep5", "");
	TwAddButton(mainMenuBar, "Assembly functions", NULL, NULL, " ");
	TwAddButton(mainMenuBar, "Delete selected", UIButtonEventDelete2, "delete", "");
	//TwAddButton(mainMenuBar, "Get assembly fill ratio", UIButtonEventAssemblyFillRatio2, "fillRatio", "");
	TwAddButton(mainMenuBar, "Swap assembly order", UIButtonEventSwapOrder2, "swaporder", "");
	TwAddButton(mainMenuBar, "Set assembly order", UIButtonEventSetAssemblyOrder2, "order", "");
	//TwAddButton(mainMenuBar, "Create supports on chassis", UIButtonEventCreateSupport2, "support", "");
	TwAddSeparator(mainMenuBar, "sep6", "");

	TwAddVarRW(mainMenuBar, "RunOptions", TwDefineEnumFromString("RunOptions", "..."), &runOption, "group='Sim'");
	DynamicArray<std::string> runOptionList;
	runOptionList.push_back("\\Design");
	runOptionList.push_back("\\Assembly Animation");
	runOptionList.push_back("\\Assembly Animation With Swept Paths");
	generateMenuEnumFromFileList("MainMenuBar/RunOptions", runOptionList);

	TwAddVarRW(mainMenuBar, "Animation Speed", TW_TYPE_DOUBLE, &animStep, " min=0 max=0.1 step=0.005 group = 'Animation' ");
	TwAddVarRW(mainMenuBar, "Animation Phase", TW_TYPE_DOUBLE, &animPhase, "min=0 max=1 step=0.005 group = 'Animation' ");
	TwAddButton(mainMenuBar, "Reset Animation", UIButtonEventResetAnim2, "reset", "group = 'Animation'");
	TwAddSeparator(mainMenuBar, "sep7", "");


	TwAddVarRW(mainMenuBar, "Bullet CPs", TW_TYPE_BOOLCPP, &drawBulletCPs, " label='Draw Bullet Debug Info' group='Viz' key='b'");
	TwAddVarRW(mainMenuBar, "Bullet Closest Point Pairs", TW_TYPE_BOOLCPP, &drawClosestPointPairs, " label='Draw Closest Point Pairs Info' group='Viz' key='p'");

	//	TwAddVarRW(mainMenuBar, "Cost function plots", TW_TYPE_BOOLCPP, &drawPlots, " label='Draw cost plots' group='Viz'");

	tWidget = new TranslateWidget(AXIS_X | AXIS_Y | AXIS_Z);
	rWidget = new RotateWidgetV2();
	rWidgetDir = new RotateWidgetV1();
	rWidgetDir->axis->localAxis = V3D(0, 1, 0);


	objectMenuList.push_back(new MakeBlockMotorDriver());
	objectMenuList.push_back(new MakeBlockOrionBoard());
	objectMenuList.push_back(new MakeBlockUltrasonic());
	objectMenuList.push_back(new MakeBlockBluetooth());
	//objectMenuList.push_back(new MakeBlockEncoderMotorDriver());
	//objectMenuList.push_back(new MakeBlockAccelerometer());
	//objectMenuList.push_back(new DCMotor());
	//objectMenuList.push_back(new ServoMotor());
	//objectMenuList.push_back(new MicroServoMotor());
	objectMenuList.push_back(new Battery9v());
	//objectMenuList.push_back(new Battery12vHolder());
	//objectMenuList.push_back(new MakeBlockSoundSensor());
	//objectMenuList.push_back(new MakeBlockLED());
	//objectMenuList.push_back(new MakeBlockButton());
	//objectMenuList.push_back(new MakeBlockMegaPi());

	for (uint i = 0; i < objectMenuList.size(); i++) {
		windowArray->addSubWindow(new ObjectDisplayWindow(objectMenuList[i]));
	}

	planner = new MCMCPlanners(&mainAssembly);


	plotWindow = new PlotWindow(260 * 1.14, mainWindowHeight - 400, 500, 400);
	plotWindow->createData("optValues", 1, 0, 0);
	plotWindow->createData("bestValue", 0, 1, 0);
	//plotWindow->createData("best10", 0, 0, 1);


	// user study enclosure and motors for each example:
	//string wallE = "siggraph17_results/userStudies/clumsy.txt";
	//mainAssembly.loadAssembly(wallE.c_str());
}

UIUserStudy::~UIUserStudy(void) {
}

//triggered when mouse moves
bool UIUserStudy::onMouseMoveEvent(double xPos, double yPos) {
	if (tWidget->onMouseMoveEvent(xPos, yPos) == true) return true;
	if (rWidget->onMouseMoveEvent(xPos, yPos) == true) return true;
	if (rWidgetDir->onMouseMoveEvent(xPos, yPos) == true) return true;

	if (menuSelectedObject) {
		unloadMenuNameFor(menuSelectedObject);
		menuSelectedObject = NULL;
	}

	if (windowArray->isActive() == true) {
		if (selectedObject) {
			unloadMenuParametersFor(selectedObject);
			selectedObject->picked = false;
			selectedObject->onComponentDeselect();
			selectedObject = NULL;
		}

		for (size_t i = 0; i < windowArray->subWindows.size(); i++)
		{
			if (windowArray->subWindows[i]->isActive()) {
				menuSelectedObject = objectMenuList[i];
				loadMenuNameFor(objectMenuList[i]);
			}
		}

	}

	if (windowArray->onMouseMoveEvent(xPos, yPos) == true)
		return true;
	if (GLApplication::onMouseMoveEvent(xPos, yPos) == true)
		return true;

	return false;
}

void UIUserStudy::loadMenuNameFor(BaseObject* objPrimitive) {
	//obj name
	std::string objName = objPrimitive->name;
	TwAddButton(mainMenuBar, objName.c_str(), NULL, NULL, " ");
}

void UIUserStudy::unloadMenuNameFor(BaseObject* objPrimitive) {
	std::string objName = objPrimitive->name;
	TwRemoveVar(mainMenuBar, objName.c_str());
}

void UIUserStudy::loadMenuParametersFor(BaseObject* objPrimitive) {
	//obj name
	std::string objName = objPrimitive->name;
	TwAddButton(mainMenuBar, objName.c_str(), NULL, NULL, " ");
	//obj assembly-dir properties
	DynamicArray<std::pair<char*, double*>> params1 = objPrimitive->getObjectParameters();
	for (auto it = params1.begin(); it != params1.end(); ++it) {
		TwAddVarRW(mainMenuBar, it->first, TW_TYPE_DOUBLE, it->second, "min=-1 max=1 step=0.1");
	}

	EMComponent* emComponent = dynamic_cast<EMComponent*>(objPrimitive);
	if (!emComponent) return;

	// obj disassembly index
	TwAddButton(mainMenuBar, "Assembly plan order", NULL, NULL, " ");
	int maxIndex = mainAssembly.electroMechanicalComponents.size() - 1;
	string range = "min=0 max=" + to_string(maxIndex) + " step=1";
	emComponent->setAssemblyIndex(maxIndex);
	DynamicArray<std::pair<char*, int*>> paramsAssem = emComponent->getOrder(maxIndex);
	for (auto it = paramsAssem.begin(); it != paramsAssem.end(); ++it) {
		TwAddVarRW(mainMenuBar, it->first, TW_TYPE_INT32, it->second, range.c_str());
	}
}

void UIUserStudy::unloadMenuParametersFor(BaseObject* objPrimitive) {
	std::string objName = objPrimitive->name;
	TwRemoveVar(mainMenuBar, objName.c_str());
	DynamicArray<std::pair<char*, double*>> params1 = objPrimitive->getObjectParameters();
	for (auto it = params1.begin(); it != params1.end(); ++it)
		TwRemoveVar(mainMenuBar, it->first);

	EMComponent* emComponent = dynamic_cast<EMComponent*>(objPrimitive);
	if (!emComponent) return;


	int maxIndex = mainAssembly.electroMechanicalComponents.size() - 1;
	TwRemoveVar(mainMenuBar, "Assembly plan order");
	DynamicArray<std::pair<char*, int*>> params3 = emComponent->getOrder(maxIndex);
	for (auto it = params3.begin(); it != params3.end(); ++it) {
		TwRemoveVar(mainMenuBar, it->first);
	}
}

//triggered when using the mouse wheel
bool UIUserStudy::onMouseWheelScrollEvent(double xOffset, double yOffset) {
	if (windowArray->onMouseWheelScrollEvent(xOffset, yOffset)) return true;

	if (GLApplication::onMouseWheelScrollEvent(xOffset, yOffset)) return true;

	return false;
}

void UIUserStudy::loadFile(const char* fName) {
	// LOADING CHASSIS IN FUTURE AS MESH
	//std::string fileName;
	//fileName.assign(fName);
	//std::string fNameExt = fileName.substr(fileName.find_last_of('.') + 1);

	//if (fNameExt == "obj")
	//{
	//	
	//}

	mainAssembly.collisionManager->clearCollisionPrimitives();
	if (selectedObject) {
		unloadMenuParametersFor(selectedObject);
		selectedObject->onComponentDeselect();
		selectedObject = NULL;
	}

	while (!mainAssembly.electroMechanicalComponents.empty()) {
		delete mainAssembly.electroMechanicalComponents.back();
		mainAssembly.electroMechanicalComponents.pop_back();
	}

	mainAssembly.constraintsManager->constraintsCollection->softConstraints.clear();
	planner->plannerState->initialized = false;
	planner->plannerState->converged = false;
	planner->plannerState->mode = NEW_ASSEMBLY_MODE;
	// for balbot..
	//planner->plannerState->mode = UPDATED_ASSEMBLY_MODE;

	runOption = DEFAULT;
	animPhase = 0.0;

	mainAssembly.loadAssembly(fName);
}

void UIUserStudy::saveFile(const char* fName) {
	Logger::consolePrint("SAVE FILE: Do not know what to do with file \'%s\'\n", fName);
}

bool UIUserStudy::onKeyEvent(int key, int action, int mods) {
	if (key >= '1' && key <= '5') {
		runOption = key - '1';

		if (action == GLFW_PRESS) {
			this->appIsRunning = true;
			if (runOption == DISASSEMBLY_VISUALIZATION || runOption == DISASSEMBLY_VISUALIZATION_WITH_SWEPT_OBJECTS)
				Logger::consolePrint("Animating assembly process!\n");
		}

		return true;
	}

	if (windowArray->onKeyEvent(key, action, mods)) return true;

	// preventing app killing on esc press
	if (key == GLFW_KEY_ESCAPE) {
		return true;
	}

	if (GLApplication::onKeyEvent(key, action, mods)) return true;

	return false;
}

bool UIUserStudy::onCharacterPressedEvent(int key, int mods) {
	if (key == 'r') {
		translateRotateWidget = !translateRotateWidget;
		return true;
	}

	if (key == 'f') {
		if (selectedObject)
			selectedObject->toggleFreeze();
	}

	if (key == 'a') {
		rWidgetDir->visible = !rWidgetDir->visible;
		rWidget->visible = false;
		tWidget->visible = false;
		return true;
	}

	if (windowArray->onCharacterPressedEvent(key, mods)) return true;
	if (GLApplication::onCharacterPressedEvent(key, mods)) return true;

	return false;
}

//triggered when mouse buttons are pressed
bool UIUserStudy::onMouseButtonEvent(int button, int action, int mods, double xPos, double yPos) {
	if (tWidget->isPicked() || rWidgetDir->isPicked() || rWidget->isPicked()) return true;
	Ray clickedRay = getRayFromScreenCoords(xPos, yPos);

	//if (rWidget->isPicked() && (mods & GLFW_MOD_SHIFT)) {
	//	auto rAxes = dynamic_cast<RotateWidgetV2*> (rWidget)->rAxes;
	//	for (auto it = rAxes.begin(); it != rAxes.end(); ++it)
	//		if (it->isPicked) {

	//			double newTorusAngle = it->getAngleOnTorusForRay(clickedRay);
	//			double ang;
	//			Quaternion q = rWidget->getOrientation();
	//			ang = q.getRotationAngle(it->rotationAxis);
	//			double angleOffset = abs(ang);// -it->lastPickedAngle;
	//			if (angleOffset > 2 * PI) angleOffset -= 2 * PI;
	//			if (angleOffset < -2 * PI) angleOffset += -2 * PI;

	//			Logger::consolePrint("offset:%lf, ang:%lf\n\n", angleOffset, newTorusAngle, it->lastPickedAngle,ang);
	//			if (abs(angleOffset) > 3 * PI / 2 + PI / 4 && abs(angleOffset) <= PI / 4) angleOffset = 0;
	//			if (abs(angleOffset) > PI / 4 && abs(angleOffset) <= PI / 2 + PI / 4) angleOffset = PI / 2 * SGN(ang);
	//			if (abs(angleOffset) >  PI/2 + PI / 4 && abs(angleOffset) <= PI + PI/4) angleOffset = PI*SGN(ang);
	//			if (abs(angleOffset) > PI + PI/4 && abs(angleOffset) <= 3*PI/2 + PI/4) angleOffset = 3*PI/2 * SGN(ang);

	//			//note: the twist angle is not bounded on either side, so it can increase arbitrarily...
	//			Quaternion orientation;
	//			orientation = getRotationQuaternion(angleOffset, it->rotationAxis)*q;
	//			//it->lastPickedAngle = angleOffset;
	//			rWidget->setOrientation(orientation);
	//			return true;
	//		}
	//}
	//else if (rWidget->isPicked())
	//return true;

	if (GLApplication::onMouseButtonEvent(button, action, mods, xPos, yPos)) return true;

	if (windowArray->isActive() == false && button == GLFW_MOUSE_BUTTON_LEFT && action == GLFW_PRESS) {
		// check to see which menu item is selected and clone it
		for (uint i = 0; i < windowArray->subWindows.size(); i++) {
			if (windowArray->subWindows[i]->isSelected() || (i == 0 && mods & GLFW_MOD_ALT)) {

				//unselect any previously selected objects
				if (selectedObject) {
					unloadMenuParametersFor(selectedObject);
					selectedObject->picked = false;
					selectedObject->onComponentDeselect();
					selectedObject = NULL;
				}

				P3D clickedPoint;
				clickedRay.getDistanceToPoint(P3D(), &clickedPoint);
				tWidget->pos = clickedPoint;
				mainAssembly.electroMechanicalComponents.push_back(objectMenuList[i]->clone());
				mainAssembly.electroMechanicalComponents.back()->disassemblyIndex = mainAssembly.electroMechanicalComponents.size() - 1;
				mainAssembly.electroMechanicalComponents.back()->setPosition(clickedPoint);
				windowArray->subWindows[i]->selected = false;

				planner->plannerState->initialized = false;
				planner->plannerState->converged = false;
				planner->plannerState->mode = UPDATED_ASSEMBLY_MODE;
				return true;
			}
		}

		// check if object in main window is selected

		//first of all, if the shift key is not held down, then unpick everything...
		if (!(mods & GLFW_MOD_SHIFT)) {
			for (uint i = 0; i < mainAssembly.electroMechanicalComponents.size(); i++)
				mainAssembly.electroMechanicalComponents[i]->picked = false;
		}

		mainAssembly.collisionManager->setupCollisionPrimitives(0, CM_EMCOMPONENTS);
		BaseObject* selectionObject = mainAssembly.collisionManager->getFirstObjectHitByRay(clickedRay);

		if (selectionObject)
			selectionObject->picked = !selectionObject->picked;

		//now, if only one object is picked, then it is selected. And selected objects have some special meaning...
		BaseObject* newObjectSelection = NULL;
		for (uint i = 0; i < mainAssembly.electroMechanicalComponents.size(); i++) {
			if (mainAssembly.electroMechanicalComponents[i]->picked) {
				if (newObjectSelection == NULL)
					newObjectSelection = mainAssembly.electroMechanicalComponents[i];
				else {
					newObjectSelection = NULL;
					break;
				}
			}
		}

		//now, if we have a new selection, then deselect the old one, and select the new one
		if (newObjectSelection != selectedObject) {
			if (selectedObject != NULL) {
				unloadMenuParametersFor(selectedObject);
				selectedObject->onComponentDeselect();
			}
			if (newObjectSelection != NULL) {
				loadMenuParametersFor(newObjectSelection);
				newObjectSelection->setWidgetsFromObjectState(tWidget, rWidget);
				if (dynamic_cast<EMComponent*> (newObjectSelection)) {
					V3D v = dynamic_cast<EMComponent*> (newObjectSelection)->disassemblyPath->getWorldCoordinatesAssemblyPath();
					Quaternion Q = getRotationAxisThatAlignsVectors(V3D(0, 1, 0), v);
					rWidgetDir->setOrientation(Q);
				}
				translateRotateWidget = true;
				newObjectSelection->onComponentSelect();
			}
			selectedObject = newObjectSelection;
		}
	}

	if (windowArray->onMouseButtonEvent(button, action, mods, xPos, yPos)) return true;

	return false;
}


// Run the App tasks
void UIUserStudy::process() {
	// do the work here...
	if (selectedObject) {
		selectedObject->setObjectStateFromWidgets(tWidget, rWidget);
	}

	if (runOption == DEFAULT) {
		animPhase = 0.0;
		this->appIsRunning = false;
	}

	if (runOption == DISASSEMBLY_VISUALIZATION || runOption == DISASSEMBLY_VISUALIZATION_WITH_SWEPT_OBJECTS) {
		animPhase += animStep / (MAX(mainAssembly.getSortedListOfAssemblableComponents().size(), 1));
		//if (animPhase > 1) animPhase -= 1;
		if (animPhase > 1) {
			animPhase = 1;
			runOption = DEFAULT;
		}
	}
}

// Draw the App scene - camera transformations, lighting, shadows, reflections, etc apply to everything drawn by this method
void UIUserStudy::drawScene() {
	//sync with the widgets...
	rWidget->pos = tWidget->pos;
	rWidgetDir->pos = tWidget->pos;

	if (selectedObject) {
		if (!rWidgetDir->visible) {
			tWidget->visible = translateRotateWidget;
			rWidget->visible = !translateRotateWidget;
		}
		selectedObject->setObjectStateFromWidgets(tWidget, rWidget);
		if (dynamic_cast<EMComponent*> (selectedObject)) {
			Quaternion Q = rWidgetDir->getOrientation();
			dynamic_cast<EMComponent*> (selectedObject)->disassemblyPath->setWorldCoordinatesAssemblyPath(Q);
		}
		selectedObject->onComponentSelect();
	}
	else {
		tWidget->visible = rWidget->visible = false;
		rWidgetDir->visible = false;
	}

	//updating support at each step right now..
	mainAssembly.snapSupportsToWalls();

	//draw all the support structures...
	DynamicArray<RigidlyAttachedSupport*> supportObjs;
	for (uint i = 0; i < mainAssembly.electroMechanicalComponents.size(); i++) {
		supportObjs.clear();
		mainAssembly.electroMechanicalComponents[i]->addSupportsToList(supportObjs);
		for (uint j = 0; j < supportObjs.size(); j++) {
			supportObjs[j]->draw();
			//supportObjs[j]->drawWallExtension();
		}

		// visualize wire meshes
		//if (dynamic_cast<MakeBlockComponents*>(mainAssembly.electroMechanicalComponents[i]) &&
		//!dynamic_cast<Battery9v*>(mainAssembly.electroMechanicalComponents[i]))
		//dynamic_cast<MakeBlockComponents*>(mainAssembly.electroMechanicalComponents[i])->getWireGeometryMesh()->drawMesh();
	}


	//draw collision primitives...
	if (mainAssembly.collisionManager && drawBulletCPs) {
		//draw them once in their original, pickable position...

		//and now in their animated position...
		if (runOption == DISASSEMBLY_VISUALIZATION || runOption == DISASSEMBLY_VISUALIZATION_WITH_SWEPT_OBJECTS) {
			if (runOption == DISASSEMBLY_VISUALIZATION)
				mainAssembly.collisionManager->setupCollisionPrimitives(animPhase);
			else
				mainAssembly.collisionManager->setupCollisionPrimitives(animPhase, CM_EMCOMPONENTS, COL_SWEPT_CVX_HULL);
			mainAssembly.collisionManager->drawCollisionPrimitives();
			mainAssembly.collisionManager->processCollisions();
			for (uint i = 0; i < mainAssembly.collisionObjects.size(); i++) {
				glColor3d(0, 1, 0);
				drawSphere(mainAssembly.collisionObjects[i].p_WorldOnO1, 0.001);
				glColor3d(0, 0, 1);
				drawSphere(mainAssembly.collisionObjects[i].p_WorldOnO2, 0.001);
			}
		}
		else {
			mainAssembly.collisionManager->setupCollisionPrimitives();
			mainAssembly.collisionManager->drawCollisionPrimitives();
		}
	}

	glEnable(GL_LIGHTING);

	if (mainAssembly.collisionManager && drawClosestPointPairs) {
		mainAssembly.collisionManager->performCollisionDetectionForEntireAssemblyProcess();
		//NOTE: at some point may want to be much more conservative with when we even check collisions between objects i and j... perhaps through an overall convex hull that includes swept object, supports, fastners?
		/*for (uint i = 0; i < mainAssembly.collisionObjects.size(); i++){
		double panicLevel = mapTo01Range(V3D(mainAssembly.collisionObjects[i].p_WorldOnO1, mainAssembly.collisionObjects[i].p_WorldOnO2).length(), 0, 0.05);
		glColor3d(1, panicLevel, panicLevel);
		drawCylinder(mainAssembly.collisionObjects[i].p_WorldOnO1, mainAssembly.collisionObjects[i].p_WorldOnO2, 0.002);
		}*/
		if (runOption != DISASSEMBLY_VISUALIZATION && runOption != DISASSEMBLY_VISUALIZATION_WITH_SWEPT_OBJECTS) {
			for (uint i = 0; i < mainAssembly.collisionObjects.size(); i++) {
				if (mainAssembly.collisionObjects[i].penetrationDepth < 0) {
					//if ((dynamic_cast<WallObject*>(mainAssembly.collisionObjects[i].obj1)) ||
					//	(dynamic_cast<WallObject*>(mainAssembly.collisionObjects[i].obj2))) {
					//	mainAssembly.chassis->collided = true;

					//	if ((dynamic_cast<FastenerCollisionObject*>(mainAssembly.collisionObjects[i].obj1)) ||
					//		(dynamic_cast<FastenerCollisionObject*>(mainAssembly.collisionObjects[i].obj2))) {
					//		continue;
					//	}
					//	else {
					//		mainAssembly.chassis->collideColor = abs(mainAssembly.collisionObjects[i].penetrationDepth) * 10;
					//	}
					//}
					////else {
					//	//double panicLevel = mapTo01Range(V3D(mainAssembly.collisionObjects[i].p_WorldOnO1, mainAssembly.collisionObjects[i].p_WorldOnO2).length(), 0, 0.09);
					//	mainAssembly.collisionObjects[i].obj1->collided = true;
					//	mainAssembly.collisionObjects[i].obj2->collided = true;

					//	if ((dynamic_cast<FastenerCollisionObject*>(mainAssembly.collisionObjects[i].obj1)) ||
					//		(dynamic_cast<FastenerCollisionObject*>(mainAssembly.collisionObjects[i].obj2))) {
					//		continue;
					//	}
					//	else {
					//		mainAssembly.collisionObjects[i].obj2->collideColor = abs(mainAssembly.collisionObjects[i].penetrationDepth) * 10;
					//		mainAssembly.collisionObjects[i].obj1->collideColor = abs(mainAssembly.collisionObjects[i].penetrationDepth) * 10;
					//	}
					////}

					/*Logger::consolePrint(" len:%lf, PD:%lf, obj1Name:%s, obj2Name:%s, collideColor: %lf \n", V3D(mainAssembly.collisionObjects[i].p_WorldOnO1, mainAssembly.collisionObjects[i].p_WorldOnO2).length(),
					mainAssembly.collisionObjects[i].penetrationDepth, mainAssembly.collisionObjects[i].obj1->name.c_str(),
					mainAssembly.collisionObjects[i].obj2->name.c_str(), 0.5 + abs(mainAssembly.collisionObjects[i].penetrationDepth) * 10);*/
					////plotWindow->addDataPoint(0, V3D(mainAssembly.collisionObjects[i].p_WorldOnO1, mainAssembly.collisionObjects[i].p_WorldOnO2).length());
					////plotWindow->addDataPoint(1, abs(mainAssembly.collisionObjects[i].penetrationDepth));

					if ((dynamic_cast<WallObject*>(mainAssembly.collisionObjects[i].obj1)) ||
						(dynamic_cast<WallObject*>(mainAssembly.collisionObjects[i].obj2))) {
						mainAssembly.chassis->collided = true;
					}
					if ((dynamic_cast<FastenerCollisionObject*>(mainAssembly.collisionObjects[i].obj1)))
						(dynamic_cast<FastenerCollisionObject*>(mainAssembly.collisionObjects[i].obj1))->parent->collided = true;
					if ((dynamic_cast<FastenerCollisionObject*>(mainAssembly.collisionObjects[i].obj2)))
						(dynamic_cast<FastenerCollisionObject*>(mainAssembly.collisionObjects[i].obj2))->parent->collided = true;
					mainAssembly.collisionObjects[i].obj1->collided = true;
					mainAssembly.collisionObjects[i].obj2->collided = true;
				}
			}
		}
	}

	// draw objects and fasteners in the assembly...
	if (runOption == DISASSEMBLY_VISUALIZATION || runOption == DISASSEMBLY_VISUALIZATION_WITH_SWEPT_OBJECTS) {
		//first off, see how many items there are to be disassembled... EMcomponents, fastners, etc. And we assume each one of these is disassemblable, so there are as many unique disassembly indices as objects...
		//DynamicArray<AssemblableComponent*> assemblableComponents = mainAssembly.getSortedListOfAssemblableComponents();
		DynamicArray<AssemblableComponent*> assemblableComponents = mainAssembly.getSortedListReverse();

		double range = 1.0 / assemblableComponents.size();
		int activeObjIndex = (int)(animPhase / range);
		double activeObjPhase = (animPhase - activeObjIndex * range) / range;
		//NOTE: we are assuming that all these assemblable components are sorted by disassembly index, with dependents (e.g. fastners) following their electromechanical component parents
		for (uint i = assemblableComponents.size(); i-- > 0;) {
			//for (uint i = 0; i < assemblableComponents.size(); i++) {
			if ((int)i < activeObjIndex)
				assemblableComponents[i]->drawOnAssemblyPath(0);
			else if ((int)i == activeObjIndex) {
				mainAssembly.collisionManager->highlightComponentsCollidingWith(assemblableComponents[i]);
				assemblableComponents[i]->drawOnAssemblyPath(1.00 - activeObjPhase);
				//assemblableComponents[i]->drawOnAssemblyPath(activeObjPhase);
			}
			//else
			//	assemblableComponents[i]->drawOnAssemblyPath(1);
		}
		if (runOption == DISASSEMBLY_VISUALIZATION_WITH_SWEPT_OBJECTS) {
			assemblableComponents[activeObjIndex]->drawSweptConvexHull();
		}
	}
	else {
		for (uint i = 0; i < mainAssembly.electroMechanicalComponents.size(); i++)
			mainAssembly.electroMechanicalComponents[i]->draw();
		//draw all the fasteners...
		DynamicArray<Fastener*> fastenerObjs;
		for (uint i = 0; i < mainAssembly.electroMechanicalComponents.size(); i++) {
			fastenerObjs.clear();
			mainAssembly.electroMechanicalComponents[i]->addFastnersToList(fastenerObjs);
			for (uint j = 0; j < fastenerObjs.size(); j++) {
				fastenerObjs[j]->draw();
			}
		}
	}

	// chassis and walls..
	if (mainAssembly.chassis)
		mainAssembly.chassis->draw();
	for (size_t i = 0; i < mainAssembly.chassis->walls.size(); i++) {
		mainAssembly.chassis->walls[i].draw();
	}
}

// This is the wild west of drawing - things that want to ignore depth buffer, camera transformations, etc. Not pretty, quite hacky, but flexible. Individual apps should be careful with implementing this method. It always gets called right at the end of the draw function
void UIUserStudy::drawAuxiliarySceneInfo() {
	//clear the depth buffer so that the widgets show up on top of the object primitives
	glClear(GL_DEPTH_BUFFER_BIT);
	glDisable(GL_TEXTURE_2D);

	// draw the widgets
	tWidget->draw();
	rWidget->draw();
	rWidgetDir->draw();

	// plots of cost function
	if (drawPlots)
		plotWindow->draw();

	// subwindow container window drawn
	windowArray->draw();

	// displaying closest points for debugging
	////if (runOption != DISASSEMBLY_VISUALIZATION && runOption != DISASSEMBLY_VISUALIZATION_WITH_SWEPT_OBJECTS) {
	//	for (uint i = 0; i < mainAssembly.collisionObjects.size(); i++) {
	//		if (mainAssembly.collisionObjects[i].penetrationDepth < 0) {
	//			P3D p1 = mainAssembly.collisionObjects[i].p_WorldOnO1;
	//			P3D p2 = mainAssembly.collisionObjects[i].p_WorldOnO2;
	//			glColor3d(1, 0, 0);
	//			drawSphere(p1, 0.002);
	//			glColor3d(0, 1, 0);
	//			drawSphere(p2, 0.002);

	//			//if (dynamic_cast<WallObject*>(mainAssembly.collisionObjects[i].obj1)) {
	//			//	glColor3d(0, 0, 1);
	//			//	drawArrow((dynamic_cast<WallObject*>(mainAssembly.collisionObjects[i].obj1))->wallCenter, p1, 0.001);
	//			//	glColor3d(0, 1, 1);
	//			//	drawArrow((dynamic_cast<WallObject*>(mainAssembly.collisionObjects[i].obj1))->wallCenter, (dynamic_cast<WallObject*>(mainAssembly.collisionObjects[i].obj1))->bounds, 0.001);
	//			//}

	//			//if (dynamic_cast<WallObject*>(mainAssembly.collisionObjects[i].obj2)) {
	//			//	glColor3d(0, 0, 1);
	//			//	drawArrow((dynamic_cast<WallObject*>(mainAssembly.collisionObjects[i].obj2))->wallCenter, p2, 0.001);
	//			//	glColor3d(0, 1, 1);
	//			//	drawArrow((dynamic_cast<WallObject*>(mainAssembly.collisionObjects[i].obj2))->wallCenter, (dynamic_cast<WallObject*>(mainAssembly.collisionObjects[i].obj2))->bounds, 0.001);
	//			//}

	//			}
	//		}
	//	//}

	// wall normals
	//for (size_t i = 0; i < mainAssembly.chassis->walls.size(); i++)
	//{
	//	glColor3d(1, 0, 0);
	//	drawArrow(mainAssembly.chassis->walls[i].wallCenter, mainAssembly.chassis->walls[i].wallCenter + mainAssembly.chassis->walls[i].normal*
	//		0.05, 0.001);
	//	glColor3d(0, 1, 0);
	//	drawSphere(mainAssembly.chassis->walls[i].wallCenter, 0.005);
	//}
}

// Restart the application.
void UIUserStudy::restart() {
	mainAssembly.collisionManager->clearCollisionPrimitives();
	if (selectedObject) {
		unloadMenuParametersFor(selectedObject);
		selectedObject->onComponentDeselect();
		selectedObject = NULL;
	}

	while (!mainAssembly.electroMechanicalComponents.empty()) {
		delete mainAssembly.electroMechanicalComponents.back();
		mainAssembly.electroMechanicalComponents.pop_back();
	}

	mainAssembly.constraintsManager->constraintsCollection->softConstraints.clear();
	planner->plannerState->initialized = false;
	planner->plannerState->converged = false;
	planner->plannerState->mode = NEW_ASSEMBLY_MODE;
	// for balbot..
	//planner->plannerState->mode = UPDATED_ASSEMBLY_MODE;

	runOption = DEFAULT;
	animPhase = 0.0;

	// reloading user study file:
	//string wallE = "siggraph17_results/userStudies/clumsy.txt";
	//mainAssembly.loadAssembly(wallE.c_str());
}

bool UIUserStudy::processCommandLine(const std::string& cmdLine) {

	if (strcmp(cmdLine.c_str(), "delete") == 0) {
		onDelete();
		return true;
	}

	if (strcmp(cmdLine.c_str(), "save") == 0) {
		mainAssembly.saveAssembly();
		return true;
	}

	if (strcmp(cmdLine.c_str(), "load") == 0) {
		mainAssembly.loadAssembly("assembly.txt");
		return true;
	}

	if (GLApplication::processCommandLine(cmdLine)) return true;

	return false;
}


void UIUserStudy::onDelete() {

	// resetting run option
	runOption = DEFAULT;

	// reset planner after deleting
	planner->plannerState->initialized = false;
	planner->plannerState->converged = false;
	planner->plannerState->mode = UPDATED_ASSEMBLY_MODE;

	mainAssembly.collisionManager->clearCollisionPrimitives();
	if (selectedObject) {
		unloadMenuParametersFor(selectedObject);
		selectedObject->onComponentDeselect();
		selectedObject = NULL;
	}

	for (uint i = 0; i < mainAssembly.electroMechanicalComponents.size();) {
		if (mainAssembly.electroMechanicalComponents[i]->picked) {
			delete mainAssembly.electroMechanicalComponents[i];
			mainAssembly.electroMechanicalComponents.erase(mainAssembly.electroMechanicalComponents.begin() + i);
		}
		else
			++i;
	}

	planner->setTopDownAssemblyOrder(&mainAssembly);

	/*for (uint i = 0; i < mainAssembly.electroMechanicalComponents.size(); i++) {
	mainAssembly.electroMechanicalComponents[i]->disassemblyIndex = i;
	}*/

	mainAssembly.collisionObjects.clear();
	mainAssembly.collisionManager->clearCollisionPrimitives();


}

void TW_CALL UIButtonEventCreateSupport2(void* clientData) {
	if (strcmp((char*)clientData, "support") == 0) {
		UIUserStudy* app = dynamic_cast<UIUserStudy*>(GLApplication::getGLAppInstance());
		if (app)
			app->mainAssembly.snapSupportsToWalls();
	}
}


void TW_CALL UIButtonEventLoad2(void* clientData) {
	if (strcmp((char*)clientData, "load") == 0) {
		UIUserStudy* app = dynamic_cast<UIUserStudy*>(GLApplication::getGLAppInstance());
		if (app) {
			app->mainAssembly.collisionManager->clearCollisionPrimitives();
			if (app->selectedObject) {
				app->unloadMenuParametersFor(app->selectedObject);
				app->selectedObject->onComponentDeselect();
				app->selectedObject = NULL;
			}

			while (!app->mainAssembly.electroMechanicalComponents.empty()) {
				delete app->mainAssembly.electroMechanicalComponents.back();
				app->mainAssembly.electroMechanicalComponents.pop_back();
			}

			app->mainAssembly.constraintsManager->constraintsCollection->softConstraints.clear();
			app->mainAssembly.loadAssembly("assembly.txt");
		}
	}
}

void TW_CALL UIButtonEventDelete2(void* clientData) {
	if (strcmp((char*)clientData, "delete") == 0) {
		UIUserStudy* app = dynamic_cast<UIUserStudy*>(GLApplication::getGLAppInstance());
		if (app)
			app->onDelete();
	}
}

void TW_CALL UIButtonEventSave2(void* clientData) {
	if (strcmp((char*)clientData, "save") == 0) {
		UIUserStudy* app = dynamic_cast<UIUserStudy*>(GLApplication::getGLAppInstance());
		if (app)
			app->mainAssembly.saveAssembly();
	}
}

void TW_CALL UIButtonEventAssemblyFillRatio2(void* clientData) {
	if (strcmp((char*)clientData, "fillRatio") == 0) {
		UIUserStudy* app = dynamic_cast<UIUserStudy*>(GLApplication::getGLAppInstance());
		if (app) {
			double ratio = app->mainAssembly.calculateFillRatio();
			Logger::consolePrint("Assembly fill ratio: %lf\n", ratio);
			//double cost = app->planner->getAssemblyCost(&app->mainAssembly);
			//Logger::consolePrint("Assembly cost: %lf\n", cost);
		}
	}
}

void TW_CALL UIButtonEventSetAssemblyOrder2(void* clientData) {
	if (strcmp((char*)clientData, "order") == 0) {
		UIUserStudy* app = dynamic_cast<UIUserStudy*>(GLApplication::getGLAppInstance());
		if (app) {
			app->planner->setTopDownAssemblyOrder(&app->mainAssembly);
			Logger::consolePrint(" set order based on height\n");
		}
	}
}

void TW_CALL UIButtonEventSwapOrder2(void* clientData) {
	if (strcmp((char*)clientData, "swaporder") == 0) {
		UIUserStudy* app = dynamic_cast<UIUserStudy*>(GLApplication::getGLAppInstance());
		if (app) {
			DynamicArray<EMComponent*> toSwapComponents;
			for (size_t i = 0; i < app->mainAssembly.electroMechanicalComponents.size(); i++)
			{
				if (app->mainAssembly.electroMechanicalComponents[i]->picked) {
					toSwapComponents.push_back(app->mainAssembly.electroMechanicalComponents[i]);
				}
			}

			if (toSwapComponents.size() > 2)
				Logger::consolePrint("Select only 2 components for swap\n");
			else {
				std::swap(toSwapComponents[0]->disassemblyIndex, toSwapComponents[1]->disassemblyIndex);
				Logger::consolePrint("Swapped assembly order of selected components\n");
			}

			toSwapComponents.clear();
		}
	}
}

void TW_CALL UIButtonEventResetAnim2(void* clientData) {
	if (strcmp((char*)clientData, "reset") == 0) {
		UIUserStudy* app = dynamic_cast<UIUserStudy*>(GLApplication::getGLAppInstance());
		if (app) {
			app->animPhase = 0.00;
		}
	}

}


void TW_CALL UIButtonEventExport2(void* clientData) {
	if (strcmp((char*)clientData, "export") == 0) {
		UIUserStudy* app = dynamic_cast<UIUserStudy*>(GLApplication::getGLAppInstance());
		if (app) {
			app->mainAssembly.saveCAD();
			Logger::consolePrint("Exported support and fastener geomerty for external boolean operations\n");
		}
	}
}


