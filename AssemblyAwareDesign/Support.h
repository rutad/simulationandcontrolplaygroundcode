#pragma once
#include "BaseObject.h"
#include "Fastener.h"
#include "SupportVolumeCostObject.h"

class Support : public BaseObject {

public:
	// support's parent
	EMComponent* parent = NULL;
	// for any support, this is the extension from the wall to it..
	GLMesh globalCoordinatesWallExtensionMesh;

	// plane of closest wall
	Plane closestWallPlane;
	// to make sure a wall is always assigned..
	int closestWallId = -1;

	// normal directions along which the support dimensions cannot be changed
	DynamicArray<V3D> nonResizeableDirections;

	// constructor
	Support(EMComponent* component) : BaseObject() {
		name = "Support";
		parent = component;
	}
	
	virtual ~Support() {
		if (globalCoordinatesWallExtensionMesh.getVertexCount() > 0)
			globalCoordinatesWallExtensionMesh.clear();
	};

	virtual bool shouldCollideWith(BaseObject* obj) {
		// support shouldn't collide with its parent
		if (dynamic_cast<EMComponent*>(obj)) {
			if (this->parent == dynamic_cast<EMComponent*>(obj))
				return false;
		}

		// support shouldn't collide with its fastener
		if (dynamic_cast<FastenerCollisionObject*>(obj)) {
			if (this->parent == dynamic_cast<FastenerCollisionObject*>(obj)->parent->parent)
				return false;
		}

		return true;
	}

	virtual P3D getStartPositionInParentLocalCoordinates() {
		return P3D();
	}

	virtual P3D getPosition() {
		setPosition(parent->getWorldCoordinates(getStartPositionInParentLocalCoordinates()));
		return BaseObject::getPosition();
	}

	virtual Quaternion getOrientation() {
		setOrientation(parent->getOrientation());
		return BaseObject::getOrientation();
	}

	virtual void addBulletCollisionObjectsToList(DynamicArray<btCollisionObject*>& bulletCollisionObjs) = 0;

	// update support size to snap to the closest wall of the chassis
	virtual void updateSupportSize() {};

	// check if the normal direction along which we will resize the support is valid.
	bool validateResizeDirection(V3D normalToClosestWall) {
		bool validDir = true;
		for (uint i = 0; i < nonResizeableDirections.size(); i++) {
			if (normalToClosestWall.angleWith(this->getWorldCoordinates(nonResizeableDirections[i])) < 0.2) {
				validDir = false;
				break;
			}
		}
		return validDir;
	};

	void drawWallExtension() {
		GLShaderMaterial tmpMat;
		tmpMat.r = 0.55; tmpMat.g = 0.28; tmpMat.b = 0.07; tmpMat.a = 0.7;
		if (collided)
			tmpMat.r = 1.00;
		globalCoordinatesWallExtensionMesh.setMaterial(tmpMat);
		globalCoordinatesWallExtensionMesh.drawMesh();
		
	}

};

class RigidlyAttachedSupport : public Support {
public:
	//these are the local coordinates of the support object relative to the parent
	P3D parentLocalSupportPos;
	//and this is the bounding box (internal representation of minimum support reqd.)
	AxisAlignedBoundingBox bBox;
	btBoxShape colShape = btBoxShape(btVector3(1.0f, 1.0f, 1.0f));
	//collision shape and object (defined in world co-ordinates)
	btConvexHullShape btSupportHullShape;
	btCollisionObject colObject;
	P3D geoMeshCenter = P3D(0, 0, 0);

	// Objective function used to fix the state of an object...
	SupportVolumeCostObject* supportVolCostObj = NULL;

	RigidlyAttachedSupport(EMComponent* parent, const P3D& parentLocalSupportPos, const AxisAlignedBoundingBox& bBox) : Support(parent) {
		this->parentLocalSupportPos = parentLocalSupportPos;
		this->bBox = bBox;

		colShape = btBoxShape(bulletVector(this->bBox.halfSides()));
		nonResizeableDirections.push_back(V3D(0, 1, 0));

		supportVolCostObj = new SupportVolumeCostObject(this);
	}
	virtual ~RigidlyAttachedSupport() {
		if (supportVolCostObj)
			delete supportVolCostObj;
	};

	virtual P3D getStartPositionInParentLocalCoordinates() {
		return parentLocalSupportPos;
	}

	virtual P3D getPosition() {
		setPosition(parent->getWorldCoordinates(getStartPositionInParentLocalCoordinates()));
		return BaseObject::getPosition();
	}

	virtual Quaternion getOrientation() {
		setOrientation(parent->getOrientation());
		return BaseObject::getOrientation();
	}

	virtual void drawObjectGeometry(GLShaderMaterial* material = NULL) {
		GLShaderMaterial tmpMat;
		GLShaderMaterial *matToUse = (material) ? material : &tmpMat;

		if (material == NULL) {
			//tmpMat.r = 0.55; tmpMat.g = 0.28; tmpMat.b = 0.07; tmpMat.a = 0.7;
			tmpMat.r = 0.55; tmpMat.g = 0.55; tmpMat.b = 0.55; tmpMat.a = 0.7;
		}

		glEnable(GL_LIGHTING);
		if (globalCoordinatesWallExtensionMesh.getVertexCount() <= 0) {
			matToUse->apply();
			drawBox(bBox.bmin(), bBox.bmax());
			matToUse->end();
		}
		else {
			globalCoordinatesWallExtensionMesh.setMaterial(*matToUse);
			globalCoordinatesWallExtensionMesh.drawMesh();
		}
		glDisable(GL_LIGHTING);
	}

	virtual void draw();
	virtual void drawSilhouette(double scale, double r, double g, double b);

	virtual void getSilhouetteScalingDimensions(double &dimX, double &dimY, double &dimZ) {
		if (globalCoordinatesWallExtensionMesh.getVertexCount() > 0) {
			auto box = globalCoordinatesWallExtensionMesh.calBoundingBox();
			dimX = (box.bmax()[0] - box.bmin()[0])/2;
			dimY = (box.bmax()[1] - box.bmin()[1])/2;
			dimZ = (box.bmax()[2] - box.bmin()[2])/2;

			geoMeshCenter = globalCoordinatesWallExtensionMesh.getCenterOfMass();
		}
		else {
			dimX = (bBox.bmax()[0] - bBox.bmin()[0]) / 2;
			dimY = (bBox.bmax()[1] - bBox.bmin()[1]) / 2;
			dimZ = (bBox.bmax()[2] - bBox.bmin()[2]) / 2;
		}
	}

	virtual void addBulletCollisionObjectsToList(DynamicArray<btCollisionObject*>& bulletCollisionObjs) {
		if (globalCoordinatesWallExtensionMesh.getVertexCount() > 0) {
			bulletCollisionObjs.push_back(getBulletCollisionObjectFromWorldCoordsShape(this, &btSupportHullShape, &colObject));
		}
		else {
			bulletCollisionObjs.push_back(getBulletCollisionObjectFromLocalCoordsShape(this, &colShape, &colObject));
		}
	}

	// update support size to snap to the closest wall of the chassis
	virtual void updateSupportSize() {

		globalCoordinatesWallExtensionMesh.clear();
		btSupportHullShape = btConvexHullShape();

		// if the support is penetrating the chassis wall(d < = 0), do nothing.
		if (closestWallPlane.getSignedDistanceToPoint(this->getPosition()) >0) {
			// we now check this while finding closest wall since so that we can correct/account for it
			//if (validateResizeDirection(-closestWallPlane.n)) {

				// new points
				DynamicArray<P3D> points;

				//addPointAndProjectionGlobalCoordsToList(points, this->bBox.bmax());
				//addPointAndProjectionGlobalCoordsToList(points, this->bBox.bmin());
				//addPointAndProjectionGlobalCoordsToList(points, P3D(this->bBox.bmax()[0], this->bBox.bmin()[1], this->bBox.bmin()[2]));
				//addPointAndProjectionGlobalCoordsToList(points, P3D(this->bBox.bmin()[0], this->bBox.bmax()[1], this->bBox.bmin()[2]));
				//addPointAndProjectionGlobalCoordsToList(points, P3D(this->bBox.bmin()[0], this->bBox.bmax()[1], this->bBox.bmax()[2]));
				//addPointAndProjectionGlobalCoordsToList(points, P3D(this->bBox.bmin()[0], this->bBox.bmin()[1], this->bBox.bmax()[2]));
				//addPointAndProjectionGlobalCoordsToList(points, P3D(this->bBox.bmax()[0], this->bBox.bmin()[1], this->bBox.bmax()[2]));
				//addPointAndProjectionGlobalCoordsToList(points, P3D(this->bBox.bmax()[0], this->bBox.bmax()[1], this->bBox.bmin()[2]));

				addPointAndWallPointGlobalCoordsToList(points, this->bBox.bmax(),closestWallPlane);
				addPointAndWallPointGlobalCoordsToList(points, this->bBox.bmin(), closestWallPlane);
				addPointAndWallPointGlobalCoordsToList(points, P3D(this->bBox.bmax()[0], this->bBox.bmin()[1], this->bBox.bmin()[2]), closestWallPlane);
				addPointAndWallPointGlobalCoordsToList(points, P3D(this->bBox.bmin()[0], this->bBox.bmax()[1], this->bBox.bmin()[2]), closestWallPlane);
				addPointAndWallPointGlobalCoordsToList(points, P3D(this->bBox.bmin()[0], this->bBox.bmax()[1], this->bBox.bmax()[2]), closestWallPlane);
				addPointAndWallPointGlobalCoordsToList(points, P3D(this->bBox.bmin()[0], this->bBox.bmin()[1], this->bBox.bmax()[2]), closestWallPlane);
				addPointAndWallPointGlobalCoordsToList(points, P3D(this->bBox.bmax()[0], this->bBox.bmin()[1], this->bBox.bmax()[2]), closestWallPlane);
				addPointAndWallPointGlobalCoordsToList(points, P3D(this->bBox.bmax()[0], this->bBox.bmax()[1], this->bBox.bmin()[2]), closestWallPlane);

				//for (size_t i = 0; i < points.size(); i++)
				//{
				//	//Logger::logPrint("supportPoint:%d -- (%10.10lf, %10.10lf, %10.10lf)\n", i, points[i][0], points[i][1],
				//		//points[i][2]);
				//}
				//Logger::logPrint("distance from wall:%10.10lf\n", closestWallPlane.getSignedDistanceToPoint(this->getPosition()));
				//Logger::logPrint("- -- - - - --------------------------- - - - -- \n");

				// update extension mesh
				ConvexHull3D::computeConvexHullFromSetOfPoints(points, &globalCoordinatesWallExtensionMesh);
				// and its collision shape..
				for (int i = 0; i < globalCoordinatesWallExtensionMesh.getVertexCount(); i++) {
					btSupportHullShape.addPoint(bulletVector(globalCoordinatesWallExtensionMesh.getVertex(i)));
				}

			//}
		}
	}

	void addPointAndProjectionGlobalCoordsToList(DynamicArray<P3D>& list, const P3D& point) {
		list.push_back(this->getWorldCoordinates(point));
		list.push_back(closestWallPlane.getProjectionOf(this->getWorldCoordinates(point)));

		//list.push_back(point);
		//list.push_back(this->getLocalCoordinates(closestWallPlane.getProjectionOf(this->getWorldCoordinates(point))));
	}

	void addPointAndWallPointGlobalCoordsToList(DynamicArray<P3D>& list, const P3D& point, Plane wall) {
		list.push_back(this->getWorldCoordinates(point));

		V3D dir = V3D(0, 0, 0);
		if (wall.n.angleWith(V3D(1, 0, 0)) <= PI / 4)
			dir = V3D(-1, 0, 0);
		else if (wall.n.angleWith(V3D(-1, 0, 0)) <= PI / 4)
			dir = V3D(1, 0, 0);
		if (wall.n.angleWith(V3D(0, 1, 0)) <= PI / 4)
			dir = V3D(0, -1, 0);
		else if (wall.n.angleWith(V3D(0, -1, 0)) <= PI / 4)
			dir = V3D(0, 1, 0);
		if (wall.n.angleWith(V3D(0, 0, 1)) <= PI / 4)
			dir = V3D(0, 0, -1);
		else if (wall.n.angleWith(V3D(0, 0, -1)) <= PI / 4)
			dir = V3D(0, 0, 1);

		// ray from support towards wall plane..
		Ray boxRay = Ray(this->getWorldCoordinates(point), dir);

		//plane ray intersection:
		double a, b, c, d;
		wall.getCartesianEquationCoefficients(a, b, c, d);
		double t = -(boxRay.origin.dot(wall.n) + d) / (dir.dot(wall.n));
		P3D wallPoint = boxRay.getPointAt(t);
		list.push_back(wallPoint);

	}
};
