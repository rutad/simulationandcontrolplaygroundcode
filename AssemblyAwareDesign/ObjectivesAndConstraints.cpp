#include "ObjectivesAndConstraints.h"
#include "BaseObject.h"
#include <OptimizationLib/SoftUnilateralConstraint.h>

/*
	Used to ensure the state of an object can be explicitly controlled, for example when it is dragged with the mouse by a user
*/
void TargetStateObjective::setTargets(const P3D& targetPos, const Quaternion& targetRot) {
	this->targetPosition = targetPos;
	this->invTargetOrientation = targetRot.getInverse();
}

double TargetStateObjective::computeValue(const dVector& p) {
	//NOTE: we assume that the parameters have already been written to the assembly...

	V3D v1(object->getPosition(), targetPosition);
	V3D v2 = (object->getOrientation() * invTargetOrientation).v;

	description = "TargetStateObjective " + object->name;
	return weightPos*v1.length2() + weightOrientation*v2.length2();
}
