#pragma once

#include <BulletCollision/btBulletCollisionCommon.h>
#include "BaseObject.h"

btCollisionObject* getBulletCollisionObjectFromLocalCoordsShape(BaseObject* p, btCollisionShape* cShape, btCollisionObject* colObject, const V3D& geometryCenterLocalOffset = V3D(0, 0, 0));
btCollisionObject* getBulletCollisionObjectFromWorldCoordsShape(BaseObject* p, btCollisionShape* cShape, btCollisionObject* colObject);
