#pragma once

#include <OptimizationLib/SoftConstraints.h>
#include <OptimizationLib/ObjectiveFunction.h>

#include <OptimizationLib/GradientDescentFunctionMinimizer.h>
#include <OptimizationLib/NewtonFunctionMinimizer.h>
#include <OptimizationLib/BFGSFunctionMinimizer.h>
#include <OptimizationLib/CMAFunctionMinimizer.h>

class Assembly;

class ConstraintsCollection : public ObjectiveFunction {
public:
	DynamicArray<ObjectiveFunction*> softConstraints;

	ConstraintsCollection(Assembly* assembly) { this->assembly = assembly; }
	virtual ~ConstraintsCollection(void) {
	}

	//regularizer looks like: r/2 * (p-p0)'*(p-p0). This function can update p0 if desired, given the current value of s.
	void updateRegularizingSolutionTo(const dVector &currentP);
	virtual double computeValue(const dVector& p);

	//this method gets called whenever a new best solution to the objective function is found
	virtual void setCurrentBestSolution(const dVector& p);

	bool printDebugInfo = false;
	bool printProgress = false;

private:

	dVector m_p0;
	dVector tmpVec;
	double regularizer = 0.01;
	Assembly* assembly;
};

/*
The Constraint Manager implements various strategies for dealing with collections of constraints:
local projections + global solve, BFGS on soft constraints, etc...
*/
class ConstraintsManager {
public:
	ConstraintsManager(Assembly* assembly);
	virtual ~ConstraintsManager(void);

	//solves over all the parameters exposed by an assembly in order to satisfy the various geometric, collision, structural and assembalability constraints
	double fixConstraints(bool& converged, int maxIterations = 1);

	// for DEBUG purposes.
	void printNonZeroConstraints();
public:
	Assembly* assembly;
	ConstraintsCollection* constraintsCollection;
	dVector params;
	BFGSFunctionMinimizer minimizer;
	//GradientDescentFunctionMinimizer minimizer;
};