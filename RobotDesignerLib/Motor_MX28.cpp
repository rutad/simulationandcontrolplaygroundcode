#include "Motor_MX28.h"
#include <GUILib/GLContentManager.h>
#include <MathLib/Quaternion.h>

Motor_MX28::Motor_MX28(RobotBodyPart* parent, RobotBodyPart* child, const P3D& worldPos, const V3D& jointAxis) : RobotJointFeature(parent, child, worldPos, jointAxis) {
	motorMeshParent = GLContentManager::getGLMesh("../data/RobotDesigner/motorMeshes/MX-28_parent.obj"); motorMeshParent->getMaterial().setColor(0.15, 0.15, 0.15, 1.0);
	motorMeshChild = GLContentManager::getGLMesh("../data/RobotDesigner/motorMeshes/MX-28_child.obj"); motorMeshChild->getMaterial().setColor(0.7, 0.7, 0.7, 1.0);
	
	parentSupportMesh = GLContentManager::getGLMesh("../data/RobotDesigner/motorMeshes/MX-28-parentSupport.obj"); parentSupportMesh->getMaterial().setColor(0.9, 0.9, 0.9, 1.0);
	childSupportMesh = GLContentManager::getGLMesh("../data/RobotDesigner/motorMeshes/MX-28-childSupport.obj"); childSupportMesh->getMaterial().setColor(0.9, 0.9, 0.9, 1.0);

	this->name = "Dynamixel MX28";

}

Motor_MX28::~Motor_MX28() {

}

/*
void Motor_MX28::getCompleteListOfPossibleAttachmentPoints(bool isStart, vector<AttachmentPoint>& attachmentPoints){
	attachmentPoints.clear();
	if (!isStart){//the start of the structure == child attachment of the motor
		double x = 0.018;
		double y = -0.043;
		double z = 0.02;
		attachmentPoints.push_back(AttachmentPoint(getWorldLocationForPointOnMotorParentSide(P3D(-x, y, -z)), getWorldLocationForVectorOnMotorParentSide(V3D(0, 0, -1).unit())));
		attachmentPoints.push_back(AttachmentPoint(getWorldLocationForPointOnMotorParentSide(P3D(x, y, -z)), getWorldLocationForVectorOnMotorParentSide(V3D(0, 0, -1).unit())));
		attachmentPoints.push_back(AttachmentPoint(getWorldLocationForPointOnMotorParentSide(P3D(x, y, z)), getWorldLocationForVectorOnMotorParentSide(V3D(0, 0, 1).unit())));
		attachmentPoints.push_back(AttachmentPoint(getWorldLocationForPointOnMotorParentSide(P3D(-x, y, z)), getWorldLocationForVectorOnMotorParentSide(V3D(0, 0, 1).unit())));
		attachmentPoints.push_back(AttachmentPoint(getWorldLocationForPointOnMotorParentSide(P3D(-x, y, -z)), getWorldLocationForVectorOnMotorParentSide(V3D(0, -1, 0).unit())));
		attachmentPoints.push_back(AttachmentPoint(getWorldLocationForPointOnMotorParentSide(P3D(x, y, -z)), getWorldLocationForVectorOnMotorParentSide(V3D(0, -1, 0).unit())));
		attachmentPoints.push_back(AttachmentPoint(getWorldLocationForPointOnMotorParentSide(P3D(x, y, z)), getWorldLocationForVectorOnMotorParentSide(V3D(0, -1, 0).unit())));
		attachmentPoints.push_back(AttachmentPoint(getWorldLocationForPointOnMotorParentSide(P3D(-x, y, z)), getWorldLocationForVectorOnMotorParentSide(V3D(0, -1, 0).unit())));
		attachmentPoints.push_back(AttachmentPoint(getWorldLocationForPointOnMotorParentSide(P3D(-x, y, -z)), getWorldLocationForVectorOnMotorParentSide(V3D(-1, 0, 0).unit())));
		attachmentPoints.push_back(AttachmentPoint(getWorldLocationForPointOnMotorParentSide(P3D(x, y, -z)), getWorldLocationForVectorOnMotorParentSide(V3D(1, 0, 0).unit())));
		attachmentPoints.push_back(AttachmentPoint(getWorldLocationForPointOnMotorParentSide(P3D(x, y, z)), getWorldLocationForVectorOnMotorParentSide(V3D(1, 0, 0).unit())));
		attachmentPoints.push_back(AttachmentPoint(getWorldLocationForPointOnMotorParentSide(P3D(-x, y, z)), getWorldLocationForVectorOnMotorParentSide(V3D(-1, 0, 0).unit())));

		y = -0.0185;
		attachmentPoints.push_back(AttachmentPoint(getWorldLocationForPointOnMotorParentSide(P3D(-x, y, -z)), getWorldLocationForVectorOnMotorParentSide(V3D(-1, 0, 0).unit())));
		attachmentPoints.push_back(AttachmentPoint(getWorldLocationForPointOnMotorParentSide(P3D(x, y, -z)), getWorldLocationForVectorOnMotorParentSide(V3D(1, 0, 0).unit())));
		attachmentPoints.push_back(AttachmentPoint(getWorldLocationForPointOnMotorParentSide(P3D(x, y, z)), getWorldLocationForVectorOnMotorParentSide(V3D(1, 0, 0).unit())));
		attachmentPoints.push_back(AttachmentPoint(getWorldLocationForPointOnMotorParentSide(P3D(-x, y, z)), getWorldLocationForVectorOnMotorParentSide(V3D(-1, 0, 0).unit())));

		attachmentPoints.push_back(AttachmentPoint(getWorldLocationForPointOnMotorParentSide(P3D(-x, y, -z)), getWorldLocationForVectorOnMotorParentSide(V3D(0, 0, -1).unit())));
		attachmentPoints.push_back(AttachmentPoint(getWorldLocationForPointOnMotorParentSide(P3D(x, y, -z)), getWorldLocationForVectorOnMotorParentSide(V3D(0, 0, -1).unit())));
		attachmentPoints.push_back(AttachmentPoint(getWorldLocationForPointOnMotorParentSide(P3D(x, y, z)), getWorldLocationForVectorOnMotorParentSide(V3D(0, 0, 1).unit())));
		attachmentPoints.push_back(AttachmentPoint(getWorldLocationForPointOnMotorParentSide(P3D(-x, y, z)), getWorldLocationForVectorOnMotorParentSide(V3D(0, 0, 1).unit())));

	}else{
		double x = 0.0178;
		double y = 0.0254;
		double z = 0.025;

		attachmentPoints.push_back(AttachmentPoint(getWorldLocationForPointOnMotorChildSide(P3D(-x, y, -z)), getWorldLocationForVectorOnMotorChildSide(V3D(0, 1, 0))));
		attachmentPoints.push_back(AttachmentPoint(getWorldLocationForPointOnMotorChildSide(P3D(x, y, -z)), getWorldLocationForVectorOnMotorChildSide(V3D(0, 1, 0))));
		attachmentPoints.push_back(AttachmentPoint(getWorldLocationForPointOnMotorChildSide(P3D(x, y, z)), getWorldLocationForVectorOnMotorChildSide(V3D(0, 1, 0))));
		attachmentPoints.push_back(AttachmentPoint(getWorldLocationForPointOnMotorChildSide(P3D(-x, y, z)), getWorldLocationForVectorOnMotorChildSide(V3D(0, 1, 0))));

		attachmentPoints.push_back(AttachmentPoint(getWorldLocationForPointOnMotorChildSide(P3D(-x, y, -z)), getWorldLocationForVectorOnMotorChildSide(V3D(-1, 0, 0))));
		attachmentPoints.push_back(AttachmentPoint(getWorldLocationForPointOnMotorChildSide(P3D(x, y, -z)), getWorldLocationForVectorOnMotorChildSide(V3D(1, 0, 0))));
		attachmentPoints.push_back(AttachmentPoint(getWorldLocationForPointOnMotorChildSide(P3D(x, y, z)), getWorldLocationForVectorOnMotorChildSide(V3D(1, 0, 0))));
		attachmentPoints.push_back(AttachmentPoint(getWorldLocationForPointOnMotorChildSide(P3D(-x, y, z)), getWorldLocationForVectorOnMotorChildSide(V3D(-1, 0, 0))));

		attachmentPoints.push_back(AttachmentPoint(getWorldLocationForPointOnMotorChildSide(P3D(-x, y, -z)), getWorldLocationForVectorOnMotorChildSide(V3D(0, 0, -1))));
		attachmentPoints.push_back(AttachmentPoint(getWorldLocationForPointOnMotorChildSide(P3D(x, y, -z)), getWorldLocationForVectorOnMotorChildSide(V3D(0, 0, -1))));
		attachmentPoints.push_back(AttachmentPoint(getWorldLocationForPointOnMotorChildSide(P3D(x, y, z)), getWorldLocationForVectorOnMotorChildSide(V3D(0, 0, 1))));
		attachmentPoints.push_back(AttachmentPoint(getWorldLocationForPointOnMotorChildSide(P3D(-x, y, z)), getWorldLocationForVectorOnMotorChildSide(V3D(0, 0, 1))));

		x = -0.0132;
		y = 0.008;
		z = 0.025;

		attachmentPoints.push_back(AttachmentPoint(getWorldLocationForPointOnMotorChildSide(P3D(-x, y, -z)), getWorldLocationForVectorOnMotorChildSide(V3D(0, 0, -1))));
		attachmentPoints.push_back(AttachmentPoint(getWorldLocationForPointOnMotorChildSide(P3D(x, y, -z)), getWorldLocationForVectorOnMotorChildSide(V3D(0, 0, -1))));
		attachmentPoints.push_back(AttachmentPoint(getWorldLocationForPointOnMotorChildSide(P3D(x, y, z)), getWorldLocationForVectorOnMotorChildSide(V3D(0, 0, 1))));
		attachmentPoints.push_back(AttachmentPoint(getWorldLocationForPointOnMotorChildSide(P3D(-x, y, z)), getWorldLocationForVectorOnMotorChildSide(V3D(0, 0, 1))));
		
		attachmentPoints.push_back(AttachmentPoint(getWorldLocationForPointOnMotorChildSide(P3D(-x, y, -z)), getWorldLocationForVectorOnMotorChildSide(V3D(1, 0, 0))));
		attachmentPoints.push_back(AttachmentPoint(getWorldLocationForPointOnMotorChildSide(P3D(x, y, -z)), getWorldLocationForVectorOnMotorChildSide(V3D(-1, 0, 0))));
		attachmentPoints.push_back(AttachmentPoint(getWorldLocationForPointOnMotorChildSide(P3D(x, y, z)), getWorldLocationForVectorOnMotorChildSide(V3D(-1, 0, 0))));
		attachmentPoints.push_back(AttachmentPoint(getWorldLocationForPointOnMotorChildSide(P3D(-x, y, z)), getWorldLocationForVectorOnMotorChildSide(V3D(1, 0, 0))));

	}
}
*/