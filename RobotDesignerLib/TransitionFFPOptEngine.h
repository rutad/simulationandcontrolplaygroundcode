#pragma once
#include "TransitionFFPOptPlan.h"
#include "TransitionFFPObjective.h"

class TransitionFFPOptEngine
{
public:
	TransitionFFPOptPlan* plan = NULL;
	TransitionFFPObjective* energyFunction = NULL;

public:
	TransitionFFPOptEngine(FootFallPattern* transFFP, FootFallPattern* startFFP, FootFallPattern* endFFP,
		int transStartIndex, int transEndIndex);
	~TransitionFFPOptEngine();

	double optimizePlan(int maxIterNum);

	void writeSolutionToFFP();
};

