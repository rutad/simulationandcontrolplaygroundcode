#pragma once

#include <MathLib/V3D.h>
#include <MathLib/P3D.h>
#include <vector>
#include "RobotBodyFeature.h"

class Motor_MX28 : public RobotJointFeature {
public:
	Motor_MX28(RobotBodyPart* parent, RobotBodyPart* child, const P3D& worldPos, const V3D& jointAxis = V3D(0, 0, 1));
	~Motor_MX28(void);
	
//	virtual void getCompleteListOfPossibleAttachmentPoints(bool isStart, vector<AttachmentPoint>& attachmentPoints);


};

