#pragma once

#include <MathLib/V3D.h>
#include <MathLib/P3D.h>
#include <vector>
#include "RobotBodyFeature.h"

class Motor_X5_M : public RobotJointFeature {
public:
	Motor_X5_M(RobotBodyPart* parent, RobotBodyPart* child, const P3D& worldPos, const V3D& jointAxis = V3D(0, 0, 1));
	~Motor_X5_M(void);
	
	virtual void getListOfPossibleAttachmentPoints(RobotBodyPart* rbp, DynamicArray<AttachmentPoint>& attachmentPoints);
};

