#pragma once
#include <MathLib\V3D.h>
#include <vector>

#include <MathLib/mathLib.h>
#include <MathLib/Quaternion.h>
#include "StructureFeature.h"
#include <GUILib/GLMesh.h>

using namespace std;
/*
class EndEffectorFeature : public StructureFeature{
private:
	DynamicArray<P3D> eeCP;
public:
	EndEffectorFeature(void);
    EndEffectorFeature(P3D _position, RobotBodyPart* _parent) :StructureFeature(RobotNodeType::FEATURE, _position, _parent) {};
	virtual ~EndEffectorFeature(void){}

	void getCompleteListOfPossibleAttachmentPoints(bool isStart, vector<AttachmentPoint>& attachmentPoints);

	void draw();

	void addEEContactPoint(const P3D& localEECPCoords){
		eeCP.push_back(localEECPCoords);
	}

	void setEEContactPointPos(int contactPointIndex, const P3D& localEECPCoords)
	{
		eeCP[contactPointIndex] = localEECPCoords;
	}

    string generateObj();
    //void addAttachmentPoint(P3D attachmentPoint);
};
*/