#pragma once
#include "MotionGraph.h"

class RobotPath
{
public:
	vector<P3D> pathPts;

public:
	void reset();
	void saveToFile(FILE* fp);
	void loadFromFile(FILE* fp);
};


class PathFollower
{
public:
	Robot* robot = NULL;
	MotionGraph* motionGraph = NULL;
	RobotPath* robotPath = NULL;

	MotionGraphNode* currentNode = NULL;

	int targetPIndex = 0;

	double targetRadius = 0.8;

public:
	PathFollower(Robot* robot, MotionGraph* motionGraph, RobotPath* robotPath);
	~PathFollower();

	void initializeRobot();
	MotionGraphNode* calBestMotionToFollow();
	bool updateTargetPoint();

protected:
	 double calCostForCurrentNode(MotionGraphNode* node);

};

