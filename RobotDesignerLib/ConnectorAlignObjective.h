#pragma once
#include <OptimizationLib/ObjectiveFunction.h>
#include "MotorOptPlan.h"

class ConnectorAlignObjective : public ObjectiveFunction
{
public:
	MotorOptPlan* plan;

public:
	ConnectorAlignObjective(MotorOptPlan* plan, const std::string& objectiveDescription, double weight);
	~ConnectorAlignObjective();

	virtual double computeValue(const dVector& p);
};

