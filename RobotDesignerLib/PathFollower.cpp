#include "PathFollower.h"
#include <MathLib/Segment.h>


void RobotPath::reset() {
	pathPts.clear();
}

void RobotPath::saveToFile(FILE* fp)
{
	fprintf(fp, "RobotPath\n\n");

	for (auto& p : pathPts)
	{
		fprintf(fp, "Point %lf %lf %lf\n", p[0], p[1], p[2]);
	}

	fprintf(fp, "EndRobotPath\n\n");
}

void RobotPath::loadFromFile(FILE* fp)
{
	reset();

	char buffer[200];
	char keyword[50];

	while (!feof(fp)) {
		//get a line from the file...
		readValidLine(buffer, fp, 200);
		if (strlen(buffer) > 195)
			throwError("The input file contains a line that is longer than ~200 characters - not allowed");
		char *line = lTrim(buffer);
		if (strlen(line) == 0) continue;
		sscanf(line, "%s", keyword);

		if (strcmp(keyword, "Point") == 0)
		{
			P3D p;
			int num = sscanf(line + strlen(keyword), "%lf %lf %lf", &p[0], &p[1], &p[2]);
			pathPts.push_back(p);
		}
		else if (strcmp(keyword, "EndRobotPath") == 0)
		{
			break;
		}
	}
}

PathFollower::PathFollower(Robot* robot, MotionGraph* motionGraph, RobotPath* robotPath)
{
	this->robot = robot;
	this->motionGraph = motionGraph;
	this->robotPath = robotPath;
}

PathFollower::~PathFollower()
{

}

void PathFollower::initializeRobot()
{
	if (robotPath->pathPts.size() < 2)
	{
		Logger::print("Robot path incomplete!\n");
		return;
	}

	ReducedRobotState currentState(robot);
	P3D pos = currentState.getPosition();
	Quaternion orientation = currentState.getOrientation();
	currentState.setPosition(P3D(robotPath->pathPts[0][0], pos[1], robotPath->pathPts[0][2]));

	V3D pathDir = robotPath->pathPts[1] - robotPath->pathPts[0];
	Quaternion orientWithoutHeading = computeHeading(orientation, V3D(0, 1, 0)).getInverse() * orientation;
	Quaternion pathHeading = getRotationQuaternion(atan2(pathDir[0], pathDir[2]), V3D(0, 1, 0));
	currentState.setOrientation(pathHeading * orientWithoutHeading);

	robot->setState(&currentState);

	targetPIndex = 1;
	currentNode = NULL;
}

MotionGraphNode* PathFollower::calBestMotionToFollow()
{	
	double minCost = DBL_MAX;
	MotionGraphNode* bestNode = NULL;

	int index = 0;
	for (auto node : motionGraph->nodeSet)
	{
		if (!(currentNode == NULL || node == currentNode || motionGraph->findEdge(currentNode, node))) continue;

		double cost = calCostForCurrentNode(node);
		// Logger::print("node %d, cost: %lf\n", index, cost);
		if (cost < minCost)
		{
			minCost = cost;
			bestNode = node;
		}
		index++;
	}

	currentNode = bestNode;
	return bestNode;
}

bool PathFollower::updateTargetPoint()
{
	if (targetPIndex <= 0) {
		initializeRobot();
	}

	P3D currentPos = robot->getRoot()->state.position;
	P3D targetPos = robotPath->pathPts[targetPIndex];
	P3D lastTargetPos = robotPath->pathPts[targetPIndex-1];

	if ((targetPos - currentPos).dot(targetPos - lastTargetPos) < 0)
	{
		targetPIndex++;
	}

	if (targetPIndex < (int)robotPath->pathPts.size() - 1
		&& (targetPos - currentPos).norm() < targetRadius)
	{
		targetPIndex++;
	}

	// Logger::print("targetPIndex %d\n", targetPIndex);

	return targetPIndex < (int)robotPath->pathPts.size();
}

double PathFollower::calCostForCurrentNode(MotionGraphNode* node)
{
	const double distWeight = 1;
	const double angleWeight = 30;

	double cost = 0;

	P3D currentPos = robot->getRoot()->state.position;
	P3D targetPos = robotPath->pathPts[targetPIndex];
	double dist = (targetPos - currentPos).norm();

	Quaternion orientation = robot->getRoot()->state.orientation;
	Quaternion heading = computeHeading(orientation, V3D(0, 1, 0));
	V3D mpDist = heading.rotate(node->motionPlan->desDistanceToTravel);
	Segment seg(currentPos, currentPos + mpDist);
	double newDist = (seg.getClosestPointTo(targetPos) - targetPos).norm();

	// *************************** distance ***************************
	cost += newDist * distWeight;


	// *************************** heading alignment ***************************
	if (dist > targetRadius)
	{
		V3D newTargetVec = (targetPos - currentPos - mpDist).normalized();
		V3D newHeadingVec = (getRotationQuaternion(node->motionPlan->desTurningAngle, V3D(0, 1, 0)) * heading).rotate(V3D(0, 0, 1));
		double newDot = newHeadingVec.dot(newTargetVec);

		cost += -newDot * angleWeight;
	}
	
	if (node == currentNode)
	{
		cost += -2.0;
	}

	return cost;
}
