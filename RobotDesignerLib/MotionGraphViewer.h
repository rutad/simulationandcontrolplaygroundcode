#pragma once

#include <GUILib/GLWindow2D.h>
#include "MotionGraph.h"


class MotionGraphViewer : public GLWindow2D {

public:
	MotionGraph* motionGraph = NULL;
	
	MotionGraphNode* highlightedNode = NULL;
	MotionGraphNode* selectedNode = NULL;
	MotionGraphNode* secondSelectedNode = NULL;
	MotionGraphEdge* highlightedEdge = NULL;
	MotionGraphEdge* selectedEdge = NULL;
	MotionGraphEdge* transitionEdge = NULL;
	bool startTransition = false;
public:
	MotionGraphViewer(int posX, int posY, int sizeX, int sizeY);

	virtual ~MotionGraphViewer(void);

	virtual void draw();

	//triggered when mouse buttons are pressed
	virtual bool onMouseButtonEvent(int button, int action, int mods, double xPos, double yPos);

	//triggered when mouse moves
	virtual bool onMouseMoveEvent(double xPos, double yPos);

	bool pickNode(double xPos, double yPos);
	bool pickEdge(double xPos, double yPos);

	void reset();
};


