#include "MOPTWindow.h"
#include "LocomotionEngineManagerGRF.h"
#include "LocomotionEngineManagerIP.h"
#include "LocomotionEngineManagerTransition.h"



MOPTWindow::MOPTWindow(int x, int y, int w, int h, GLApplication* glApp) : GLWindow3D(x, y, w, h)
{
	this->glApp = glApp;
	mainMenuBar = glApp->mainMenuBar;

	ffpViewer = new FootFallPatternViewer(x, (int)(h * 3.0 / 4), (int)(w), (int)(h / 4.0));
	start_ffpViewer = new FootFallPatternViewer(x, y, (int)(w / 2), (int)(h / 4.0));
	end_ffpViewer = new FootFallPatternViewer(x + w / 2, y, (int)(w / 2), (int)(h / 4.0));
	ffpViewer->ffp = &footFallPattern;
	start_ffpViewer->ffp = &footFallPatternStart;
	end_ffpViewer->ffp = &footFallPatternEnd;
	ffpViewer->cursorMovable = false;
	start_ffpViewer->ffpMovable = false;
	end_ffpViewer->ffpMovable = false;

	anmParams.f = 0;
	anmParams.animationCycle = 0;
	anmParams.drawOrientation = false;
	anmParams.drawContactForces = false;
	anmParams.drawRobotPose = true;
	anmParams.drawPlanDetails = true;

	TwAddVarRW(mainMenuBar, "DrawPlanDetails", TW_TYPE_BOOLCPP, &anmParams.drawPlanDetails, "group='MOPTVis'");
	TwAddVarRW(mainMenuBar, "MotionPlanInitDuration", TW_TYPE_DOUBLE, &motionPlanTime, "min=0 max=5.0 step=0.1 group='InitParams'");

	optimizeOption = GRF_OPT_V2;
	TwAddVarRW(mainMenuBar, "MOPTmodel", TwDefineEnumFromString("MOPT model", "..."), &optimizeOption, "group='Sim'");
	DynamicArray<std::string> optimizeOptionList;
	optimizeOptionList.push_back("\\GRFv1");
	optimizeOptionList.push_back("\\GRFv2");
	optimizeOptionList.push_back("\\GRFv3");
	optimizeOptionList.push_back("\\IPv1");
	optimizeOptionList.push_back("\\IPv2");

	generateMenuEnumFromFileList("MainMenuBar/MOPTmodel", optimizeOptionList);

	dynamic_cast<GLTrackingCamera*>(this->camera)->rotAboutRightAxis = 0.25;
	dynamic_cast<GLTrackingCamera*>(this->camera)->rotAboutUpAxis = 0.95;
	dynamic_cast<GLTrackingCamera*>(this->camera)->camDistance = -1.5;
}


MOPTWindow::~MOPTWindow()
{
	clear();
}

void MOPTWindow::clear()
{

}

void MOPTWindow::loadRobot(Robot* robot, ReducedRobotState* startState)
{
	clear();

	initialized = true;
	this->robot = robot;
	this->startState = *startState;

	int nLegs = robot->bFrame->limbs.size();
	nPoints = 3 * nLegs;

	// ******************* footfall patern *******************
	footFallPattern = FootFallPattern();

	int iMin = 0, iMax = nPoints / nLegs - 1;
	footFallPattern.strideSamplePoints = nPoints;
	for (int j = 0; j < nLegs; j++)
		footFallPattern.addStepPattern(robot->bFrame->limbs[j], iMin + j*nPoints / nLegs, iMax + j*nPoints / nLegs);

	footFallPattern.loadFromFile("..\\out\\tmpFFP.ffp");
}

void MOPTWindow::loadParametersForMotionPlan()
{
	if (locomotionManager && locomotionManager->motionPlan) {
		TwAddVarRW(mainMenuBar, "swing foot height", TW_TYPE_DOUBLE, &locomotionManager->motionPlan->swingFootHeight, "min=0 max=0.1 step=0.01 group='MOPTParams'");
		TwAddVarRW(mainMenuBar, "desired speed X", TW_TYPE_DOUBLE, &locomotionManager->motionPlan->desDistanceToTravel.x(), "min=-0.5 max=0.5 step=0.05 group='MOPTParams'");
		TwAddVarRW(mainMenuBar, "desired speed Z", TW_TYPE_DOUBLE, &locomotionManager->motionPlan->desDistanceToTravel.z(), "min=-0.5 max=0.5 step=0.05 group='MOPTParams'");
		TwAddVarRW(mainMenuBar, "turning angle", TW_TYPE_DOUBLE, &locomotionManager->motionPlan->desTurningAngle, "min=-0.5 max=0.5 step=0.05 group='MOPTParams'");
		//TwAddVarRW(mainMenuBar, "motionPlanDuration", TW_TYPE_DOUBLE, &locomotionManager->motionPlan->motionPlanDuration, "min=0.1 max=10 step=0.1 group='MOPTParams'");
		//TwAddVarRW(mainMenuBar, "min vertical GRF", TW_TYPE_DOUBLE, &locomotionManager->motionPlan->verticalGRFLowerBoundVal, "min=-100 max=100 step=0.01 group='MOPTParams'");
		//TwAddVarRW(mainMenuBar, "GRF Epsilon", TW_TYPE_DOUBLE, &locomotionManager->motionPlan->GRFEpsilon, "min=-100 max=100 step=0.01 group='MOPTParams'");

		//TwAddVarRW(mainMenuBar, "optimizeCOMParams", TW_TYPE_BOOLCPP, &locomotionManager->motionPlan->optimizeCOMParams, " label='optimize COM params' group='MOPTParams'");
		//TwAddVarRW(mainMenuBar, "optimizeFeetParams", TW_TYPE_BOOLCPP, &locomotionManager->motionPlan->optimizeFeetParams, " label='optimize feet params' group='MOPTParams'");
		//TwAddVarRW(mainMenuBar, "useLBFGS", TW_TYPE_BOOLCPP, &locomotionManager->useBFGS, " label='use L-BFGS solver'");
		//TwAddVarRW(mainMenuBar, "optimizeRobotStates", TW_TYPE_BOOLCPP, &locomotionManager->motionPlan->optimizeRobotStates, " label='optimize robot states' group='MOPTParams'");
		//TwAddVarRW(mainMenuBar, "enforceGRFConstraints", TW_TYPE_BOOLCPP, &locomotionManager->motionPlan->enforceGRFConstraints, " label='enforce GRF constraints' group='MOPTParams'");
		//TwAddVarRW(mainMenuBar, "printDebugInfo", TW_TYPE_BOOLCPP, &locomotionManager->printDebugInfo, " label='printDebugInfo' group='MOPTParams'");
		//TwAddVarRW(mainMenuBar, "checkDerivatives", TW_TYPE_BOOLCPP, &locomotionManager->checkDerivatives, " label='checkDerivatives' group='MOPTParams'");
		TwAddVarRW(mainMenuBar, "regularizer", TW_TYPE_DOUBLE, &locomotionManager->locomotionEngine->energyFunction->regularizer, "label='regularizer value' group='MOPTParams'");
	}
}

void MOPTWindow::unloadParametersForMotionPlan()
{
	if (locomotionManager && locomotionManager->motionPlan) {
		TwRemoveVar(mainMenuBar, "swing foot height");
		TwRemoveVar(mainMenuBar, "desired speed X");
		TwRemoveVar(mainMenuBar, "desired speed Z");
		TwRemoveVar(mainMenuBar, "turning angle");
		//TwRemoveVar(mainMenuBar, "motionPlanDuration");
		//TwRemoveVar(mainMenuBar, "min vertical GRF");
		//TwRemoveVar(mainMenuBar, "GRF Epsilon");
		//TwRemoveVar(mainMenuBar, "checkDerivatives");
		TwRemoveVar(mainMenuBar, "regularizer");
	}
}

LocomotionEngineManager* MOPTWindow::initializeNewMP(bool doWarmStart)
{
	footFallPattern.writeToFile("..\\out\\tmpFFP.ffp");

	unloadParametersForMotionPlan();

	/* ----------- Reset the state of the robot ------------ */
	robot->setState(&startState);


	/* ---------- Set up the motion plan ---------- */
	switch (optimizeOption)
	{
	case GRF_OPT:
		locomotionManager = new LocomotionEngineManagerGRF(robot, &footFallPattern, nPoints + 1); break;
	case GRF_OPT_V2:
		locomotionManager = new LocomotionEngineManagerGRFv2(robot, &footFallPattern, nPoints + 1); break;
	case GRF_OPT_V3:
		locomotionManager = new LocomotionEngineManagerGRFv3(robot, &footFallPattern, nPoints + 1); break;
	case IP_OPT:
		locomotionManager = new LocomotionEngineManagerIP(robot, &footFallPattern, nPoints + 1); break;
	case IP_OPT_V2:
		locomotionManager = new LocomotionEngineManagerIPv2(robot, &footFallPattern, nPoints + 1); break;
	default:
		locomotionManager = new LocomotionEngineManagerGRFv2(robot, &footFallPattern, nPoints + 1); break;
	}
	
	locomotionManager->motionPlan->motionPlanDuration = motionPlanTime;

	loadParametersForMotionPlan();

	/* ------ run a few steps of the motion plan ------*/

	if (doWarmStart) {
	//	locomotionManager->printDebugInfo = false;
		locomotionManager->warmStartMOpt();
	//	locomotionManager->printDebugInfo = true;
	}

	locomotionManager->motionPlan->optimizeEndEffectorPositions =
	locomotionManager->motionPlan->optimizeCOMPositions =
	locomotionManager->motionPlan->optimizeCOMOrientations =
	locomotionManager->motionPlan->optimizeRobotStates =
	locomotionManager->motionPlan->optimizeContactForces = true;

	return locomotionManager;
}

LocomotionEngineManager* MOPTWindow::initializeNewTransitionMP()
{
	unloadParametersForMotionPlan();

	/* ----------- Reset the state of the robot ------------ */
	robot->setState(&startState);

	int startTIndex = (int)round(start_ffpViewer->cursorPosition * footFallPatternStart.strideSamplePoints);
	int endTIndex = (int)round(end_ffpViewer->cursorPosition * footFallPatternEnd.strideSamplePoints);

	/* ---------- Set up the motion plan ---------- */
	locomotionManager = new LocomotionEngineManagerTransition(robot, &footFallPattern, nPoints, locomotionManagerStart->motionPlan, locomotionManagerEnd->motionPlan, startTIndex, endTIndex);
	locomotionManager->motionPlan->motionPlanDuration = motionPlanTime;

	loadParametersForMotionPlan();

	/* ------ run a few steps of the motion plan ------*/

	locomotionManager->warmStartMOpt();

	locomotionManager->motionPlan->desDistanceToTravel = (locomotionManagerStart->motionPlan->desDistanceToTravel
		+ locomotionManagerEnd->motionPlan->desDistanceToTravel) * 0.5;

	return locomotionManager;
}

double MOPTWindow::runMOPTStep()
{
	double energyVal = locomotionManager->runMOPTStep();

	return energyVal;
}


void MOPTWindow::syncWithMotionGraphNode(MotionGraphNode* node)
{
	if (node)
	{
		locomotionManagerStart = NULL;
		locomotionManagerEnd = NULL;
		unloadParametersForMotionPlan();
		locomotionManager = node->manager;
		locomotionManager->motionPlan->syncFootFallPatternWithMotionPlan(footFallPattern);
		loadParametersForMotionPlan();
	}
}

void MOPTWindow::syncWithMotionGraphEdge(MotionGraphEdge* edge)
{
	if (edge)
	{
		unloadParametersForMotionPlan();
		locomotionManager = edge->manager;
		locomotionManagerStart = edge->source->manager;
		locomotionManagerEnd = edge->sink->manager;
		locomotionManagerStart->motionPlan->syncFootFallPatternWithMotionPlan(footFallPatternStart);
		locomotionManagerEnd->motionPlan->syncFootFallPatternWithMotionPlan(footFallPatternEnd);
		start_ffpViewer->cursorPosition = locomotionManager->motionPlan->transitionStartIndex / (double)footFallPatternStart.strideSamplePoints;
		end_ffpViewer->cursorPosition = locomotionManager->motionPlan->transitionEndIndex / (double)footFallPatternEnd.strideSamplePoints;
		locomotionManager->motionPlan->syncFootFallPatternWithMotionPlan(footFallPattern);
		loadParametersForMotionPlan();
	}
}

void MOPTWindow::prepareForTransitionMOPT(MotionGraphNode* startNode, MotionGraphNode* endNode)
{
	if (locomotionManager)
	{
		unloadParametersForMotionPlan();
		locomotionManager = NULL;
	}

	locomotionManagerStart = startNode->manager;
	locomotionManagerEnd = endNode->manager;
	locomotionManagerStart->motionPlan->syncFootFallPatternWithMotionPlan(footFallPatternStart);
	locomotionManagerEnd->motionPlan->syncFootFallPatternWithMotionPlan(footFallPatternEnd);
	createTransitionFFP();
}

void MOPTWindow::createTransitionFFP()
{
	int startTIndex = (int)round(start_ffpViewer->cursorPosition * footFallPatternStart.strideSamplePoints);
	int endTIndex = (int)round(end_ffpViewer->cursorPosition * footFallPatternEnd.strideSamplePoints);

	footFallPattern = FootFallPattern();
	for (uint i = 0; i < footFallPatternStart.stepPatterns.size(); i++)
	{
		StepPattern& SPStart = footFallPatternStart.stepPatterns[i];
		StepPattern& SPEnd = footFallPatternEnd.stepPatterns[i];
		int start = INT_MAX;
		int end = -INT_MAX;

		if (footFallPatternEnd.isInSwing(&SPEnd, endTIndex)) {
			start = min(start, -2);
			end = max(end, -2);
		}
		if (footFallPatternEnd.isInSwing(&SPEnd, endTIndex + 1)) {
			start = min(start, -1);
			end = max(end, -1);
		}
		if (footFallPatternStart.isInSwing(&SPStart, startTIndex)) {
			start = min(start, 0);
			end = max(end, 0);
		}
		if (footFallPatternStart.isInSwing(&SPStart, startTIndex + 1)) {
			start = min(start, 1);
			end = max(end, 1);
		}
		if (start < 0)
		{
			start += footFallPatternStart.strideSamplePoints;
			end += footFallPatternEnd.strideSamplePoints;
		}

		if (start == INT_MAX || end == -INT_MAX)
		{
			footFallPattern.addStepPattern(SPStart.limb, 3, 3 + footFallPattern.strideSamplePoints / footFallPatternStart.stepPatterns.size());
		}
		else {
			footFallPattern.addStepPattern(SPStart.limb, start, end);
		}
	}
}

void MOPTWindow::reset()
{
	unloadParametersForMotionPlan();
	locomotionManager = NULL;
	locomotionManagerStart = NULL;
	locomotionManagerEnd = NULL;
}

void MOPTWindow::setAnimationParams(double f, int animationCycle)
{
	anmParams.f = f;
	anmParams.animationCycle = animationCycle;

	ffpViewer->cursorPosition = f;
}

void MOPTWindow::saveToFile(FILE* fp)
{
	fprintf(fp, "MOPTWindow\n\n");
	fprintf(fp, "CursorPositions %lf %lf %lf\n", ffpViewer->cursorPosition, start_ffpViewer->cursorPosition, end_ffpViewer->cursorPosition);
	fprintf(fp, "FootFallPattern\n");
	footFallPattern.writeToFile(fp);
	fprintf(fp, "\n");

	fprintf(fp, "EndMOPTWindow\n\n");
}

void MOPTWindow::loadFromFile(FILE* fp)
{
	char buffer[200];
	char keyword[50];

	while (!feof(fp)) {
		//get a line from the file...
		readValidLine(buffer, fp, 200);
		if (strlen(buffer) > 195)
			throwError("The input file contains a line that is longer than ~200 characters - not allowed");
		char *line = lTrim(buffer);
		if (strlen(line) == 0) continue;
		sscanf(line, "%s", keyword);

		if (strcmp(keyword, "CursorPositions") == 0)
		{
			int num = sscanf(line + strlen(keyword), "%lf %lf %lf", &(ffpViewer->cursorPosition), &(start_ffpViewer->cursorPosition), &(end_ffpViewer->cursorPosition));
		}
		else if (strcmp(keyword, "FootFallPattern") == 0)
		{
			footFallPattern.loadFromFile(fp);
		}
		else if (strcmp(keyword, "EndMOPTWindow") == 0)
		{
			break;
		}
	}
}

void MOPTWindow::loadFFPFromFile(const char* fName)
{
	footFallPattern.loadFromFile(fName);
}

void MOPTWindow::drawScene() {

	glColor3d(1, 1, 1);
	glDisable(GL_LIGHTING);
	drawGround();
	glEnable(GL_LIGHTING);

	if (locomotionManager)
	{
		locomotionManager->drawMotionPlan(anmParams.f, anmParams.animationCycle, anmParams.drawRobotPose, anmParams.drawPlanDetails, anmParams.drawContactForces, anmParams.drawOrientation);
	}
}

void MOPTWindow::drawAuxiliarySceneInfo()
{
	glClear(GL_DEPTH_BUFFER_BIT);

	preDraw();
	ffpViewer->draw();
	if (locomotionManagerStart)
		start_ffpViewer->draw();
	if (locomotionManagerEnd)
		end_ffpViewer->draw();
	postDraw();

	glClear(GL_DEPTH_BUFFER_BIT);

}

bool MOPTWindow::onMouseMoveEvent(double xPos, double yPos)
{

	if (initialized) {
		if (ffpViewer->mouseIsWithinWindow(xPos, yPos) || ffpViewer->isDragging())
			if (ffpViewer->onMouseMoveEvent(xPos, yPos)) return true;

		if (locomotionManagerStart && locomotionManagerEnd)
		{
			double startOldCursor = start_ffpViewer->cursorPosition;
			double endOldCursor = end_ffpViewer->cursorPosition;
			if (start_ffpViewer->mouseIsWithinWindow(xPos, yPos) || start_ffpViewer->isDragging())
				if (start_ffpViewer->onMouseMoveEvent(xPos, yPos)) {
					if (startOldCursor != start_ffpViewer->cursorPosition) {
						createTransitionFFP();
					}
					return true;
				}
			if (end_ffpViewer->mouseIsWithinWindow(xPos, yPos) || end_ffpViewer->isDragging())
				if (end_ffpViewer->onMouseMoveEvent(xPos, yPos)) {
					if (endOldCursor != end_ffpViewer->cursorPosition) {
						createTransitionFFP();
					}
					return true;
				}
		}
	}

	return GLWindow3D::onMouseMoveEvent(xPos, yPos);
}

bool MOPTWindow::onMouseButtonEvent(int button, int action, int mods, double xPos, double yPos)
{
	if (initialized) {
		if (ffpViewer->mouseIsWithinWindow(xPos, yPos) || ffpViewer->isDragging())
			if (ffpViewer->onMouseButtonEvent(button, action, mods, xPos, yPos)) return true;

		if (locomotionManagerStart && locomotionManagerEnd) {
			double startOldCursor = start_ffpViewer->cursorPosition;
			double endOldCursor = end_ffpViewer->cursorPosition;
			if (start_ffpViewer->mouseIsWithinWindow(xPos, yPos) || start_ffpViewer->isDragging())
				if (start_ffpViewer->onMouseButtonEvent(button, action, mods, xPos, yPos)) {
					if (startOldCursor != start_ffpViewer->cursorPosition) {
						createTransitionFFP();
					}
					return true;
				}
			if (end_ffpViewer->mouseIsWithinWindow(xPos, yPos) || end_ffpViewer->isDragging())
				if (end_ffpViewer->onMouseButtonEvent(button, action, mods, xPos, yPos)) {
					if (endOldCursor != end_ffpViewer->cursorPosition) {
						createTransitionFFP();
					}
					return true;
				}
		}

	}

	return GLWindow3D::onMouseButtonEvent(button, action, mods, xPos, yPos);
}

void MOPTWindow::setViewportParameters(int posX, int posY, int sizeX, int sizeY)
{
	GLWindow3D::setViewportParameters(posX, posY, sizeX, sizeY);
	ffpViewer->setViewportParameters(posX, (int)(sizeY * 3.0 / 4), sizeX, (int)(sizeY / 4.0));
	start_ffpViewer->setViewportParameters(posX, posY, (int)(sizeX / 2), (int)(sizeY / 4.0));
	end_ffpViewer->setViewportParameters(posX + sizeX / 2, posY, (int)(sizeX / 2), (int)(sizeY / 4.0));

}

void MOPTWindow::setupLights() {

	GLfloat bright[] = { 0.8f, 0.8f, 0.8f, 1.0f };
	GLfloat mediumbright[] = { 0.3f, 0.3f, 0.3f, 1.0f };

	glLightfv(GL_LIGHT1, GL_DIFFUSE, bright);
	glLightfv(GL_LIGHT2, GL_DIFFUSE, mediumbright);
	glLightfv(GL_LIGHT3, GL_DIFFUSE, mediumbright);
	glLightfv(GL_LIGHT4, GL_DIFFUSE, mediumbright);


	GLfloat light0_position[] = { 0.0f, 10000.0f, 10000.0f, 0.0f };
	GLfloat light0_direction[] = { 0.0f, -10000.0f, -10000.0f, 0.0f };

	GLfloat light1_position[] = { 0.0f, 10000.0f, -10000.0f, 0.0f };
	GLfloat light1_direction[] = { 0.0f, -10000.0f, 10000.0f, 0.0f };

	GLfloat light2_position[] = { 0.0f, -10000.0f, 0.0f, 0.0f };
	GLfloat light2_direction[] = { 0.0f, 10000.0f, -0.0f, 0.0f };

	GLfloat light3_position[] = { 10000.0f, -10000.0f, 0.0f, 0.0f };
	GLfloat light3_direction[] = { -10000.0f, 10000.0f, -0.0f, 0.0f };

	GLfloat light4_position[] = { -10000.0f, -10000.0f, 0.0f, 0.0f };
	GLfloat light4_direction[] = { 10000.0f, 10000.0f, -0.0f, 0.0f };


	glLightfv(GL_LIGHT0, GL_POSITION, light0_position);
	glLightfv(GL_LIGHT1, GL_POSITION, light1_position);
	glLightfv(GL_LIGHT2, GL_POSITION, light2_position);
	glLightfv(GL_LIGHT3, GL_POSITION, light3_position);
	glLightfv(GL_LIGHT4, GL_POSITION, light4_position);


	glLightfv(GL_LIGHT0, GL_SPOT_DIRECTION, light0_direction);
	glLightfv(GL_LIGHT1, GL_SPOT_DIRECTION, light1_direction);
	glLightfv(GL_LIGHT2, GL_SPOT_DIRECTION, light2_direction);
	glLightfv(GL_LIGHT3, GL_SPOT_DIRECTION, light3_direction);
	glLightfv(GL_LIGHT4, GL_SPOT_DIRECTION, light4_direction);


	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHT1);
	glEnable(GL_LIGHT2);
	glEnable(GL_LIGHT3);
	glEnable(GL_LIGHT4);
}