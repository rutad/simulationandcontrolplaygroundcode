#include "LocomotionEngineManagerTransition.h"
#include "MPO_PeriodicRobotStateTrajectoriesObjective.h"


LocomotionEngineManagerTransition::LocomotionEngineManagerTransition(){
}

LocomotionEngineManagerTransition::LocomotionEngineManagerTransition(Robot* robot, FootFallPattern* footFallPattern, int nSamplePoints, LocomotionEngineMotionPlan* startPlan, LocomotionEngineMotionPlan* endPlan, int transitionStartIndex, int transitionEndIndex){
	this->footFallPattern = footFallPattern;

	motionPlan = new LocomotionEngineMotionPlan(robot, nSamplePoints);
	motionPlan->transitionStartPlan = startPlan;
	motionPlan->transitionEndPlan = endPlan;
	motionPlan->transitionStartIndex = transitionStartIndex;
	motionPlan->transitionEndIndex = transitionEndIndex;
	motionPlan->transitionStartPhase = transitionStartIndex / (double)nSamplePoints;
	motionPlan->transitionEndPhase = (transitionEndIndex + 1) / (double)nSamplePoints;
	motionPlan->syncMotionPlanWithFootFallPattern(*footFallPattern);
	

	motionPlan->frictionCoeff = -1;

	locomotionEngine = new LocomotionEngine(motionPlan);
	locomotionEngine->energyFunction->setupTransitionSubObjectives();
	locomotionEngine->useObjectivesOnly = true;

	// checkDerivatives = true;
}

LocomotionEngineManagerTransition::LocomotionEngineManagerTransition(Robot* robot, int nSamplePoints)
{
	this->footFallPattern = &origFootFallPattern;
	motionPlan = new LocomotionEngineMotionPlan(robot, nSamplePoints);

	motionPlan->frictionCoeff = -1;

	locomotionEngine = new LocomotionEngine(motionPlan);
	// locomotionEngine->energyFunction->setupTransitionSubObjectives();
	locomotionEngine->useObjectivesOnly = true;
}

void LocomotionEngineManagerTransition::warmStartMOpt() {

	warmStartMOptGRF();

	double transitionWeight = 1;
	for (int k = 0; k < 5; k++)
	{
		for (auto obj : locomotionEngine->energyFunction->objectives)
		{
			if (obj->description == "transition regularizer")
			{
				obj->weight = transitionWeight;
			}
		}

		for (int i = 0; i < 10; i++)
		{
			runMOPTStep();
		}

		transitionWeight *= 10;
	}

	
}

LocomotionEngineManagerTransition::~LocomotionEngineManagerTransition(){
	delete motionPlan;
	delete locomotionEngine;
}
