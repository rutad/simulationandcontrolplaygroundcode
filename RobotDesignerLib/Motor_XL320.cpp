#include "Motor_XL320.h"
#include <GUILib/GLContentManager.h>
#include <MathLib/Quaternion.h>

Motor_XL320::Motor_XL320(RobotBodyPart* parent, RobotBodyPart* child, const P3D& worldPos, const V3D& jointAxis) : RobotJointFeature(parent, child, worldPos, jointAxis) {
	motorMeshParent = GLContentManager::getGLMesh("../data/RobotDesigner/motorMeshes/XL-320_parent.obj"); motorMeshParent->getMaterial().setColor(0.15, 0.15, 0.15, 1.0);
	motorMeshChild = GLContentManager::getGLMesh("../data/RobotDesigner/motorMeshes/XL-320_child.obj"); motorMeshChild->getMaterial().setColor(0.7, 0.7, 0.7, 1.0);
	
	parentSupportMesh = GLContentManager::getGLMesh("../data/RobotDesigner/motorMeshes/XL-320-parentSupport.obj"); parentSupportMesh->getMaterial().setColor(0.9, 0.9, 0.9, 1.0);
	childSupportMesh = GLContentManager::getGLMesh("../data/RobotDesigner/motorMeshes/XL-320-childSupport.obj"); childSupportMesh->getMaterial().setColor(0.9, 0.9, 0.9, 1.0);

	this->name = "Dynamixel XL320";
}

Motor_XL320::~Motor_XL320() {

}

void Motor_XL320::getListOfPossibleAttachmentPoints(RobotBodyPart* rbp, DynamicArray<AttachmentPoint>& attachmentPoints) {
//	attachmentPoints.clear();
	if (rbp == parent){
		double x = 0.015 + 0.0005;
		double y = -0.0285 - 0.0005;
		double z = 0.015 + 0.0005;
		attachmentPoints.push_back(AttachmentPoint(getWorldLocationForPointOnMotorParentSide(P3D(-x, y, -z)), getWorldLocationForVectorOnMotorParentSide(V3D(0, 0, -1).unit())));
		attachmentPoints.push_back(AttachmentPoint(getWorldLocationForPointOnMotorParentSide(P3D(x, y, -z)), getWorldLocationForVectorOnMotorParentSide(V3D(0, 0, -1).unit())));
		attachmentPoints.push_back(AttachmentPoint(getWorldLocationForPointOnMotorParentSide(P3D(x, y, z)), getWorldLocationForVectorOnMotorParentSide(V3D(0, 0, 1).unit())));
		attachmentPoints.push_back(AttachmentPoint(getWorldLocationForPointOnMotorParentSide(P3D(-x, y, z)), getWorldLocationForVectorOnMotorParentSide(V3D(0, 0, 1).unit())));
		attachmentPoints.push_back(AttachmentPoint(getWorldLocationForPointOnMotorParentSide(P3D(-x, y, -z)), getWorldLocationForVectorOnMotorParentSide(V3D(0, -1, 0).unit())));
		attachmentPoints.push_back(AttachmentPoint(getWorldLocationForPointOnMotorParentSide(P3D(x, y, -z)), getWorldLocationForVectorOnMotorParentSide(V3D(0, -1, 0).unit())));
		attachmentPoints.push_back(AttachmentPoint(getWorldLocationForPointOnMotorParentSide(P3D(x, y, z)), getWorldLocationForVectorOnMotorParentSide(V3D(0, -1, 0).unit())));
		attachmentPoints.push_back(AttachmentPoint(getWorldLocationForPointOnMotorParentSide(P3D(-x, y, z)), getWorldLocationForVectorOnMotorParentSide(V3D(0, -1, 0).unit())));
		attachmentPoints.push_back(AttachmentPoint(getWorldLocationForPointOnMotorParentSide(P3D(-x, y, -z)), getWorldLocationForVectorOnMotorParentSide(V3D(-1, 0, 0).unit())));
		attachmentPoints.push_back(AttachmentPoint(getWorldLocationForPointOnMotorParentSide(P3D(x, y, -z)), getWorldLocationForVectorOnMotorParentSide(V3D(1, 0, 0).unit())));
		attachmentPoints.push_back(AttachmentPoint(getWorldLocationForPointOnMotorParentSide(P3D(x, y, z)), getWorldLocationForVectorOnMotorParentSide(V3D(1, 0, 0).unit())));
		attachmentPoints.push_back(AttachmentPoint(getWorldLocationForPointOnMotorParentSide(P3D(-x, y, z)), getWorldLocationForVectorOnMotorParentSide(V3D(-1, 0, 0).unit())));

		y = -0.0105 - 0.0005;
		attachmentPoints.push_back(AttachmentPoint(getWorldLocationForPointOnMotorParentSide(P3D(-x, y, -z)), getWorldLocationForVectorOnMotorParentSide(V3D(-1, 0, 0).unit())));
		attachmentPoints.push_back(AttachmentPoint(getWorldLocationForPointOnMotorParentSide(P3D(x, y, -z)), getWorldLocationForVectorOnMotorParentSide(V3D(1, 0, 0).unit())));
		attachmentPoints.push_back(AttachmentPoint(getWorldLocationForPointOnMotorParentSide(P3D(x, y, z)), getWorldLocationForVectorOnMotorParentSide(V3D(1, 0, 0).unit())));
		attachmentPoints.push_back(AttachmentPoint(getWorldLocationForPointOnMotorParentSide(P3D(-x, y, z)), getWorldLocationForVectorOnMotorParentSide(V3D(-1, 0, 0).unit())));

		attachmentPoints.push_back(AttachmentPoint(getWorldLocationForPointOnMotorParentSide(P3D(-x, y, -z)), getWorldLocationForVectorOnMotorParentSide(V3D(0, 0, -1).unit())));
		attachmentPoints.push_back(AttachmentPoint(getWorldLocationForPointOnMotorParentSide(P3D(x, y, -z)), getWorldLocationForVectorOnMotorParentSide(V3D(0, 0, -1).unit())));
		attachmentPoints.push_back(AttachmentPoint(getWorldLocationForPointOnMotorParentSide(P3D(x, y, z)), getWorldLocationForVectorOnMotorParentSide(V3D(0, 0, 1).unit())));
		attachmentPoints.push_back(AttachmentPoint(getWorldLocationForPointOnMotorParentSide(P3D(-x, y, z)), getWorldLocationForVectorOnMotorParentSide(V3D(0, 0, 1).unit())));

	}else{
		double x = 0.015 + 0.0005;
		double y = 0.015 + 0.0005;
		double z = 0.018 + 0.0005;

		attachmentPoints.push_back(AttachmentPoint(getWorldLocationForPointOnMotorChildSide(P3D(-x, y, -z)), getWorldLocationForVectorOnMotorChildSide(V3D(0, 1, 0))));
		attachmentPoints.push_back(AttachmentPoint(getWorldLocationForPointOnMotorChildSide(P3D(x, y, -z)), getWorldLocationForVectorOnMotorChildSide(V3D(0, 1, 0))));
		attachmentPoints.push_back(AttachmentPoint(getWorldLocationForPointOnMotorChildSide(P3D(x, y, z)), getWorldLocationForVectorOnMotorChildSide(V3D(0, 1, 0))));
		attachmentPoints.push_back(AttachmentPoint(getWorldLocationForPointOnMotorChildSide(P3D(-x, y, z)), getWorldLocationForVectorOnMotorChildSide(V3D(0, 1, 0))));

		attachmentPoints.push_back(AttachmentPoint(getWorldLocationForPointOnMotorChildSide(P3D(-x, y, -z)), getWorldLocationForVectorOnMotorChildSide(V3D(-1, 0, 0))));
		attachmentPoints.push_back(AttachmentPoint(getWorldLocationForPointOnMotorChildSide(P3D(x, y, -z)), getWorldLocationForVectorOnMotorChildSide(V3D(1, 0, 0))));
		attachmentPoints.push_back(AttachmentPoint(getWorldLocationForPointOnMotorChildSide(P3D(x, y, z)), getWorldLocationForVectorOnMotorChildSide(V3D(1, 0, 0))));
		attachmentPoints.push_back(AttachmentPoint(getWorldLocationForPointOnMotorChildSide(P3D(-x, y, z)), getWorldLocationForVectorOnMotorChildSide(V3D(-1, 0, 0))));

		attachmentPoints.push_back(AttachmentPoint(getWorldLocationForPointOnMotorChildSide(P3D(-x, y, -z)), getWorldLocationForVectorOnMotorChildSide(V3D(0, 0, -1))));
		attachmentPoints.push_back(AttachmentPoint(getWorldLocationForPointOnMotorChildSide(P3D(x, y, -z)), getWorldLocationForVectorOnMotorChildSide(V3D(0, 0, -1))));
		attachmentPoints.push_back(AttachmentPoint(getWorldLocationForPointOnMotorChildSide(P3D(x, y, z)), getWorldLocationForVectorOnMotorChildSide(V3D(0, 0, 1))));
		attachmentPoints.push_back(AttachmentPoint(getWorldLocationForPointOnMotorChildSide(P3D(-x, y, z)), getWorldLocationForVectorOnMotorChildSide(V3D(0, 0, 1))));
/*
		x = 0.0135;
		y = 0.005;
		z = 0.018;

		attachmentPoints.push_back(AttachmentPoint(getWorldLocationForPointOnMotorChildSide(P3D(-x, y, -z)), getWorldLocationForVectorOnMotorChildSide(V3D(0, 0, -1))));
		attachmentPoints.push_back(AttachmentPoint(getWorldLocationForPointOnMotorChildSide(P3D(x, y, -z)), getWorldLocationForVectorOnMotorChildSide(V3D(0, 0, -1))));
		attachmentPoints.push_back(AttachmentPoint(getWorldLocationForPointOnMotorChildSide(P3D(x, y, z)), getWorldLocationForVectorOnMotorChildSide(V3D(0, 0, 1))));
		attachmentPoints.push_back(AttachmentPoint(getWorldLocationForPointOnMotorChildSide(P3D(-x, y, z)), getWorldLocationForVectorOnMotorChildSide(V3D(0, 0, 1))));
		
		attachmentPoints.push_back(AttachmentPoint(getWorldLocationForPointOnMotorChildSide(P3D(-x, y, -z)), getWorldLocationForVectorOnMotorChildSide(V3D(1, 0, 0))));
		attachmentPoints.push_back(AttachmentPoint(getWorldLocationForPointOnMotorChildSide(P3D(x, y, -z)), getWorldLocationForVectorOnMotorChildSide(V3D(-1, 0, 0))));
		attachmentPoints.push_back(AttachmentPoint(getWorldLocationForPointOnMotorChildSide(P3D(x, y, z)), getWorldLocationForVectorOnMotorChildSide(V3D(-1, 0, 0))));
		attachmentPoints.push_back(AttachmentPoint(getWorldLocationForPointOnMotorChildSide(P3D(-x, y, z)), getWorldLocationForVectorOnMotorChildSide(V3D(1, 0, 0))));
*/
	}

}
