#include "RobotDesign.h"
#include "errors.h"
#include <RBSimLib/ODERBEngine.h>
#include <ControlLib/Robot.h>

//TODO: the rbs file format still needs some info for embedding motors: flipped or not, embedded angle saved or not.

RobotDesign::RobotDesign(){
	//add two bodies connected by a joint by default...
	robotBodyParts.push_back(createRobotBodyPart());
	robotBodyParts.push_back(createRobotBodyPart());
	createJointBetween(robotBodyParts[0], robotBodyParts[1], P3D(0,0,0), V3D(0,0,1));
	robotBodyParts[0]->addFeaturePoint(P3D(), 0.03);
}

RobotDesign::~RobotDesign() {

}

void RobotDesign::clear(){
	robotBodyParts.clear();
}

void RobotDesign::createJointBetween(RobotBodyPart* parent, RobotBodyPart* child, const P3D& pos, const V3D& axis) {
//	parent->addJointToBodyPart_MX28(child, pos, axis);
//	parent->addJointToBodyPart_XL320(child, pos, axis);
//	parent->addJointToBodyPart_X5(child, pos, axis);
	parent->addJointToBodyPart_XM430(child, pos, axis); 
}

RobotBodyPart* RobotDesign::createRobotBodyPart() {
	return new RobotBodyPart();
}

RobotBodyPart* RobotDesign::getRobotBodyPartByName(const std::string& name) {
	for (uint i = 0; i < robotBodyParts.size(); i++)
		if (robotBodyParts[i]->name.compare(name) == 0)
			return robotBodyParts[i];
	return NULL;
}

int RobotDesign::getRobotBodyPartIndexByName(const std::string& name) {
	for (uint i = 0; i < robotBodyParts.size(); i++)
		if (robotBodyParts[i]->name.compare(name) == 0)
			return i;
	return -1;
}

RobotBodyPart* RobotDesign::createRobotBodyPart(RigidBody* rb) {
	RobotBodyPart* rbPart = new RobotBodyPart();

	rbPart->name = rb->name;

	for (int i = 0; i < rb->rbProperties.getEndEffectorPointCount(); i++)
		rbPart->addEEFeaturePoint(rb->getWorldCoordinates(rb->rbProperties.endEffectorPoints[i].coords), rb->rbProperties.endEffectorPoints[i].featureSize);

	for (uint i = 0; i < rb->rbProperties.bodyPointFeatures.size(); i++)
		rbPart->addFeaturePoint(rb->getWorldCoordinates(rb->rbProperties.bodyPointFeatures[i].coords), rb->rbProperties.bodyPointFeatures[i].featureSize);

	return rbPart;
}

void RobotDesign::readRobotFromFile(const char* filename) {
	clear();
	
	//we will read the articulated rigid body structure and translate it to a robot design...
	ODERBEngine* rbEngine = new ODERBEngine();
	rbEngine->loadRBsFromFile(filename);
	//assume the first rb is the root!
	Robot* robot = new Robot(rbEngine->rbs[0]);
	//reset the pose of the robot to "all zeros"
	ReducedRobotState rs(robot);
	rs.setPosition(P3D());
	rs.setOrientation(Quaternion());
	for (int i = 0; i < robot->getJointCount(); i++)
		rs.setJointRelativeOrientation(Quaternion(), i);
	robot->setState(&rs);

	//all right. Now, there is a one to one map between the robot and our robot design. So go through all the rigid bodies and all the joints and link everything...
	robotBodyParts.push_back(createRobotBodyPart(robot->root));

//	if (robotBodyParts[0]->bodyFeatures.size() == 0)
//		robotBodyParts[0]->addFeaturePoint(P3D(), 0.03);

	for (int i = 0; i < robot->getJointCount(); i++)
		robotBodyParts.push_back(createRobotBodyPart(robot->getJoint(i)->child));

//	Logger::consolePrint("got %d bodies!\n", robotBodyParts.size());

	for (int i = 0; i < robot->getJointCount();i++){
		P3D jPos = robot->getJoint(i)->getWorldPosition();
		V3D jRotAxis = V3D(0, 0, 1);
		HingeJoint* hj = dynamic_cast<HingeJoint*>(robot->getJoint(i));
		if (hj)
			jRotAxis = hj->parent->getWorldCoordinates(hj->rotationAxis);

		createJointBetween(getRobotBodyPartByName(robot->getJoint(i)->parent->name), getRobotBodyPartByName(robot->getJoint(i)->child->name), jPos, jRotAxis);

		if (hj) 
			getRobotBodyPartByName(robot->getJoint(i)->child->name)->pJoint->dynamixelProperties = hj->dynamixelProperties;

		getRobotBodyPartByName(robot->getJoint(i)->child->name)->pJoint->originalJointName = robot->getJoint(i)->name;

		
	}

	delete robot;
	delete rbEngine;

	//now, if parts of a robot look symmetric, try to load them up as such, they probably are...
	for (uint i = 0; i < robotBodyParts.size(); i++)
		for (uint j = 0; j < robotBodyParts.size(); j++){
			if (i == j)
				continue;
			P3D p1 = robotBodyParts[i]->getPosition(); p1[0] *= -1;
			P3D p2 = robotBodyParts[j]->getPosition();
			if (IS_ZERO(V3D(p1, p2).length())) {
				robotBodyParts[i]->mirroredBodyPart = robotBodyParts[j];
				robotBodyParts[j]->mirroredBodyPart = robotBodyParts[i];
			}
		}
}

void RobotDesign::saveRobotToFile(const char* filename) {
	//we will create an RB world, populate it with all the right rigid bodies and constraints, and then write it all out to a file...

	FILE* fp = fopen(filename, "w");
	DynamicArray<RigidBody*> rbs;

	//map each body part of the robot to a rigid body
	for (uint i = 0; i < robotBodyParts.size(); i++) {
		robotBodyParts[i]->name = std::string("rb") + std::to_string(i);
		RigidBody* newRB = new RigidBody();
		robotBodyParts[i]->populateRB(newRB);
//		if (i == 0)
//			newRB->rbProperties.isFrozen = true;
		newRB->writeToFile(fp);
		fprintf(fp, "\n\n");
		rbs.push_back(newRB);
	}

	//and now create all the joints...
	for (uint i = 0; i < robotBodyParts.size(); i++) {
		if (RobotJointFeature* j = robotBodyParts[i]->pJoint) {
			HingeJoint* hj = new HingeJoint();
			if (j->originalJointName.length() > 0)
				hj->name = j->originalJointName;
			else
				hj->name = j->parent->name + "_" + j->child->name;
			hj->rotationAxis = j->parent->getLocalCoordinates(j->jointAxis);
			hj->parent = rbs[getRobotBodyPartIndexByName(j->parent->name)];
			hj->child = rbs[getRobotBodyPartIndexByName(j->child->name)];
			hj->pJPos = j->parent->getLocalCoordinates(j->positionWorld);
			hj->cJPos = j->child->getLocalCoordinates(j->positionWorld);
			hj->dynamixelProperties = j->dynamixelProperties;

			hj->writeToFile(fp);
			fprintf(fp, "\n\n");
			delete hj;
		}
	}

	for (uint i = 0; i < rbs.size();i++)
		delete rbs[i];

	fclose(fp);
}

void RobotDesign::draw(){
	for (uint i = 0; i < robotBodyParts.size(); i++)
		robotBodyParts[i]->drawBodyParts();
}


//assume vertices of the mesh are in world frame
GLMesh* transformMeshFromWorldToLocalCoords(GLMesh* inputMesh, RigidBody* parent) {
	for (int k = 0; k < inputMesh->getVertexCount(); k++) {
		P3D vertex = inputMesh->getVertex(k);
		inputMesh->setVertex(k, parent->getLocalCoordinates(vertex));
	}
	inputMesh->computeNormals();
	return inputMesh;
}

//assume vertices of the mesh are in local coords, and we need to transform them to coordinate frame that represents their embedding, and then move them to a specific location (of the joint) in world coordinates
GLMesh* transformMeshFromLocalCoordsToEmbeddingCoords(GLMesh* inputMesh, RigidBody* parent, const Quaternion& embeddingTransformation, const P3D& meshLocalPos) {
	for (int k = 0; k < inputMesh->getVertexCount(); k++) {
		P3D vertex = inputMesh->getVertex(k);
		inputMesh->setVertex(k, meshLocalPos + embeddingTransformation.rotate(V3D(P3D(), vertex)));
	}
	inputMesh->computeNormals();
	return inputMesh;
}

//This method assumes a one to one correspondence between the robot and the robot designs
void RobotDesign::transferMeshes(const DynamicArray<RigidBody*>& rbList) {
	for (uint i = 0; i < robotBodyParts.size(); i++) {
		RobotBodyPart* rbp = robotBodyParts[i];
		RigidBody* rb = rbList[i];
		DynamicArray<StructureFeature*> structureFeatures;
		rbp->getStructureFeatures(structureFeatures);

		for (uint j = 0; j < structureFeatures.size(); j++) {
			rb->meshes.push_back(transformMeshFromWorldToLocalCoords(structureFeatures[j]->structureMesh.clone(), rb));
			rb->meshes.back()->getMaterial().setColor(0.8 * 243 / 255.0, 0.8 * 245 / 255.0, 0.8 * 221 / 255.0, 1);
		}

		//now the motors...
		for (uint j = 0; j < rbp->cJoints.size(); j++) {
			rb->meshes.push_back(transformMeshFromLocalCoordsToEmbeddingCoords(rbp->cJoints[j]->parentSupportMesh->clone(), rb, rbp->cJoints[j]->getOrientation(), rb->getLocalCoordinates(rbp->cJoints[j]->positionWorld)));
			rb->meshes.back()->getMaterial().setColor(0.8 * 243 / 255.0, 0.8 * 245 / 255.0, 0.8 * 221 / 255.0, 1);
			rb->meshes.push_back(transformMeshFromLocalCoordsToEmbeddingCoords(rbp->cJoints[j]->motorMeshParent->clone(), rb, rbp->cJoints[j]->getOrientation(), rb->getLocalCoordinates(rbp->cJoints[j]->positionWorld)));
		}
		if (rbp->pJoint) {
			rb->meshes.push_back(transformMeshFromLocalCoordsToEmbeddingCoords(rbp->pJoint->childSupportMesh->clone(), rb, rbp->pJoint->getOrientation(), rb->getLocalCoordinates(rbp->pJoint->positionWorld)));
			rb->meshes.back()->getMaterial().setColor(0.8 * 243 / 255.0, 0.8 * 245 / 255.0, 0.8 * 221 / 255.0, 1);
			rb->meshes.push_back(transformMeshFromLocalCoordsToEmbeddingCoords(rbp->pJoint->motorMeshChild->clone(), rb, rbp->pJoint->getOrientation(), rb->getLocalCoordinates(rbp->pJoint->positionWorld)));
		}
	}

}
