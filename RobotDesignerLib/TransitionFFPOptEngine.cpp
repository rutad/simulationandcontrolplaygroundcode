#include "TransitionFFPOptEngine.h"
#include <OptimizationLib/BFGSFunctionMinimizer.h>


TransitionFFPOptEngine::TransitionFFPOptEngine(FootFallPattern* transFFP, FootFallPattern* startFFP, FootFallPattern* endFFP, int transStartIndex, int transEndIndex)
{
	plan = new TransitionFFPOptPlan(transFFP, startFFP, endFFP, transStartIndex, transEndIndex);
	energyFunction = new TransitionFFPObjective(plan, "Transition FFP Objective", 1);
}

TransitionFFPOptEngine::~TransitionFFPOptEngine()
{
	delete energyFunction;
	delete plan;
}

double TransitionFFPOptEngine::optimizePlan(int maxIterNum) {
	// NewtonFunctionMinimizer minimizer(maxIterNum);
	BFGSFunctionMinimizer minimizer(maxIterNum);

	dVector params;
	this->plan->writeParamsToList(params);

	//energyFunction->testGradientWithFD(params);
	//energyFunction->testHessianWithFD(params);

	double val = 0;
	minimizer.minimize(energyFunction, params, val);

	writeSolutionToFFP();

	return val;
}

void TransitionFFPOptEngine::writeSolutionToFFP()
{
	for (int i = 0; i < (int)plan->planStepPatterns.size(); i++)
	{
		auto& planPattern = plan->planStepPatterns[i];
		auto& sPattern = plan->transFFP->stepPatterns[i];

		sPattern.startIndex = (int)round(planPattern.startIndex);
		sPattern.endIndex = (int)round(planPattern.endIndex);

		Logger::print("%d: %i %i\n", i, sPattern.startIndex, sPattern.endIndex);
	}
}

