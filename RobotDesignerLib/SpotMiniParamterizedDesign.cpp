#include "SpotMiniParamterizedDesign.h"
#include "GUILib/GLContentManager.h"
#include <iostream>

ParametrizedRobotObjective::ParametrizedRobotObjective(SpotMiniParameterizedDesign *prd) { // TODO: just take ptr to design
	this->prd = prd;
	// TODO: Consider storing:  MOPTWindow *window; (TODO: is window ever deleted/recreated?)

	// E <=> window->locomotionmManager->energyFunction
	// PRObjective needs E.
	// PRDesign needs to be able to create its own objective.

	// Proposed Plan: Store *window in PRDesign and PRObjective
	// TODO: Consider function: this->get_E(), which returns a pointer to the energy function.

}

LocomotionEngine_EnergyFunction * ParametrizedRobotObjective::E() {
	// What are locomotionEngine[Start|End]
	return this->prd->mopt->locomotionManager->locomotionEngine->energyFunction;
}

// compute the objective values from PRD's design params
double ParametrizedRobotObjective::computeValue(const dVector& xi) {

	
	// // Propogate xi into the design.
	// TODO: Operate on a temporary PRDesign, FORNOW: modify in place
	vector<double> xi_as_vector;
	for (int i = 0; i < xi.size(); ++i) { 
		double c_xi = xi[i];// std::min(std::max(xi[i], 0.1), 10.);
		xi_as_vector.push_back( c_xi ); 
	}
	prd->setParameters(xi_as_vector);

	// New version.
	delete this->prd->mopt->locomotionManager;
	this->prd->mopt->locomotionManager = this->prd->mopt->initializeNewMP();
	this->prd->mopt->locomotionManager->printDebugInfo = false;
	double threshold = 1e-8;
	unsigned int num_MOPT_iters = 10000;
	double last_value = 0;
	for (auto i = 0U; i < num_MOPT_iters; i++) {
		dVector P;
		this->prd->mopt->locomotionManager->runMOPTStep();
		this->prd->mopt->locomotionManager->motionPlan->writeMPParametersToList(P);
		double current = this->prd->mopt->locomotionManager->locomotionEngine->energyFunction->objectives[11]->computeValue(P);
		if (std::abs(current - last_value) < threshold) {
			std::cout << "Early out at " << i << " iterations." << std::endl;
			break;
		}
		last_value = current;
	}

	dVector P;
	this->prd->mopt->locomotionManager->motionPlan->writeMPParametersToList(P);
	double O = this->prd->mopt->locomotionManager->locomotionEngine->energyFunction->objectives[11]->computeValue(P);

	// // reg_xi
	double reg_xi =  1e-5 * .5 * xi.squaredNorm();
	Logger::logPrint("[PRO computeValue ] O %e \t xi %e \n", O + reg_xi, xi[0]);
	Logger::consolePrint("[PRO computeValue ] O %e \t xi %e \n", O + reg_xi, xi[0]);
	

	cout << "xi: " << xi[0] << endl;
	cout << " O: " << O     << endl;
	cout << endl;


	return O;
	// return  pow(xi[0] - 1.5, 2) + pow(xi[1] - 0.5, 2);
}


SpotMiniParameterizedDesign::SpotMiniParameterizedDesign(Robot* robot, MOPTWindow *mopt) : ParameterizedRobotDesign(robot, NULL) {
	this->robot = robot;
	this->mopt = mopt;
	this->objective = new ParametrizedRobotObjective(this); // FORNOW: Default constructor.

	for (int i = 0; i < robot->getJointCount(); i++)
		initialMorphology.push_back(JointParameters(robot->getJoint(i)));

	// just scale for now
	currentParams.clear();
	currentParams.push_back(0.1);
//	currentParams.push_back(0.);

	setParameters(currentParams);
}

SpotMiniParameterizedDesign::SpotMiniParameterizedDesign(Robot* robot) : ParameterizedRobotDesign (robot, NULL){
	//should probably check that this really is the morphology we expect...


}


SpotMiniParameterizedDesign::~SpotMiniParameterizedDesign() {
}

void SpotMiniParameterizedDesign::getCurrentSetOfParameters(DynamicArray<double>& params) {
	params = currentParams;
}

void SpotMiniParameterizedDesign::setParameters(const DynamicArray<double>& params) {
	
	for (int i = 0; i < robot->getJointCount(); i++) {
		robot->getJoint(i)->pJPos = initialMorphology[i].pJPos * params[0];
		robot->getJoint(i)->cJPos = initialMorphology[i].cJPos * params[0];
	}

	/*
	//double f = TEST_DOUBLE;
	double f = params[1];


	for (auto &i : top_layer) {
		auto joint = robot->getJoint(i);
		V3D p2c_hat = V3D(joint->pJPos, joint->cJPos).normalized();
		joint->cJPos += p2c_hat * f;
	}
	*/

	/*
	currentParams = params;

	//offset the positions of the hip joints, symetrically...

	robot->getJoint(0)->pJPos.x() = initialMorphology[0].pJPos.x() + params[0];
	robot->getJoint(1)->pJPos.x() = initialMorphology[1].pJPos.x() - params[0];
	robot->getJoint(2)->pJPos.x() = initialMorphology[2].pJPos.x() + params[0];
	robot->getJoint(3)->pJPos.x() = initialMorphology[3].pJPos.x() - params[0];

	robot->getJoint(0)->pJPos.z() = initialMorphology[0].pJPos.z() + params[1];
	robot->getJoint(1)->pJPos.z() = initialMorphology[1].pJPos.z() + params[1];
	robot->getJoint(2)->pJPos.z() = initialMorphology[2].pJPos.z() - params[1];
	robot->getJoint(3)->pJPos.z() = initialMorphology[3].pJPos.z() - params[1];

	robot->getJoint(0)->pJPos.y() = initialMorphology[0].pJPos.y() + params[2];
	robot->getJoint(1)->pJPos.y() = initialMorphology[1].pJPos.y() + params[2];
	robot->getJoint(2)->pJPos.y() = initialMorphology[2].pJPos.y() + params[3];
	robot->getJoint(3)->pJPos.y() = initialMorphology[3].pJPos.y() + params[3];
	*/
}

void SpotMiniParameterizedDesign::optimize() {

	/*
	// out of robot
	vector<double> params;
	getCurrentSetOfParameters(params);
	*/

	auto &xi = currentParams;

	GradientDescentFunctionMinimizer minimizer(1); // TODO: 3?
	minimizer.printOutput = true;
	

	// into minimizer
	dVector paramsSolver;
	paramsSolver.resize(xi.size());
	paramsSolver.setZero();
	for (size_t i = 0; i < xi.size(); ++i) { paramsSolver[i] = xi[i]; } // TODO: vec2dVec

	Logger::logPrint("----------------\n");
	Logger::logPrint("RUNNING OPT STEP\n");
	Logger::logPrint("----------------\n");
																		// minimize
	double functionValue = objective->computeValue(paramsSolver);
	minimizer.minimize(objective, paramsSolver, functionValue);

	cout <<"xi "<< paramsSolver[0] << endl;

	Logger::logPrint("----------------\n");
	Logger::logPrint("DONE OPT STEP\n");
	Logger::logPrint("----------------\n");




	// out of minimizer
	for (size_t i = 0; i < xi.size(); ++i) { xi[i] = paramsSolver[i]; } // TODO: dVec2vec

																		// into robot
	setParameters(xi);


}