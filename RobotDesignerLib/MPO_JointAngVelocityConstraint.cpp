#include "MPO_JointAngVelocityConstraint.h"
#include "MPO_GRFSoftConstraints.h"

MPO_JointAngVelocityConstraint::MPO_JointAngVelocityConstraint(LocomotionEngineMotionPlan* mp, const std::string& objectiveDescription, double weight, int startQIndex, int endQIndex){
    theMotionPlan = mp;
    this->startQIndex = startQIndex;
    this->endQIndex = endQIndex;
    this->description = objectiveDescription;
    this->weight = weight;
}

MPO_JointAngVelocityConstraint::~MPO_JointAngVelocityConstraint(void){
}

double MPO_JointAngVelocityConstraint::computeValue(const dVector& s){
    //assume the parameters of the motion plan have been set already by the collection of objective functions class
    //  theMotionPlan->setMPParametersFromList(p);

    double retVal = 0;
    
    int end = theMotionPlan->nSamplePoints;
    if (theMotionPlan->wrapAroundBoundaryIndex >= 0) end -= 1; //don't double count... the last robot pose is already the same as the first one, which means that COM and feet locations are in correct locations relative to each other, so no need to ask for that again explicitely...

    // SUC functions
    SoftUnilateralConstraint sucVelocityLowerBound(-theMotionPlan->jointAngVelocityUpperBoundVal, theMotionPlan->jointAngVelocityStiffness, theMotionPlan->jointAngVelocityEpsilon);
    SoftUnilateralConstraint sucVelocityUpperBound(-theMotionPlan->jointAngVelocityUpperBoundVal, theMotionPlan->jointAngVelocityStiffness, theMotionPlan->jointAngVelocityEpsilon);

    for (int j = 0; j < end; j++){
        int jm, jp;

        theMotionPlan->getVelocityTimeIndicesFor(j, jm, jp);
        if (jm == -1 || jp == -1) continue;

        double dt = theMotionPlan->motionPlanDuration / theMotionPlan->nSamplePoints; dt = 1.0;
        for (int i = startQIndex; i <= endQIndex; i++){
            double velocity = theMotionPlan->robotStateTrajectory.qArray[jp][i] - theMotionPlan->robotStateTrajectory.qArray[jm][i];
            velocity /= dt;
            
            retVal += sucVelocityLowerBound.computeValue(velocity);
            retVal += sucVelocityUpperBound.computeValue(-velocity);
        }
    }
    
    return retVal * weight;
}

void MPO_JointAngVelocityConstraint::addGradientTo(dVector& grad, const dVector& p) {
    //  assume the parameters of the motion plan have been set already by the collection of objective functions class
    //  theMotionPlan->setMPParametersFromList(p);

    //and now compute the gradient with respect to the robot q's
    if (theMotionPlan->robotStatesParamsStartIndex >= 0){
        int end = theMotionPlan->nSamplePoints;
        if (theMotionPlan->wrapAroundBoundaryIndex >= 0) end -= 1; //don't double count... the last robot pose is already the same as the first one, which means that COM and feet locations are in correct locations relative to each other, so no need to ask for that again explicitely...

	    // SUC functions
		SoftUnilateralConstraint sucVelocityLowerBound(-theMotionPlan->jointAngVelocityUpperBoundVal, theMotionPlan->jointAngVelocityStiffness, theMotionPlan->jointAngVelocityEpsilon);
		SoftUnilateralConstraint sucVelocityUpperBound(-theMotionPlan->jointAngVelocityUpperBoundVal, theMotionPlan->jointAngVelocityStiffness, theMotionPlan->jointAngVelocityEpsilon);

        for (int j = 0; j < end; j++){
            int jm, jp;

            theMotionPlan->getVelocityTimeIndicesFor(j, jm, jp);
            if (jm == -1 || jp == -1) continue;

            double dt = theMotionPlan->motionPlanDuration / theMotionPlan->nSamplePoints; dt = 1.0;

            for (int i = startQIndex; i <= endQIndex; i++){
				double velocity = theMotionPlan->robotStateTrajectory.qArray[jp][i] - theMotionPlan->robotStateTrajectory.qArray[jm][i];
                velocity /= dt;

                grad[theMotionPlan->robotStatesParamsStartIndex + theMotionPlan->robotStateTrajectory.nStateDim * jp + i] += sucVelocityLowerBound.computeDerivative(velocity) * weight / dt;
				grad[theMotionPlan->robotStatesParamsStartIndex + theMotionPlan->robotStateTrajectory.nStateDim * jp + i] += -sucVelocityUpperBound.computeDerivative(-velocity) * weight / dt;
                
				grad[theMotionPlan->robotStatesParamsStartIndex + theMotionPlan->robotStateTrajectory.nStateDim * jm + i] += -sucVelocityLowerBound.computeDerivative(velocity) * weight / dt;
				grad[theMotionPlan->robotStatesParamsStartIndex + theMotionPlan->robotStateTrajectory.nStateDim * jm + i] += sucVelocityUpperBound.computeDerivative(-velocity) * weight / dt;
            }
        }
    }
}

void MPO_JointAngVelocityConstraint::addHessianEntriesTo(DynamicArray<MTriplet>& hessianEntries, const dVector& p) {
    //  assume the parameters of the motion plan have been set already by the collection of objective functions class
    //  theMotionPlan->setMPParametersFromList(p);

	if (theMotionPlan->robotStatesParamsStartIndex >= 0) {
		int end = theMotionPlan->nSamplePoints;
		if (theMotionPlan->wrapAroundBoundaryIndex >= 0) end -= 1; //don't double count... the last robot pose is already the same as the first one, which means that COM and feet locations are in correct locations relative to each other, so no need to ask for that again explicitely...

		// SUC functions
		SoftUnilateralConstraint sucVelocityLowerBound(-theMotionPlan->jointAngVelocityUpperBoundVal, theMotionPlan->jointAngVelocityStiffness, theMotionPlan->jointAngVelocityEpsilon);
		SoftUnilateralConstraint sucVelocityUpperBound(-theMotionPlan->jointAngVelocityUpperBoundVal, theMotionPlan->jointAngVelocityStiffness, theMotionPlan->jointAngVelocityEpsilon);

		for (int j = 0; j < end; j++) {
			int jm, jp;

			theMotionPlan->getVelocityTimeIndicesFor(j, jm, jp);
			if (jm == -1 || jp == -1) continue;

			//      logPrint("%d %d %d %d\n", jpp, jp, jm, jmm);

			double dt = theMotionPlan->motionPlanDuration / theMotionPlan->nSamplePoints; dt = 1.0;
			double offset = 1 / (dt * dt);

			for (int i = startQIndex; i <= endQIndex; i++) {
				double velocity = theMotionPlan->robotStateTrajectory.qArray[jp][i] - theMotionPlan->robotStateTrajectory.qArray[jm][i];
				velocity /= dt;

				ADD_HES_ELEMENT(hessianEntries, theMotionPlan->robotStatesParamsStartIndex + jp * theMotionPlan->robotStateTrajectory.nStateDim + i, theMotionPlan->robotStatesParamsStartIndex + jp * theMotionPlan->robotStateTrajectory.nStateDim + i, sucVelocityLowerBound.computeSecondDerivative(velocity) / (dt * dt), weight);
				ADD_HES_ELEMENT(hessianEntries, theMotionPlan->robotStatesParamsStartIndex + jp * theMotionPlan->robotStateTrajectory.nStateDim + i, theMotionPlan->robotStatesParamsStartIndex + jp * theMotionPlan->robotStateTrajectory.nStateDim + i, sucVelocityUpperBound.computeSecondDerivative(-velocity) / (dt * dt), weight);
				
				ADD_HES_ELEMENT(hessianEntries, theMotionPlan->robotStatesParamsStartIndex + jp * theMotionPlan->robotStateTrajectory.nStateDim + i, theMotionPlan->robotStatesParamsStartIndex + jm * theMotionPlan->robotStateTrajectory.nStateDim + i, -sucVelocityLowerBound.computeSecondDerivative(velocity) / (dt * dt), weight);
				ADD_HES_ELEMENT(hessianEntries, theMotionPlan->robotStatesParamsStartIndex + jp * theMotionPlan->robotStateTrajectory.nStateDim + i, theMotionPlan->robotStatesParamsStartIndex + jm * theMotionPlan->robotStateTrajectory.nStateDim + i, -sucVelocityUpperBound.computeSecondDerivative(-velocity) / (dt * dt), weight);
				
				ADD_HES_ELEMENT(hessianEntries, theMotionPlan->robotStatesParamsStartIndex + jm * theMotionPlan->robotStateTrajectory.nStateDim + i, theMotionPlan->robotStatesParamsStartIndex + jm * theMotionPlan->robotStateTrajectory.nStateDim + i, sucVelocityLowerBound.computeSecondDerivative(velocity) / (dt * dt), weight);
				ADD_HES_ELEMENT(hessianEntries, theMotionPlan->robotStatesParamsStartIndex + jm * theMotionPlan->robotStateTrajectory.nStateDim + i, theMotionPlan->robotStatesParamsStartIndex + jm * theMotionPlan->robotStateTrajectory.nStateDim + i, sucVelocityUpperBound.computeSecondDerivative(-velocity) / (dt * dt), weight);
			}
		}
	}
}


