#include "LocomotionEngine.h"
#include <OptimizationLib/NewtonFunctionMinimizer.h>
#include "Playground/Profiler.h"
#include "OptimizationLib/LBFGS.h"

using namespace LBFGSpp;

LocomotionEngine::LocomotionEngine(LocomotionEngineMotionPlan* motionPlan): param(), solver(param) {
	this->motionPlan = motionPlan;
	energyFunction = new LocomotionEngine_EnergyFunction(this->motionPlan);
	constraints = new LocomotionEngine_Constraints(this->motionPlan);
	constrainedObjectiveFunction = new ConstrainedObjectiveFunction(energyFunction, constraints);
	BFGSSQPminimizer = new SQPFunctionMinimizer_BFGS(10);
	BFGSminimizer = new BFGSFunctionMinimizer(10);
}

LocomotionEngine::~LocomotionEngine(void){
	delete energyFunction;
	delete constraints;
	delete constrainedObjectiveFunction;
	delete BFGSSQPminimizer;
	delete BFGSminimizer;
}

double LocomotionEngine::optimizePlan(int maxIterations){
	double val = 0;
	dVector params;
	this->motionPlan->writeMPParametersToList(params);
	if (params.size() == 0)
		return 0;
	if (energyFunction->printDebugInfo)
		Logger::logPrint("Starting Problem size: %d\n", params.size());

	if (useObjectivesOnly) {
		NewtonFunctionMinimizer minimizer(maxIterations);
		minimizer.maxLineSearchIterations = 12;
		minimizer.printOutput = energyFunction->printDebugInfo;
		//Profiler::startTimer("Minimizer", "Optimize Plan");
		converged = minimizer.minimize(energyFunction, params, val);
		//Profiler::collectTimer("Minimizer");
	}
	else {
		SQPFunctionMinimizer minimizer(maxIterations);
		minimizer.maxLineSearchIterations_ = 12;
		minimizer.printOutput_ = energyFunction->printDebugInfo;
		//Logger::printStatic("There are %d parameters\n", params.size());
		converged = minimizer.minimize(constrainedObjectiveFunction, params, val);
	}

	//Beichen Li: dynamically update the regularizer
//	energyFunction->updateRegularizer(oldFunctionValue - val);
//	if (energyFunction->printDebugInfo)
//		Logger::consolePrint("df: %lf\n", oldFunctionValue - val);
//	oldFunctionValue = val;

	return val;
}

double LocomotionEngine::optimizePlan_BFGS(int maxIterations){
	double val = 0;
	dVector params;
	this->motionPlan->writeMPParametersToList(params);

	if (params.size() == 0)
		return 0;
	if (energyFunction->printDebugInfo)
		Logger::consolePrint("Problem size: %d\n", params.size());

	if (useObjectivesOnly) {
		param.max_iterations = maxIterations;
		param.max_linesearch = 15;

		converged = (solver.MOPTMinimize(*energyFunction, params, oldFunctionValue) >= maxIterations && oldFunctionValue < 1e8);

//		BFGSminimizer->maxLineSearchIterations = 12;
//		BFGSminimizer->printOutput = energyFunction->printDebugInfo;
//		BFGSminimizer->minimize(energyFunction, params, val);
	}
	else {
		BFGSSQPminimizer->maxLineSearchIterations_ = 12;
		BFGSSQPminimizer->printOutput_ = energyFunction->printDebugInfo;
		BFGSSQPminimizer->minimize(constrainedObjectiveFunction, params, val);
	}
	return oldFunctionValue;
}

