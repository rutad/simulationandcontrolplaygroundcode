#include "NaiveDesignSampler.h"


NaiveDesignSampler::NaiveDesignSampler(ModularDesignWindow* modularDesign, LivingBracketParametrizedDesign* livingDesign)
	: LivingDesignSampler(modularDesign, livingDesign)
{

}

NaiveDesignSampler::~NaiveDesignSampler()
{
}

void NaiveDesignSampler::generateDesignSamples(vector<dVector>& designs)
{
	sampleFpBasedDesigns(designs);
	samplePositionBasedDesigns(designs);
	sampleOrientationBasedDesigns(designs);
}

void NaiveDesignSampler::samplePositionBasedDesigns(vector<dVector>& designs)
{
	auto& mirrorMap = modularDesign->rmcMirrorMap;

	for (auto rmc : livingDesign->movableRMCs)
	{
		livingDesign->setDesignParams(livingDesign->defaultParams);
		if (mirrorMap.count(rmc))
			rmc->state.position += V3D(0.05, 0.0, 0.0);
		else
			rmc->state.position += V3D(0.0, 0.0, 0.05);
		designs.push_back(livingDesign->getCurrentDesignParams());
	}
}

void NaiveDesignSampler::sampleOrientationBasedDesigns(vector<dVector>& designs)
{
	auto& mirrorMap = modularDesign->rmcMirrorMap;

	for (auto rmc : livingDesign->movableRMCs)
	{
		if (mirrorMap.count(rmc) == 0) continue;

		livingDesign->setDesignParams(livingDesign->defaultParams);
		rmc->state.orientation = getRotationQuaternion(RAD(90), V3D(0, 0, 1)) * rmc->state.orientation;
		designs.push_back(livingDesign->getCurrentDesignParams());
	}
}

void NaiveDesignSampler::sampleFpBasedDesigns(vector<dVector>& designs)
{
	for (auto bodyFp : livingDesign->bodyFeaturePts)
	{
		livingDesign->setDesignParams(livingDesign->defaultParams);
		bodyFp->coords += V3D(0.0, 0.1, 0.0);
		designs.push_back(livingDesign->getCurrentDesignParams());
	}
}
