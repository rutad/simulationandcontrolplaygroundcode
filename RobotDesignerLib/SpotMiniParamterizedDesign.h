#pragma once
#include "StructureFeature.h"
#include <MathLib\V3D.h>
#include <vector>

#include <MathLib/mathLib.h>
#include <MathLib/Quaternion.h>

#include <ControlLib/Robot.h>
#include "ParameterizedRobotDesign.h"

#include <OptimizationLib/ObjectiveFunction.h>
#include <OptimizationLib/GradientDescentFunctionMinimizer.h>
#include <RobotDesignerLib/LocomotionEngineEnergyFunction.h>
#include <RobotDesignerLib\MOPTWindow.h>

using namespace std;

class SpotMiniParameterizedDesign;

class ParametrizedRobotObjective : public ObjectiveFunction {
public:
	ParametrizedRobotObjective() {};
	ParametrizedRobotObjective(SpotMiniParameterizedDesign* prd);
	virtual double computeValue(const dVector& p);

	LocomotionEngine_EnergyFunction *E();
	SpotMiniParameterizedDesign *prd;
};
/**
	Given a set of parameters, instances of this class will output new robot morphologies.
*/

class SpotMiniParameterizedDesign : public ParameterizedRobotDesign {

public:
	DynamicArray<double> currentParams;
	ParametrizedRobotObjective *objective;
	MOPTWindow *mopt;

public:
	double TEST_DOUBLE = 0.;
	int TEST_i = 0;
	vector<int>  top_layer = { 0, 1, 2, 3 };

public:
	SpotMiniParameterizedDesign(Robot *robot);
	SpotMiniParameterizedDesign(Robot* robot, MOPTWindow *mopt);
	~SpotMiniParameterizedDesign();

	virtual void getCurrentSetOfParameters(DynamicArray<double>& params);
	virtual void setParameters(const DynamicArray<double>& params);

	virtual void optimize();
};


