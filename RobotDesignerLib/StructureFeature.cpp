#include "StructureFeature.h"
#include <MathLib/ConvexHull3D.h>

bool StructureFeature::showAttachmentPoints = false;
bool StructureFeature::showConvexHull = true;
bool StructureFeature::showWireFrameConvexHull = false;

StructureFeature::StructureFeature(const DynamicArray<AttachmentPoint> &attachmentPointsF1, DynamicArray<AttachmentPoint> &attachmentPointsF2) {
	apl1Initial = attachmentPointsF1;
	apl1Final = attachmentPointsF1;
	apl2Initial = attachmentPointsF2;
	apl2Final = attachmentPointsF2;
	generateStructure();
}

StructureFeature::StructureFeature(BaseRobotBodyFeature* feature1, BaseRobotBodyFeature* feature2, RobotBodyPart* rbp) {
//	this->feature1 = feature1;
//	this->feature2 = feature2;
//	this->rbp = rbp;

	feature1->getListOfPossibleAttachmentPoints(rbp, apl1Initial);
	feature2->getListOfPossibleAttachmentPoints(rbp, apl2Initial);

	apl1Final = apl1Initial;
	apl2Final = apl2Initial;
	generateStructure();
}

void removeInactivePoints(vector<AttachmentPoint>& attachmentPoints) {
	for (int i = 0; i<(int)attachmentPoints.size(); i++) {
		if (attachmentPoints.size() > 1) {
			if (attachmentPoints[i].isActive == false) {
				attachmentPoints.erase(attachmentPoints.begin() + i);
				i -= 1;
			}
		}
	}
}

void pruneListOfPoints(vector<AttachmentPoint>& attachmentPoints, const P3D& midPoint, const V3D& axis) {
	//"turn off" all the points that have negative dot products with the direction between the two features
	for (int i = 0; i<(int)attachmentPoints.size(); i++) {
		if (attachmentPoints[i].shouldPrune && axis.dot(attachmentPoints[i].normal) <= 0.1)
			attachmentPoints[i].isActive = false;
	}
	removeInactivePoints(attachmentPoints);
}

void StructureFeature::pruneListsOfPoints(vector<AttachmentPoint>& attachmentPointsStart, vector<AttachmentPoint>& attachmentPointsEnd) {
	return;

	P3D midPoint1, midPoint2;

	for (uint i = 0; i<attachmentPointsStart.size(); i++)
		midPoint1 += attachmentPointsStart[i].point / attachmentPointsStart.size();

	for (uint i = 0; i<attachmentPointsEnd.size(); i++)
		midPoint2 += attachmentPointsEnd[i].point / attachmentPointsEnd.size();

	V3D axis(midPoint1, midPoint2);
	axis.toUnit();

	pruneListOfPoints(attachmentPointsStart, midPoint1, axis);
	pruneListOfPoints(attachmentPointsEnd, midPoint2, -axis);
}

void StructureFeature::generateStructure() {
	pruneListsOfPoints(apl1Final, apl2Final);
//	int zeroIndex = mesh->getVertexCount();
	int zeroIndex = 0; structureMesh.clear();

	vector<P3D> points;
	for (uint i = 0; i < apl1Final.size(); i++)
		points.push_back(apl1Final[i].point);
	for (uint i = 0; i < apl2Final.size(); i++)
		points.push_back(apl2Final[i].point);

	ConvexHull3D::computeConvexHullFromSetOfPoints(points, &structureMesh, true);
}

void StructureFeature::draw(double r, double g, double b) {
	glEnable(GL_LIGHTING);

	if (showAttachmentPoints) {
		glColor3d(0, 1, 0);
		for (uint i = 0; i<apl1Initial.size(); i++)
			drawSphere(apl1Initial[i].point, 0.0014);

		for (uint i = 0; i<apl2Initial.size(); i++)
			drawSphere(apl2Initial[i].point, 0.0014);

		glColor3d(1, 0, 0);
		for (uint i = 0; i<apl1Final.size(); i++)
			drawSphere(apl1Final[i].point, 0.0015);

		for (uint i = 0; i<apl2Final.size(); i++)
			drawSphere(apl2Final[i].point, 0.0015);
	}

	if (showConvexHull || showWireFrameConvexHull) {
		structureMesh.getMaterial().setColor(r, g, b, 1);
//		structureMesh.getMaterial().setColor(MAX(0.0, 100.0 / 256.0 - deltColor), MAX(0.0, 60.0 / 256.0 - deltColor), MAX(0.0, 147.0 / 256.0 - deltColor), 1.0);
		glDisable(GL_LIGHTING);
		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
		if (showWireFrameConvexHull)
			structureMesh.drawMesh();
		glEnable(GL_LIGHTING);
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
		if (showConvexHull){
			structureMesh.drawMesh();
		}
	}

	glDisable(GL_LIGHTING);
}

//returns the distance from the ray's origin if the ray hits the body part, or -1 otherwise...
double StructureFeature::getDistanceToRayOriginIfHit(const Ray& ray){
	double dist = -1.0;
	for (int i = 0; i < structureMesh.getTriangleCount(); i++) {
		GLIndexedTriangle tri = structureMesh.getTriangle(i);
		P3D p1 = structureMesh.getVertex(tri.indexes[0]);
		P3D p2 = structureMesh.getVertex(tri.indexes[1]);
		P3D p3 = structureMesh.getVertex(tri.indexes[2]);
		if ((p1 - p2).length() < 1e-7 || (p1 - p3).length() < 1e-7 || (p2 - p3).length() < 1e-7)
			continue;
		V3D n = V3D(p1, p2).cross(V3D(p1, p3));
		if (fabs(n.dot(ray.direction)) < 1e-6)
			continue;
		double t = n.dot(V3D(ray.origin, p1)) / n.dot(ray.direction);
		P3D p4 = ray.origin + ray.direction * t;
		// judge if p4 lies in triangle (p1, p2, p3)
		double area1 = n.length();
		double area2 = V3D(p4, p1).cross(V3D(p4, p2)).length() + V3D(p4, p2).cross(V3D(p4, p3)).length() + V3D(p4, p3).cross(V3D(p4, p1)).length();
		if (fabs(area1 - area2) > 1e-4)
			continue;
		if (t > -1e-7 && (dist < -1e-7 || t < dist))
			dist = t;
	}
	return dist * ray.direction.length();
}
