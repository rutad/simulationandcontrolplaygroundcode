#include "LivingBracketParametrizedDesign.h"


LivingBracketParametrizedDesign::LivingBracketParametrizedDesign(ModularDesignWindow* modularDesign)
{
	this->modularDesign = modularDesign;
	init();
}

LivingBracketParametrizedDesign::~LivingBracketParametrizedDesign()
{

}

void LivingBracketParametrizedDesign::init()
{
	connectors.clear();
	movableRMCs.clear();
	bodyFeaturePts.clear();
	defaultPositions.clear();
	defaultOrientations.clear();

	auto& rmcRobots = modularDesign->rmcRobots;
	auto& mirrorMap = modularDesign->rmcMirrorMap;
	auto& fpMirrorMap = modularDesign->bodyFpMirrorMap;

	for (auto rmcRobot : rmcRobots)
	{
		for (int i = 0; i < rmcRobot->getRMCCount(); i++)
		{
			RMC* rmc = rmcRobot->getRMC(i);
			if (useSymmParams && mirrorMap.count(rmc) && rmc->state.position[0] <= 0) continue;

			if (rmc->type == LIVING_CONNECTOR)
			{
				LivingConnector* livingConnector = dynamic_cast<LivingConnector*>(rmc);
				if (livingConnector->isFullyConnected())
					connectors.push_back(livingConnector);
			}
			else if (rmc->isMovable())
			{
				movableRMCs.push_back(rmc);
				defaultPositions.push_back(rmc->state.position);
				defaultOrientations.push_back(rmc->state.orientation);
			}
		}
	}

	for (auto& fp : modularDesign->bodyFeaturePts)
	{
		if (useSymmParams && fpMirrorMap.count(&fp) && fp.coords[0] <= 0) continue;
		
		bodyFeaturePts.push_back(&fp);
	}

	// get parameter count and indice
	getParamCountAndStartIndice();
	// get default parameters
	defaultParams = getCurrentDesignParams();
}

void LivingBracketParametrizedDesign::getParamCountAndStartIndice()
{
	int curStartIndex = 0;

	// ******************* position parameters *******************
	{
		posParamStartIndex = curStartIndex;
		curStartIndex += (int)movableRMCs.size() * 3;
	}
	
	// ******************* orientation parameters *******************
	{
		orientParamStartIndex = curStartIndex;
		curStartIndex += (int)movableRMCs.size() * 4;
	}

	// ******************* body feature points parameters *******************
	{
		bodyFpParamStartIndex = curStartIndex;
		curStartIndex += (int)bodyFeaturePts.size() * 4;
	}

	totalParamCount = curStartIndex;
}

dVector LivingBracketParametrizedDesign::getCurrentDesignParams()
{
	dVector p(totalParamCount);

	for (uint i = 0; i < movableRMCs.size(); i++)
	{
		RMC* rmc = movableRMCs[i];
		P3D& pos = rmc->state.position;
		Quaternion& q = rmc->state.orientation;

		p.segment<3>(posParamStartIndex + 3 * i) = pos;
		p.segment<3>(orientParamStartIndex + 4 * i) = q.v;
		p[orientParamStartIndex + 4 * i + 3] = q.s;
	}

	for (uint i = 0; i < bodyFeaturePts.size(); i++)
	{
		RBFeaturePoint* bodyFp = bodyFeaturePts[i];
		p.segment<3>(bodyFpParamStartIndex + 4 * i) = bodyFp->coords;
		p[bodyFpParamStartIndex + 4 * i + 3] = bodyFp->featureSize;
	}

	return p;
}

void LivingBracketParametrizedDesign::setDesignParams(const dVector& p)
{
	for (uint i = 0; i < movableRMCs.size(); i++)
	{
		RMC* rmc = movableRMCs[i];
		P3D& pos = rmc->state.position;
		Quaternion& q = rmc->state.orientation;

		pos = V3D(p.segment<3>(posParamStartIndex + 3 * i));
		q.v = V3D(p.segment<3>(orientParamStartIndex + 4 * i));
		q.s = p[orientParamStartIndex + 4 * i + 3];

		if (useSymmParams)
		{
			modularDesign->propagatePosToMirrorRMC(rmc);
			modularDesign->propagateOrientToMirrorRMC(rmc);
		}
	}

	for (uint i = 0; i < bodyFeaturePts.size(); i++)
	{
		RBFeaturePoint* bodyFp = bodyFeaturePts[i];
		bodyFp->coords = V3D(p.segment<3>(bodyFpParamStartIndex + 4 * i));
		bodyFp->featureSize = p[bodyFpParamStartIndex + 4 * i + 3];

		if (useSymmParams)
		{
			modularDesign->propagatePosToMirrorFp(bodyFp);
		}
	}

	modularDesign->updateLivingBracket();
	modularDesign->createBodyMesh3D();
}
