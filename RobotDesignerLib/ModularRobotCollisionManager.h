#pragma once
#include <GUILib/GLApplication.h>
#include <BulletCollision/btBulletCollisionCommon.h>
#include "RMCSearchTree.h"

class RMCRobot;

class ModularRobotCollisionManager {
public:
	vector<RMCRobot*>& robotList;
	RMCSearchTree* searchTree;
	vector<double> collisionDepths;

public:

	// bullet related
	btCollisionConfiguration* bt_collision_configuration = NULL;
	btCollisionDispatcher* bt_dispatcher = NULL;
	btBroadphaseInterface* bt_broadphase = NULL;
	btCollisionWorld* bt_collision_world = NULL;
	btIDebugDraw* bt_DebugDrawer = NULL;

	// constructor - destructor
	ModularRobotCollisionManager::ModularRobotCollisionManager(vector<RMCRobot*>& _rmcRobots) : robotList(_rmcRobots) {}
	~ModularRobotCollisionManager(void) {};

	// broad phase collision functions
	void initializeCollisionHandler();
	void setupBulletCollisionWorld(RMCSearchNode* node);
	void clearBulletCollisionWorld();
	void performCollisionDetection();

	void setSearchTree(RMCSearchTree* _searchTree) {
		searchTree = _searchTree;
	}

	bool isConnected(AbstractBulletObject* objA, AbstractBulletObject* objB);
};

class GLDebugDrawer : public btIDebugDraw {
	int m_debugMode = DBG_DrawWireframe;

public:

	GLDebugDrawer() {}
	virtual ~GLDebugDrawer() {}

	virtual void drawLine(const btVector3& from, const btVector3& to, const btVector3&  fromColor, const btVector3& toColor) {
		glBegin(GL_LINES);
		glColor3f(fromColor.getX(), fromColor.getY(), fromColor.getZ());
		glVertex3d(from.getX(), from.getY(), from.getZ());
		glColor3f(toColor.getX(), toColor.getY(), toColor.getZ());
		glVertex3d(to.getX(), to.getY(), to.getZ());
		glEnd();
	}

	virtual void    drawLine(const btVector3& from, const btVector3& to, const btVector3& color) {
		glBegin(GL_LINES);
		glColor3f(color.getX(), color.getY(), color.getZ());
		glVertex3d(from.getX(), from.getY(), from.getZ());
		glVertex3d(to.getX(), to.getY(), to.getZ());
		glEnd();
	}

	virtual void drawContactPoint(const btVector3& PointOnB, const btVector3& normalOnB, btScalar distance, int lifeTime, const btVector3& color) {}

	virtual void reportErrorWarning(const char* warningString) {}

	virtual void draw3dText(const btVector3& location, const char* textString) {}

	virtual void    setDebugMode(int debugMode) { m_debugMode = debugMode; }

	virtual int     getDebugMode() const { return m_debugMode; }
};
