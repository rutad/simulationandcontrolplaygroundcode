#pragma once

#include <MathLib/V3D.h>
#include <MathLib/P3D.h>
#include <vector>
#include "RobotBodyFeature.h"

class Motor_XM430 : public RobotJointFeature {
public:
	Motor_XM430(RobotBodyPart* parent, RobotBodyPart* child, const P3D& worldPos, const V3D& jointAxis = V3D(0, 0, 1));
	~Motor_XM430(void);
	
	virtual void getListOfPossibleAttachmentPoints(RobotBodyPart* rbp, DynamicArray<AttachmentPoint>& attachmentPoints);
};

