#include "MotionGraphViewer.h"

#include <GUILib/GLContentManager.h>
#include <GUILib/FreeType.h>
#include <GUILib/GLUtils.h>
#include <stdlib.h>
#include <math.h>
#include <Utils/Logger.h>
#include <MathLib/Segment.h>

MotionGraphViewer::MotionGraphViewer(int posX, int posY, int sizeX, int sizeY) : GLWindow2D(posX,posY, sizeX, sizeY){
	

	bgColorR = bgColorG = bgColorB = bgColorA = 1.0;

}

MotionGraphViewer::~MotionGraphViewer(void){
}


void MotionGraphViewer::draw(){

	if (!motionGraph) return;

	preDraw();

	// *************************** draw nodes ***************************
	glLineWidth(2.0);
	int id = 0;
	int size = motionGraph->nodeSet.size();
	double outterR = 0.3;
	double innerR = min(0.1, outterR * sin(PI / max(size, 2)) * 0.9);

	for (auto node : motionGraph->nodeSet)
	{
		double theta = 2 * PI / size * id;
		double cx = 0.5 - outterR * cos(theta);
		double cy = 0.5 + outterR * sin(theta);

		if (node == selectedNode || node == secondSelectedNode)
			glColor3d(1.0, 0.0, 0.0);
		else if (node == highlightedNode)
			glColor3d(0.0, 1.0, 0.0);
		else
			glColor3d(0.0, 1.0, 1.0);
		drawCircleFill(cx, cy, innerR, 48);
		glColor3f(0.0, 0.0, 0.0);
		drawCircle(cx, cy, innerR, 48);

		id++;
	}

	// *************************** draw edges ***************************
	id = 0;
	map<MotionGraphNode*, P3D> nodePosMap;
	for (auto node : motionGraph->nodeSet)
	{
		double theta = 2 * PI / size * id;
		double cx = 0.5 - outterR * cos(theta);
		double cy = 0.5 + outterR * sin(theta);

		nodePosMap[node] = P3D(cx, cy, 0);
		id++;
	}

	for (auto edge : motionGraph->edgeSet)
	{
		P3D p1 = nodePosMap[edge->source];
		P3D p2 = nodePosMap[edge->sink];

		if (edge == selectedEdge || edge == transitionEdge)
			glColor3d(1.0, 0.0, 0.0);
		else if (edge == highlightedEdge)
			glColor3d(0.0, 1.0, 0.0);
		else
			glColor3d(0.0, 0.8, 1.0);

		double w = innerR;
		double theta = atan2(p2[1] - p1[1], p2[0] - p1[0]);
		double dx = 0.5 * w * sin(theta);
		double dy = -0.5 * w * cos(theta);
		p1[0] += dx; p2[0] += dx;
		p1[1] += dy; p2[1] += dy;
		dx = 0.866 * w * cos(theta);
		dy = 0.866 * w * sin(theta);
		p1[0] += dx; p2[0] -= dx;
		p1[1] += dy; p2[1] -= dy;

		// draw2DLineFill(p1[0], p1[1], p2[0], p2[1], 0.4 * innerR);
		draw2DArrowFill(p1[0], p1[1], p2[0], p2[1], 0.4 * innerR);
	}

	// Restore attributes
	postDraw();
}


//triggered when mouse buttons are pressed
bool MotionGraphViewer::onMouseButtonEvent(int button, int action, int mods, double xPos, double yPos) {
	if (GLWindow2D::onMouseButtonEvent(button, action, mods, xPos, yPos))
		return true;

	if (button == GLFW_MOUSE_BUTTON_LEFT && action == GLFW_PRESS)
	{
		secondSelectedNode = NULL;
		if ((mods & GLFW_MOD_SHIFT) > 0)
		{
			if (selectedNode && highlightedNode && highlightedNode != selectedNode)
			{
				secondSelectedNode = highlightedNode;
				selectedEdge = NULL;
				return true;
			}
		}

		selectedNode = highlightedNode;
		selectedEdge = highlightedEdge;
	}

	

	return true;
}

//triggered when mouse moves
bool MotionGraphViewer::onMouseMoveEvent(double xPos, double yPos) {
	if (GLWindow2D::onMouseMoveEvent(xPos, yPos))
		return true;

	highlightedEdge = NULL;
	highlightedNode = NULL;
	if (pickEdge(xPos, yPos)) return true;
	if (pickNode(xPos, yPos)) return true;
	

	return true;
}

bool MotionGraphViewer::pickNode(double xPos, double yPos)
{
	double x = this->getRelativeXFromViewportX(getViewportXFromWindowX(xPos));
	double y = this->getRelativeYFromViewportY(getViewportYFromWindowY(yPos));

	double outterR = 0.3;
	int id = 0;
	int size = motionGraph->nodeSet.size();
	double innerR = min(0.1, outterR * sin(PI / max(size, 2)) * 0.9);

	for (auto node : motionGraph->nodeSet)
	{
		double theta = 2 * PI / size * id;
		double cx = 0.5 - outterR * cos(theta);
		double cy = 0.5 + outterR * sin(theta);

		if ((x - cx) * (x - cx) + (y - cy) * (y - cy) < innerR * innerR)
		{
			highlightedNode = node;
			return true;
		}
		id++;
	}
	
	return false;
}

bool MotionGraphViewer::pickEdge(double xPos, double yPos)
{
	double x = this->getRelativeXFromViewportX(getViewportXFromWindowX(xPos));
	double y = this->getRelativeYFromViewportY(getViewportYFromWindowY(yPos));

	int id = 0;
	int size = motionGraph->nodeSet.size();
	double outterR = 0.3;
	double innerR = min(0.1, outterR * sin(PI / max(size, 2)) * 0.9);

	map<MotionGraphNode*, P3D> nodePosMap;
	for (auto node : motionGraph->nodeSet)
	{
		double theta = 2 * PI / size * id;
		double cx = 0.5 - outterR * cos(theta);
		double cy = 0.5 + outterR * sin(theta);

		nodePosMap[node] = P3D(cx, cy, 0);
		id++;
	}

	P3D p(x, y, 0);
	for (auto edge : motionGraph->edgeSet)
	{
		P3D p1 = nodePosMap[edge->source];
		P3D p2 = nodePosMap[edge->sink];
		double w = innerR;
		double theta = atan2(p2[1] - p1[1], p2[0] - p1[0]);
		double dx = 0.5 * w * sin(theta);
		double dy = -0.5 * w * cos(theta);
		p1[0] += dx; p2[0] += dx;
		p1[1] += dy; p2[1] += dy;
		dx = 0.866 * w * cos(theta);
		dy = 0.866 * w * sin(theta);
		p1[0] += dx; p2[0] -= dx;
		p1[1] += dy; p2[1] -= dy;

		Segment seg(p1, p2);
		if ((seg.getClosestPointTo(p) - p).norm() < 0.2 * innerR)
		{
			highlightedEdge = edge;
			return true;
		}
	}
	
	return false;
}

void MotionGraphViewer::reset()
{
	selectedNode = NULL;
	secondSelectedNode = NULL;
	selectedEdge = NULL;
	highlightedNode = NULL;
	highlightedEdge = NULL;
}

