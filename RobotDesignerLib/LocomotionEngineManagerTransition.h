#pragma once

#include "LocomotionEngineManager.h"

class LocomotionEngineManagerTransition : public LocomotionEngineManager {
public:
	LocomotionEngineManagerTransition();
	LocomotionEngineManagerTransition(Robot* robot, FootFallPattern* footFallPattern, int nSamplePoints, LocomotionEngineMotionPlan* startPlan, LocomotionEngineMotionPlan* endPlan,
		int startTIndex, int endTIndex);
	LocomotionEngineManagerTransition(Robot* robot, int nSamplePoints);

	~LocomotionEngineManagerTransition();

	void warmStartMOpt();
};

