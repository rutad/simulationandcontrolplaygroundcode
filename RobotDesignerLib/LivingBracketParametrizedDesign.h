#pragma once
#include "ModularDesignWindow.h"

class LivingBracketParametrizedDesign
{
public:
	ModularDesignWindow* modularDesign = NULL;

	// whether use symmetric parametrization
	bool useSymmParams = true;

	// design variables
	vector<LivingConnector*> connectors;
	vector<RMC*> movableRMCs;
	vector<RBFeaturePoint*> bodyFeaturePts;

	vector<P3D> defaultPositions;
	vector<Quaternion> defaultOrientations;

	// design parameters
	int posParamStartIndex = -1;
	int orientParamStartIndex = -1;
	int bodyFpParamStartIndex = -1;
	int totalParamCount = 0;
	dVector defaultParams;

public:
	LivingBracketParametrizedDesign(ModularDesignWindow* modularDesign);
	~LivingBracketParametrizedDesign();

	// intialization
	void init();
	// get parameter count and start indice
	void getParamCountAndStartIndice();
	// get current design parameters
	dVector getCurrentDesignParams();
	// set design parameters
	void setDesignParams(const dVector& p);
};

