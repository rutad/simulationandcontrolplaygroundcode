#pragma once

#include <OptimizationLib/ObjectiveFunction.h>
#include "LocomotionEngineMotionPlan.h"

class LocomotionEngine_EnergyFunction : public ObjectiveFunction {
public:
	DynamicArray<ObjectiveFunction*> objectives;

	LocomotionEngine_EnergyFunction(LocomotionEngineMotionPlan* mp);
	virtual ~LocomotionEngine_EnergyFunction(void);

	//regularizer looks like: r/2 * (p-p0)'*(p-p0). This function can update p0 if desired, given the current value of s.
	void updateRegularizingSolutionTo(const dVector &currentP);
	virtual double computeValue(const dVector& p);

	virtual void addHessianEntriesTo(DynamicArray<MTriplet>& hessianEntries, const dVector& p);
	virtual void addGradientTo(dVector& grad, const dVector& p);

	//this method gets called whenever a new best solution to the objective function is found
	virtual void setCurrentBestSolution(const dVector& p);

	void setupIPSubObjectives();
	void setupIPv2SubObjectives();
	void setupGRFSubObjectives();
	void setupGRFv2SubObjectives();
	void setupGRFv3SubObjectives();
	void setupTransitionSubObjectives();

	void testIndividualGradient(dVector& params);
	void testIndividualHessian(dVector& params);

	//Beichen Li: LBFGS interface
	double operator () (const dVector& p, dVector& grad);

	//Beichen Li: dynamically update regularizer value according to df
	//The update condition contains two parts:
	//1. df should be small enough
	//2. df should be stable enough
	void updateRegularizer(double df) {
		if (df > 0 && df * coefficient < regularizer &&
			dfLast > df && dfLast - df < epsilon * df) {
			regularizer *= 0.1;
			coefficient *= 2.0;
		}
		dfLast = df;
	}

	void resetRegularizer() {
		regularizer = 1.0; //1.0 for Newton's method, 0.1 for LBFGS
		coefficient = 1000.0;
	}

	bool printDebugInfo;


public:
	double regularizer = 1.0; //1.0 for Newton's method, 0.1 for LBFGS
	double coefficient = 1000.0;
	double dfLast = 0.0;
	double epsilon = 0.1;

private:

	//this is the configuration of the sim mesh that is used as a regularizing solution...
	dVector m_p0;
	dVector tmpVec;

	//the energy function operates on a motion plan...
	LocomotionEngineMotionPlan* theMotionPlan;
};

