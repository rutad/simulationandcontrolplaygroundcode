#pragma once
#include <RobotDesignerLib/LocomotionEngineManager.h>
#include <vector>
#include <set>

using namespace std;

class MotionGraphEdge;

class MotionGraphNode
{
public:
	LocomotionEngineManager* manager;
	LocomotionEngineMotionPlan* motionPlan;

	set<MotionGraphEdge*> outEdges;
	set<MotionGraphEdge*> inEdges;

	int index = -1;
public:
	MotionGraphNode(LocomotionEngineManager* manager);
	~MotionGraphNode();
};

class MotionGraphEdge
{
public:
	LocomotionEngineManager* manager;
	LocomotionEngineMotionPlan* motionPlan;

	MotionGraphNode* source;
	MotionGraphNode* sink;

public:
	MotionGraphEdge(LocomotionEngineManager* manager, MotionGraphNode* source, MotionGraphNode* sink);
	~MotionGraphEdge();
};

class MotionGraph
{
public:
	MotionGraph();
	~MotionGraph();

	vector<MotionGraphNode*> nodeSet;
	vector<MotionGraphEdge*> edgeSet;

	int getNodeIndex(MotionGraphNode* node);
	int getEdgeIndex(MotionGraphEdge* edge);
	MotionGraphNode* getNode(int index) { return nodeSet[index]; }
	MotionGraphEdge* getEdge(int index) { return edgeSet[index]; }

	// the motion tree is responsible for release the memory of LocomotionManager
	void addNode(MotionGraphNode* node);
	void addEdge(MotionGraphEdge* edge);
	void removeNode(MotionGraphNode* node);
	void removeEdge(MotionGraphEdge* edge);

	MotionGraphEdge* findEdge(MotionGraphNode* source, MotionGraphNode* sink);

	void saveToFile(FILE* fp);
	void loadFromFile(FILE* fp, Robot* robot);
};

