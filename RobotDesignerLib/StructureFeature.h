#pragma once

#include "RobotBodyFeature.h"
#include <MathLib\V3D.h>
#include <GUILib/GLMesh.h>
#include <GUILib/GLUtils.h>
#include <vector>
#include <MathLib/mathLib.h>
#include <MathLib/Quaternion.h>

/*
	Each robot body part consists of multiple structures which connect different pairs of body features. The connection
	is generated by computing the convex hull given the list of attachment points of the two features
*/
class StructureFeature {
public:
	static bool showAttachmentPoints;
	static bool showConvexHull;
	static bool showWireFrameConvexHull;

	GLMesh structureMesh;

private:
	void drawConvexHull();

//	BaseRobotBodyFeature* feature1 = NULL;
//	BaseRobotBodyFeature* feature2 = NULL;
//	RobotBodyPart* rbp = NULL;

	DynamicArray<AttachmentPoint> apl1Initial, apl1Final, apl2Initial, apl2Final;

	void generateStructure();

public:
	StructureFeature(BaseRobotBodyFeature* feature1, BaseRobotBodyFeature* feature2, RobotBodyPart* rbp);
	StructureFeature(const DynamicArray<AttachmentPoint> &attachmentPointsF1, DynamicArray<AttachmentPoint> &attachmentPointsF2);


	virtual ~StructureFeature(void) {}

	virtual void draw(double r, double g, double b);
	void pruneListsOfPoints(DynamicArray<AttachmentPoint>& attachmentPointsStart, DynamicArray<AttachmentPoint>& attachmentPointsEnd);
	//returns the distance from the ray's origin if the ray hits the body part, or -1 otherwise...
	double getDistanceToRayOriginIfHit(const Ray& ray);

};

