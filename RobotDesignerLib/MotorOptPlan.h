#pragma once

#include "RMCRobot.h"
#include "LivingMotor.h"
#include "LivingConnector.h"

class MotorOptPlan
{
public:
	vector<LivingConnector*> livingConnectors;
	vector<LivingMotor*> livingMotors;
	vector<LivingMotor*> movableMotors; // motor that are not rigidly attached to its parent, so that it can change its orientation.
	vector<Quaternion> defaultOrientations; // the motor orientations when initializing the optimization.
	vector<V3D> rotationAxes; // rotation axes in world coord.
	map<LivingMotor*, int> livingIndexMap;
	map<LivingMotor*, int> movableIndexMap;

	bool optimizeMotorEmbedding = true;
	bool optimizeMotorBracket = true;
	int motorEmbeddingStartIndex = -1;
	int bracketParamStartIndex = -1;
	int totalParamNum = -1;

public:
	MotorOptPlan();
	~MotorOptPlan();

	void initialize(vector<RMCRobot*>& rmcRobots);

	void getParamCountAndStartIndice();

	void setParamsFromList(const dVector& p);
	void writeParamsToList(dVector& p);
};

